#!/bin/bash

set -e
set -x 
project_name="etl_lowes_neo4j_matching"
version="0.0.1"
project_flow_to_be_schedule="neo4j_matching"

# call Start application of azkaban machine
bash /home/ubuntu/azk_deployment_scripts/start_application.sh $project_name $version $project_flow_to_be_schedule
