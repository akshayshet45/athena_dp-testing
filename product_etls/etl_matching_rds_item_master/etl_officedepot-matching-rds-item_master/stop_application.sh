#!/bin/bash

set -e
set -x

project_name="etl_officedepot-matching-rds-item_master"

# call Stop application of azkaban machine
bash /home/ubuntu/azk_deployment_scripts/stop_application.sh $project_name

echo "$(date) :  $project_name application clean job was successfully finished">/tmp/startLog.txt
