#!/bin/bash

set -e
set -x 

project_name="dp_snapshot_test"

# call Stop application of azkaban machine
bash /home/ubuntu/azk_deployment_scripts/stop_application.sh $project_name 

echo "$(date) : $project_name is cleaned">>/tmp/startLog.log

