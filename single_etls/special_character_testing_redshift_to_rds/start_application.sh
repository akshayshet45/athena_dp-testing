#!/bin/bash

set -e
set -x 
project_name="special_character_testing_redshift_to_rds"
version="0.0.1"
project_flow_to_be_schedule="special_character_testing_redshift_to_rds"

# call Start application of azkaban machine
bash /home/ubuntu/azk_deployment_scripts/start_application.sh $project_name $version $project_flow_to_be_schedule
