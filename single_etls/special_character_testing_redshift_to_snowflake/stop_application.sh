#!/bin/bash

set -e

project_name="special_character_testing_redshift_to_snowflake"
project_root_dir="/tmp/$project_name"
sudo rm -rf $project_root_dir
mkdir -p $project_root_dir

echo "$project_name application clean job was successfully finished">/tmp/mysql_to_mysql_datatype1.log
