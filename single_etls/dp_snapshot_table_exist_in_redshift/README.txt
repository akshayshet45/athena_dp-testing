//Run following command in order to build the project azkaban zip :

mvn clean package

//default property values for the above built project are :

boomerang.etl.date = {{current_date}}
boomerang.skipvalidation = true
boomerang.semanticvalidation = false

//in order to build the project with custom properties, run the following :

mvn clean package -Dboomerang.etl.date=20150101 -Dboomerang.skipvalidation=true -Dboomerang.semanticvalidation=true
