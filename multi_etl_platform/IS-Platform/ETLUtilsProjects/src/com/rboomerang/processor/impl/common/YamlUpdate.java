//this program will take 5 arguments: 1.client_name 2.referenceFile.headers 3.referenceFile.mapping 4. outputfile.mapping 5.outputFileHeaders
package com.rboomerang.processor.impl.common;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;

public class YamlUpdate {

	public static void main(String[] args) throws IOException,
			InterruptedException {

		String user_home = System.getProperty("user.home");
		String sourcepath = user_home
				+ "/athena_poconfig/price_optimizer/vars/configs/client_env/"
				+ args[0] + "/" + args[0] + "_prod_config.yml";
		System.out.println(sourcepath);
		String destinationpath = sourcepath;
		File file = new File(sourcepath);
		if (file.exists()) {
			// System.out.println(destinationpath);
			YamlReader reader = new YamlReader(new FileReader(sourcepath));

			// Getting yml file data in map and editing needed value
			Map temp_start = (Map) reader.read();
			// System.out.println(temp_start);
			TreeMap start = new TreeMap(temp_start);
			String key = (String) start.firstKey();
			Map temp_price_optimizer_configs = (Map) start
					.get("price_optimizer_configs");
			TreeMap price_optimizer_configs = new TreeMap(
					temp_price_optimizer_configs);
			price_optimizer_configs.put("referenceFile.headers",YamlUpdate.toCamelCase(args[1]));
			price_optimizer_configs.put("referenceFile.mapping", args[2]);
			price_optimizer_configs.put("outputFileHeaders", YamlUpdate.toCamelCase(args[3]));
			price_optimizer_configs.put("outputfile.mapping", args[4]);
			TreeMap finalMap = new TreeMap();
			finalMap.put(key, price_optimizer_configs);
			System.out.println(finalMap);

			// updating yml file
			if (finalMap != null) {
				YamlWriter writer = new YamlWriter(new FileWriter(
						destinationpath));
				writer.write(finalMap);
				writer.close();

			}
		}
	}

	public static String toCamelCase(String inputString) {
		String result = "";
		if (inputString.length() == 0) {
			return result;
		}
		char firstChar = inputString.charAt(0);
		char firstCharToUpperCase = Character.toUpperCase(firstChar);
		result = result + firstCharToUpperCase;
		for (int i = 1; i < inputString.length(); i++) {
			char currentChar = inputString.charAt(i);
			char previousChar = inputString.charAt(i - 1);
			if (previousChar == ' ') {
				char currentCharToUpperCase = Character
						.toUpperCase(currentChar);
				result = result + currentCharToUpperCase;
			} else {
				char currentCharToLowerCase = Character
						.toLowerCase(currentChar);
				result = result + currentCharToLowerCase;
			}
		}
		return result;
	}

}
