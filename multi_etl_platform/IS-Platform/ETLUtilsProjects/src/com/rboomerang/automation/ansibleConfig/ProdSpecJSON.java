package com.rboomerang.automation.ansibleConfig;

import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rboomerang.automation.ansibleConfig.javabeans.ProdSpecBean;

public class ProdSpecJSON implements Closeable {
	private static Logger logger = LoggerFactory.getLogger(ProdSpecJSON.class);

	private String prodSpecPath;
	private FileReader fileReader = null;
	private JSONParser parser = null;
	private JSONObject jsonObj = null;
	private ProdSpecBean prodSpecBean = null;

	public ProdSpecJSON(String prodSpecPath) throws IOException, ParseException, FileNotFoundException {
		this.prodSpecPath = prodSpecPath;

		initialzeProdSpec();
		logger.info("ProdSpecInitialization constructor called");
	}

	public ProdSpecBean getProdSpecObject() {
		logger.info("ProdSpecInitialization ProdSpecBean called");
		return prodSpecBean;
	}

	@SuppressWarnings("static-access")

	private void initialzeProdSpec() throws IOException, ParseException, FileNotFoundException {
		fileReader = new FileReader(this.prodSpecPath);
		parser = new JSONParser();
		Object jsonElement = parser.parse(fileReader);
		jsonObj = (JSONObject) jsonElement;
		JSONArray configArray = (JSONArray) jsonObj.get(prodSpecBean.CONFIGS);

		prodSpecBean = new ProdSpecBean();

		for (int i = 0, size = configArray.size(); i < size; i++) {
			if (configArray.get(i).toString().toLowerCase().equals("canvas")) {
				prodSpecBean.setCanvas(true);
			} else if (configArray.get(i).toString().toLowerCase().equals("po")) {
				prodSpecBean.setPo(true);

			} else if (configArray.get(i).toString().toLowerCase().equals("ppi")) {
				prodSpecBean.setPpi(true);

			}
		}
       
		//set canvas details
		prodSpecBean.setClient((String) jsonObj.get(prodSpecBean.CLIENT));
		prodSpecBean.setClientEmailDomain((String) jsonObj.get(prodSpecBean.CLIENT_EMAIL_DOMAIN));
		prodSpecBean.setClientName((String) jsonObj.get(prodSpecBean.CLIENTNAME));
		prodSpecBean.setClientWebsite((String) jsonObj.get(prodSpecBean.CLIENT_WEBSITE));
		prodSpecBean.setLogoName((String) jsonObj.get(prodSpecBean.LOGONAME));
		
		
		
		//set redshift details
		prodSpecBean.setClientRedshiftHost((String) jsonObj.get(prodSpecBean.CLIENT_REDSHIFT_HOST));
		prodSpecBean.setClientRedshiftUser((String) jsonObj.get(prodSpecBean.CLIENT_REDSHIFT_USER));
		prodSpecBean.setClientRedshiftDatabase((String) jsonObj.get(prodSpecBean.CLIENT_REDSHIFT_DATABASE));
		prodSpecBean.setClientRedshiftPassword((String) jsonObj.get(prodSpecBean.CLIENT_REDSHIFT_PASSWORD));
		
		
		//set ppi
		
		JSONObject ppiObject=(JSONObject) jsonObj.get("ppi");
		
		prodSpecBean.setClientScraperSite((String) ppiObject.get(prodSpecBean.CLIENT_SCRAPER_SITE));
		prodSpecBean.setClientScraperUrl((String) ppiObject.get(prodSpecBean.CLIENT_SCRAPER_URL));
		
		prodSpecBean.setClientRedshiftPpiUser((String) ppiObject.get(prodSpecBean.CLIENT_REDSHIFT_PPI_USER));
		prodSpecBean.setClientRedshiftPpiUserPass((String) ppiObject.get(prodSpecBean.CLIENT_REDSHIFT_PPI_USER_PASS));
		
		//set po 
		JSONObject poObject=(JSONObject) jsonObj.get("po");

		prodSpecBean.setClientRedshiftPOUser((String) poObject.get(prodSpecBean.CLIENT_REDSHIFT_PO_USER));
		prodSpecBean.setClientRedshiftPOUserPass((String) poObject.get(prodSpecBean.CLIENT_REDSHIFT_PO_USER_PASS));
		
		prodSpecBean.setClientSftpHost((String)poObject.get(prodSpecBean.CLIENT_SFTP_HOST));
		prodSpecBean.setClientSftpUser((String)poObject.get(prodSpecBean.CLIENT_SFTP_USER));
		prodSpecBean.setClientSftpPassword((String)poObject.get(prodSpecBean.CLIENT_SFTP_PASSWORD));

		logger.info("ProdSpecInitialization initialzeProdSpec called");
	}

	public void close() throws IOException {
		if (fileReader != null)
			fileReader.close();
		logger.info("ProdSpecInitialization close called");
	}

}
