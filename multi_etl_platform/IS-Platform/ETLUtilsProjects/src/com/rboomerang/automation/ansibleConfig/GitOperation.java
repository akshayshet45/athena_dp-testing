package com.rboomerang.automation.ansibleConfig;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ResetCommand.ResetType;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GitOperation {
	private static Logger logger = LoggerFactory.getLogger(GitOperation.class);

	private static String localPath;
	private static String remotePath;
	private Repository localRepo;
	private static Git git;
	static UsernamePasswordCredentialsProvider creds;

	public GitOperation(String pLocalPath, String pRemotePath) throws IOException {

		localPath = pLocalPath;
		remotePath = pRemotePath;
		localRepo = new FileRepository(localPath + "/.git");
		git = new Git(localRepo);
		creds = new UsernamePasswordCredentialsProvider("jenkins", "jenkins");

	}

	public void setupRepo() {

		Path path = Paths.get(localPath);
		try {
			Files.createDirectories(path);
			logger.info("Directory created:Ansible_Config  ...\n");

		} catch (Exception e) {
			logger.info("Directory exists:Ansible_Config ...\n");
		}

		String localPath = path.toString();
//		logger.info("localPath : " + localPath.toString());
		try {
			doClone();
			logger.info("cloned repo ...\t " + localPath);
		} catch (Exception e) {
			logger.info("Repo already cloned...\t" + localPath);

		}

		try {
			pull();
			logger.info("pulled repo ...\t" + localPath);
		} catch (Exception e) {
			logger.info("failed to pull repo ...\t " + localPath);
		}

		try {
			resetAndClean();
			logger.info("cleaned everything ...\t " + localPath);
		} catch (Exception e) {
			logger.info("failed to clean repo ...\t " + localPath);
			e.printStackTrace();
		}

	}

	public void pushFile(String file, String meassage) {
		try {
			pull();
			logger.info("pulled repo ...... " + localPath);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			add(file);
			logger.info("added file to commit ...... " + file);
			commit(meassage);		
			push();
			logger.info("Pushed file into repo...... " + file);

		} catch (Exception e) {
			logger.info("Commit failed ...... " + localPath + " \nTrying to clean the stash");

			try {
				resetAndClean();
			} catch (Exception e1) {
				logger.info("Error cleaning up changes/stash ...... " + localPath);
			}

		}
	}

	private static void doClone() throws IOException, GitAPIException {
		Git.cloneRepository().setURI(remotePath).setDirectory(new File(localPath)).setCredentialsProvider(creds).call();

	}

	private static void add(String addFile) throws IOException, GitAPIException {
		logger.info("Adding file to commit...\t" + addFile);

		git.add().addFilepattern(addFile).call();
	}

	private static void commit(String message) throws IOException, GitAPIException, JGitInternalException {
		logger.info("Setting  message to commit...\t" + message);
		git.commit().setMessage(message).call();
	}

	private static void push() throws IOException, JGitInternalException, GitAPIException {
		git.push().setCredentialsProvider(creds).call();
	}

	private static void resetAndClean() throws IOException, JGitInternalException, GitAPIException {
		git.reset().setMode(ResetType.HARD).setRef("origin/master").call();
		git.clean().setCleanDirectories(true).setIgnore(false).call();
	}

	private static void pull() throws IOException, GitAPIException {
		git.pull().setCredentialsProvider(creds).call();
	}

	public void close() {
		git.close();
	}
}
