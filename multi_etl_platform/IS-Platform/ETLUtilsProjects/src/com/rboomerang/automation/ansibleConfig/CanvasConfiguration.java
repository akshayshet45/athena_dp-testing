package com.rboomerang.automation.ansibleConfig;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rboomerang.automation.ansibleConfig.javabeans.ProdSpecBean;

public class CanvasConfiguration {
	static String prodSpecPath;

	private static Logger logger = LoggerFactory.getLogger(CanvasConfiguration.class);
	private static String client;
	private static String user_home = System.getProperty("user.home");
	private static ProdSpecBean prodSpecDetails;

	private static boolean commit;

	public static void main(String args[]) {
		
		try {
			if(args.length!=1){
				throw new Exception("arguments not passed");
						}
						//client = "acme";// argu
						prodSpecPath=args[0];
						//prodSpecPath = user_home+"athena_is-platform/acme/configs/product-spec.json"; /// argument
				        System.out.println(prodSpecPath);
			prodSpecDetails = new ProdSpecJSON(prodSpecPath).getProdSpecObject();
			client =prodSpecDetails.getClient();
			commit =false;
			if (client==null){
				throw new Exception("can not create configs for  client, please check product-spec json");
			}
			if(prodSpecDetails.isCanvas())
			{
			generateCanvasConfig();
			}
			if(prodSpecDetails.isPpi())
			{
			generatePpiConfig();
			}
			if(prodSpecDetails.isPo())
			{
			generatePoConfig();
			}

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	private static void generatePoConfig() {

		final String poClientEnvFile = "/Ansible_Config/athena_poconfig/price_optimizer/vars/configs/client_env/acme/acme_prod_config.yml";
		String remotePath = "https://athena.unfuddle.com/git/athena_poconfig/";
		String inputFilePath = user_home + poClientEnvFile;
		String outputFilePath = user_home + "/Ansible_Config/athena_poconfig/price_optimizer/vars/configs/client_env/" + client + "/"
				+ client + "_prod_config.yml";
		
		try {
			GitOperation gitOperation = new GitOperation(user_home + "/Ansible_Config/athena_poconfig/", remotePath);
			gitOperation.setupRepo();
			updateFile(getPoEnvConfigValues(prodSpecDetails), inputFilePath, outputFilePath, gitOperation, "price_optimizer");
		} catch (Exception e) {
			e.printStackTrace();
		}

			
	}

	private static void generatePpiConfig() {

		final String ppiClientSpecificfile = "/Ansible_Config/athena_ppiconfig/ppi/vars/configs/client_specific/acme_config.yml";
		final String ppiClientEnvFile = "/Ansible_Config/athena_ppiconfig/ppi/vars/configs/client_env/acme/acme_prod_config.yml";
		String remotePath = "https://athena.unfuddle.com/git/athena_ppiconfig/";

		String inputFilePath = user_home + ppiClientSpecificfile;
		String outputFilePath = user_home + "/Ansible_Config/athena_ppiconfig/ppi/vars/configs/client_specific/"
				+ client + "_config.yml";
		GitOperation gitOperation=null;
		try {
			 gitOperation = new GitOperation(user_home + "/Ansible_Config/athena_ppiconfig/", remotePath);
			gitOperation.setupRepo();
			updateFile(getPpiConfigValues(prodSpecDetails), inputFilePath, outputFilePath, gitOperation, "ppi");
			inputFilePath = user_home + ppiClientEnvFile;
			outputFilePath = user_home + "/Ansible_Config/athena_ppiconfig/ppi/vars/configs/client_env/" + client + "/"
					+ client + "_prod_config.yml";
			updateFile(getPpiEnvConfigValues(prodSpecDetails), inputFilePath, outputFilePath, gitOperation, "ppi");
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			gitOperation.close();

		}

	}

	private static Map<String, String> getPoEnvConfigValues(ProdSpecBean prodSpecDetails2) {

		HashMap<String, String> poConfig = new HashMap<String, String>();
		poConfig.put("<" + ProdSpecBean.CLIENT + ">", prodSpecDetails.getClient());
		poConfig.put("<" + ProdSpecBean.CLIENT_REDSHIFT_HOST + ">", prodSpecDetails.getClientRedshiftHost());
		poConfig.put("<" + ProdSpecBean.CLIENT_REDSHIFT_DATABASE + ">", prodSpecDetails.getClientRedshiftDatabase());
		poConfig.put("<" + ProdSpecBean.CLIENT_REDSHIFT_PO_USER_PASS + ">",prodSpecDetails.getClientRedshiftPOUserPass());
		poConfig.put("<" + ProdSpecBean.CLIENT_REDSHIFT_PO_USER + ">", prodSpecDetails.getClientRedshiftPOUser());
		poConfig.put("<" + ProdSpecBean.CLIENT_SFTP_HOST + ">", prodSpecDetails.getClientSftpHost());
		poConfig.put("<" + ProdSpecBean.CLIENT_SFTP_USER + ">", prodSpecDetails.getClientSftpPassword());
		poConfig.put("<" + ProdSpecBean.CLIENT_SFTP_PASSWORD + ">", prodSpecDetails.getClientSftpUser());
		return poConfig;
	}

	private static void generateCanvasConfig() {
		String remotePath = "https://athena.unfuddle.com/git/athena_canvasconfig/";
		String canvasClientSpecificfileSample = "/Ansible_Config/athena_canvasconfig/canvas/vars/configs/client_specific/acme_config.yml";
		String canvasClientEnvSample = "/Ansible_Config/athena_canvasconfig/canvas/vars/configs/client_env/acme/acme_prod_config.yml";
		String inputFilePath = user_home + canvasClientSpecificfileSample;
		String outputFilePath = user_home + "/Ansible_Config/athena_canvasconfig/canvas/vars/configs/client_specific/"
				+ client + "_config.yml";
		GitOperation gitOperation = null;
		try {
			gitOperation = new GitOperation(user_home + "/Ansible_Config/athena_canvasconfig/", remotePath);
			gitOperation.setupRepo();
			updateFile(getCanvasClientConfigValues(prodSpecDetails), inputFilePath, outputFilePath, gitOperation,
					"canvas");
			inputFilePath = user_home + canvasClientEnvSample;
			outputFilePath = user_home + "/Ansible_Config/athena_canvasconfig/canvas/vars/configs/client_env/" + client
					+ "/" + client + "_prod_config.yml";
			updateFile(getCanvasEnvConfigValues(prodSpecDetails), inputFilePath, outputFilePath, gitOperation,
					"canvas");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			gitOperation.close();

		}
	}

	static void updateFile(Map<String, String> clientConfigs, String inputFilePath, String outputFilePath,
			GitOperation gitOperation, String component) {
		String strLine;
		BufferedReader br;
		try {

			Path path = Paths.get(outputFilePath);
			Files.createDirectories(path.getParent());
			Files.createFile(path);
			br = new BufferedReader(new FileReader(inputFilePath));
			StringBuilder replacedFile = new StringBuilder();
			while ((strLine = br.readLine()) != null) {
				for (Entry<String, String> entry : (clientConfigs).entrySet()) {
					if (entry.getValue() != null && !entry.getValue().isEmpty()
							&& strLine.matches("(.*?)" + entry.getKey() + "(.*?)")) {
						strLine = strLine.replaceFirst(entry.getKey(), entry.getValue());
					}
				}

				replacedFile.append(strLine);
				replacedFile.append("\n");

			}
			Writer output = new FileWriter(outputFilePath);
			output.write(replacedFile.toString());
			output.flush();
			br.close();
			logger.info("File created... " + outputFilePath);
			output.close();
			String newFile = outputFilePath.substring(outputFilePath.indexOf(component + "/"));
			if (commit)
			{
				gitOperation.pushFile(newFile, "(IS Automation) Adding " + component + " configs for " + client);
			}
		} catch (FileAlreadyExistsException e) {
			logger.error(
					"File already exists.please delete the file from git or change it manually... " + outputFilePath);
		} catch (IOException e) {
			logger.error("Error: " + e);
		}

	}

	static HashMap<String, String> getCanvasClientConfigValues(ProdSpecBean prodSpecDetails) {
		HashMap<String, String> canvasConfig = new HashMap<String, String>();
		canvasConfig.put("<" + ProdSpecBean.CLIENTNAME + ">", prodSpecDetails.getClientName());
		canvasConfig.put("<" + ProdSpecBean.LOGONAME + ">", prodSpecDetails.getLogoName());
		canvasConfig.put("<" + ProdSpecBean.CLIENT_EMAIL_DOMAIN + ">", prodSpecDetails.getClientEmailDomain());
		canvasConfig.put("<" + ProdSpecBean.CLIENT_WEBSITE + ">", prodSpecDetails.getClientWebsite());
		return canvasConfig;

	}

	static HashMap<String, String> getCanvasEnvConfigValues(ProdSpecBean prodSpecDetails) {
		HashMap<String, String> canvasConfig = new HashMap<String, String>();
		canvasConfig.put("<" + ProdSpecBean.CLIENT + ">", prodSpecDetails.getClient());
		return canvasConfig;

	}

	static HashMap<String, String> getPpiEnvConfigValues(ProdSpecBean prodSpecDetails) {
		HashMap<String, String> ppiConfig = new HashMap<String, String>();
		ppiConfig.put("<" + ProdSpecBean.CLIENT_REDSHIFT_HOST + ">", prodSpecDetails.getClientRedshiftHost());
		ppiConfig.put("<" + ProdSpecBean.CLIENT_REDSHIFT_DATABASE + ">", prodSpecDetails.getClientRedshiftDatabase());
		ppiConfig.put("<" + ProdSpecBean.CLIENT_REDSHIFT_PPI_USER_PASS + ">",
				prodSpecDetails.getClientRedshiftPpiUserPass());
		ppiConfig.put("<" + ProdSpecBean.CLIENT_REDSHIFT_PPI_USER + ">", prodSpecDetails.getClientRedshiftPpiUser());
		return ppiConfig;
	}

	static HashMap<String, String> getPpiConfigValues(ProdSpecBean prodSpecDetails) {
		HashMap<String, String> ppiConfig = new HashMap<String, String>();
		ppiConfig.put("<" + ProdSpecBean.CLIENT_SCRAPER_SITE + ">", prodSpecDetails.getClientScraperSite());
		ppiConfig.put("<" + ProdSpecBean.CLIENT_SCRAPER_URL + ">", prodSpecDetails.getClientScraperUrl());

		return ppiConfig;
	}

}
