package com.rboomerang.automation.ansibleConfig.javabeans;

public class ProdSpecBean {
	public static final String CONFIGS="configuration";

	public static final String LOGONAME = "clientLogoName";
	public static final String CLIENTNAME = "clientName";
	public static final String CLIENT = "client";
    public static final String CLIENT_WEBSITE = "clientWebsite";
    public static final String CLIENT_EMAIL_DOMAIN = "clientEmailDomain";
	
	public static final String CLIENT_REDSHIFT_HOST = "clientRedshiftHost";
	public static final String CLIENT_REDSHIFT_DATABASE = "clientRedshiftDataBase";
	public static final String CLIENT_REDSHIFT_USER = "clientRedshiftUser";
	public static final String CLIENT_REDSHIFT_PASSWORD = "clientRedshiftPassword";
	
	
	public static final String CLIENT_SCRAPER_SITE = "clientScraperSite";
	public static final String CLIENT_SCRAPER_URL = "clientScraperURL";
	public static final String CLIENT_REDSHIFT_PPI_USER = "clientRedshiftPpiUser";
	public static final String CLIENT_REDSHIFT_PPI_USER_PASS="clientRedshiftPpiUserPass";

	public static final String CLIENT_REDSHIFT_PO_USER = "clientRedshiftPOUser";
	public static final String CLIENT_REDSHIFT_PO_USER_PASS="clientRedshiftPOUserPass";
	
	public static final String CLIENT_SFTP_HOST = "sftpHost";
	public static final String CLIENT_SFTP_USER = "sftpUser";
	public static final String CLIENT_SFTP_PASSWORD = "sftpPass";
	
	
	
	
	private String logoName;
	private String clientName;
	private String client;
	private String clientWebsite;
	private String clientEmailDomain;
	private String clientScraperSite;
	private String clientScraperUrl;
	private String clientRedshiftHost;
	private String clientRedshiftDatabase;
	private String clientRedshiftUser;
	private String clientRedshiftPassword;
	private String clientRedshiftPOUser;
	private String clientRedshiftPOUserPass;
	private String clientSftpHost;
	private String clientSftpUser;
	private String clientSftpPassword;
	private String clientRedshiftPpiUser;
	private String clientRedshiftPpiUserPass;
	private boolean canvas;
	private boolean ppi;
	private boolean po;


	public String getLogoName() {
		return logoName;
	}
	public void setLogoName(String logoName) {
		this.logoName = logoName;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getClientWebsite() {
		return clientWebsite;
	}
	public void setClientWebsite(String clientWebsite) {
		this.clientWebsite = clientWebsite;
	}
	public String getClientEmailDomain() {
		return clientEmailDomain;
	}
	public void setClientEmailDomain(String clientEmailDomain) {
		this.clientEmailDomain = clientEmailDomain;
	}
	public String getClientScraperSite() {
		return clientScraperSite;
	}
	public void setClientScraperSite(String clientScraperSite) {
		this.clientScraperSite = clientScraperSite;
	}
	public String getClientScraperUrl() {
		return clientScraperUrl;
	}
	public void setClientScraperUrl(String clientScraperUrl) {
		this.clientScraperUrl = clientScraperUrl;
	}
	public String getClientRedshiftHost() {
		return clientRedshiftHost;
	}
	public void setClientRedshiftHost(String clientRedshiftHost) {
		this.clientRedshiftHost = clientRedshiftHost;
	}
	public String getClientRedshiftDatabase() {
		return clientRedshiftDatabase;
	}
	public void setClientRedshiftDatabase(String clientRedshiftDatabase) {
		this.clientRedshiftDatabase = clientRedshiftDatabase;
	}

	public String getClientRedshiftUser() {
		return clientRedshiftUser;
	}
	public void setClientRedshiftUser(String clientRedshiftUser) {
		this.clientRedshiftUser = clientRedshiftUser;
	}
	public String getClientRedshiftPassword() {
		return clientRedshiftPassword;
	}
	public void setClientRedshiftPassword(String clientRedshiftPassword) {
		this.clientRedshiftPassword = clientRedshiftPassword;
	}
	public String getClientRedshiftPOUser() {
		return clientRedshiftPOUser;
	}
	public void setClientRedshiftPOUser(String clientRedshiftPOUser) {
		this.clientRedshiftPOUser = clientRedshiftPOUser;
	}
	public String getClientRedshiftPOUserPass() {
		return clientRedshiftPOUserPass;
	}
	public void setClientRedshiftPOUserPass(String clientRedshiftPOUserPass) {
		this.clientRedshiftPOUserPass = clientRedshiftPOUserPass;
	}
	public String getClientRedshiftPpiUser() {
		return clientRedshiftPpiUser;
	}
	public void setClientRedshiftPpiUser(String clientRedshiftPpiUser) {
		this.clientRedshiftPpiUser = clientRedshiftPpiUser;
	}
	public String getClientRedshiftPpiUserPass() {
		return clientRedshiftPpiUserPass;
	}
	public void setClientRedshiftPpiUserPass(String clientRedshiftPpiUserPass) {
		this.clientRedshiftPpiUserPass = clientRedshiftPpiUserPass;
	}
	
	public String getClientSftpHost() {
		return clientSftpHost;
	}
	public void setClientSftpHost(String clientSftpHost) {
		this.clientSftpHost = clientSftpHost;
	}
	public String getClientSftpUser() {
		return clientSftpUser;
	}
	public void setClientSftpUser(String clientSftpUser) {
		this.clientSftpUser = clientSftpUser;
	}
	public String getClientSftpPassword() {
		return clientSftpPassword;
	}
	public void setClientSftpPassword(String clientSftpPassword) {
		this.clientSftpPassword = clientSftpPassword;
	}
	public boolean isCanvas() {
		return canvas;
	}
	public void setCanvas(boolean canvas) {
		this.canvas = canvas;
	}
	public boolean isPpi() {
		return ppi;
	}
	public void setPpi(boolean ppi) {
		this.ppi = ppi;
	}
	public boolean isPo() {
		return po;
	}
	public void setPo(boolean po) {
		this.po = po;
	}
	


	
}
