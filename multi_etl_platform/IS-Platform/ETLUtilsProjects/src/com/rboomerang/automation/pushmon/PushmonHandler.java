package com.rboomerang.automation.pushmon;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.Keys;

public class PushmonHandler {
	private static Logger logger = LoggerFactory.getLogger(PushmonHandler.class);
	private FileReader fileReader = null;
	private JSONObject pushmonSpecJson = null;
	private final String pushmonUserName = "madhu@boomerangcommerce.com";
	private final String pushmonPassword = "boomerang123$";
	private final String escalationEmailId = "is-team@boomerangcommerce.com";

	public static void main(String[] args) throws InterruptedException, IOException, ParseException {

		if (args.length != 4) {
			logger.info("mismatch in number of arguments passed to PushmonHandler,expected 4, found " + args.length);
			System.exit(-1);
		}
		String rootdir = args[0];
		String pushmonSpec = args[1];
		String clientName = args[2];
		String configFilePath = args[3];
		PushmonHandler pushmonHandler = new PushmonHandler();
		String pushmonSpecJson = rootdir + "/" + pushmonSpec;
		String timeToSchedule = pushmonHandler.getAttributeValue(pushmonSpecJson, "timeToSchedule");
		String timezone = pushmonHandler.getAttributeValue(pushmonSpecJson, "timezone");
		String pushmonName = pushmonHandler.getAttributeValue(pushmonSpecJson, "pushmonName");

		String filePath = rootdir + "/" + configFilePath;
		String pushmonUrl = pushmonHandler.fetchPushmonUrl(rootdir + "/" + configFilePath);
		if (pushmonUrl != null) {
			logger.info("updating pushmon url");
			boolean isFoundURl = pushmonHandler.updatePushmonUrl(pushmonUrl, timeToSchedule, timezone);
		} else {
			logger.info("creating pushmon url");
			String newPushmonUrl = pushmonHandler.createPushmonURL(pushmonHandler.pushmonUserName,
					pushmonHandler.pushmonPassword, timeToSchedule, pushmonName, pushmonHandler.escalationEmailId,
					timezone);
			pushmonHandler.createPushmonJson(newPushmonUrl, filePath);
		}
		pushmonHandler.createJobConfig(rootdir, configFilePath, clientName);
	}

	private void createJobConfig(String rootdir, String filePath, String clientName) throws IOException {
		String jsonConfigPath = filePath;
		filePath = filePath.replace("properties", "");
		filePath = filePath.replace("//", "/");
		filePath = filePath.replace("json", "job");
		String jobFilePath = rootdir + "/" + filePath;
		FileWriter writer = new FileWriter(jobFilePath, false);
		writer.write("type=javaprocess\n");
		writer.write("java.class=com.rboomerang.exector.azkaban.RemoteJobExecutor " + clientName + "/" + filePath + "\n");
		writer.write("service.class=com.rboomerang.processor.impl.common.UrlTrigger\n");
		writer.write("file.properties=" + clientName + "/" + jsonConfigPath + "\n");
		writer.close();

	}

	private String fetchPushmonUrl(String configFilePath) {
		File pushmonConfig = new File(configFilePath);
		if (!pushmonConfig.exists()) {
			logger.info("pushmon config not found in: " + configFilePath);
			return null;
		}
		FileReader fileReader;
		JSONObject pushmonJson = null;
		try {
			fileReader = new FileReader(configFilePath);
			JSONParser parser = new JSONParser();
			Object jsonElement = null;
			jsonElement = parser.parse(fileReader);
			pushmonJson = (JSONObject) jsonElement;

		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		String urlValue = (String) pushmonJson.get("url");
		logger.info("available url = " + urlValue);
		return urlValue;
	}

	private String getAttributeValue(String pushmonSpecPath, String attribute) throws IOException, ParseException {
		fileReader = new FileReader(pushmonSpecPath);
		JSONParser parser = new JSONParser();
		Object jsonElement = null;
		jsonElement = parser.parse(fileReader);
		pushmonSpecJson = (JSONObject) jsonElement;
		String attribValue = (String) pushmonSpecJson.get(attribute);
		if (attribValue == null) {
			logger.info("attribute is missing in " + pushmonSpecPath + "attribute name: " + attribute);
			System.exit(-1);
		}
		return attribValue;
	}

	/**
	 * @throws InterruptedException
	 */
	private String createPushmonURL(String userName, String password, String timeToSchedule, String pushmonName,
			String email, String timezone) throws InterruptedException {
		WebDriver web = new FirefoxDriver();

		web.get("http://www.pushmon.com/");

		logger.info("Title : " + web.getTitle());

		web.findElement(By.xpath(".//*[@id='menu-item-755']/a")).click();

		logger.info("Title : " + web.getTitle());

		web.findElement(By.xpath(".//*[@id='j_username']")).sendKeys(userName);
		web.findElement(By.xpath(".//*[@id='j_password']")).sendKeys(password);
		web.findElement(By.xpath(".//*[@id='login']")).click();
		Thread.sleep(1000);

		logger.info("Title : " + web.getTitle());

		web.findElement(By.xpath(".//*[@id='container']/header/nav/ul/li[3]/a")).click();

		Thread.sleep(1000);
		logger.info("Title : " + web.getTitle());
		web.findElement(By.xpath(".//*[@id='scheduleString']")).sendKeys(timeToSchedule);
		Select dropdown = new Select(web.findElement(By.id("timeZone")));
		dropdown.selectByVisibleText(timezone);
		web.findElement(By.xpath(".//*[@id='email']")).clear();
		web.findElement(By.xpath(".//*[@id='email']")).sendKeys(email);
		// Select drop = new Select(web.findElement(By.id("timeZone")));
		// drop.selectByVisibleText("(GMT+05:30) Asia/Kolkata");
		web.findElement(By.xpath(".//*[@id='name']")).sendKeys(pushmonName);
		web.findElement(By.xpath(".//*[@id='createUrlSubmit']")).click();

		Thread.sleep(9000);

		String pushmonURL = web.findElement(By.xpath(".//*[@class='url selectText']/b")).getText();
		web.findElement(By.xpath(".//*[@id='container']/header/nav/ul/li[5]/a")).click();
		logger.info("purshmon URL created successfully: " + pushmonURL);
		// web.close();
		return pushmonURL;

	}

	private void createPushmonJson(String pushmonURL, String jsonFilePath) {
		JSONObject obj = new JSONObject();
		obj.put("url", pushmonURL);
		obj.put("abortRun", new Boolean(false));
		obj.put("notify", "error");
		obj.put("pagerdutyAlert", new Boolean(false));
		obj.put("endpoint", "");
		obj.put("helpLink", "");
		obj.put("pagerdutyKey", "");

		JSONArray list = new JSONArray();
		JSONObject obj1 = new JSONObject();
		obj1.put("type", "error");
		obj1.put("subject", "[Error] {{clientTag}} Error while processing for pushmon");
		obj1.put("body", "Hey Team, \n\n   Error while processing for pushmon: \n\n\n");
		list.add(obj1);
		JSONObject obj2 = new JSONObject();
		obj2.put("type", "success");
		obj2.put("subject", "[Success] {{clientTag}} Success while processing for pushmon");
		obj2.put("body", "Hey Team, \n\n   Success while processing for pushmon: \n\n\n");
		list.add(obj2);
		obj.put("message", list);

		try {
			FileWriter writer = new FileWriter(jsonFilePath);
			writer.write(obj.toJSONString());
			writer.flush();
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		}
	}

	private boolean updatePushmonUrl(String pushmonURL, String newTimeToSchedule, String timezone)
			throws InterruptedException {

		PushmonHandler pushmonHandler = new PushmonHandler();
		WebDriver driver = new FirefoxDriver();
		String url = "http://www.pushmon.com/";
		driver.get(url);
		driver.findElement(By.xpath(".//*[@id='menu-item-755']/a")).click();
		driver.findElement(By.xpath(".//*[@id='j_username']")).sendKeys(pushmonHandler.pushmonUserName);
		driver.findElement(By.xpath(".//*[@id='j_password']")).sendKeys(pushmonHandler.pushmonPassword);
		driver.findElement(By.xpath(".//*[@id='login']")).click();
		boolean isFoundURL = false;
		int rowNumToEdit = 0;
		Pattern pushmonUrlPattern = Pattern.compile(pushmonURL);
		Matcher matcher = null;

		List<WebElement> classes = driver.findElements(By.xpath(".//*[@id='dashboard']/table/tbody"));
		for (int i = 0; i < classes.size(); i++) {
			String allRows = classes.get(i).getText();
			matcher = pushmonUrlPattern.matcher(allRows);
			if (matcher.find()) {
				String[] rows = allRows.split("\n");
				for (int j = 0; j < rows.length; j++) {
					if (rows[j].contains(pushmonURL)) {
						isFoundURL = true;
						rowNumToEdit = j + 1;
						break;
					}
				}
			}
		}
		if (!isFoundURL) {
			logger.info(
					"url in config file is wrong, please delete the config file from /flow/properties/ and try again");
			driver.findElement(By.xpath(".//*[@id='container']/header/nav/ul/li[5]/a")).click();
			System.exit(-1);
			return isFoundURL;
		}
		StringBuilder builder = new StringBuilder();
		builder.append(".//*[@id='dashboard']/table/tbody/tr[");
		builder.append(rowNumToEdit);
		builder.append("]/td[1]/a[3]");
		String xpathFormed = builder.toString();
		driver.findElement(By.xpath(xpathFormed)).click();
		driver.findElement(By.xpath(".//*[@id='scheduleString']")).clear();
		driver.findElement(By.xpath(".//*[@id='scheduleString']")).sendKeys(newTimeToSchedule);
		Select dropdown = new Select(driver.findElement(By.id("timeZone")));
		dropdown.selectByVisibleText(timezone);
		driver.findElement(By.xpath(".//*[@id='createUrlSubmit']")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='container']/header/nav/ul/li[5]/a")).click();
		logger.info("updated pushon url successfully");
		return isFoundURL;
	}
}
