package com.rboomerang.validator.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JobValidator {

	private static boolean isValid = true;

	public JobValidator() {
		this.isValid = true;
	}

	public static void main(String[] args) throws Exception {
		String rootdir = args[0];
		String filePath = args[1];
		String client = args[2];
		Logger logger = LoggerFactory.getLogger(JobValidator.class);
		String etlSpceClient = getClientName(rootdir, filePath, client);
		boolean isService = false;
		String serviceClassPath = null;
		// gets all the files from given folder
		StringBuffer flowsFolderBuff = new StringBuffer();
		flowsFolderBuff.append(rootdir).append("/" + "/").append(filePath);
		File flowsFolder = new File(flowsFolderBuff.toString());
		File[] listOfFiles = flowsFolder.listFiles();
		if (listOfFiles == null || listOfFiles.length == 0) {
			return;
		}
		String fileName = null;
		for (int i = 0; i < listOfFiles.length; i++) {
			fileName = listOfFiles[i].getName();
			if (listOfFiles[i].isFile()) {
				// checks for only .job files
				if (!fileName.substring(fileName.lastIndexOf(".") + 1).equals("job")) {
					continue;
				}
				isService = false;
				StringBuffer fullFilePath = new StringBuffer();
				fullFilePath.append(flowsFolder).append("/").append(fileName);
				try (BufferedReader br = new BufferedReader(new FileReader(fullFilePath.toString()))) {
					String currLine;

					while ((currLine = br.readLine()) != null) {
						if (currLine.isEmpty()) {
							continue;
						}
						if (currLine.split("=").length < 2) {
							continue;
						}
						String[] inputFileName = currLine.split("=")[1].split("/");
						String expectedJsonFileName = fileName.split("\\.")[0] + ".json";
						StringBuffer jsonFilePathh = new StringBuffer();
						String jsonFilePath = jsonFilePathh.append(rootdir).append("/").append(filePath)
								.append("/properties/").append(expectedJsonFileName).toString();
						if (currLine.contains("java.class")) {

							if (!(currLine.contains("RemoteJobExecutor") || (currLine.contains("RemoteShutdown")))) {
								StringBuffer logMsg = new StringBuffer();
								logMsg.append("In ").append(client).append(" ").append(fileName)
										.append(", invalid job executor");
								logger.info(logMsg.toString());
								JobValidator.isValid = false;
							}
							if ((currLine.contains("RemoteJobExecutor"))) {
								isService = true;
							}
							if ((currLine.contains("RemoteShutdown"))) {
								break;
							}
							String[] javaClassName = currLine.split(" ");
							String expectedInput = etlSpceClient + "/" + filePath + "/" + fileName;
							if (!javaClassName[1].equals(expectedInput)) {
								StringBuffer logMsg = new StringBuffer();
								logMsg.append("In ").append(client).append(" ").append(fileName)
										.append(", invalid input for java.class");
								logger.info(logMsg.toString());
								JobValidator.isValid = false;
							}
						}
						if (currLine.contains("service.class")) {
							if (!isService) {
								StringBuffer logMsg = new StringBuffer();
								logMsg.append(
										"mismatch in type of RemoteJobExecutor and validator/processor .class used, in file ")
										.append(fileName);
								logger.info(logMsg.toString());
								JobValidator.isValid = false;
							}
							if (currLine != null)
								serviceClassPath = currLine;
						}
						if (currLine.contains("file.properties")) {
							Pattern p = Pattern.compile(".json$");
							Matcher m = p.matcher(currLine);
							if (!m.find()) {
								StringBuffer logMsg = new StringBuffer();
								logMsg.append("In  ").append(client).append(" ").append(fileName)
										.append(", not found .json file for file.properties");
								logger.info(logMsg.toString());
								JobValidator.isValid = false;
							}

							if (!(expectedJsonFileName).equals(inputFileName[inputFileName.length - 1])) {
								StringBuffer logMsg = new StringBuffer();
								logMsg.append("In  ").append(client).append(" ").append(fileName).append(", not found ")
										.append(expectedJsonFileName).append(" in file.properties");
								logger.info(logMsg.toString());
								JobValidator.isValid = false;
							}

							fileExistanceCheck(jsonFilePath);
							String[] list = serviceClassPath.split("\\.");
							StringBuffer jsonSchemaNameBuf = new StringBuffer();
							jsonSchemaNameBuf.append(list[list.length - 1]).append(".json");
							String jsonSchemaName = jsonSchemaNameBuf.toString();
							jsonSchemaValidation(jsonFilePath, jsonSchemaName);
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		if (!JobValidator.isValid) {
			System.exit(-1);
		}
	}

	private static void jsonSchemaValidation(String jsonFilePath, String jsonSchemaPath)
			throws FileNotFoundException, IOException {

		com.rboomerang.validator.impl.JsonSchemaValidator
				.validates_schema_in_classpath(readFile(jsonFilePath), jsonSchemaPath);
	}

	private static boolean fileExistanceCheck(String file) throws FileNotFoundException, IOException {
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			return true;
		}
	}

	// fetches client name from etl-spec
	protected static String getClientName(String rootdir, String filePath, String client) {
		StringBuffer sb = new StringBuffer();
		String eltSpecFile = sb.append(rootdir).append("/").append("/etl/etl-files/etl-spec.json").toString();
		try (BufferedReader br = new BufferedReader(new FileReader(eltSpecFile))) {
			String currLine;
			while ((currLine = br.readLine()) != null) {
				if (currLine.contains("client")) {
					String[] arr = currLine.split(":");
					return arr[1].split("\"")[1];
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String readFile(String filename) {
		String result = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();
			while (line != null) {
				sb.append(line);
				line = br.readLine();
			}
			result = sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
