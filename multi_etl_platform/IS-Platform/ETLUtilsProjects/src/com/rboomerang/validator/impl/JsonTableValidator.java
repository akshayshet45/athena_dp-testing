package com.rboomerang.validator.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JsonTableValidator {

	private static boolean isValid = true;
	private Logger logger = LoggerFactory.getLogger(JsonTableValidator.class);

	public JsonTableValidator() {
		super();
	}

	public static void main(String args[]) {
		JsonTableValidator jsonTableValidator = new JsonTableValidator();
		if (args.length != 3) {
			jsonTableValidator.logger.info(
					"mismatch in number of arguments passed to jsonTableValidator,expected 3, found " + args.length);
			System.exit(-1);
		}
		String rootdir = args[0];
		String tablePath = args[1];
		String client = args[2];
		String tablesPath = rootdir + "/" + tablePath;// "etl/etl-files/tables"
		jsonTableValidator.isValidBucketSortKey(tablesPath);

	}

	private boolean isValidBucketSortKey(String tablesPath) {
		boolean isBucketSortKeyNotFound = true;
		String bucketSortKey = null;
		String fileName = null;
		String currLine = null;
		BufferedReader br = null;
		String columnName = null;
		Pattern bucketKey = Pattern.compile("bucketSortKey");
		Pattern columnNameKey = Pattern.compile("name");
		String feedDate = "feed_date";
		String creationDate = "creation_date";
		boolean hasFeedDate = false;
		boolean hasCreationDate = false;
		File[] listOfFiles = getListOfFiles(tablesPath);
		if (listOfFiles == null || listOfFiles.length == 0) {
			return isBucketSortKeyNotFound;
		}
		for (int i = 0; i < listOfFiles.length; i++) {
			fileName = listOfFiles[i].getName();
			try {
				hasFeedDate = false;
				hasCreationDate = false;
				isBucketSortKeyNotFound = true;
				bucketSortKey = null;
				br = new BufferedReader(new FileReader(tablesPath + "/" + fileName));
				while ((currLine = br.readLine()) != null) {
					columnName = getValueForKey(currLine, columnNameKey);
					if(feedDate.equals(columnName)){
						hasFeedDate = true;
					}
					if(creationDate.equals(columnName)){
						hasCreationDate = true;
					}
					if (isBucketSortKeyNotFound)
						bucketSortKey = getValueForKey(currLine, bucketKey);
					if (bucketSortKey == null) {
						continue;
					}
					if ((bucketSortKey != null) && (!bucketSortKey.trim().equals(""))) {
						isBucketSortKeyNotFound = false;
					}
					
					if (columnName == null || (columnName.trim().equals(""))) {
						continue;
					}
					if (bucketSortKey.equals(columnName)) {
						isBucketSortKeyNotFound = true;
					}

				}
				if (!isBucketSortKeyNotFound) {
					logger.info("bucketSortKey: " + bucketSortKey + " is not a column name in file " + fileName);
					JsonTableValidator.isValid = false;
				}
				if(!hasCreationDate){
					logger.info("creation_date column is missing in file: "+fileName);
//					JsonTableValidator.isValid = false;
				}
				if(!hasFeedDate){
					logger.info("feed_date column is missing in file: "+fileName);
//					JsonTableValidator.isValid = false;
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				logger.info(e.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
				logger.info(e.getMessage());
			} finally {
				try {
					if (br != null)
						br.close();
				} catch (IOException ex) {
					ex.printStackTrace();
					logger.info(ex.getMessage());
				}
			}
		}
		if (!JsonTableValidator.isValid) {
			System.exit(-1);
		}
		return JsonTableValidator.isValid;
	}

	/**
	 * @param currLine
	 * @param pattern
	 */
	private String getValueForKey(String currLine, Pattern pattern) {
		String valueForKey = null;
		Matcher matcher = null;
		matcher = pattern.matcher(currLine);
		if (matcher.find()) {
			String[] words = currLine.split(":");
			if (words.length < 2) {
				return null;
			}
			if (words[1].split("\"").length < 2) {
				return null;
			}
			valueForKey = (words[1].split("\""))[1];
		}
		return valueForKey;
	}

	public File[] getListOfFiles(String folderPath) {
		File flowsFolder = new File(folderPath);
		File[] listOfFiles = flowsFolder.listFiles();
		if (listOfFiles == null || listOfFiles.length == 0) {
			return null;
		}
		return listOfFiles;
	}
}
