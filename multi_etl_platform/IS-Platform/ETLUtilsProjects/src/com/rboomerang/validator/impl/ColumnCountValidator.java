package com.rboomerang.validator.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ColumnCountValidator {

	private static boolean isValid = true;
	private Logger logger = LoggerFactory.getLogger(ColumnCountValidator.class);

	public static void main(String args[]) {
		ColumnCountValidator columnCountValidator = new ColumnCountValidator();
		if (args.length != 4) {
			columnCountValidator.logger.info(
					"mismatch in number of arguments passed to columnCountValidator,expected 4, found " + args.length);
			System.exit(-1);
		}
		String rootdir = args[0];
		String feedPath = args[1];
		String client = args[2];
		String headerPath = args[3];
		String feedsPath = rootdir + "/" + feedPath;// "etl/etl-files/tables"
		String headersPath = "";
		Map<String, Integer> feedNameColCountMap = columnCountValidator.getFeedNameColCountMap(headerPath);
		columnCountValidator.validateColumnCount(feedsPath, feedNameColCountMap);

	}

	/**
	 * @param columnCountValidator
	 * @param feedPath
	 */
	private void validateColumnCount(String feedPath, Map<String, Integer> feedNameColCountMap) {
		File[] listOfFeeds = getListOfFiles(feedPath);
		boolean isSftpFileNameFound = false;
		String sftpFeedFileName = null;
		boolean isColumnCountFound = false;
		int columnCount = 0;
		Pattern path = Pattern.compile("path");
		Pattern columnCountt = Pattern.compile("columnCount");
		String jsonFileName = null;
		BufferedReader br = null;
		String currLine = null;
		if (listOfFeeds == null || (listOfFeeds.length == 0)) {
			return;
		}
		for (int i = 0; i < listOfFeeds.length; i++) {
			jsonFileName = listOfFeeds[i].getName();
			isSftpFileNameFound = false;
			isColumnCountFound = false;
			try {
				br = new BufferedReader(new FileReader(feedPath + "/" + jsonFileName));
				while ((currLine = br.readLine()) != null) {
					if (!isSftpFileNameFound) {
						String completePath = getValueForKey(currLine, path);
						if (completePath != null) {
							String[] arr = completePath.split("/");
							String name = (arr[arr.length - 1]).split("\\$")[0];
							sftpFeedFileName = name.substring(0, name.length() - 1).split("\\.")[0];
							isSftpFileNameFound = true;
						}

					}
					if (!isColumnCountFound) {
						columnCount = getColumnCount(columnCountt, currLine);
						if (columnCount != -1) {
							isColumnCountFound = true;
						}
					}
				}
				try{
				if (feedNameColCountMap.get(sftpFeedFileName) != columnCount) {
					logger.info("In " + sftpFeedFileName + " column count is wrong, expected: "
							+ feedNameColCountMap.get(sftpFeedFileName) + " found: " + columnCount);
					ColumnCountValidator.isValid = false;
				}
				}catch(Exception e){
					logger.info("please configure header-files for feed file: "+sftpFeedFileName);
					logger.info(e.getMessage());
				}
			} catch (FileNotFoundException e) {
				logger.info(e.getMessage());
				e.printStackTrace();
			} catch (IOException e) {
				logger.info(e.getMessage());
				e.printStackTrace();
			} finally {
				try {
					if (br != null)
						br.close();
				} catch (IOException ex) {
					ex.printStackTrace();
					logger.info(ex.getMessage());
				}
			}

		}
		if (!ColumnCountValidator.isValid) {
//			System.exit(-1);
			logger.info("\n");logger.info("\n");
			logger.info("please ensure header-files are properly configured");
		}
	}

	/**
	 * @param columnCountt
	 * @param currLine
	 */
	private int getColumnCount(Pattern columnCountt, String currLine) {
		int columnCount;
		Matcher matcherr = columnCountt.matcher(currLine);
		if (matcherr.find()) {
			String[] words = currLine.split(":");
			if (words.length < 2) {
				return -1;
			}
			String[] arr = words[1].split(",");
			columnCount = Integer.parseInt(arr[0].trim());
			return columnCount;
		}
		return -1;
	}

	public File[] getListOfFiles(String folderPath) {
		File flowsFolder = new File(folderPath);
		File[] listOfFiles = flowsFolder.listFiles();
		if (listOfFiles == null || listOfFiles.length == 0) {
			return null;
		}
		return listOfFiles;
	}

	private String getValueForKey(String currLine, Pattern pattern) {
		String valueForKey = null;
		Matcher matcher = null;
		matcher = pattern.matcher(currLine);
		if (matcher.find()) {
			String[] words = currLine.split(":");
			if (words.length < 2) {
				return null;
			}
			if (words[1].split("\"").length < 2) {
				return null;
			}
			valueForKey = (words[1].split("\""))[1];
		}
		return valueForKey;
	}

	private Map<String, Integer> getFeedNameColCountMap(String headerPath) {
		Map<String, Integer> feedNameColCountMap = new HashMap<String, Integer>();
		BufferedReader br = null;
		String currLine = null;
		String headerFileName = null;
		Integer columnCount = null;
		File[] headerFiles = getListOfFiles(headerPath);
		if (headerFiles == null || (headerFiles.length == 0)) {
			return null;
		}
		for (int i = 0; i < headerFiles.length; i++) {
			headerFileName = headerFiles[i].getName();
			try {
				br = new BufferedReader(new FileReader(headerPath + "/" + headerFileName));
				while ((currLine = br.readLine()) != null) {
					if (currLine.isEmpty()) {
						continue;
					}
					String[] columns = currLine.split("\t");
					columnCount = columns.length;
				}
				String modifiedHeaderName = headerFileName.split("\\.")[0];
				feedNameColCountMap.put(modifiedHeaderName, columnCount);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				logger.info(e.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
				logger.info(e.getMessage());
			} finally {
				try {
					if (br != null)
						br.close();
				} catch (IOException ex) {
					ex.printStackTrace();
					logger.info(ex.getMessage());
				}
			}
		}
		return feedNameColCountMap;

	}
}
