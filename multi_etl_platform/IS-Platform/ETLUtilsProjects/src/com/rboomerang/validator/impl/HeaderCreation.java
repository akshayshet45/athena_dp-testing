package com.rboomerang.validator.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class HeaderCreation {

	public static String SFTPSource;
	public static String DESTINATION;
	public static String SFTP_HOST;
	public static String SFTP_PORT;
	public static String SFTP_USER;
	public static String SFTP_PASS;

	private static String clientSpecPath;
	private static Session session = null;
	private static Channel channel = null;
	private static ChannelSftp channelSftp = null;
	private static JSONParser parser = null;
	private static FileReader fileReader = null;

	private static final String ENDPOINTS = "endpoints";
	private static final String ENDPOINT_TYPE = "type";
	private static final String ENDPOINT_PROPERTIES = "properties";
	private static final String HOST = "host";
	private static final String USER = "user";
	private static final String PASS = "pass";
	private static final String FEED_METADATA = "feedMetadata";

	@SuppressWarnings("unchecked")
	private static void createHeaderFiles() throws Exception {

		JSch jsch = new JSch();
		session = jsch.getSession(SFTP_USER, SFTP_HOST, Integer.parseInt(SFTP_PORT));
		session.setPassword(SFTP_PASS);

		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);
		session.connect();
		channel = session.openChannel("sftp");
		channel.connect();
		channelSftp = (ChannelSftp) channel;

		channelSftp.cd(SFTPSource);
		Vector<ChannelSftp.LsEntry> directories = channelSftp.ls(".");

		for (ChannelSftp.LsEntry oListItem : directories) {
			if (oListItem.getAttrs().isDir()) {
				System.out.println(oListItem.getFilename());
				if (oListItem.getFilename().startsWith("."))
					continue;

				channelSftp.cd(oListItem.getFilename());
				Vector<ChannelSftp.LsEntry> files = channelSftp.ls(".");
				for (ChannelSftp.LsEntry fListItem : files) {
					if (!fListItem.getAttrs().isDir()) {
						String feedFile = fListItem.getFilename();
						System.out.println(feedFile);

						if (feedFile.endsWith(".txt.gz") || feedFile.endsWith(".zip")) {
							continue;
						}

						InputStream feedNow = channelSftp.get(fListItem.getFilename());
						BufferedReader reader = new BufferedReader(new InputStreamReader(feedNow));
						String headerLine = reader.readLine();
						System.out.println(headerLine);
						reader.close();
						feedNow.close();
						writeToFeedMetadata(headerLine, feedFile);
						break;
					}
				}
				channelSftp.cd("..");
			}
		}

		channelSftp.disconnect();
		channelSftp.exit();
		session.disconnect();

		return;
	}

	private static void writeToFeedMetadata(String headerLine, String feedFile) throws IOException {
		StringBuilder builder = new StringBuilder(feedFile);
		builder = builder.delete(feedFile.length() - 4, feedFile.length());
		if (Character.isDigit(builder.toString().charAt(builder.length() - 1))) {
			builder = builder.delete(builder.length() - 8, builder.length());
			if (builder.toString().endsWith("_"))
				builder = builder.delete(builder.length() - 1, builder.length());
		}
		
		feedFile = builder.toString();
		feedFile = feedFile + ".txt";
		feedFile = DESTINATION + feedFile;

		System.out.println(feedFile);

		FileOutputStream fop = null;
		File file;

		file = new File(feedFile);
		fop = new FileOutputStream(file);

		if (!file.exists()) {
			file.createNewFile();
		}

		byte[] contentInBytes = headerLine.getBytes();
		fop.write(contentInBytes);
		fop.flush();
		fop.close();

		return;
	}

	public static void main(String[] args) throws Exception {

		clientSpecPath = args[0];
		SFTPSource = args[1];
		fileReader = new FileReader(clientSpecPath);
		parser = new JSONParser();
		JSONObject jsonObj = (JSONObject) parser.parse(fileReader);

		JSONArray array = (JSONArray) jsonObj.get(HeaderCreation.ENDPOINTS);
		for (int i = 0; i < array.size(); i++) {
			JSONObject obj = (JSONObject) array.get(i);
			String type = (String) obj.get(HeaderCreation.ENDPOINT_TYPE);
			JSONObject properties = (JSONObject) obj.get(HeaderCreation.ENDPOINT_PROPERTIES);
			if (type.contains("sftp") || type.contains("SFTP")) {
				SFTP_HOST = (String) properties.get(HeaderCreation.HOST);
				SFTP_USER = (String) properties.get(HeaderCreation.USER);
				SFTP_PASS = (String) properties.get(HeaderCreation.PASS);
			}
		}

		DESTINATION = (String) jsonObj.get(HeaderCreation.FEED_METADATA);
		System.out.println(DESTINATION);
		SFTP_PORT = "22";

		createHeaderFiles();
	}

}
