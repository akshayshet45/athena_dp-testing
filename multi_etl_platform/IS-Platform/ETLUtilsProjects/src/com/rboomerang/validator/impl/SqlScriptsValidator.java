package com.rboomerang.validator.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SqlScriptsValidator {

	private static boolean isValid = true;
	private static boolean createFlag;
	private static boolean sortKeyFlag;
	private static boolean distKeyFlag;
	private static boolean semiColonFlag;
	private static Logger logger = LoggerFactory.getLogger(SqlScriptsValidator.class);

	public SqlScriptsValidator() {
		this.createFlag = false;
		this.sortKeyFlag = false;
		this.distKeyFlag = false;
		this.semiColonFlag = false;
		this.isValid = true;
	}

	public static void main(String args[]) throws Exception {
		String rootdir = args[0];
		String tablePath = args[1];
		String client = args[2];
		String schema_Path = args[3];
		String tablesPath = rootdir + "/" + tablePath;// "etl/etl-files/tables"
		String schemaPath = rootdir + "/" + schema_Path;
		System.out.println(tablesPath);
		ArrayList<String> tableNames = getETLTableNames(tablesPath);
		if (tableNames == null) {
			return;
		}
		boolean isTableDDLWritten;
		for (String tableName : tableNames) {
			isTableDDLWritten = doesTableExistsInScript(tableName, schemaPath);
			if (!isTableDDLWritten) {
				logger.info("DDL missing for " + tableName);
				SqlScriptsValidator.isValid = false;
			}
			redshiftGuidelinesValidation(tableName, schemaPath);
			if (!SqlScriptsValidator.isValid) {
				System.exit(-1);
			}
		}
	}

	private static void redshiftGuidelinesValidation(String tableName, String schemaPath) throws Exception {
		String fileName;
		String currLine;

		File schemaFolder = new File(schemaPath);
		File[] sqlFiles = schemaFolder.listFiles();
		if (sqlFiles == null) {
			logger.info("no files found in " + schemaPath);
			return;
		}
		for (int i = 0; i < sqlFiles.length; i++) {
			fileName = sqlFiles[i].getName();
			try (BufferedReader br = new BufferedReader(new FileReader(schemaPath + "/" + fileName))) {
				while ((currLine = br.readLine()) != null) {
					validateRedshifGuidelines(currLine, fileName);
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				logger.info(e.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
				logger.info(e.getMessage());
			}
		}

	}

	private static boolean doesTableExistsInScript(String tableName, String schemaPath) {
		String fileName;
		String currLine;

		File schemaFolder = new File(schemaPath);
		File[] sqlFiles = schemaFolder.listFiles();
		if (sqlFiles == null) {
			logger.info("no files found in " + schemaPath);
			return true;
		}
		for (int i = 0; i < sqlFiles.length; i++) {
			fileName = sqlFiles[i].getName();
			try (BufferedReader br = new BufferedReader(new FileReader(schemaPath + "/" + fileName))) {
				while ((currLine = br.readLine()) != null) {
					if (currLine.contains(tableName)) {
						return true;
					}
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				logger.info(e.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
				logger.info(e.getMessage());
			}
		}
		return false;
	}

	private static void validateRedshifGuidelines(String line, String fileName) throws Exception {
		if (line.contains("create")) {
			String[] words = line.split(" ");
			for (String word : words) {
				if (word.equals("create")) {
					if (SqlScriptsValidator.createFlag == false) {
						SqlScriptsValidator.createFlag = true;
					} else {
						logger.info("invalid DDL statement found in " + fileName + ", missing semicolon");
						SqlScriptsValidator.isValid = false;
					}
				}
			}
		}
		if (line.contains("sortkey")) {
			if (SqlScriptsValidator.sortKeyFlag == false) {
				SqlScriptsValidator.sortKeyFlag = true;
			} else {
				logger.info("invalid DDL statement found in " + fileName + ", Error in sortkey");
				SqlScriptsValidator.isValid = false;
			}
		}
		if (line.contains("distkey")) {
			if (SqlScriptsValidator.distKeyFlag == false) {
				SqlScriptsValidator.distKeyFlag = true;
			} else {
				logger.info("invalid DDL statement found in " + fileName + ", Error in distKey");
				SqlScriptsValidator.isValid = false;
			}
		}
		if (line.contains(";")) {
			if ((SqlScriptsValidator.createFlag == true) && (SqlScriptsValidator.sortKeyFlag == true)
					&& (SqlScriptsValidator.distKeyFlag == true)) {
				SqlScriptsValidator.createFlag = false;
				SqlScriptsValidator.sortKeyFlag = false;
				SqlScriptsValidator.distKeyFlag = false;
				SqlScriptsValidator.semiColonFlag = false;
			} else {
				if (SqlScriptsValidator.createFlag == false) {
					logger.info("create is missing in file " + fileName);
					SqlScriptsValidator.isValid = false;
				}
				if (SqlScriptsValidator.sortKeyFlag == false) {
					logger.info("sortkey is missing in file " + fileName);
					SqlScriptsValidator.isValid = false;
				}
				if (SqlScriptsValidator.distKeyFlag == false) {
					logger.info("distkey is missing in file " + fileName);
					SqlScriptsValidator.isValid = false;
				}
			}
		}
	}

	private static ArrayList<String> getETLTableNames(String path) {
		File etlFolder = new File(path);
		File[] jsonTableFiles = etlFolder.listFiles();
		if (jsonTableFiles == null || jsonTableFiles.length == 0) {
			return null;
		}
		String currLine;
		String tableName;
		String schemaName;
		String fileName = null;
		ArrayList<String> tables = new ArrayList<String>();
		for (int i = 0; i < jsonTableFiles.length; i++) {
			if(!jsonTableFiles[i].isFile()){
				continue;
			}
			fileName = jsonTableFiles[i].getName();
			if (!fileName.substring(fileName.lastIndexOf(".") + 1).equals("json")) {
				continue;
			}
			try (BufferedReader br = new BufferedReader(new FileReader(path + "/" + fileName))) {

				while ((currLine = br.readLine()) != null) {
					if (currLine.contains("\"table\"")) {
						if (currLine.split(":").length < 3) {
							continue;
						}
						tableName = currLine.split(":")[1];
						if (!tableName.contains("[]")) {
							schemaName = tableName.split("\"")[1];
							if (!schemaName.contains("base")) {
								tables.add(schemaName);
							}
						}
					}

				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				logger.info(e.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
				logger.info(e.getMessage());
			}
		}
		return tables;
	}
}
