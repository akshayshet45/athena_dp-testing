package com.rboomerang.validator.impl;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientSpecValidator {
	private static Logger logger = LoggerFactory.getLogger(ClientSpecValidator.class);
	private FileReader fileReader = null;
	private JSONObject etlSpecJson = null;
	private static boolean isValid = true;

	public static void main(String args[]) throws IOException, ParseException {
		 if (args.length != 1) {
		 logger.info("mismatch in number of arguments passed to ClientSpecValidator,expected 1, found " + args.length);
		 System.exit(-1);
		 }
		 String rootdir = args[0];
		ClientSpecValidator clientSpecValidator = new ClientSpecValidator();
		List<String> etlPathList = clientSpecValidator.getEtlPath(rootdir + "/client-spec.json");
		clientSpecValidator.validateEtlPaths(etlPathList, rootdir);
		System.out.println();
		if (!clientSpecValidator.isValid) {
			logger.info("client spec validation failed for " + rootdir);
			System.exit(-1);
		}
	}

	private boolean validateEtlPaths(List<String> etlPathList, String rootDir) {
		ClientSpecValidator clientSpecValidator = new ClientSpecValidator();
		HashMap<String, String> clientNames = new HashMap<String, String>();
		for (String path : etlPathList) {
			String clientName = clientSpecValidator.getClientName(path, rootDir);
			clientNames.put(path, clientName);
		}
		logger.info("fetched client names successufully");
		boolean hasDuplicateClient = clientSpecValidator.checkDuplicateData(clientNames);
		if (hasDuplicateClient) {
			return true;
		}
		return false;
	}

	private boolean checkDuplicateData(HashMap<String, String> clientNames) {
		List<String> clientNameList = new ArrayList<String>(clientNames.values());
		int actualSize = clientNameList.size();
		Set<String> clientNameSet = new HashSet<String>(clientNames.values());
		int expectedSize = clientNameSet.size();
		if (actualSize != expectedSize) {
			Collections.sort(clientNameList);
			for (int i = 0; i < clientNameList.size() - 1; i++) {
				if (clientNameList.get(i).equals(clientNameList.get(i + 1))) {
					logger.info("found multiple ETL with client name " + clientNameList.get(i));
				}
			}
			ClientSpecValidator clientSpecValidator = new ClientSpecValidator();
			clientSpecValidator.isValid = false;
			return true;
		}
		return false;
	}

	private String getClientName(String path, String rootDir) {
		String attributeName = "client";
		String fileName = "etl-spec.json";
		FileReader fileReader;
		try {
			fileReader = new FileReader(rootDir + "/" + path + "/" + fileName);
			JSONParser parser = new JSONParser();
			Object jsonElement = null;
			jsonElement = parser.parse(fileReader);
			etlSpecJson = (JSONObject) jsonElement;
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		String clientName = (String) etlSpecJson.get(attributeName);
		return clientName;

	}

	private List<String> getEtlPath(String filePath) throws IOException, ParseException {
		List<String> etlPathList = new ArrayList<String>();
		String attributeName = "etl_path";
		String etlListAttribute="etl_list";
		fileReader = new FileReader(filePath);
		JSONParser parser = new JSONParser();
		Object jsonElement = null;
		if (filePath == null || filePath.trim().isEmpty()) {
			return etlPathList;
		}
		jsonElement = parser.parse(fileReader);
		etlSpecJson = (JSONObject) jsonElement;
		JSONArray etlPaths = (JSONArray) etlSpecJson.get(etlListAttribute);
		if (etlPaths == null) {
			logger.info("etl_list is not available in " + filePath);
			return etlPathList;
		}
		for (int i = 0; i < etlPaths.size(); i++) {
			JSONObject etlPathObj = (JSONObject) etlPaths.get(i);
			String etlPath = (String) etlPathObj.get(attributeName);
			etlPathList.add(etlPath);
		}
		logger.info("fetched etl paths successfully from client-spec");
		return etlPathList;

	}
}
