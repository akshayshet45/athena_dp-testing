package com.rboomerang.validator.impl;

import static org.hamcrest.MatcherAssert.assertThat;

import java.io.BufferedReader;
import java.io.FileReader;

import org.junit.Test;
//for more useage details https://github.com/jayway/rest-assured/wiki/Usage
public class JsonSchemaValidator {
	@Test
	public static void validates_schema_in_classpath(String jsonFileWithData, String JsonSchema) {
		System.out.println("validation for  JsonSchema  "+JsonSchema);
		assertThat(jsonFileWithData,
				com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath(JsonSchema));
	}

	public static String readFile(String filename) {
		String result = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();
			while (line != null) {
				sb.append(line);
				line = br.readLine();
			}
			result = sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
