package com.rboomerang.validator.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NotificationMessageValidator {

	private static boolean isValid=true;
	public NotificationMessageValidator() {
		this.isValid = true;
	}
	
	public static void main(String args[]) {
		String rootdir = args[0];
		String filePath = args[1];
		String client = args[2];
		Logger logger = LoggerFactory.getLogger(NotificationMessageValidator.class);
		File flowsFolder = new File(rootdir + "/" + "/" + filePath);
		File[] listOfFiles = flowsFolder.listFiles();
		String fileName = null;
		String notificationType = null;
		BufferedReader br = null;
		boolean isDefinedNotificationType = false;
		for (int i = 0; i < listOfFiles.length; i++) {
			fileName = listOfFiles[i].getName();
			if (listOfFiles[i].isFile()) {
				// checks for only .json files
				if (!fileName.substring(fileName.lastIndexOf(".") + 1).equals("json")) {
					continue;
				}
				isDefinedNotificationType = true;
				notificationType = null;
				try {
					String currLine;
					br = new BufferedReader(new FileReader(flowsFolder + "/" + fileName));
					while ((currLine = br.readLine()) != null) {
						if (currLine.contains("notify")) {
							if (currLine.split(":")[0].split("\"")[1].equals("notify")) {
								notificationType = currLine.split(":")[1].split("\"")[1];
								isDefinedNotificationType = false;
							}
						}
						if (notificationType != null) {
							if (currLine.contains(notificationType)) {
								if (currLine.split(":")[0].split("\"")[1].equals("type")) {
									if (currLine.split(":")[1].split("\"")[1].equals(notificationType)) {
										isDefinedNotificationType = true;
									}
								}
							}
						}
					}
					if (isDefinedNotificationType == false) {
						logger.info(
								"notify type: " + notificationType + " is not defined in " + client + "/" + fileName);
						NotificationMessageValidator.isValid=false;
					}
				} catch (

				IOException e)

				{
					e.printStackTrace();
				} finally

				{
					try {
						if (br != null)
							br.close();
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}
			}
		}
		if(!NotificationMessageValidator.isValid){
			System.exit(-1);
		}
	}
}
