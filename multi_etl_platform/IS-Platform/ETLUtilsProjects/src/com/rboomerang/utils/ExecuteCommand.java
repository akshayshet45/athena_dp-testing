package com.rboomerang.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ExecuteCommand {
	
	public static String executeCommand(String command) throws Exception {

		StringBuffer output = new StringBuffer();
		Process p;

		p = Runtime.getRuntime().exec(command);
		p.waitFor();
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

		String line = "";			
		while ((line = reader.readLine())!= null) {
			output.append(line);
		}

		return output.toString();
	}
}
