#!/bin/bash
set -e

FILENAME=$1
PASS=$2
HOST=$3
DB=$4
UNAME=$5
	
echo "check"
if [ ! -f $FILENAME ]; then
	echo "File not found\n"
	exit -1
fi
	
PGPASSWORD=$PASS /usr/bin/psql -h $HOST -d $DB -U $UNAME -p 5439 -a -w -f $FILENAME

exit 0