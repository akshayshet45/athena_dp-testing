#!/bin/bash

set -e

echooo "_____________________________"
echo $1
date +"%Y%m%d"
echo " "
echo "Common Sense is like a deodorant. The people who need it most, never use it :("
echo "_____________________________"

exit