package com.rboomerang.test.processor.common;


import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.ScriptExecutor;


public class ScriptExecutorTest {
	private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/ScriptExecutorSuccess.json";
	private static final String ERROR_SPEC = "src/test/resources/processor/properties/ScriptExecutorError.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20151024";

	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}

	@Test
	public void testScriptExecutorSuccess() throws Exception {
		ScriptExecutor processor = new ScriptExecutor(CLIENT_SPEC, SUCCESS_SPEC, ScriptExecutorTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_SUCCESS, status.getStatusCode());

		processor.close();
	}

	@Test
	public void testScriptExecutorError() throws Exception {
		ScriptExecutor processor = new ScriptExecutor(CLIENT_SPEC, ERROR_SPEC, ScriptExecutorTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_ERROR, status.getStatusCode());

		processor.close();
	}
}
