package com.rboomerang.test.processor.common;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.FileMerger;

public class FileMergerTest {
	private static final String CSV_MERGE_SUCCESS_SPEC = "src/test/resources/processor/properties/CsvMergeSuccess.json";
	private static final String CSV_MERGE_FAILURE_SPEC = "src/test/resources/processor/properties/CsvMergeFailure.json";
	private static final String XLSX_MERGE_SUCCESS_SPEC = "src/test/resources/processor/properties/XlsxMergeSuccess.json";
	private static final String XLSX_MERGE_FAILURE_SPEC = "src/test/resources/processor/properties/XlsxMergeFailure.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160525";

	@Test
	public void testCsvFileMergerSuccess() throws Exception {
		FileMerger processor = new FileMerger(CLIENT_SPEC, CSV_MERGE_SUCCESS_SPEC, FileMergerTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testCsvFileMergerSuccess done");
		processor.close();
	}

	@Test
	public void testCsvFileMergerFailure() throws Exception {
		FileMerger processor = new FileMerger(CLIENT_SPEC, CSV_MERGE_FAILURE_SPEC, FileMergerTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_ERROR);
		System.out.println("testCsvFileMergerFailure done");
		processor.close();
	}

	@Test
	public void testXlsxFileMergerSuccess() throws Exception {
		FileMerger processor = new FileMerger(CLIENT_SPEC, XLSX_MERGE_SUCCESS_SPEC, FileMergerTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testXlsxFileMergerSuccess done");
		processor.close();
	}

	@Test
	public void testXlsxFileMergerFailure() throws Exception {
		FileMerger processor = new FileMerger(CLIENT_SPEC, XLSX_MERGE_FAILURE_SPEC, FileMergerTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_ERROR);
		System.out.println("testXlsxFileMergerFailure done");
		processor.close();
	}
}
