package com.rboomerang.test.processor.common;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.RedshiftDump;

public class RedshiftDumpTest {
	private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/RedshiftDumpSuccess.json";
	private static final String SUCCESS_SPEC2 = "src/test/resources/processor/properties/RedshiftDumpSuccess2.json";
	private static final String SUCCESS_SPEC3 = "src/test/resources/processor/properties/RedshiftDumpSuccess3.json";
	private static final String SUCCESS_SPEC4 = "src/test/resources/processor/properties/RedshiftDumpSuccess4.json";
	private static final String SUCCESS_SPEC5 = "src/test/resources/processor/properties/RedshiftDumpSuccess5.json";
	private static final String SUCCESS_SPEC6 = "src/test/resources/processor/properties/RedshiftDumpSuccess6.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160304";
	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}

	@Test
	public void testRedshiftDumpSuccessAddQuotes() throws Exception {
		RedshiftDump processor = new RedshiftDump(CLIENT_SPEC, SUCCESS_SPEC5,RedshiftDumpTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testRedshiftDumpSuccessAddQuotes done");
		processor.close();
	}
	
	@Test
	public void testRedshiftDumpSuccessEscape() throws Exception {
		RedshiftDump processor = new RedshiftDump(CLIENT_SPEC, SUCCESS_SPEC6,RedshiftDumpTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testRedshiftDumpSuccessEscape done");
		processor.close();
	}
	
	@Test
	public void testRedshiftDumpSuccessParallelOff() throws Exception {
		RedshiftDump processor = new RedshiftDump(CLIENT_SPEC, SUCCESS_SPEC4,RedshiftDumpTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testParallelOffSuccess done");
		processor.close();
	}
	
	
	@Test
	public void testRedshiftDumpSuccess() throws Exception {
		RedshiftDump processor = new RedshiftDump(CLIENT_SPEC, SUCCESS_SPEC,RedshiftDumpTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testsqlTableDumpToCsvSuccess done");
		processor.close();
	}
	@Test
	public void testRedshiftDumpSuccess2() throws Exception {
		RedshiftDump processor = new RedshiftDump(CLIENT_SPEC, SUCCESS_SPEC2,RedshiftDumpTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testsqlTableDumpToCsvSuccess done");
		processor.close();
	}
	
	@Test
	public void testRedshiftDumpSuccess3() throws Exception {
		RedshiftDump processor = new RedshiftDump(CLIENT_SPEC, SUCCESS_SPEC3,RedshiftDumpTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testsqlTableDumpToCsvSuccess done");
		processor.close();
	}
}
