package com.rboomerang.test.service.common;


import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.DmlSqlExecution;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class DmlSqlTest {
    private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/TestCases/DmlSqlExecutor/DmlSqlExeSuccessful.json";
    private static final String ERROR_SPEC = "src/test/resources/processor/properties/TestCases/DmlSqlExecutor/DmlSqlExecutionFail.json";
    private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
    private static final String RUNTIMEDATE = "20151024";

    @BeforeMethod
    protected void setUp() throws Exception {
    }

    @AfterMethod
    protected void tearDown() throws Exception {
    }

    @Test
    public void testDmlSqlSuccess() throws Exception {
        DmlSqlExecution processor = new DmlSqlExecution(CLIENT_SPEC, SUCCESS_SPEC, DmlSqlTest.RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(Status.STATUS_SUCCESS, status.getStatusCode());

        processor.close();
    }

    @Test
    public void testDmlSqlError() throws Exception {
        DmlSqlExecution processor = new DmlSqlExecution(CLIENT_SPEC, ERROR_SPEC, DmlSqlTest.RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(Status.STATUS_ERROR, status.getStatusCode());

        processor.close();
    }
}
