package com.rboomerang.test.processor.common;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;

import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.DmlSqlExecution;
import com.rboomerang.processor.impl.common.RecordCountReport;

public class RecordCountReportTest {
	private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/RecordCountReportSuccess.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160323";
	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}

	@Test
	public void testCountReportTestSuccess() throws Exception {
		RecordCountReport processor = new RecordCountReport(CLIENT_SPEC, SUCCESS_SPEC,RecordCountReportTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testRecordCountReportTestSuccess done");
		processor.close();
	
	}
}
