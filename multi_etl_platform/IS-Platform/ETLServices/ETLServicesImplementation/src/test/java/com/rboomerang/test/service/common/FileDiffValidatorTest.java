package com.rboomerang.test.service.common;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import com.rboomerang.common.framework.Status;
import com.rboomerang.validator.impl.common.FileDiffValidator;

public class FileDiffValidatorTest {
	private static final String SUCCESS_SPEC = "src/test/resources/validator/properties/TestCases/FileDiffValidator/2FilesOnDiffLocation.json";
	private static final String SUCCESS_SPEC2 = "src/test/resources/validator/properties/TestCases/FileDiffValidator/2FilesOnDiffLocationDiffName.json";
	private static final String ERROR_SPEC = "src/test/resources/validator/properties/TestCases/FileDiffValidator/DiffFileDiffContent.json";
	private static final String ERROR_SPEC2 = "src/test/resources/validator/properties/TestCases/FileDiffValidator/DiffFileDiffExtention.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160118";
	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}

	@Test
	public void TwoFilesOnDiffLocation() throws Exception {
		FileDiffValidator validator = new FileDiffValidator(CLIENT_SPEC, SUCCESS_SPEC,FileDiffValidatorTest.RUNTIMEDATE);
		Status status = validator.service(validator.getClientSpec(), validator.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testFileDiffValidatorSuccess done");
		validator.close();
	}
	
	@Test
	public void TwoFilesOnDiffLocationDiffName() throws Exception {
		FileDiffValidator validator = new FileDiffValidator(CLIENT_SPEC, SUCCESS_SPEC2,FileDiffValidatorTest.RUNTIMEDATE);
		Status status = validator.service(validator.getClientSpec(), validator.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testFileDiffValidatorSuccess done");
		validator.close();
	}
	
	@Test
	public void DiffFileDiffSource() throws Exception {
		FileDiffValidator validator = new FileDiffValidator(CLIENT_SPEC, ERROR_SPEC,FileDiffValidatorTest.RUNTIMEDATE);
		Status status = validator.service(validator.getClientSpec(), validator.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_ERROR);
		System.out.println("testFileDiffValidatorError done");
		validator.close();
	}
	
	@Test
	public void DiffFileDiffExtention() throws Exception {
		FileDiffValidator validator = new FileDiffValidator(CLIENT_SPEC, ERROR_SPEC2,FileDiffValidatorTest.RUNTIMEDATE);
		Status status = validator.service(validator.getClientSpec(), validator.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_ERROR);
		System.out.println("testFileDiffValidatorError done");
		validator.close();
	}
	
}
