package com.rboomerang.test.validator.common;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import com.rboomerang.common.framework.Status;
import com.rboomerang.validator.impl.common.FileDiffValidator;

public class FileDiffValidatorTest {
	private static final String SUCCESS_SPEC = "src/test/resources/validator/properties/FileDiffValidatorSuccess.json";
	private static final String ERROR_SPEC = "src/test/resources/validator/properties/FileDiffValidatorFail.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160118";
	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}

	@Test
	public void testFileDiffValidatorSuccess() throws Exception {
		FileDiffValidator validator = new FileDiffValidator(CLIENT_SPEC, SUCCESS_SPEC,FileDiffValidatorTest.RUNTIMEDATE);
		Status status = validator.service(validator.getClientSpec(), validator.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testFileDiffValidatorSuccess done");
		validator.close();
	}
	
	
	@Test
	public void testFileDiffValidatorError() throws Exception {
		FileDiffValidator validator = new FileDiffValidator(CLIENT_SPEC, ERROR_SPEC,FileDiffValidatorTest.RUNTIMEDATE);
		Status status = validator.service(validator.getClientSpec(), validator.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_ERROR);
		System.out.println("testFileDiffValidatorError done");
		validator.close();
	}
}
