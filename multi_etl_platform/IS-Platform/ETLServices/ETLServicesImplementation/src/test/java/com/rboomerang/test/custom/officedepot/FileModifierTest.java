package com.rboomerang.test.custom.officedepot;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.TestNG;

import com.rboomerang.common.framework.Status;
import com.rboomerang.custom.officedepot.FileModifier;



public class FileModifierTest extends TestNG {
	private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/FileModifierTest.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160211";
	
	@Test
	public void testFileModifierTestSuccess() throws Exception {
		FileModifier processor = new FileModifier(CLIENT_SPEC, SUCCESS_SPEC,FileModifierTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testFileModifierTestSuccess done");
		processor.close();
	}
	
}
