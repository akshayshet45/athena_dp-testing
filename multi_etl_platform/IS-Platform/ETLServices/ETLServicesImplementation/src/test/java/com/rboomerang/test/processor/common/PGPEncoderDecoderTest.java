package com.rboomerang.test.processor.common;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.PGPEncoderDecoder;

public class PGPEncoderDecoderTest{
	private static final String FAIL_SPEC = "src/test/resources/processor/properties/PGPEncoderDecoderFail.json";
	private static final String SUCCESS_SPEC1 = "src/test/resources/processor/properties/PGPEncoderDecoderSuccessEncode.json";
	private static final String SUCCESS_SPEC2 = "src/test/resources/processor/properties/PGPEncoderDecoderSuccessDecode.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160125";
	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}
	@Test
	public void testPGPDecoderSuccess() throws Exception {
		PGPEncoderDecoder processor = new PGPEncoderDecoder(CLIENT_SPEC, SUCCESS_SPEC2,PGPEncoderDecoderTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testPGPEncoderDecoderSuccess done");
		processor.close();
	}
	@Test
	public void testPGPEncoderSuccess() throws Exception {
		PGPEncoderDecoder processor = new PGPEncoderDecoder(CLIENT_SPEC, SUCCESS_SPEC1,PGPEncoderDecoderTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testPGPEncoderDecoderSuccess done");
		processor.close();
	}
	@Test
	public void testPGPEncoderDecoderFail() throws Exception {
		PGPEncoderDecoder processor = new PGPEncoderDecoder(CLIENT_SPEC, FAIL_SPEC,PGPEncoderDecoderTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_ERROR);
		System.out.println("testPGPEncoderDecoderFail done");
		processor.close();
	}
}
