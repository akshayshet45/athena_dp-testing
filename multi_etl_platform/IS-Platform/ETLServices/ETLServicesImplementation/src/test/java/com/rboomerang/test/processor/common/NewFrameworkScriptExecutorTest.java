package com.rboomerang.test.processor.common;

import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.NewFrameworkScriptExecutor;

public class NewFrameworkScriptExecutorTest {
	private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/NewFrameworkScriptExecutorSuccess.json";
	private static final String ERROR_SPEC = "src/test/resources/processor/properties/NewFrameworkScriptExecutorError.json";
	private static final String CLIENT_SPEC = "src/test/resources/NewFrameworkClient-spec.json";
	private static final String RUNTIMEDATE = "20151024";

	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}

	@Test
	public void testScriptExecutorSuccess() throws Exception {
		NewFrameworkScriptExecutor processor = new NewFrameworkScriptExecutor(
				CLIENT_SPEC, SUCCESS_SPEC,
				NewFrameworkScriptExecutorTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(),
				processor.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_SUCCESS, status.getStatusCode());

		processor.close();
	}

	@Test
	public void testScriptExecutorError() throws Exception {
		NewFrameworkScriptExecutor processor = new NewFrameworkScriptExecutor(
				CLIENT_SPEC, ERROR_SPEC,
				NewFrameworkScriptExecutorTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(),
				processor.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_ERROR, status.getStatusCode());

		processor.close();
	}
}
