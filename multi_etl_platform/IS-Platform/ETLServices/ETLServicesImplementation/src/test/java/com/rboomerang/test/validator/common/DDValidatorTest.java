package com.rboomerang.test.validator.common;

import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.rboomerang.common.framework.Status;
import com.rboomerang.validator.impl.common.DDValidator;

public class DDValidatorTest {
	private static final String SUCCESS_SPEC = "src/test/resources/validator/properties/ddValidatorSuccess.json";
	private static final String ERROR_SPEC = "src/test/resources/validator/properties/ddValidatorError.json";
	private static final String ERROR_SPEC_COUNT = "src/test/resources/validator/properties/ddValidatorErrorCount.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160118";
	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}
	
	@Test
	public void ddValidatorSuccess() throws Exception {
		DDValidator validator = new DDValidator(CLIENT_SPEC, SUCCESS_SPEC,DDValidatorTest.RUNTIMEDATE);
		Status status = validator.service(validator.getClientSpec(), validator.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_SUCCESS, status.getStatusCode());
		System.out.println("ddValidatorSuccess done");
		validator.close();
	}
	
	@Test
	public void ddValidatorError() throws Exception {
		DDValidator validator = new DDValidator(CLIENT_SPEC, ERROR_SPEC,DDValidatorTest.RUNTIMEDATE);
		Status status = validator.service(validator.getClientSpec(), validator.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_ERROR, status.getStatusCode());
		System.out.println("ddValidatorError done");
		validator.close();
	}
	
	@Test
	public void ddValidatorErrorCount() throws Exception {
		DDValidator validator = new DDValidator(CLIENT_SPEC, ERROR_SPEC_COUNT,DDValidatorTest.RUNTIMEDATE);
		Status status = validator.service(validator.getClientSpec(), validator.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_ERROR, status.getStatusCode());
		System.out.println("ddValidatorError done");
		validator.close();
	}

	// public void testFileDiffValidatorError() throws Exception {
	// FileDiffValidator validator = new FileDiffValidator(CLIENT_SPEC,
	// ERROR_SPEC);
	// Status status = validator.validate(validator.getClientSpec(),
	// validator.getValidatorSpec());
	// validator.close();
	// }


}
