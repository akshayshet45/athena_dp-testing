package com.rboomerang.test.service.common;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.FeedCopy;
import com.rboomerang.processor.impl.common.FileCopy;

public class FeedCopyTestCase {
	private static final String SUCCESS_SPEC_SFTP_S3 = "src/test/resources/processor/properties/TestCases/FileCopy/FilePresentAfterCopy.json";
	private static final String FAILURE_SPEC_AlreadyPresent= "src/test/resources/processor/properties/TestCases/FileCopy/FileAlreadyPresent.json";
	private static final String FAILURE_SPEC_WrongPath= "src/test/resources/processor/properties/TestCases/FileCopy/WrongPath.json";
	private static final String FAILURE_SPEC_Notcopied= "src/test/resources/processor/properties/TestCases/FileCopy/FileNotcopiedToDest.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160104";
	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}

	@Test
	public void testFileCopySuccess_sftp_s3() throws Exception {
		FileCopy processor = new FileCopy(CLIENT_SPEC, SUCCESS_SPEC_SFTP_S3,RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_SUCCESS, status.getStatusCode());
		System.out.println("S3 to SFTP Test Case is successful");
		processor.close();
	}

	@Test
	public void testFileCopyFailure() throws Exception {
		FileCopy processor = new FileCopy(CLIENT_SPEC, FAILURE_SPEC_AlreadyPresent,RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_ERROR, status.getStatusCode());
		System.out.println("FAILURE Test cases successful!!");
		processor.close();
	}
	
	@Test
	public void testFileCopyFailure2() throws Exception {
		FileCopy processor = new FileCopy(CLIENT_SPEC, FAILURE_SPEC_WrongPath,RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_ERROR, status.getStatusCode());
		System.out.println("FAILURE Test cases successful!!");
		processor.close();
	}
	
	@Test
	public void testFileCopyFailure3() throws Exception {
		FileCopy processor = new FileCopy(CLIENT_SPEC, FAILURE_SPEC_Notcopied,"20160318");
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_ERROR, status.getStatusCode());
		System.out.println("FAILURE Test cases successful!!");
		processor.close();
	}
	
}