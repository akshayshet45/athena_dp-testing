package com.rboomerang.test.custom.officedepot;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.TestNG;

import com.rboomerang.common.framework.Status;
import com.rboomerang.custom.officedepot.XmlToCsvCustom;



public class XmlToCsvCustomTestCases extends TestNG {
	private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/XmlToCsvCustom.json";
	private static final String FAILURE_SPEC= "src/test/resources/processor/properties/XmlToCsvCustomFailure.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160112";
	
	@Test
	public void testXmlToCsvCustomTestSuccess() throws Exception {
		XmlToCsvCustom processor = new XmlToCsvCustom(CLIENT_SPEC, SUCCESS_SPEC,RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testXmlToCsvSuccess done");
		processor.close();
	}
	@Test
	public void testRScriptExecutorTestFailure() throws Exception {
		XmlToCsvCustom processor = new XmlToCsvCustom(CLIENT_SPEC, FAILURE_SPEC,RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_ERROR);
		System.out.println("testXmlToCsvFailure done");
		processor.close();
	}
}