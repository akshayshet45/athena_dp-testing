package com.rboomerang.test.processor.common;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.FeedCopy;

public class FeedCopyTestCase {
	private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/feed_copy.json";
	private static final String SUCCESS_SPEC2 = "src/test/resources/processor/properties/feed_copy2.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160125";
	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}

	@Test
	public void testFeedCopySuccess() throws Exception {
		FeedCopy processor = new FeedCopy(CLIENT_SPEC, SUCCESS_SPEC,RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_SUCCESS, status.getStatusCode());

		processor.close();
	}
	
//	@Test
//	public void testFeedCopySuccess2() throws Exception {
//		FeedCopy processor = new FeedCopy(CLIENT_SPEC, SUCCESS_SPEC2,RUNTIMEDATE);
//		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
//		AssertJUnit.assertEquals(Status.STATUS_SUCCESS, status.getStatusCode());
//
//		processor.close();
//	}
}