package com.rboomerang.test.validator.common;

import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.rboomerang.common.framework.Status;
import com.rboomerang.validator.impl.common.CountDegradationAlert;

public class CountDegradationAlertTest {
	private static final String SUCCESS_SPEC = "src/test/resources/validator/properties/count_degradation_alert.json";
	//private static final String ERROR_SPEC = "src/test/resources/validator/properties/count_degradation_alert.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160118";
	
	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}
	@Test
	public void testCountDegradationAlerts() throws Exception {
		
		CountDegradationAlert processor = new CountDegradationAlert(CLIENT_SPEC, SUCCESS_SPEC,RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testCountDegradationAlerts done");
		processor.close();
	}

	/*@Test
	public void testAmazonSqsProcessorError() throws Exception {
		CountDegradationAlert processor = new CountDegradationAlert(CLIENT_SPEC, ERROR_SPEC);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_ERROR);
		System.out.println("testAmazonSqsClientError done");
		processor.close();
	}*/


}
