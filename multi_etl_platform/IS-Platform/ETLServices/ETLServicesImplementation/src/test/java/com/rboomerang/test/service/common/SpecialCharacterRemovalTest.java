package com.rboomerang.test.service.common;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.SpecialCharactersRemoval;
  
public class SpecialCharacterRemovalTest {
    private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/TestCases/SpecialCharacterRemoval/NonUTF-8_to_UTF-8_delimiter_change_no_quotesAs_ISO-8859-1.json";
    private static final String SUCCESS_SPEC2 = "src/test/resources/processor/properties/TestCases/SpecialCharacterRemoval/UTF-8_to_UTF-8_delimiter_change_no_quotes.json";
    private static final String SUCCESS_SPEC3 = "src/test/resources/processor/properties/TestCases/SpecialCharacterRemoval/UTF-8_to_UTF-8_delimiter_change_All_quoted.json";
    private static final String SUCCESS_SPEC4 = "src/test/resources/processor/properties/TestCases/SpecialCharacterRemoval/UTF-8_to_UTF-8_delimiter_change_Few_quoted.json";
    private static final String SUCCESS_SPEC5 = "src/test/resources/processor/properties/TestCases/SpecialCharacterRemoval/NonUTF-8_to_UTF-8_delimiter_change_Few_quoted_As_ISO-8859.json";
    private static final String SUCCESS_SPEC6 = "src/test/resources/processor/properties/TestCases/SpecialCharacterRemoval/NonUTF-8_to_UTF-8_delimiter_change_All_quoted_As_ISO-8859.json";
    private static final String ERROR_SPEC = "src/test/resources/processor/properties/SpecialCharacterRemovalFail.json";
    private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
    private static final String RUNTIMEDATE = "20151216";
    @BeforeMethod
	protected void setUp() throws Exception {
    }
  
    @AfterMethod
	protected void tearDown() throws Exception {
    }
  
    @Test
	public void testspecialCharacterRemovalSuccess1() throws Exception {
        SpecialCharactersRemoval processor = new SpecialCharactersRemoval(CLIENT_SPEC, SUCCESS_SPEC,SpecialCharacterRemovalTest.RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
        System.out.println("testspecialCharacterRemovalZipTestSuccess done\n");
        processor.close();
    }
     
    @Test
	public void testspecialCharacterRemovalSuccess2() throws Exception {
        SpecialCharactersRemoval processor = new SpecialCharactersRemoval(CLIENT_SPEC, SUCCESS_SPEC2,SpecialCharacterRemovalTest.RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
        System.out.println("testspecialCharacterRemovalNoCompressionSuccess done\n");
        processor.close();
    }
    @Test
	public void testspecialCharacterRemovalSuccess3() throws Exception {
        SpecialCharactersRemoval processor = new SpecialCharactersRemoval(CLIENT_SPEC, SUCCESS_SPEC3,SpecialCharacterRemovalTest.RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
        System.out.println("testspecialCharacterRemovalNoCompressionSuccess done\n");
        processor.close();
    }
    @Test
	public void testspecialCharacterRemovalSuccess4() throws Exception {
        SpecialCharactersRemoval processor = new SpecialCharactersRemoval(CLIENT_SPEC, SUCCESS_SPEC4,SpecialCharacterRemovalTest.RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
        System.out.println("testspecialCharacterRemovalNoCompressionSuccess done\n");
        processor.close();
    }
    
    @Test
	public void testspecialCharacterRemovalSuccess5() throws Exception {
        SpecialCharactersRemoval processor = new SpecialCharactersRemoval(CLIENT_SPEC, SUCCESS_SPEC5,SpecialCharacterRemovalTest.RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
        System.out.println("testspecialCharacterRemovalNoCompressionSuccess done\n");
        processor.close();
    }
    
    @Test
	public void testspecialCharacterRemovalSuccess6() throws Exception {
        SpecialCharactersRemoval processor = new SpecialCharactersRemoval(CLIENT_SPEC, SUCCESS_SPEC6,SpecialCharacterRemovalTest.RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
        System.out.println("testspecialCharacterRemovalNoCompressionSuccess done\n");
        processor.close();
    }
    
    @Test
	public void testspecialCharacterRemovalError1() throws Exception {
        SpecialCharactersRemoval processor = new SpecialCharactersRemoval(CLIENT_SPEC, ERROR_SPEC,SpecialCharacterRemovalTest.RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_ERROR);
        System.out.println("testspecialCharacterRemovalError done\n");
        processor.close();
    }
}
