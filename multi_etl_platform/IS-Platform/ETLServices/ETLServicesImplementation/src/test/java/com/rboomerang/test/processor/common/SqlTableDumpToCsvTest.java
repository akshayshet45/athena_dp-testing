package com.rboomerang.test.processor.common;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.SqlTableDumpToCsv;

public class SqlTableDumpToCsvTest {
	private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/SqlTableDumpToCsvSuccess.json";
	private static final String ERROR_SPEC = "src/test/resources/processor/properties/SqlTableDumpToCsvFail.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160323";
	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}

	@Test
	public void testsqlTableDumpToCsvSuccess() throws Exception {
		SqlTableDumpToCsv processor = new SqlTableDumpToCsv(CLIENT_SPEC, SUCCESS_SPEC,SqlTableDumpToCsvTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testsqlTableDumpToCsvSuccess done");
		processor.close();
	}

	@Test
	public void testsqlTableDumpToCsvError() throws Exception {
		SqlTableDumpToCsv processor = new SqlTableDumpToCsv(CLIENT_SPEC, ERROR_SPEC,SqlTableDumpToCsvTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_ERROR);
		System.out.println("testsqlTableDumpToCsvError done");
		processor.close();
	}
}
