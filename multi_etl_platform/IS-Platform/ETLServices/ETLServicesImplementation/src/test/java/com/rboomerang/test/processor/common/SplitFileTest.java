package com.rboomerang.test.processor.common;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.SplitFile;

public class SplitFileTest {
	private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/SplitFile.json";
	private static final String ERROR_SPEC = "src/test/resources/processor/properties/SplitFileFail.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20151216";
	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}

	@Test
	public void testsplitFileSuccess() throws Exception {
		SplitFile processor = new SplitFile(CLIENT_SPEC, SUCCESS_SPEC,SplitFileTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_SUCCESS,status.getStatusCode());
		System.out.println("testsplitFileSuccess done");
		processor.close();
	}
	
	@Test
	public void testsplitFileError() throws Exception {
		SplitFile processor = new SplitFile(CLIENT_SPEC, ERROR_SPEC,SplitFileTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_ERROR,status.getStatusCode());
		System.out.println("testsplitFileError done");
		processor.close();
	}
}
