package com.rboomerang.test.processor.common;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.MailAttachmentDownloader;

public class MailAttachmentDownloaderTest {
	private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/MailAttachmentDownloaderSuccess.json";
	private static final String SUCCESS_SPEC2 = "src/test/resources/processor/properties/MailAttachmentDownloaderSuccess3.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160311";
	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}

	@Test
	public void testMailAttachmentDownloaderSuccess() throws Exception {
		MailAttachmentDownloader processor = new MailAttachmentDownloader(CLIENT_SPEC, SUCCESS_SPEC,MailAttachmentDownloaderTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testMailAttachmentDownloaderSuccess done");
		processor.close();
	}

	@Test
	public void testMailAttachmentDownloaderSuccess2() throws Exception {
		MailAttachmentDownloader processor = new MailAttachmentDownloader(CLIENT_SPEC, SUCCESS_SPEC2,"20160302");
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testMailAttachmentDownloaderSuccess2 done");
		processor.close();
	}
}
