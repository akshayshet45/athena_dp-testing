package com.rboomerang.test.processor.common;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.SpecialCharactersRemoval;
  
public class SpecialCharacterRemovalTest {
    private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/SpecialCharacterRemovalSuccess.json";
    private static final String SUCCESS_SPEC_NO_COMPRESSION = "src/test/resources/processor/properties/SpecialCharacterRemovalSuccess2.json";
    private static final String ERROR_SPEC = "src/test/resources/processor/properties/SpecialCharacterRemovalFail.json";
    private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
    private static final String RUNTIMEDATE = "20151216";
    @BeforeMethod
	protected void setUp() throws Exception {
    }
  
    @AfterMethod
	protected void tearDown() throws Exception {
    }
  
    @Test
	public void testspecialCharacterRemovalZipTestSuccess() throws Exception {
        SpecialCharactersRemoval processor = new SpecialCharactersRemoval(CLIENT_SPEC, SUCCESS_SPEC,SpecialCharacterRemovalTest.RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
        System.out.println("testspecialCharacterRemovalZipTestSuccess done\n");
        processor.close();
    }
     
    @Test
	public void testspecialCharacterRemovalNoCompressionSuccess() throws Exception {
        SpecialCharactersRemoval processor = new SpecialCharactersRemoval(CLIENT_SPEC, SUCCESS_SPEC_NO_COMPRESSION,SpecialCharacterRemovalTest.RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
        System.out.println("testspecialCharacterRemovalNoCompressionSuccess done\n");
        processor.close();
    }
  
    @Test
	public void testspecialCharacterRemovalError() throws Exception {
        SpecialCharactersRemoval processor = new SpecialCharactersRemoval(CLIENT_SPEC, ERROR_SPEC,SpecialCharacterRemovalTest.RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_ERROR);
        System.out.println("testspecialCharacterRemovalError done\n");
        processor.close();
    }
}
