package com.rboomerang.test.service.common;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.SplitFile;

public class SplitFileTest {
	private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/TestCases/SplitFile/Split_csv_with_quotes.json";
	private static final String SUCCESS_SPEC2 = "src/test/resources/processor/properties/TestCases/SplitFile/Split_csv_without_quotes.json";
	private static final String SUCCESS_SPEC3 = "src/test/resources/processor/properties/TestCases/SplitFile/Split_csv_FewColumn_quoted.json";
	private static final String SUCCESS_SPEC4 = "src/test/resources/processor/properties/TestCases/SplitFile/Split_without_fewColumnValues_skip_rowsAs_No.json";
	private static final String SUCCESS_SPEC5 = "src/test/resources/processor/properties/TestCases/SplitFile/Split_without_fewColumnValues_skip_rowsAs_Yes.json";
	private static final String ERROR_SPEC = "src/test/resources/processor/properties/SplitFileFail.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20151216";
	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}

	@Test
	public void Split_csv_with_quotes() throws Exception {
		SplitFile processor = new SplitFile(CLIENT_SPEC, SUCCESS_SPEC,SplitFileTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_SUCCESS,status.getStatusCode());
		System.out.println("testsplitFileSuccess done");
		processor.close();
	}
	
	@Test
	public void Split_csv_without_quotes() throws Exception {
		SplitFile processor = new SplitFile(CLIENT_SPEC, SUCCESS_SPEC2,SplitFileTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_SUCCESS,status.getStatusCode());
		System.out.println("testsplitFileSuccess done");
		processor.close();
	}
	
	@Test
	public void Split_csv_FewColumn_quoted() throws Exception {
		SplitFile processor = new SplitFile(CLIENT_SPEC, SUCCESS_SPEC3,SplitFileTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_SUCCESS,status.getStatusCode());
		System.out.println("testsplitFileSuccess done");
		processor.close();
	}
	
	@Test
	public void Split_without_fewColumnValues_skip_rowsAs_No() throws Exception {
		SplitFile processor = new SplitFile(CLIENT_SPEC, SUCCESS_SPEC4,SplitFileTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_SUCCESS,status.getStatusCode());
		System.out.println("testsplitFileSuccess done");
		processor.close();
	}
	@Test
	public void Split_without_fewColumnValues_skip_rowsAs_Yes() throws Exception {
		SplitFile processor = new SplitFile(CLIENT_SPEC, SUCCESS_SPEC5,SplitFileTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_SUCCESS,status.getStatusCode());
		System.out.println("testsplitFileSuccess done");
		processor.close();
	}
	
	
	@Test
	public void testsplitFileError() throws Exception {
		SplitFile processor = new SplitFile(CLIENT_SPEC, ERROR_SPEC,SplitFileTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_ERROR,status.getStatusCode());
		System.out.println("testsplitFileError done");
		processor.close();
	}
}
