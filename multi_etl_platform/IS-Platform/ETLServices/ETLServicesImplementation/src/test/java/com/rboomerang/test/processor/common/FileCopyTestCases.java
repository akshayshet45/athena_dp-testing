package com.rboomerang.test.processor.common;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.FileCopy;

public class FileCopyTestCases {
	private static final String SUCCESS_SPEC_SFTP_SFTP = "src/test/resources/processor/properties/FileCopy.json";
	private static final String SUCCESS_SPEC_SFTP_S3 = "src/test/resources/processor/properties/FileCopy2.json";
	private static final String SUCCESS_SPEC_S3_SFTP = "src/test/resources/processor/properties/FileCopy3.json";
	private static final String SUCCESS_SPEC_S3_S3 = "src/test/resources/processor/properties/FileCopy4.json";
	private static final String FAILURE_SPEC = "src/test/resources/processor/properties/FileCopyFailure.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160108";
	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}

	@Test
	public void testFileCopySuccess_sftp_sftp() throws Exception {
		FileCopy processor = new FileCopy(CLIENT_SPEC, SUCCESS_SPEC_SFTP_SFTP,RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_SUCCESS, status.getStatusCode());
		System.out.println("SFTP to SFTP Test case is successful");
		processor.close();
	}
//	@Test
//	public void testFileCopySuccess_sftp_s3() throws Exception {
//		FileCopy processor = new FileCopy(CLIENT_SPEC, SUCCESS_SPEC_SFTP_S3,RUNTIMEDATE);
//		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
//		AssertJUnit.assertEquals(Status.STATUS_SUCCESS, status.getStatusCode());
//		System.out.println("SFTP to S3 Test Case is successful");
//		processor.close();
//	}
	@Test
	public void testFileCopySuccess_s3_sftp() throws Exception {
		FileCopy processor = new FileCopy(CLIENT_SPEC, SUCCESS_SPEC_S3_SFTP,RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_SUCCESS, status.getStatusCode());
		System.out.println("S3 to SFTP Test Case is successful");
		processor.close();
	}
//	@Test
//	public void testFileCopySuccess_s3_s3() throws Exception {
//		FileCopy processor = new FileCopy(CLIENT_SPEC, SUCCESS_SPEC_S3_S3,RUNTIMEDATE);
//		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
//		AssertJUnit.assertEquals(Status.STATUS_SUCCESS, status.getStatusCode());
//		System.out.println("S3 to S3 Test caseis successful!!");
//		processor.close();
//	}
	@Test
	public void testFileCopyFailure() throws Exception {
		FileCopy processor = new FileCopy(CLIENT_SPEC, FAILURE_SPEC,RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_ERROR, status.getStatusCode());
		System.out.println("FAILURE Test caseis successful!!");
		processor.close();
	}
}