package com.rboomerang.test.processor.common;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import org.testng.AssertJUnit;

import com.amazonaws.auth.AWSCredentials;
import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.AmazonSqsProcessor;
import com.rboomerang.utils.AWSAccess;
//import com.rboomerang.utils.AmazonSqsHelper;
import com.rboomerang.utils.AmazonSqsHelper;

public class AmazonSqsProcessorTest{

	private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/AmazonSqsProcessorSuccess.json";
	private static final String ERROR_SPEC = "src/test/resources/processor/properties/AmazonSqsProcessorfailure.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20151216";

	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}

	
	@Test
	public void testAmazonSqsProcessorSuccess() throws Exception {
		AWSAccess awsAccess = new AWSAccess();
		AWSCredentials basicAWSCredentials = awsAccess.getAWSUserSpecificAccess();
		AmazonSqsHelper sqsHelper = new AmazonSqsHelper(basicAWSCredentials, "us-west-2", "", "");

		sqsHelper.sendMessage(
				"{\"outputPath\":\"/prod/\",\"clientName\":\"IS-Project\",\"runId\":1,\"isSimulation\":true}",
				"https://sqs.us-west-2.amazonaws.com/208876916689/kishan_test", "kishan_test");
		//creating sqs message in queue for success case
		AmazonSqsProcessor processor = new AmazonSqsProcessor(CLIENT_SPEC, SUCCESS_SPEC,
				AmazonSqsProcessorTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testAmazonSqsClientSuccess done");
		processor.close();
	}

	@Test
	public void testAmazonSqsProcessorError() throws Exception {
		AmazonSqsProcessor processor = new AmazonSqsProcessor(CLIENT_SPEC, ERROR_SPEC,
				AmazonSqsProcessorTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_ERROR);
		System.out.println("testAmazonSqsClientError done");
		processor.close();
	}

}
