package com.rboomerang.test.processor.common;
  
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.MailSender;
  
public class MailSenderTestCases{
    private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/MailSender.json";
    private static final String FAIL_CASE = "src/test/resources/processor/properties/MailSenderFailure.json";
    private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
    private static final String RUNTIMEDATE = "20160304";
    @BeforeMethod
    protected void setUp() throws Exception {
    }
  
    @AfterMethod
    protected void tearDown() throws Exception {
    }
  
    @Test
    public void testMailProcessorSuccess() throws Exception {
        MailSender processor = new MailSender(CLIENT_SPEC, SUCCESS_SPEC,RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(Status.STATUS_SUCCESS, status.getStatusCode());
        System.out.println("testCase success case passed!!");
        processor.close();
    }
     
    @Test
    public void testMailProcessorFailure() throws Exception {
        MailSender processor = new MailSender(CLIENT_SPEC, FAIL_CASE,RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(Status.STATUS_ERROR, status.getStatusCode());
        System.out.println("Fail Test case passed!!");
        processor.close();
    }
}
