package com.rboomerang.test.processor.common;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.FileConverter;

public class FileConverterTest {

	private static final String CSV_TO_XLSX_SUCCESS_SPEC = "src/test/resources/processor/properties/CsvToXlsxConverterSuccess.json";
	private static final String XLSX_TO_CSV_SUCCESS_SPEC = "src/test/resources/processor/properties/XlsxToCsvConverterSuccess.json";
	private static final String CSV_TO_XLSX_FAILURE_SPEC = "src/test/resources/processor/properties/CsvToXlsxConverterFailure.json";
	private static final String XLSX_TO_CSV_FAILURE_SPEC = "src/test/resources/processor/properties/XlsxToCsvConverterFailure.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160525";

	@Test
	public void testXlsxToCsvConverterSuccess() throws Exception {
		FileConverter processor = new FileConverter(CLIENT_SPEC, XLSX_TO_CSV_SUCCESS_SPEC,
				FileConverterTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testXlsxToCsvConverterSuccess done");
		processor.close();
	}

	@Test
	public void testCsvToXlsxConverterSuccess() throws Exception {
		FileConverter processor = new FileConverter(CLIENT_SPEC, CSV_TO_XLSX_SUCCESS_SPEC,
				FileConverterTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testCsvToXlsxConverterSuccess done");
		processor.close();
	}

	@Test
	public void testXlsxToCsvConverterFailure() throws Exception {
		FileConverter processor = new FileConverter(CLIENT_SPEC, XLSX_TO_CSV_FAILURE_SPEC,
				FileConverterTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_ERROR);
		System.out.println("testXlsxToCsvConverterFailure done");
		processor.close();
	}

	@Test
	public void testCsvToXlsxConverterFailure() throws Exception {
		FileConverter processor = new FileConverter(CLIENT_SPEC, CSV_TO_XLSX_FAILURE_SPEC,
				FileConverterTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_ERROR);
		System.out.println("testCsvToXlsxConverterFailure done");
		processor.close();
	}
}
