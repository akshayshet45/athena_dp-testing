package com.rboomerang.test.validator.common;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import com.rboomerang.common.framework.Status;
import com.rboomerang.validator.impl.common.HeaderValidation;;

public class HeaderValidationTestCase {
	private static final String SUCCESS_SPEC = "src/test/resources/validator/properties/header_validation.json";
	private static final String SUCCESS_SPEC2 = "src/test/resources/validator/properties/header_validation2.json";
	private static final String ERROR_SPEC = "src/test/resources/validator/properties/header_validation_fail.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160125";
	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}

	@Test
	public void testHeaderValidationSuccess() throws Exception {
		HeaderValidation validator = new HeaderValidation(CLIENT_SPEC, SUCCESS_SPEC,HeaderValidationTestCase.RUNTIMEDATE);
		Status status = validator.service(validator.getClientSpec(), validator.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_SUCCESS, status.getStatusCode());
		validator.close();
	}
	@Test
	public void testHeaderValidationSuccess2() throws Exception {
		HeaderValidation validator = new HeaderValidation(CLIENT_SPEC, SUCCESS_SPEC2,HeaderValidationTestCase.RUNTIMEDATE);
		Status status = validator.service(validator.getClientSpec(), validator.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_SUCCESS, status.getStatusCode());
		validator.close();
	}
	
	@Test
	public void testHeaderValidationError() throws Exception {
		HeaderValidation validator = new HeaderValidation(CLIENT_SPEC, ERROR_SPEC,HeaderValidationTestCase.RUNTIMEDATE);
		Status status = validator.service(validator.getClientSpec(), validator.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_ERROR, status.getStatusCode());
		validator.close();
	}
}