package com.rboomerang.test.processor.common;


import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.ScriptLiteExecutor;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class ScriptLiteExecutorTest {
    private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/ScriptLiteExecutor.json";
    private static final String FAILURE_SPEC = "src/test/resources/processor/properties/ScriptLiteExecutorFailure.json";
    private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
    private static final String RUNTIMEDATE = "20151024";

    @BeforeMethod
    protected void setUp() throws Exception {
    }

    @AfterMethod
    protected void tearDown() throws Exception {
    }

    @Test
    public void testScriptLiteExecutorSuccess() throws Exception {
    	ScriptLiteExecutor processor = new ScriptLiteExecutor(CLIENT_SPEC, SUCCESS_SPEC, ScriptLiteExecutorTest.RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(Status.STATUS_SUCCESS, status.getStatusCode());

        processor.close();
    }
    @Test
    public void testScriptLiteExecutorFailure() throws Exception {
    	ScriptLiteExecutor processor = new ScriptLiteExecutor(CLIENT_SPEC, FAILURE_SPEC, ScriptLiteExecutorTest.RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(Status.STATUS_ERROR, status.getStatusCode());
        processor.close();
    }

   
}
