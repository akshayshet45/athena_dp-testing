package com.rboomerang.test.processor.common;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.CSVProcessor;

public class CSVProcessorTest {
	private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/CSVProcessorSuccess.json";
	private static final String ERROR_SPEC = "src/test/resources/processor/properties/CSVProcessorFail.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160517";

	@Test
	public void testCSVProcessorSuccess() throws Exception {
		CSVProcessor processor = new CSVProcessor(CLIENT_SPEC, SUCCESS_SPEC,
				CSVProcessorTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testCSVProcessorSuccess done");
		processor.close();
	}

	@Test
	public void testCSVProcessorError() throws Exception {
		CSVProcessor processor = new CSVProcessor(CLIENT_SPEC, ERROR_SPEC,
				CSVProcessorTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_ERROR);
		System.out.println("testCSVProcessorError done");
		processor.close();
	}
}
