package com.rboomerang.test.custom.nordstrom;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.TestNG;

import com.rboomerang.common.framework.Status;
import com.rboomerang.custom.nordstrom.NordstromFileManipulation;



public class NordstromFileManipulationTest extends TestNG {
	private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/NordstromFileManipulation.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20151216";
	
	@Test
	public void testNordstromFileManipulationTestSuccess() throws Exception {
		NordstromFileManipulation processor = new NordstromFileManipulation(CLIENT_SPEC, SUCCESS_SPEC,NordstromFileManipulationTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testNordstromFileManipulationSuccess done");
		processor.close();
	}
	
}
