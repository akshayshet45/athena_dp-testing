package com.rboomerang.test.validator.common;

import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.rboomerang.common.framework.Status;
import com.rboomerang.validator.impl.common.DuplicateValidator;

public class DuplicateValidatorTest {
	private static final String SUCCESS_SPEC = "src/test/resources/validator/properties/ddValidatorSuccess.json";
	private static final String ERROR_SPEC = "src/test/resources/validator/properties/ddValidatorError.json";
	private static final String ERROR_SPEC_COUNT = "src/test/resources/validator/properties/ddValidatorErrorCount.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160118";

	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}

	@Test
	public void ddValidatorSuccess() throws Exception {
		DuplicateValidator validator = new DuplicateValidator(CLIENT_SPEC,
				SUCCESS_SPEC, DuplicateValidatorTest.RUNTIMEDATE);
		Status status = validator.service(validator.getClientSpec(),
				validator.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_SUCCESS, status.getStatusCode());
		System.out.println("ddValidatorSuccess done");
		validator.close();
	}

	@Test
	public void ddValidatorError() throws Exception {
		DuplicateValidator validator = new DuplicateValidator(CLIENT_SPEC,
				ERROR_SPEC, DuplicateValidatorTest.RUNTIMEDATE);
		Status status = validator.service(validator.getClientSpec(),
				validator.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_ERROR, status.getStatusCode());
		System.out.println("ddValidatorError done");
		validator.close();
	}

	@Test
	public void ddValidatorErrorCount() throws Exception {
		DuplicateValidator validator = new DuplicateValidator(CLIENT_SPEC,
				ERROR_SPEC_COUNT, DuplicateValidatorTest.RUNTIMEDATE);
		Status status = validator.service(validator.getClientSpec(),
				validator.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_ERROR, status.getStatusCode());
		System.out.println("ddValidatorError done");
		validator.close();
	}

	// public void testFileDiffValidatorError() throws Exception {
	// FileDiffValidator validator = new FileDiffValidator(CLIENT_SPEC,
	// ERROR_SPEC);
	// Status status = validator.validate(validator.getClientSpec(),
	// validator.getValidatorSpec());
	// validator.close();
	// }

}
