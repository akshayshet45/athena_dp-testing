package com.rboomerang.test.processor.common;
  
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.MailchimpMailSender;
  
public class MailchimpMailSenderTest{
    private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/MailchimpMailSender.json";
    private static final String SUCCESS_SPEC_2 = "src/test/resources/processor/properties/MailchimpMailSenderSuccess2.json";
    private static final String SUCCESS_SPEC_3 = "src/test/resources/processor/properties/MailchimpMailSenderSuccess3.json";
    private static final String FAIL_CASE = "src/test/resources/processor/properties/MailchimpMailSenderTestFailure.json";
    private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
    private static final String RUNTIMEDATE = "20160321";
    @BeforeMethod
    protected void setUp() throws Exception {
    }
   
    @AfterMethod
    protected void tearDown() throws Exception {
    }
  
    @Test
    public void testMailProcessorSuccess() throws Exception {
    	MailchimpMailSender processor = new MailchimpMailSender(CLIENT_SPEC, SUCCESS_SPEC_3,RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(Status.STATUS_SUCCESS, status.getStatusCode());
        System.out.println("testCase success case passed!!");
        processor.close();
    }
     
    @Test
    public void testMailProcessorFailure() throws Exception {
    	MailchimpMailSender processor = new MailchimpMailSender(CLIENT_SPEC, FAIL_CASE,RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(Status.STATUS_ERROR, status.getStatusCode());
        System.out.println("Fail Test case passed!!");
        processor.close();
    }
}
