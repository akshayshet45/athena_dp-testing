package com.rboomerang.test.processor.common;
  
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.Utf8Converter;
  
public class Utf8ConverterTestCases{
    private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/Utf8Converter.json";
    private static final String FAIL_CASE = "src/test/resources/processor/properties/Utf8ConverterFailure.json";
    private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
    private static final String RUNTIMEDATE = "20160120";
    @BeforeMethod
    protected void setUp() throws Exception {
    }
  
    @AfterMethod
    protected void tearDown() throws Exception {
    }
  
    @Test
    public void testUtf8ConverterProcessorSuccess() throws Exception {
    	Utf8Converter processor = new Utf8Converter(CLIENT_SPEC, SUCCESS_SPEC,RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(Status.STATUS_SUCCESS, status.getStatusCode());
        System.out.println("testCase success case passed!!");
        processor.close();
    }
     
    @Test
    public void tesUtf8ProcessorFailure() throws Exception {
    	Utf8Converter processor = new Utf8Converter(CLIENT_SPEC, FAIL_CASE,RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(Status.STATUS_ERROR, status.getStatusCode());
        System.out.println("Fail Test case passed!!");
        processor.close();
    }
}
