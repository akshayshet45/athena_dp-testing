package com.rboomerang.test.processor.common;

import org.testng.AssertJUnit;
import org.testng.TestNG;
import org.testng.annotations.Test;

import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.AmazonSqsClient;



@Test
public class AmazonSqsClienttest extends TestNG {
	
	private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/AmazonSqsClientSuccess.json";
	private static final String SUCCESS_SPEC_NO_QUOTES = "src/test/resources/processor/properties/CSVProcessorSuccess2.json";
	private static final String ERROR_SPEC = "src/test/resources/processor/properties/CSVProcessorFail.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20151216";
	
	
	/*public void testAmazonSqsClientSuccess() throws Exception {
		AmazonSqsClient processor = new AmazonSqsClient(CLIENT_SPEC, SUCCESS_SPEC,AmazonSqsClienttest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testAmazonSqsClientSuccess done");
		processor.close();
	}*/

}
