package com.rboomerang.test.validator.common;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import com.rboomerang.common.framework.Status;
import com.rboomerang.validator.impl.common.TableValidationReporter;;

public class TableValidationReporterTest {
	 private static final String ERROR_SPEC ="src/test/resources/validator/properties/table_validation_reporter_failure.json";

	private static final String SUCCESS_SPEC = "src/test/resources/validator/properties/table_validation_reporter_success.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160125";

	@Test
	public void TableValidationReporterTestSuccess() throws Exception {
		TableValidationReporter validator = new TableValidationReporter(CLIENT_SPEC, SUCCESS_SPEC, RUNTIMEDATE);
		Status status = validator.service(validator.getClientSpec(), validator.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_SUCCESS, status.getStatusCode());
		validator.close();
	}
	@Test

	 public void TableValidationReporterTestFailure() throws Exception {
	 TableValidationReporter validator = new TableValidationReporter(CLIENT_SPEC,ERROR_SPEC,RUNTIMEDATE);
	 Status status = validator.service(validator.getClientSpec(),
	 validator.getServiceSpec());
	 AssertJUnit.assertEquals(Status.STATUS_ERROR, status.getStatusCode());
	 validator.close();
	 }
}