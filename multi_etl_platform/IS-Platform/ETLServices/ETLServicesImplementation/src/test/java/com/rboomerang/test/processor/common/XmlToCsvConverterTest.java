package com.rboomerang.test.processor.common;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.XmlToCsvConverter;

public class XmlToCsvConverterTest{
	private static final String Success_SPEC = "src/test/resources/processor/properties/XmlToCsvConverterSuccess.json";
	private static final String Success_SPEC2 = "src/test/resources/processor/properties/XmlToCsvConverterSuccess2.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160125";
	
	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}
	
	@Test
	public void testXmlToCsvConverterZipSuccess() throws Exception {
		XmlToCsvConverter processor = new XmlToCsvConverter(CLIENT_SPEC, Success_SPEC2,XmlToCsvConverterTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testXmlToCsvConverterSuccess done");
		processor.close();
	}
	
	@Test
	public void testXmlToCsvConverterXmlSuccess() throws Exception {
		XmlToCsvConverter processor = new XmlToCsvConverter(CLIENT_SPEC, Success_SPEC,XmlToCsvConverterTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testXmlToCsvConverterSuccess done");
		processor.close();
	}
}
