package com.rboomerang.test.validator.common;

import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.rboomerang.common.framework.Status;
import com.rboomerang.validator.impl.common.S3Poll;

public class S3PollTest {
	private static final String SUCCESS_SPEC = "src/test/resources/validator/properties/S3PollSuccess.json";
	private static final String SUCCESS_SPEC2 = "src/test/resources/validator/properties/S3PollSuccess2.json";
	private static final String ERROR_SPEC = "src/test/resources/validator/properties/S3PollFail.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160405";
	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}

	@Test
	public void testS3PollSuccess() throws Exception {
		S3Poll validator = new S3Poll(CLIENT_SPEC, SUCCESS_SPEC,RUNTIMEDATE);
		Status status = validator.service(validator.getClientSpec(), validator.getServiceSpec());
		System.out.println(status.getMessage());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testS3PollSuccess done");
		validator.close();
	}
	
	@Test
	public void testS3PollSuccessSizeCheckOff() throws Exception {
		S3Poll validator = new S3Poll(CLIENT_SPEC, SUCCESS_SPEC2,RUNTIMEDATE);
		Status status = validator.service(validator.getClientSpec(), validator.getServiceSpec());
		System.out.println(status.getMessage());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testS3PollSuccess done");
		validator.close();
	}
	
	@Test
	public void testS3PollError() throws Exception {
		S3Poll validator = new S3Poll(CLIENT_SPEC, ERROR_SPEC,RUNTIMEDATE);
		Status status = validator.service(validator.getClientSpec(), validator.getServiceSpec());
	    System.out.println(status.getMessage());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_ERROR);
		System.out.println("testS3PollError done");
		validator.close();
	}
}
