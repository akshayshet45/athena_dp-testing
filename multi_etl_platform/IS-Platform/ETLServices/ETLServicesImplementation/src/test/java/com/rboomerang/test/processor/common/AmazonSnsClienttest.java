package com.rboomerang.test.processor.common;

import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.AmazonSnsClient;
import com.rboomerang.processor.impl.common.CSVProcessor;
import org.testng.AssertJUnit;
import org.testng.TestNG;
import org.testng.annotations.Test;


@Test
public class AmazonSnsClienttest extends TestNG {

	private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/AmazonSnsClientSuccess.json";
	private static final String ERROR_SPEC = "src/test/resources/processor/properties/AmazonSnsClientFailure.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20151216";


	@Test
	public void testAmazonSNSSuccess() throws Exception {
        System.out.println("testAmazonSNSSuccess");
		AmazonSnsClient processor = new AmazonSnsClient(CLIENT_SPEC, SUCCESS_SPEC,AmazonSnsClienttest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("testAmazonSNSSuccess done");
		processor.close();
	}

	@Test
	public void testAmazonSNSError() throws Exception {
        System.out.println("testAmazonSNSSuccess");
		AmazonSnsClient processor = new AmazonSnsClient(CLIENT_SPEC, ERROR_SPEC,AmazonSnsClienttest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_ERROR);
		System.out.println("testAmazonSNSSuccess done");
		processor.close();
	}

}
