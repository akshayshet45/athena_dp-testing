package com.rboomerang.test.service.common;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import com.rboomerang.common.framework.Status;
import com.rboomerang.validator.impl.common.FileExistenceCheck;

public class FileExistenceCheckTestCase {
	private static final String SUCCESS_SPEC = "src/test/resources/validator/properties/TestCases/file_check_sni.json";
	private static final String ERROR_SPEC = "src/test/resources/validator/properties/TestCases/file_check_fail_sni.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20160125";

	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}

	@Test
	public void testFileExistenceSuccess() throws Exception {
		
		System.out.println("testFileExistenceSuccess called");
		FileExistenceCheck validator = new FileExistenceCheck(CLIENT_SPEC, SUCCESS_SPEC,FileExistenceCheckTestCase.RUNTIMEDATE);
		Status status = validator.service(validator.getClientSpec(), validator.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_SUCCESS, status.getStatusCode());
		System.out.println("testFileExistenceSuccess done");
		validator.close();
	}

	@Test
	public void testFileExistenceError() throws Exception {
		System.out.println("testFileExistenceError called");
		FileExistenceCheck validator = new FileExistenceCheck(CLIENT_SPEC, ERROR_SPEC,FileExistenceCheckTestCase.RUNTIMEDATE);
		Status status = validator.service(validator.getClientSpec(), validator.getServiceSpec());
		AssertJUnit.assertEquals(Status.STATUS_ERROR, status.getStatusCode());
		System.out.println("testFileExistenceError done");
		validator.close();
	}
	
	
}
