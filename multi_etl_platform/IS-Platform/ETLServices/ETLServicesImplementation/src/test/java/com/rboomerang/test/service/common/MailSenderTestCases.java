package com.rboomerang.test.service.common;
  
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.MailSender;
  
public class MailSenderTestCases{
    private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/TestCases/MailSender/MailSender_Success.json";
    private static final String FAIL_CASE = "src/test/resources/processor/properties/TestCases/MailSender/MailSender_WrongFileName.json";
    private static final String FAIL_CASE2 = "src/test/resources/processor/properties/TestCases/MailSender/MailSender_FileGraterThan25MB.json";
    private static final String FAIL_CASE3 = "src/test/resources/processor/properties/TestCases/MailSender/MailSender_empty_recipientList.json";
    private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
    private static final String RUNTIMEDATE = "20160304";
    @BeforeMethod
    protected void setUp() throws Exception {
    }
  
    @AfterMethod
    protected void tearDown() throws Exception {
    }
  
    @Test
    public void MailSender_Success() throws Exception {
        MailSender processor = new MailSender(CLIENT_SPEC, SUCCESS_SPEC,RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(Status.STATUS_SUCCESS, status.getStatusCode());
        System.out.println("testCase success case passed!!");
        processor.close();
    }
     
    @Test
    public void MailSender_WrongFileName() throws Exception {
        MailSender processor = new MailSender(CLIENT_SPEC, FAIL_CASE,RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(Status.STATUS_ERROR, status.getStatusCode());
        System.out.println("Fail Test case passed!!");
        processor.close();
    }
    @Test
    public void MailSender_FileGraterThan25MB() throws Exception {
        MailSender processor = new MailSender(CLIENT_SPEC, FAIL_CASE2,RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(Status.STATUS_ERROR, status.getStatusCode());
        System.out.println("Fail Test case passed!!");
        processor.close();
    }

    @Test
    public void MailSender_empty_recipientList() throws Exception {
        MailSender processor = new MailSender(CLIENT_SPEC, FAIL_CASE3,RUNTIMEDATE);
        Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
        AssertJUnit.assertEquals(Status.STATUS_ERROR, status.getStatusCode());
        System.out.println("Fail Test case passed!!");
        processor.close();
    }
}

