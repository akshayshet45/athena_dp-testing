package com.rboomerang.test.custom.nordstrom;

import org.testng.AssertJUnit;
import org.testng.TestNG;
import org.testng.annotations.Test;

import com.rboomerang.common.framework.Status;
import com.rboomerang.custom.nordstrom.NordstromMissmatchesProccessor;

public class NordstromMismatchesProcesserTest extends TestNG {
	
	private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/NordstromMissmatchesSuccess.json";
	private static final String SUCCESS_SPEC2 = "src/test/resources/processor/properties/NordstromMissmatchesFTP2S3Success.json";
	private static final String ERROR_SPEC = "src/test/resources/processor/properties/NordstromMissmatchesNotExistingFail.json";

	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20151216";
	
	@Test
	public void NordstromMismatchesSuccess() throws Exception {
		NordstromMissmatchesProccessor processor = new NordstromMissmatchesProccessor(CLIENT_SPEC, SUCCESS_SPEC,RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("test success :: NordstromMissmatchesProccessor done");
		processor.close();
	}
	
	@Test
	public void NordstromMismatchesSuccess2() throws Exception {
		NordstromMissmatchesProccessor processor = new NordstromMissmatchesProccessor(CLIENT_SPEC, SUCCESS_SPEC2,RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		System.out.println("test success :: NordstromMissmatchesProccessor done");
		processor.close();
	}
	
	@Test
	public void NordstromMismatchesFailure() throws Exception {
		NordstromMissmatchesProccessor processor = new NordstromMissmatchesProccessor(CLIENT_SPEC, ERROR_SPEC,RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(), processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_ERROR);
		System.out.println("test success :: NordstromMissmatchesProccessor done");
		processor.close();
	}	
	
}
