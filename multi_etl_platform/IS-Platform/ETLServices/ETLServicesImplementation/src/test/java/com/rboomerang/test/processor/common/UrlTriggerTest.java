package com.rboomerang.test.processor.common;

import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import com.rboomerang.common.framework.Status;
import com.rboomerang.processor.impl.common.UrlTrigger;

public class UrlTriggerTest {

	private static final String SUCCESS_SPEC = "src/test/resources/processor/properties/url_trigger.json";
	private static final String ERROR_SPEC = "src/test/resources/processor/properties/url_trigger_fail.json";
	private static final String CLIENT_SPEC = "src/test/resources/client-spec.json";
	private static final String RUNTIMEDATE = "20151216";

	@BeforeMethod
	protected void setUp() throws Exception {
	}

	@AfterMethod
	protected void tearDown() throws Exception {
	}

	@Test
	public void testUrlTrigerSuccess() throws Exception {
		UrlTrigger processor = new UrlTrigger(CLIENT_SPEC, SUCCESS_SPEC,
				UrlTriggerTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(),
				processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_SUCCESS);
		processor.close();
	}

	@Test
	public void testUrlTrigerError() throws Exception {
		UrlTrigger processor = new UrlTrigger(CLIENT_SPEC, ERROR_SPEC,
				UrlTriggerTest.RUNTIMEDATE);
		Status status = processor.service(processor.getClientSpec(),
				processor.getServiceSpec());
		AssertJUnit.assertEquals(status.getStatusCode(), Status.STATUS_ERROR);
		processor.close();
	}
}
