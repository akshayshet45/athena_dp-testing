<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" >
<xsl:output method="text" omit-xml-declaration="yes" indent="no"/>
<xsl:template match="/">
styleID,styleName,current_price,vendor,ParentSKU,ChildSKU,WebSkuID
<xsl:for-each select="//ChildSKU">
<xsl:value-of select="concat(../../../../Style/@styleID,',',../../../../Style/@Name,',',../../../PriceView/@current,',',../../../Vendor,',',../../ParentSKU,',',./@sku,',',WebSkuID,'&#xA;')"/>
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>
