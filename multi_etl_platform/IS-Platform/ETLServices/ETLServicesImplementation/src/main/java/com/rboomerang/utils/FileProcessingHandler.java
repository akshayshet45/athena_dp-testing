package com.rboomerang.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.CSVReader;

/**
 * This util will be called by various services to perform service specific
 * tasks. Like FileConverter calls methods to convert csv file to xlsx,xlsx to
 * csv. FileMerger service calls for merging different file formats.
 * 
 * @author sumit
 */
public class FileProcessingHandler {

	private static Logger logger = LoggerFactory.getLogger(FileProcessingHandler.class);
	private static List<String> baseSheetHeaderColumns = new ArrayList<>();
	private static int baseSheetHeaderCount;

	/**
	 * 
	 * @param inputStream
	 * @param outputFile
	 * @param skipLines
	 * @param multipleXlsxSheet
	 * @throws Exception
	 */
	@SuppressWarnings("resource")
	public static void processXLSXFile(InputStream inputStream, File outputFile, int skipLines,
			boolean multipleXlsxSheet) throws Exception {
		BufferedInputStream bufferInputStream = new BufferedInputStream(inputStream);
		int totalRowsProcessed = 0;
		try {
			// Get the workbook object for XLSX file
			XSSFWorkbook wBook = new XSSFWorkbook(bufferInputStream);
			// Get first sheet from the workbook
			XSSFSheet sheet = wBook.getSheetAt(0);
			totalRowsProcessed += convertXLSXSheetToCSV(sheet, true, outputFile);
			Row row = null;
			Cell cell = null;
			Iterator<Cell> cellIterator = null;
			XSSFSheet sheet1 = null;
			if (multipleXlsxSheet && wBook.getNumberOfSheets() > 1) {
				for (int sheetCount = 1; sheetCount < wBook.getNumberOfSheets(); sheetCount++) {
					sheet1 = wBook.getSheetAt(sheetCount);
					row = sheet1.getRow(0);
					cellIterator = row.iterator();
					List<String> currentSheetHeaderColumns = new ArrayList<>();
					while (cellIterator.hasNext()) {
						cell = cellIterator.next();
						currentSheetHeaderColumns.add(cell.getStringCellValue());
					}
					// Before merging one sheet of xlsx file, check if their
					// headers are in the same order as that of base sheet and
					// in the same order.
					if (checkForHeaderMatch(currentSheetHeaderColumns, baseSheetHeaderColumns)) {
						totalRowsProcessed += convertXLSXSheetToCSV(sheet1, false, outputFile);
					} else {
						logger.warn("Headers found for sheet: [" + sheet1.getSheetName()
								+ "] doesn't match headers for sheet considered as base for processing: ["
								+ sheet.getSheetName() + "]\n Skipping data merging from this sheet");
					}
				}
			}
			logger.debug("Final count of rows processed: " + totalRowsProcessed);

		} finally {
			try {
				bufferInputStream.close();
			} catch (IOException ioe) {
				logger.error("Error closing input stream", ioe);
			}

		}
	}

	/**
	 * 
	 * @param currentSheetHeaderColumns
	 * @param baseSheetHeaderColumns
	 * @return
	 */
	private static boolean checkForHeaderMatch(List<String> currentSheetHeaderColumns,
			List<String> baseSheetHeaderColumns) {
		boolean match = true;
		if (currentSheetHeaderColumns.size() == baseSheetHeaderColumns.size()) {
			for (int index = 0; index < baseSheetHeaderColumns.size(); index++) {
				if (currentSheetHeaderColumns.get(index) != baseSheetHeaderColumns.get(index)) {
					match = false;
				}
			}
		} else {
			match = false;
		}
		return match;
	}

	/**
	 * 
	 * @param sheet
	 * @param isHeaderRow
	 * @param localFile
	 * @throws IOException
	 */
	public static int convertXLSXSheetToCSV(XSSFSheet sheet, boolean isHeaderRow, File localFile) throws IOException {
		Row row = null;
		Cell cell = null;
		Iterator<Row> rowIterator = sheet.iterator();
		Iterator<Cell> cellIterator = null;
		StringBuffer data = new StringBuffer();
		BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(localFile, true));
		if (!isHeaderRow && rowIterator.hasNext()) {
			rowIterator.next();
		}
		DataFormatter df = new DataFormatter();
		int rowCount = 0;
		boolean isEmptyRow = true;
		int emptyRowCount = 0;
		while (rowIterator.hasNext() && emptyRowCount < 2) {
			rowCount++;
			row = rowIterator.next();
			cellIterator = row.cellIterator();
			int lastCellIndex = row.getLastCellNum() - 1;
			while (cellIterator.hasNext()) {
				cell = cellIterator.next();
				if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK
						&& StringUtils.isNotBlank(cell.toString())) {
					isEmptyRow = false;
				}
				if (isHeaderRow) {
					if ((cell.getCellType() == Cell.CELL_TYPE_BLANK)) {
						data.replace(data.length() - 1, data.length(), "");
						isHeaderRow = false;
						break;
					}
					baseSheetHeaderCount++;
					baseSheetHeaderColumns.add(cell.getStringCellValue());
				} else if (cell.getColumnIndex() >= baseSheetHeaderCount) {
					break;
				}
				String formattedValue = df.formatCellValue(cell);
				if (formattedValue != null && formattedValue.contains(",")) {
					data.append("\"" + formattedValue + "\"");
				} else {
					data.append(formattedValue);
				}
				if ((isHeaderRow && cell.getColumnIndex() < (lastCellIndex - 1))
						|| (!isHeaderRow && cell.getColumnIndex() < (baseSheetHeaderCount - 1))) {
					data.append(",");
				}
			}
			data.append("\n");
			if (isEmptyRow) {
				emptyRowCount++;
			}
		}

		outputStream.write(data.toString().getBytes());
		outputStream.close();
		logger.debug(
				"Total rows processed for sheet: [" + sheet.getSheetName() + "] associated with xlsx: " + rowCount);
		return rowCount;
	}

	/**
	 * 
	 * @param inputStream
	 * @param outputFile
	 * @param skipLines
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	public static void convertCSVToXLSX(InputStream inputStream, File outputFile, int skipLines, char qouteChar,
			char delimeter) throws IOException {
		BufferedReader sourceReader = null;
		BufferedOutputStream outputStream = null;
		try {
			sourceReader = new BufferedReader(new InputStreamReader(inputStream));
			outputStream = new BufferedOutputStream(new FileOutputStream(outputFile));
			String line = null;
			InputStream lineStream = null;
			CSVReader csvreader = null;
			String[] csvRow = null;
			XSSFWorkbook workBook = new XSSFWorkbook();
			XSSFSheet sheet = workBook.createSheet("sheet1");
			int rowNum = -1;
			while (true) {
				if ((line = sourceReader.readLine()) == null) {
					break;
				}
				rowNum++;
				if (rowNum < skipLines) {
					continue;
				}
				XSSFRow currentSheetRow = sheet.createRow(rowNum);
				lineStream = IOUtils.toInputStream(line, "UTF-8");
				csvreader = new CSVReader(new InputStreamReader(lineStream), delimeter, qouteChar);
				csvRow = csvreader.readNext();
				csvreader.close();
				lineStream.close();
				for (int i = 0; i < csvRow.length; i++) {
					currentSheetRow.createCell(i).setCellValue(csvRow[i]);
				}
			}
			workBook.write(outputStream);
			logger.debug("Total rows processed for file: " + (rowNum + 1 - skipLines));

		} finally {
			try {
				sourceReader.close();
				outputStream.close();
			} catch (IOException e) {
				logger.error("Unable to close stream resources.", e);
			}

		}

	}

	/**
	 * 
	 * @param inputStream
	 * @param outputFile
	 * @param createNewWorkbook
	 * @param skipline
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	public static int mergeXLSXFiles(InputStream inputStream, File outputFile, boolean createNewWorkbook, int skipline)
			throws FileNotFoundException, IOException {
		XSSFWorkbook workbook = null;
		BufferedInputStream buffeInputStream = new BufferedInputStream(inputStream);
		if (createNewWorkbook) {
			workbook = new XSSFWorkbook();
		} else {
			workbook = new XSSFWorkbook(new FileInputStream(outputFile));
		}
		SXSSFWorkbook wb = new SXSSFWorkbook(workbook, 100);
		XSSFWorkbook sourceWorkbook = new XSSFWorkbook(buffeInputStream);
		XSSFSheet sheet2 = sourceWorkbook.getSheetAt(0);

		Sheet sheet1 = null;
		int mergedSheetLineCount = 0;
		if (createNewWorkbook) {
			sheet1 = wb.createSheet("sheet1");
		} else {
			sheet1 = wb.getSheetAt(0);
		}
		XSSFSheet sheet3 = workbook.getSheetAt(0);
		if (createNewWorkbook) {
			mergedSheetLineCount = sheet3.getLastRowNum();
		} else {
			mergedSheetLineCount = sheet3.getLastRowNum() + 1;
		}
		// add sheet2 to sheet1
		int processedRows = addSheet(sheet1, sheet2, mergedSheetLineCount, skipline);

		FileOutputStream outputStream1 = new FileOutputStream(outputFile);
		wb.write(outputStream1);
		buffeInputStream.close();
		outputStream1.close();
		return processedRows;

	}

	/**
	 * 
	 * @param mergedSheet
	 * @param sheet
	 * @param len
	 * @param skipline
	 */
	private static int addSheet(Sheet mergedSheet, XSSFSheet sheet, int len, int skipline) {
		int rowCount = skipline;
		DataFormatter df = new DataFormatter();
		int emptyRowCount = 0;
		for (rowCount = skipline; rowCount <= sheet.getLastRowNum() && emptyRowCount <= 2; rowCount++) {
			Row row = sheet.getRow(rowCount);
			Row mrow = mergedSheet.createRow(len++);
			boolean isEmptyRow = true;
			for (int k = row.getFirstCellNum(); k < row.getLastCellNum(); k++) {
				Cell cell = row.getCell(k);
				if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK
						&& StringUtils.isNotBlank(cell.toString())) {
					isEmptyRow = false;
				}
				Cell mcell = mrow.createCell(k);
				String formattedValue = null;
				if (cell != null) {
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_FORMULA:
						mcell.setCellFormula(cell.getCellFormula());
						formattedValue = df.formatCellValue(cell);
						mcell.setCellValue(formattedValue);
						break;
					case Cell.CELL_TYPE_BOOLEAN:
						mcell.setCellValue(cell.getBooleanCellValue());
						break;
					default:
						formattedValue = df.formatCellValue(cell);
						mcell.setCellValue(formattedValue);
						break;
					}
				}
			}
			if (isEmptyRow) {
				emptyRowCount++;
			}
		}
		logger.debug("Total rows processed for file: " + (rowCount - skipline));
		return (rowCount - skipline);
	}

	public static int mergeCSVFiles(InputStream inputStream, File localFile, int linesToBeSkipped) throws IOException {
		BufferedReader sourceReader = null;
		BufferedWriter writer = null;
		try {
			sourceReader = new BufferedReader(new InputStreamReader(inputStream));
			writer = new BufferedWriter(new FileWriter(localFile, true));
			String line = null;
			int rowNum = -1;
			while (true) {
				if ((line = sourceReader.readLine()) == null) {
					break;
				}
				rowNum++;
				if (rowNum < linesToBeSkipped) {
					continue;
				}
				writer.write(line);
				writer.write("\n");
			}
			logger.debug("Total rows processed for file: " + (rowNum + 1 - linesToBeSkipped));
			return (rowNum + 1 - linesToBeSkipped);
		} finally {
			try {
				writer.close();
				sourceReader.close();
			} catch (IOException e) {
				logger.error("Error while closing stream resources.", e);
			}

		}
	}
}
