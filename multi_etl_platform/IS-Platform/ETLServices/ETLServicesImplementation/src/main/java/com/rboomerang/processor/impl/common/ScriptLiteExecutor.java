package com.rboomerang.processor.impl.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Properties;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.ExecuteCommand;
import com.rboomerang.utils.InputStreamHandler;

public class ScriptLiteExecutor extends AbstractService
{

	private static Logger logger = LoggerFactory.getLogger(ScriptLiteExecutor.class);
	private final String PROD_ENV="prod";
	private final String QA_ENV="qa";
	private final String BETA_ENV="beta";
	private final String COMMON_ENV="common";
	public ScriptLiteExecutor(String clientSpecpath, String processorSpecPath) throws Exception
	{
		super(clientSpecpath, processorSpecPath);
		logger.info("ScriptLiteExecutor constructor called");
	}
	public ScriptLiteExecutor(String clientSpecpath, String processorSpecPath, String runtimeDate) throws Exception
	{
		super(clientSpecpath, processorSpecPath,runtimeDate);
		logger.info("ScriptLiteExecutor constructor called");
	}
	public ScriptLiteExecutor(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext, String runtimeDate) throws Exception 
	{
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.info("ScriptLiteExecutor constructor called");
	}
	@Override
	public Status service(ClientSpec clientSpec, JSONObject serviceSpec)
			throws JSchException, SftpException,Exception
	{
		Status status=null;
		try
		{
			JSONArray scriptArray=(JSONArray)serviceSpec.get("scriptName");
			int fileCount=scriptArray.size();
			String environment=System.getProperty(CommonConstants.AZKABAN_ENV_TAG);
			String projectName=serviceSpec.get("projectName").toString();
			logger.debug("Retrieved environment is "+environment);
			logger.debug("Validating environment...");
			String[] validEnvs=new String[]{PROD_ENV,QA_ENV,BETA_ENV};
			if(Arrays.asList(validEnvs).contains(new String(environment).toLowerCase()))
			{
				logger.debug("Provided environment "+environment+" is valid.");
				logger.debug("Reading environment specific properties");
				String redshift_host=null;
				String redshift_user=null;
				String redshift_pass=null;
				String redshift_db=null;
				String sqlFilePath=null;
				Properties jobProps= loadEnvironmentProperties(environment,projectName);
				Properties jobCommonProps=loadEnvironmentProperties(COMMON_ENV,projectName);
				redshift_host=jobProps.getProperty("bc_redshift_host");
				redshift_user=jobProps.getProperty("bc_redshift_user");
				redshift_pass=jobProps.getProperty("bc_redshift_pass");	
				redshift_db=jobCommonProps.getProperty("bc_redshift_db");
				logger.debug("\nRedshift details are "+redshift_host+"\t"+redshift_user+"\t"+redshift_db);
				int index;
				StringBuffer output=new StringBuffer();
				for(index=0;index<fileCount;index++)
				{
					String scriptName=(String)((JSONObject)scriptArray.get(index)).get(CommonConstants.FILENAME);
					StringBuffer urlBuffer=new StringBuffer();
					urlBuffer.append("jar:file:").append(CommonConstants.SCRIPTLITE_DIR).append("/").append(projectName)
							  .append("!/scripts/").append(environment).append("/").append(scriptName);
					URL url=null;
					JarURLConnection conn=null;
					try
					{
					url=new URL(urlBuffer.toString());
					conn = (JarURLConnection)url.openConnection();
					}catch(FileNotFoundException e)
					{
						e.printStackTrace();
						logger.error(urlBuffer.toString()+" not found!!");
						status=new Status(Status.STATUS_ERROR,"Script file not found "+urlBuffer.toString());
						return status;
					}
					File temp=File.createTempFile("tempSqlFile",".sql");
					InputStreamHandler.writeBufferedInputStreamToFile(conn.getInputStream(),temp.getAbsolutePath());
					logger.debug(temp.getAbsolutePath());
					sqlFilePath=temp.getAbsolutePath();
					logger.info("Executing sql file :"+scriptName);
					String shFilePath = this.getRuntimeContext().getETLServicesDirectory() + "/" + CommonConstants.DML_SCRIPT;
					StringBuilder execute = new StringBuilder();
					execute.append(sqlFilePath).append(CommonConstants.SPACE).append(redshift_pass)
							.append(CommonConstants.SPACE).append(redshift_host).append(CommonConstants.SPACE)
							.append(redshift_db).append(CommonConstants.SPACE).append(redshift_user);
				
					String fileOutput = ExecuteCommand.executeCommands(shFilePath, execute.toString());
					output.append(fileOutput);
					logger.debug("Deleting temporary file ");
					String result=(temp.delete())?new String("Successfully done"):new String("Unalbe to delete!!");
					logger.debug(result);
				}
				status = (output.toString().contains("ERROR")) ? new Status(Status.STATUS_ERROR,"ScriptLiteExecution Failed!!") : new Status(Status.STATUS_SUCCESS,"Success in executing ScriptLite");
				status.setMessage(output.toString());
				logger.info(status.getMessage());
			}
			else
			{
				logger.error("Invalid Environmnet provided:"+environment);
				status=new Status(Status.STATUS_ERROR,"Invalid environment provided "+environment);
				return status;
			}
		}catch(Exception e)
		{
			e.printStackTrace();
			status=new Status(Status.STATUS_ERROR,"ScriptLite Executor Failed");
			return status;
		}
		
		return status;
	}
	/**
	 * Loads environment specific properties
	 * @param environment	Selected environment beta,qa,prod
	 * @return Properties object
	 * @throws MalformedURLException 
	 * @throws ClassNotFoundException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private Properties loadEnvironmentProperties(String environment,String projectName) throws ClassNotFoundException, FileNotFoundException, IOException
	{
		Properties props=new Properties();
		StringBuffer urlBuffer=new StringBuffer();
		urlBuffer.append("jar:file:").append(CommonConstants.SCRIPTLITE_DIR).append("/").append(projectName)
				  .append("!/env_configs/").append(environment).append(".properties");
		URL url= new URL(urlBuffer.toString());
		JarURLConnection conn = (JarURLConnection)url.openConnection();
		props.load(conn.getInputStream());
		return props;
	}
}
