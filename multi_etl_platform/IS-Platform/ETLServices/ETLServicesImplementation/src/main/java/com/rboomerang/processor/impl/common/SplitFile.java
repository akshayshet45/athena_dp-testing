package com.rboomerang.processor.impl.common;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

public class SplitFile extends AbstractService {
	private static final Logger logger = LoggerFactory.getLogger(SplitFile.class);

	//Elements specific to this code:
	private static String COLUMN_VAL="columnValue";

	public SplitFile(String clientSpecpath, String processorSpecPath) throws Exception {
		super(clientSpecpath, processorSpecPath);
		logger.info("SplitFile constructor called");
	}
	public SplitFile(String clientSpecpath, String processorSpecPath,String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath,runtimeDate);
		logger.info("SplitFile constructor called");
	}

	public SplitFile(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext,String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath,runtimeContext, runtimeDate);
		logger.info("SplitFile constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject processorSpec)
			throws JSchException, SftpException, Exception {

		//obtaining Common elements from processor spec list 
		JSONObject list = (JSONObject)processorSpec.get(CommonConstants.LIST);
		JSONObject source = (JSONObject)list.get(CommonConstants.SOURCE);
		JSONObject dest = (JSONObject)list.get(CommonConstants.DESTINATION);
		String sourceEndPoint = (String)source.get(CommonConstants.ENDPOINTNAME);
		logger.debug("Input File Source :: "+sourceEndPoint);
		String destEndPoint = (String)dest.get(CommonConstants.ENDPOINTNAME);
		logger.debug("Output File destination :: "+destEndPoint);
		String sourceDateSuffix = (String) source.get(CommonConstants.FEED_DATE);
		String destDateSuffix = (String) dest.get(CommonConstants.FEED_DATE);

		if ( sourceDateSuffix == null || sourceDateSuffix.isEmpty())
			sourceDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
		if (destDateSuffix == null || destDateSuffix.isEmpty() )
			destDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
		String leftOverDataFileName =null;
		String skipRows = (String)list.get("skipRows");
		if (skipRows==null) {
			skipRows="yes";			
		}
		else if (skipRows.equalsIgnoreCase("no")) {
			leftOverDataFileName=(String)list.get("leftOverDataFileName");
			leftOverDataFileName = replaceDateTag(leftOverDataFileName, destDateSuffix);
		}
		
		String delimiter = (String)list.get(CommonConstants.DELIMITER);
		logger.debug("Delimiter :: \'"+delimiter+"\'");
		String sourcePath = (String)source.get(CommonConstants.PATH);
		logger.debug("Input File Source :: "+sourcePath);
		String destPath = (String)dest.get(CommonConstants.PATH);
		logger.debug("Output File destination :: "+destPath);
		sourcePath = replaceDynamicFolders(sourcePath, sourceDateSuffix);
		logger.debug("Input File dynamic Path :: "+sourcePath);
		destPath  = replaceDynamicFolders(destPath, destDateSuffix);
		logger.debug("Output File dynamic Path :: "+destPath);
		//replacing feedDate from fileName
		String sourceFile=(String)source.get(CommonConstants.FILE_NAME);
		sourceFile = replaceDateTag(sourceFile, sourceDateSuffix);
		logger.debug("Source File Name : "+sourceFile);
		
		String destFileCompression = (String) dest.get(CommonConstants.COMPRESSION);

		//obtaining elements from list that are specific to this class
		String splitColumn =(String) list.get("columnNameToSplitFileOn");
		JSONArray fileSpec=(JSONArray) dest.get("fileSpec");
		HashMap<String, String> columnAndFileName = new HashMap<String, String>();

		//Adding the columnvalue and its respective filename to hashmap
		int columnValueCount=0;
		for(columnValueCount=0;columnValueCount<fileSpec.size();columnValueCount++)
		{
			JSONObject fileSpecObject = (JSONObject)fileSpec.get(columnValueCount);
			String fileNameForColumn = (String)fileSpecObject.get(CommonConstants.FILE_NAME);
			fileNameForColumn = replaceDateTag(fileNameForColumn, destDateSuffix);
			//fileName will be added after replacing feed date
			logger.debug("Column Value :: "+(String)fileSpecObject.get(COLUMN_VAL)+" :: and filename :: "+fileNameForColumn);
			columnAndFileName.put((String)fileSpecObject.get(COLUMN_VAL),fileNameForColumn);
		}

		//getting source endpoint properties		
		String sourceType = super.getTypeOfEndPoint(clientSpec, sourceEndPoint);
		HashMap<String, String> sourceEndPointResourceProperties = super.buildResourceProperties(sourceEndPoint, clientSpec,sourceType);

		//making connection with source endpoint
		IEndPointResource sourceEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(
				sourceType, sourceEndPointResourceProperties);
		if (sourceType.equals("sftp"))
			sourceFile = makeRegex(sourceFile);
		
		//downloading the sourceFile
		InputStream iStream = sourceEndPointResource.getFile(sourcePath, sourceFile);
		File leftOverDataFile = File.createTempFile("remainingData", ".tmp");
		for(String localFileName:columnAndFileName.values())
		{
			//before processing need to remove file if exists locally
			if((new File("/tmp/"+localFileName)).delete())
				logger.debug("local file --> "+localFileName+" : exists and deleted successfully");
			else
				logger.debug("local file --> "+localFileName+" : not found ");
		}
		try{
		processInputStream(iStream,columnValueCount,splitColumn,columnAndFileName,delimiter,skipRows,leftOverDataFile);
		}catch(Exception e)
		{
			logger.error("Exception occured in processing files ",e);
			return new Status(Status.STATUS_ERROR, "Error : "+e.getMessage()+" occured in processing "+sourceFile+" at path "+sourcePath+" at endpoint"+sourceEndPoint);
		}

		sourceEndPointResource.closeConnection();

		iStream.close();
		//getting destination end point properties
		String destType = getTypeOfEndPoint(clientSpec, destEndPoint);
		HashMap<String, String> destEndPointResourceProperties = super.buildResourceProperties(destEndPoint, clientSpec,destType);


		//uploading all files
		for(String localFileName:columnAndFileName.values())
		{
			File finalFile = new File("/tmp/"+localFileName);
			if(destFileCompression!=null && destFileCompression.equalsIgnoreCase("zip"))
			{
				logger.debug("compressing file to zip");
				finalFile=zip(finalFile, localFileName);
			}
			//making connection with source endpoint
			IEndPointResource destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(
					destType, destEndPointResourceProperties);

			destEndPointResource.writeFile(new FileInputStream(finalFile), destPath, localFileName);

			//after uploading file has to be deleted from local system or else running program next time will append new data to old one on file
			if((finalFile).delete())
				logger.info("local file from system --> "+localFileName+" : deleted successfully");
			else
				logger.info("unable to find and delete local file --> "+localFileName);
			destEndPointResource.closeConnection();
		}
		if(skipRows.equalsIgnoreCase("no")){
			if(destFileCompression!=null && destFileCompression.equalsIgnoreCase("zip"))
			{
				logger.debug("compressing file to zip");
				leftOverDataFile=zip(leftOverDataFile, leftOverDataFileName);
			}
			
			
			IEndPointResource destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(
					destType, destEndPointResourceProperties);

			destEndPointResource.writeFile(new FileInputStream(leftOverDataFile), destPath, leftOverDataFileName);
			leftOverDataFile.delete();
			destEndPointResource.closeConnection();
		}

		return new Status(Status.STATUS_SUCCESS, "Split File Success on file "+sourceFile+" output files are placed at "+destPath+ " at "+destEndPoint);
	}

	private static void processInputStream(InputStream inputStream,int columnValueCount,String splitColumn,
			HashMap<String, String> columnAndFileName,String delimiter,String skipRows,File leftOverDataFile) throws Exception
	{
		logger.info("Input Stream recieved successfully, now starting to process input stream !!");
		int rowCount=0;
		int rowSkipped=0;
		int columnIndex = -1;
		BufferedReader reader=new BufferedReader(new InputStreamReader( inputStream , "UTF-8" ));

		String headerLine = reader.readLine();
		rowCount++;

		String headers[] = headerLine.split(delimiter);
		for(int x=0;x<headers.length;x++)
		{
			if(headers[x].equals(splitColumn))
				columnIndex=x;
		}
		if(columnIndex==-1)
		{
			throw new Exception("File Containes no Such header :: "+splitColumn);
		}
		logger.info(" Column number :: "+(columnIndex+1)+" --> "+splitColumn+" , is going to be removed from csv!! ");
		//writing headers to all files from hashmap
		for(String fileName:columnAndFileName.values())
		{
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/tmp/"+fileName,true));
			bufferedWriter.write(headerLine+"\n");
			bufferedWriter.close();
		}
		BufferedWriter bufferedWriter1 = new BufferedWriter(new FileWriter(leftOverDataFile,true));
		bufferedWriter1.write(headerLine+"\n");
		bufferedWriter1.close();
		String row=null;
		try
		{
			row = reader.readLine();
			rowCount++;
		}
		catch(NullPointerException e)
		{
			throw new Exception("File contains only headers");
		}
		boolean quoteFlag = false;
		String[] rowElements;
		String outputFileName;
		while(row!=null)
		{
			//splitting the row to its elements
			if(row.contains("\""))
			{	
				//				logger.info("CSV contains quotes!!");
				quoteFlag=true;//for checking if the csv is having quotes
				rowElements=processQuotes(row,delimiter);
			}
			else
				//				logger.info("CSV does not contain quotes!!");
				rowElements=row.split(delimiter);

			//for testing only
			if(!columnAndFileName.containsKey(rowElements[columnIndex]))
			{
				if(skipRows.equalsIgnoreCase("no")){
					BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(leftOverDataFile,true));
					for(int i=0;i<rowElements.length;i++)
					{
						if(quoteFlag)
							rowElements[i]="\""+rowElements[i]+"\"";
						bufferedWriter.write(rowElements[i]);
						if(i<rowElements.length-1)
							bufferedWriter.write(delimiter);
					}
					bufferedWriter.write("\n");
					bufferedWriter.close();
					
				}else{
				rowSkipped++;
				}
				row=reader.readLine();
				rowCount++;
				continue;
			}
			//			else
			//				logger.info("Adding row to file :: "+outputFileName);

			//getting fileName for that particular row
			outputFileName=columnAndFileName.get(rowElements[columnIndex]);




			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/tmp/"+outputFileName,true));
			for(int i=0;i<rowElements.length;i++)
			{
				if(quoteFlag)
					rowElements[i]="\""+rowElements[i]+"\"";
				bufferedWriter.write(rowElements[i]);
				if(i<rowElements.length-1)
					bufferedWriter.write(delimiter);
			}
			bufferedWriter.write("\n");
			bufferedWriter.close();


			row=reader.readLine();
			rowCount++;
		}
		logger.debug("Total Number of Rows Processed ::: "+rowCount);
		logger.debug("Total Number of Rows Skipped ::: "+rowSkipped);

		reader.close();
		inputStream.close();
	}

	private static String[] processQuotes(String row,String delimiter) {

		String[] rowElements=row.split("\""+delimiter+"\"");
		for(int i=0;i<rowElements.length;i++)
		{
			rowElements[i]=(rowElements[i]).replaceAll("[\"]","" );
		}
		return rowElements;
	}

}
