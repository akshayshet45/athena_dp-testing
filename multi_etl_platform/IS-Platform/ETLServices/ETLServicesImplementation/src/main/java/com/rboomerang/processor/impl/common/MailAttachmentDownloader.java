package com.rboomerang.processor.impl.common;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.InputStreamHandler;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

public class MailAttachmentDownloader extends AbstractService {

	private static Logger logger = LoggerFactory.getLogger(MailAttachmentDownloader.class);;
	private static final Status SUCCESS = new Status(Status.STATUS_SUCCESS,
			"Execution MailAttachmentDownloader successful");
	public static final String MAIL_FILTER = "mailFilter";
	public static final String AFTER_DATE = "afterDate";
	public static final String ATTACHMENT_FILE_NAME = "attachmentFileName";
	public static final String SUBJECT = "subjectContains";

	public MailAttachmentDownloader(String clientSpecpath, String processorSpecPath) throws Exception {
		super(clientSpecpath, processorSpecPath);
		logger.debug("MailAttachmentDownloader constructor called");
	}

	public MailAttachmentDownloader(String clientSpecpath, String processorSpecPath, String runtimeDate)
			throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeDate);
		logger.debug("MailAttachmentDownloader constructor called");
	}

	public MailAttachmentDownloader(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext,
			String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.debug("MailAttachmentDownloader constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject processorSpec)
			throws JSchException, SftpException, Exception {

		// accessing list elements from processor spec
		JSONObject list = (JSONObject) processorSpec.get(CommonConstants.LIST);
		JSONObject mailFilter = (JSONObject) list.get(MailAttachmentDownloader.MAIL_FILTER);
		JSONObject dest = (JSONObject) list.get(CommonConstants.DESTINATION);

		// elements to filter mail search in gmail.

		String sourceDateSuffix = (String) mailFilter.get(CommonConstants.FEED_DATE);
		String destDateSuffix = (String) dest.get(CommonConstants.FEED_DATE);

		if (sourceDateSuffix == null || sourceDateSuffix.isEmpty())
			sourceDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
		if (destDateSuffix == null || destDateSuffix.isEmpty())
			destDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();

		String downloadType = (String) mailFilter.get("type");
		if (downloadType == null)
			downloadType = "attachment";

		String attachmentName = null;
		if (downloadType.equalsIgnoreCase("attachment")) {
			attachmentName = (String) mailFilter.get(MailAttachmentDownloader.ATTACHMENT_FILE_NAME);
			attachmentName = super.replaceDateTag(attachmentName, sourceDateSuffix);
		}
		String mailFrom = (String) mailFilter.get("from");
		String mailSubject = (String) mailFilter.get(MailAttachmentDownloader.SUBJECT);
		mailSubject = super.replaceDateTag(mailSubject, sourceDateSuffix);

		// getting Destination Endpoint details
		String destEndPoint = (String) dest.get(CommonConstants.ENDPOINTNAME);
		logger.info("Output File destination Endpoint:: " + destEndPoint);
		// getting destination file path from spec file and replacing date month
		// or year from it!
		String destPath = super.replaceDynamicFolders((String) dest.get(CommonConstants.PATH), destDateSuffix);
		// getting destination file path from spec file and replacing feedDate
		// from it!
		String destFileName = super.replaceDateTag((String) dest.get(CommonConstants.FILE_NAME), destDateSuffix);
		logger.info("Output File destination --> " + destPath + " :: and FileName --> " + destFileName);

		// creating temporary file to download file from mail
		File sourceDownloadedFile = File.createTempFile("sourceFile", ".tmp");
		logger.debug("downloaded file : " + sourceDownloadedFile.getAbsolutePath());

		String emailAddress = (String) list.get(CommonConstants.MAIL_ADDRESS);
		String password = (String) list.get(CommonConstants.MAIL_PASSWORD);

		// if not specified in processor spec it will automatically take
		// data@boomerangcommerce.com as default email id
		if (emailAddress == null) {
			emailAddress = CommonConstants.EMAIL_ID;
			password = CommonConstants.EMAIL_PASSWORD;
			logger.debug("No Email Specified, taking " + emailAddress + " as default");
		}

		String mailDateFilter = super.replaceDateTag(ClientSpec.DATE_FORMAT_REGEX,
				(String) mailFilter.get(MailAttachmentDownloader.AFTER_DATE));
		// for filtering the mail,processor stops reading mails if their date
		// exceeds this value
		logger.info("searching for mails after date " + mailDateFilter);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

		// smtp configuration.
		Properties connectionProperties = new Properties();
		connectionProperties.put("mail.smtp.host", "smtp.gmail.com");
		connectionProperties.put("mail.smtp.port", "465");
		connectionProperties.put("mail.smtp.socketFactory.port", "465");
		connectionProperties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		connectionProperties.put("mail.smtp.auth", "true");

		// used to create new instance and not the default instance so that it
		// does'nt create problem for Abstract processor to send mail with other
		// configuration
		Session mailDownloadSession = Session.getInstance(connectionProperties, null);
		// Session mailDownloadSession = Session.getDefaultInstance();
		Store store = mailDownloadSession.getStore("imaps");
		store.connect("smtp.gmail.com", emailAddress, password);
		logger.debug("Connection made to smtp");
		Folder inbox = store.getFolder("inbox");
		logger.debug("Fetching Inbox");
		// this helps in keeping the mail unread even after downloading the
		// attachment
		inbox.open(Folder.READ_ONLY);
		Message[] messages = inbox.getMessages();
		logger.debug("Mail Count : " + messages.length);
		logger.debug("Mail Download type " + downloadType);
		for (int i = messages.length - 1; i >= 0; i--) {
			Date mailDate = messages[i].getReceivedDate();
			// logger.info("going through Mails of date "+mailDate);
			if (mailDate.after(dateFormat.parse(mailDateFilter)))// checks when
																	// to break
																	// the code
																	// and not
																	// search
																	// after
																	// some date
			{
				try {
					if (messages[i].getSubject().contains(mailSubject)
							&& messages[i].getFrom()[0].toString().contains(mailFrom)) {
						logger.debug("Mail Recieved on : " + messages[i].getReceivedDate());
						logger.debug("Mail from : " + messages[i].getFrom()[0].toString());
						logger.debug("Mail Subject " + i + " :- " + messages[i].getSubject());

						if (downloadType.equalsIgnoreCase("attachment")) {
							if (checkForAttachmentInMail(messages[i], attachmentName,
									sourceDownloadedFile.getAbsolutePath())) {
								logger.debug("attachment downloaded successfully : " + attachmentName);
								break;
							}
						} else {
							String destType = getTypeOfEndPoint(clientSpec, destEndPoint);
							HashMap<String, String> destEndPointResourceProperties = super.buildResourceProperties(
									destEndPoint, clientSpec, destType);
							if (checkForInLineInMail((messages[i]), sourceDownloadedFile.getAbsolutePath(),destEndPointResourceProperties,destType,destPath)) {
								logger.debug("inline downloads successful");
								break;

							}
						}
					}
				} catch (NullPointerException ne) {
					logger.debug("Null pointer exception while reading  mail no.: " + i);
					ne.printStackTrace();
				}
			} else {
				// throw new Exception("No New mails having following attachment
				// : "+attachmentName+" from "+mailFrom+" after
				// "+mailDateFilter);
				return new Status(Status.STATUS_ERROR, "No New mails having following attachment : " + attachmentName
						+ " from " + mailFrom + " after " + mailDateFilter);
			}
		}

		inbox.close(false);
		store.close();
		logger.debug("Mail Connection Closed!!");

		// getting destination end point properties
		String destType = getTypeOfEndPoint(clientSpec, destEndPoint);
		HashMap<String, String> destEndPointResourceProperties = super.buildResourceProperties(destEndPoint, clientSpec,
				destType);

		// making connection with source endpoint
		IEndPointResource destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(destType,
				destEndPointResourceProperties);

		// uploading encrypted file
		destEndPointResource.writeFile(new FileInputStream(new File(sourceDownloadedFile.getAbsolutePath())), destPath,
				destFileName);
		destEndPointResource.closeConnection();

		// deleting all temporary files
		sourceDownloadedFile.delete();

		return SUCCESS;
	}

	private boolean checkForInLineInMail(Message mail, String localFile,HashMap<String, String> destEndPointResourceProperties,String destType,String destPath) throws Exception {
		Boolean foundFlag = false;
		BufferedWriter writer = new BufferedWriter(new FileWriter(localFile, true));
//		logger.debug("checking for inline in mail");
		logger.info("Content type : " + mail.getContentType());
		if (mail.getContentType().equalsIgnoreCase(("TEXT/PLAIN; charset=UTF-8"))
				|| mail.getContentType().equalsIgnoreCase("TEXT/HTML; charset=UTF-8")) {
//			logger.info("Mail content" + mail.getContent());
			writer.write(mail.getContent() + "\n");

		}
		if (mail.getContentType().contains(("IMAGE/PNG"))) {
			DataHandler handler = mail.getDataHandler();
			logger.debug("inline File Name : " + handler.getName());

			// making connection with source endpoint
			IEndPointResource destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(destType,
					destEndPointResourceProperties);

			// uploading encrypted file
			destEndPointResource.writeFile(handler.getInputStream(), destPath, handler.getName());
			destEndPointResource.closeConnection();

			foundFlag = true;
		}
		String disposition = mail.getDisposition();
		logger.debug("checking disposition in mail "+disposition);
		if (disposition != null && (disposition.equalsIgnoreCase("inline"))) {
			DataHandler handler = mail.getDataHandler();
			logger.debug("inline File Name : " + handler.getName());

			// making connection with source endpoint
			IEndPointResource destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(destType,
					destEndPointResourceProperties);

			// uploading encrypted file
			destEndPointResource.writeFile(handler.getInputStream(), destPath, handler.getName());
			destEndPointResource.closeConnection();

			foundFlag = true;
		}
		if (mail.isMimeType("multipart/*")) {
			Multipart multipart = (Multipart) mail.getContent();
			for (int j = 0; j < multipart.getCount(); j++) {
				BodyPart bodyPart = multipart.getBodyPart(j);
				if (checkForInlineInBody(bodyPart, localFile,destEndPointResourceProperties,destType,destPath)) {
					foundFlag = true;
				}
			}

		}
		writer.close();
		return foundFlag;
	}

	private static boolean checkForInlineInBody(BodyPart mail, String localFile,HashMap<String, String> destEndPointResourceProperties,String destType,String destPath)
			throws Exception {
		BufferedWriter writer = new BufferedWriter(new FileWriter(localFile, true));
		Boolean foundFlag = false;
//		logger.debug("checking for inline in mail body");
		logger.info("Content type : " + mail.getContentType());
		if (mail.getContentType().equalsIgnoreCase(("TEXT/PLAIN; charset=UTF-8"))
				|| mail.getContentType().equalsIgnoreCase("TEXT/HTML; charset=UTF-8")) {
//			logger.info("Mail content" + mail.getContent());
			writer.write(mail.getContent() + "\n");
		}
		if (mail.getContentType().contains(("IMAGE/PNG"))) {
			DataHandler handler = mail.getDataHandler();
			logger.debug("inline File Name : " + handler.getName());

			// making connection with source endpoint
			IEndPointResource destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(destType,
					destEndPointResourceProperties);

			// uploading encrypted file
			destEndPointResource.writeFile(handler.getInputStream(), destPath, handler.getName());
			destEndPointResource.closeConnection();

			foundFlag = true;
		}
		String disposition = mail.getDisposition();
		logger.debug("checking disposition in body "+disposition);
		if (disposition != null && (disposition.equalsIgnoreCase("inline"))) {
			DataHandler handler = mail.getDataHandler();
			logger.debug("inline File Name : " + handler.getName());
			// making connection with source endpoint
			IEndPointResource destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(destType,
					destEndPointResourceProperties);

			// uploading encrypted file
			destEndPointResource.writeFile(handler.getInputStream(), destPath, handler.getName());
			destEndPointResource.closeConnection();

			foundFlag = true;
		}
		if (mail.isMimeType("multipart/*")) {
			Multipart multipart = (Multipart) mail.getContent();
			for (int j = 0; j < multipart.getCount(); j++) {
				BodyPart bodyPart = multipart.getBodyPart(j);
				if (checkForInlineInBody(bodyPart, localFile,destEndPointResourceProperties,destType,destPath)) {
					foundFlag = true;
				}
			}

		}
		writer.close();
		return foundFlag;
	}

	// for multipart mails (mostly the ones that are forwarded) check for
	// attachment in mail and in body methods have been seperated.
	private static boolean checkForAttachmentInMail(Message mail, String attachmentName, String localFile)
			throws MessagingException, IOException {
		Boolean foundFlag = false;
		String disposition = mail.getDisposition();
		if (disposition != null && (disposition.equalsIgnoreCase("ATTACHMENT"))) {
			DataHandler handler = mail.getDataHandler();
			logger.debug("\nattachment name searching for : " + attachmentName + "\nFile Name : " + handler.getName());
			if (handler.getName().equals(attachmentName)) {
				InputStreamHandler.writeBufferedInputStreamToFile(handler.getInputStream(), localFile);
				foundFlag = true;
			}
		} else if (mail.isMimeType("multipart/*")) {
			Multipart multipart = (Multipart) mail.getContent();
			for (int j = 0; j < multipart.getCount(); j++) {
				BodyPart bodyPart = multipart.getBodyPart(j);
				if (checkForAttachmentInBody(bodyPart, attachmentName, localFile)) {
					foundFlag = true;
				}
			}

		}
		return foundFlag;
	}

	// for multipart mails (mostly the ones that are forwarded) check for
	// attachment in mail and in body methods have been seperated.
	private static boolean checkForAttachmentInBody(BodyPart mail, String attachmentName, String localFile)
			throws MessagingException, IOException {
		Boolean foundFlag = false;
		String disposition = mail.getDisposition();
		if (disposition != null && (disposition.equalsIgnoreCase("ATTACHMENT"))) {
			DataHandler handler = mail.getDataHandler();
			logger.debug("\nattachment name searching for : " + attachmentName + "\nFile Name : " + handler.getName());
			if (handler.getName().equals(attachmentName)) {
				InputStreamHandler.writeBufferedInputStreamToFile(handler.getInputStream(), localFile);
				foundFlag = true;
			}
		} else if (mail.isMimeType("multipart/*")) {
			Multipart multipart = (Multipart) mail.getContent();
			for (int j = 0; j < multipart.getCount(); j++) {
				BodyPart bodyPart = multipart.getBodyPart(j);
				if (checkForAttachmentInBody(bodyPart, attachmentName, localFile)) {
					foundFlag = true;
				}
			}

		}
		return foundFlag;
	}

}
