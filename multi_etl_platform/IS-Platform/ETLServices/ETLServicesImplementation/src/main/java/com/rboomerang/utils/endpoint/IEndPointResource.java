package com.rboomerang.utils.endpoint;

import java.io.InputStream;
import java.util.List;


// Interface to provide a common API layer irrespective of the various endpoints like SFTP, S3 etc
public interface IEndPointResource {
	public boolean checkFileExistence(String baseDirectory, String fileName) throws Exception;
	public InputStream getFile(String endpointFilePath,String endpointFileName) throws Exception;
	public void writeFile(InputStream inputStream,String endpointFilePath,String endpointFileName) throws Exception;
	public void closeConnection() throws Exception;
	public String getFileHeaderLine(String location, String fileName) throws Exception;
	public List<String> getFileList(String sftpFilePath,String sftpFileName) throws Exception;
}