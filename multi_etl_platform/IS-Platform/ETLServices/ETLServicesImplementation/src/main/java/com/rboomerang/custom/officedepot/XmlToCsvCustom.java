package com.rboomerang.custom.officedepot;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.InputStreamHandler;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XmlToCsvCustom extends AbstractService {

	private static final Logger logger = LoggerFactory.getLogger(XmlToCsvCustom.class);
	
	public XmlToCsvCustom(String clientSpecpath, String processorSpecPath) throws Exception
	{
		super(clientSpecpath, processorSpecPath);
		logger.info("XmlToCsvCustom processor constructor called");
	}
	public XmlToCsvCustom(String clientSpecpath, String processorSpecPath, String runtimeDate) throws Exception
	{
		super(clientSpecpath, processorSpecPath, runtimeDate);
		logger.info("XmlToCsvCustom constructor called");
	}
	public XmlToCsvCustom(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext,
			String runtimeDate) throws Exception 
	{
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.info("XmlToCsvCustom constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject serviceSpec)  throws JSchException, SftpException, Exception 
	{
		Status status=new Status(Status.STATUS_SUCCESS,"Success in converting xml to csv");
		//Reading source file (xml) endpoint properties
		JSONObject sourceParams= (JSONObject)serviceSpec.get(CommonConstants.SOURCE);
		JSONObject destParams= (JSONObject)serviceSpec.get(CommonConstants.DESTINATION);
		String sourceEndPoint=sourceParams.get(CommonConstants.ENDPOINTNAME).toString();
		String sourceLocation=sourceParams.get(CommonConstants.LOCATION).toString();
		String sourceFileName=sourceParams.get(CommonConstants.FILE_NAME).toString();
		String sourceFeedDate=sourceParams.get(CommonConstants.FEED_DATE).toString();
		String destEndPoint=destParams.get(CommonConstants.ENDPOINTNAME).toString();
		String destLocation=destParams.get(CommonConstants.LOCATION).toString();
		String destFeedDate=destParams.get(CommonConstants.FEED_DATE).toString();
		String destFileName=destParams.get(CommonConstants.FILE_NAME).toString();
		if(destFeedDate==null || destFeedDate.isEmpty())
		{
			destFeedDate=clientSpec.getDateFormat()+CommonConstants.COMMA+clientSpec.getDaysAgo();
		}
		if(sourceFeedDate==null || sourceFeedDate.isEmpty())
		{
			sourceFeedDate=clientSpec.getDateFormat()+CommonConstants.COMMA+clientSpec.getDaysAgo();
		}
		sourceFileName=replaceDateTag(sourceFileName,sourceFeedDate);
		destFileName=replaceDateTag(destFileName,destFeedDate);
		IEndPointResource sourceEndPointResource=null;
		IEndPointResource destEndPointResource=null;
		try
		{
			logger.debug("Reading source end point details..");
			String sourceType = super.getTypeOfEndPoint(clientSpec, sourceEndPoint);
			HashMap<String, String> sourceEndPointResourceProperties = super.buildResourceProperties(sourceEndPoint, clientSpec,sourceType);
			sourceEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(sourceType, sourceEndPointResourceProperties);
			logger.debug("Downloading file "+sourceFileName);
			InputStream sin=sourceEndPointResource.getFile(sourceLocation, sourceFileName);
			File tempFile=File.createTempFile("temp", ".csv");
			InputStreamHandler.writeInputStreamToFile(sin, tempFile.getAbsolutePath());
			logger.info("Staring to Convert XML file to CSV File");
			SAXParserFactory spfac = SAXParserFactory.newInstance();
			SAXParser sp = spfac.newSAXParser();
			XmlParser handler = new XmlParser();
			sp.parse(tempFile.getAbsolutePath(), handler);
			File tempResult=File.createTempFile("tempResult",".csv");
			handler.readList(tempResult.getAbsolutePath());
			logger.info("XML to CSV file conversion Completed");
			logger.debug("Uploading file to the endpoint");
			String destType = getTypeOfEndPoint(clientSpec, destEndPoint);
			HashMap<String, String> destEndPointResourceProperties = super.buildResourceProperties(destEndPoint, clientSpec,destType);
			destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(destType, destEndPointResourceProperties);
			InputStream out=new FileInputStream(new File(tempResult.getAbsolutePath()));
			destEndPointResource.writeFile(out, destLocation, destFileName);
			destEndPointResource.closeConnection();
			sourceEndPointResource.closeConnection();
			logger.debug("Done!! uploaded to specific endpoint");
			logger.debug("Deleting temporary files");
			if(tempFile.delete())
			{
				logger.debug("Deleted temporary file!!");
			}
			else
			{
				logger.warn("Failed to delete temporary file");
			}
		}
		catch(Exception e)
		{
			logger.error("Found Exception : "+ e);
			status=new Status(Status.STATUS_ERROR,"Failure in converting xml to csv");
		}
		
		return status;
	}
	
}

class XmlParser extends DefaultHandler
{
	private ProductUtility prod;
	private String temp;
	private String model;
	private boolean foundModel=false;
	private boolean setModel=true;
	private ArrayList<ProductUtility> accList = new ArrayList<ProductUtility>();
	public XmlParser()
	{
	}
	/*
	 * When the parser encounters plain text (not XML elements),
	 * it calls(this method, which accumulates them in a string buffer
	 */
	public void characters(char[] buffer, int start, int length)
	{
		temp = new String(buffer, start, length);
	}
	/*
	 * Every time the parser encounters the beginning of a new element,
	 * it calls this method, which resets the string buffer
	 */ 
	public void startElement(String uri, String localName,
			String qName, Attributes attributes) throws SAXException
	{
		temp = "";
		if (qName.equalsIgnoreCase("PRODUCT"))
		{
			prod = new ProductUtility();
			prod.setsku(attributes.getValue("ID"));
			foundModel=false;
			setModel=true;
		}
		if(qName.equalsIgnoreCase("ATTRIBUTE"))
		{
			model=attributes.getValue("NAME");
			if(model.equals("ManufacturerPartNo"))
			{
				foundModel=true;
			}
		}
	}

	/*
	 * When the parser encounters the end of an element, it calls this method
	 */
	public void endElement(String uri, String localName, String qName)
			throws SAXException
	{

		if (qName.equalsIgnoreCase("PRODUCT")) {
			// add it to the list
			accList.add(prod);

		} else if (qName.equalsIgnoreCase("PRODUCTNAME")) {
			prod.setproductname(temp);
		} else if (qName.equalsIgnoreCase("LISTPRICE")) {
			prod.setlistprice(temp);
		} else if (qName.equalsIgnoreCase("SALEPRICE")) {
			prod.setsalesprice(temp);
		} else if (qName.equalsIgnoreCase("PRODUCTURL")) {
			prod.setProducturl(temp);
		}
		else if (qName.equalsIgnoreCase("IMAGEURL")) {
			prod.setImage_link(temp);
		}
		else if (qName.equalsIgnoreCase("BRAND")) {
			prod.setBrand(temp);
		}
		else if (qName.equalsIgnoreCase("PRODUCTDESCRIPTION")) {
			prod.setDescription(temp);
		}
		else if (foundModel && setModel)
		{	  prod.setModel(temp);
		setModel=false;
		}
	}

	void readList(String destFileName) throws IOException
	{
		FileWriter fileWriter=null;
		String filename=destFileName;
		String FILE_HEADER="\"sku\",\"title\",\"listprice\",\"salesprice\",\"producturl\",\"imageurl\",\"brand\",\"model\",\"productdescription\"";
		System.out.println("No of  the Products in Feed '" + accList.size()  + "'.");
		Iterator<ProductUtility> it = accList.iterator();
		fileWriter = new FileWriter(filename);
		fileWriter.append(FILE_HEADER.toString());
		fileWriter.append("\n");
		while (it.hasNext())
		{
			write_to_file(fileWriter,it.next());
		}
		fileWriter.flush();
		fileWriter.close();
	}
	public void write_to_file(FileWriter fw,ProductUtility p) throws IOException
	{
		//final String COMMA_DELIMITER = ",";
		final String NEW_LINE_SEPARATOR = "\n";
		fw.append(p.toString().toString().replace("\t", "").trim());
		fw.append(NEW_LINE_SEPARATOR);
	}
}
