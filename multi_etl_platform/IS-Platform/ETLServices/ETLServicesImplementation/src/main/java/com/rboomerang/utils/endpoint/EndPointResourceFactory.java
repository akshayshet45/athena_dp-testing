package com.rboomerang.utils.endpoint;

import java.util.HashMap;

import com.rboomerang.common.framework.javabeans.ClientSpec;

// Factory to get EndPointResource Implementation, given the type of the Endpoint
public class EndPointResourceFactory {
	public static EndPointResourceFactory instance = null;
	
	private EndPointResourceFactory() {
		// Private constructor
	}
	
	public static EndPointResourceFactory getInstance() {
		if(instance == null){
	        synchronized (EndPointResourceFactory.class) {
	            if(instance == null){
	                instance = new EndPointResourceFactory();
	            }
	        }
	    }
	    return instance;	
	}
	
	public IEndPointResource getEndPointResource(String type, HashMap<String, String> initProperties) throws Exception {
		// For any new EndPointResource added the appropriate if statement to return the specific class
		if (type.equals(ClientSpec.SFTP)) {
			return new SftpResourceEndPoint(initProperties);
		} else if (type.equals(ClientSpec.S3)) {
			return new S3ResourceEndPoint(initProperties);
		} 
		
		throw new Exception("No Endpoint Resource configured for the give type " + type);
	}
	
}
