package com.rboomerang.validator.impl.common;

import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.common.framework.javabeans.DbSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.SendEmail;
import com.rboomerang.utils.SqlHelper;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

public class TableValidationReporter extends AbstractService {
	private static Logger logger = LoggerFactory.getLogger(TableValidationReporter.class);
	private Status success = new Status(Status.STATUS_SUCCESS, "Execution Table validation reporter successful");
	private static final String NEW_LINE_SEPARATOR = "\n";

	public TableValidationReporter(String clientSpecpath, String validatorSpecPath) throws Exception {
		super(clientSpecpath, validatorSpecPath);
		logger.info("Table threashold validator constructor called");
	}

	public TableValidationReporter(String clientSpecpath, String validatorSpecPath, String runtimeDate)
			throws Exception {
		super(clientSpecpath, validatorSpecPath, runtimeDate);

		logger.info("Table threashold validator constructor called");
	}

	public TableValidationReporter(String clientSpecpath, String validatorSpecPath, RuntimeContext runtimeContext,
			String runtimeDate) throws Exception {
		super(clientSpecpath, validatorSpecPath, runtimeContext, runtimeDate);
		logger.info("Table threashold validator constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject validatorSpec)
			throws JSchException, SftpException, Exception {

		Object[] dbtype = getDbDetails((String) validatorSpec.get(CommonConstants.ENDPOINTNAME));
		DbSpec dbDetails = (DbSpec) dbtype[1];
		String runId = getExecid();

		String tableName = (String) validatorSpec.get(CommonConstants.TABLENAME);
		String validatorName = (String) validatorSpec.get(CommonConstants.VALIDATORNAME);
		String columnName = (String) validatorSpec.get(CommonConstants.COLUMNNAME);
		String errorRange = (String) validatorSpec.get(CommonConstants.ALERTRANGE);
		String validationType = (String) validatorSpec.get(CommonConstants.VALIDATORTYPE);
		String recipients = (String) validatorSpec.get(CommonConstants.RECIPIENT_LIST);
		String alertMessageSub = getMessageContent().get("warn").getSubject();
		String alertMessageText = getMessageContent().get("warn").getBody();

		String csvHeader = (String) validatorSpec.get(CommonConstants.PRIMARYKEY);

		JSONObject dest = (JSONObject) validatorSpec.get(CommonConstants.DESTINATION);
		String destDateFormat = (String) dest.get(CommonConstants.FEED_DATE);
		if (destDateFormat == null || destDateFormat.isEmpty())
			destDateFormat = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
		String destEndPoint = (String) dest.get(CommonConstants.ENDPOINTNAME);
		String destPath = replaceDynamicFolders((String) dest.get(CommonConstants.PATH), destDateFormat);
		String destFileName = replaceDateTag((String) dest.get(CommonConstants.FILE_NAME), destDateFormat);
		int threshold = 0;
		if (dbDetails==null) {
			logger.error("Please specify the end point");
			return new Status(Status.STATUS_ERROR, "End point is not specified properly");

		}
		if (tableName.isEmpty() || validatorName.isEmpty() || validationType.isEmpty() || errorRange.isEmpty()
				|| (!validationType.isEmpty() && validationType == "COLUMN_VALIDATION" && columnName.isEmpty())) {
			logger.error("One of the mandatory input in job spec JSON is empty ");
			return new Status(Status.STATUS_ERROR,
					"One of the mandatory input in validation reporter job spec JSON is empty ");
		}
		if (recipients.isEmpty()) {
			logger.error("There are no notification action defined : Please specify recipient email list");
			return new Status(Status.STATUS_ERROR,
					"There are no notification action defined : Please specify recipient email list and Re run the job");

		}

		if (errorRange.charAt(errorRange.length() - 1) == '%') {
			threshold = Integer.parseInt(errorRange.substring(0, errorRange.length() - 1));
		} else {
			threshold = Integer.parseInt(errorRange);
		}
		logger.info("db details: " + dbDetails.getDb() + "\t" + dbDetails.getHost() + "\t" + dbDetails.getPasswd()
				+ "\t" + dbDetails.getUname());

		String totalCountQuery = "select run_id,client,name,row_count_in,row_count_out from dp.dp_run_stats where run_id="
				+ runId + " and name='" + tableName + "'";

		StringBuilder invalidCountQuery = new StringBuilder("select primary_key from dp.validation_logs where run_id=")
				.append(runId).append(" and validation_type = '").append(validationType).append("' and table_name ='")
				.append(tableName).append("' and failed_validation_id ='").append(validatorName);

		if (validationType == "COLUMN_VALIDATION") {
			invalidCountQuery.append("' and col_name ='").append(columnName);
		}
		invalidCountQuery.append("'");
try{
		int finalCount = getFinalRowCount(dbDetails, totalCountQuery);
		int invalidRowCount = getInvalidRowReportAndCount(dbDetails, invalidCountQuery.toString(), csvHeader,
				clientSpec, destEndPoint, destPath, destFileName);

		if (invalidRowCount > 0) {
			StringBuilder errorDescription = new StringBuilder("\n\nNumber of invalid rows = ").append(invalidRowCount)
					.append("\nTotal rows inserted into table = " + finalCount)
					.append("\nValidation type = " + validationType).append("\nValidator name = " + validatorName)
					.append("\nTable name = " + tableName).append("\nInvalid rows are written to :" + getTypeOfEndPoint(clientSpec, destEndPoint))
					.append("\nLocation :" + destPath + destFileName);

			if (validationType == "COLUMN_VALIDATION") {
				errorDescription.append("Column name =" + columnName);
			}
			int invalidRowCountPctg = (invalidRowCount * 100) / finalCount;
			if (invalidRowCountPctg >= threshold) {
				// overwrite Error text
				alertMessageText = alertMessageText + "\nInvalid rows exceed the threshold of " + threshold
						+ "% Total of " + invalidRowCountPctg + " % invalid rows found.";
			}
			logger.info(alertMessageText + errorDescription);
			SendEmail.send(recipients, alertMessageSub, alertMessageText + errorDescription.toString());

		} else {
			logger.info("No invalid rows found for : \n Validation type = " + validationType + " Validator name ="
					+ validatorName + "Table name = " + tableName);

		}
}
catch(Exception e){
	e.printStackTrace();
	return new Status(Status.STATUS_ERROR, "Error while getting the validation report.Please check if validation tables and db exist");
}
		return this.success;
	}

	private int getFinalRowCount(DbSpec dbDetails, String totalCountQuery) throws ClassNotFoundException, SQLException {

		ResultSet rs = null;
		int finalRowCount = 0;

		logger.info("Connecting to database: \n Host = " + dbDetails.getHost() + "Database = " + dbDetails.getDb()
				+ "user = " + dbDetails.getUname() + "pass=" + dbDetails.getPasswd());
		SqlHelper redshift = new SqlHelper(dbDetails, "redshift");

		logger.info("Query: " + totalCountQuery);
		try {
			rs = redshift.getSelectedData(totalCountQuery);

			while (rs.next()) {
				finalRowCount = rs.getInt(5);
				logger.info("Final row count: " + finalRowCount);
				break;
			}
		} finally {
			rs.close();
		}

		return finalRowCount;
	}

	private int getInvalidRowReportAndCount(DbSpec dbDetails, String invalidCountQuery, String csvHeader,
			ClientSpec clientSpec, String destEndPoint, String destPath, String destFileName) throws Exception {
		ResultSet rs = null;
		StringBuilder csvFile = new StringBuilder(csvHeader);

		int invalidRowCount = 0;

		logger.info("Connecting to database: \n Host = " + dbDetails.getHost() + "Database = " + dbDetails.getDb()
				+ "user = " + dbDetails.getUname());
		SqlHelper redshift = new SqlHelper(dbDetails, "redshift");
		logger.info("Query: " + invalidCountQuery);
		try {
			rs = redshift.getSelectedData(invalidCountQuery.toString());

			while (rs.next()) {
				csvFile.append(NEW_LINE_SEPARATOR);
				csvFile.append(rs.getString(1)); // append invalid row in a new
													// line.
				invalidRowCount += 1;
			}
			if (invalidRowCount > 0) {
				InputStream in = IOUtils.toInputStream(csvFile.toString(), "UTF-8");
				pushInvalidRowsToFile(in, clientSpec, destEndPoint, destPath, destFileName);

			}
		} finally {
			rs.close();
		}
		return invalidRowCount;

	}

	private void pushInvalidRowsToFile(InputStream fileToBeWritten, ClientSpec clientSpec, String destEndPoint,
			String destPath, String destFileName) throws Exception {
		String destType;

		try {
			destType = getTypeOfEndPoint(clientSpec, destEndPoint);
			HashMap<String, String> destEndPointResourceProperties = super.buildResourceProperties(destEndPoint,
					clientSpec, destType);

			// making connection with source endpoint(SFTP/S3)
			IEndPointResource destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(destType,
					destEndPointResourceProperties);

			// uploading invalid rows to source endpoint(SFTP/s3)
			destEndPointResource.writeFile(fileToBeWritten, destPath, destFileName);
			destEndPointResource.closeConnection();
		} finally {

		}

	}

}
