package com.rboomerang.utils;

import java.io.File;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;

public class Converter {
	public static void xmltocsv(String xslFilePath, String xmlFilePath, String csvFilePath) throws Exception {
		File stylesheet = new File(xslFilePath);
		File xmlSource = new File(xmlFilePath);

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(xmlSource);

		StreamSource stylesource = new StreamSource(stylesheet);
		Transformer transformer = TransformerFactory.newInstance().newTransformer(stylesource);
		Source source = new DOMSource(document);
		Result outputTarget = new StreamResult(new File(csvFilePath));
		transformer.transform(source, outputTarget);
		
		
	}
	public static void xmltocsv(String xslFilePath,InputStream xmlInputStream, String csvFilePath) throws Exception {
		File stylesheet = new File(xslFilePath);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(xmlInputStream);

		StreamSource stylesource = new StreamSource(stylesheet);
		Transformer transformer = TransformerFactory.newInstance().newTransformer(stylesource);
		Source source = new DOMSource(document);
		Result outputTarget = new StreamResult(new File(csvFilePath));
		
		transformer.transform(source, outputTarget);
	}
}
