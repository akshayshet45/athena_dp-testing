package com.rboomerang.utils;

import java.io.IOException;
import java.security.InvalidParameterException;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.sns.model.CreateTopicResult;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;

public final class AmazonSnsHelper {
	
	private AmazonSNSClient amazonSNS;
	private String topicARN;
	
	private final Logger log = LoggerFactory.getLogger(AmazonSnsHelper.class);
	
	public AmazonSnsHelper(String snsName, String region) throws IOException {
		
		AWSAccess awsAccess = new AWSAccess();
		amazonSNS = new AmazonSNSClient(awsAccess.getAWSAccess());
		amazonSNS.setRegion((getRegion(region)));
		try {
            CreateTopicRequest createTopicRequest = new CreateTopicRequest(snsName);
            CreateTopicResult createTopicResult = amazonSNS.createTopic(createTopicRequest);
            topicARN=createTopicResult.getTopicArn();
        } catch (Exception e){
            log.error("Exception while creating SNSClient Object",e);
        }
		
		log.info("Topic ARN value: ",topicARN);
		
	}
	
	private Region getRegion(String region) {
		// TODO Auto-generated method stub
		Region _region = null;
		
		switch(region.toLowerCase()){
		case "us-east-1":
			_region=Region.getRegion(Regions.US_EAST_1);
			break;
		case "virginia":
			_region=Region.getRegion(Regions.US_EAST_1);
			break;
		case "us-west-1":
			_region=Region.getRegion(Regions.US_WEST_1);
			break;
		case "california":
			_region=Region.getRegion(Regions.US_WEST_1);
			break;
		case "us-west-2":
			_region=Region.getRegion(Regions.US_WEST_2);
			break;
		case "oregon":
			_region=Region.getRegion(Regions.US_WEST_2);
			break;
		default:
			throw new InvalidParameterException("Not a correct region selection");
		}
		
		return _region;
	}

	public boolean publish(JSONObject object){
		boolean status = true;
		try{
			PublishRequest publishRequest = new PublishRequest(topicARN, object.toString());
			PublishResult publishResult = amazonSNS.publish(publishRequest);
			log.info("MessageId - " + publishResult.getMessageId());
		}catch(Exception err){
			log.error("Error while publishing SNS Message", err);
			status=false;
		}
		return status;
	}
	
}
