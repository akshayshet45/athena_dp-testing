package com.rboomerang.processor.impl.common;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;

public class UrlTrigger extends AbstractService {
	private static Logger logger = LoggerFactory.getLogger(UrlTrigger.class);
	private static final String URL = "url";
	private static final String TIMEOUT = "timeOut";
	private static final String REQUEST = "GET";
	private static final String REQUESTMETHOD = "requestMethod";
	Status status = new Status(Status.STATUS_SUCCESS, "Running Success");

	public UrlTrigger(String clientSpecpath, String processorSpecPath)
			throws Exception {
		super(clientSpecpath, processorSpecPath);
		logger.info("UrlTrigger constructor1 called");
	}

	public UrlTrigger(String clientSpecpath, String processorSpecPath,
			String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeDate);
		logger.info("UrlTrigger constructor2 called");
	}

	public UrlTrigger(String clientSpecpath, String processorSpecPath,
			RuntimeContext runtimeContext, String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.info("UrlTrigger constructor3 called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject processorSpec)
			throws JSchException, SftpException, Exception {

		String urlString = (String) processorSpec.get(URL);
		String timeOutString = null;
		String requestMethod = REQUEST;
		int timeOutInSec = 60;
		try {
			if (processorSpec.containsKey(TIMEOUT)) {
				timeOutString = ((String) processorSpec.get(TIMEOUT)).trim();
				timeOutInSec = Integer.parseInt(timeOutString);
			}
		} catch (NumberFormatException e) {
			logger.error("NumberFormatException Thrown for Input--> TIMEOUT : "
					+ timeOutString + " , Please provide integer value");
			logger.error("Error Msg: " + e);
			status.setStatusCode(Status.STATUS_ERROR);
			status.setMessage("NumberFormatException Thrown for Input--> TIMEOUT : "
					+ timeOutString + " , Please provide integer value");
		}
		if (processorSpec.containsKey(REQUESTMETHOD)) {
			requestMethod = ((String) processorSpec.get(REQUESTMETHOD))
					.toUpperCase().trim();
		}
		int timeOut = timeOutInSec * 1000;
		logger.info("URL : " + urlString);
		HttpURLConnection connection = null;
		URL url = null;
		String url_pattern = urlString;
		int code = 0;

		if (url_pattern != null && (!url_pattern.equalsIgnoreCase(""))
				&& status.getStatusCode() != Status.STATUS_ERROR) {
			try {
				logger.info("TimeOut: is set to " + timeOutInSec + " s");
				url = new URL(url_pattern);
				connection = (HttpURLConnection) url.openConnection();
				connection.setConnectTimeout(timeOut);
				connection.setReadTimeout(timeOut);
				connection.setRequestMethod(requestMethod);
				connection.connect();
				code = connection.getResponseCode();

			} catch (MalformedURLException e) {

				logger.error("MalformedURLException Thrown for URL: " + url);
				logger.error("Error Msg: " + e);
				status.setStatusCode(Status.STATUS_ERROR);
				status.setMessage("MalformedURLException Thrown for URL: "
						+ url);

			} catch (SocketTimeoutException e) {
				logger.error("Time-out occur, HttpURLConnection took time >"
						+ timeOut
						+ " for Making Connection or Getting Response for URL :"
						+ url);
				logger.error("SocketTimeoutException Thrown: " + e);
				status.setStatusCode(Status.STATUS_ERROR);
				status.setMessage("Time-out occur, HttpURLConnection took time >"
						+ timeOut
						+ " for Making Connection or Getting Response for URL :"
						+ url);

			} catch (IOException e) {

				logger.error("IOException Thrown for URL: " + url);
				logger.error("Error Msg: " + e);
				status.setStatusCode(Status.STATUS_ERROR);
				status.setMessage("IOException Thrown for URL:" + url);
			}finally{
				if (connection!=null){
					connection.disconnect();
				}
			}

			if (status.getStatusCode() != Status.STATUS_ERROR) {
				logger.info("Response code of the URL Trigger is " + code);
				if (code == 200 || code == 201) {

					logger.info("URL Triggered successfully");
				} else {
					Exception e = new Exception(
							"URL Trigger failed with error code: " + code
									+ " For URL " + url+" using requestMethod :"+requestMethod);
					logger.error("URL_Trigger failed with error code: " + code
							+ " For URL " + url );
					logger.error("Error Msg: "+e);
					status.setStatusCode(Status.STATUS_ERROR);
					status.setMessage("URL Trigger failed with error code: "
							+ code + " For URL " + url+ " using requestMethod :"+requestMethod );
				}
			}
		} else {
			if (status.getStatusCode() != Status.STATUS_ERROR) {
				logger.error("please provide correct URL ");
				status.setStatusCode(Status.STATUS_ERROR);
				status.setMessage("please provide correct URL ");
			}
		}

		return status;
	}
}
