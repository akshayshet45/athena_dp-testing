package com.rboomerang.processor.impl.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.FileProcessingHandler;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

/**
 * This service would take care of merging multiple files of particular type to
 * generate a merged output file. FileProcessingHandler is being used as a
 * utilty to convert the files.
 * 
 * @author sumit
 *
 */
public class FileMerger extends AbstractService {
	private static final Logger logger = LoggerFactory.getLogger(FileMerger.class);
	private static Set<String> supportedFileTypes = new HashSet<>();
	static {
		supportedFileTypes.add("csv");
		supportedFileTypes.add("xlsx");
	}

	public FileMerger(String clientSpecpath, String processorSpecPath) throws Exception {
		super(clientSpecpath, processorSpecPath);
		logger.info("FileMerger constructor called");
	}

	public FileMerger(String clientSpecpath, String processorSpecPath, String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeDate);
		logger.info("FileMerger constructor called");
	}

	public FileMerger(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext,
			String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.info("FileMerger constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject processorSpec)
			throws JSchException, SftpException, Exception {
		logger.info("FileMerger service started");
		// Reading ProcessorSecifications
		JSONObject list = (JSONObject) processorSpec.get(CommonConstants.LIST);
		JSONObject source = (JSONObject) list.get(CommonConstants.SOURCE);
		JSONObject dest = (JSONObject) list.get(CommonConstants.DESTINATION);
		int totalRowsProcessed = 0;
		Boolean createEmptyFile = (Boolean) list.get("createEmptyFile");
		File tempFile = File.createTempFile("processed", ".tmp");
		IEndPointResource destEndPointResource = null;
		IEndPointResource sourceEndPointResource = null;
		boolean createNewWorkbook = true;
		try {
			// getting source Endpoint details
			String sourceEndPoint = (String) source.get(CommonConstants.ENDPOINTNAME);
			String sourcePath = (String) source.get(CommonConstants.PATH);
			String sourceFileName = (String) source.get(CommonConstants.FILE_NAME);

			// check for supported mergeType before processing the files
			String fileType = getFileExtension(sourceFileName);
			if (!supportedFileTypes.contains(fileType)) {
				logger.error("File Merge not suported for the fileType: " + fileType);
				return new Status(Status.STATUS_ERROR, "File Merge not suported for the fileType: " + fileType);
			}

			// Generating Date format
			String sourceDateSuffix = (String) source.get(CommonConstants.FEED_DATE);
			if (sourceDateSuffix == null || sourceDateSuffix.isEmpty()) {
				sourceDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
			}
			sourcePath = replaceDynamicFolders(sourcePath, sourceDateSuffix);
			sourceFileName = replaceDateTag(sourceFileName, sourceDateSuffix);
			logger.debug("Input File Source Endpoint :: " + sourceEndPoint);
			logger.debug("Input File Source Path --> " + sourcePath + " :: FileName --> " + sourceFileName);

			String skipLines = (String) source.get("skipLines");
			int linesToSkip = 0;
			// getting source endpoint properties
			String sourceType = super.getTypeOfEndPoint(clientSpec, sourceEndPoint);
			sourceFileName = makeRegex(sourceFileName);

			HashMap<String, String> sourceEndPointResourceProperties = super.buildResourceProperties(sourceEndPoint,
					clientSpec, sourceType);
			// making connection with source endpoint
			sourceEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(sourceType,
					sourceEndPointResourceProperties);
			List<String> fileList = sourceEndPointResource.getFileList(sourcePath, sourceFileName);
			logger.debug("Files fetched from source: " + fileList);
			sourceEndPointResource.closeConnection();

			// start file merge
			if (fileList != null) {
				for (int index = 0; index < fileList.size(); index++) {
					String fileName = fileList.get(index);
					InputStream iStream = sourceEndPointResource.getFile(sourcePath, fileName);
					logger.info("Started merge for file: " + fileName);
					if (index > 0) {
						linesToSkip = Integer.parseInt(skipLines);
					}
					// performing sanity check for file extension to avoid
					// merging incorrect data.
					if ("csv".equalsIgnoreCase(fileType)) {
						totalRowsProcessed += FileProcessingHandler.mergeCSVFiles(iStream, tempFile, linesToSkip);
					} else if ("xlsx".equalsIgnoreCase(fileType)) {
						totalRowsProcessed += FileProcessingHandler.mergeXLSXFiles(iStream, tempFile, createNewWorkbook,
								linesToSkip);
						if (createNewWorkbook) {
							createNewWorkbook = !createNewWorkbook;
						}
					}
					logger.info("Completed merge for file: " + fileName);
					sourceEndPointResource.closeConnection();
				}
			}

			String destEndPoint = (String) dest.get(CommonConstants.ENDPOINTNAME);
			String destPath = (String) dest.get(CommonConstants.PATH);
			String destFileName = (String) dest.get(CommonConstants.FILE_NAME);
			String destDateSuffix = (String) dest.get(CommonConstants.FEED_DATE);
			if (destDateSuffix == null || destDateSuffix.isEmpty()) {
				destDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
			}
			destPath = replaceDynamicFolders(destPath, destDateSuffix);
			destFileName = replaceDateTag(destFileName, destDateSuffix);
			String destType = getTypeOfEndPoint(clientSpec, destEndPoint);
			logger.debug("Output File destination Endpoint:: " + destEndPoint);
			logger.debug("Output File destination --> " + destPath + " :: and FileName --> " + destFileName);
			if ((fileList != null && fileList.size() > 0) || createEmptyFile) {
				HashMap<String, String> destEndPointResourceProperties = super.buildResourceProperties(destEndPoint,
						clientSpec, destType);
				// making connection with destination endpoint
				destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(destType,
						destEndPointResourceProperties);
				// uploading encrypted file
				destEndPointResource.writeFile(new FileInputStream(new File(tempFile.getAbsolutePath())), destPath,
						destFileName);
			}
			logger.debug("Count of total rows processed for all the merged files: " + totalRowsProcessed);
			return new Status(Status.STATUS_SUCCESS, "FileMerge successful. Processed file " + destFileName
					+ " put to path " + destPath + " at endpoint " + destEndPoint);

		} finally {
			if (tempFile.exists()) {
				tempFile.delete();
			}
			try {
				if (sourceEndPointResource != null) {
					sourceEndPointResource.closeConnection();
				}
				if (destEndPointResource != null) {
					destEndPointResource.closeConnection();
				}
			} catch (Exception e) {
				logger.error("Unable to close EndPointResource Objects", e);
			}
		}
	}

	/**
	 * 
	 * @param fileName
	 * @return
	 */
	private static String getFileExtension(String fileName) {
		return fileName.substring(fileName.lastIndexOf('.') + 1);
	}

}
