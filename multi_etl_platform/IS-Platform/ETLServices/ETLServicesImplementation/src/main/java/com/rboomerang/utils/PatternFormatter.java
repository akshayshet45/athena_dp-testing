package com.rboomerang.utils;

import java.util.Arrays;

import com.rboomerang.common.framework.javabeans.ClientSpec;


public class PatternFormatter {

	public static String replaceDateTag(String fileName, String dateCommand) throws Exception {
		String[] command = dateCommand.split(",");
		String dateFormat=ExecuteCommand.executeAttributeCommands(Arrays.asList(command));
		fileName=fileName.replace(ClientSpec.DATE_FORMAT_REGEX, dateFormat);
		return fileName;
	}
	public static String replaceDateTagForPath(String path, String dateCommand) throws Exception {
		String[] command = dateCommand.split(",");
		String dateFormat=ExecuteCommand.executeAttributeCommands(Arrays.asList(command));

		String fullYear=dateFormat.substring(0, 4);
		String year=dateFormat.substring(2, 4);
		String month=dateFormat.substring(4, 6);
		String date=dateFormat.substring(6);
		path=path.replace("{{DD}}", date);
		path=path.replace("{{MM}}", month);
		path=path.replace("{{YYYY}}", fullYear);
		path=path.replace("{{YY}}", year);
		return path;
	}
}
