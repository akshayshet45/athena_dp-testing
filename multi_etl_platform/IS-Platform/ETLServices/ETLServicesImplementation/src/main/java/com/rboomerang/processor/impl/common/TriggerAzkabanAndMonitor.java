package com.rboomerang.processor.impl.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.activity.InvalidActivityException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.sqs.model.Message;
import com.google.common.collect.ImmutableMap;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.azkaban.utils.HTTPRequestHandler;
import com.rboomerang.azkaban.utils.pojo.ExecuteFlow;
import com.rboomerang.azkaban.utils.pojo.MonitorFlow;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.common.service.utils.amazonUtils.AmazonSqsHelper;
import com.rboomerang.service.framework.AbstractService;

public class TriggerAzkabanAndMonitor extends AbstractService {
	private final Logger logger = LoggerFactory
			.getLogger(TriggerAzkabanAndMonitor.class);
	private AmazonSqsHelper amazonSqsHelper = null;
	private HTTPRequestHandler handler = null;
	private String requestID;

	public TriggerAzkabanAndMonitor(String clientSpecpath,
			String serviceSpecPath) throws Exception {
		super(clientSpecpath, serviceSpecPath);
		logger.info("TriggerAzkabanAndMonitor constructor called");
	}

	public TriggerAzkabanAndMonitor(String clientSpecpath,
			String serviceSpecPath, String runtimeDate) throws Exception {
		super(clientSpecpath, serviceSpecPath, runtimeDate);
		logger.info("TriggerAzkabanAndMonitor constructor called");
	}

	public TriggerAzkabanAndMonitor(String clientSpecpath,
			String serviceSpecPath, RuntimeContext runtimeContext,
			String runtimeDate) throws Exception {
		super(clientSpecpath, serviceSpecPath, runtimeContext, runtimeDate);
		logger.info("TriggerAzkabanAndMonitor constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject serviceSpec)
			throws JSchException, SftpException, Exception {
		Status statusResponse = new Status(Status.STATUS_SUCCESS,
				"Service Execution completed successfully");
		long timeout = Long.parseLong(serviceSpec.get("timeOut").toString()) * 60000;
		long waitTime = Long.parseLong(serviceSpec.get("waitTime").toString()) * 60000;
		int numberOfMessagesPool = Integer.parseInt(serviceSpec.get(
				"numberOfMessagesPool").toString());
		requestID = clientSpec.getClient() + "-" + getRuntimeDate() + "-"
				+ serviceSpec.get("projectName") + "-"
				+ serviceSpec.get("flowName");
		Properties prop = new Properties();
		prop.load(getClass().getClassLoader().getResourceAsStream(
				"awsConfiguraion.properties"));
		amazonSqsHelper = new AmazonSqsHelper(
				prop.getProperty("aws-s3-identity"),
				prop.getProperty("aws-s3-credential"),
				(String) serviceSpec.get("SQSRegion"),
				(String) serviceSpec.get("responseSQS"));
		ExecuteFlow executeFlow = triggerAzkabanProject(serviceSpec, clientSpec);
		if (executeFlow == null) {
			logger.error("Trigger Unsuccessful");
			statusResponse = new Status(Status.STATUS_ERROR,
					"Trigger Unsuccessful");
		} else {
			logger.info("executeFlow info " + executeFlow.getExecId());
			logger.info("executeFlow info " + executeFlow.getFlow());
			logger.info("executeFlow info " + executeFlow.getMessage());
			logger.info("executeFlow info " + executeFlow.getProject());
			statusResponse = monitorExecution(
					Integer.toString(executeFlow.getExecId()), timeout,
					waitTime, numberOfMessagesPool);
		}
		return statusResponse;
	}

	private Status monitorExecution(String execId, Long timeout, Long waitTime,
			int numberOfMessagesPool) throws Exception {
		Status status = new Status(Status.STATUS_SUCCESS, "");

		while (true) {

			long startTime = System.currentTimeMillis();
			MonitorFlow moniterExecution = handler.moniterExecution(execId);
			if (moniterExecution == null)
				throw new Exception("moniterExecution failed");
			if (moniterExecution.getStatus().equals("RUNNING")
					|| moniterExecution.getStatus().equals("PREPARING")) {
				try {
					logger.info("Going to sleep as process is executing");
					logger.info("sleeping for :: " + waitTime);
					long stopTime = System.currentTimeMillis();
					timeout -= (waitTime + (stopTime - startTime));
					if (timeout <= 0) {
						logger.info("Job Timed out");
					}
					logger.info("timeout left :: " + timeout);
					Thread.sleep(waitTime);
				} catch (InterruptedException e) {
					e.printStackTrace();
					logger.error(
							"InterruptedException while reading pooling sqs", e);
				}
			} else {
				List<Message> listMessage = amazonSqsHelper.receiveMessages(
						amazonSqsHelper.getQueueUrl(), numberOfMessagesPool);
				if (listMessage.size() == 0) {
					status = new Status(Status.STATUS_ERROR,
							"Execution Stopped :: "
									+ moniterExecution.getStatus()
									+ "but no message in sqs");
					logger.info("status :: " + status.getMessage());
					break;
				} else {
					boolean breaking = false;
					for (Message message : listMessage) {
						JSONParser jsonParser = new JSONParser();
						JSONObject jsonObject = (JSONObject) jsonParser
								.parse(message.getBody());
						logger.debug("jsonobject : "
								+ jsonObject.toJSONString());
						String actualMessage = message.getBody();

						logger.info("actual message :: " + actualMessage);

						jsonObject = (JSONObject) jsonParser
								.parse(actualMessage);

						if (jsonObject.get("requestID").toString()
								.equals(requestID)) {
							status = jsonObject.get("status").toString()
									.toLowerCase().contains("succes") ? new Status(
									Status.STATUS_SUCCESS, jsonObject.get(
											"status").toString()) : new Status(
									Status.STATUS_ERROR, jsonObject.get(
											"status").toString()
											+ "\n"
											+ jsonObject.get("errorMessage")
													.toString()
											+ "\n"
											+ jsonObject
													.get("errorDescription")
													.toString());
							amazonSqsHelper.deleteMessage(
									message.getReceiptHandle(),
									amazonSqsHelper.getQueueUrl());
							breaking = true;
							break;
						}
					}
					if (breaking)
						break;
				}
				long stopTime = System.currentTimeMillis();
				timeout -= (waitTime + (stopTime - startTime));
				if (timeout <= 0) {
					status = new Status(Status.STATUS_ERROR,
							"Monitering job timed out");
					logger.info("Job Timed out");
					break;
				}
				logger.info("timeout left :: " + timeout);
			}

		}
		return status;
	}

	private ExecuteFlow triggerAzkabanProject(JSONObject serviceSpec,
			ClientSpec clientSpec) throws InvalidActivityException {
		logger.info("TriggerAzkabanProjects about to trigger azkaban project");
		logger.info("TriggerAzkabanProjects in service method");
		List<String> successMailList = new ArrayList<String>();
		for (Object mail : (JSONArray) serviceSpec.get("successMailList")) {
			successMailList.add((String) mail);
		}

		List<String> failureMailList = new ArrayList<String>();
		for (Object mail : (JSONArray) serviceSpec.get("failureMailList")) {
			successMailList.add((String) mail);
		}
		handler = new HTTPRequestHandler(
				(String) serviceSpec.get("azkabanUserName"),
				(String) serviceSpec.get("azkabanPassword"),
				(String) serviceSpec.get("azkabanPort"),
				(String) serviceSpec.get("azkabanUrl"), false, successMailList,
				failureMailList);
		logger.info("TriggerAzkabanProjects triggered project");
		return handler.triggerProject((String) serviceSpec.get("projectName"),
				(String) serviceSpec.get("flowName"), (String) serviceSpec
						.get("parallel"), ImmutableMap.of("clientID",
						clientSpec.getClient(), "requestID", requestID,
						"responseSQS", (String) serviceSpec.get("responseSQS"),
						"SQSRegion", (String) serviceSpec.get("SQSRegion")));

	}
}
