package com.rboomerang.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.CreateQueueResult;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.DeleteQueueRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.amazonaws.services.sqs.model.SetQueueAttributesRequest;

public class AmazonSqsHelper {
	private static Logger logger = LoggerFactory.getLogger(AmazonSqsHelper.class);
//	private static final Logger logger = Logger
//			.getLogger(AmazonSqsHelper.class);
//	private static final Logger PD_LOGGER = Logger.getLogger("fatalLoggerName");
	private AmazonSQS sqsClient;
	String awsSqsMaxNoOfRetries;
	String awsDeadLetterTargetArn;

	public AmazonSqsHelper(AWSCredentials awsCredentials,
			String awsRegionName, String awsSqsMaxNoOfRetries,
			String awsDeadLetterTargetArn) {
		
		sqsClient = new AmazonSQSClient(awsCredentials);
		String regionName = awsRegionName;
		Region region = Region.getRegion(Regions.fromName(regionName));
		sqsClient.setRegion(region);
		this.awsSqsMaxNoOfRetries = awsSqsMaxNoOfRetries;
		this.awsDeadLetterTargetArn = awsDeadLetterTargetArn;
	}

	public AmazonSQS getInstance() {
		return sqsClient;
	}

	public List<String> getAllQueues() {
		return sqsClient.listQueues().getQueueUrls();
	}

	public String getQueueURL(String queueName) {

		GetQueueUrlResult s = sqsClient.getQueueUrl(queueName);
		String queueURL = s.getQueueUrl();
		return queueURL;
	}

	public String createQueue(String queueName) {
		if (queueName == null)
			throw new IllegalArgumentException("queueName can't be null");
		CreateQueueResult createQueue = null;
		try {

			createQueue = sqsClient.createQueue(new CreateQueueRequest(
					queueName));
		} catch (AmazonServiceException ase) {
			logger.error("error while creating  queue SQS", ase);
//			PD_LOGGER.error("error while creating  queue SQS", ase);
		} catch (AmazonClientException ace) {
			logger.error("error while creating  queue SQS", ace);
//			PD_LOGGER.error("error while creating  queue SQS", ace);
		}
		String queueUrl = createQueue.getQueueUrl();
		SetQueueAttributesRequest setQueueAttributesRequest = new SetQueueAttributesRequest();
		Map<String, String> attributes = new HashMap<String, String>();
		// String redrivePolicy = "{\"maxReceiveCount\":\"30\"}";

		String redrivePolicy = "{\"maxReceiveCount\":\"" + awsSqsMaxNoOfRetries
				+ "\", \"deadLetterTargetArn\":\"" + awsDeadLetterTargetArn
				+ "\"};";
		String visibilityTimeout = "120";// time after which another consumer
											// sees the message in queue
											// ofter one consumer fails

		attributes.put("RedrivePolicy", redrivePolicy);
		attributes.put("VisibilityTimeout", visibilityTimeout);
		setQueueAttributesRequest.setAttributes(attributes);
		setQueueAttributesRequest.setQueueUrl(queueUrl);
		sqsClient.setQueueAttributes(setQueueAttributesRequest);
		logger.info("Queue with name: " + queueName + " create with queueUrl: "
				+ queueUrl);
		return queueUrl;
	}
	
	
	public SendMessageResult sendMessage(String message, String queueUrl,
			String queueName, int delaySeconds) {
		SendMessageResult sendMessageResult = null;
		if (queueUrl == null && queueName == null)
			throw new IllegalArgumentException(
					"queueUrl and queueName both can't be null");
		if (queueUrl == null) {
			queueUrl = createQueue(queueName);
		}
		if (queueUrl == null || message == null)
			throw new IllegalArgumentException(
					"queueUrl and message can't be null");
		try {
			SendMessageRequest sendMessageRequest = new SendMessageRequest(queueUrl, message);
			sendMessageRequest.setDelaySeconds(delaySeconds);
			sendMessageResult = sqsClient.sendMessage(sendMessageRequest);
			logger.info("Message: " + message + ", sent to queueUrl: "
					+ queueUrl + " - SendMessageResult: " + sendMessageResult);
		} catch (AmazonServiceException ase) {
			logger.error("error while sending message SQS", ase);
//			PD_LOGGER.error("error while sending message SQS", ase);
		} catch (AmazonClientException ace) {
			logger.error("error while sending message SQS", ace);
//			PD_LOGGER.error("error while sending message SQS", ace);
		}
		return sendMessageResult;
	}


	public SendMessageResult sendMessage(String message, String queueUrl,
			String queueName) {
		SendMessageResult sendMessageResult = null;
		if (queueUrl == null && queueName == null)
			throw new IllegalArgumentException(
					"queueUrl and queueName both can't be null");
		if (queueUrl == null) {
			queueUrl = createQueue(queueName);
		}
		if (queueUrl == null || message == null)
			throw new IllegalArgumentException(
					"queueUrl and message can't be null");
		try {
			sendMessageResult = sqsClient.sendMessage(new SendMessageRequest(
					queueUrl, message));
			logger.info("Message: " + message + ", sent to queueUrl: "
					+ queueUrl + " - SendMessageResult: " + sendMessageResult);
		} catch (AmazonServiceException ase) {
			logger.error("error while sending message SQS", ase);
//			PD_LOGGER.error("error while sending message SQS", ase);
		} catch (AmazonClientException ace) {
			logger.error("error while sending message SQS", ace);
//			PD_LOGGER.error("error while sending message SQS", ace);
		}
		return sendMessageResult;
	}
	
	
	public List<Message> receiveMessages(String queueUrl,
			int maxNumberOfMessages, int waitTimeSeconds) {
		logger.info("Receive messages for queueUrl: " + queueUrl);
		if (queueUrl == null)
			throw new IllegalArgumentException("queueUrl can't be null");
		ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(
				queueUrl);
		receiveMessageRequest.setMaxNumberOfMessages(maxNumberOfMessages);
		receiveMessageRequest.setWaitTimeSeconds(waitTimeSeconds);
		return sqsClient.receiveMessage(receiveMessageRequest).getMessages();
	}


	public List<Message> receiveMessages(String queueUrl,
			int maxNumberOfMessages) {
		logger.info("Receive messages for queueUrl: " + queueUrl);
		if (queueUrl == null)
			throw new IllegalArgumentException("queueUrl can't be null");
		ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(
				queueUrl);
		receiveMessageRequest.setMaxNumberOfMessages(maxNumberOfMessages);
		return sqsClient.receiveMessage(receiveMessageRequest).getMessages();
	}

	public void deleteMessage(String messageReceiptHandle, String queueUrl) {
		logger.info("Deleting message: " + messageReceiptHandle
				+ " from queueUrl: " + queueUrl);
		if (queueUrl == null)
			throw new IllegalArgumentException("queueUrl can't be null");
		sqsClient.deleteMessage(new DeleteMessageRequest(queueUrl,
				messageReceiptHandle));
	}

	public void deleteQueue(String queueUrl) {
		logger.info("Deleting queueUrl: " + queueUrl);
		if (queueUrl == null)
			throw new IllegalArgumentException("queueUrl can't be null");
		sqsClient.deleteQueue(new DeleteQueueRequest(queueUrl));
	}

}
