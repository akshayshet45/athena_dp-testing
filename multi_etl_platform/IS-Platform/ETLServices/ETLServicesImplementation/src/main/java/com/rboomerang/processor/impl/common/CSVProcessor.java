package com.rboomerang.processor.impl.common;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.opencsv.CSVReader;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

/**
 * 
 * @author sumit
 *
 */
public class CSVProcessor extends AbstractService {

	private static Logger logger = LoggerFactory.getLogger(CSVProcessor.class);

	public CSVProcessor(String clientSpecpath, String processorSpecPath) throws Exception {
		super(clientSpecpath, processorSpecPath);
		logger.info("CSVProcessor constructor called");
	}

	public CSVProcessor(String clientSpecpath, String processorSpecPath, String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeDate);
		logger.info("CSVProcessor constructor called");
	}

	public CSVProcessor(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext,
			String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.info("CSVProcessor constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject processorSpec)
			throws JSchException, SftpException, Exception {
		logger.info("CSVProcessor initiated");
		JSONObject list = (JSONObject) processorSpec.get(CommonConstants.LIST);
		JSONObject source = (JSONObject) list.get(CommonConstants.SOURCE);
		JSONObject dest = (JSONObject) list.get(CommonConstants.DESTINATION);

		// getting feed_date date format for source and destination file
		String sourceDateFormat = (String) source.get(CommonConstants.FEED_DATE);
		String destDateFormat = (String) dest.get(CommonConstants.FEED_DATE);

		if (sourceDateFormat == null || sourceDateFormat.isEmpty())
			sourceDateFormat = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
		if (destDateFormat == null || destDateFormat.isEmpty())
			destDateFormat = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();

		// getting source Endpoint details
		String sourceEndPoint = (String) source.get(CommonConstants.ENDPOINTNAME);
		// getting source file path from spec file and replacing date month or
		// year from it!
		String sourcePath = replaceDynamicFolders((String) source.get(CommonConstants.PATH), sourceDateFormat);
		// getting source file name from spec file and replacing feedDate from
		// it!
		String sourceFileName = replaceDateTag((String) source.get(CommonConstants.FILE_NAME), sourceDateFormat);
		// getting Destination Endpoint details
		String destEndPoint = (String) dest.get(CommonConstants.ENDPOINTNAME);
		// getting destination file path from spec file and replacing date month
		// or year from it!
		String destPath = replaceDynamicFolders((String) dest.get(CommonConstants.PATH), destDateFormat);
		// getting destination file name from spec file and replacing
		// feedDate from it!
		String destFileName = replaceDateTag((String) dest.get(CommonConstants.FILE_NAME), destDateFormat);

		logger.debug("Source End Point: " + sourceEndPoint);
		logger.debug("Source File path: " + sourcePath);
		logger.debug("Source File Name: " + sourceFileName);
		logger.debug("Destination End Point: " + destEndPoint);
		logger.debug("Destination File path: " + destPath);
		logger.debug("Destination File Name: " + destFileName);
		String delimiter = (String) list.get(CommonConstants.DELIMITER);
		String qouteCharacter = (String) list.get("qouteCharacter");
		char quoteChar = '\"';
		char delim = ',';
		boolean appendQuoteChar = false;
		if (StringUtils.isNotEmpty(qouteCharacter)) {
			appendQuoteChar = true;
			quoteChar = qouteCharacter.charAt(0);
		}
		if (StringUtils.isNotEmpty(delimiter)) {
			delim = delimiter.charAt(0);
		}
		logger.debug("DelimeterToBeused: " + delim + " ::QouteCharacterToBeUsed: " + quoteChar);
		JSONArray columnsToProcess = (JSONArray) list.get("columns");
		JSONObject action = (JSONObject) list.get("action");
		Boolean remove = (Boolean) action.get("remove");
		Boolean add = (Boolean) action.get("add");
		if ((remove && add) || (!remove && !add)) {
			logger.error("Both add and remove can not be true or false together ! please make change in spec file.");
			return new Status(Status.STATUS_ERROR,
					"Both add and remove can not be true or false together ! please make change in spec file.");
		}
		/*
		 * adding all original and new header values to hashmap to be updated in
		 * csv
		 */
		int columnHeadersLength = columnsToProcess.size();
		String[] columnHeaders = new String[columnHeadersLength];
		for (int j = 0; j < columnHeadersLength; j++) {
			columnHeaders[j] = (String) columnsToProcess.get(j);
			logger.info("Column to process : " + columnHeaders[j]);
		}

		// getting source endpoint properties
		String sourceType = super.getTypeOfEndPoint(clientSpec, sourceEndPoint);
		if (sourceType.equals("sftp"))
			sourceFileName = makeRegex(sourceFileName);

		HashMap<String, String> sourceEndPointResourceProperties = super.buildResourceProperties(sourceEndPoint,
				clientSpec, sourceType);

		// making connection with source endpoint
		IEndPointResource sourceEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(sourceType,
				sourceEndPointResourceProperties);

		// downloading the sourceFile
		InputStream iStream = sourceEndPointResource.getFile(sourcePath, sourceFileName);
		File processedFile = File.createTempFile("processed", ".tmp");

		if (add) {
			logger.info("-----------------Performing Add action----------------");
			remove = !add;
		} else if (remove) {
			logger.info("-----------------Performing Removal action----------------");
		}
		performConfigBasedAction(columnHeaders, iStream, processedFile, delim, quoteChar, remove,
				appendQuoteChar);
		if (add) {
			logger.info("-----------------Add action completed----------------");
		} else if (remove) {
			logger.info("-----------------Removal action completed----------------");
		}
		sourceEndPointResource.closeConnection();

		// getting destination end point properties
		String destType = getTypeOfEndPoint(clientSpec, destEndPoint);
		HashMap<String, String> destEndPointResourceProperties = super.buildResourceProperties(destEndPoint, clientSpec,
				destType);

		// making connection with destination endpoint
		IEndPointResource destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(destType,
				destEndPointResourceProperties);

		// uploading encrypted file
		destEndPointResource.writeFile(new FileInputStream(new File(processedFile.getAbsolutePath())), destPath,
				destFileName);
		destEndPointResource.closeConnection();

		return new Status(Status.STATUS_SUCCESS, "FileProcessor successful. provcessed file " + destFileName
				+ " put to path " + destPath + " at endpoint " + destEndPoint);

	}

	/**
	 * 
	 * @param headerArray
	 * @param sourceStream
	 * @param localFile
	 * @param delimiter
	 * @param quoteCharacter
	 * @param removeAction
	 * @throws Exception
	 */
	private static void performConfigBasedAction(String[] headerArray, InputStream sourceStream, File localFile,
			char delimiter, char quoteCharacter, boolean removeAction, boolean appendQuoteChar) throws Exception {
		// for counting no of rows in csv
		int count = 0;
		String logMessage = "Header Column to be removed::";
		if (!removeAction) {
			logMessage = "Header Column left::";
		}
		for (int l = 0; l < headerArray.length; l++)
			logger.info(logMessage + headerArray[l]);
		BufferedReader sourceReader = null;
		BufferedWriter writer = null;
		try {
			sourceReader = new BufferedReader(new InputStreamReader(sourceStream));
			writer = new BufferedWriter(new FileWriter(localFile));
			String line = sourceReader.readLine();
			if (StringUtils.isBlank(line)) {
				localFile.delete();
				logger.error("Input source file is empty!");
				throw new Exception("Input source file is empty!");
			}
			InputStream lineStream = IOUtils.toInputStream(line, "UTF-8");
			CSVReader csvreader = new CSVReader(new InputStreamReader(lineStream), delimiter, quoteCharacter);
			String[] headerRow = csvreader.readNext();
			csvreader.close();
			lineStream.close();

			if (headerRow != null && headerRow.length > 0) {
				count++;
			}
			if (removeAction) {
				List<String> headerArray_List = new ArrayList<String>(Arrays.asList(headerArray));
				List<String> headerLine_List = new ArrayList<String>(Arrays.asList(headerRow));
				headerLine_List.removeAll(headerArray_List);
				headerArray = headerLine_List.toArray(new String[0]);
			}

			HashMap<String, Integer> headerHashMap = new HashMap<String, Integer>();
			int headerIndex = 0;
			for (String header : headerRow) {
				headerHashMap.put(header, headerIndex++);
			}
			for (int array = 0; array < headerArray.length; array++) {
				logger.info("row element no:" + array + "::element:" + headerArray[array]);
				if (appendQuoteChar) {
					writer.write(quoteCharacter + headerArray[array] + quoteCharacter);
				} else {
					writer.write(headerArray[array]);
				}
				if (array < headerArray.length - 1) {
					writer.write(delimiter);
				}
			}
			writer.write("\n");
			String[] dataRow = null;
			while (true) {
				if ((line = sourceReader.readLine()) == null) {
					break;
				}
				count++;
				lineStream = IOUtils.toInputStream(line, "UTF-8");
				csvreader = new CSVReader(new InputStreamReader(lineStream), delimiter, quoteCharacter);
				dataRow = csvreader.readNext();
				csvreader.close();
				lineStream.close();
				for (int i = 0; i < headerArray.length; i++) {
					Integer columnValueIndex = headerHashMap.get(headerArray[i]);
					if (appendQuoteChar) {
						writer.write(quoteCharacter + dataRow[columnValueIndex] + quoteCharacter);
					} else {
						writer.write(dataRow[columnValueIndex]);
					}
					if (i < headerArray.length - 1) {
						writer.write(delimiter);
					}

				}
				writer.write("\n");
			}
		} finally {
			logger.info("Count of rows processed::" + count);
			try {
				writer.close();
				sourceReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
