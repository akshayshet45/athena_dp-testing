package com.rboomerang.custom.officedepot;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.opencsv.CSVReader;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

public class FileModifier extends AbstractService {
	private static final Logger logger = LoggerFactory.getLogger(FileModifier.class);
	private static Properties props = System.getProperties();
	private static String DELIMITER;

	public FileModifier(String clientSpecpath, String processorSpecPath) throws Exception {
		super(clientSpecpath, processorSpecPath);
		logger.info("FileModifier constructor called");
	}

	public FileModifier(String clientSpecpath, String processorSpecPath, String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeDate);
		logger.info("FileModifier constructor called");
	}

	public FileModifier(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext,
			String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.info("FileModifier constructor called");
	}

	public Status service(ClientSpec clientSpec, JSONObject processorSpec)
			throws JSchException, SftpException, Exception {

		JSONObject list = (JSONObject) processorSpec.get(CommonConstants.LIST);
		DELIMITER = (String) list.get(CommonConstants.DELIMITER);
		JSONObject source = (JSONObject) list.get(CommonConstants.SOURCE);
		JSONObject dest = (JSONObject) list.get(CommonConstants.DESTINATION);
		String systemTempPath = (String) list.get("systemTempPath");

		if (systemTempPath != null) {
			props.setProperty("java.io.tmpdir", systemTempPath);
		}

		String sourceDateSuffix = (String) source.get(CommonConstants.FEED_DATE);
		String destDateSuffix = (String) dest.get(CommonConstants.FEED_DATE);
		if (sourceDateSuffix == null || sourceDateSuffix.isEmpty())
			sourceDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
		if (destDateSuffix == null || destDateSuffix.isEmpty())
			destDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();

		// getting source Endpoint details
		String sourceEndPoint = (String) source.get(CommonConstants.ENDPOINTNAME);
		// getting source file path from spec file and replacing date month or
		// year from it!
		String sourcePath = (String) source.get(CommonConstants.PATH);
		// getting source file name from spec file and replacing feedDate from
		// it!
		String sourceFileName = (String) source.get(CommonConstants.FILE_NAME);

		// getting Destination Endpoint details
		String destEndPoint = (String) dest.get(CommonConstants.ENDPOINTNAME);

		// getting destination file path from spec file and replacing date month
		// or year from it!
		String destPath = (String) dest.get(CommonConstants.PATH);
		// getting destination file path from spec file and replacing feedDate
		// from it!
		String destFileName = (String) dest.get(CommonConstants.FILE_NAME);
		destPath = replaceDynamicFolders(destPath, destDateSuffix);
		sourcePath = replaceDynamicFolders(sourcePath, sourceDateSuffix);
		destFileName = replaceDateTag(destFileName, destDateSuffix);
		sourceFileName = replaceDateTag(sourceFileName, sourceDateSuffix);

		logger.info("Input File Source Endpoint :: " + sourceEndPoint);
		logger.info("Output File destination Endpoint:: " + destEndPoint);
		logger.info("Input File Source Path --> " + sourcePath + " :: FileName --> " + sourceFileName);
		logger.info("Output File destination --> " + destPath + " :: and FileName --> " + destFileName);

		// getting source endpoint properties
		String sourceType = super.getTypeOfEndPoint(clientSpec, sourceEndPoint);
		if (sourceType.equals("sftp"))
			sourceFileName = makeRegex(sourceFileName);

		HashMap<String, String> sourceEndPointResourceProperties = super.buildResourceProperties(sourceEndPoint,
				clientSpec, sourceType);

		// making connection with source endpoint
		IEndPointResource sourceEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(sourceType,
				sourceEndPointResourceProperties);

		// downloading the sourceFile
		InputStream inputStream = sourceEndPointResource.getFile(sourcePath, sourceFileName);
		File processedFile = File.createTempFile("processedFileOfficeDepot", ".tmp");

		logger.info("Processed File path  :  " + processedFile.getAbsolutePath());
		processStream(inputStream, processedFile);
		
		
		sourceEndPointResource.closeConnection();
		// testing processed file for column count mismatch
		test(processedFile);
		// getting destination end point properties
		String destType = getTypeOfEndPoint(clientSpec, destEndPoint);
		HashMap<String, String> destEndPointResourceProperties = super.buildResourceProperties(destEndPoint, clientSpec,
				destType);

		// making connection with source endpoint
		IEndPointResource destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(destType,
				destEndPointResourceProperties);

		// uploading encrypted file
		destEndPointResource.writeFile(new FileInputStream(processedFile), destPath, destFileName);
		destEndPointResource.closeConnection();

		// deleting all temporary files

		processedFile.delete();
		logger.info("deleting all temp files!");

		return new Status(Status.STATUS_SUCCESS,
				"Execution successful for file " + sourceFileName + " in FileModifier");
	}

	private void test(File processedFile) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(processedFile)));
		String line = reader.readLine();
		int count = (line.split(",", -1)).length;
		logger.debug("column count should be in processed file : " + count);
		int err = 0;
		while (true) {

			line = reader.readLine();
			if (line == null)
				break;
			int count1 = (line.split(",")).length;
			if (count1 != count) {
				err++;
//				logger.error(line);
			}

		}
		logger.error("invalid lines count :" + err);
		reader.close();

	}

	private void processStream(InputStream inputStream, File processedFile) throws IOException {
//		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		BufferedWriter writer = new BufferedWriter(new FileWriter(processedFile));
//		String line = null;
//		int lineCount = 0;
//		line = reader.readLine();
//		if (line == null) {
//			logger.error("File Empty no header!!");
//			writer.close();
//			reader.close();
//			return;
//		}
//		writer.write(line);
//		String[] columnCount = line.split(DELIMITER, -1);
//		logger.debug("file column count should be ::: " + columnCount.length);
//		// Starting to process rows after headers
//		while (true) {
//			line = reader.readLine();
//			if (line == null)
//				break;
//			writer.write("\n");
//			lineCount++;
//			if (line.contains("\"")) {
//				int beginIndex = line.indexOf(DELIMITER + "\"");
//				int lastIndex = line.lastIndexOf("\"" + DELIMITER);
//				// logger.debug("line : \n"+line);
//				writer.write(line.substring(0, beginIndex));
//				String linePart = line.substring(beginIndex, lastIndex);
//				linePart = linePart.replaceAll("\"", " ");
//				linePart = linePart.replaceAll(",", " ");
//				writer.write(DELIMITER + linePart + DELIMITER);
//				writer.write(line.substring(lastIndex + 2, line.length()));
//			} else {
//				writer.write(line);
//			}
		
		
//		String[] row = null;
        int count=0,faultCount=0;;
        CSVReader csvreader = new CSVReader(new InputStreamReader(inputStream));
//        List content = csvreader.readAll();
        String[] header=csvreader.readNext();
        
        for (int i=0;i<header.length;i++) {
			writer.write(header[i]);
			if(i<header.length-1)
				writer.write(DELIMITER);
		}
        
        int columnCount= header.length;
        logger.debug("column count : "+columnCount);
        String[] row = null;
        while((row=csvreader.readNext())!=null)
        {
        	writer.write("\n");
        	
        	if(row.length!=columnCount)
        		faultCount++;
        	
        	for (int i=0;i<row.length;i++) {
				writer.write(row[i].replaceAll(",", " ").replaceAll("\"", ""));
				if(i<row.length-1)
					writer.write(DELIMITER);
			}
        	
        }
        writer.close();
        logger.info("faulty lines count : "+faultCount);
        
        
//        for (Object object : content) {
//            row = (String[]) object;
//            System.out.println(row[0].replaceAll(",", " ").replaceAll("\"", "")+ " # " + row[1].replaceAll(",", " ").replaceAll("\"", "")+ " #  " + row[2].replaceAll(",", " ").replaceAll("\"", "")+ " #  " + row[3].replaceAll(",", " ").replaceAll("\"", "")+ " #  " + row[4].replaceAll(",", " ").replaceAll("\"", "")+ " #  " + row[5].replaceAll(",", " ").replaceAll("\"", "")+ " #  " + row[6].replaceAll(",", " ").replaceAll("\"", "")+ " #  " + row[7].replaceAll(",", " ").replaceAll("\"", "")+ " #  " + row[8].replaceAll(",", " ").replaceAll("\"", ""));
//            if(count<20)
//            {
//                count++;
//            }
//            else
//            {
//                break;
//            }
//        }
		
		
		
		
		
		
			// String[] columnValues = line.split(DELIMITER,-1);
			// int processedColumnCount = 0;
			// boolean quoteFlag = false;
			//
			// for (int i = 0; i < columnValues.length; i++) {
			// //logger.info("processing : "+columnValues[i]+" column value size
			// : "+columnValues[i].length());
			// if(!quoteFlag && columnValues[i].length()==0){
			// writer.write(DELIMITER);
			// processedColumnCount++;
			// continue;
			// }
			// else if(quoteFlag && columnValues[i].length()==0){
			// continue;
			// }
			// if (columnValues[i].substring(0, 1).contains("\"")) {
			// quoteFlag = true;
			// writer.write(columnValues[i].substring(1,columnValues[i].length()));
			// }
			// else if
			// (columnValues[i].substring(columnValues[i].length()-1,columnValues[i].length()).contains("\""))
			// {
			// quoteFlag = false;
			// writer.write(columnValues[i].substring(0,columnValues[i].length()-1));
			// }
			// else {
			// if(!quoteFlag)
			// writer.write(columnValues[i]);
			// else
			// writer.write(" "+columnValues[i]+" ");
			// }
			//
			// if (i < columnValues.length - 1 && !quoteFlag){
			// writer.write(DELIMITER);
			// processedColumnCount++;
			// }
			// }
			// for testing only

			// logger.debug("rows column count after processing : " +
			// processedColumnCount);
			// logger.debug("rows column count before processing : " +
			// columnValues.length);
			// for (String string : columnValues) {
			// logger.info(string);
			// }
			// if (true) {
			// writer.close();
			// reader.close();
			// return;
			// }
			// for testing only

        
        
        
//		}
//		writer.close();
//		reader.close();
//		logger.debug("total lines processed excluding headers : " + lineCount);
	}

}
