package com.rboomerang.custom.usap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

/**
 * 
 */
public class ColumnAddition 
{
    private static CSVReader read;
    private static CSVWriter write;
	private static String[] values;

	public static void main( String[] args )
    {
		Properties prop = new Properties();
		String propFileName = "config.properties";

		
    	File folder = new File(args[0]);
    	File[] listOfFiles = folder.listFiles();
    	for(File fileName:listOfFiles){
    		System.out.println(fileName.getName());
    		int i = 0;
    		if(fileName.getName().matches("Product(.*)_(.*).csv")){
    			try {
    				InputStream inputStream = new FileInputStream(propFileName);
    				prop.load(inputStream);
    				read = new CSVReader(new FileReader(fileName));
    				String urlname=fileName.getName().split("_")[1];
    				write = new CSVWriter(new FileWriter(args[0]+File.separator+fileName.getName().split("\\.")[0]+"_modifiled.csv"));
    				String[] nextLine = read.readNext();
    				String []header = new String[8];
    				int index_Product=-1;
    				for(i=0;i<nextLine.length;i++){
    					header[i]=nextLine[i].replace(" ", "_");
    					if(nextLine[i].trim().toLowerCase().equals("products"))
    						index_Product=i;
    				}
    				header[i++]="WebSite";
    				header[i++]="ChannelName";
    				header[i++]="URL";
    				write.writeNext(header);
    				@SuppressWarnings("unused")
					int x=0;
    				while((values =read.readNext()) !=null){
    					x++;
    					for(i=0;i<nextLine.length;i++){
        					header[i]=values[i].trim();
        					if(index_Product==i){
        						if(!values[i].equals("") && values[i].trim().length() > 1 ){
        						if(values[i].contains("|")){
        							String str = "";
        							try{
        								str=values[i].split("\\|")[1].trim();
        							}catch(Exception w){
        								
        							}
        							if(str != null && !str.isEmpty()){
        							header[i]=str;
        							}
        							else {
        								header[i]="";
        							}
        						}
        						else{
        							String str = "";
        							try{
        								str=values[i].split("\\.")[1].trim();
        							}catch(Exception w){
        								
        							}
        							if(str != null && !str.isEmpty()){
        							header[i]=str;
        							}
        							else {
        								header[i]="";
        							}
        						}
        						}
        					}
        				}
    					header[i++]=urlname;
    					header[i++]=prop.getProperty(urlname);
        				header[i++]=prop.getProperty(prop.getProperty(urlname));
        				write.writeNext(header);
    				}
    			} catch (FileNotFoundException e) {
    				e.printStackTrace();
    				System.out.println("Exception" + i);
    				System.exit(1);
    			} catch (IOException e) {
    				e.printStackTrace();
    				System.out.println("IOException" + i);
    				System.exit(1);
    			}catch (Exception e) {
    				e.printStackTrace();
    				System.out.println("Exception" + i);
    				System.exit(1);
    			}finally{
    				try {
						read.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
    				try {
						write.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
    			}
    		}
    	}
    	
    }
}
