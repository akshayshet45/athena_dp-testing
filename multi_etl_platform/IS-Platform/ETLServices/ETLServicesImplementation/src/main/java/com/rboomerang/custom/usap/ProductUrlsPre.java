package com.rboomerang.custom.usap;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.TreeMap;

import com.opencsv.CSVReader;

public class ProductUrlsPre {
	private static TreeMap<String, String> Price2 = new TreeMap<String, String>();
	private static TreeMap<String, String> Price3 = new TreeMap<String, String>();

	private static void populatePrice2() {
		Price2.put("partstrain.com", "1");
		Price2.put("usautoparts.net", "2");
		Price2.put("canadapartsonline.com", "3");
		Price2.put("bongo.partstrain.com", "4");
		Price2.put("bongo.usautoparts.net", "5");

		return;
	}

	private static void populatePrice3() {
		Price3.put("discountbodyparts.com", "1");
		Price3.put("discountautomirrors.com", "2");
		Price3.put("discountcatalyticconverters.com", "3");
		Price3.put("discountexhaustsystems.com", "4");
		Price3.put("discountbrakes.com", "5");
		Price3.put("discountautoradiator.com", "6");
		Price3.put("discountcarlights.com", "7");
		Price3.put("discountautoshocks.com", "8");
		Price3.put("discounto2sensor.com", "9");
		Price3.put("discountfuelsystems.com", "10");
		Price3.put("discountairintake.com", "11");

		return;
	}

	public static void main(String[] args) throws IOException {
		CSVReader reader = null;

		populatePrice2();
		populatePrice3();

		File file = new File("ProductUrls_Processed.txt");

		if (!file.exists()) {
			file.createNewFile();
		}

		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);

		reader = new CSVReader(new FileReader(args[0]), '|');
		String[] nextLine;

		while ((nextLine = reader.readNext()) != null) {

			String line = "", domain_name = "";
			boolean price2 = false, price3 = false;
			int cols = 0;

			for (String token : nextLine) {
				cols++;

				if (cols == 3) {
					if (token.equals("Price2"))
						price2 = true;
					else if (token.equals("Price3"))
						price3 = true;
				} else if (cols == 2)
					domain_name = token;

				line += token;
				line += "|";
			}

			if (price2)
				line += Price2.get(domain_name);
			else if (price3)
				line += Price3.get(domain_name);
			else {
				if (domain_name.equals("domain_name"))
					line += "priority";
				else
					line += "1";
			}

			line += "\n";

			bw.write(line);
		}

		bw.close();
		reader.close();
	}

}
