package com.rboomerang.processor.impl.common;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.common.framework.javabeans.DbSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.SendEmail;
import com.rboomerang.utils.SqlHelper;

public class RecordCountReport extends AbstractService {

	private static Logger logger = LoggerFactory.getLogger(RecordCountReport.class);
	
	public RecordCountReport(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext, String runtimeDate)
			throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.info("RecordCountReport constructor called");
	}

	public RecordCountReport(String clientSpecpath, String validatorSpecPath, String runtimeDate) throws Exception {
		super(clientSpecpath, validatorSpecPath, runtimeDate);
		logger.info("RecordCountReport constructor called");
	}

	public RecordCountReport(String clientSpecpath, String validatorSpecPath) throws Exception {
		super(clientSpecpath, validatorSpecPath);
		logger.info("RecordCountReport constructor called");
	}
	
	private String[] getDateList(int numOfDays,HashMap<String, String> dateValues){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		String dateList[] = new String[numOfDays];
		
		for(int i=0; i<numOfDays; i++){
			Calendar c = Calendar.getInstance();
			c.set(Calendar.YEAR, Integer.parseInt(dateValues.get("YEAR")));
		    c.set(Calendar.MONTH, Integer.parseInt(dateValues.get("MONTH"))-1);
		    c.set(Calendar.DAY_OF_MONTH,Integer.parseInt(dateValues.get("DATE")));
			logger.info("getTime: "+c.getTime());
			c.add(Calendar.DATE, -i);
			dateList[i]= sdf.format(c.getTime());
			
		}
		return dateList;
	}
	

//	status = countCheck(details,countDays,countTables,countColumns,recipients,clientTag,dateValues);
	public Status countCheck(DbSpec details,String[]  countDays,String countTables[],String[] countColumns,String recipients,String clientTag,HashMap<String, String> dateValues) throws Exception{
		String sql=null;
		String table = null;
		ResultSet rs = null;
		SqlHelper redshift = null;
		Status status = new Status(Status.STATUS_SUCCESS, "CountReport sent successfully!");
		StringBuilder message = new StringBuilder("");
		boolean sendMessage = false;
		try{
			redshift= new SqlHelper(details,"redshift");
			
			
			
			for(int i=0; i<countTables.length; i++){
				table = countTables[i]; 
				String dateList[] = getDateList(Integer.parseInt(countDays[i]),dateValues);
				for(int j=0; j<dateList.length; j++){
					sql="select count(*) count  from "+table+" WHERE trunc("+countColumns[i]+") = '"+dateList[j]+"'";
					logger.info("Countcheck Query: "+sql);
					rs = redshift.getSelectedData(sql);
					while(rs.next()){					
						if(rs.getInt("count") == 0){
							message.append("Table \""+table+"\" Has 0 records for the feed Date: \""+dateList[j]+"\"");
							message.append("\n");
							sendMessage = true;
						}
					}//while ends
				}// inner for loop ends
			}//outer for loop ends 
		}catch(Exception e){
			logger.error("error while executing query", e.getMessage());
			status = new Status(Status.STATUS_ERROR,"Error while exeuting query");
		}finally{
			try{
			redshift.close();
			rs.close();
			}catch(SQLException e){
				logger.error("error while closing DB resources", e.getMessage());
			}
		}
		
		if (sendMessage) {
			logger.info(message.toString());
			SendEmail.send(recipients,
					new StringBuilder("[").append(clientTag).append("]").append(" Count check - ").toString(), message.toString());
			return new Status(Status.STATUS_ERROR,"Record count check failed!");
		}

		return status;
	}	
	
	@Override
	public Status service(ClientSpec clientSpec, JSONObject processorSpec) throws Exception {
		Status status = null;
		
		StringBuilder errorMessage=new StringBuilder();
		
		Object[] dbtype = getDbDetails((String) processorSpec.get(CommonConstants.ENDPOINT));
		String recipients = (String) processorSpec.get(CommonConstants.RECIPIENT_LIST);
		String messageHeading=(String) processorSpec.get(CommonConstants.MESSAGE_HEADING);

		boolean countCheck = (boolean)processorSpec.get(CommonConstants.ZERO_COUNT);
		
		JSONObject list = (JSONObject)processorSpec.get(CommonConstants.LIST);
		
		JSONObject source1 = (JSONObject)list.get(CommonConstants.REPORT_TABLES);
		String reportTableNames = (String)source1.get(CommonConstants.TABLE_NAMES);
		String reportTableColumns = (String)source1.get(CommonConstants.DATE_COLUMNS);
		String reportTableDays = (String)source1.get(CommonConstants.NUMOFDAYS);
		
		String reportTables[] = reportTableNames.split(",");
		String reportColumns[] = reportTableColumns.split(",");
		String reportDays[] = reportTableDays.split(",");
		
		//Validating inputs for report tables
		if(!((reportTables.length == reportColumns.length) && (reportColumns.length == reportDays.length))){
			logger.info("Failed in count Report tables inputs count check...");
			errorMessage.append("Failed in validating Report input configs").
						append("Total tables configured : "+reportTables.length).
						append("Total date columns configured : "+reportColumns.length).
						append("Total count input configured : "+reportDays.length);
			return new Status(Status.STATUS_ERROR, "Failed in validating inputs "+errorMessage);
		}
		
		
		JSONObject source2 = (JSONObject)list.get(CommonConstants.COUNT_TABLES);
		String countTableNames = (String)source2.get(CommonConstants.TABLE_NAMES);
		String countTableColumns = (String)source2.get(CommonConstants.DATE_COLUMNS);
		String countTableDays = (String)source2.get("numOfDays");
		
		String countTables[] = countTableNames.split(",");
		String countColumns[] = countTableColumns.split(",");
		String countDays[] = countTableDays.split(",");
		
		//Validating inputs for count tables
		if(!((countTables.length == countColumns.length) && (countColumns.length == countDays.length))){
			System.out.println("Failed in count check...");
			errorMessage.append("Failed in validating Report input configs").
			append("Total tables configured : "+countTables.length).
			append("Total date columns configured : "+countColumns.length).
			append("Total count input configured : "+countDays.length);
			return new Status(Status.STATUS_ERROR, "Failed in validating inputs "+errorMessage);
		}
		
		
		
		
		String clientTag = clientSpec.getClient();
		DbSpec details = (DbSpec) dbtype[1];
		
		String dateSuffix = (String) processorSpec.get(CommonConstants.FEED_DATE);
		logger.info("Date Suffix: "+dateSuffix);
		
		if ( dateSuffix == null || dateSuffix.isEmpty())
			dateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
		
		String startDate = replaceDateTag(ClientSpec.DATE_FORMAT_REGEX, dateSuffix);
		
		HashMap<String, String> dateValues = new HashMap<String,String>();
		dateValues.put("YEAR", startDate.substring(0,4));
		dateValues.put("MONTH", startDate.substring(4,6));
		dateValues.put("DATE", startDate.substring(6,8));
		
		logger.info("Start Date : "+startDate);
		
		logger.info("DB Details: \n Host = "+details.getHost() + "Password = "+details.getPasswd() + "Database = "+details.getDb() + "user = "+details.getUname());
		logger.info("recipients = "+recipients);
		
		
//		status = sendReport(details,numOfDays,tablesNames,recipients,messageHeading,dateValues);
		status = sendReport(details,reportDays,reportTables,reportColumns,recipients,messageHeading,dateValues);
		
		if (status.getStatusCode() == Status.STATUS_ERROR) {
			return status;
		}
		
		if(countCheck){
			status = countCheck(details,countDays,countTables,countColumns,recipients,clientTag,dateValues);
		
			if (status.getStatusCode() == Status.STATUS_ERROR) {
				return status;
			}
		}
		
		return new Status(Status.STATUS_SUCCESS, "Record Count report successful for "+messageHeading);
		
	}
	
//	status = sendReport(details,reportDays,reportTables,reportColumns,recipients,messageHeading,dateValues);
	public Status sendReport(DbSpec details,String[] reportDays,String reportTables[],String[] reportColumns,String recipients,String messageHeading,HashMap<String, String> dateValues){
		
		String output = "";
		String sql=null;
		ResultSet rs = null;
		SqlHelper redshift = null;
		Status status = new Status(Status.STATUS_SUCCESS, "CountReport sent successfully!");
		
		try{
			redshift= new SqlHelper(details,"redshift");
		
		
			
		for(int i=0; i<reportTables.length; i++){
				String table = reportTables[i]; 
				output+= i+1+": "+table+" count\n";
				output+= "Feed Date \t Row Count\n";
				
				String dateList[] = getDateList(Integer.parseInt(reportDays[i]),dateValues);
				
				for(int j=0; j<dateList.length; j++){
					sql="select trunc("+reportColumns[i]+") feed_date,count(*) count  from "+table+" WHERE "+reportColumns[i]+" = '"+dateList[j]+"' group by 1";
					logger.info("Query: "+sql);
					rs = redshift.getSelectedData(sql);
					
					
					if(rs.next()){					
						output+=(rs.getString("feed_date"));
						output+=("\t");
						output+=(rs.getString("count"));
						output+=("\n");
					}else{
						output+=(dateList[j]);
						output+=("\t");
						output+=("0");
						output+=("\n");
					}	
				}// inner for loop ends
				output+=("\n");
			}//outer for loop ends 

			
		SendEmail.send(recipients,messageHeading,output);
		logger.info("Output:\n"+output);
		
		}catch(Exception e){
			logger.error("error while executing query", e.getMessage());
			status = new Status(Status.STATUS_ERROR,"Error while exeuting query");
		}finally{
			try{
			redshift.close();
			rs.close();
			}catch(SQLException e){
				logger.error("error while closing DB resources", e.getMessage());
			}
		}
		return status;
	}
}

