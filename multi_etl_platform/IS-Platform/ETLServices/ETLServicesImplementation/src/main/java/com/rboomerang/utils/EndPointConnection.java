package com.rboomerang.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.utils.endpoint.SftpResourceEndPoint;

public class EndPointConnection {

	private static Logger logger = LoggerFactory.getLogger(EndPointConnection.class);
	SFTPHelper sftpHelper = null;
	S3Helper s3Helper = null;
	
	public InputStream getInputFile(String sourceFileName,String sourceFilePath,ClientSpec clientSpec,String sourceEndPoint,
			HashMap<String, String> sourceEndPointResourceProperties   ) throws Exception
	{
		
		//getting source endpoint properties
		

		//getting file from source
		InputStream iStream =null;
		if(sourceEndPoint.equals("sftp"))
		{

			logger.info("Downloading File from SFTP");
			sftpHelper = new SFTPHelper(sourceEndPointResourceProperties.get(SftpResourceEndPoint.SFTP_HOST),
					sourceEndPointResourceProperties.get(SftpResourceEndPoint.SFTP_PORT),
					sourceEndPointResourceProperties.get(SftpResourceEndPoint.SFTP_USER),
					sourceEndPointResourceProperties.get(SftpResourceEndPoint.SFTP_PASS));
			iStream=sftpHelper.getFromSFTP(sourceFilePath, sourceFileName);
		}
		else if (sourceEndPoint.equals("s3")) {
			logger.info("Downloading File from S3");
			s3Helper= new S3Helper(sourceEndPointResourceProperties);
			iStream=s3Helper.readInputStream(sourceFilePath,sourceFileName);

		}

		return iStream;
	}
	public void writeFile(String destinationEndPoint,String localPath,String localFileName,
			String destinationCSVPath,
			String destinationFileName,
			HashMap<String, 
			String> destEndPointResourceProperties) throws Exception
	{
		
		
		if(!localPath.substring(localPath.length()-1).equals("/"))
			localPath+="/";
		if(!localPath.substring(0,1).equals("/"))
			localPath="/"+localPath;
		if(localFileName.substring(0,1).equals("/"))
			localFileName=localFileName.substring(1);
		
		if(!destinationCSVPath.substring(destinationCSVPath.length()-1).equals("/"))
			destinationCSVPath+="/";
		if(!destinationCSVPath.substring(0,1).equals("/"))
			destinationCSVPath="/"+destinationCSVPath;
		if(destinationFileName.substring(0,1).equals("/"))
			destinationFileName=destinationFileName.substring(1);
		
		if(destinationEndPoint.equals("sftp"))
			writeFileToSFTP(localPath+localFileName, destinationCSVPath+destinationFileName,destEndPointResourceProperties);
		else if (destinationEndPoint.equals("s3")) 
			writeFileToS3(localPath+localFileName, destinationCSVPath+destinationFileName,destEndPointResourceProperties);
		
	}
	private static void writeFileToSFTP(String localFilePath,String sftpFilePath,HashMap<String, String> endPointResourceProperties)throws Exception
	{
		SFTPHelper sftpHelper = new SFTPHelper(endPointResourceProperties.get(SftpResourceEndPoint.SFTP_HOST),
				endPointResourceProperties.get(SftpResourceEndPoint.SFTP_PORT),
				endPointResourceProperties.get(SftpResourceEndPoint.SFTP_USER),
				endPointResourceProperties.get(SftpResourceEndPoint.SFTP_PASS));
		logger.info("In SFTP ---> putting local file --> \""+localFilePath+"\"  to SFTP Path --> \""+sftpFilePath+"\"");
		sftpHelper.putInSFTP(new FileInputStream(localFilePath),sftpFilePath);
		sftpHelper.close();
		return;
	}
	private static void writeFileToS3(String localFilePath,String s3FilePath,HashMap<String, String> endPointResourceProperties) throws IOException
	{
		String prefix="";
		S3Helper s3Helper = new S3Helper(endPointResourceProperties);
		String[] path=s3FilePath.split("/");
		String filename=path[path.length-1];
		for(int i=1;i<path.length-1;i++){
			if(!path[i].equals(null))
				prefix=prefix+path[i];
			if(i<path.length-2){
				prefix+="/";
			}
		}
		logger.info("prefix:::"+prefix);
		logger.info("File Name:::"+filename);
		logger.info("adding file to S3");
		s3Helper.storeObject(prefix, filename, new FileInputStream(localFilePath));
	}
	public void closeConnections() throws IOException {
		sftpHelper.close();
		logger.info("closing sftp helper in Get Source File");
	}

}
