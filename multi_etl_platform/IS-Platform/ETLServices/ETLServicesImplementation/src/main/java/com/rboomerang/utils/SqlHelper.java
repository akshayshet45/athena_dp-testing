package com.rboomerang.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rboomerang.common.framework.javabeans.DbSpec;

/**
 * SqlHelper Class is to provide the basic interface for executing Sqls.
 *
 * @author Vishal Prakash Narain Srivastava
 * @version 1.0
 * @since 2015-10-12
 */
public class SqlHelper {
	private final Logger log = LoggerFactory.getLogger(SqlHelper.class);
	public static final String TYPE_REDSHIFT = "redshift";
	public static final String TYPE_MYSQL = "mysql";
	private static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
	private static final String POSTGRE_DRIVER = "com.amazon.redshift.jdbc41.Driver";

	private DbSpec spec;
	private String DB_DRIVER;
	private String DB_URL = "jdbc:{{TYPE}}://{{HOST}}/{{DATABASE}}";

	private Connection conn = null;
	private Statement stmt = null;

	public SqlHelper(DbSpec spec, String type) throws ClassNotFoundException,
			SQLException {
		this.spec = spec;
		switch (type) {
		case TYPE_REDSHIFT:
			DB_DRIVER = POSTGRE_DRIVER;
			DB_URL = DB_URL.replace("{{TYPE}}", "redshift")
					.replace("{{HOST}}", (this.spec.getHost())+":5439")
					.replace("{{DATABASE}}", this.spec.getDb());
			break;
		case TYPE_MYSQL:
			DB_DRIVER = MYSQL_DRIVER;
			DB_URL = DB_URL.replace("{{TYPE}}", "mysql")
					.replace("{{HOST}}", this.spec.getHost() + ":3306")
					.replace("{{DATABASE}}", this.spec.getDb());
			break;
		}
		Class.forName(DB_DRIVER);
		log.info("Connecting to database...");
		conn = DriverManager.getConnection(DB_URL, this.spec.getUname(),
				this.spec.getPasswd());
		log.info("SqlHelper constructor called");
	}
	
	public boolean executeQuery(String sql){
		boolean flag=false;
		try {
			stmt = conn.createStatement();
			flag = stmt.execute(sql);
		} catch (SQLException e) {
			log.error("Error while exeuting SqlHelper.executeQuery at SQLException", e);
		} catch (Exception e) {
			log.error("Error while exeuting SqlHelper.executeQuery at Exception", e);
		} finally {
			try {
				close();
			} catch (SQLException e) {
				log.error("Error while closing stmt and conn SqlHelper.executeQuery at Exception", e);
			}
		}
		return flag;
	}

	public ResultSet getSelectData(String sql){
		ResultSet resultSet = null;
		try {
			stmt = conn.createStatement();
			resultSet = stmt.executeQuery(sql);
		} catch (SQLException e) {
			log.error("Error while exeuting SqlHelper.getSelectData at SQLException", e);
		} catch (Exception e) {
			log.error("Error while exeuting SqlHelper.getSelectData at Exception", e);
		} 
		return resultSet;
	}
	
	public ResultSet getSelectData(String sql, int rowCount){
		ResultSet resultSet = null;
		try {
			stmt = conn.createStatement();
			stmt.setMaxRows(rowCount);
			resultSet = stmt.executeQuery(sql);
		} catch (SQLException e) {
			log.error("Error while exeuting SqlHelper.getSelectData at SQLException", e);
		} catch (Exception e) {
			log.error("Error while exeuting SqlHelper.getSelectData at Exception", e);
		} finally {
			try {
				close();
			} catch (SQLException e) {
				log.error("Error while closing stmt and conn SqlHelper.getSelectData at Exception", e);
			}
		}
		return resultSet;
	}

	
	public ResultSet getSelectedData(String sql) throws SQLException {
		ResultSet resultSet = null;

		stmt = conn.createStatement();
		resultSet = stmt.executeQuery(sql);

		return resultSet;
	}
	
	public void close() throws SQLException {
		if (!stmt.isClosed())
			stmt.close();
		if (!conn.isClosed())
			conn.close();
	}
}
