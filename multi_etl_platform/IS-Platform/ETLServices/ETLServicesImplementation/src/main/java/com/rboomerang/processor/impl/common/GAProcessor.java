package com.rboomerang.processor.impl.common;
/**
 * Description: Google Analytics Processor
 */


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.GoogleAnalyticsAPI;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

public class GAProcessor extends AbstractService 
{
	private static Logger logger = LoggerFactory.getLogger(GAProcessor.class);
	public GAProcessor(String clientSpecpath, String processorSpecPath) throws Exception
	{
		super(clientSpecpath, processorSpecPath);
		logger.info("GAProcessor constructor called");
	}
	public GAProcessor(String clientSpecpath, String processorSpecPath, String runtimeDate) throws Exception
	{
		super(clientSpecpath, processorSpecPath,runtimeDate);
		logger.info("GAProcessor constructor called");
	}
	public GAProcessor(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext, String runtimeDate) throws Exception 
	{
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.info("GAProcessor constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject processorSpec) throws Exception
	{
		logger.info("GoogleAnalytics Processor execution started");
		//Reading GA API global parameters
		String clientName=clientSpec.getClient();
		JSONObject accountParams = (JSONObject) processorSpec.get("account");
		JSONObject apiQuery= (JSONObject) processorSpec.get(CommonConstants.QUERY);
		JSONObject obj= accountParams;
		String keyFilePath=this.getRuntimeContext().transformPath((String)obj.get("keyLocation"));
		String accountMail=(String)obj.get("accountMail");
		
		//Invoking GoogleAnalytics API query
		GoogleAnalyticsAPI GAObject =new GoogleAnalyticsAPI(clientName,keyFilePath,accountMail,apiQuery);
		String fileName=GAObject.start();
		
		Status status=null;
		IEndPointResource endPointResource=null;
		try
		{
			JSONObject endpointDetails=(JSONObject) processorSpec.get(CommonConstants.DESTINATION);
			String feedFileName = (String)endpointDetails.get(CommonConstants.FILENAME);
			String destDateSuffix = (String)endpointDetails.get(CommonConstants.FEED_DATE);
			String location = (String)endpointDetails.get(CommonConstants.LOCATION);
			if (destDateSuffix == null || destDateSuffix.isEmpty() )
			{
				destDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
			}
			feedFileName = replaceDateTag(feedFileName, destDateSuffix);
			logger.info("output file name is "+feedFileName);
			InputStream in= new FileInputStream(new File(fileName));
			System.out.println(fileName);
			File outputTempFile=new File(fileName);
			
			if(fileName !=null)
			{
				String endpointName = (String)endpointDetails.get(CommonConstants.ENDPOINTNAME);
				String endpointType = getTypeOfEndPoint(clientSpec,endpointName);
				HashMap<String, String> endPointResourceProperties = buildResourceProperties(endpointName, clientSpec,endpointType);
				endPointResource = EndPointResourceFactory.getInstance().getEndPointResource(endpointType,endPointResourceProperties);
				logger.debug("Uploading file to "+endPointResourceProperties);
				endPointResource.writeFile(in,location,feedFileName);
				logger.info(String.format("Successfully uploaded!! file path is %s %s%s",endpointType,location,feedFileName));
				status = new Status(Status.STATUS_SUCCESS, "GAProcessor Processed metrics successfully!!");
				boolean delStatus=outputTempFile.delete();
				if(delStatus)
				{
					logger.debug("Temporary file "+fileName+" has been deleted!!");
				}
				else
				{
					logger.warn("Unable to delete temp file "+fileName);
				}
			}
			else
			{
				status = new Status(Status.STATUS_ERROR, "Uploading generated metric file failed!!");
			}
		}
		catch(Exception e)
		{
			logger.error("Uploading file to endPoint failed");
			status = new Status(Status.STATUS_ERROR, "Uploading generated metric file failed!!");
			e.printStackTrace();
		}
		finally
		{
			endPointResource.closeConnection();
			logger.debug("Connection to endpoint closed");
		}	
		return status;
	}
}
