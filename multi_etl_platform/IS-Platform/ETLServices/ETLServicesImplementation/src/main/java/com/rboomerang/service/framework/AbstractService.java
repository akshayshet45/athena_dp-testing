package com.rboomerang.service.framework;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.ClientSpecInitialization;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.AzkabanSpec;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.common.framework.javabeans.DbSpec;
import com.rboomerang.common.framework.javabeans.MessageContentSpec;
import com.rboomerang.common.framework.javabeans.S3Spec;
import com.rboomerang.common.framework.javabeans.SftpSpec;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.RuntimeParametersUtility;
import com.rboomerang.utils.SendEmail;
import com.rboomerang.utils.endpoint.S3ResourceEndPoint;
import com.rboomerang.utils.endpoint.SftpResourceEndPoint;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.QueueDoesNotExistException;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.rboomerang.utils.AWSAccess;
import com.rboomerang.utils.AmazonSqsHelper;


public abstract class AbstractService implements IService {
	private static Logger logger = LoggerFactory
			.getLogger(AbstractService.class);

	private static final String PROPERTY_MESSAGE_TYPE = "type";
	private static final String PROPERTY_MESSAGE_SUBJECT = "subject";
	private static final String PROPERTY_MESSAGE_BODY = "body";
	private static final String PROPERTY_MESSAGE = "message";
	private ClientSpec clientSpec = null;
	private String execid = null;

	private String jobName = null;
	private String azkabanHost = null;
	private String runtimeDate = null;
	private ClientSpecInitialization initializer = null;
	private JSONObject serviceSpec = null;
	private FileReader fileReader = null;
	private HashMap<String, MessageContentSpec> messageContent = new HashMap<String, MessageContentSpec>();
	private List<String> successList = new ArrayList<String>();
	private List<String> failureList = new ArrayList<String>();

	private RuntimeContext runtimeContext = null;

	public String getExecid() {
		return execid;
	}

	protected String getRuntimeDate() {
		return runtimeDate;
	}

	public void setExecid(String execid) {
		this.execid = execid;
	}

	public ClientSpec getClientSpec() {
		logger.info("Abstract Service getClientSpec");
		return clientSpec;
	}

	public JSONObject getServiceSpec() {
		logger.info("Abstract Service getServiceSpec");
		return serviceSpec;
	}

	public HashMap<String, MessageContentSpec> getMessageContent() {
		logger.info("Abstract Service getMessageContent");
		return messageContent;
	}

	public RuntimeContext getRuntimeContext() {
		return this.runtimeContext;
	}

	public String extractLocalPath(String path) {
		String[] splitString = path.split("/");
		String localPath = splitString[splitString.length - 2]; // take the last
		// string

		return localPath;
	}

	public AbstractService(String clientSpecpath, String serviceSpecPath)
			throws Exception {
		initializer = new ClientSpecInitialization(clientSpecpath);
		clientSpec = initializer.getClientSpecObject();
		this.runtimeContext = new RuntimeContext(false, null);

		initalizeServiceSpec(serviceSpecPath);
		logger.info("Abstract Service Called");
	}

	public AbstractService(String clientSpecpath, String serviceSpecPath,
			String runtimeDate) throws Exception {
		initializer = new ClientSpecInitialization(clientSpecpath);
		clientSpec = initializer.getClientSpecObject();
		this.runtimeContext = new RuntimeContext(false, null);
		this.runtimeDate = runtimeDate;
		String clientSpecDate = calculateSpecDate(runtimeDate, clientSpec);
		clientSpec.setDate(clientSpecDate);
		initalizeServiceSpec(serviceSpecPath);
		logger.info("Abstract Service Called");
	}

	public AbstractService(String clientSpecpath, String serviceSpecPath,
			RuntimeContext runtimeContext, String runtimeDate) throws Exception {
		initializer = new ClientSpecInitialization(clientSpecpath);
		clientSpec = initializer.getClientSpecObject();
		this.runtimeContext = runtimeContext;

		String clientSpecDate = calculateSpecDate(runtimeDate, clientSpec);
		clientSpec.setDate(clientSpecDate);
		initalizeServiceSpec(serviceSpecPath);

		logger.info("Abstract Service Called");
	}

	@Override
	public void close() throws IOException {
		if (fileReader != null)
			fileReader.close();
		logger.info("Abstract Service Closed");
	}

	public HashMap<String, String> buildResourceProperties(String endpointName,
			ClientSpec clientSpec, String type) {
		HashMap<String, String> properties = new HashMap<String, String>();
		if (type.equals("sftp")) {
			SftpSpec sftpauth = clientSpec.getSftpAuthList().get(endpointName);
			properties.put(SftpResourceEndPoint.SFTP_HOST, sftpauth.getHost());
			properties.put(SftpResourceEndPoint.SFTP_PORT, "22");
			properties.put(SftpResourceEndPoint.SFTP_USER, sftpauth.getUname());
			properties
					.put(SftpResourceEndPoint.SFTP_PASS, sftpauth.getPasswd());
		} else if (type.equals("s3")) {
			S3Spec s3auth = clientSpec.getS3AuthList().get(endpointName);
			properties.put(S3ResourceEndPoint.S3_BUCKET_NAME,
					s3auth.getBucketName());
			properties.put(S3ResourceEndPoint.S3_BUCKET_REGION,
					s3auth.getBucketRegion());
		}

		return properties;
	}

	public Object[] getDbDetails(String aliasName) {
		Object[] retVal = new Object[2];
		DbSpec redSpec = clientSpec.getRedshiftAuthList().get(aliasName);
		if (redSpec != null) {
			retVal[0] = "redshift";
			retVal[1] = redSpec;
		}
		DbSpec mysqlSpec = clientSpec.getMysqlAuthList().get(aliasName);
		if (mysqlSpec != null) {
			retVal[0] = "mysql";
			retVal[1] = mysqlSpec;
		}
		return retVal;
	}

	public String getTypeOfEndPoint(ClientSpec clientSpec, String endPointName)
			throws Exception {
		String retVal = null;

		if (clientSpec.getSftpAuthList().get(endPointName) != null)
			retVal = "sftp";
		else if (clientSpec.getMysqlAuthList().get(endPointName) != null)
			retVal = "mysql";
		else if (clientSpec.getRedshiftAuthList().get(endPointName) != null)
			retVal = "redshift";
		else if (clientSpec.getS3AuthList().get(endPointName) != null)
			retVal = "s3";
		return retVal;
	}

	@Override
	public// Implementation classes need to override this method
	abstract Status service(ClientSpec clientSpec, JSONObject serviceSpec)
			throws JSchException, SftpException, Exception;

	// This method orchestrates the validation and notification mechanism of the
	// validation result
	public void execute(ClientSpec clientSpec, JSONObject serviceSpec,
			String execid, String jobName, 
			String runtimeDate, String waitFrequency, String decoratorTimeout)
			throws JSchException, SftpException, Exception {
		logger.info("Retrieved runtime feed date: " + runtimeDate);
		AzkabanSpec azkabanSpec = clientSpec.getAzkabanAuthList().get(
				clientSpec.getAzkabanName());
		setExecid(execid);

		this.execid = execid;
		this.jobName = jobName;
		this.azkabanHost = azkabanSpec == null ? "nothing" : azkabanSpec
				.getAzkaban_host();
		this.runtimeDate = runtimeDate;

		String clientSpecDate = calculateSpecDate(this.runtimeDate, clientSpec);
		clientSpec.setDate(clientSpecDate);

		Status status = null;

		if (waitFrequency != null && decoratorTimeout != null) {
			// decorator mode active. handler needed.
			status = decoratorHandler(clientSpec, serviceSpec, Integer.parseInt(waitFrequency),
					Integer.parseInt(decoratorTimeout));
		} else
			status = this.service(clientSpec, serviceSpec);

		logger.info("Status for Service: " + status.getStatusCode());
		logger.info("Message for Service: " + status.getMessage());
		logger.info("Finally, Retrieved azkaban execid: " + execid);
		logger.info("Finally, Retrieved azkaban jobName: " + jobName);

		//status.setHelpLink((String) serviceSpec.get(CommonConstants.HELP_LINK));
		status.setAbortRun((boolean) serviceSpec.get(CommonConstants.ABORT_RUN));
		status.setPagerdutyKey((String) serviceSpec.get(CommonConstants.PAGERDUTY_KEY));
		status.setPagerdutyAlert((boolean) serviceSpec.get(CommonConstants.PAGERDUTY_ALERT));
		String notificationEmail=(String)serviceSpec.get(CommonConstants.NOTIFICATION_EMAIL);
		String errorEmail=(String)serviceSpec.get(CommonConstants.ERROR_EMAIL);
		if (notificationEmail==null || notificationEmail.equals("")) {
			notificationEmail=StringUtils.join(clientSpec.getNotificationEmail(), ',');
			logger.debug("Notification email not provided in Service spec, taking the default one from clientSpec : "+notificationEmail);
		}else
		{
			logger.debug("Notification email provided in Service spec : "+notificationEmail);
		}
		String pagerdutyKey = (status.getPagerdutyKey() != null) ? status.getPagerdutyKey()
				: clientSpec.getPagerDuty();
		String notify = (String) serviceSpec.get(CommonConstants.NOTIFY);

		if (status.isStatusSuccess()) {
			this.onSuccess(status, notificationEmail);
		} else {
			 notify = (String) serviceSpec.get(CommonConstants.NOTIFY);
			if (notify.equals(MessageContentSpec.WARNING)) {
				this.onWarning(status, notificationEmail, pagerdutyKey);
			} else if (notify.equals(MessageContentSpec.ERROR)) {
				this.onError(status,notificationEmail,pagerdutyKey);
			}else if(notify.equals(MessageContentSpec.ERROR_ONLY))
			{
				if(errorEmail==null ||errorEmail.equals(""))
				{
					logger.debug("Error mail is not provided in ServiceSpec, taking notification mails instead");
					this.onError(status,notificationEmail,pagerdutyKey);
				}
				else
				{
					this.onError(status,errorEmail,pagerdutyKey);
				}
				
			}
		}
	}
	
	@SuppressWarnings("unused")
	private Status decoratorHandler(ClientSpec clientSpec, JSONObject serviceSpec, int waitFrequency,
			int decoratorTimeout) throws JSchException, SftpException, Exception {
		Status status = null;

		// create queue (use one if exists)
		AWSAccess awsAccess = new AWSAccess();
		AmazonSqsHelper sqsHelper = new AmazonSqsHelper(awsAccess.getAWSUserSpecificAccess(), "us-west-2", "", "");

		String queueName = "is-etl-services-decorator", queueUrl = "";
		try {
			queueUrl = sqsHelper.getQueueURL(queueName);
		} catch (QueueDoesNotExistException e) {
			logger.info("queue not found !! creating now...");
			queueUrl = sqsHelper.createQueue(queueName);
		}

		// loop till decoratorTimeout
		// inside loop: 1) put message to queue, 2) wait for waitFrequency 3)
		// get message from queue
		// and perform service 4) on status true - return, on status false -
		// repeat
		int repeatRecieve = waitFrequency / 20;
		List<Message> messages = new ArrayList<Message>();
		while (decoratorTimeout > 0) {
			SendMessageResult sendMessageResult = sqsHelper.sendMessage(this.jobName, queueUrl, queueName,
					waitFrequency);
			while (repeatRecieve > 0) {
				messages = sqsHelper.receiveMessages(queueUrl, 1, 20);
				if (!messages.isEmpty())
					break;
				--repeatRecieve;
				logger.info("did not find the message in queue yet");
			}
			repeatRecieve = waitFrequency / 20;
			logger.info("this is the message receieved: " + messages.get(0).getBody());
			sqsHelper.deleteMessage(messages.get(0).getReceiptHandle(), queueUrl);
			messages.clear();
			status = this.service(clientSpec, serviceSpec);

			if (status.isStatusSuccess())
				return status;
			--decoratorTimeout;
		}

		return status;
	}


	public void onSuccess(Status status, String notificationEmail)
			throws Exception {
		MessageContentSpec mcon = messageContent
				.get(MessageContentSpec.SUCCESS);
		String clientSpecDate = clientSpec.getDate();
		String subject = "[" + clientSpec.getClient() + "]"
				+ CommonConstants.SPACE + "[#" + this.execid + "]"
				+ CommonConstants.SPACE;
		SendEmail.send(
				notificationEmail,
				subject
						+ mcon.getSubject().replace(
								ClientSpec.DATE_FORMAT_REGEX, clientSpecDate),
				mcon.getBody() + CommonConstants.NEWLINE + status.getMessage());

		Map<String, String> responseMessage = new HashMap<String, String>();
		Properties properties = getAzkabanFlowParam();
//		String filePath = System
//				.getProperty(CommonConstants.AZKABAN_JOB_PROPERTIES_FILE_PATH);
//		logger.debug(filePath);
//		properties.load(new FileInputStream(filePath));
		
		logger.debug("SQSRegion : "+properties.getProperty("SQSRegion"));
		logger.debug("responseSQS : "+properties.getProperty("responseSQS"));
		responseMessage.put(
				"requestID",
				properties.get("requestID") == null ? "" : properties.get(
						"requestID").toString());
		responseMessage.put("clientID", properties.get("clientID") == null ? ""
				: properties.get("clientID").toString());
		responseMessage.put("status", "success");
		responseMessage.put("errorCode",
				Integer.toString(status.getStatusCode()));
		responseMessage.put("errorMessage", "");
		responseMessage.put("errorDescription", "");
		subject = subject
				+ mcon.getSubject().replace(ClientSpec.DATE_FORMAT_REGEX,
						clientSpecDate);

		initializer.getNotifier().notifySuccess(successList, subject,
				responseMessage);

	}

	public void onError(Status status, String notificationEmail,
			String pagerdutyKey) throws Exception {
		MessageContentSpec mcon = messageContent.get(MessageContentSpec.ERROR);
		String clientSpecDate = clientSpec.getDate();
		String logLink = CommonConstants.AZKABAN_URL_SUFFIX;
		logLink = logLink.replace(ClientSpec.AZKABAN_HOST, this.azkabanHost)
				.replace(CommonConstants.EXEC_ID, this.execid).replace(CommonConstants.JOB_NAME, this.jobName);

		String subject = "["+clientSpec.getClient() +"]"+ CommonConstants.SPACE +"[#"+ this.execid+"]" + CommonConstants.SPACE;
		String bodyForMessage = CommonConstants.LOG_LINK_TITLE + ": " + logLink + CommonConstants.NEWLINE;
				
		String helpLinkMessage="\n\nTo resume the flow,Please follow below instructions.\n\t1.Go to the logLink specified below\n\t\t"+bodyForMessage
				+"\n\t2.Click on \"Execution "+this.execid+"\""
				+ "\n\t3.Click on Prepare Execution button on top right corner in UI\n\t"
				+"4.Click on Execute button in the popup window to resume.\n\n";
		status.setHelpLink(helpLinkMessage);
		SendEmail.sendErrorMail(notificationEmail,
				subject + mcon.getSubject().replace(ClientSpec.DATE_FORMAT_REGEX, clientSpecDate),
				 mcon.getBody()+status.getMessage() + status.getHelpLink(), clientSpec.getClient());


		if (status.getPagerdutyAlert()) {
			SendEmail.sendPagerDuty(
					pagerdutyKey,
					mcon.getSubject()
							.replace(ClientSpec.CLIENT_TAG_REGEX,
									clientSpec.getClient())
							.replace(ClientSpec.DATE_FORMAT_REGEX,
									clientSpecDate)
							.replace(CommonConstants.EXEC_ID, this.execid),
					ImmutableMap.of(
							"Client",
							clientSpec.getClient(),
							CommonConstants.LOG_LINK_TITLE,
							logLink,
							CommonConstants.HELP_LINK_TITLE,
							status.getHelpLink(),
							"Message",
							mcon.getBody() + CommonConstants.NEWLINE
									+ status.getMessage()));
		}

		Map<String, String> responseMessage = new HashMap<String, String>();
		Properties properties = getAzkabanFlowParam();
//		String filePath = System
//				.getProperty(CommonConstants.AZKABAN_JOB_PROPERTIES_FILE_PATH);
//		logger.debug(filePath);
//		properties.load(new FileInputStream(filePath));
		logger.debug("SQSRegion : "+properties.getProperty("SQSRegion"));
		logger.debug("responseSQS : "+properties.getProperty("responseSQS"));
		responseMessage.put(
				"requestID",
				properties.get("requestID") == null ? "" : properties.get(
						"requestID").toString());
		responseMessage.put("clientID", properties.get("clientID") == null ? ""
				: properties.get("clientID").toString());
		responseMessage.put("status", "failure");
		responseMessage.put("errorCode",
				Integer.toString(status.getStatusCode()));
		responseMessage.put("errorMessage", status.getMessage());
		responseMessage.put("errorDescription", status.getMessage());
		subject = subject
				+ mcon.getSubject().replace(ClientSpec.DATE_FORMAT_REGEX,
						clientSpecDate);

		initializer.getNotifier().notifyFailure(failureList, subject,
				responseMessage);

		if (status.getAbortRun())
			System.exit(-1);
	}

	public void onWarning(Status status, String notificationEmail,
			String pagerdutyKey) throws Exception {
		MessageContentSpec mcon = messageContent
				.get(MessageContentSpec.WARNING);
		String clientSpecDate = clientSpec.getDate();
		String logLink = CommonConstants.AZKABAN_URL_SUFFIX;
		logLink = logLink.replace(ClientSpec.AZKABAN_HOST, this.azkabanHost)
				.replace(CommonConstants.EXEC_ID, this.execid).replace(CommonConstants.JOB_NAME, this.jobName);

		String subject = "["+clientSpec.getClient() +"]"+ CommonConstants.SPACE +"[#"+ this.execid+"]" + CommonConstants.SPACE;
		String bodyForMessage = CommonConstants.LOG_LINK_TITLE + ": " + logLink + CommonConstants.NEWLINE;
		String helpLinkMessage="\n\nTo resume the flow,Please follow below instructions.\n\n\t1.Go to the logLink specified below\n\t\t"+bodyForMessage
				+"\n\t2.Click on \"Execution "+this.execid+"\""
				+ "\n\t3.Click on Prepare Execution button on top right corner in UI\n\t"
				+"4.Click on Execute button in the popup window to resume.\n\n";
		status.setHelpLink(helpLinkMessage);
		SendEmail.sendErrorMail(notificationEmail,
				subject + mcon.getSubject().replace(ClientSpec.DATE_FORMAT_REGEX, clientSpecDate),
				 mcon.getBody() + status.getMessage()+status.getHelpLink(), clientSpec.getClient());


		if (status.getPagerdutyAlert()) {

			SendEmail.sendPagerDuty(
					pagerdutyKey,
					mcon.getSubject()
							.replace(ClientSpec.CLIENT_TAG_REGEX,
									clientSpec.getClient())
							.replace(ClientSpec.DATE_FORMAT_REGEX,
									clientSpecDate)
							.replace(CommonConstants.EXEC_ID, this.execid),
					ImmutableMap.of(
							"Client",
							clientSpec.getClient(),
							CommonConstants.LOG_LINK_TITLE,
							logLink,
							CommonConstants.HELP_LINK_TITLE,
							status.getHelpLink(),
							"Message",
							mcon.getBody() + CommonConstants.NEWLINE
									+ status.getMessage()));
		}
		Map<String, String> responseMessage = new HashMap<String, String>();
		Properties properties = getAzkabanFlowParam();
//		String filePath = System
//				.getProperty(CommonConstants.AZKABAN_JOB_PROPERTIES_FILE_PATH);
//		logger.debug(filePath);
//		properties.load(new FileInputStream(filePath));
		logger.debug("SQSRegion : "+properties.getProperty("SQSRegion"));
		logger.debug("responseSQS : "+properties.getProperty("responseSQS"));
		responseMessage.put(
				"requestID",
				properties.get("requestID") == null ? "" : properties.get(
						"requestID").toString());
		responseMessage.put("clientID", properties.get("clientID") == null ? ""
				: properties.get("clientID").toString());
		responseMessage.put("status", "failure");
		responseMessage.put("errorCode",
				Integer.toString(status.getStatusCode()));
		responseMessage.put("errorMessage", status.getMessage());
		responseMessage.put("errorDescription", status.getMessage());
		subject = subject
				+ mcon.getSubject().replace(ClientSpec.DATE_FORMAT_REGEX,
						clientSpecDate);
		initializer.getNotifier().notifyFailure(failureList, subject,
				responseMessage);

		if (status.getAbortRun())
			System.exit(-1);
	}

	private void setSuccessList() {
		if (serviceSpec.get("successEndpoints") != null) {
			for (Object val : (JSONArray) serviceSpec.get("successEndpoints")) {
				successList.add(val.toString());
			}
		}
	}

	private void setFailureList() {
		if (serviceSpec.get("failureEndpoints") != null) {
			JSONArray failEndpoints = (JSONArray) serviceSpec.get("failureEndpoints");
			logger.debug("*****"+failEndpoints.size()+"*****"+failEndpoints.toString());
			for (Object val : failEndpoints) {
				logger.debug("failure endPoint : "+val.toString());
				failureList.add(val.toString());
			}
		}
	}

	private void initalizeServiceSpec(String serviceSpecPath) throws Exception {
		fileReader = new FileReader(serviceSpecPath);
		JSONParser parser = new JSONParser();
		Object jsonElement = null;
		jsonElement = parser.parse(fileReader);
		serviceSpec = (JSONObject) jsonElement;
		serviceSpec = (JSONObject) parser.parse(RuntimeParametersUtility
				.replaceRuntimeParameters(serviceSpec.toString(),
						clientSpec.getClient()));

		JSONArray arr = (JSONArray) serviceSpec.get(PROPERTY_MESSAGE);
		for (int i = 0; i < arr.size(); i++) {
			JSONObject obj = (JSONObject) arr.get(i);
			MessageContentSpec con = new MessageContentSpec();
			con.setBody((String) obj.get(PROPERTY_MESSAGE_BODY));
			String clientSpecDate = clientSpec.getDate();
			con.setSubject((String) obj.get(PROPERTY_MESSAGE_SUBJECT),
					clientSpecDate, clientSpec.getClient());
			messageContent.put((String) obj.get(PROPERTY_MESSAGE_TYPE), con);
		}
		setSuccessList();
		setFailureList();
		logger.info("Abstract Service initalized");
	}

	public String calculateSpecDate(String runtimeDate, ClientSpec clientSpec)
			throws ParseException {
		String daysAgo = clientSpec.getDaysAgo();
		String dateFormat = clientSpec.getDateFormat();

		DateFormat format = new SimpleDateFormat(
				CommonConstants.DEFAULT_DATE_FORMAT, Locale.ENGLISH);
		Date date = format.parse(runtimeDate);

		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, (-1) * Integer.parseInt(daysAgo));
		date.setTime(c.getTime().getTime());
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);

		c.clear();
		return simpleDateFormat.format(date);
	}

	@SuppressWarnings("deprecation")
	public String replaceDynamicFolders(String location, String dateCommand)
			throws InterruptedException, IOException, ParseException {
		String daysAgo = dateCommand.split(",")[1];
		DateFormat format = new SimpleDateFormat(
				CommonConstants.DEFAULT_DATE_FORMAT, Locale.ENGLISH);
		Date date = format.parse(this.runtimeDate);
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, (-1) * Integer.parseInt(daysAgo));
		date.setTime(c.getTime().getTime());

		String month = (date.getMonth() + 1 < 10) ? ("0" + Integer
				.toString(date.getMonth() + 1)) : Integer.toString(date
				.getMonth() + 1);
		String day = (date.getDate() < 10) ? ("0" + Integer.toString(date
				.getDate())) : Integer.toString(date.getDate());

		logger.info(Integer.toString(date.getYear()) + " " + month + " " + day);
		location = location.replace(CommonConstants.YEAR,
				Integer.toString(date.getYear() + 1900));
		location = location.replace(CommonConstants.MONTH, month);
		location = location.replace(CommonConstants.DAY, day);

		c.clear();

		return location;
	}

	public File zip(File uncompressedFile, String fileName) throws Exception {
		File compressedFile = File.createTempFile("compressedZip", ".tmp");
		fileName = fileName.replace("zip", "csv");
		byte[] buffer = new byte[1024];
		FileOutputStream fos = new FileOutputStream(
				compressedFile.getAbsolutePath());
		ZipOutputStream zos = new ZipOutputStream(fos);
		ZipEntry ze = new ZipEntry(fileName);
		zos.putNextEntry(ze);
		FileInputStream in = new FileInputStream(
				uncompressedFile.getAbsolutePath());
		int len;
		while ((len = in.read(buffer)) > 0) {
			zos.write(buffer, 0, len);
		}
		in.close();
		zos.closeEntry();
		zos.close();
		logger.debug("zipping done");
		uncompressedFile.delete();
		return compressedFile;

	}

	public static String getAzkabanFlowParam(String key)
			throws FileNotFoundException, IOException {
		String value = null;
		Properties azkabanFlowParameters = null;
		String azkabanFlowParametersFilePath = System
				.getProperty(CommonConstants.AZKABAN_JOB_PROPERTIES_FILE_PATH);
		if (azkabanFlowParametersFilePath != null) {
			logger.debug("AZKABAN_JOB_PROPERTIES_FILE_PATH : "
					+ azkabanFlowParametersFilePath);
			azkabanFlowParameters = new Properties();
			azkabanFlowParameters.load(new FileInputStream(new File(
					azkabanFlowParametersFilePath)));
			value = (String) azkabanFlowParameters.get(key);
		}

		return value;

	}
	public static Properties getAzkabanFlowParam()
			throws FileNotFoundException, IOException {
		Properties azkabanFlowParameters = null;
		String azkabanFlowParametersFilePath = System
				.getProperty(CommonConstants.AZKABAN_JOB_PROPERTIES_FILE_PATH);
		if (azkabanFlowParametersFilePath != null) {
			logger.debug("AZKABAN_JOB_PROPERTIES_FILE_PATH : "
					+ azkabanFlowParametersFilePath);
			azkabanFlowParameters = new Properties();
			azkabanFlowParameters.load(new FileInputStream(new File(
					azkabanFlowParametersFilePath)));
			
		}

		return azkabanFlowParameters;

	}

	public String replaceDateTag(String fileName, String dateCommand)
			throws Exception {
		String dateFormat = dateCommand.split(",")[0];
		String daysAgo = dateCommand.split(",")[1];
		DateFormat format = new SimpleDateFormat(
				CommonConstants.DEFAULT_DATE_FORMAT, Locale.ENGLISH);
		Date date = format.parse(this.runtimeDate);
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, (-1) * Integer.parseInt(daysAgo));
		date.setTime(c.getTime().getTime());
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
		fileName = fileName.replace(ClientSpec.DATE_FORMAT_REGEX,
				simpleDateFormat.format(date));

		c.clear();
		return fileName;
	}

	public String makeRegex(String input) {
		if (!input.contains("{{regex}}"))
			return input;

		System.out.println("make regex");
		input = input.replaceAll("\\.", "\\\\.");
		input = input.replaceAll(CommonConstants.REGEX_TAG,
				CommonConstants.REGEX_STRING);

		return input;
	}

	@SuppressWarnings("rawtypes")
	public String getHeaderLine(String fileName, String feedMetadata,
			String headerFileNameInFeedMetaData) throws IOException {
		logger.info(feedMetadata + " - FeedMetadata Location");
		logger.info(fileName + " - FeedName");

		StringBuilder builder = new StringBuilder(fileName);
		if (builder.toString().endsWith(".txt.gz"))
			builder = builder.delete(fileName.length() - 7, fileName.length());
		else
			builder = builder.delete(fileName.length() - 4, fileName.length());
		if (Character.isDigit(builder.toString().charAt(builder.length() - 1))) {
			builder = builder.delete(builder.length() - 8, builder.length());
			if (builder.toString().endsWith("_"))
				builder = builder
						.delete(builder.length() - 1, builder.length());
		}

		String neededHeader = builder.toString();
		if (headerFileNameInFeedMetaData != null)
			neededHeader = headerFileNameInFeedMetaData;

		logger.debug("needed headerfile : " + neededHeader);
		String expectedHeader = "";
		Iterator it = FileUtils.iterateFiles(new File(feedMetadata), null,
				false);
		while (it.hasNext()) {
			File headerFile = (File) it.next();
			StringBuilder headerFileName = new StringBuilder(
					headerFile.getName());
			logger.info("Header File Now:" + headerFileName.toString());
			headerFileName = headerFileName.delete(headerFileName.length() - 4,
					headerFileName.length());

			if (neededHeader.equals(headerFileName.toString())) {
				InputStream headerFileStream = new FileInputStream(headerFile);
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(headerFileStream));
				expectedHeader = reader.readLine();
				reader.close();
				headerFileStream.close();
				break;
			}
		}

		return expectedHeader;
	}

}
