package com.rboomerang.processor.impl.common;
/**
 * Description: Omniture Reporting api Processor
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;
import com.rboomerang.utils.AdobeOmnitureAPI;

public class OmnitureReportProcessor extends AbstractService 
{
	private static Logger logger = LoggerFactory.getLogger(OmnitureReportProcessor.class);
	public OmnitureReportProcessor(String clientSpecpath, String processorSpecPath) throws Exception
	{
		super(clientSpecpath, processorSpecPath);
		logger.info("OmnitureProcessor constructor called");
	}
	public OmnitureReportProcessor(String clientSpecpath, String processorSpecPath, String runtimeDate) throws Exception
	{
		super(clientSpecpath, processorSpecPath,runtimeDate);
		logger.info("OmnitureProcessor constructor called");
	}
	public OmnitureReportProcessor(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext, String runtimeDate) throws Exception 
	{
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.info("OmnitureProcessor constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject processorSpec) throws Exception
	{
		logger.info("OmnitureReport Processor execution started");
		//Reading Omniture Reporting API global parameters
		String clientName=clientSpec.getClient();
		JSONObject accountParams = (JSONObject) processorSpec.get("account");
		JSONObject apiQuery= (JSONObject) processorSpec.get(CommonConstants.QUERY);
		String userName= accountParams.get("user").toString();
		String password = accountParams.get("secret").toString();
		String feedDate =apiQuery.get("feedDate").toString();
		feedDate = replaceDateTag(ClientSpec.DATE_FORMAT_REGEX,"yyyy-MM-dd,"+feedDate);
		//Invoking Omniture Reporting API query
		AdobeOmnitureAPI reportObject =new AdobeOmnitureAPI(clientName,userName,password,apiQuery,feedDate);
		logger.debug("Writing the file to endPoint for backup!!");
		IEndPointResource endPointResource=null;
		Status status=null;
		try
		{
			JSONObject endpointDetails=(JSONObject) processorSpec.get(CommonConstants.DESTINATION);
			String feedFileNameCommerce = (String)endpointDetails.get(CommonConstants.FILENAME+"_commerce");
			String feedFileNameTraffic = (String)endpointDetails.get(CommonConstants.FILENAME+"_traffic");
			String destDateSuffix = (String)endpointDetails.get(CommonConstants.FEED_DATE);
			String location = (String)endpointDetails.get(CommonConstants.LOCATION);
			if (destDateSuffix == null || destDateSuffix.isEmpty() )
			{
				destDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
			}
			
			feedFileNameCommerce = replaceDateTag(feedFileNameCommerce, destDateSuffix);
			feedFileNameTraffic = replaceDateTag(feedFileNameTraffic, destDateSuffix);
			logger.debug("output file names are "+feedFileNameCommerce+"\t"+feedFileNameTraffic);
			//Getting endPoint specific details
			String endpointName = (String)endpointDetails.get(CommonConstants.ENDPOINTNAME);
			String endpointType = getTypeOfEndPoint(clientSpec,endpointName);
			HashMap<String, String> endPointResourceProperties = buildResourceProperties(endpointName, clientSpec,endpointType);
			endPointResource = EndPointResourceFactory.getInstance().getEndPointResource(endpointType,endPointResourceProperties);
			logger.debug("Uploading file to "+endPointResourceProperties);
			String fileNames[]=reportObject.start();
			//Writing both files to the same endPoint
			for(String file:fileNames)
			{
				InputStream in= new FileInputStream(new File(file));
				File outputTempFile=new File(file);
				if(outputTempFile !=null)
				{
					endPointResource.writeFile(in,location,feedFileNameCommerce);
					logger.info(String.format("Successfully uploaded!! file path is %s %s%s",endpointType,location,feedFileNameCommerce));
					boolean delStatus=outputTempFile.delete();
					if(delStatus)
					{
						logger.info("Temporary file "+file+" has been deleted!!");
					}
					else
					{
						logger.info("Unable to delete temp file "+file);
					}
					feedFileNameCommerce=feedFileNameTraffic;
				}
			}
			status = new Status(Status.STATUS_SUCCESS, "Omniture Metric report generated successfully!!");
		}catch(Exception e)
		{
			status = new Status(Status.STATUS_ERROR, "Failed to generate Report!!");
			e.printStackTrace();
		}
		finally
		{
			endPointResource.closeConnection();
			logger.debug("Connection to endpoint Closed!!");
		}
		return status;
	}
}