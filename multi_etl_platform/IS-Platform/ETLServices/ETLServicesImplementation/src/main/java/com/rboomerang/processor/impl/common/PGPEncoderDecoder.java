package com.rboomerang.processor.impl.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.InputStreamHandler;
import com.rboomerang.utils.PGPFileProcessor;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

public class PGPEncoderDecoder  extends AbstractService   {
	private static final Logger logger = LoggerFactory.getLogger(PGPEncoderDecoder.class);
	
	public PGPEncoderDecoder(String clientSpecpath, String processorSpecPath) throws Exception {
		super(clientSpecpath, processorSpecPath);
		logger.info("PGPEncoderDecoder constructor called");
	}
	public PGPEncoderDecoder(String clientSpecpath, String processorSpecPath,String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeDate);
		logger.info("PGPEncoderDecoder constructor called");
	}
	public PGPEncoderDecoder(String clientSpecpath, String processorSpecPath,RuntimeContext runtimeContext,String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath,runtimeContext, runtimeDate);
		logger.info("PGPEncoderDecoder constructor called");
	}
	public Status service(ClientSpec clientSpec, JSONObject processorSpec)
			throws JSchException, SftpException, Exception {

		JSONObject list = (JSONObject)processorSpec.get(CommonConstants.LIST);
		JSONObject source = (JSONObject)list.get(CommonConstants.SOURCE);
		JSONObject dest = (JSONObject)list.get(CommonConstants.DESTINATION);

		String sourceDateSuffix = (String) source.get(CommonConstants.FEED_DATE);
		String destDateSuffix = (String) dest.get(CommonConstants.FEED_DATE);
		if ( sourceDateSuffix == null || sourceDateSuffix.isEmpty())
			sourceDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
		if (destDateSuffix == null || destDateSuffix.isEmpty() )
			destDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
		
		//getting source Endpoint details
		String sourceEndPoint = (String)source.get(CommonConstants.ENDPOINTNAME);
		//getting source file path from spec file and replacing date month or year from it!
		String sourcePath = (String)source.get(CommonConstants.PATH);
		//getting source file name from spec file and replacing feedDate from it!
		String sourceFileName = (String)source.get(CommonConstants.FILE_NAME);

		//getting Destination Endpoint details
		String destEndPoint = (String)dest.get(CommonConstants.ENDPOINTNAME);
		
		//getting destination file path from spec file and replacing date month or year from it!
		String destPath = (String)dest.get(CommonConstants.PATH);
		//getting destination file path from spec file and replacing feedDate from it!
		String destFileName = (String)dest.get(CommonConstants.FILE_NAME);
		destPath = replaceDynamicFolders(destPath, destDateSuffix);
		sourcePath = replaceDynamicFolders(sourcePath, sourceDateSuffix);
		destFileName = replaceDateTag(destFileName, destDateSuffix);
		sourceFileName = replaceDateTag(sourceFileName, sourceDateSuffix);
		logger.debug("Input File Source Endpoint :: "+sourceEndPoint);
		logger.debug("Output File destination Endpoint:: "+destEndPoint);
		logger.debug("Input File Source Path --> "+sourcePath+" :: FileName --> "+sourceFileName);
		logger.debug("Output File destination --> "+destPath+" :: and FileName --> "+destFileName);
		
		String key = this.getRuntimeContext().transformPath((String)list.get("key"));
		String publicKey=this.getRuntimeContext().transformPath((String)list.get("publicKey"));
		String passPhrase = (String)list.get("passphrase");
		String mode = (String)list.get("pgpMode");
		logger.debug(" mode --> "+mode);

		if(!mode.equals("encode") && !mode.equals("decode"))
			{
			logger.error("This mode is not supported by the Processor : "+mode);
			return new Status(Status.STATUS_ERROR, "This mode is not supported by the Processor : "+mode);
			}
		//creating temporary file to download file from endpoint
		File sourceDownloadedFile = File.createTempFile("sourceFile",".tmp");

		//getting source endpoint properties		
		String sourceType = super.getTypeOfEndPoint(clientSpec, sourceEndPoint);
		if (sourceType.equals("sftp"))
			sourceFileName = makeRegex(sourceFileName);
		
		HashMap<String, String> sourceEndPointResourceProperties = super.buildResourceProperties(sourceEndPoint, clientSpec,sourceType);

		//making connection with source endpoint
		IEndPointResource sourceEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(
				sourceType, sourceEndPointResourceProperties);

		//downloading the sourceFile
		InputStream inputStream = sourceEndPointResource.getFile(sourcePath, sourceFileName);

		String tempFilePath = sourceDownloadedFile.getParent();
		String pgpFileName= destFileName;
		if(pgpFileName.contains(".pgp"))
			pgpFileName=pgpFileName.split(".pgp")[0];
		else if(pgpFileName.contains(".gpg"))
			pgpFileName=pgpFileName.split(".gpg")[0];
		sourceDownloadedFile.delete();
		
		sourceDownloadedFile=new File(tempFilePath+"/"+destFileName);
		
		//writing sourceInputStream to file
		InputStreamHandler.writeBufferedInputStreamToFile(inputStream,sourceDownloadedFile.getAbsolutePath());
		sourceEndPointResource.closeConnection();

		//creating temporary file to write encrypted data on that file
		File processedSourceFile = File.createTempFile("processedSourceFile",".tmp");
		PGPFileProcessor pgpFileProcessor = new PGPFileProcessor();
		pgpFileProcessor.setInputFileName(sourceDownloadedFile.getAbsolutePath());
		pgpFileProcessor.setOutputFileName(processedSourceFile.getAbsolutePath());
		pgpFileProcessor.setPassphrase(passPhrase);
		
		if(mode.equals("encode")){
			//calling pgp encryption method on the source file
			pgpFileProcessor.setPublicKeyFileName(publicKey);
			pgpFileProcessor.encrypt();
			logger.debug("Encryption complete!!");
		}
		else
		{
			//calling pgp decryption method on the source file
			pgpFileProcessor.setSecretKeyFileName(key);
			pgpFileProcessor.setPublicKeyFileName(publicKey);
			pgpFileProcessor.decrypt();
			logger.debug("Decryption complete!!");
		}
		//getting destination end point properties
		String destType = getTypeOfEndPoint(clientSpec, destEndPoint);
		HashMap<String, String> destEndPointResourceProperties = super.buildResourceProperties(destEndPoint, clientSpec,destType);

		//making connection with source endpoint
		IEndPointResource destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(
				destType, destEndPointResourceProperties);

		//uploading encrypted file
		destEndPointResource.writeFile(new FileInputStream(new File(processedSourceFile.getAbsolutePath())), destPath, destFileName);
		destEndPointResource.closeConnection();

		//deleting all temporary files
		sourceDownloadedFile.delete();
		processedSourceFile.delete();
		logger.info("deleting all temp files!");

		return new Status(Status.STATUS_SUCCESS, "Execution "+mode+" successful for file "+sourceFileName+" in PGPEncoderDecoder");
	}

}
