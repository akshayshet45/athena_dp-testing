package com.rboomerang.custom.officedepot;

public class ProductUtility
{
       private String sku;
       private String listprice;
       private String salesprice;
       private String productname;
       private String producturl;
       private String brand;
       private String model;
       private String department;
       private String color;
       private String size;
       private String dimensions;
       private String material;
       private String gender;
       private String description;
       private String image_link;
       public ProductUtility()
       {
       }
       public ProductUtility(String sku, String listprice, String salesprice, String productname) 
       {
              this.sku = sku;
              this.salesprice = salesprice;
              this.listprice = listprice;
              this.productname = productname;
       }

       public String getsalesprice() 
       {
              return salesprice;
       }

       public void setsalesprice(String salesprice) 
       {
              this.salesprice = salesprice;
       }

       public String getlistprice() 
       {
              return listprice;
       }

       public void setlistprice(String listprice)
       {
              this.listprice = listprice;
       }

       public String getsku()
       {
              return sku;
       }

       public void setsku(String sku)
       {
              this.sku = sku;
       }

       public String getproductname() 
       {
              return productname;
       }

       public void setproductname(String productname) 
       {
              this.productname = productname;
       }

       public String getProducturl() 
       {
		return producturl;
       }

       public void setProducturl(String producturl) 
       {
		this.producturl = producturl;
       }

       public String getBrand()
       {
		return brand;
       }

       public void setBrand(String brand) 
       {
		this.brand = brand;
       }

       public String getModel() 
       {
		return model;
       }

       public void setModel(String model) 
       {
		this.model = model;
       }

       public String getDepartment() 
       {
		return department;
       }

       public void setDepartment(String department) 
       {
		this.department = department;
       }

       public String getColor() 
       {
		return color;
       }

       public void setColor(String color) 
       {
		this.color = color;
       }

       public String getSize() 
       {
		return size;
       }

       public void setSize(String size) 
       {
		this.size = size;
       }

       public String getDimensions() 
       {
		return dimensions;
       }

		public void setDimensions(String dimensions) 
		{
			this.dimensions = dimensions;
		}
	
		public String getMaterial() 
		{
			return material;
		}
	
		public void setMaterial(String material) 
		{
			this.material = material;
		}
	
		public String getGender() 
		{
			return gender;
		}
	
		public void setGender(String gender) 
		{
			this.gender = gender;
		}
	
		public String getDescription() 
		{
			return description;
		}
	
		public void setDescription(String description) 
		{
			this.description = description;
		}
	
		public String getImage_link() 
		{
			return image_link;
		}
	
		public void setImage_link(String image_link) 
		{
			this.image_link = image_link;
		}
	
		public String toString()
		{
	              StringBuffer sb = new StringBuffer();
	              if(getsku() != null && !getsku().isEmpty())
	              {
	            	  sb.append("\"" + getsku().replaceAll("\"", "").replaceAll(",", "").replaceAll("(\\r|\\n|\\r\\n)+", "")+"\"");//sku
	                  sb.append(",");
	              }
	              else
	              {
	            	  sb.append("\"\",");
	              }
	              
	              if(getproductname() != null && !getproductname().isEmpty())
	              {
	            	  sb.append("\"" + getproductname().replaceAll("\"", "").replaceAll(",", "").replaceAll("(\\r|\\n|\\r\\n)+", "")+"\"");//Title
	                  sb.append(",");
	              }
	              else
	              {
	            	  sb.append("\"\",");
	              }
	              
	              if(getlistprice() != null && !getlistprice().isEmpty())
	              {
	            	  sb.append("\"" + getlistprice().replaceAll("\"", "").replaceAll(",", "").replaceAll("(\\r|\\n|\\r\\n)+", "")+"\"");//LP
	                  sb.append(",");
	              }
	              else
	              {
	            	  sb.append("\"\",");
	              }
	              
	              if(getsalesprice() != null && !getsalesprice().isEmpty())
	              {
	            	  sb.append("\"" + getsalesprice().replaceAll("\"", "").replaceAll(",", "").replaceAll("(\\r|\\n|\\r\\n)+", "")+"\"");//SP
	                  sb.append(",");
	              }
	              else
	              {
	            	  sb.append("\"\",");
	              }
	              
	              if(getProducturl() != null && !getProducturl().isEmpty())
	              {
	            	  sb.append("\"" + getProducturl().replaceAll("\"", "").replaceAll(",", "").replaceAll("(\\r|\\n|\\r\\n)+", "")+"\"");//Product URL
	                  sb.append(",");
	              }
	              else
	              {
	            	  sb.append("\"\",");
	              }
	              
	              
	              if(getImage_link() != null && !getImage_link().isEmpty())
	              {
	            	  sb.append("\"" + getImage_link().replaceAll("\"", "").replaceAll(",", "").replaceAll("(\\r|\\n|\\r\\n)+", "")+"\"");//sku
	                  sb.append(",");
	              }
	              else
	              {
	            	  sb.append("\"\",");
	              }
	              
	              if(getBrand() != null && !getBrand().isEmpty())
	              {
	            	  sb.append("\"" + getBrand().replaceAll("\"", "").replaceAll(",", "").replaceAll("(\\r|\\n|\\r\\n)+", "")+"\"");//Brand
	                  sb.append(",");
	              }
	              else
	              {
	            	  sb.append("\"\",");
	              }
	              
	              if(getModel() != null && !getModel().isEmpty())
	              {
	            	  sb.append("\"" + getModel().replaceAll("\"", "").replaceAll(",", "").replaceAll("(\\r|\\n|\\r\\n)+", "")+"\"");//Model
	                  sb.append(",");
	              }
	              else
	              {
	            	  sb.append("\"\",");
	              }
	              
	              if(getDescription() != null && !getDescription().isEmpty())
	              {
	            	  sb.append("\"" + getDescription().replaceAll("\"", "").replaceAll(",", "").replaceAll("(\\r|\\n|\\r\\n)+", "")+"\"");//productdescription:Wq
	              }
	              else
	              {
	            	  sb.append("\"\"");
	              }
	              sb.append("\n");
	              return sb.toString();
	       }
}
