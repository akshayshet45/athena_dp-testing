package com.rboomerang.validator.impl.common;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.common.framework.javabeans.DbSpec;
import com.rboomerang.processor.impl.common.RecordCountReport;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.SendEmail;
import com.rboomerang.utils.SqlHelper;

public class CountDegradationAlert extends AbstractService {

	private static Logger logger = LoggerFactory
			.getLogger(RecordCountReport.class);

	public CountDegradationAlert(String clientSpecpath,
			String processorSpecPath, RuntimeContext runtimeContext,
			String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.info("RecordCountReport constructor called");
	}

	public CountDegradationAlert(String clientSpecpath,
			String validatorSpecPath, String runtimeDate) throws Exception {
		super(clientSpecpath, validatorSpecPath, runtimeDate);
		logger.info("RecordCountReport constructor called");
	}

	public CountDegradationAlert(String clientSpecpath, String validatorSpecPath)
			throws Exception {
		super(clientSpecpath, validatorSpecPath);
		logger.info("RecordCountReport constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject serviceSpec)
			throws JSchException, SftpException, Exception {

		Status status = new Status(Status.STATUS_SUCCESS,
				"CountDegradationAlert executed Successfully");
		Object[] dbtype = getDbDetails((String) serviceSpec
				.get(CommonConstants.ENDPOINT));
		String recipients = (String) serviceSpec
				.get(CommonConstants.RECIPIENT_LIST);
		String messageHeading = (String) serviceSpec
				.get(CommonConstants.MESSAGE_HEADING);
		String messageBody = (String) serviceSpec.get("messageBody");
		JSONArray list = (JSONArray) serviceSpec.get(CommonConstants.LIST);
		String clientTag = clientSpec.getClient();
		DbSpec db = (DbSpec) dbtype[1];
		String feed_date = null;

		feed_date = getFeedDate(clientSpec, serviceSpec);
		logger.debug("feed_date: " + feed_date);

		feed_date = "2016-05-28 00:00:00";
		logger.info("feed_date: " + feed_date);
		logger.info("clientTag: " + clientTag);
		logger.debug("DB Details: \n Host = " + db.getHost() + "Database = "
				+ db.getDb() + "user = " + db.getUname());
		ArrayList<String> alertList = checkCount(db, list, feed_date);
		if (!alertList.isEmpty()) {
			sendReport(recipients, messageHeading, messageBody, alertList);
		}

		return status;
	}

	private ArrayList<String> checkCount(DbSpec db, JSONArray list,
			String feed_date) throws SQLException, ClassNotFoundException {
		float degradPercentage = 0;
		SqlHelper redshift = null;
		float todayCount = -1;
		float aggCount = -1;
		ArrayList<String> alertList = new ArrayList<String>();

		try {
			redshift = new SqlHelper(db, "redshift");
		} catch (SQLException e) {
			throw new SQLException(
					"SQLException while creating SqlHelper instance/getting Redshift JDBC Connection ,error Msg: "
							+ e.getMessage() + "\n" + e);
		}

		for (int i = 0; i < list.size(); i++) {
			float degradeLimit = 0;
			String dateCol = null;
			int aggDays = 1;
			String aggFunction = null;
			JSONArray tableList = null;
			if (!list.isEmpty()) {
				JSONObject confObj = (JSONObject) list.get(i);
				if (confObj.containsKey("degradationLimit")) {
					String limit = ((String) confObj.get("degradationLimit"))
							.trim();
					degradeLimit = Float.parseFloat(limit.trim());
				}
				if (confObj.containsKey("dateColumn")) {
					dateCol = ((String) confObj.get("dateColumn")).trim();
				}
				if (confObj.containsKey("days")) {
					String addDays = ((String) confObj.get("days")).trim();
					aggDays = Integer.parseInt(addDays);
				}
				if (confObj.containsKey("aggregateFunction")) {
					aggFunction = ((String) confObj.get("aggregateFunction"))
							.trim();
				}
				if (confObj.containsKey("tables")) {
					tableList = (JSONArray) confObj.get("tables");
				}
			}
			for (int j = 0; j < tableList.size(); j++) {
				String tableName = ((String) tableList.get(j)).trim();
				todayCount = getTodaysCount(redshift, feed_date, dateCol,
						tableName);
				aggCount = getAggrigatedCount(redshift, feed_date, aggFunction,
						dateCol, aggDays, tableName);

				if (todayCount < aggCount && aggCount != 0) {
					System.out
							.println("todaycount while calculate degradation: "
									+ todayCount);
					System.out.println("aggCount while calculate degradation: "
							+ aggCount);
					degradPercentage = 100 - ((todayCount / aggCount) * 100);
					System.out.println("degradPercentage :" + degradPercentage);
				} else {
					degradPercentage = 0;
				}

				if (degradPercentage > degradeLimit) {
					alertList.add(degradPercentage
							+ " % degradation found for Table : " + tableName
							+ " for Date :" + feed_date
							+ " in terms of row count. Today's Row Count: " + todayCount
							+ " , Agreegated Row Count (" + aggFunction + " of count of " + aggDays
							+ " days) : " + aggCount);
				}
			}
		}
		System.out.println("alertList : " + alertList);
		return alertList;
	}

	private long getAggrigatedCount(SqlHelper redshift, String feed_date,
			String aggFunction, String dateCol, int aggDays, String tableName)
			throws SQLException {
		String sql = "select " + aggFunction
				+ "(count) as aggregatecount from(select count(*) as count,"
				+ dateCol + " from " + tableName + " where " + dateCol
				+ ">=(select dateadd(d,-" + aggDays + ",'" + feed_date
				+ "')) and " + dateCol + "<'" + feed_date + "' group by "
				+ dateCol + ")";

		ResultSet rs = redshift.getSelectData(sql);
		long aggCount = 0;
		try {
			if (rs != null) {
				if (rs.next()) {
					aggCount = rs.getLong("aggregatecount");
					// System.out.println("aggCount: " + aggCount);
				}
			}
		} catch (SQLException e) {
			throw new SQLException(
					"SQLException while Trying to getting AggrigatedCount ,error Msg: "
							+ e.getMessage() + "\n" + e);
		}
		return aggCount;

	}

	private long getTodaysCount(SqlHelper redshift, String feed_date,
			String dateCol, String tableName) throws SQLException {
		String sql = "select count(*) as todaycount from " + tableName
				+ " where " + dateCol + "='" + feed_date + "'";
		ResultSet rs = redshift.getSelectData(sql);
		long todayCount = 0;
		try {

			if (rs != null) {
				if (rs.next()) {
					todayCount = rs.getLong("todaycount");
					// System.out.println("todaycount: " + todayCount);
				}
			}
		} catch (SQLException e) {
			throw new SQLException(
					"SQLException while Trying to getting TodaysCount ,error Msg: "
							+ e.getMessage() + "\n" + e);
		}
		return todayCount;
	}

	private String getFeedDate(ClientSpec clientSpec, JSONObject serviceSpec)
			throws Exception {

		String feed_date = (String) serviceSpec.get("feed_date");
		if (feed_date == null || feed_date.equals(""))
			feed_date = clientSpec.getDateFormat() + CommonConstants.COMMA
					+ clientSpec.getDaysAgo();
		try {
			feed_date = replaceDateTag("{{feed_date}}", feed_date);
		} catch (Exception e1) {
			throw new Exception("Exception while getting Feed Date ,error Msg:"
					+ e1.getMessage() + "\n" + e1);
		}
		DateFormat destdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat srcdf = new SimpleDateFormat("yyyyMMdd");
		Date srcdate = null;
		try {
			srcdate = srcdf.parse(feed_date);
		} catch (ParseException e) {
			throw new ParseException(
					"ParseException while getting Feed Date ,error Msg:"
							+ e.getMessage() + "\n" + e, e.getErrorOffset());
		}
		String destdate = destdf.format(srcdate);

		return destdate;

	}

	private void sendReport(String recipients, String messageHeading,
			String messageBody, ArrayList<String> alertList) {
		String msgBody=messageBody+"\n";
		for (String alertMsg : alertList) {
			msgBody+="\n"+alertMsg;
		}
		msgBody+=" \n \n You will not receive this email on days when there are no degradation in terms of percentage record count. \n \n Good day!, \nBoomerang Commerce";
		SendEmail.send(recipients,messageHeading,msgBody);
	}
	

}
