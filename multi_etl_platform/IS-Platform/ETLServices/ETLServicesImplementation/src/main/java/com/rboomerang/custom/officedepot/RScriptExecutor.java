package com.rboomerang.custom.officedepot;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

public class RScriptExecutor extends AbstractService
{
	private static final Logger logger = LoggerFactory.getLogger(RScriptExecutor.class);
	private String outputFileLocation="/home/ubuntu/is-platform/OfficeDepot/scripts/GNL/";
	public RScriptExecutor(String clientSpecpath, String processorSpecPath) throws Exception
	{
		super(clientSpecpath, processorSpecPath);
		logger.info("RScriptExecutor constructor called");
	}

	public RScriptExecutor(String clientSpecpath, String processorSpecPath, String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeDate);
		logger.info("RScriptExecutor constructor called");
	}

	public RScriptExecutor(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext,
			String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.info("RScriptExecutor constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject serviceSpec) throws JSchException, SftpException, Exception
	{
		JSONObject rParams= (JSONObject)serviceSpec.get("params");
		JSONObject sourceParams= (JSONObject)serviceSpec.get(CommonConstants.SOURCE);
		JSONObject destParams= (JSONObject)serviceSpec.get(CommonConstants.DESTINATION);
		Status status=new Status(Status.STATUS_ERROR,"RScript Execution Failed!!");
		//Downloading PO_Ouput File(s)
		logger.info("Downloading PO ouptut Files from source");
		logger.debug("Reading source and dest parameters ");
		String sourceLocation=sourceParams.get(CommonConstants.LOCATION).toString();
		String sourceFileNames=sourceParams.get(CommonConstants.FILE_NAME).toString();
		String sourceFeedDate=sourceParams.get(CommonConstants.FEED_DATE).toString();
		String destLocation=destParams.get(CommonConstants.LOCATION).toString();
		
		String destFeedDate=destParams.get(CommonConstants.FEED_DATE).toString();
		if(sourceFeedDate==null || sourceFeedDate.isEmpty())
		{
			sourceFeedDate=clientSpec.getDateFormat()+CommonConstants.COMMA+clientSpec.getDaysAgo();
		}
		if(destFeedDate==null || destFeedDate.isEmpty())
		{
			destFeedDate=clientSpec.getDateFormat()+CommonConstants.COMMA+clientSpec.getDaysAgo();
		}
		sourceLocation=replaceDynamicFolders(sourceLocation,sourceFeedDate);
		destLocation=replaceDateTag(destLocation,destFeedDate);
		//Parsing source FileNames (# seperated)
		List<String> sFiles=new ArrayList<String>();
		StringTokenizer parser=new StringTokenizer(sourceFileNames,"#");
		
		//Defining source and destination endPoints as multiple files need to be uploaded
		String sourceEndPointName=sourceParams.get(CommonConstants.ENDPOINTNAME).toString();
		String sourceEndPointType= super.getTypeOfEndPoint(clientSpec, sourceEndPointName);
		HashMap<String, String> sourceEndPointResourceProperties = buildResourceProperties(sourceEndPointName, clientSpec,sourceEndPointType);
		IEndPointResource sourceEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(sourceEndPointType,sourceEndPointResourceProperties);
		
		String destEndPointName=destParams.get(CommonConstants.ENDPOINTNAME).toString();
		String destEndPointType= super.getTypeOfEndPoint(clientSpec, destEndPointName);
		HashMap<String, String> destEndPointResourceProperties = buildResourceProperties(destEndPointName, clientSpec,destEndPointType);
		IEndPointResource destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(destEndPointType,destEndPointResourceProperties);
		
		while(parser.hasMoreTokens())
		{
			sFiles.add(parser.nextToken());
		}
		//Download The files
		
		for(String file:sFiles)
		{
			logger.info("Cleaning up the directory to remove old files..!!");
			for(File t: new File(outputFileLocation+"data").listFiles())
			{
				if(!t.delete())
				{
					logger.error("Error in cleaning up direcotry");
					throw new IOException("Cleaning up directory failed!! Aborting run!!");
				}
				
			}
			file=replaceDateTag(file,sourceFeedDate);
			String key=file+"/";
			logger.debug("Downloading file from "+sourceEndPointResourceProperties);
			InputStream in=sourceEndPointResource.getFile(sourceLocation, file); 
			Files.copy(in, Paths.get(outputFileLocation+"data/"+file));
			logger.debug("Done!! file is stored at "+outputFileLocation+"data/"+file);
			logger.debug("Executing RScripts for the file "+file);
			File[] listOuputFiles=executeRScripts(rParams);
			logger.info("Uploading files to destination "+destEndPointResourceProperties);
			for(File f:listOuputFiles)
			{
				String name=f.getName();
				if(name.endsWith(".xlsx")||name.endsWith(".csv"))
				{
					InputStream inputStream=new FileInputStream(f);
					destEndPointResource.writeFile(inputStream, destLocation+key, name);
					//delete the file locally upon upload
					boolean delStatus=f.delete();
					if(delStatus)
					{
						logger.debug("file "+name+" has been deleted");
					}
					else
					{
						logger.warn("Unable to delete temp file "+name);
					}
				}
				else
				{
					f.delete();
				}
					
			}
		}
		status=new Status(Status.STATUS_SUCCESS,"RScript Execution succeeded!!");
		return status;
	}
	 
	private File[] executeRScripts(JSONObject rParams) throws IOException, InterruptedException
	{
		Runtime runEnv= Runtime.getRuntime();
		StringBuilder command=new StringBuilder();
		command.append("/usr/bin/Rscript").append(CommonConstants.SPACE)
			   .append(outputFileLocation).append("batch.apply.price.constraints.R");
		logger.debug("Running command "+command.toString());
		Process child= runEnv.exec(command.toString());
		int exitValue=-1;
		exitValue=child.waitFor();
		if(exitValue!=0)
		{
			//Printing the script output/error for debug
			Scanner errorScan=new Scanner(child.getErrorStream());
			String err="";
			while(errorScan.hasNext())
			{
				err += errorScan.nextLine();
			}
			errorScan.close();
			logger.error(err);
			throw new IOException(err);
		}
		else
		{
			logger.debug("RScripts Execution is successfull");
			//Retrieving generated output Files and Writing them to Destination end point
			File[] listDir =new File(outputFileLocation+"data").listFiles();
			logger.info("GNL exectuion completed");
			return listDir;
		}
	}
	
}
