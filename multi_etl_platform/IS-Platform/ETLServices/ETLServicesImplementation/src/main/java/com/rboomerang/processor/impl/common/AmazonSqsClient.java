package com.rboomerang.processor.impl.common;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;

public class AmazonSqsClient extends AbstractService {

	private static final String ENV = "env";
	private static final String CLIENTNAMEDTAGGED = "clientNamedTagged";
	private static final String TIMEOUTINMINUTE="timeoutInMinute";
	Status status = new Status(Status.STATUS_SUCCESS, "Running Success");
	private static Logger logger = LoggerFactory.getLogger(AmazonSqsClient.class);
	public AmazonSqsClient(String clientSpecpath, String processorSpecPath) throws Exception {
		super(clientSpecpath, processorSpecPath);
		logger.info("AmazonSqsClient constructor called");
	}
	public AmazonSqsClient(String clientSpecpath, String processorSpecPath, String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath,runtimeDate);
		logger.info("AmazonSqsClient constructor called");
	}
	public AmazonSqsClient(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext, String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.info("AmazonSqsClient constructor called");
	}
	@Override
	public Status service(ClientSpec clientSpec, JSONObject serviceSpec)
			throws JSchException, SftpException, Exception {
		int timeoutInMinute;
		String clientName=clientSpec.getClient();
		String env=(String) serviceSpec.get(ENV);
		String clientNamedTagged=(String) serviceSpec.get(CLIENTNAMEDTAGGED);
		try{
		try{
		String timeoutInMinuteString=(String) serviceSpec.get(TIMEOUTINMINUTE);
		timeoutInMinute=Integer.parseInt(timeoutInMinuteString);
		}catch(Exception e){
			logger.info("Exception : "+e);
			throw new Exception("Exception: Non integer value provided in timeoutInMinute field "+e.getMessage());
		}
		
		AmazonSqsClientWaiter queueMsgRdr = new AmazonSqsClientWaiter(clientName);
		queueMsgRdr.initialize(queueMsgRdr, clientName, env, clientNamedTagged, timeoutInMinute);
		}catch(Exception e){
			status.setStatusCode(Status.STATUS_ERROR);
			status.setMessage("Exception : "+e.getMessage());
			logger.info("Exception : "+e.getMessage());
		}
		return status;
	}
}
