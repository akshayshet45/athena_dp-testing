package com.rboomerang.validator.impl.common;

import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.opencsv.CSVReader;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.common.framework.javabeans.DbSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.SendEmail;
import com.rboomerang.utils.SqlHelper;

public class DuplicateValidator extends AbstractService {

	private static Logger logger = LoggerFactory
			.getLogger(DuplicateValidator.class);

	public DuplicateValidator(String clientSpecpath, String validatorSpecPath,
			RuntimeContext runtimeContext, String runtimeDate) throws Exception {
		super(clientSpecpath, validatorSpecPath, runtimeContext, runtimeDate);
		logger.info("DuplicateValidator constructor called");
	}

	public DuplicateValidator(String clientSpecpath, String validatorSpecPath,
			String runtimeDate) throws Exception {
		super(clientSpecpath, validatorSpecPath, runtimeDate);
		logger.info("DuplicateValidator constructor called");
	}

	public DuplicateValidator(String clientSpecpath, String serviceSpecPath)
			throws Exception {
		super(clientSpecpath, serviceSpecPath);
		// TODO Auto-generated constructor stub
		logger.info("DuplicateValidator Constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject serviceSpec)
			throws JSchException, SftpException, Exception {
		// TODO Auto-generated method stub
		JSONArray fileToVerify = (JSONArray) serviceSpec
				.get(CommonConstants.LIST);

		Status finalStatus = null;

		for (int i = 0; i < fileToVerify.size(); i++) {
			JSONObject obj = (JSONObject) fileToVerify.get(i);
			CSVReader read = new CSVReader(new FileReader(this
					.getRuntimeContext().transformPath(
							(String) obj.get("filename"))));

			String sourceEndPoint = (String) obj.get(CommonConstants.ENDPOINT);
			Object[] dbtype = getDbDetails(sourceEndPoint);
			DbSpec details = (DbSpec) dbtype[1];
			Status currentStatus = queryExecutor(queryCreator(read),
					(boolean) obj.get("skuListing"), details,
					(String) obj.get("alertEmailId"), clientSpec.getClient());
			if (finalStatus == null) {
				finalStatus = currentStatus;
			}
			if (currentStatus.getStatusCode() == Status.STATUS_ERROR) {
				finalStatus = currentStatus;
			}
		}
		logger.debug("final status : " + finalStatus.getStatusCode()
				+ finalStatus.getMessage());
		return finalStatus;
	}

	private Status queryExecutor(List<String> queryCreator, boolean skuListing,
			DbSpec dbSpec, String alertEmailId, String clientTag)
			throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		logger.debug("queryExecutor called!!");
		SqlHelper sqlHelper = new SqlHelper(dbSpec, SqlHelper.TYPE_REDSHIFT);
		StringBuilder message = new StringBuilder(
				"Following are the queries which shows that tables has duplicates\n\n\n\n");
		boolean sendMessage = false;
		Status status = new Status(Status.STATUS_SUCCESS,
				"duplicates not found");
		for (String query : queryCreator) {
			logger.info(query);
			try (ResultSet result = sqlHelper.getSelectData(query)) {
				logger.info("query complete");
				int count = 0;
				if (result.next())
					count = result.getInt("count_dup");
				if (count != 0) {
					message.append(query);
					message.append("\n");
					message.append("count:  ").append(count);
					sendMessage = true;
				}
			} catch (SQLException | NullPointerException e) {
				logger.error("error while executing query", e);
				status = new Status(Status.STATUS_ERROR,
						"error while exeuting query");
			}

			if (sendMessage) {
				status = new Status(Status.STATUS_ERROR, "duplicates found");
			}

		}
		logger.debug("send message : " + sendMessage);
		if (sendMessage) {
			logger.info(message.toString());
			SendEmail.send(alertEmailId,
					new StringBuilder("[").append(clientTag).append("]")
							.append("Following tables has duplicates")
							.toString(), message.toString());
		}
		logger.debug("return status for queryExecutor : " + status.getMessage()
				+ " : " + status.getStatusCode());
		return status;
	}

	private List<String> queryCreator(CSVReader read) throws IOException {
		// TODO Auto-generated method stub
		List<String> queryMap = new ArrayList<String>();
		String[] nextLine = null;
		while ((nextLine = read.readNext()) != null) {
			String query = queryTemplate(nextLine[0], nextLine[1], nextLine[2]);
			logger.info("query generted " + query);
			queryMap.add(query);
		}
		return queryMap;
	}

	private String queryTemplate(String tableName, String columns,
			String columnName) {
		StringBuilder query = new StringBuilder();
		query.append("select count(*) as count_dup from (select count(*), ")
				.append(columns).append(" from ").append(tableName);
		if (columnName != null && !columnName.isEmpty())
			query.append(" where ").append(columnName).append("=(select max(")
					.append(columnName).append(") from ").append(tableName)
					.append(")");
		query.append(" group by ").append(columns)
				.append(" having count(*)>1 ) as temp").toString();
		logger.info("Query created :: "+query.toString());
		return query.toString();
	}

}
