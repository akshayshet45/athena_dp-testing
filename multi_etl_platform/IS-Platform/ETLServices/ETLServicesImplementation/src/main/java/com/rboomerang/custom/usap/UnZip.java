package com.rboomerang.custom.usap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class UnZip
{
    private List<String> fileList;
    private String INPUT_FOLDER;
    private String OUTPUT_FOLDER;
    private ZipInputStream zis;
    
    public UnZip(String inputfolder,String outputfolder, List<String> fileList){
    	this.fileList=fileList;
    	INPUT_FOLDER=inputfolder;
    	OUTPUT_FOLDER=outputfolder;
    }
    
    /**
     * Unzip it
     */
    public boolean unZipIt(){

     byte[] buffer = new byte[1024];
     boolean returnVal= false;
    	
     try{
    		
    	//create output directory is not exists
    	File folder = new File(OUTPUT_FOLDER);
    	if(!folder.exists()){
    		folder.mkdir();
    	}
    		
    	for(String zipFile : fileList){
    		//get the zip file content
        	zis = 
        		new ZipInputStream(new FileInputStream(INPUT_FOLDER +File.separator+ zipFile));
        	//get the zipped file list entry
        	ZipEntry ze = zis.getNextEntry();
        		
        	while(ze!=null){
        			
        	   String fileName = ze.getName();
               File newFile = new File(OUTPUT_FOLDER + File.separator + fileName);
                    
               System.out.println("file unzip : "+ newFile.getAbsoluteFile());
                    
                //create all non exists folders
                //else you will hit FileNotFoundException for compressed folder
                new File(newFile.getParent()).mkdirs();
                  
                FileOutputStream fos = new FileOutputStream(newFile);             

                int len;
                while ((len = zis.read(buffer)) > 0) {
           		fos.write(buffer, 0, len);
                }
            		
                fos.close();   
                ze = zis.getNextEntry();
        	}
    	}
    	
       
    		
    	System.out.println("Done");
    	returnVal=true;	
    }catch(IOException ex){
       ex.printStackTrace(); 
    }
     finally{
    	 try {
			zis.closeEntry();
		} catch (IOException e) {
			e.printStackTrace();
		}
     	try {
			zis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
     }
     return returnVal;
   }    
}