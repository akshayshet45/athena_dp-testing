package com.rboomerang.processor.impl.common;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.AWSCredentials;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.common.service.utils.amazonUtils.AmazonSqsHelper;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.AWSAccess;


public class SqsNotifier extends AbstractService {
	
	private final Logger logger = LoggerFactory
			.getLogger(SqsNotifier.class);


	public SqsNotifier(String clientSpecpath, String serviceSpecPath)
			throws Exception {
		super(clientSpecpath, serviceSpecPath);
		logger.info("TriggerDD constructor called");
	}

	public SqsNotifier(String clientSpecpath,
			String serviceSpecPath, String runtimeDate) throws Exception {
		super(clientSpecpath, serviceSpecPath, runtimeDate);
		logger.info("TriggerDD constructor called");
	}

	public SqsNotifier(String clientSpecpath,
			String serviceSpecPath, RuntimeContext runtimeContext,
			String runtimeDate) throws Exception {
		super(clientSpecpath, serviceSpecPath, runtimeContext, runtimeDate);
		logger.info("TriggerDD constructor called");
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public Status service(ClientSpec clientSpec, JSONObject serviceSpec)
			throws JSchException, SftpException, Exception {
		Status status = new Status(Status.STATUS_SUCCESS, "SQS notifier trigger successful");
		try{

			AWSAccess awsAccess = new AWSAccess();
			AWSCredentials basicAWSCredentials = awsAccess.getAWSAccess();
			String accessKey = basicAWSCredentials.getAWSAccessKeyId();
			String secretKey = basicAWSCredentials.getAWSSecretKey();
			logger.debug(getAzkabanFlowParam("SQSRegion"));
			logger.debug(getAzkabanFlowParam("responseSQS"));
			AmazonSqsHelper amazonSqsHelper = new AmazonSqsHelper(accessKey, secretKey,getAzkabanFlowParam("SQSRegion"), getAzkabanFlowParam("responseSQS"));
			JSONObject object = new JSONObject();
			object.put("requestID", getAzkabanFlowParam("requestID"));
			object.put("clientID", getAzkabanFlowParam("clientID"));

			object.put("status", "successful");
			object.put("errorCode", 0);
			object.put("errorMessage", "");
			object.put("errorDescription", "");
			amazonSqsHelper.sendMessage(object.toJSONString(), amazonSqsHelper.getQueueUrl(),getAzkabanFlowParam("responseSQS"));

		}catch(Exception err){
			err.printStackTrace();
			logger.error("Error while posting message",err);
			status = new Status(Status.STATUS_ERROR, "SQS notifier trigger successful");
		}
		return status;
	}

}
