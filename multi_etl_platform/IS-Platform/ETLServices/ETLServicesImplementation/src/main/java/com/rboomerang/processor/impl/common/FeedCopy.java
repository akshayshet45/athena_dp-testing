package com.rboomerang.processor.impl.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

public class FeedCopy extends AbstractService {
	private static Logger logger = LoggerFactory.getLogger(FeedCopy.class);

	public FeedCopy(String clientSpecpath, String validatorSpecPath) throws Exception {
		super(clientSpecpath, validatorSpecPath);
		logger.info("Feed Copy constructor called");
	}

	public FeedCopy(String clientSpecpath, String validatorSpecPath, String runtimeDate) throws Exception {
		super(clientSpecpath, validatorSpecPath, runtimeDate);
		logger.info("Feed Copy constructor called");
	}

	public FeedCopy(String clientSpecpath, String validatorSpecPath, RuntimeContext runtimeContext, String runtimeDate)
			throws Exception {
		super(clientSpecpath, validatorSpecPath, runtimeContext, runtimeDate);
		logger.info("Feed Copy constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject processorSpec) throws Exception {
		logger.info("Feed Copy execute called");

		JSONArray filesToCopy = (JSONArray) processorSpec.get(CommonConstants.LIST);
		StringBuilder filesCopied = new StringBuilder();
		StringBuilder filesNotCopied = new StringBuilder();

		for (int file = 0; file < filesToCopy.size(); file++) {
			JSONObject obj = (JSONObject) filesToCopy.get(file);

			String endpointName = (String) obj.get(CommonConstants.ENDPOINT);
			String locationDestination = (String) obj.get(CommonConstants.LOCATION);
			String fileNameTo = (String) obj.get(CommonConstants.FILENAME);
			String dateTo = (String) obj.get(CommonConstants.FEED_DATE);
			String headerFileNameInFeedMetaData = (String) obj.get("headerFileName");
			
			if (dateTo.isEmpty() || dateTo == null)
				dateTo = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
			logger.debug("Current Feed date :::: "+dateTo);
			String endpointType = getTypeOfEndPoint(clientSpec, endpointName);
			locationDestination = replaceDynamicFolders(locationDestination, dateTo);
			fileNameTo = replaceDateTag(fileNameTo, dateTo);
			if (endpointType.equals("sftp"))
				fileNameTo = makeRegex(fileNameTo);
			logger.info("filename :::: "+fileNameTo);
			HashMap<String, String> endPointResourceProperties = buildResourceProperties(endpointName, clientSpec,
					endpointType);
			IEndPointResource endPointResource = EndPointResourceFactory.getInstance().getEndPointResource(endpointType,
					endPointResourceProperties);

			boolean fileFound = endPointResource.checkFileExistence(locationDestination, fileNameTo);
			
			logger.info(fileNameTo + " " + fileFound);

			if (fileFound)
				filesCopied.append(locationDestination).append(fileNameTo).append(CommonConstants.NEWLINE);
			else {
				boolean copied = false;
				String locationSource = (String) obj.get(CommonConstants.LOCATION);
				String fileNameFrom = (String) obj.get(CommonConstants.FILENAME);
				String format = (String) obj.get(CommonConstants.FORMAT);
				String dateFrom = (String) obj.get(CommonConstants.COPY_FEED_DATE);
				fileNameTo = (String) obj.get(CommonConstants.FILENAME);

				fileNameTo = replaceDateTag(fileNameTo, dateTo);
				fileNameTo = fileNameTo.replaceAll(CommonConstants.REGEX_TAG, "");
				logger.info("Format Requested - " + format);
				locationSource = replaceDynamicFolders(locationSource, dateFrom);
				fileNameFrom = replaceDateTag(fileNameFrom, dateFrom);

				if (format.equalsIgnoreCase("data")) {
					logger.debug("checking for file!!");
					if (endpointType.equals("sftp"))
						fileNameFrom = makeRegex(fileNameFrom);
					boolean sourceFileFound = endPointResource.checkFileExistence(locationSource, fileNameFrom);
					logger.info(fileNameFrom + " " + sourceFileFound);
					if (!sourceFileFound) {
						filesNotCopied.append(locationDestination).append(fileNameTo).append(CommonConstants.NEWLINE);
						break;
					}
					IEndPointResource endPointResource1 = EndPointResourceFactory.getInstance().getEndPointResource(endpointType,
							endPointResourceProperties);
					InputStream data = endPointResource1.getFile(locationSource, fileNameFrom);
					
					logger.info("Data retrieved from source file");
					IEndPointResource endPointResource2 = EndPointResourceFactory.getInstance().getEndPointResource(endpointType,
							endPointResourceProperties);
					endPointResource2.writeFile(data, locationDestination, fileNameTo);
					endPointResource1.closeConnection();
					endPointResource2.closeConnection();
					data.close();
					copied = true;
				} else {
					logger.info("Checking for file !!!");
					if (endpointType.equals("sftp"))
						fileNameFrom = makeRegex(fileNameFrom);
					boolean sourceFileFound = endPointResource.checkFileExistence(locationSource, fileNameFrom);
					logger.info(fileNameFrom + " " + sourceFileFound);
					if (!sourceFileFound) {
						filesNotCopied.append(locationDestination).append(fileNameTo).append(CommonConstants.NEWLINE);
						break;
					}
					fileNameFrom = fileNameFrom.replaceAll(CommonConstants.REGEX_TAG, "");
					String headerLine = getHeaderLine(fileNameFrom,
							this.getRuntimeContext().transformPath(clientSpec.getFeedMetadata()),headerFileNameInFeedMetaData);
					logger.info("Headers retrieved from header file");
					String[] fileExtension = fileNameTo.split("\\.");
					logger.debug("filenameto:" + fileNameTo + "  " + fileExtension.length);
					for (int x = 0; x < fileExtension.length; x++) {
						logger.debug("xx:: " + fileExtension[x]);
					}

					if (fileExtension[fileExtension.length - 1].equals("gz")) {
						logger.info("headers for gz:: " + headerLine);
						File compressedHeaderFile = File.createTempFile("tempCompressedHeader", ".gz");
						byte[] gzbuffer = new byte[1024];
						GZIPOutputStream gzos = new GZIPOutputStream(
								new FileOutputStream(compressedHeaderFile.getAbsolutePath()));
						InputStream inputStream = IOUtils.toInputStream(headerLine);
						int gzlen;
						while ((gzlen = inputStream.read(gzbuffer)) > 0) {
							gzos.write(gzbuffer, 0, gzlen);
						}
						inputStream.close();
						gzos.finish();
						gzos.close();
						endPointResource.writeFile(new FileInputStream(compressedHeaderFile.getAbsolutePath()),locationDestination, fileNameTo);
						compressedHeaderFile.delete();
					} else if (fileExtension[fileExtension.length - 1].equals("zip")) {
						logger.info("headers for zip:: " + headerLine);
						File compressedHeaderFile = File.createTempFile("tempCompressedHeader", ".zip");
						byte[] zipbuffer = new byte[1024];
						FileOutputStream zipfos = new FileOutputStream(compressedHeaderFile.getAbsolutePath());
						ZipOutputStream zipzos = new ZipOutputStream(zipfos);
						ZipEntry ze = new ZipEntry(fileNameTo.substring(0,fileNameTo.length()-4)+".txt");
						zipzos.putNextEntry(ze);
						InputStream inputStream = IOUtils.toInputStream(headerLine);
						int ziplen;
						while ((ziplen = inputStream.read(zipbuffer)) > 0) {
							zipzos.write(zipbuffer, 0, ziplen);
						}
						inputStream.close();
						zipzos.closeEntry();
						zipzos.close();
						endPointResource.writeFile(new FileInputStream(compressedHeaderFile.getAbsolutePath()),locationDestination, fileNameTo);
						compressedHeaderFile.delete();
					} else
						endPointResource.writeFile(IOUtils.toInputStream(headerLine), locationDestination, fileNameTo);
					endPointResource.closeConnection();
					copied = true;
				}

				if (copied)
					filesCopied.append(locationDestination).append(fileNameTo).append(CommonConstants.NEWLINE);
				else
					filesNotCopied.append(locationDestination).append(fileNameTo).append(CommonConstants.NEWLINE);
			}
		}

		logger.info("Feeds not copied size - > " + filesNotCopied.length());
		Status status = null;
		if (filesNotCopied.toString().isEmpty())
			status = new Status(Status.STATUS_SUCCESS, filesCopied.toString());
		else
			status = new Status(Status.STATUS_ERROR, filesNotCopied.toString());

		logger.info("Feed Copy service called");

		return status;
	}
}