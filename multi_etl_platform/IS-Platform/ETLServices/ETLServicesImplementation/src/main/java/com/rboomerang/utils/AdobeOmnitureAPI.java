package com.rboomerang.utils;
/**
 * References: 	https://marketing.adobe.com/developer/api-explorer
 * 				https://marketing.adobe.com/developer/blog
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import biz.source_code.base64Coder.Base64Coder;

public class AdobeOmnitureAPI
{
    private String userName = null;
    private String password = null;
    private String ENDPOINT = CommonConstants.OMNITURE_ENDPOINT; 
    private org.json.simple.JSONObject query=null;
    private String feedDate=null;
    private String[] outputFilePath=new String[2];
    private static Logger logger = LoggerFactory.getLogger(AdobeOmnitureAPI.class);
    private static int resultLimit=50000;
    public AdobeOmnitureAPI(String clientName,String userName,String password, org.json.simple.JSONObject apiQuery,String feedDate)
    {
    	this.userName=userName;
    	this.password=password;
    	this.query=apiQuery;
    	this.feedDate=feedDate;
    	logger.info("Omniture Report API Constructor Called!!");
    }
/**
 * Initiates api method call for the specific end point
 * @param method	API Method 
 * @param data		input for specific api method
 * @return 			Response as a JSON String
 * @throws IOException
 * @throws InterruptedException 
 */
    public  String callMethod(String method, String data) throws IOException, InterruptedException
    {
    	logger.debug("Generating REST API URL");
        URL url = new URL(ENDPOINT + "?method=" + method);
        URLConnection connection = url.openConnection();
        connection.addRequestProperty("X-WSSE", getHeader());
        connection.setDoOutput(true);
        OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
        wr.write(data);
        wr.flush();
        InputStream in=null;
        try {
        	in= connection.getInputStream();
		} catch (IOException e) {
			logger.error("IOException : "+e.getMessage());
			e.printStackTrace();
			Thread.sleep(5000);
			connection = url.openConnection();
	        connection.addRequestProperty("X-WSSE", getHeader());
	        connection.setDoOutput(true);
	        wr = new OutputStreamWriter(connection.getOutputStream());
	        wr.write(data);
	        wr.flush();
	        in= connection.getInputStream();
		}
        
        BufferedReader res = new BufferedReader(new InputStreamReader(in, "UTF-8"));
        StringBuffer sBuffer = new StringBuffer();
        String inputLine;
        while ((inputLine = res.readLine()) != null)
        {
            sBuffer.append(inputLine);
        }
        res.close();
        return sBuffer.toString();
    }
/**
 * Generates the header required for Omniture REST API
 * @return returns URL header
 * @throws UnsupportedEncodingException
 */
    private String getHeader() throws UnsupportedEncodingException
    {
        byte[] nonceB = generateNonce();
        String nonce = base64Encode(nonceB);
        String created = generateTimestamp();
        String password64 = getBase64Digest(nonceB, created.getBytes("UTF-8"), password.getBytes("UTF-8"));
        StringBuffer header = new StringBuffer("UsernameToken Username=\"");
        header.append(userName);
        header.append("\", ");
        header.append("PasswordDigest=\"");
        header.append(password64.trim());
        header.append("\", ");
        header.append("Nonce=\"");
        header.append(nonce.trim());
        header.append("\", ");
        header.append("Created=\"");
        header.append(created);
        header.append("\"");
        return header.toString();
    }
//Utility methods for Header encoding 
    private  byte[] generateNonce()
    {
        String nonce = Long.toString(new Date().getTime());
        return nonce.getBytes();
    }

    private String generateTimestamp()
    {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        return dateFormatter.format(new Date());
    }

    private  synchronized String getBase64Digest(byte[] nonce, byte[] created, byte[] password)
    {
      try
      {
        MessageDigest messageDigester = MessageDigest.getInstance("SHA-1");
        // SHA-1 ( nonce + created + password )
        messageDigester.reset();
        messageDigester.update(nonce);
        messageDigester.update(created);
        messageDigester.update(password);
        return base64Encode(messageDigester.digest());
      }
      catch (java.security.NoSuchAlgorithmException e)
      {
        throw new RuntimeException(e);
      }
    }

    private static String base64Encode(byte[] bytes) 
    {
      return Base64Coder.encodeLines(bytes);
    }
/**
 * Report request Queuer and generator
 * @throws IOException
 * @throws InterruptedException 
 */
    public String[] start() throws IOException, InterruptedException 
    {
       
        logger.info("Reading required query parameters");
        int resultCount=0;
	    String reportSuiteID=query.get("reportSuiteID").toString();
	    String commerceMetrics =query.get("metrics_commerce").toString();
	    String trafficMetrics =query.get("metrics_traffic").toString();
	    ArrayList<String> metricsCommerce= getMetrics(commerceMetrics);
	    ArrayList<String> metricsTraffic =getMetrics(trafficMetrics);
    	String classification =query.get("classification").toString();
    	String maxResults= query.get("maxResults").toString();
    	String element =query.get("element").toString();
    	
        logger.debug("Making the request JSON for Queuing report");	
        Map<String,Object> requestParams =new HashMap<String, Object>();
        Map<String, Object> valueParams =new HashMap<String, Object>();
        Map<String, String> elementParams =new HashMap<String,String>();
        elementParams.put("id",element);
        elementParams.put("classification",classification);
        elementParams.put("top",maxResults);
        elementParams.put("startingWith", "1");
        List<Object> elParams =new ArrayList<Object>();
        elParams.add(elementParams);
        valueParams.put("reportSuiteID",reportSuiteID);
        valueParams.put("date",feedDate);
        valueParams.put("elements",elParams);
        
        logger.debug(requestParams.toString());
        logger.info("Queuing request for Omniture");
        logger.info("Generating report for commerce metrics");
        List<Object> metricParams =new ArrayList<Object>();
            
        int iter;
        int length=metricsCommerce.size();
        for (iter=0;iter<length;iter++)
        {
        	Map<String,String> metricData =new HashMap<String,String>();
        	metricData.put("id",metricsCommerce.get(iter));
        	metricParams.add(metricData);
        }
        valueParams.put("metrics", metricParams);
        requestParams.put("reportDescription", valueParams);
        logger.debug("Request Parameters"+requestParams);
        File commerceFile =File.createTempFile("ctemp", ".csv");
        boolean headerCheck=false;
    	resultCount=generateReport(requestParams,commerceFile,headerCheck,feedDate);
    	int startWith=1;
    	while(resultCount==resultLimit)
    	{
    		startWith +=resultLimit;
    		elementParams.put("startingWith",new Integer(startWith).toString());
    		resultCount=generateReport(requestParams,commerceFile,headerCheck,feedDate);
    		headerCheck=true;
    	}
		logger.info("Commerce metrics have been collected.. running report for traffic variables!!");
		headerCheck=false;
		List<Object> metricParamsTraffic =new ArrayList<Object>();
        int size =metricsTraffic.size();
        for (int i=0;i<size;i++)
        {
        	Map<String,String> metricData =new HashMap<String,String>();
        	metricData.put("id",metricsTraffic.get(i));
        	metricParamsTraffic.add(metricData);
        }
        elementParams.put("startingWith","1");
        valueParams.put("metrics", metricParamsTraffic);
        requestParams.put("reportDescription", valueParams);
        logger.info("Request Parameters"+requestParams);
        File trafficFile =File.createTempFile("ctemp", ".csv");
        resultCount=generateReport(requestParams,trafficFile,headerCheck,feedDate);
        startWith=1;
        while(resultCount==resultLimit)
        {
        	startWith +=resultLimit;
        	elementParams.put("startingWith",new Integer(startWith).toString());
    		resultCount=generateReport(requestParams,trafficFile,headerCheck,feedDate);
    		headerCheck=true;
        }
        logger.info("Traffic Metrics also collected");
    	outputFilePath[0]=commerceFile.getAbsolutePath();
    	outputFilePath[1]=trafficFile.getAbsolutePath();
    	return outputFilePath;
    }
    
 /**
   * Utility to process the comma separated metrics
   * @param input metric string
   * @return list of metrics
   */
    private static ArrayList<String> getMetrics(String input)
    {
    	ArrayList<String> metricArray =new ArrayList<String>();
    	StringTokenizer strToken =new StringTokenizer(input,",");
    	while(strToken.hasMoreTokens())
    	{
    		String metric = strToken.nextToken();
    		if(!metric.equals(null))
    			metricArray.add(metric);
    	}
    	return metricArray;
    }
   /**
    * Generates final metric report
    * @param requestParams	
    * @param file			OuputFile
    * @param headerCheck	For writing header once for a file
    * @param feedDate		Date for which report has been generating
    * @return
    * @throws IOException
    * @throws InterruptedException
    */
    private int generateReport(Map<String,Object> requestParams, File file,boolean headerCheck,String feedDate) throws IOException, InterruptedException
    {
    	String reportResponse= callMethod("Report.QueueRanked", JSONObject.fromObject(requestParams).toString());
        String reportID=JSONObject.fromObject(reportResponse).get("reportID").toString();
        logger.info("Report Request has been created "+ reportID);
        
        Map<String,String> getReportParams =new HashMap<String,String>();
        getReportParams.put("reportID",reportID);
        Map<String,String> getStatusParams=new HashMap<String,String>();
        getStatusParams.put("reportID",reportID);
        PrintWriter pw=null;
        int resultLength=0; 
    	logger.debug("Checking the status of report creation "+reportID);
    	while(true)
    	{
      		String statusResponse =callMethod("Report.GetStatus", JSONObject.fromObject(getStatusParams).toString());
    		String status = JSONObject.fromObject(statusResponse).getString("status").toString();
    		if(!status.equals("done"))
    		{
    			logger.debug(status+reportID);
    			Thread.sleep(10000);
    			continue;
    		}
    		else
    		{	
			  logger.info("Report has been created!! Downloading the report...");
			  String reportData= callMethod("Report.GetReport", new JSONObject(getReportParams).toString());
			  logger.debug("Writing the report data to a file...");
			  pw=new PrintWriter(new BufferedWriter(new FileWriter(file.getAbsolutePath(),true)));
			  JSONArray dataArray=(JSONObject.fromObject(reportData).getJSONObject("report").getJSONArray("data"));
			  JSONArray headerArray=(JSONObject.fromObject(reportData).getJSONObject("report").getJSONArray("metrics"));
			  resultLength=dataArray.length();
			  logger.debug("Writing data to a temporary file!!");
			  if(!headerCheck)
			  {
				  pw.print("Sku"+",");
				  for(int i=0;i<headerArray.length();i++)
				  {
					  JSONObject obj= headerArray.getJSONObject(i);
					  pw.print(obj.getString("id")+",");
				  }
				  pw.print("feedDate");
				  pw.println();
			  }
			  for(int i=0;i<resultLength;i++)
			  {
				  JSONObject el =(JSONObject)dataArray.getJSONObject(i);
				  pw.print(el.get("name")+",");
				  String valueMetric=el.get("counts").toString();
				  StringTokenizer sT= new StringTokenizer(valueMetric.substring(1, valueMetric.length()-1),",");
				while(sT.hasMoreTokens())
				{
					pw.print(sT.nextToken()+",");
				}
				pw.print(feedDate);
				pw.println();
			  }
			  pw.close();
			  break;
    		}
    	}
    	return resultLength;
    }
}