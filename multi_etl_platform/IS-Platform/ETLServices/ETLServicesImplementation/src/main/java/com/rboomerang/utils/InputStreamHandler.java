package com.rboomerang.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

public class InputStreamHandler {

	
	public static void writeInputStreamToFile(InputStream inputStream,String sourceFileName,String encoding)throws IOException{

		BufferedReader reader=new BufferedReader(new InputStreamReader( inputStream, encoding ));
		BufferedWriter writer = new BufferedWriter(new FileWriter(sourceFileName));
		String line = null;
		while(true){

			line=reader.readLine();
			if(line==null)
				break;
			writer.write(line+"\n");

		}
		writer.close();
		reader.close();
	}
	public static void writeInputStreamToFile(InputStream inputStream,String sourceFileName)throws IOException{

		BufferedReader reader=new BufferedReader(new InputStreamReader( inputStream ));
		BufferedWriter writer = new BufferedWriter(new FileWriter(sourceFileName));
		String line = null;
		while(true){

			line=reader.readLine();
			if(line==null)
				break;
			writer.write(line+"\n");

		}
		writer.close();
		reader.close();
	}
	public static void appendInputStreamToFile(InputStream inputStream,String sourceFileName)throws IOException{

		BufferedReader reader=new BufferedReader(new InputStreamReader( inputStream ));
		BufferedWriter writer = new BufferedWriter(new FileWriter(sourceFileName,true));
		String line = null;
		while(true){

			line=reader.readLine();
			if(line==null)
				break;
			writer.write(line+"\n");

		}
		writer.close();
		reader.close();
	}
	public static void writeBufferedInputStreamToFile(InputStream inputStream,String sourceFileName)throws IOException{

		byte[] buffer = new byte[1024];
		BufferedInputStream bis = new BufferedInputStream(inputStream);
		File newFile = new File(sourceFileName);
		OutputStream os = new FileOutputStream(newFile);
		BufferedOutputStream bos = new BufferedOutputStream(os);
		int readCount;
		while( (readCount = bis.read(buffer)) > 0) {
			bos.write(buffer, 0, readCount);
		}
		bis.close();
		bos.close();

	}

}
