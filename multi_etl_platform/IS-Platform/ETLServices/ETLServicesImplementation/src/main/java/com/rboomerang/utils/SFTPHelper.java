package com.rboomerang.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class SFTPHelper {
	private String SFTP_HOST;
	private String SFTP_PORT;
	private String SFTP_USER;
	private String SFTP_PASS;

	private Session session = null;
	private Channel channel = null;
	private RegexUtility rUtility = null;
	private ChannelSftp channelSftp = null;

	private static Logger logger = LoggerFactory.getLogger(SFTPHelper.class);

	public SFTPHelper(String HOST, String PORT, String USER, String PASS) {
		SFTP_HOST = HOST;
		SFTP_PORT = PORT;
		SFTP_USER = USER;
		SFTP_PASS = PASS;
	}

	public void close() throws IOException {
		if (channelSftp.isConnected())
			channelSftp.disconnect();
		if (channel.isConnected())
			channel.disconnect();
		if (session.isConnected())
			session.disconnect();
		logger.info("SftpAccess close called");
	}

	public void putInSFTP(InputStream stream, String destination)
			throws NumberFormatException, JSchException, SftpException, InterruptedException {
		JSch jsch = new JSch();
		session = jsch.getSession(SFTP_USER, SFTP_HOST, Integer.parseInt(SFTP_PORT));
		session.setPassword(SFTP_PASS);

		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);
		session.connect();
		channel = session.openChannel("sftp");
		try {
			channel.connect();
		} catch (Exception e) {
			logger.error(e.getMessage());
			if (e.getMessage().contains("channel is not opened")) {
				channel.disconnect();
				session.disconnect();
				Thread.sleep(50000);
				session = jsch.getSession(SFTP_USER, SFTP_HOST, Integer.parseInt(SFTP_PORT));
				session.setPassword(SFTP_PASS);
				session.setConfig(config);
				session.connect();
				channel = session.openChannel("sftp");
				channel.connect(60000);
			} else
				throw e;
		}
		channelSftp = (ChannelSftp) channel;

		channelSftp.put(stream, destination);

		return;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public InputStream getFromSFTP(String sourcePath, String sourceFileName)
			throws SftpException, JSchException, InterruptedException {
		JSch jsch = new JSch();
		session = jsch.getSession(SFTP_USER, SFTP_HOST, Integer.parseInt(SFTP_PORT));
		session.setPassword(SFTP_PASS);

		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);
		session.connect();
		channel = session.openChannel("sftp");
		try {
			channel.connect();
		} catch (Exception e) {
			logger.error(e.getMessage());
			if (e.getMessage().contains("channel is not opened")) {
				channel.disconnect();
				session.disconnect();
				Thread.sleep(50000);
				session = jsch.getSession(SFTP_USER, SFTP_HOST, Integer.parseInt(SFTP_PORT));
				session.setPassword(SFTP_PASS);
				session.setConfig(config);
				session.connect();
				channel = session.openChannel("sftp");
				channel.connect(60000);
			} else
				throw e;
		}
		channelSftp = (ChannelSftp) channel;
		InputStream stream = null;
		boolean fileFound = false;

		if (sourceFileName.contains(CommonConstants.REGEX_STRING)) {
			String actualFileName = "";
			try {
				Vector<ChannelSftp.LsEntry> list = channelSftp.ls(sourcePath);
				Iterator<ChannelSftp.LsEntry> directoryIterator = list.iterator();

				while (directoryIterator.hasNext()) {
					ChannelSftp.LsEntry obj = directoryIterator.next();
					if (rUtility.compareRegex(sourceFileName, obj.getFilename())) {
						fileFound = true;
						actualFileName = obj.getFilename();
						logger.debug("actualFileName ::::: " + actualFileName);
						break;
					}
				}

			} catch (SftpException e) {
				logger.error("while parsing files in sftp", e);
			}
			if (fileFound) {
				sourceFileName = actualFileName;
				stream = channelSftp.get(sourcePath + sourceFileName);
			}
		} else
			stream = channelSftp.get(sourcePath + sourceFileName);
		return stream;
	}

	@SuppressWarnings("unchecked")
	public List<String> getFileListMatchingRegexFromSFTP(String sourcePath, String sourceFileName)
			throws SftpException, JSchException, InterruptedException {
		List<String> matchingFileList = new ArrayList<>();
		JSch jsch = new JSch();
		session = jsch.getSession(SFTP_USER, SFTP_HOST, Integer.parseInt(SFTP_PORT));
		session.setPassword(SFTP_PASS);

		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);
		session.connect();
		channel = session.openChannel("sftp");
		try {
			channel.connect();
		} catch (Exception e) {
			logger.error(e.getMessage());
			if (e.getMessage().contains("channel is not opened")) {
				channel.disconnect();
				session.disconnect();
				Thread.sleep(50000);
				session = jsch.getSession(SFTP_USER, SFTP_HOST, Integer.parseInt(SFTP_PORT));
				session.setPassword(SFTP_PASS);
				session.setConfig(config);
				session.connect();
				channel = session.openChannel("sftp");
				channel.connect(60000);
			} else
				throw e;
		}
		channelSftp = (ChannelSftp) channel;

		if (sourceFileName.contains(CommonConstants.REGEX_STRING)) {
			try {
				Vector<ChannelSftp.LsEntry> list = channelSftp.ls(sourcePath);
				Iterator<ChannelSftp.LsEntry> directoryIterator = list.iterator();

				while (directoryIterator.hasNext()) {
					ChannelSftp.LsEntry obj = directoryIterator.next();
					if (RegexUtility.compareRegex(sourceFileName, obj.getFilename())) {
						matchingFileList.add(obj.getFilename());
						logger.debug("actualFileName ::::: " + obj.getFilename());
					}
				}

			} catch (SftpException e) {
				logger.error("while parsing files in sftp", e);
			}
		}
		return matchingFileList;
	}
}
