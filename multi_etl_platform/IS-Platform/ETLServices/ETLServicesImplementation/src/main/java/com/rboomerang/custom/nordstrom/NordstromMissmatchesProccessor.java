
package com.rboomerang.custom.nordstrom;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.common.framework.javabeans.DbSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.SqlHelper;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

/**
 * @author Nisha Rai
 *
 */
public class NordstromMissmatchesProccessor extends AbstractService {

	private Logger logger = LoggerFactory.getLogger(NordstromMissmatchesProccessor.class);
	DataFormatter df = new DataFormatter();
	private XSSFWorkbook final_workbook;
	String scrapper = "http://beta.oregon.scraper.rboomerang.com/product-scraper/getSiteMetaData?";
	private XSSFWorkbook workbook_bad;

	public NordstromMissmatchesProccessor(String clientSpecpath, String serviceSpecPath) throws Exception {
		super(clientSpecpath, serviceSpecPath);
		logger.info("NordstromMissmatchesProccessor constructor called");

	}

	public NordstromMissmatchesProccessor(String clientSpecpath, String serviceSpecPath, String runtimeDate)
			throws Exception {
		super(clientSpecpath, serviceSpecPath, runtimeDate);
		logger.info("NordstromMissmatchesProccessor constructor called");

	}

	public NordstromMissmatchesProccessor(String clientSpecpath, String serviceSpecPath, RuntimeContext runtimeContext,
			String runtimeDate) throws Exception {
		super(clientSpecpath, serviceSpecPath, runtimeContext, runtimeDate);
		logger.info("NordstromMissmatchesProccessor constructor called");

	}

	/*
	 * https://docs.google.com/a/boomerangcommerce.com/document/d/
	 * 1qhgshm9rE1hLcsREwTvrAdnDWwKIRLd_hdBy5413FOI/edit?usp=sharing
	 */
	@Override
	public Status service(ClientSpec clientSpec, JSONObject processorSpec)
			throws JSchException, SftpException, Exception {
		logger.info("NordstromMissmatches proccessing started");
		File outputFile = File.createTempFile("processed", ".tmp");
		File faultRowsFile = File.createTempFile("bad_rows", ".tmp");
		IEndPointResource destEndPointResource = null;
		IEndPointResource sourceEndPointResource = null;

		JSONObject list = (JSONObject) processorSpec.get(CommonConstants.LIST);
		JSONObject source = (JSONObject) list.get(CommonConstants.SOURCE);
		JSONObject dest = (JSONObject) list.get(CommonConstants.DESTINATION);
		try {
			// getting source Endpoint details
			String sourceEndPoint = (String) source.get(CommonConstants.ENDPOINTNAME);
			String sourcePath = (String) source.get(CommonConstants.PATH);
			String sourceFileName = (String) source.get(CommonConstants.FILE_NAME);
			String sourceDateSuffix = (String) source.get(CommonConstants.FEED_DATE);
			if (sourceDateSuffix == null || sourceDateSuffix.isEmpty()) {
				sourcePath = replaceDynamicFolders(sourcePath, sourceDateSuffix);
				sourceFileName = replaceDateTag(sourceFileName, sourceDateSuffix);
			}

			// getting Destination Endpoint details
			String destEndPoint = (String) dest.get(CommonConstants.ENDPOINTNAME);
			String destPath = (String) dest.get(CommonConstants.PATH);
			String destFileName = (String) dest.get(CommonConstants.FILE_NAME);
			String destDateSuffix = (String) dest.get(CommonConstants.FEED_DATE);
			if (destDateSuffix == null || destDateSuffix.isEmpty()) {
				destPath = replaceDynamicFolders(destPath, destDateSuffix);
				destFileName = replaceDateTag(destFileName, destDateSuffix);
			}

			logger.info("Input File Source Endpoint :: " + sourceEndPoint);
			logger.info("Output File destination Endpoint:: " + destEndPoint);
			logger.info("Input File Source Path --> " + sourcePath + " :: FileName --> " + sourceFileName);
			logger.info("Output File destination --> " + destPath + " :: and FileName --> " + destFileName);

			// downloading the sourceFile
			String sourceType = super.getTypeOfEndPoint(clientSpec, sourceEndPoint);
			HashMap<String, String> sourceEndPointResourceProperties = super.buildResourceProperties(sourceEndPoint,
					clientSpec, sourceType);
			sourceEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(sourceType,
					sourceEndPointResourceProperties);
			logger.info("Downloading ......." + sourceFileName);
			InputStream sourceStream = sourceEndPointResource.getFile(sourcePath, sourceFileName);

			// process the file
			processSourceFile(sourceStream, outputFile,faultRowsFile);

			// upload file to destination
			String destType = getTypeOfEndPoint(clientSpec, destEndPoint);

			HashMap<String, String> destEndPointResourceProperties = super.buildResourceProperties(destEndPoint,
					clientSpec, destType);
			destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(destType,
					destEndPointResourceProperties);
			logger.info("writing processed file.....");
			destEndPointResource.writeFile(new FileInputStream(new File(outputFile.getAbsolutePath())), destPath,
					destFileName);
			
			logger.info("writing non processed file.....");
			String bad_rows_file_name = FilenameUtils.removeExtension(destFileName).concat("-bad_rows.xlsx");
			destEndPointResource.writeFile(new FileInputStream(new File(faultRowsFile.getAbsolutePath())), destPath,
					bad_rows_file_name);
			
			sourceStream.close();
			sourceEndPointResource.closeConnection();
			return new Status(Status.STATUS_SUCCESS, "Nordstrom mismatch file proceesing completed" + destFileName
					+ " put to path " + destPath + " at endpoint " + destEndPoint);
		} catch (Exception e)
		{
			e.printStackTrace();
			return new Status(Status.STATUS_ERROR,"Error in reading JSONSpec");
		}
		finally {
			if (outputFile.exists()) {
				outputFile.delete();
			}
			if (faultRowsFile.exists()) {
				faultRowsFile.delete();
			}
			try {
				if (sourceEndPointResource != null) {
					sourceEndPointResource.closeConnection();
				}
				if (destEndPointResource != null) {
					destEndPointResource.closeConnection();
				}
			} catch (Exception e) {
				logger.error("Unable to close EndPointResource Objects", e);
			}
		}
	}

	private void processSourceFile(InputStream sourceStream, File outputFile,File faultRowsFile) {
		List<String> clientSku;
		try {
			// create output excel
			FileOutputStream final_file = new FileOutputStream(outputFile);
			final_workbook = new XSSFWorkbook();
			XSSFSheet processed_sheet = final_workbook.createSheet("sheet1");

			// create faulty file
			FileOutputStream fault_file = new FileOutputStream(faultRowsFile);
			workbook_bad = new XSSFWorkbook();
			XSSFSheet sheet_bad = workbook_bad.createSheet("bad_rows");

			// read rows from stream
			Workbook workbook = WorkbookFactory.create(sourceStream);
			Sheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			int row_count = -1;
			logger.info("Total rows = " + sheet.getLastRowNum());
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				row_count = row.getRowNum();
				logger.info("\n----------reading row :" + row_count+"-----------------");
				if (isLastRow(row)) {
					logger.info("Continues empty rows found at" + row_count + ". Stop processing here!");
					break;
				}
				if (isEmptyRow(row)) {
					logger.info("Empty row found at" + row_count + ". Skip processing here!");
					continue;
				}
				if (row_count == 0) {
					// create the header for processed file
					Row header = processed_sheet.createRow(0);
					header.createCell(0).setCellValue("SKU Text");
					header.createCell(1).setCellValue("Comp Name");
					header.createCell(2).setCellValue("Comp SKU");
					header.createCell(3).setCellValue("Status");
					header.setRowNum(0);
				} else {

					String compName = df.formatCellValue(row.getCell(10));
					String CompUrl = df.formatCellValue(row.getCell(11));
					String style_num = df.formatCellValue(row.getCell(0));
					String size_display_name = df.formatCellValue(row.getCell(5));
					String color_code = df.formatCellValue(row.getCell(1)); // exel
					String compSku = getSkuFromUrl(scrapper, compName, CompUrl);
					clientSku = getSkuFromQuery(style_num, color_code, size_display_name);
					if (clientSku.isEmpty()) {
						Row bad_row = sheet_bad.createRow(sheet_bad.getLastRowNum() + 1);
						bad_row = row;
						bad_row.createCell(12)
								.setCellValue("No client sku found in catalog for style_num, color_code given");
						logger.error(bad_row.getCell(12).getStringCellValue());
					} else if (compSku == null) {
						Row bad_row = sheet_bad.createRow(sheet_bad.getLastRowNum() + 1);
						bad_row = row;
						bad_row.createCell(12).setCellValue("Scraper did not fetch any sku for comp_name,url passed");
						logger.error(bad_row.getCell(12).getStringCellValue());

					} else {
						for (String sku : clientSku) {
							int row_num = processed_sheet.getLastRowNum() + 1;
							Row new_row = processed_sheet.createRow(row_num);
							logger.info("inserting row  ---- >" + row_num);
							new_row.createCell(0).setCellValue(sku);
							new_row.createCell(1).setCellValue(compName);
							new_row.createCell(2).setCellValue(compSku);
							new_row.createCell(3).setCellValue("Rejected");
						}

					}
				}

			}
			// writing processed xls to local temp file
			final_workbook.write(final_file);
			final_file.close();
			workbook_bad.write(fault_file);
			final_file.close();
			logger.info("bulk approval file generation completed");
			final_workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private List<String> getSkuFromQuery(String style_num, String color_code, String size_display_name) {
		DbSpec dbDetails = new DbSpec();
		dbDetails.setDb("insights");
		dbDetails.setHost("nordstrom.cnjeis9czdgg.us-west-2.redshift.amazonaws.com");
		dbDetails.setPasswd("mxWX1uy1jALVVtp");
		dbDetails.setUname("dp_write_only");
		List<String> skus = new ArrayList<String>();
		ResultSet rs = null;
		String size_display_name_q;
		SqlHelper redshift = null;

		if (size_display_name.toLowerCase().equals("none")) {
			size_display_name_q = "size_display_name is null";
			System.out.println(size_display_name);
		} else {
			size_display_name_q = "size_display_name='" + size_display_name + "'";
		}

		String tableName = "nordstorm.custom_catalog";
		String query = "select distinct(sku) from " + tableName + " where style_num='" + style_num
				+ "' and color_code='" + color_code + "' and " + size_display_name_q;

		logger.info("Connecting to database: \n Host = " + dbDetails.getHost() + "Database = " + dbDetails.getDb()
				+ "user = " + dbDetails.getUname() + "pass=" + dbDetails.getPasswd());
		try {
			redshift = new SqlHelper(dbDetails, "redshift");
			logger.info("Query: " + query);
			rs = redshift.getSelectedData(query);
			while (rs.next()) {
				skus.add(rs.getString(1));
			}

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				redshift.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		logger.info(skus.size() + "skus fetched");
		return skus;
	}

	public String getSkuFromUrl(String scrapper, String compName, String compUrl) {
		String url = scrapper + "site=" + compName + "&url=" + compUrl;
		System.out.println("Requeted URL:" + url);
		HttpURLConnection urlConn = null;
		InputStreamReader in = null;
		JSONObject json = null;
		try {
			urlConn = (HttpURLConnection) new URL(url).openConnection();
			if (urlConn != null)
				urlConn.setReadTimeout(60 * 1000);
			if (urlConn != null && urlConn.getResponseCode() < 400 && urlConn.getInputStream() != null) {
				in = new InputStreamReader(urlConn.getInputStream(), Charset.defaultCharset());
				JSONParser parser = new JSONParser();
				json = (JSONObject) parser.parse(IOUtils.toString(in));
				if (json.get("sku") != null) {
					return (String) json.get("sku");
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("Exception while calling URL:" + url, e);
		} finally {
			try {
				if (in != null)
					in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	boolean isEmptyRow(Row row) {
		boolean isEmptyRow = true;
		// breaking cellnum to 15 as file wont have more than 15 columns
		for (int cellNum = row.getFirstCellNum(); cellNum < 15; cellNum++) {
			Cell cell = row.getCell(cellNum);
			if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK && !df.formatCellValue(cell).equals("")) {
				isEmptyRow = false;
			}
		}
		return isEmptyRow;
	}

	boolean isLastRow(Row row) {
		if (row.getSheet().getLastRowNum() <= row.getRowNum())
			return true;
		if (isEmptyRow(row) && isEmptyRow(row.getSheet().getRow(row.getRowNum() + 1)))
			return true;
		return false;
	}
}
