package com.rboomerang.processor.impl.common;


import java.io.InputStream;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

public class FileCopy  extends AbstractService   {
	private static final Logger logger = LoggerFactory.getLogger(FileCopy.class);
	
	public FileCopy(String clientSpecpath, String processorSpecPath) throws Exception {
		super(clientSpecpath, processorSpecPath);
		logger.info("PGPEncoderDecoder constructor called");
	}
	public FileCopy(String clientSpecpath, String processorSpecPath,String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeDate);
		logger.info("PGPEncoderDecoder constructor called");
	}
	public FileCopy(String clientSpecpath, String processorSpecPath,RuntimeContext runtimeContext,String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath,runtimeContext, runtimeDate);
		logger.info("PGPEncoderDecoder constructor called");
	}
	public Status service(ClientSpec clientSpec, JSONObject processorSpec) throws JSchException, SftpException, Exception 
	{
		//Reading ProcessorSecifications
		JSONArray listArray = (JSONArray)processorSpec.get(CommonConstants.LIST);
		int length=listArray.size();
		int index=0;
		Status fileCopyStatus =null;
		for(index=0;index<length;index++)
		{	
			JSONObject list=(JSONObject)listArray.get(index);
			JSONObject source = (JSONObject)list.get(CommonConstants.SOURCE);
			JSONObject dest = (JSONObject)list.get(CommonConstants.DESTINATION);
			//Generating Date format
			String sourceDateSuffix = (String) source.get(CommonConstants.FEED_DATE);
			String destDateSuffix = (String) dest.get(CommonConstants.FEED_DATE);
			if ( sourceDateSuffix == null || sourceDateSuffix.isEmpty())
			{
				sourceDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
			}
			if (destDateSuffix == null || destDateSuffix.isEmpty() )
			{
				destDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
			}
			//getting source Endpoint details
			String sourceEndPoint = (String)source.get(CommonConstants.ENDPOINTNAME);
			String sourcePath = (String)source.get(CommonConstants.PATH);
			String sourceFileName = (String)source.get(CommonConstants.FILE_NAME);
			
			//getting Destination Endpoint details
			String destEndPoint = (String)dest.get(CommonConstants.ENDPOINTNAME);
			String destPath = (String)dest.get(CommonConstants.PATH);
			String destFileName = (String)dest.get(CommonConstants.FILE_NAME);
			destPath = replaceDynamicFolders(destPath, destDateSuffix);
			sourcePath = replaceDynamicFolders(sourcePath, sourceDateSuffix);
			destFileName = replaceDateTag(destFileName, destDateSuffix);
			sourceFileName = replaceDateTag(sourceFileName, sourceDateSuffix);
			logger.debug("Input File Source Endpoint :: "+sourceEndPoint);
			logger.debug("Output File destination Endpoint:: "+destEndPoint);
			logger.debug("Input File Source Path --> "+sourcePath+" :: FileName --> "+sourceFileName);
			logger.debug("Output File destination --> "+destPath+" :: and FileName --> "+destFileName);
			
			IEndPointResource sourceEndPointResource=null;
			IEndPointResource destEndPointResource=null;
			try
			{
				//Defining Source End point Properties		
				String sourceType = super.getTypeOfEndPoint(clientSpec, sourceEndPoint);
				HashMap<String, String> sourceEndPointResourceProperties = super.buildResourceProperties(sourceEndPoint, clientSpec,sourceType);
				sourceEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(sourceType, sourceEndPointResourceProperties);
				//Defining Destination end point Properties
				String destType = getTypeOfEndPoint(clientSpec, destEndPoint);
				HashMap<String, String> destEndPointResourceProperties = super.buildResourceProperties(destEndPoint, clientSpec,destType);
				destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(destType, destEndPointResourceProperties);
				//Checking the existence of file on source
				boolean isFileExist =sourceEndPointResource.checkFileExistence(sourcePath, sourceFileName);
				logger.info("Checking the source file existence at "+sourcePath+sourceFileName);
				if(isFileExist)
				{
					logger.info(sourceFileName+" is present in "+sourcePath);
					//Read the file into bufferStream to redirect to Destination
					logger.info("Reading the source file ...");
					InputStream inputStream = sourceEndPointResource.getFile(sourcePath, sourceFileName);
					if(inputStream!=null)
					{
						logger.info("Writing the source file to destination file");
						destEndPointResource.writeFile(inputStream, destPath, destFileName);
						//check for the written file at destination end to ensure the file copied
						logger.info("Verifying the destination file creation...");
						boolean isDestFileExist=destEndPointResource.checkFileExistence(destPath, destFileName);
						if(isDestFileExist)
						{
							logger.info("Done!!"+sourcePath+sourceFileName+" has been copied successfully to "+destPath+destFileName);
							fileCopyStatus=new Status(Status.STATUS_SUCCESS,"FileCopy procesor has been successful!!");
						}
						else
						{
							logger.error("Writing to destination failed!! Dest Path : "+destPath);
							fileCopyStatus =new Status(Status.STATUS_ERROR,"Writing to destination failed!! Dest Path : "+destPath);
							return fileCopyStatus;
						}
					}
					else
					{
						fileCopyStatus =new Status(Status.STATUS_ERROR,"Failed to establish connection to source File!!");
						logger.error("Failed to make connection to source file"+sourcePath+sourceFileName);
						return fileCopyStatus;
					}
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				fileCopyStatus =new Status(Status.STATUS_ERROR,"Error in reading JSONSpec");
			}
			finally
			{
				//Close the connections Destination and then Source
				try
				{
					destEndPointResource.closeConnection();
					sourceEndPointResource.closeConnection();
				}
				catch (Exception e)
				{
					fileCopyStatus =new Status(Status.STATUS_ERROR,"Unable to create EndPointResource Objects");
					logger.error("Unable to create EndPointResource Objects");
					e.printStackTrace();
				}
			}
		}
		return fileCopyStatus;
	}
}
