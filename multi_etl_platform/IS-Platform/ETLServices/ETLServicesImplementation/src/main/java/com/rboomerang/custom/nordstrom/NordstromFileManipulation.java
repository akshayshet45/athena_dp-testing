package com.rboomerang.custom.nordstrom;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.mail.internet.NewsAddress;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.CSVReader;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

public class NordstromFileManipulation extends AbstractService {
	private static Logger logger = LoggerFactory.getLogger(NordstromFileManipulation.class);

	public NordstromFileManipulation(String clientSpecpath, String processorSpecPath) throws Exception {
		super(clientSpecpath, processorSpecPath);
		logger.info("NordstromFileManipulation constructor called");
	}

	public NordstromFileManipulation(String clientSpecpath, String processorSpecPath, String runtimeDate)
			throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeDate);
		logger.info("NordstromFileManipulation constructor called");
	}

	public NordstromFileManipulation(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext,
			String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.info("NordstromFileManipulation constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject processorSpec) throws Exception {
		String delimiter = ",";
		File outputFile = File.createTempFile("output", ".tmp");
		File faultReportOutputFile = File.createTempFile("faultReport", ".tmp");
		int commonHeadersCount = 16;
		int splitCount = 39;
		int eachSplitHeaderCount = 6;
		int totalcount = 250;

		JSONObject list = (JSONObject) processorSpec.get(CommonConstants.LIST);
		JSONObject source = (JSONObject) list.get(CommonConstants.SOURCE);
		JSONObject outputDest = (JSONObject) list.get("outputFileDestination");
		JSONObject reportDest = (JSONObject) list.get("reportFileDestination");

		String sourceDateSuffix = (String) source.get(CommonConstants.FEED_DATE);
		String outputDestDateSuffix = (String) outputDest.get(CommonConstants.FEED_DATE);
		String reportDestDateSuffix = (String) reportDest.get(CommonConstants.FEED_DATE);
		if (sourceDateSuffix == null || sourceDateSuffix.isEmpty())
			sourceDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
		if (outputDestDateSuffix == null || outputDestDateSuffix.isEmpty())
			outputDestDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
		if (reportDestDateSuffix == null || reportDestDateSuffix.isEmpty())
			reportDestDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();

		// getting source Endpoint details
		String sourceEndPoint = (String) source.get(CommonConstants.ENDPOINTNAME);
		// getting source file path from spec file and replacing date month or
		// year from it!
		String sourcePath = (String) source.get(CommonConstants.PATH);
		// getting source file name from spec file and replacing feedDate from
		// it!
		String sourceFileName = (String) source.get(CommonConstants.FILE_NAME);

		// getting Destination Endpoint details
		String outputDestEndPoint = (String) outputDest.get(CommonConstants.ENDPOINTNAME);
		String reportDestEndPoint = (String) reportDest.get(CommonConstants.ENDPOINTNAME);

		// getting destination file path from spec file and replacing date month
		// or year from it!
		String outputDestPath = (String) outputDest.get(CommonConstants.PATH);
		String reportDestPath = (String) reportDest.get(CommonConstants.PATH);
		// getting destination file path from spec file and replacing feedDate
		// from it!

		String outputDestFileName = (String) outputDest.get(CommonConstants.FILE_NAME);
		outputDestPath = replaceDynamicFolders(outputDestPath, outputDestDateSuffix);
		String reportDestFileName = (String) reportDest.get(CommonConstants.FILE_NAME);
		reportDestPath = replaceDynamicFolders(reportDestPath, reportDestDateSuffix);
		sourcePath = replaceDynamicFolders(sourcePath, sourceDateSuffix);

		outputDestFileName = replaceDateTag(outputDestFileName, outputDestDateSuffix);
		reportDestFileName = replaceDateTag(reportDestFileName, reportDestDateSuffix);

		sourceFileName = replaceDateTag(sourceFileName, sourceDateSuffix);
		logger.info("Input File Source Endpoint :: " + sourceEndPoint);
		logger.info("Output File destination Endpoint:: " + outputDestEndPoint);
		logger.info("Report File destination Endpoint:: " + reportDestEndPoint);
		logger.info("Input File Source Path --> " + sourcePath + " :: FileName --> " + sourceFileName);
		logger.info("Output File destination --> " + outputDestPath + " :: and FileName --> " + outputDestFileName);
		logger.info("Report File destination --> " + reportDestPath + " :: and FileName --> " + reportDestFileName);

		File sourceZipFile = File.createTempFile("sourceZipFile", ".tmp");

		// getting source endpoint properties
		String sourceType = super.getTypeOfEndPoint(clientSpec, sourceEndPoint);
		HashMap<String, String> sourceEndPointResourceProperties = super.buildResourceProperties(sourceEndPoint,
				clientSpec, sourceType);

		// making connection with source endpoint
		IEndPointResource sourceEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(sourceType,
				sourceEndPointResourceProperties);

		// downloading the sourceFile
		InputStream sourceStream = sourceEndPointResource.getFile(sourcePath, sourceFileName);
		if ((sourceFileName.split("\\."))[1].equalsIgnoreCase("zip")) {
			try {
				unzip(sourceStream, sourceZipFile.getAbsolutePath());
			} catch (Exception e) {
				if (e.getMessage().contains("Zip Contains multiple files"))
					return new Status(Status.STATUS_ERROR, sourceFileName + " at path " + sourcePath + " at "
							+ sourceEndPoint + " contains multiple files!");
				else if (e.getMessage().contains("zip is empty!"))
					return new Status(Status.STATUS_ERROR,
							sourceFileName + " at path " + sourcePath + " at " + sourceEndPoint + " is empty");
				else
					return new Status(Status.STATUS_ERROR, "Exception occured : " + e.getMessage());
			}
			processSourceFile(new FileInputStream(sourceZipFile), delimiter, commonHeadersCount, splitCount, eachSplitHeaderCount, totalcount,
					outputFile, faultReportOutputFile);

		} else {
			processSourceFile(sourceStream, delimiter, commonHeadersCount, splitCount, eachSplitHeaderCount, totalcount,
					outputFile, faultReportOutputFile);
		}
		sourceEndPointResource.closeConnection();

		// getting destination end point properties
		String outputDestType = getTypeOfEndPoint(clientSpec, outputDestEndPoint);
		HashMap<String, String> outputDestEndPointResourceProperties = super.buildResourceProperties(outputDestEndPoint,
				clientSpec, outputDestType);

		// making connection with source endpoint
		IEndPointResource outputDestEndPointResource = EndPointResourceFactory.getInstance()
				.getEndPointResource(outputDestType, outputDestEndPointResourceProperties);

		// uploading encrypted file
		outputDestEndPointResource.writeFile(new FileInputStream(new File(outputFile.getAbsolutePath())),
				outputDestPath, outputDestFileName);
		outputDestEndPointResource.closeConnection();

		// getting destination end point properties
		String reportDestType = getTypeOfEndPoint(clientSpec, reportDestEndPoint);
		HashMap<String, String> reportDestEndPointResourceProperties = super.buildResourceProperties(reportDestEndPoint,
				clientSpec, reportDestType);

		// making connection with source endpoint
		IEndPointResource reportDestEndPointResource = EndPointResourceFactory.getInstance()
				.getEndPointResource(reportDestType, reportDestEndPointResourceProperties);

		// uploading encrypted file
		reportDestEndPointResource.writeFile(new FileInputStream(new File(faultReportOutputFile.getAbsolutePath())),
				reportDestPath, reportDestFileName);
		reportDestEndPointResource.closeConnection();

//		outputFile.delete();
//		faultReportOutputFile.delete();
		return new Status(Status.STATUS_SUCCESS, "Nordstrom fileManipulation Successful");

	}

	private static void processSourceFile(InputStream sourceStream, String delimiter, int commonHeadersCount,
			int splitCount, int eachSplitHeaderCount, int totalcount, File outputFile, File faultReportOutputFile)
					throws Exception {

		logger.info("outputFile path : " + outputFile.getAbsolutePath());
		logger.info("faultReportFile path : " + faultReportOutputFile.getAbsolutePath());
		int lineCount = 0;
		int faultylines = 0;
		
		BufferedReader sourceReader = new BufferedReader(new InputStreamReader(sourceStream));
		
		
		BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFile));
		BufferedWriter faultyLinesWriter = new BufferedWriter(new FileWriter(faultReportOutputFile));
		
		InputStream lineStream = IOUtils.toInputStream(sourceReader.readLine(), "UTF-8");
		CSVReader csvreader = new CSVReader(new InputStreamReader(lineStream),',','"');
		String[] originalHeader = csvreader.readNext();
		csvreader.close();
		lineStream.close();
		
		if (originalHeader == null) {
			outputWriter.close();
			faultyLinesWriter.close();
			outputFile.delete();
			faultReportOutputFile.delete();
			logger.error("Input source file is empty!");
			throw new Exception("Input source file is empty!");
		}
		String competitiors[] = new String[splitCount];
		for (int i = commonHeadersCount, j = 0; i < originalHeader.length - 1; i += eachSplitHeaderCount, j++) {
			competitiors[j] = (originalHeader[i].split(" "))[0];
			logger.debug("competitior : " + competitiors[j]);
		}
		logger.debug("total competitors : " + splitCount);
		outputWriter
				.write("Product,# matches,Prod. Id,ColorCode,Color,Cat. level 1,Category path,My Price,My Size Count,Mfgr/Brand,Availability,Sale/Promo,Labels,Competitors' lowest price,Competitors' highest price,Division,"
						+ "competitor,competitor price,competitor last scan,competitor size counts,competitor Availability,competitor Sale/Promo,competitor URL");
		faultyLinesWriter.write("lineNumber,columnCount,"
				+ "Product,# matches,Prod. Id,ColorCode,Color,Cat. level 1,Category path,My Price,My Size Count,Mfgr/Brand,Availability,Sale/Promo,Labels,Competitors' lowest price,Competitors' highest price,Division,"
				+ "competitor,competitor price,competitor last scan,competitor size counts,competitor Availability,competitor Sale/Promo,competitor URL");
//		String line = null;
		while (true) {
//			line = sourceReader.readLine();
			lineCount++;
//			if (line == null)
//				break;
			
			try{
			lineStream = IOUtils.toInputStream(sourceReader.readLine(), "UTF-8");}
			catch(NullPointerException nE)
			{
				logger.debug("End of File reached!! Total Line Count : "+lineCount);
				break;
			}
			csvreader = new CSVReader(new InputStreamReader(lineStream),',','"');
			String[] column = csvreader.readNext();
			csvreader.close();
			lineStream.close();

			if (column.length != totalcount) {
				faultyLinesWriter.write("\n");
				faultyLinesWriter.write(lineCount + " " +column.length + " " + column);
				faultylines++;
				continue;
			} else {
				for (int competitorCount = 0; competitorCount < splitCount; competitorCount++) {
					outputWriter.write("\n");
					for (int i = 0; i < commonHeadersCount; i++) {
						column[i]=column[i].replaceAll(",", " ").replaceAll("\"", "");
						outputWriter.write(column[i] + delimiter);
						// logger.info(column[i]);
					}
					competitiors[competitorCount]=competitiors[competitorCount].replaceAll(",", " ").replaceAll("\"", "");
					outputWriter.write(competitiors[competitorCount] + delimiter);
					// logger.info(competitiors[competitorCount]);
					for (int i = commonHeadersCount + competitorCount * eachSplitHeaderCount; i < commonHeadersCount
							+ (competitorCount + 1) * eachSplitHeaderCount; i++) {
						column[i]=column[i].replaceAll(",", " ").replaceAll("\"", "");
						outputWriter.write(column[i]);
						// logger.info(column[i]);
						if (i < commonHeadersCount + (competitorCount + 1) * eachSplitHeaderCount - 1)
							outputWriter.write(delimiter);
					}

				}

			}

		}
		logger.error(
				"*****************************total no of lines: " + lineCount + " , faulty lines : " + faultylines);
		outputWriter.close();
		faultyLinesWriter.close();

	}

	private void unzip(InputStream inputStream, String opFilePath) throws Exception {

		ZipInputStream zipIs = null;
		ZipEntry zEntry = null;

		zipIs = new ZipInputStream(inputStream);
		String fileName = null;
		zEntry = zipIs.getNextEntry();
		if (zEntry == null) {
			logger.error("zip is empty!");
			throw new Exception("zip is empty!");
		}
		byte[] tmp = new byte[4 * 1024];
		FileOutputStream fos = null;
		fileName = zEntry.getName();
		logger.debug("Extracting following " + fileName + " file to " + opFilePath);
		fos = new FileOutputStream(opFilePath);
		int size = 0;
		while ((size = zipIs.read(tmp)) != -1) {
			fos.write(tmp, 0, size);
		}
		fos.flush();
		fos.close();
		if (zipIs.getNextEntry() != null) {
			logger.error("zip contains more than one file or directories !");
			throw new Exception("Zip Contains multiple files");
		}
		zipIs.close();
	}

}
