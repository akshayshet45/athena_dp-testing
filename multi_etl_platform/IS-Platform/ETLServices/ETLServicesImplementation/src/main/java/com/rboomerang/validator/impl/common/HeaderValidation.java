package com.rboomerang.validator.impl.common;

import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

public class HeaderValidation extends AbstractService {
	private static Logger logger = LoggerFactory.getLogger(HeaderValidation.class);

	public HeaderValidation(String clientSpecpath, String validatorSpecPath, RuntimeContext runtimeContext,
			String runtimeDate) throws Exception {
		super(clientSpecpath, validatorSpecPath, runtimeContext, runtimeDate);
		logger.info("Header Validation constructor called");
	}

	public HeaderValidation(String clientSpecpath, String validatorSpecPath, String runtimeDate) throws Exception {
		super(clientSpecpath, validatorSpecPath, runtimeDate);
		logger.info("Header Validation constructor called");
	}

	public HeaderValidation(String clientSpecpath, String validatorSpecPath) throws Exception {
		super(clientSpecpath, validatorSpecPath);
		logger.info("Header Validation constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject validatorSpec)
			throws JSchException, SftpException, Exception {
		JSONArray filesToCheck = (JSONArray) validatorSpec.get(CommonConstants.LIST);
		StringBuilder validFiles = new StringBuilder();
		StringBuilder invalidFiles = new StringBuilder();

		for (int file = 0; file < filesToCheck.size(); file++) {
			JSONObject obj = (JSONObject) filesToCheck.get(file);

			String endpointName = (String) obj.get(CommonConstants.ENDPOINT);
			String location = (String) obj.get(CommonConstants.LOCATION);
			String fileName = (String) obj.get(CommonConstants.FILENAME);
			String dateSuffix = (String) obj.get(CommonConstants.FEED_DATE);
			String headerFileNameInFeedMetaData = (String) obj.get("headerFileName");

			if (dateSuffix.isEmpty() || dateSuffix == null)
				dateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
			
			String endpointType = getTypeOfEndPoint(clientSpec, endpointName);
			location = replaceDynamicFolders(location, dateSuffix);
			fileName = replaceDateTag(fileName, dateSuffix);

			if (endpointType.equals("sftp"))
				fileName = makeRegex(fileName);

			HashMap<String, String> endPointResourceProperties = buildResourceProperties(endpointName, clientSpec,
					endpointType);
			IEndPointResource endPointResource = EndPointResourceFactory.getInstance().getEndPointResource(endpointType,
					endPointResourceProperties);
			String fileheaderLine =null;
			try {
				fileheaderLine=endPointResource.getFileHeaderLine(location, fileName);	
			} catch (Exception e) {
				logger.error("Exception occured :: ",e);
				return new Status(Status.STATUS_ERROR, e.getMessage());
			}
			 

			fileName = (String) obj.get(CommonConstants.FILENAME);
			fileName = replaceDateTag(fileName, dateSuffix);
			fileName = fileName.replaceAll(CommonConstants.REGEX_TAG, "");
			
			String expectedHeader = getHeaderLine(fileName,
					this.getRuntimeContext().transformPath(clientSpec.getFeedMetadata()),headerFileNameInFeedMetaData);

			logger.info("This is the expected header\n" + expectedHeader);
			logger.info("This is the actual header\n" + fileheaderLine);

			String change = compareHeaders(expectedHeader, fileheaderLine);
			logger.info(change);

			if (change.equals(StringUtils.EMPTY))
				validFiles.append(location).append(fileName).append(CommonConstants.NEWLINE);
			else
				invalidFiles.append(location).append(fileName).append(CommonConstants.TAB).append(change)
						.append(CommonConstants.NEWLINE);
		}

		Status status = null;
		if (invalidFiles.toString().isEmpty())
			status = new Status(Status.STATUS_SUCCESS, validFiles.toString());
		else
			status = new Status(Status.STATUS_ERROR, invalidFiles.toString());

		return status;
	}

	private String compareHeaders(String expectedHeaderLine, String fileHeaderLine) {
		expectedHeaderLine.trim();
		fileHeaderLine.trim();
		if (expectedHeaderLine.equals(fileHeaderLine)) {
			logger.info("Headers Equal :) ");
			return StringUtils.EMPTY;
		}

		logger.info("Headers Not Equal :( ");
		return StringUtils.difference(expectedHeaderLine, fileHeaderLine);
	}
}