package com.rboomerang.processor.impl.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.FileProcessingHandler;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

/**
 * This converter would take care of converting a file of one type to other
 * type. Like xlsx to csv,csv to xlsx. If any new converter need to be added,
 * only the conversion method need to be added to FileProcessingHandler and that
 * could be integrated to FileConverter.
 * 
 * @author sumit
 *
 */
public class FileConverter extends AbstractService {
	private static final Logger logger = LoggerFactory.getLogger(FileConverter.class);
	private static Set<String> fileToBeConvertedMapping = new HashSet<>();
	static {
		fileToBeConvertedMapping.add("csvToxlsx");
		fileToBeConvertedMapping.add("xlsxTocsv");
	}

	public FileConverter(String clientSpecpath, String processorSpecPath) throws Exception {
		super(clientSpecpath, processorSpecPath);
		logger.info("FileConvertor constructor called");
	}

	public FileConverter(String clientSpecpath, String processorSpecPath, String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeDate);
		logger.info("FileConvertor constructor called");
	}

	public FileConverter(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext,
			String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.info("FileConvertor constructor called");
	}

	public Status service(ClientSpec clientSpec, JSONObject processorSpec)
			throws JSchException, SftpException, Exception {
		logger.info("Initiating FileConvertor service");
		JSONObject list = (JSONObject) processorSpec.get(CommonConstants.LIST);
		JSONObject source = (JSONObject) list.get(CommonConstants.SOURCE);
		JSONObject dest = (JSONObject) list.get(CommonConstants.DESTINATION);

		// getting feed_date date format for source and destination file
		String sourceDateFormat = (String) source.get(CommonConstants.FEED_DATE);
		String destDateFormat = (String) dest.get(CommonConstants.FEED_DATE);
		if (sourceDateFormat == null || sourceDateFormat.isEmpty())
			sourceDateFormat = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
		if (destDateFormat == null || destDateFormat.isEmpty())
			destDateFormat = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
		// getting source Endpoint details
		String sourceEndPoint = (String) source.get(CommonConstants.ENDPOINTNAME);
		// getting source file path from spec file and replacing date month or
		// year from it!
		String sourcePath = replaceDynamicFolders((String) source.get(CommonConstants.PATH), sourceDateFormat);
		// getting source file name from spec file and replacing feedDate from
		// it!
		String sourceFileName = replaceDateTag((String) source.get(CommonConstants.FILE_NAME), sourceDateFormat);
		// getting Destination Endpoint details
		String destEndPoint = (String) dest.get(CommonConstants.ENDPOINTNAME);
		// getting destination file path from spec file and replacing date month
		// or year from it!
		String destPath = replaceDynamicFolders((String) dest.get(CommonConstants.PATH), destDateFormat);
		// getting destination file name from spec file and replacing feedDate
		// from it!
		String destFileName = replaceDateTag((String) dest.get(CommonConstants.FILE_NAME), destDateFormat);
		logger.debug("Source End Point: " + sourceEndPoint);
		logger.debug("Source File path: " + sourcePath);
		logger.debug("Source File Name: " + sourceFileName);
		logger.debug("Destination End Point: " + destEndPoint);
		logger.debug("Destination File path: " + destPath);
		logger.debug("Destination File Name: " + destFileName);
		String delimiter = (String) list.get(CommonConstants.DELIMITER);
		String qouteCharacter = (String) list.get("qouteCharacter");
		String skipLines = (String) list.get("skipLines");
		char quoteChar = '"';
		char delim = ',';
		Boolean multipleXlsxSheet = (Boolean) list.get("multipleXlsxSheet");
		if (StringUtils.isNotEmpty(qouteCharacter)) {
			quoteChar = qouteCharacter.charAt(0);
		}
		if (StringUtils.isNotEmpty(delimiter)) {
			delim = delimiter.charAt(0);
		}
		int linesToSkip = 0;
		if (StringUtils.isNoneEmpty(skipLines)) {
			try {
				linesToSkip = Integer.parseInt(skipLines);
			} catch (NumberFormatException ne) {
				logger.error("Unable to parse skipLines from job spec.");
				throw ne;
			}
		}
		// getting source endpoint properties
		String sourceType = super.getTypeOfEndPoint(clientSpec, sourceEndPoint);
		if (sourceType.equals("sftp"))
			sourceFileName = makeRegex(sourceFileName);
		IEndPointResource destEndPointResource = null;
		IEndPointResource sourceEndPointResource = null;
		File tempFile = File.createTempFile("processed", ".tmp");
		Status fileCopyStatus = null;
		try {
			String conversionType = getFileConversionType(sourceFileName, destFileName);
			if (!fileToBeConvertedMapping.contains(conversionType)) {
				logger.error("File Conversion not suported for the conversionType: " + conversionType);
				return new Status(Status.STATUS_ERROR,
						"File Conversion not suported for the conversionType: " + conversionType);
			}
			if("csvToxlsx".equals(conversionType)){
				logger.info("DelimeterToBeused: " + delim + " ::QouteCharacterToBeUsed: " + quoteChar);
			}
			HashMap<String, String> sourceEndPointResourceProperties = super.buildResourceProperties(sourceEndPoint,
					clientSpec, sourceType);

			// making connection with source endpoint
			sourceEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(sourceType,
					sourceEndPointResourceProperties);

			// downloading the sourceFile
			InputStream iStream = sourceEndPointResource.getFile(sourcePath, sourceFileName);
			logger.info("Started file conversion of type:" + conversionType);
			if (conversionType.equals("csvToxlsx")) {
				FileProcessingHandler.convertCSVToXLSX(iStream, tempFile, linesToSkip, quoteChar, delim);
			} else if (conversionType.equals("xlsxTocsv")) {
				FileProcessingHandler.processXLSXFile(iStream, tempFile, linesToSkip, multipleXlsxSheet);
			}
			logger.info("Finished file conversion of type:" + conversionType);
			// getting destination end point properties
			String destType = getTypeOfEndPoint(clientSpec, destEndPoint);
			HashMap<String, String> destEndPointResourceProperties = super.buildResourceProperties(destEndPoint,
					clientSpec, destType);

			// making connection with destination endpoint
			destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(destType,
					destEndPointResourceProperties);

			// uploading encrypted file
			destEndPointResource.writeFile(new FileInputStream(new File(tempFile.getAbsolutePath())), destPath,
					destFileName);

			fileCopyStatus = new Status(Status.STATUS_SUCCESS, "FileConvertor successful. Processed file "
					+ destFileName + " put to path " + destPath + " at endpoint " + destEndPoint);

		} finally {
			if (tempFile.exists()) {
				tempFile.delete();
			}
			try {
				if (sourceEndPointResource != null) {
					sourceEndPointResource.closeConnection();
				}
				if (destEndPointResource != null) {
					destEndPointResource.closeConnection();
				}
			} catch (Exception e) {
				fileCopyStatus = new Status(Status.STATUS_ERROR, "Unable to close EndPointResource Objects");
				logger.error("Unable to close EndPointResource Objects", e);
			}
		}
		return fileCopyStatus;
	}

	/**
	 * 
	 * @param sourceFileName
	 * @param destFileName
	 * @return
	 * @throws Exception
	 */
	private static String getFileConversionType(String sourceFileName, String destFileName) throws Exception {
		String sourceFileExtension = sourceFileName.substring(sourceFileName.lastIndexOf('.') + 1,
				sourceFileName.length());
		String destFileExtension = destFileName.substring(destFileName.lastIndexOf('.') + 1, destFileName.length());
		if (StringUtils.isEmpty(sourceFileExtension) || StringUtils.isEmpty(destFileExtension)) {
			throw new Exception("File extensions cannot be empty for conversion");
		}
		String conversionType = sourceFileExtension + "To" + destFileExtension;
		return conversionType;
	}
}
