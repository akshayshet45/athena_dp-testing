package com.rboomerang.custom.poextention;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.AWSAccess;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;
import com.rboomerang.utils.endpoint.S3ResourceEndPoint;

public class RScriptGenericExecutor extends AbstractService
{
	private AmazonS3Client s3Client;
	private String regionName;
	private Region region;
	private HashMap<String, String> inputmap;
	private static final Logger logger = LoggerFactory.getLogger(RScriptGenericExecutor.class);
	//private String outputFileLocation="/mnt/poextention/client/";
	public RScriptGenericExecutor(String clientSpecpath, String processorSpecPath) throws Exception
	{
		super(clientSpecpath, processorSpecPath);
		logger.info("RScriptExecutor constructor called");
	}

	public RScriptGenericExecutor(String clientSpecpath, String processorSpecPath, String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeDate);
		logger.info("RScriptExecutor constructor called");
	}

	public RScriptGenericExecutor(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext,
			String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.info("RScriptExecutor constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject serviceSpec) throws JSchException, SftpException, Exception
	{
		JSONObject rParams= (JSONObject)serviceSpec.get("params");
		JSONObject destParams= (JSONObject)serviceSpec.get(CommonConstants.DESTINATION);
		Status status=new Status(Status.STATUS_ERROR,"RScript Execution Failed!!");
		
		//Downloading PO_Ouput File(s)
		logger.info("Downloading PO ouptut Files from source");
		logger.debug("Reading source and dest parameters ");
		logger.debug("Destination Parameters"+destParams.toJSONString());
		String destLocation=destParams.get("output_location").toString();
		String auditDestLocation=(String) destParams.get("audit_location");
		String outputFileLocation=rParams.get("OutputPath").toString();
		
		
		
		String destEndPointName=destParams.get(CommonConstants.ENDPOINTNAME).toString();
		String destEndPointType= super.getTypeOfEndPoint(clientSpec, destEndPointName);
		HashMap<String, String> destEndPointResourceProperties = buildResourceProperties(destEndPointName, clientSpec,destEndPointType);
		IEndPointResource destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(destEndPointType,destEndPointResourceProperties);
		
		
		//Download The files
		
		new File(outputFileLocation).mkdirs();
		
			logger.info("Cleaning up the directory to remove old files..!!");
			for(File t: new File(outputFileLocation).listFiles())
			{
				if(t.getName().endsWith(".xlsx")||t.getName().endsWith(".csv"))
				{
				if(!t.delete())
				{
					logger.error("Error in cleaning up direcotry");
					throw new IOException("Cleaning up directory failed!! Aborting run!!");
				}
				}
			}
			
			logger.debug("Executing RScripts for the file ");
			File[] listOuputFiles=executeRScripts(rParams);
			logger.info("Uploading files to destination ");
			
			AWSAccess awsAccess = new AWSAccess();
			s3Client = new AmazonS3Client(awsAccess.getAWSAccess());
			
			String output_s3_path=destLocation.substring(5);
			String audit_s3_path=auditDestLocation.substring(5);
			
			int index = output_s3_path.indexOf("/");
			String s3_bucket_output = (String) output_s3_path.substring(0, index);
			String s3_prefix_output = (String) output_s3_path.substring(index+1);
			
			index = audit_s3_path.indexOf("/");
			String audit_s3_bucket_output = (String) audit_s3_path.substring(0, index);
			String audit_s3_prefix_output = (String) audit_s3_path.substring(index+1);
			
			File dir = new File(outputFileLocation);
			File[] directoryListing = dir.listFiles();
			
			for(File f:directoryListing)
			{
				String name=f.getName();
				if(name.endsWith(".xlsx")||name.endsWith(".csv"))
				{
					
					InputStream inputStream=new FileInputStream(f);
					if(name.contains("r_script_output"))
					{
						this.storeObject(s3_bucket_output, s3_prefix_output, name, inputStream);
					}
					else if(name.contains("r_script_audit"))
					{
						this.storeObject(audit_s3_bucket_output, audit_s3_prefix_output, name, inputStream);
					}
					
					
					//delete the file locally upon upload
					boolean delStatus=f.delete();
					if(delStatus)
					{
						logger.debug("file "+name+" has been deleted");
					}
					else
					{
						logger.warn("Unable to delete temp file "+name);
					}
				}
					
			}
		status=new Status(Status.STATUS_SUCCESS,"RScript Execution succeeded!!");
		return status;
	}
	
	
	private File[] executeRScripts(JSONObject rParams) throws IOException, InterruptedException
	{
		String outputFileLocation=rParams.get("ScriptDirectory").toString();
		Runtime runEnv= Runtime.getRuntime();
		StringBuilder command=new StringBuilder();
		command.append("/usr/bin/Rscript").append(CommonConstants.SPACE)
			   .append(outputFileLocation).append(rParams.get("RSrciptName").toString()).append(CommonConstants.SPACE).append(rParams.get("RuntimeParameters"));
		logger.debug("Running command "+command.toString());
		Process child= runEnv.exec(command.toString());
		int exitValue=-1;
		exitValue=child.waitFor();
		if(exitValue!=0)
		{
			//Printing the script output/error for debug
			Scanner errorScan=new Scanner(child.getErrorStream());
			String err="";
			while(errorScan.hasNext())
			{
				err += errorScan.nextLine();
			}
			errorScan.close();
			logger.error(err);
			throw new IOException(err);
		}
		else
		{
			logger.debug("RScripts Execution is successfull");
			//Retrieving generated output Files and Writing them to Destination end point
			File[] listDir =new File(outputFileLocation).listFiles();
			logger.info("GNL exectuion completed");
			return listDir;
		}
	}
	
	public void storeObject(String bucketName, String prefix, String filename, InputStream input) {
		String bucket = bucketName;
		logger.info("Create Bucket if not present " + bucket);
		createBucketOnAbsence(bucket);
		logger.info("Create prefix in the S3 bucket if not present " + prefix);

		createPrefixOnAbsence(bucket + "/", prefix);

		String container = bucket;
		boolean keyPresent = isKeyAlreadyPresent(container, prefix + filename);
		if (keyPresent) {
			throw new AmazonClientException("Report File already exists");
		} else {
			logger.info("Adding the report file " + input);
			s3Client.putObject(container, prefix + filename, input, new ObjectMetadata());
		}
	}

	private void createBucketOnAbsence(String bucket) throws AmazonClientException, AmazonServiceException {
		if (!s3Client.doesBucketExist(bucket)) {
			logger.info("The specified bucket name is not already present. So creating the bucket");
			s3Client.createBucket(bucket, regionName);

			region = Region.getRegion(Regions.fromName(regionName));
			s3Client.setRegion(region);
		} else {
			logger.info("The specified bucket is already present");
			regionName = s3Client.getBucketLocation(bucket);
			if (regionName.equals("US")) {
				regionName = "us-east-1";
			} else if (regionName.equals("EU")) {
				regionName = "eu-west-1";
			}
			region = Region.getRegion(Regions.fromName(regionName));
			s3Client.setRegion(region);
		}
	}

	private void createPrefixOnAbsence(String bucket, String prefix)
			throws AmazonClientException, AmazonServiceException {

		if (!isKeyAlreadyPresent(bucket, prefix)) {
			logger.info("Adding the prefix " + prefix);
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentLength(0);
			// Create empty content
			InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
			PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, prefix, emptyContent, metadata);

			s3Client.putObject(putObjectRequest);
		} else {
			logger.info("The prefix is already present " + prefix);
		}
	}

	public boolean isPresent(String key) {
		return isKeyAlreadyPresent(inputmap.get(S3ResourceEndPoint.S3_BUCKET_NAME), key);
	}

	@SuppressWarnings("unused")
	private boolean isKeyAlreadyPresent(String container, String key)
			throws AmazonClientException, AmazonServiceException {
		try {
			logger.info("Checking if the key is already present " + key + " container - " + container);
			S3Object object = s3Client.getObject(container, key);
		} catch (AmazonS3Exception Ae) {
			logger.info("The key is not present " + key);
			String errorCode = Ae.getErrorCode();
			logger.info("The error code is " + errorCode, Ae);
			if (errorCode.equalsIgnoreCase("NoSuchKey")) {
				return false;
			} else
				return true;
		} catch (AmazonServiceException e) {
			logger.info("The key is not present " + key);
			String errorCode = e.getErrorCode();
			logger.info("The error code is " + errorCode, e);
			if (errorCode.equalsIgnoreCase("NoSuchKey")) {
				return false;
			} else
				return false;
		}
		
		return true;
	}
	
	public Long getKeySize(String key) throws Exception{
		String container = inputmap.get(S3ResourceEndPoint.S3_BUCKET_NAME);
		
		if(!isKeyAlreadyPresent(container, key))
			throw new Exception("key "+key+" not present on S3 bucket "+container);
		
		return s3Client.getObjectMetadata(container, key).getContentLength();
		
	}
	
	
	public boolean readS3ObjectUsingByteArray(String path, String key, String outputFileLocation) {
		boolean returnVal = false;
		byte[] content = new byte[2048];

		try {
			S3Object s3object = s3Client
					.getObject(new GetObjectRequest(inputmap.get(S3ResourceEndPoint.S3_BUCKET_NAME), path + key));
			InputStream stream = s3object.getObjectContent();
			try (BufferedOutputStream outputStream = new BufferedOutputStream(
					new FileOutputStream(outputFileLocation + key))) {
				int totalSize = 0;
				int bytesRead;
				while ((bytesRead = stream.read(content)) != -1) {
					logger.info(String.format("%d bytes read from stream", bytesRead));
					outputStream.write(content, 0, bytesRead);
					totalSize += bytesRead;
				}
				logger.info("Total Size of file in bytes = " + totalSize);
			}
			// close resource even during exception
			returnVal = true;
		} catch (IOException ex) {
			logger.error("IOException at readS3ObjectUsingByteArray", ex);
		} catch (Exception e) {
			logger.error("Exception at readS3ObjectUsingByteArray", e);
		}
		return returnVal;
	}

	public InputStream readInputStream(String fileLocation, String fileName) {
		String container = inputmap.get(S3ResourceEndPoint.S3_BUCKET_NAME);
		InputStream stream = null;
		logger.info(container);
		
		S3Object object = s3Client.getObject(new GetObjectRequest(container, fileLocation + fileName));
		stream = new BufferedInputStream(object.getObjectContent());
		return stream;
	}
	
}
