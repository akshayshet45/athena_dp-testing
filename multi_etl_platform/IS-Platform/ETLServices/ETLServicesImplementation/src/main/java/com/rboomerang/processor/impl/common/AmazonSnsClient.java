package com.rboomerang.processor.impl.common;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.devicefarm.model.ArgumentException;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.AmazonSnsHelper;

/**
 * Created by vishal on 4/11/16.
 */
public class AmazonSnsClient extends AbstractService {

    private final Logger logger = LoggerFactory.getLogger(AmazonSnsClient.class);


    public AmazonSnsClient(String clientSpecpath, String serviceSpecPath) throws Exception {
        super(clientSpecpath, serviceSpecPath);
        logger.info("AmazonSnsClient constructor called");
    }

    public AmazonSnsClient(String clientSpecpath, String serviceSpecPath, String runtimeDate) throws Exception {
        super(clientSpecpath, serviceSpecPath, runtimeDate);
        logger.info("AmazonSnsClient constructor called");
    }

    public AmazonSnsClient(String clientSpecpath, String serviceSpecPath, RuntimeContext runtimeContext, String runtimeDate) throws Exception {
        super(clientSpecpath, serviceSpecPath, runtimeContext, runtimeDate);
        logger.info("AmazonSnsClient constructor called");
    }

    @Override
    public Status service(ClientSpec clientSpec, JSONObject serviceSpec) throws JSchException, SftpException, Exception {
        boolean response=false;

        try{
            String clientName=clientSpec.getClient();
            String snsName = (String) serviceSpec.get("snsName");
            String snsRegion = (String) serviceSpec.get("snsRegion");
            String serviceApi = (String) serviceSpec.get("serviceApi");
            String responseTopic = (String) serviceSpec.get("responseTopic");

            if(snsName == null || snsRegion == null || serviceApi == null || responseTopic == null)
                throw new ArgumentException("Import Assignment failed");

            JSONObject json = new JSONObject();
            json.put("requestPath",serviceApi);
            json.put("reuqestTTL",600);
            json.put("clientID",clientName);
            json.put("responseTopic", responseTopic);
            json.put("requestMessage",serviceSpec.get("requestMessage"));

            AmazonSnsHelper amazonSnsHelper =new AmazonSnsHelper(snsName,snsRegion);
            response = amazonSnsHelper.publish(json);
        }catch (Exception err){
            logger.info("Exception while executing service in AmazonSns", err);
            response=false;
        }

        logger.info("Response Value :: " + response);

        return new Status(response ? Status.STATUS_SUCCESS : Status.STATUS_ERROR , response ? "Message Published successfully" : "Message publication failed");
    }
}
