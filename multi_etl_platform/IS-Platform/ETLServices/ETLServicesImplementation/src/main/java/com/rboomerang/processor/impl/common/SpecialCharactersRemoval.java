package com.rboomerang.processor.impl.common;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.InputStreamHandler;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

public class SpecialCharactersRemoval extends AbstractService{

	private static Logger logger = LoggerFactory.getLogger(SpecialCharactersRemoval.class);

	//Elements specific to this class:
	private static String CURRENT_CSV_DELIMITER="currentCsvDelimiter";
	private static String REQUIRED_CSV_DELIMITER="requiredCsvDelimiter";


	public SpecialCharactersRemoval(String clientSpecpath, String processorSpecPath , RuntimeContext runtimeContext, String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.debug("SpecialCharactersRemoval cunstructor called");
	}
	public SpecialCharactersRemoval(String clientSpecpath, String processorSpecPath , String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeDate);
		logger.debug("SpecialCharactersRemoval cunstructor called");
	}
	public SpecialCharactersRemoval(String clientSpecpath, String processorSpecPath) throws Exception {
		super(clientSpecpath, processorSpecPath);
		logger.debug("SpecialCharactersRemoval cunstructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject processorSpec)
			throws JSchException, SftpException, Exception {

		//obtaining Common elements from processor spec list 
		JSONObject list = (JSONObject)processorSpec.get(CommonConstants.LIST);
		JSONObject source = (JSONObject)list.get(CommonConstants.SOURCE);
		JSONObject dest = (JSONObject)list.get(CommonConstants.DESTINATION);
		String sourceEndPoint = (String)source.get(CommonConstants.ENDPOINTNAME);
		logger.debug("Input File Source :: "+sourceEndPoint);
		String destEndPoint = (String)dest.get(CommonConstants.ENDPOINTNAME);
		logger.debug("Output File destination :: "+destEndPoint);
		
		String sourceDateSuffix = (String) source.get(CommonConstants.FEED_DATE);
		String destDateSuffix = (String) dest.get(CommonConstants.FEED_DATE);
		
		if ( sourceDateSuffix == null || sourceDateSuffix.isEmpty())
			sourceDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
		if (destDateSuffix == null || destDateSuffix.isEmpty() )
			destDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
		
		String sourcePath = (String)source.get(CommonConstants.PATH);
		String sourceFileName =(String)source.get(CommonConstants.FILE_NAME);
		logger.debug("Input File Source Path --> "+sourcePath+" :: and FileName --> "+sourceFileName);
		
		String destPath = (String)dest.get(CommonConstants.PATH);
		String destFileName = (String)dest.get(CommonConstants.FILE_NAME);
		logger.debug("Output File destination --> "+destPath+" :: and FileName --> "+destFileName);

		destPath = replaceDynamicFolders(destPath, destDateSuffix);
		sourcePath = replaceDynamicFolders(sourcePath, sourceDateSuffix);
		destFileName = replaceDateTag(destFileName, destDateSuffix);
		sourceFileName = replaceDateTag(sourceFileName, sourceDateSuffix);
		
		//obtaining elements from list that are specific to this class
		String currentDelimiter = (String)list.get(CURRENT_CSV_DELIMITER);
		logger.debug("Present CSV Delimiter :: \'"+currentDelimiter+"\'");
		String requiredDelimiter = (String)list.get(REQUIRED_CSV_DELIMITER);
		logger.debug("Required CSV Delimiter :: \'"+requiredDelimiter+"\'");
		String compression = (String)list.get("compression");
		String encoding =(String)list.get("fileEncoding");


		//getting source endpoint properties		
		String sourceType = super.getTypeOfEndPoint(clientSpec, sourceEndPoint);
		HashMap<String, String> sourceEndPointResourceProperties = super.buildResourceProperties(sourceEndPoint, clientSpec,sourceType);
		if (sourceType.equals("sftp"))
			sourceFileName = makeRegex(sourceFileName);
		
		//making connection with source endpoint
		IEndPointResource sourceEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(
				sourceType, sourceEndPointResourceProperties);

		//downloading the sourceFile
		InputStream iStream = sourceEndPointResource.getFile(sourcePath, sourceFileName);
		
		//creating temporary file to download file from endpoint and to save processed data
		File sourceDownloadedFile = File.createTempFile("sourceFile",".tmp");
		File processedFile = File.createTempFile("processedFile",".tmp");
		
		if(compression.equals("zip"))
		{
			try{
				unzip(iStream,sourceDownloadedFile.getAbsolutePath());
			}
			catch(Exception e)
			{
				if(e.getMessage().contains("Zip Contains multiple files"))
					return new Status(Status.STATUS_ERROR, sourceFileName+" at path "+sourcePath+" at "+sourceEndPoint+" contains multiple files!");
				else if (e.getMessage().contains("zip is empty!")) 
					return new Status(Status.STATUS_ERROR, sourceFileName+" at path "+sourcePath+" at "+sourceEndPoint+" is empty");
				else 
					return new Status(Status.STATUS_ERROR, "Exception occured : "+e.getMessage());
			}

		}
		else if(compression.equals(""))
		{
			InputStreamHandler.writeBufferedInputStreamToFile(iStream, sourceDownloadedFile.getAbsolutePath());
		}
		iStream.close();
		sourceEndPointResource.closeConnection();

		//this method replaces current delimiter with required delimiter and saves the file locally as test_unzipfile.csv
		processFile(sourceDownloadedFile,processedFile, currentDelimiter, requiredDelimiter, encoding);

		//getting destination end point properties
		String destType = getTypeOfEndPoint(clientSpec, destEndPoint);
		HashMap<String, String> destEndPointResourceProperties = super.buildResourceProperties(destEndPoint, clientSpec,destType);

		//making connection with source endpoint
		IEndPointResource destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(
				destType, destEndPointResourceProperties);

		//uploading encrypted file
		destEndPointResource.writeFile(new FileInputStream(processedFile), destPath, destFileName);
		destEndPointResource.closeConnection();

		//after uploading file has to be deleted from local system or else running program next time will append new data to old one on file

		sourceDownloadedFile.delete();
		processedFile.delete();

		return new Status(Status.STATUS_SUCCESS, "Special character Removal Success on file "+sourceFileName+" at path "+sourcePath
				+"\n"+destFileName+" File uploaded successfully at path "+destPath);
	}
	private void processFile(File sourceDownloadedFile,File processedFile,String currentDelimiter,String requiredDelimiter,String encoding) throws IOException
	{
		logger.info("Processing file for delimiter change!");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(sourceDownloadedFile),encoding));
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(processedFile));
		String line = null;
		int rows=0;
		while(true){

			line=bufferedReader.readLine();
			if(line==null)
				break;
			rows++;
			line=line.replaceAll("["+currentDelimiter+"]", requiredDelimiter);
			bufferedWriter.write(line+"\n");

		}
		bufferedWriter.close();
		bufferedReader.close();
		logger.debug("Processed "+rows+" rows!!");
	}
	
	private void unzip(InputStream inputStream,String opFilePath) throws Exception{

		ZipInputStream zipIs = null;
		ZipEntry zEntry = null;
		
		zipIs = new ZipInputStream(inputStream);
		String fileName=null;
		zEntry = zipIs.getNextEntry();
		if(zEntry==null){
			logger.error("zip is empty!");
			throw new Exception("zip is empty!");
		}
		byte[] tmp = new byte[4*1024];
		FileOutputStream fos = null;
		fileName=zEntry.getName();
		logger.debug("Extracting following "+fileName+" file to "+opFilePath);
		fos = new FileOutputStream(opFilePath);
		int size = 0;
		while((size = zipIs.read(tmp)) != -1)
		{
			fos.write(tmp, 0 , size);
		}
		fos.flush();
		fos.close();
		if(zipIs.getNextEntry()!=null){
			logger.error("zip contains more than one file or directories !");
			throw new Exception("Zip Contains multiple files");
		}
		zipIs.close();
	}


}
