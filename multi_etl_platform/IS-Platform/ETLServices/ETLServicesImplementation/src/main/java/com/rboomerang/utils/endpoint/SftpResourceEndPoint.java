package com.rboomerang.utils.endpoint;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.rboomerang.utils.RegexUtility;
import com.rboomerang.utils.SFTPHelper;

public class SftpResourceEndPoint implements Closeable, IEndPointResource {

	public static final String SFTP_HOST = "SFTP_HOST";
	public static final String SFTP_PORT = "SFTP_PORT";
	public static final String SFTP_USER = "SFTP_USER";
	public static final String SFTP_PASS = "SFTP_PASS";

	private Session session = null;
	private Channel channel = null;
	private ChannelSftp channelSftp = null;
	private SFTPHelper sftpHelper = null;
	private RegexUtility rUtility = null;
	private HashMap<String, String> initValues = new HashMap<String, String>();
	private static Logger logger = LoggerFactory.getLogger(SftpResourceEndPoint.class);

	public SftpResourceEndPoint(HashMap<String, String> initValues) {
		this.initValues = initValues;
		logger.info("SftpAccess constructor called");
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean checkFileExistence(String baseDirectory, String fileName) throws Exception {
		JSch jsch = new JSch();
		boolean fileFound = false;
		try {
			session = jsch.getSession(initValues.get(SftpResourceEndPoint.SFTP_USER),
					initValues.get(SftpResourceEndPoint.SFTP_HOST),
					Integer.parseInt(initValues.get(SftpResourceEndPoint.SFTP_PORT)));
		} catch (NumberFormatException | JSchException e) {
			logger.error("while creating session sftp", e);
		}
		session.setPassword(initValues.get(SftpResourceEndPoint.SFTP_PASS));

		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);
		try {
			session.connect();
		} catch (JSchException e) {
			logger.error("while connection establishment session sftp", e);
		}
		try {
			channel = session.openChannel("sftp");
		} catch (JSchException e) {
			logger.error("while opening channel sftp", e);
		}
		try {
			channel.connect();
		} catch (Exception e) {
			logger.error(e.getMessage());
			if (e.getMessage().contains("channel is not opened")) {
				channel.disconnect();
				session.disconnect();
				Thread.sleep(50000);
				try {
					session = jsch.getSession(initValues.get(SftpResourceEndPoint.SFTP_USER),
							initValues.get(SftpResourceEndPoint.SFTP_HOST),
							Integer.parseInt(initValues.get(SftpResourceEndPoint.SFTP_PORT)));
				} catch (NumberFormatException | JSchException ex) {
					logger.error("while creating session sftp", ex);
				}
				session.setPassword(initValues.get(SftpResourceEndPoint.SFTP_PASS));
				session.setConfig(config);
				session.connect();
				channel = session.openChannel("sftp");
				channel.connect(60000);
			} else
				throw e;
		}

		channelSftp = (ChannelSftp) channel;

		try {
			Vector<ChannelSftp.LsEntry> list = channelSftp.ls(baseDirectory);
			Iterator<ChannelSftp.LsEntry> directoryIterator = list.iterator();

			while (directoryIterator.hasNext()) {
				ChannelSftp.LsEntry obj = directoryIterator.next();
				if (rUtility.compareRegex(fileName, obj.getFilename())) {
					fileFound = true;
					break;
				}
			}

		} catch (SftpException e) {
			logger.error("while parsing files in sftp", e);
		}

		// Close the connections
		try {
			this.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("while closing sftp session channel", e);
		}

		logger.info("SftpAccess checkFileExistence called");
		return fileFound;
	}

	public void closeConnection() throws Exception {
		sftpHelper.close();
		logger.info("SftpAccess close called");
	}

	@Override
	public void close() throws IOException {
		if (channelSftp.isConnected())
			channelSftp.disconnect();
		if (channel.isConnected())
			channel.disconnect();
		if (session.isConnected())
			session.disconnect();
		logger.info("SftpAccess close called");
	}

	public InputStream getFile(String sftpFilePath, String sftpFileName) throws Exception {
		logger.info("Downloading File from SFTP");
		sftpHelper = new SFTPHelper(initValues.get(SftpResourceEndPoint.SFTP_HOST),
				initValues.get(SftpResourceEndPoint.SFTP_PORT), initValues.get(SftpResourceEndPoint.SFTP_USER),
				initValues.get(SftpResourceEndPoint.SFTP_PASS));
		InputStream iStream = null;
		try {
			iStream = sftpHelper.getFromSFTP(sftpFilePath, sftpFileName);
		} catch (Exception e) {
			logger.error(e.getMessage());
			if (e.getMessage().contains("No such file"))
				throw new Exception(sftpFileName + " File not present in SFTP at following path " + sftpFilePath);
			else
				throw e;
		}
		return iStream;
	}

	@Override
	public List<String> getFileList(String sftpFilePath, String sftpFileName) throws Exception {
		logger.info("Downloading File list from SFTP");
		sftpHelper = new SFTPHelper(initValues.get(SftpResourceEndPoint.SFTP_HOST),
				initValues.get(SftpResourceEndPoint.SFTP_PORT), initValues.get(SftpResourceEndPoint.SFTP_USER),
				initValues.get(SftpResourceEndPoint.SFTP_PASS));
		List<String> fileList = null;
		try {
			fileList = sftpHelper.getFileListMatchingRegexFromSFTP(sftpFilePath, sftpFileName);
		} catch (Exception e) {
			logger.error(e.getMessage());
			if (e.getMessage().contains("No such file"))
				throw new Exception(sftpFileName + " File not present in SFTP at following path " + sftpFilePath);
			else
				throw e;
		}
		return fileList;
	}

	public void writeFile(InputStream inputStream, String sftpFilePath, String sftpFileName) throws Exception {
		logger.info("Uploading to SFTP");
		sftpHelper = new SFTPHelper(initValues.get(SftpResourceEndPoint.SFTP_HOST),
				initValues.get(SftpResourceEndPoint.SFTP_PORT), initValues.get(SftpResourceEndPoint.SFTP_USER),
				initValues.get(SftpResourceEndPoint.SFTP_PASS));
		sftpHelper.putInSFTP(inputStream, sftpFilePath + sftpFileName);

		return;
	}

	@Override
	public String getFileHeaderLine(String location, String fileName) throws Exception {
		String fileHeader = "";
		InputStream fileStream = getFile(location, fileName);
		BufferedReader reader = new BufferedReader(new InputStreamReader(fileStream));
		fileHeader = reader.readLine();
		reader.close();
		fileStream.close();

		try {
			closeConnection();
		} catch (Exception e) {
			logger.info("Error closing SFTP Connection");
			e.printStackTrace();
		}

		return fileHeader;
	}

}
