package com.rboomerang.processor.impl.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazon.sqs.javamessaging.AmazonSQSMessagingClientWrapper;
import com.amazon.sqs.javamessaging.SQSConnection;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.internal.StaticCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.util.json.JSONException;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.AWSAccess;
import com.rboomerang.utils.CommonConstants;

public class AmazonSqsProcessor extends AbstractService {

	// ClientSpec clientSpec=null;
	// JSONObject serviceSpec=null;
	private static Logger logger = LoggerFactory
			.getLogger(AmazonSqsProcessor.class);
	Status status = new Status(Status.STATUS_SUCCESS, "Running Success");

	public AmazonSqsProcessor(String clientSpecpath, String serviceSpecPath,
			String runtimeDate) throws Exception {
		super(clientSpecpath, serviceSpecPath, runtimeDate);
		logger.info("AmazonSqsProcessor constructor called");
	}

	public AmazonSqsProcessor(String clientSpecpath, String serviceSpecPath,
			RuntimeContext runtimeContext, String runtimeDate) throws Exception {
		super(clientSpecpath, serviceSpecPath, runtimeContext, runtimeDate);
		logger.info("AmazonSqsProcessor constructor called");
	}

	public AmazonSqsProcessor(String clientSpecpath, String serviceSpecPath)
			throws Exception {
		super(clientSpecpath, serviceSpecPath);
		logger.info("AmazonSqsProcessor constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject serviceSpec)
			throws JSchException, SftpException, Exception {
		try {
			/*
			 * amazonSqsEventBased sqs= new amazonSqsEventBased();
			 * if(serviceSpec != null){ sqs.initiate(serviceSpec,status);
			 */
			// this.clientSpec=clientSpec;
			// this.serviceSpec=serviceSpec;
			initiate(clientSpec, serviceSpec);

		} catch (Exception e) {
			status.setStatusCode(Status.STATUS_ERROR);
			status.setMessage("Exception : " + e.getMessage());
			logger.error("Exception : " + e.getMessage());
		}
		return status;
	}

	public void initiate(ClientSpec clientSpec, JSONObject serviceSpec)
			throws Exception {

		final Date starttime = new Date();
		Date timenow;

		AWSAccess awsAccess = new AWSAccess();
		AWSCredentialsProvider provider = new StaticCredentialsProvider(
				awsAccess.getAWSUserSpecificAccess());
		SQSConnectionFactory connectionFactory = SQSConnectionFactory.builder()
				.withRegion(Region.getRegion(Regions.US_WEST_2))
				.withAWSCredentialsProvider(provider).build();

		// Create the connection.
		SQSConnection connection = connectionFactory.createConnection();
		// AWSCredentials basicAWSCredentials = new
		// BasicAWSCredentials(accessKey, secretKey);
		// AmazonSQSClient awsclient= new AmazonSQSClient(basicAWSCredentials);
		// AmazonSQSMessagingClientWrapper client=new
		// AmazonSQSMessagingClientWrapper(awsclient);
		// SQSConnection connection= new SQSConnection(client, 0);

		AmazonSQSMessagingClientWrapper client = connection
				.getWrappedAmazonSQSClient();
		logger.info("Connection created successfully with Amazon SQS ");

		String queueName = (String) serviceSpec.get("queueName");
		// Create an SQS queue named – if it does not already exist.
		if (!client.queueExists(queueName)) {
			client.createQueue(queueName);
			logger.info("Queue is not present, Creating Queue named: "
					+ queueName);
		}
		Session session = connection.createSession(false,
				Session.CLIENT_ACKNOWLEDGE);
		MessageConsumer consumer = session.createConsumer(session
				.createQueue(queueName));
		Thread t1 = Thread.currentThread();
		ReceiverCallback callback = new ReceiverCallback(t1, serviceSpec,
				clientSpec, status);
		consumer.setMessageListener((MessageListener) callback);
		String timeoutString = (String) serviceSpec.get("timeoutInMinute");
		long timeout = Long.parseLong(timeoutString);
		logger.info("Waiting for PO to be completed and time out is set to : "
				+ timeout + " min");
		timeout = timeout * 1000 * 60;
		// No messages will be processed until this is called

		connection.start();
		synchronized (t1) {
			t1.wait(timeout);
			timenow = new Date();
			long timediff = timenow.getTime() - starttime.getTime();
			if (timediff >= timeout) {
				connection.close();
				logger.error("Timeout Exception : waiting for PO to be completed since : "
						+ (timediff / (1000.00 * 60.00)) + "min");
				throw new Exception(
						"Timeout Exception : waiting for PO to be completed since"
								+ (timediff / (1000.00 * 60.00)) + "min");
			}
		}
		connection.close();
		logger.info("AWS SQS Connection closed");
	}

	private class ReceiverCallback implements MessageListener {
		// Used to listen for message silence

		Thread t1;
		org.json.simple.JSONObject serviceSpec;
		Status status;
		ClientSpec clientSpec;

		public ReceiverCallback(Thread t1,
				org.json.simple.JSONObject serviceSpec, ClientSpec clientSpec,
				Status status) {
			this.t1 = t1;
			this.serviceSpec = serviceSpec;
			this.status = status;
			this.clientSpec = clientSpec;
		}

		public void onMessage(Message message) {
			try {
				com.amazonaws.util.json.JSONObject obj = handleMessage(message);

				boolean matchflag = match(obj);
				if (matchflag) {
					message.acknowledge();
					logger.info("Acknowledged message "
							+ message.getJMSMessageID());
					synchronized (t1) {
						t1.notify();
					}

				} else {
					message.acknowledge();
					logger.info("Acknowledged message "
							+ message.getJMSMessageID());
				}

			} catch (JMSException e) {
				logger.error("Error processing message: " + e.getMessage());
				//status.setStatusCode(Status.STATUS_ERROR);
				//status.setMessage("Exception : " + e.getMessage());

			} catch (JSONException e) {
				logger.error("Error processing Json" + e.getMessage());
				//status.setStatusCode(Status.STATUS_ERROR);
				//status.setMessage("Exception : " + e.getMessage());
			} catch (Exception e) {
				logger.error("Error processing Json" + e.getMessage());
			}
		}

		public String convertIntoString(Object valueobj){
			String value = null;
			if(valueobj instanceof String){
				 value=(String)valueobj;
			 }
			 if(valueobj instanceof Integer){
				 value=""+(Integer)valueobj;
			 }
			 if(valueobj instanceof Boolean){
				 value=""+(Boolean)valueobj;
			 }
			 if(valueobj instanceof Float){
				 value=""+(Float)valueobj;
			 }
			 if(valueobj instanceof Long){
				 value=""+(Long)valueobj;
			 }
			
			return value.trim();
		}
		public boolean match(com.amazonaws.util.json.JSONObject obj)
				throws Exception {
			String keyString = "";
			boolean matchflag = true;
			String feed_date = (String) serviceSpec.get("feed_date");
			if (feed_date == null || feed_date.equals(""))
				feed_date = clientSpec.getDateFormat() + CommonConstants.COMMA
						+ clientSpec.getDaysAgo();
			feed_date = replaceDateTag("{{feed_date}}", feed_date);
			List<org.json.simple.JSONObject> matchMassageTags = (List<org.json.simple.JSONObject>) serviceSpec
					.get("matchMassageTags");
			Set keysets;
			if (obj == null || matchMassageTags == null) {
				logger.info("matchMassageTags are null");
				return true;
			}
			if (obj != null) {
				if (obj.has("startDate")) {
					String startDate = obj.getString("startDate");
					DateFormat srcdf = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					DateFormat destdf = new SimpleDateFormat("yyyyMMdd");
					Date srcdate = srcdf.parse(startDate);
					String destdate = destdf.format(srcdate);
					if (!(destdate.trim().equals(feed_date.trim()))) {
						logger.warn("startDate (provide by PO) and current date is different, startDate : "
								+ destdate + " current date: " + feed_date);
						return false;
					}
				}
			}

			for (org.json.simple.JSONObject matchMassageTag : matchMassageTags) {

				keysets = matchMassageTag.keySet();
				for (Object key : keysets) {
					
					keyString = (String) key;
					String value = null;
					String matchValue=null;
					if (obj.has(keyString)) {
						Object valueobj = obj.get(keyString);
						value=convertIntoString(valueobj);
						
						matchValue= convertIntoString(matchMassageTag.get(keyString));
						if (!value.equals(matchValue)) {
							matchflag = matchflag & false;
						} else {
							matchflag = matchflag & true;
						}
					} else {
						matchflag = matchflag & false;
					}

				}

			}

			return matchflag;

		}
		

		public com.amazonaws.util.json.JSONObject handleMessage(Message message)
				throws JMSException, JSONException {
			logger.info("Got message " + message.getJMSMessageID());
			logger.info("Content: ");
			com.amazonaws.util.json.JSONObject obj = null;
			if (message instanceof TextMessage) {
				TextMessage txtMessage = (TextMessage) message;
				logger.info("\t" + txtMessage.getText());
				obj = new com.amazonaws.util.json.JSONObject(
						txtMessage.getText());

			} else if (message instanceof BytesMessage) {
				BytesMessage byteMessage = (BytesMessage) message;
				// Assume the length fits in an int - SQS only supports sizes up
				// to 256k so that
				// should be true
				byte[] bytes = new byte[(int) byteMessage.getBodyLength()];
				byteMessage.readBytes(bytes);
				obj = new com.amazonaws.util.json.JSONObject(
						byteMessage.readUTF());

				// System.out.println( "\t" + Base64.encodeAsString( bytes ) );
			} else if (message instanceof ObjectMessage) {
				ObjectMessage objMessage = (ObjectMessage) message;
				logger.info("\t" + objMessage.getObject());
				obj = (com.amazonaws.util.json.JSONObject) objMessage
						.getObject();
			}
			return obj;
		}

	}
}
