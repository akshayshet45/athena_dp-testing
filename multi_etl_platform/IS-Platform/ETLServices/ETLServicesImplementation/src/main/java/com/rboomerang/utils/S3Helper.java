package com.rboomerang.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.utils.endpoint.S3ResourceEndPoint;

public class S3Helper {

	private AmazonS3Client s3Client;
	private String regionName;
	private Region region;
	private HashMap<String, String> inputmap;
	private final Logger log = LoggerFactory.getLogger(S3Helper.class);

	public static void main(String[] args) {
		HashMap<String, String> inputmap = new HashMap<String, String>();
		inputmap.put(S3ResourceEndPoint.S3_BUCKET_NAME, "kartik-test");

		// S3Helper help = new S3Helper(inputmap);
	}

	public S3Helper(HashMap<String, String> input) throws IOException {
		this.inputmap = input;
		AWSAccess awsAccess = new AWSAccess();
		log.debug("s3 object through aws key :" + awsAccess.getAWSAccess().getAWSAccessKeyId());
		s3Client = new AmazonS3Client(awsAccess.getAWSAccess());
		log.info("S3 object initialized");
	}

	public void storeObject(String prefix, String filename, InputStream input) {
		String bucket = inputmap.get(S3ResourceEndPoint.S3_BUCKET_NAME);
		log.info("Create Bucket if not present " + bucket);
		createBucketOnAbsence(bucket);
		log.info("Create prefix in the S3 bucket if not present " + prefix);

		createPrefixOnAbsence(bucket + "/", prefix);

		String container = bucket;
		boolean keyPresent = isKeyAlreadyPresent(container, prefix + filename);
		if (keyPresent) {
			throw new AmazonClientException("Report File already exists");
		} else {
			log.info("Adding the report file " + input);
			long contentLength = 0;
			ByteArrayInputStream finalStream = null;
			try {
				// Make a copy of stream as it needs to be read twice
				ByteArrayOutputStream copyStream = new ByteArrayOutputStream();
				IOUtils.copy(input, copyStream);
				byte[] bytes = copyStream.toByteArray();
				contentLength = bytes.length;
				copyStream.close();
				finalStream = new ByteArrayInputStream(bytes);
			} catch (IOException e) {
				log.error("Failed to calculate contentLength!! Using default value 0" + e.getMessage());
				contentLength = 0;
			}
			log.debug("File size calculated:" + contentLength);
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentLength(contentLength);
			s3Client.putObject(container, prefix + filename, finalStream, metadata);
		}
	}

	private void createBucketOnAbsence(String bucket) throws AmazonClientException, AmazonServiceException {
		if (!s3Client.doesBucketExist(bucket)) {
			log.info("The specified bucket name is not already present. So creating the bucket");
			s3Client.createBucket(bucket, regionName);

			region = Region.getRegion(Regions.fromName(regionName));
			s3Client.setRegion(region);
		} else {
			log.info("The specified bucket is already present");
			regionName = s3Client.getBucketLocation(bucket);
			if (regionName.equals("US")) {
				regionName = "us-east-1";
			} else if (regionName.equals("EU")) {
				regionName = "eu-west-1";
			}
			region = Region.getRegion(Regions.fromName(regionName));
			s3Client.setRegion(region);
		}
	}

	private void createPrefixOnAbsence(String bucket, String prefix)
			throws AmazonClientException, AmazonServiceException {

		if (!isKeyAlreadyPresent(bucket, prefix)) {
			log.info("Adding the prefix " + prefix);
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentLength(0);
			// Create empty content
			InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
			PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, prefix, emptyContent, metadata);

			s3Client.putObject(putObjectRequest);
		} else {
			log.info("The prefix is already present " + prefix);
		}
	}

	public boolean isPresent(String key) {
		return isKeyAlreadyPresent(inputmap.get(S3ResourceEndPoint.S3_BUCKET_NAME), key);
	}

	@SuppressWarnings("unused")
	private boolean isKeyAlreadyPresent(String container, String key)
			throws AmazonClientException, AmazonServiceException {
		try {
			log.info("Checking if the key is already present " + key + " container - " + container);
			S3Object object = s3Client.getObject(container, key);
		} catch (AmazonS3Exception Ae) {
			log.info("The key is not present " + key);
			String errorCode = Ae.getErrorCode();
			log.info("The error code is " + errorCode);
			if (errorCode.equalsIgnoreCase("NoSuchKey")) {
				return false;
			} else
				return true;
		} catch (AmazonServiceException e) {
			log.info("The key is not present " + key);
			String errorCode = e.getErrorCode();
			log.info("The error code is " + errorCode);
			if (errorCode.equalsIgnoreCase("NoSuchKey")) {
				return false;
			} else
				return false;
		}

		return true;
	}

	public Long getKeySize(String key) throws Exception {
		String container = inputmap.get(S3ResourceEndPoint.S3_BUCKET_NAME);

		if (!isKeyAlreadyPresent(container, key))
			throw new Exception("key " + key + " not present on S3 bucket " + container);

		return s3Client.getObjectMetadata(container, key).getContentLength();

	}

	public boolean readS3ObjectUsingByteArray(String path, String key, String outputFileLocation) {
		boolean returnVal = false;
		byte[] content = new byte[2048];

		try {
			S3Object s3object = s3Client
					.getObject(new GetObjectRequest(inputmap.get(S3ResourceEndPoint.S3_BUCKET_NAME), path + key));
			InputStream stream = s3object.getObjectContent();
			try (BufferedOutputStream outputStream = new BufferedOutputStream(
					new FileOutputStream(outputFileLocation + key))) {
				int totalSize = 0;
				int bytesRead;
				while ((bytesRead = stream.read(content)) != -1) {
					log.info(String.format("%d bytes read from stream", bytesRead));
					outputStream.write(content, 0, bytesRead);
					totalSize += bytesRead;
				}
				log.info("Total Size of file in bytes = " + totalSize);
			}
			// close resource even during exception
			returnVal = true;
		} catch (IOException ex) {
			log.error("IOException at readS3ObjectUsingByteArray", ex);
		} catch (Exception e) {
			log.error("Exception at readS3ObjectUsingByteArray", e);
		}
		return returnVal;
	}

	public InputStream readInputStream(String fileLocation, String fileName) {
		String container = inputmap.get(S3ResourceEndPoint.S3_BUCKET_NAME);
		InputStream stream = null;
		log.info(container);
		S3Object object = s3Client.getObject(new GetObjectRequest(container, fileLocation + fileName));
		stream = new BufferedInputStream(object.getObjectContent());
		return stream;
	}

	public List<String> getFileListMatchingRegexFromSFTP(String fileLocation, String regexFileName)
			throws SftpException, JSchException, InterruptedException {
		List<String> matchingFileList = new ArrayList<>();
		String container = inputmap.get(S3ResourceEndPoint.S3_BUCKET_NAME);
		log.info(container);
		ObjectListing listing = s3Client.listObjects(container, fileLocation);
		List<S3ObjectSummary> summaries = listing.getObjectSummaries();
		while (listing.isTruncated()) {
			listing = s3Client.listNextBatchOfObjects(listing);
			summaries.addAll(listing.getObjectSummaries());
		}
		for (int i = 0; i < summaries.size(); i++) {
			String key = summaries.get(i).getKey();
			if (key.contains("/")) {
				String fileName = key.substring(key.lastIndexOf('/') + 1);
				if (StringUtils.isNotEmpty(fileName) && RegexUtility.compareRegex(regexFileName, fileName)) {
					matchingFileList.add(fileName);
					log.debug("actualFileName ::::: " + fileName);
				}
			}

		}

		return matchingFileList;
	}
}