package com.rboomerang.processor.impl.common;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rboomerang.utils.AWSAccess;
//import com.esotericsoftware.yamlbeans.YamlException;
//import com.esotericsoftware.yamlbeans.YamlReader;
import com.rboomerang.utils.AmazonSqsHelper;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.util.json.JSONObject;

public class AmazonSqsClientWaiter {
	private final String SQS_SUCCESS_NOTIFY = "-sqs-success-notify";
	private String expectedqueuename;
	private String status = "false";
	private String run_again_status = "false";
	private String client = "";
	private final Date starttime;

	Date timenow;
	private static Logger logger = LoggerFactory
			.getLogger(AmazonSqsClient.class);

	public AmazonSqsClientWaiter(String client) {
		this.client = client;
		starttime = new Date();
	};

	public void initialize(AmazonSqsClientWaiter queueMsgRdr,
			String clientName, String env, String clientNamedTagged,
			int timeoutInMinute) throws Exception {

		this.client = clientName;
		// AmazonSqsClientWaiter queueMsgRdr = new
		// AmazonSqsClientWaiter(clientName);
		queueMsgRdr.setExpectedqueuename(queueMsgRdr.generateQueueName(env,
				clientNamedTagged));
		AWSAccess awsAccess = new AWSAccess();
		AmazonSqsHelper sqsHelper = new AmazonSqsHelper(awsAccess.getAWSUserSpecificAccess(), "us-west-2", "", "");
		List<String> Queuelist = sqsHelper.getAllQueues();

		int queueindex = 0;
		String massage = "";
		String queuename = "";
		List<Message> massages;

		// wait till PO creates Queue for your client
		while (true) {
			queueMsgRdr.status = "false";
			queuename = queueMsgRdr.matchQueueName(queueMsgRdr, queueindex,
					massage, queuename, Queuelist, timeoutInMinute);

			String queueURL = queuename;
			// logger.info("Queue URL : " + queueURL);
			massages = sqsHelper.receiveMessages(queueURL, 10);

			// process each massage from queue to match our client name and date
			if (massages == null || massages.equals("") || massages.isEmpty()) {
				TimeUnit.MINUTES.sleep(1);
				queueMsgRdr.initialize(queueMsgRdr, clientName, env,
						clientNamedTagged, timeoutInMinute);
			}

			for (Message msg : massages) {
				massage = msg.getBody();
				// System.out.println(massage.toString());
				JSONObject msgobj = new JSONObject(massage);
				String client_name = (String) msgobj.get("clientName");
				logger.info("Client Name in Queue Msg:" + client_name);
				// String startDate = (String) obj.get("startDate");
				if (client_name.equals(client)) {

					logger.info("PO Massage processed for client: " + client);
					String messageRecieptHandle = msg.getReceiptHandle();
					sqsHelper.deleteMessage(messageRecieptHandle, queueURL);
					queueMsgRdr.run_again_status = "false";
					break;
				} else {

					queueMsgRdr.run_again_status = "true";
					queueMsgRdr.status = "false";

				}

			}
			if (queueMsgRdr.run_again_status.equals("false")) {
				break;
			}

		}
	}

	public String getExpectedqueuename() {
		return expectedqueuename;
	}

	private void setExpectedqueuename(String expectedqueuename) {
		this.expectedqueuename = expectedqueuename;
	}

	private String matchQueueName(AmazonSqsClientWaiter queueMsgRdr,
			int queueindex, String massage, String queuename,
			List<String> Queuelist, int timeoutInMinute) throws Exception {
		long timediff;
		while (queueMsgRdr.status.equals("false")) {
			queueindex = 0;
			massage = "";
			queuename = "";
			// check your queue among list of all Queues
			while (queueindex < Queuelist.size()) {
				queuename = Queuelist.get(queueindex);
				// System.out.println("queuename" + queuename);
				if (queuename
						.equals("https://sqs.us-west-2.amazonaws.com/208876916689/"
								+ queueMsgRdr.expectedqueuename)) {
					queueMsgRdr.status = "true";
					break;

				} else {
					queueMsgRdr.status = "false";
				}
				queueindex++;
			}

			timenow = new Date();
			timediff = timenow.getTime() - starttime.getTime();
			logger.info("waiting for PO to be completed since : "
					+ (timediff / (60 * 1000)) + " minute");
			if (timediff / (60 * 1000) >= timeoutInMinute) {
				throw new Exception("Time out due to long wait for PO : "
						+ timediff + " min");
			}
			// sleep for 1 min
			if (queueMsgRdr.status == "false") {
				TimeUnit.MINUTES.sleep(1);
				logger.info("waiting for PO to be completed and expecting queue Named to be created : " + queueMsgRdr.expectedqueuename);
			}
		}
		return queuename;
	}

	private String generateQueueName(String env, String clientNamedTagged) {
		String queueName = "";
		// String
		// path="/Users/kishanpatel/athena_poconfig/price_optimizer/vars/configs/client_env/kishan/this.client";
		// YamlReader reader = new YamlReader(new FileReader("contact.yml"));
		// Object object = null;
		// try {
		// object = reader.read();
		// } catch (YamlException e) {
		// e.printStackTrace();
		// }
		// Map map = (Map)object;
		// String env=(String) map.get("env");
		// String clientNamedTagged=(String) map.get("clientNamedTagged");
		queueName = clientNamedTagged + "-" + env + SQS_SUCCESS_NOTIFY;
		// System.out.println("Expected Queue Name :" + queueName);
		return queueName;
	}

}
