package com.rboomerang.processor.impl.common;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.ExecuteCommand;

public class ScriptExecutor extends AbstractService {

	private static Logger logger = LoggerFactory.getLogger(ScriptExecutor.class);
	private Status success = new Status(Status.STATUS_SUCCESS, "Executed Script Sucessfully");
	private Status failure = new Status(Status.STATUS_ERROR, "Execution of Script Failed");

	public ScriptExecutor(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext, String runtimeDate)
			throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.info("Script Executor constructor called");
	}

	public ScriptExecutor(String clientSpecpath, String validatorSpecPath, String runtimeDate) throws Exception {
		super(clientSpecpath, validatorSpecPath, runtimeDate);
		logger.info("Script Executor constructor called");
	}

	public ScriptExecutor(String clientSpecpath, String validatorSpecPath) throws Exception {
		super(clientSpecpath, validatorSpecPath);
		logger.info("Script Executor constructor called");
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Status service(ClientSpec clientSpec, JSONObject processorSpec) throws Exception {
		Status returnval = failure;

		String shFilePath = this.getRuntimeContext().transformPath((String) processorSpec.get(CommonConstants.FILE_LOCATION));
		logger.info("The script path: " + shFilePath);
		JSONArray listArray = (JSONArray) processorSpec.get("arguments");

		List commands = new ArrayList();
		commands.add("bash");
		commands.add(shFilePath);
		
		for (int i = 0; i < listArray.size(); i++) {
			JSONObject innerObj = (JSONObject) listArray.get(i);
			String argumentNow = (String) innerObj.get("argument");
			commands.add(argumentNow);
		}

		String output = ExecuteCommand.executeAttributeCommands(commands);

		if (output.contains("ERROR")) {
			returnval.setMessage(output);
		}
		else {
			returnval = success;
			returnval.setMessage(output);
		}
		
		//logger.info(returnval.getMessage());

		return returnval;
	}
}