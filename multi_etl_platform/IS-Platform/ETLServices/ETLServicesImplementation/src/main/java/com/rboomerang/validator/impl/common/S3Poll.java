package com.rboomerang.validator.impl.common;

import java.util.HashMap;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.S3Helper;

public class S3Poll extends AbstractService {

	public S3Poll(String clientSpecpath, String processorSpecPath) throws Exception {
		super(clientSpecpath, processorSpecPath);
		logger.debug("S3Poll constructor called");
	}

	public S3Poll(String clientSpecpath, String processorSpecPath, String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeDate);
		logger.debug("S3Poll constructor called");
	}

	public S3Poll(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext, String runtimeDate)
			throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.debug("S3Poll constructor called");
	}

	private static Logger logger = LoggerFactory.getLogger(S3Poll.class);

	@Override
	public Status service(ClientSpec clientSpec, JSONObject serviceSpec)
			throws JSchException, SftpException, Exception {

		JSONObject source = (JSONObject) serviceSpec.get(CommonConstants.SOURCE);

		String sourceDateSuffix = (String) source.get(CommonConstants.FEED_DATE);

		if (sourceDateSuffix == null || sourceDateSuffix.isEmpty())
			sourceDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();

		// getting source Endpoint details
		String sourceEndPoint = (String) source.get(CommonConstants.ENDPOINT);
		// getting source file path from spec file and replacing date month or
		// year from it!
		String sourcePath = (String) source.get(CommonConstants.LOCATION);
		// getting source file name from spec file and replacing feedDate from
		// it!
		String sourceFileName = (String) source.get(CommonConstants.FILENAME);

		sourcePath = replaceDynamicFolders(sourcePath, sourceDateSuffix);
		sourceFileName = replaceDateTag(sourceFileName, sourceDateSuffix);
		logger.info("Input File Source Endpoint :: " + sourceEndPoint);
		logger.info("Input File Source Path --> " + sourcePath + " :: FileName --> " + sourceFileName);

		String sourceType = super.getTypeOfEndPoint(clientSpec, sourceEndPoint);
		if (!sourceType.equals("s3"))
			return new Status(Status.STATUS_ERROR,
					"Only S3 endPoint is supported for S3poll service. You have configured endpoint type of "
							+ sourceType);

		String totalPollTimeInMinutes = (String) source.get("totalPollTimeInMinutes");
		String fileExistenceCheckRetryTimeInMinutes = (String) source.get("fileExistenceCheckRetryTimeInMinutes");
		String fileSizeCheckRetryTimeInMinutes = (String) source.get("fileSizeCheckRetryTimeInMinutes");
		String sizeCheck = (String) source.get("sizeCheck");
		logger.debug("totalPollTimeInMinutes : " + totalPollTimeInMinutes);
		logger.debug("fileExistenceCheckRetryTimeInMinutes : " + fileExistenceCheckRetryTimeInMinutes);
		logger.debug("fileSizeCheckRetryTimeInMinutes : " + fileSizeCheckRetryTimeInMinutes);

		Long startTime = System.currentTimeMillis();

		HashMap<String, String> sourceEndPointResourceProperties = super.buildResourceProperties(sourceEndPoint,
				clientSpec, sourceType);
		S3Helper s3Helper = new S3Helper(sourceEndPointResourceProperties);
		boolean filePresent = false;
		
		//file present logic placed here
		while (System.currentTimeMillis() < startTime + Long.parseLong(totalPollTimeInMinutes) * 60000) {
			if (s3Helper.isPresent(sourcePath + sourceFileName)) {
				filePresent = true;
				break;
			} else {
				logger.debug("waiting for "+fileExistenceCheckRetryTimeInMinutes+" minute/s to check if file is present!");
				Thread.sleep(Long.parseLong(fileExistenceCheckRetryTimeInMinutes)*60000);
				logger.info("File Not found");
			}
		}

		if (filePresent) {
			
			logger.info(sourceFileName + " arrived at " + sourcePath);
			Long fileSize = s3Helper.getKeySize(sourcePath+sourceFileName);
			logger.debug("File size : "+ fileSize+" bytes");
			//size logic placed here
			boolean fileComplete = false;
			while (System.currentTimeMillis() < startTime + Long.parseLong(totalPollTimeInMinutes) * 60000) {
				logger.debug("waiting for "+fileSizeCheckRetryTimeInMinutes+" minute/s to check if file is ready!");
				Thread.sleep(Long.parseLong(fileSizeCheckRetryTimeInMinutes)*60000);
				Long presentFileSize = s3Helper.getKeySize(sourcePath+sourceFileName);
				logger.debug(" present file size : "+ presentFileSize +" bytes");
				if (presentFileSize==fileSize || sizeCheck.equals("off")) {
					fileComplete = true;
					break;
				} else {
					fileSize = presentFileSize;
					logger.info("File Size still updating");
				}
			}
			if (fileComplete) {
				return new Status(Status.STATUS_SUCCESS, "File " + sourceFileName+ " is ready in s3");
			}
			else
			{
				return new Status(Status.STATUS_ERROR, sourceFileName + " size is still changing at " + sourcePath+" , waited for "+totalPollTimeInMinutes+" minutes");
			}
		} else {
			return new Status(Status.STATUS_ERROR, sourceFileName + " did not arrive at " + sourcePath+" , waited for "+totalPollTimeInMinutes+" minutes");
		}

		
	}
}
