package com.rboomerang.processor.impl.common;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.common.framework.javabeans.DbSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

public class SqlTableDumpToCsv extends AbstractService {
	private static Logger logger = LoggerFactory.getLogger(SqlTableDumpToCsv.class);

	private static final String SQL_FILE_PATH = "sqlFilePath";
	private static final String ORIGINAL_HEADER="originalHeader";
	private static final String NEW_HEADER = "newHeader";
	private static final String NEWLINE = "\n";

	public SqlTableDumpToCsv(String clientSpecpath, String processorSpecPath) throws Exception {
		super(clientSpecpath, processorSpecPath);
		logger.info("SqlTableDumpToCsv constructor called");
	}
	public SqlTableDumpToCsv(String clientSpecpath, String processorSpecPath,String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeDate);
		logger.info("SqlTableDumpToCsv constructor called");
	}
	public SqlTableDumpToCsv(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext,String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath,runtimeContext, runtimeDate);
		logger.info("SqlTableDumpToCsv constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject processorSpec) throws Exception {

		JSONObject list = (JSONObject)processorSpec.get(CommonConstants.LIST);
		JSONObject source = (JSONObject)list.get(CommonConstants.SOURCE);
		JSONObject dest = (JSONObject)list.get(CommonConstants.DESTINATION);

		String sourceEndPoint = (String)source.get(CommonConstants.ENDPOINTNAME);
		String destEndPoint = (String)dest.get(CommonConstants.ENDPOINTNAME);
		String sqlFilePath = this.getRuntimeContext().transformPath((String)source.get(SQL_FILE_PATH));
		String dateSuffix = (String) dest.get(CommonConstants.FEED_DATE);
		
		if (dateSuffix == null || dateSuffix.isEmpty() )
			dateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
		
		String destPath = (String)dest.get(CommonConstants.PATH);
		destPath = replaceDynamicFolders(destPath,dateSuffix);
		String destFileName = (String)dest.get(CommonConstants.FILE_NAME);
		destFileName = replaceDateTag(destFileName, dateSuffix);
		String delimiter = (String)list.get(CommonConstants.DELIMITER);

		logger.info("Initiating Process....");
		HashMap<String, String> headerValues = new HashMap<String,String>();//for storing the column header values to be changed in file

		JSONArray headerList = (JSONArray) list.get("changeHeaders");
		/*
		 * adding all original and new header values to hashmap to be updated in file
		 */
		for (int j = 0; j < headerList.size(); j++) {
			JSONObject headerMapping = (JSONObject) headerList.get(j);
			logger.info("original Header::"+(String) headerMapping.get(ORIGINAL_HEADER)+"\n"+"new Header::"+(String) headerMapping.get(NEW_HEADER));
			headerValues.put((String) headerMapping.get(ORIGINAL_HEADER),(String) headerMapping.get(NEW_HEADER));
		}
		/*
		 * loading list values from Processor_spec.Json
		 */
		String query;
		try{
			query = readQuery(sqlFilePath);
		}catch(Exception e)
		{
			logger.error("Error occured in reading query : "+e.getMessage());
			return new Status(Status.STATUS_ERROR, "Exception occured in reading query : "+e.getMessage());
		}

		//getting db credentials
		Object[] dbtype = getDbDetails(sourceEndPoint);
		DbSpec details = (DbSpec) dbtype[1];
		String url=details.getHost();
		String port="3306";
		String database= details.getDb();
		String userName=details.getUname();
		String password=details.getPasswd();

		String coulumn=null;
		Statement stmt = null;
		//creating temporary file to write data from endpoint
		File sourceFile = File.createTempFile("sourceFile",".tmp");

		FileWriter writer = new FileWriter(sourceFile);
		Class.forName("org.postgresql.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://"+url+":"+port+"/"+database,userName,password);
		conn.setAutoCommit(false);
		logger.info("Opened database successfully");
		stmt = conn.createStatement();
		ResultSet rs;
		try{
			rs= stmt.executeQuery(query);	
		}
		catch(Exception e)
		{
			logger.error(e.getMessage());
			writer.close();
			return new Status(Status.STATUS_ERROR, "Error in executing query \'" +query+"\' in endpoint "+sourceEndPoint);
		}
		ResultSetMetaData metaData = rs.getMetaData();
		int columnCount = metaData.getColumnCount();
		for(int i=1;i<=columnCount;i++)
		{
			coulumn=metaData.getColumnName(i);
			if(headerValues.containsKey(coulumn)){
				coulumn=headerValues.get(coulumn);
				logger.info("Column header : "+coulumn);
			}
			writer.append(coulumn);
			if(i!=columnCount)
			{
				writer.append(delimiter);
			}
		}
		writer.append(NEWLINE);
		while ( rs.next() ) {
			for(int i=1;i<=columnCount;i++){
				coulumn=rs.getString(i);
				writer.append(coulumn);
				if(i!=columnCount)
				{
					writer.append(delimiter);
				}
			}
			writer.append(NEWLINE);
		}

		rs.close();
		stmt.close();
		conn.close();
		writer.flush();
		writer.close();


		//getting destination end point properties
		String destType = getTypeOfEndPoint(clientSpec, destEndPoint);
		HashMap<String, String> destEndPointResourceProperties = super.buildResourceProperties(destEndPoint, clientSpec,destType);

		//making connection with source endpoint
		IEndPointResource destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(
				destType, destEndPointResourceProperties);

		//uploading file
		destEndPointResource.writeFile(new FileInputStream(new File(sourceFile.getAbsolutePath())), destPath, destFileName);
		destEndPointResource.closeConnection();
		sourceFile.delete();
		return new Status(Status.STATUS_SUCCESS, "Split file successfull ! "+destFileName+" file uploaded to "+destEndPoint+" at path "+destPath);

	}

	private String readQuery(String sqlFilePath) throws Exception {
		BufferedReader bufferedReader = new BufferedReader(new FileReader(sqlFilePath));
		String query= bufferedReader.readLine();  
		bufferedReader.close();
		return query;
	}


}
