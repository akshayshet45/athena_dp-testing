package com.rboomerang.processor.impl.common;
/**
 * Description: MailSender Processor
 */


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

public class MailSender extends AbstractService 
{
	private static Logger logger = LoggerFactory.getLogger(MailSender.class);
	public MailSender(String clientSpecpath, String processorSpecPath) throws Exception
	{
		super(clientSpecpath, processorSpecPath);
		logger.info("MailSender constructor called");
	}
	public MailSender(String clientSpecpath, String processorSpecPath, String runtimeDate) throws Exception
	{
		super(clientSpecpath, processorSpecPath,runtimeDate);
		logger.info("MailSender constructor called");
	}
	public MailSender(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext, String runtimeDate) throws Exception 
	{
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.info("MailSender constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject processorSpec) throws Exception
	{
		logger.info("MailSender Processor execution started");
		Status status=null;
		IEndPointResource endPointResource=null;
		try
		{
			JSONArray endpointDetailsArray=(JSONArray) processorSpec.get(CommonConstants.SOURCE);
			int fileCount=endpointDetailsArray.size();
			String[] tempFiles=new String[fileCount];
			String[] fileNames=new String[fileCount];
			String recipients=(String)processorSpec.get(CommonConstants.RECIPIENT_LIST);
			String subject=(String)processorSpec.get("subject");
			String body=(String)processorSpec.get("body");
			String bodyDateFormat=(String)processorSpec.get("bodyDateFormat");
			if(subject.endsWith("{{feed_date}}"))
			{
				subject= subject.split("[{{]")[0]+(String)(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			}
			logger.debug(bodyDateFormat);
			body=replaceDateTag(body,bodyDateFormat);
			for(int index=0;index<fileCount;index++)
			{
				JSONObject endpointDetails=(JSONObject)endpointDetailsArray.get(index);
				String feedFileName = (String)endpointDetails.get(CommonConstants.FILENAME);
				
				String destDateSuffix = (String)endpointDetails.get(CommonConstants.FEED_DATE);
				String location = (String)endpointDetails.get(CommonConstants.LOCATION);
				
				if (destDateSuffix == null || destDateSuffix.isEmpty() )
				{
					destDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
				}
				feedFileName = replaceDateTag(feedFileName, destDateSuffix);
				fileNames[index]=feedFileName;
				logger.info("Attachment file name is "+feedFileName);
				String endpointName = (String)endpointDetails.get(CommonConstants.ENDPOINTNAME);
				String endpointType = getTypeOfEndPoint(clientSpec,endpointName);
				HashMap<String, String> endPointResourceProperties = buildResourceProperties(endpointName, clientSpec,endpointType);
				endPointResource = EndPointResourceFactory.getInstance().getEndPointResource(endpointType,endPointResourceProperties);
				logger.debug("Downloading file from "+endPointResourceProperties);
				InputStream in =endPointResource.getFile(location, feedFileName);
				File file=File.createTempFile("temp",".csv");
				PrintWriter pw=new PrintWriter(new BufferedWriter(new FileWriter(file)));
				BufferedReader br=new BufferedReader(new InputStreamReader(in));
				String line=null;
				while((line=br.readLine()) !=null)
				{
					pw.println(line);
				}
				tempFiles[index]=file.getAbsolutePath();
				br.close();
				pw.close();
				endPointResource.closeConnection();
				logger.info("Done!! File downloaded is :"+feedFileName);
			}
			logger.info("Sending Mail...to "+recipients);
			sendMail(recipients,subject,body,tempFiles,fileNames);
			status = new Status(Status.STATUS_SUCCESS, "Mail Sender executed successfully!!");
				
		}
		catch(Exception e)
		{
			status=new Status(Status.STATUS_ERROR,"Error in sending mail!!");
			if(endPointResource!=null)
			{
				endPointResource.closeConnection();
			}
			e.printStackTrace();
		}
		return status;
	}
	private void sendMail(String to,String subject,String body,String tempFiles[],String outputFileName[]) throws AddressException, MessagingException
	{
		final String from = "ops@boomerangcommerce.com";

		String host = "smtp.gmail.com";

		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		properties.setProperty("mail.smtp.host", host);
		properties.setProperty("mail.user", from);
		properties.setProperty("mail.password", "Naganat123");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "587");

		// Get the default Session object.
		Session session = Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, "Naganat123");
			}
		});
		// Create a default MimeMessage object.
		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress(from));
		// Set To: header field of the header.
		for (String retval : to.split(","))
		{
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(retval));
		}
		// Set Subject: header field
		message.setSubject(subject);
		// Now set the actual message
		BodyPart messageText=new MimeBodyPart();
		messageText.setText(body);
		//Adding Attachment
		
		Multipart multipart=new MimeMultipart();
		int iter;
		int count=tempFiles.length;
		for(iter=0;iter<count;iter++)
		{	
			logger.info("Adding attachment.."+outputFileName[iter]);
			File file=new File(tempFiles[iter]);
			BodyPart messageBodyPart=new MimeBodyPart();
			DataSource source = new FileDataSource(file.getAbsolutePath());
	        messageBodyPart.setDataHandler(new DataHandler(source));
	        messageBodyPart.setFileName(outputFileName[iter]);
	        multipart.addBodyPart(messageBodyPart);   
	        logger.info("Successfully attached!!");
	       
		}
		multipart.addBodyPart(messageText);
		message.setContent(multipart);
		// Send message
		Transport.send(message);
		logger.info("Sent message successfully....");
		for(iter=0;iter<count;iter++)
		{
			File temp=new File(tempFiles[iter]);
			 if(temp.delete())
		        {
				 logger.debug("Deleted the temporary File ");
		        }
		        else
		        {
		        	logger.warn("unable to delete the temporary file");
		        }
		}
	}
}
