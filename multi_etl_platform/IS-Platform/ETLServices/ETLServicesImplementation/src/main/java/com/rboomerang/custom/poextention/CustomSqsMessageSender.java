package com.rboomerang.custom.poextention;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.common.service.utils.amazonUtils.AmazonSqsHelper;
import com.rboomerang.utils.CommonConstants;

public class CustomSqsMessageSender extends AbstractService {
	
	private final Logger logger = LoggerFactory
			.getLogger(CustomSqsMessageSender.class);

	
	public CustomSqsMessageSender(String clientSpecpath, String serviceSpecPath)
			throws Exception {
		super(clientSpecpath, serviceSpecPath);
		logger.info("TriggerDD constructor called");
	}

	public CustomSqsMessageSender(String clientSpecpath,
			String serviceSpecPath, String runtimeDate) throws Exception {
		super(clientSpecpath, serviceSpecPath, runtimeDate);
		logger.info("TriggerDD constructor called");
	}

	public CustomSqsMessageSender(String clientSpecpath,
			String serviceSpecPath, RuntimeContext runtimeContext,
			String runtimeDate) throws Exception {
		super(clientSpecpath, serviceSpecPath, runtimeContext, runtimeDate);
		logger.info("TriggerDD constructor called");
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public Status service(ClientSpec clientSpec, JSONObject serviceSpec)
			throws JSchException, SftpException, Exception {
		
		JSONObject sqsmessage= (JSONObject)serviceSpec.get("sqsmessage");
		//JSONObject sqsbody = (JSONObject)sqsmessage.get("body");
		
		String testMessage = sqsmessage.toJSONString();
		//String testBody = sqsbody.toJSONString();
		logger.debug("sqsmessage.toJSONString() ====== "+testMessage);
		//logger.debug("sqsbody.toJSONString() ====== "+testBody);
		
		
		JSONArray messagebody=(JSONArray)sqsmessage.get("body");
		//logger.debug(messagebody.get(0).toString());
		String result="";
		for(int k=0;k<messagebody.size();k++)
			result += messagebody.get(k).toString();
		logger.debug(result);
		Status status = new Status(Status.STATUS_SUCCESS, "SQS notifier trigger successful");
		try{
			Properties prop = new Properties();
			try {
				prop.load(getClass().getClassLoader().getResourceAsStream("awsConfiguraion.properties"));
			} catch (IOException e) {
				logger.error("Error wile reading awsConfiguraion.properties", e);
				e.printStackTrace();
			}
			String accessKey = (String) prop.get("aws-s3-identity");
			String secretKey = (String) prop.get("aws-s3-credential");
			Properties properties = new Properties();
			String filePath=System.getProperty(CommonConstants.AZKABAN_JOB_PROPERTIES_FILE_PATH);
			logger.debug(filePath);
			properties.load(new FileInputStream(filePath));
			logger.debug(properties.getProperty("SQSRegion"));
			logger.debug(properties.getProperty("responseSQS"));
			AmazonSqsHelper amazonSqsHelper = new AmazonSqsHelper(accessKey, secretKey,(String) properties.get("SQSRegion"), (String)properties.get("responseSQS"));
			
			result=result.replace("||",",").replace("#", "\"");
			logger.debug("Replaced final message"+result);
			amazonSqsHelper.sendMessage(result, amazonSqsHelper.getQueueUrl(), (String)properties.get("responseSQS"));
		}catch(Exception err){
			err.printStackTrace();
			logger.error("Error while posting message",err);
			status = new Status(Status.STATUS_ERROR, "SQS notifier trigger successful");
		}
		return status;
	}

}
