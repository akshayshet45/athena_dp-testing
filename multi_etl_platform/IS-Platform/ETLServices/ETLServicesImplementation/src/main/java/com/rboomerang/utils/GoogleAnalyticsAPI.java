package com.rboomerang.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.SocketTimeoutException;
import java.util.List;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.analytics.Analytics;
import com.google.api.services.analytics.AnalyticsScopes;
import com.google.api.services.analytics.model.GaData;
import com.google.api.services.analytics.model.GaData.ColumnHeaders;

public class GoogleAnalyticsAPI

{
	private String applicationName = null;
	private String keyFileLocation = null;
	private String serviceAccountMail = null;
	private JSONObject queryObject=null;
	private int defaultMaxResults=999999;
	private String outputFileName=null;
	private static Logger logger = LoggerFactory.getLogger(GoogleAnalyticsAPI.class);
	private static final JsonFactory jsonFactory = GsonFactory.getDefaultInstance();
	//Constructor that initiates query with required parameters
	public GoogleAnalyticsAPI(String client, String keyFilePath, String accountMail, JSONObject params)
	{
		this.applicationName=client;
		this.keyFileLocation = keyFilePath;
		this.serviceAccountMail=accountMail;
		this.queryObject=params;
	}
	public String start() throws Exception
	{
		//get the queryParameters
		String profileId=null;
		String startTime=null;
		String endTime=null;
		String metrics=null;
		String dimensions=null;
		Integer maxResults=0;
		profileId = (String)queryObject.get("profileID");
		startTime = (String)queryObject.get("startTime");
		endTime = (String)queryObject.get("endTime");
		metrics = (String)queryObject.get("metrics");
		dimensions = (String)queryObject.get("dimensions");
		maxResults = ((Long)queryObject.get("maxResults")).intValue();
		logger.info("Working Directory = "+ System.getProperty("user.dir"));
		Analytics analytics = initializeAnalytics();
		logger.info("Profile Id: " + profileId);
		GaData results=getResults(analytics, profileId,startTime,endTime,metrics,dimensions,maxResults);
		if(results==null)
		{
			throw new Exception("Wrong input metrics or No Data available");
		}	
		return printGaData(results);
	}

 // Initializes an authorized Analytics Service Object
 
	private Analytics initializeAnalytics() throws Exception
	{
		// Construct a GoogleCredential object with the service account email and p12 file downloqded from the developer console.
		HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
		GoogleCredential credential = new GoogleCredential.Builder().setTransport(httpTransport).setJsonFactory(jsonFactory)
				.setServiceAccountId(serviceAccountMail)
				.setServiceAccountPrivateKeyFromP12File(new File(keyFileLocation))
				.setServiceAccountScopes(AnalyticsScopes.all()).build();

		// Construct the Analytics service object.
		return new Analytics.Builder(httpTransport, jsonFactory, credential)
				.setApplicationName(applicationName).build();
	}

	private GaData getResults(Analytics analytics, String profileId, String startTime, String endTime,String metrics,String dimensions, int maxResults)
			throws IOException, InterruptedException 
	{
		// Query the Core Reporting API for the number of sessions
		if(maxResults==0)
		{
			maxResults=defaultMaxResults;
		}
		if(profileId != null && startTime!= null && endTime !=null && metrics!=null && dimensions!=null)
		{
			logger.debug("Executing Query.....");
			try
			{
			return analytics
				.data()
				.ga()
				.get("ga:" + profileId, startTime,endTime,metrics)
				.setDimensions(dimensions).setMaxResults(maxResults)
				.execute();
			}catch(SocketTimeoutException e)
			{
				logger.debug("Retrying as timeout happen...");
				Thread.sleep(10000);
				return analytics
						.data()
						.ga()
						.get("ga:" + profileId, startTime,endTime,metrics)
						.setDimensions(dimensions).setMaxResults(maxResults)
						.execute();
			}
		}
		else
		{
			logger.error("Query parameters should not be null!!");
			return null;
		}
	}
	private String printGaData(GaData results) throws IOException
	{ 
	     //Writing results to a file
	     PrintWriter pw = null;
    	 logger.debug("Writing results to file ..");
    	 File f=File.createTempFile("temp",".csv");
    	 outputFileName=f.getAbsolutePath();
    	 pw=new PrintWriter(new BufferedWriter(new FileWriter(f.getAbsolutePath())));
    	 //getting the queries to print
	     if (results.getRows() == null || results.getRows().isEmpty()) 
	     {
	         pw.println("No results Found.");
	         logger.debug("No results Found.");
	         pw.close();
	         throw new IOException("No Results found for the query");
	     }
	     else
	     {
		      // Printing column headers.
		      int count=0;
		      for (ColumnHeaders header : results.getColumnHeaders())
		      {
		    	  if(count==0)
		    	  {
		    		   pw.print(header.getName().replaceAll("ga:",""));
		    		   count++;
		    	  }
		    	  else
		    	  {
		    		   pw.print("|"+header.getName().replaceAll("ga:",""));
		    	  }
		           
		       }
		      // Print actual data.
		      for (List<String> row : results.getRows()) 
		      {
		    	count=0;
		    	pw.println();
		        for (String column : row) 
		        {
		        	if(count==0)
		        	{
		        		pw.print(column.replaceAll(",", "").replaceAll("\"", ""));
		        		count++;
		        	}
		        	else
		        	{
		        		pw.print("|"+column.replaceAll(",", "").replaceAll("\"", ""));
		        	} 
		        }
		      } 
		      pw.close();
		      logger.info("Completed!! All metrics are stored in file");
		    }
	     return outputFileName;
	  }
}
