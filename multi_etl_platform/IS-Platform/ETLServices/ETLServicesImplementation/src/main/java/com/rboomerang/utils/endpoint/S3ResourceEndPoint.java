package com.rboomerang.utils.endpoint;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.rboomerang.utils.S3Helper;

public class S3ResourceEndPoint implements IEndPointResource {
	private static Logger logger = LoggerFactory.getLogger(S3ResourceEndPoint.class);

	public static final String S3_BUCKET_NAME = "S3_BUCKET_NAME";
	public static final String S3_BUCKET_REGION = "S3_BUCKET_REGION";

	private HashMap<String, String> initValues = new HashMap<String, String>();
	private S3Helper s3helper;

	public S3ResourceEndPoint(HashMap<String, String> initValues) {
		this.initValues = initValues;
		logger.info("S3ResourceEndPoint constructor called");
	}

	@Override
	public void closeConnection() throws IOException {
		logger.info("S3 close called");
	}

	@Override
	public boolean checkFileExistence(String baseDirectory, String fileName) throws Exception {
		// TODO Auto-generated method stub
		s3helper = new S3Helper(initValues);
		boolean fileFound = false;

		try {

			if (s3helper.isPresent(baseDirectory + fileName))
				fileFound = true;

			/*
			 * String command = "/usr/local/bin/aws s3 ls "
			 * +baseDirectory+"/"+file; logger.info(command); String result =
			 * ExecuteCommand.executeCommand(command);
			 * if(result.contains(file)){ retVal.put(file, true); }
			 */

		} catch (AmazonServiceException e) {
		} catch (AmazonClientException exp) {
		}
		logger.info("S3ResourceEndPoint checkFileExistence called");
		return fileFound;
	}

	public static void main(String[] args) throws Exception {
		HashMap<String, String> init = new HashMap<String, String>();
		init.put(S3_BUCKET_NAME, "etl-backup-all-clients");
		S3ResourceEndPoint s = new S3ResourceEndPoint(init);
		// ArrayList<String> list = new ArrayList<String>();
		// list.add("USAP_CHANNEL_ITEM_MASTER_20151008.txt");
		// list.add("USAP_FACT_PERFORMANCE_20151008.txt");
		System.out.println(
				s.checkFileExistence("us-auto-parts/archive/2015/10/08", "USAP_FACT_PERFORMANCE_20151008.txt"));
	}

	@Override
	public InputStream getFile(String s3FilePath, String s3FileName) throws Exception {
		logger.info("Downloading File from S3");
		s3helper = new S3Helper(initValues);
		InputStream iStream = null;
		try {
			iStream = s3helper.readInputStream(s3FilePath, s3FileName);
		} catch (AmazonS3Exception aE) {
			logger.error(aE.getMessage());
			if (aE.getMessage().contains("The specified key does not exist."))
				throw new Exception(s3FileName + " File not present in S3 at following path " + s3FilePath);
			else
				throw aE;
		}
		return iStream;
	}

	@Override
	public void writeFile(InputStream inputStream, String s3FilePath, String s3FileName) throws Exception {
		S3Helper s3Helper = new S3Helper(initValues);
		s3Helper.storeObject(s3FilePath, s3FileName, inputStream);
	}

	@Override
	public String getFileHeaderLine(String location, String fileName) throws IOException {
		String fileHeader = "";
		InputStream fileStream = null;
		try {
			fileStream = getFile(location, fileName);
		} catch (Exception e) {
			logger.info("File not available in S3");
			e.printStackTrace();
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(fileStream));
		fileHeader = reader.readLine();
		reader.close();
		fileStream.close();

		return fileHeader;
	}
	
	@Override
	public List<String> getFileList(String sftpFilePath, String sftpFileName) throws Exception {
		logger.info("Downloading Files from S3");
		s3helper = new S3Helper(initValues);
		return s3helper.getFileListMatchingRegexFromSFTP(sftpFilePath, sftpFileName);
	}
}
