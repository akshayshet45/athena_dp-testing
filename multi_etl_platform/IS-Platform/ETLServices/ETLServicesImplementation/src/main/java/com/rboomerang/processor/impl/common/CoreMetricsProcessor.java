package com.rboomerang.processor.impl.common;
/**
 * Description: CoreMetricsAPI Processor
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.CoreMetricsAPI;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

public class CoreMetricsProcessor extends AbstractService 
{
	private static Logger logger = LoggerFactory.getLogger(CoreMetricsProcessor.class);
	public CoreMetricsProcessor(String clientSpecpath, String processorSpecPath) throws Exception
	{
		super(clientSpecpath, processorSpecPath);
		logger.info("CoreMetricsProcessor constructor called");
	}
	public CoreMetricsProcessor(String clientSpecpath, String processorSpecPath, String runtimeDate) throws Exception
	{
		super(clientSpecpath, processorSpecPath,runtimeDate);
		logger.info("CoreMetricsProcessor constructor called");
	}
	public CoreMetricsProcessor(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext, String runtimeDate) throws Exception 
	{
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.info("CoreMetricsProcessor constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject processorSpec) throws Exception
	{
		logger.info("CoreMetricsProcessor Processor execution started");
		//Reading CoreMetrics API global parameters
		JSONObject accountParams = (JSONObject) processorSpec.get("account");
		JSONObject apiQuery= (JSONObject) processorSpec.get(CommonConstants.QUERY);
		String clientID=accountParams.get("clientID").toString();
		String userName=accountParams.get("user").toString();
		String secret=accountParams.get("secret").toString();
		//Invoking GoogleAnalytics API query
		CoreMetricsAPI CMObject =new CoreMetricsAPI(clientID,userName,secret,apiQuery);
		Status status=null;
		//Writing the file to specified endPoint for backup
		IEndPointResource endPointResource=null;
		try
		{
			JSONObject endpointDetails=(JSONObject) processorSpec.get(CommonConstants.DESTINATION);
			String feedFileName = (String)endpointDetails.get(CommonConstants.FILENAME);
			String destDateSuffix = (String)endpointDetails.get(CommonConstants.FEED_DATE);
			String location = (String)endpointDetails.get(CommonConstants.LOCATION);
			if (destDateSuffix == null || destDateSuffix.isEmpty() )
			{
				destDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
			}
			feedFileName = replaceDateTag(feedFileName, destDateSuffix);
			logger.info("output file name is "+feedFileName);
			String fileName=CMObject.generateMetricReport();

			if(fileName !=null)
			{
				File outputTempFile=new File(fileName);
				InputStream in= new FileInputStream(new File(fileName));
				String endpointName = (String)endpointDetails.get(CommonConstants.ENDPOINTNAME);
				String endpointType = getTypeOfEndPoint(clientSpec,endpointName);
				HashMap<String, String> endPointResourceProperties = buildResourceProperties(endpointName, clientSpec,endpointType);
				endPointResource = EndPointResourceFactory.getInstance().getEndPointResource(endpointType,endPointResourceProperties);
				logger.debug("Uploading file to "+endPointResourceProperties);
				endPointResource.writeFile(in,location,feedFileName);
				logger.info(String.format("Successfully uploaded!! file path is %s %s%s",endpointType,location,feedFileName));
				status = new Status(Status.STATUS_SUCCESS, "CoreMetricsProcessor Processed metrics successfully!!");
				boolean delStatus=outputTempFile.delete();
				if(delStatus)
				{
					logger.debug("Temporary file "+fileName+" has been deleted!!");
				}
				else
				{
					logger.debug("Unable to delete temp file "+fileName);
				}
			}
			else
			{
				status = new Status(Status.STATUS_ERROR, "Uploading generated metric file failed!!");
			}
		}
		catch(Exception e)
		{
			logger.error("Uploading file to endPoint failed");
			status = new Status(Status.STATUS_ERROR, "Uploading generated metric file failed!!");
			e.printStackTrace();
		}
		finally
		{
			endPointResource.closeConnection();
			logger.debug("Connection to endPoint closed");
		}	
		return status;
	}
}
