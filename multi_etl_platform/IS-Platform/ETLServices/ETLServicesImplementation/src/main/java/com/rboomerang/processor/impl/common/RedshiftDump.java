package com.rboomerang.processor.impl.common;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSSessionCredentials;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.common.framework.javabeans.DbSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.AWSAccess;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.InputStreamHandler;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;
import com.rboomerang.utils.endpoint.S3ResourceEndPoint;

public class RedshiftDump extends AbstractService {
	private static Logger logger = LoggerFactory.getLogger(RedshiftDump.class);
	private static Properties props = System.getProperties();

	public RedshiftDump(String clientSpecpath, String processorSpecPath) throws Exception {
		super(clientSpecpath, processorSpecPath);
		logger.info("RedshiftDump constructor called");
	}

	public RedshiftDump(String clientSpecpath, String processorSpecPath, String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeDate);
		logger.info("RedshiftDump constructor called");
	}

	public RedshiftDump(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext,
			String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.info("RedshiftDump constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject processorSpec) throws Exception {
		// getting parameters from json
		JSONObject list = (JSONObject) processorSpec.get(CommonConstants.LIST);
		JSONObject source = (JSONObject) list.get(CommonConstants.SOURCE);
		JSONObject temporaryS3Dump = (JSONObject) list.get("temporaryS3Dump");
		JSONObject dest = (JSONObject) list.get(CommonConstants.DESTINATION);

		String systemTempPath = (String) list.get("systemTempPath");

		if (systemTempPath != null) {
			props.setProperty("java.io.tmpdir", systemTempPath);
		}

		String sourceEndPoint = (String) source.get(CommonConstants.ENDPOINTNAME);
		String unloadQuery = null;
		String sqlFilePath = null;
		String queryFilePath = (String) source.get("sqlFilePath");

		if (queryFilePath == null) {
			logger.debug("query provided through spec");
			unloadQuery = (String) source.get(CommonConstants.QUERY);
		} else {
			sqlFilePath = this.getRuntimeContext().transformPath(queryFilePath);
			logger.debug("query provided through file");
			try {
				unloadQuery = readQuery(sqlFilePath);
			} catch (Exception e) {
				logger.error("Error occured in reading query : " + e.getMessage());
				return new Status(Status.STATUS_ERROR, "Exception occured in reading query : " + e.getMessage());
			}
		}
		String destEndPoint = (String) dest.get(CommonConstants.ENDPOINTNAME);
		String temporaryS3DumpEndPoint = (String) temporaryS3Dump.get(CommonConstants.ENDPOINTNAME);

		String dateSuffix = (String) dest.get(CommonConstants.FEED_DATE);
		if (dateSuffix == null || dateSuffix.isEmpty())
			dateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();

		String destPath = (String) dest.get(CommonConstants.PATH);
		destPath = replaceDynamicFolders(destPath, dateSuffix);
		String destFileName = (String) dest.get(CommonConstants.FILE_NAME);
		destFileName = replaceDateTag(destFileName, dateSuffix);
		String destFileCompression = (String) dest.get(CommonConstants.COMPRESSION);
		String delimiter = (String) list.get(CommonConstants.DELIMITER);
		String temporaryS3DumpPath = (String) temporaryS3Dump.get(CommonConstants.PATH);
		String headers = (String) list.get("headers");
		String parallel = (String) list.get("parallel");

		if (parallel == null)
			parallel = "on";

		String escape = (String) list.get("escape");

		if (escape == null)
			escape = "on";

		String addQuotes = (String) list.get("addQuotes");

		if (addQuotes == null)
			addQuotes = "off";

		// getting temp s3 dump end point properties
		String temporaryS3DumpType = getTypeOfEndPoint(clientSpec, temporaryS3DumpEndPoint);
		HashMap<String, String> temporaryS3DumpEndPointResourceProperties = super.buildResourceProperties(
				temporaryS3DumpEndPoint, clientSpec, temporaryS3DumpType);

		logger.debug("\nDump location on s3 : bucketName -->"
				+ temporaryS3DumpEndPointResourceProperties.get(S3ResourceEndPoint.S3_BUCKET_NAME) + "\npath --> "
				+ temporaryS3DumpPath);
		AWSAccess awsAccess = new AWSAccess();
		AWSSessionCredentials awsCredentials = awsAccess.getAWSAccessWithSession();
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("unload('").append(unloadQuery).append("') to 's3://")
				.append(temporaryS3DumpEndPointResourceProperties.get(S3ResourceEndPoint.S3_BUCKET_NAME)).append("/")
				.append(temporaryS3DumpPath).append("s3Unload_' credentials 'aws_access_key_id=").append(awsCredentials.getAWSAccessKeyId())
				.append(";aws_secret_access_key=").append(awsCredentials.getAWSSecretKey())
				.append(";token=").append(awsCredentials.getSessionToken())
				.append("' delimiter '").append(delimiter)
				.append("' parallel ").append(parallel);
		
		if (addQuotes.equals("on")) {
			queryBuilder.append(" addquotes");
		}
		
		if(escape.equals("on")){
			queryBuilder.append(" escape");
		}
		queryBuilder.append(" allowoverwrite manifest;");
		String query = queryBuilder.toString();
		logger.debug("query to be run :\n" + query);
		logger.info("Initiating Process....");
		File finalDumpFile = File.createTempFile("finalRedshiftDump", ".tmp");
		logger.debug("Final file creation path : " + finalDumpFile.getAbsolutePath());

		BufferedWriter writer = new BufferedWriter(new FileWriter(finalDumpFile, true));
		logger.info("headers :: " + headers);
		writer.write(headers + "\n");
		writer.close();

		// getting db credentials
		Object[] dbtype = getDbDetails(sourceEndPoint);
		DbSpec details = (DbSpec) dbtype[1];
		String url = details.getHost();
		String port = "5439";
		String database = details.getDb();
		String userName = details.getUname();
		String password = details.getPasswd();
		Statement stmt = null;
		Class.forName("org.postgresql.Driver");
		logger.debug("jdbc:postgresql://" + url + ":" + port + "/" + database + "____" + userName + "____" + password);
		Connection conn = DriverManager.getConnection("jdbc:postgresql://" + url + ":" + port + "/" + database,
				userName, password);
		logger.debug("Connection successfull");
		conn.setAutoCommit(false);
		logger.debug("Opened database successfully");
		stmt = conn.createStatement();
		logger.debug("executing unload query now.");
		stmt.executeUpdate(query);
		stmt.close();
		conn.close();

		// making connection with source endpoint
		IEndPointResource temporaryS3DumpEndPointResource = EndPointResourceFactory.getInstance()
				.getEndPointResource(temporaryS3DumpType, temporaryS3DumpEndPointResourceProperties);

		// downloading the sourceFile
		InputStream inputStream = temporaryS3DumpEndPointResource.getFile(temporaryS3DumpPath, "s3Unload_manifest");
		ArrayList<String> listOfFileNames = getListOfFiles(inputStream, temporaryS3DumpPath);

		for (String fileName : listOfFileNames) {
			logger.info(" getting fileName ::: " + fileName);
			InputStream fileData = temporaryS3DumpEndPointResource.getFile(temporaryS3DumpPath, fileName);
			InputStreamHandler.appendInputStreamToFile(fileData, finalDumpFile.getAbsolutePath());
		}
		temporaryS3DumpEndPointResource.closeConnection();

		// getting destination end point properties
		String destType = getTypeOfEndPoint(clientSpec, destEndPoint);
		HashMap<String, String> destEndPointResourceProperties = super.buildResourceProperties(destEndPoint, clientSpec,
				destType);

		// making connection with source endpoint
		IEndPointResource destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(destType,
				destEndPointResourceProperties);
		// uploading encrypted file
		if (destFileCompression != null && destFileCompression.equalsIgnoreCase("zip")) {
			logger.debug("compressing file to zip");
			finalDumpFile = zip(finalDumpFile, destFileName);
		}

		destEndPointResource.writeFile(new FileInputStream(finalDumpFile), destPath, destFileName);
		destEndPointResource.closeConnection();
		finalDumpFile.delete();
		return new Status(Status.STATUS_SUCCESS, "Redshift dump successfull ! " + destFileName + " file uploaded to "
				+ destEndPoint + " at path " + destPath);

	}


	private ArrayList<String> getListOfFiles(InputStream sourceStream, String splitValue) throws IOException {
		ArrayList<String> fileNames = new ArrayList<String>();
		BufferedReader reader = new BufferedReader(new InputStreamReader(sourceStream));
		String line = null;
		while (true) {

			line = reader.readLine();
			if (line == null)
				break;
			if (line.contains(splitValue)) {
				String[] words = line.split(splitValue);
				fileNames.add(words[words.length - 1].split("\"")[0]);
			}
		}

		return fileNames;

	}

	private String readQuery(String sqlFilePath) throws Exception {
		BufferedReader bufferedReader = new BufferedReader(new FileReader(sqlFilePath));
		String query = bufferedReader.readLine();
		bufferedReader.close();
		return query;
	}
}
