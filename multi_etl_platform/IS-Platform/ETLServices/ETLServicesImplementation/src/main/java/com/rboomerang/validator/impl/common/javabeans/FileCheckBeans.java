package com.rboomerang.validator.impl.common.javabeans;

import java.util.HashMap;
import java.util.List;

import com.rboomerang.common.framework.javabeans.DbSpec;
import com.rboomerang.common.framework.javabeans.DirectroyFile;
import com.rboomerang.common.framework.javabeans.MessageContentSpec;
import com.rboomerang.common.framework.javabeans.SftpSpec;

public class FileCheckBeans {
	private List<DirectroyFile> list;
	private HashMap<String,MessageContentSpec> message;
	private boolean abortRun;
	private String notify;
	private boolean pagerdutyAlert;
	private String endpointName;
	private String type;
	private SftpSpec sftpAuth=null;
	private DbSpec dbAuth=null;
	
	public String getType(){
		return type;
	}
	
	public List<DirectroyFile> getList() {
		return list;
	}
	public void setList(DirectroyFile directroyFile) {
		list.add(directroyFile);
	}
	public void setList(List<DirectroyFile> list) {
		this.list = list;
	}
	public HashMap<String, MessageContentSpec> getMessage() {
		return message;
	}
	public void setMessage(HashMap<String, MessageContentSpec> message) {
		this.message = message;
	}
	public void setMessage(String type, MessageContentSpec message) {
		this.message.put(type, message);
	}
	public boolean isAbortRun() {
		return abortRun;
	}
	public void setAbortRun(boolean abortRun) {
		this.abortRun = abortRun;
	}
	public String getNotify() {
		return notify;
	}
	public void setNotify(String notify) {
		this.notify = notify;
	}
	public boolean isPagerdutyAlert() {
		return pagerdutyAlert;
	}
	public void setPagerdutyAlert(boolean pagerdutyAlert) {
		this.pagerdutyAlert = pagerdutyAlert;
	}
	public SftpSpec getSftpAuth() {
		return sftpAuth;
	}
	public void setSftpAuth(SftpSpec sftpAuth) {
		this.sftpAuth = sftpAuth;
	}
	public DbSpec getDbAuth() {
		return dbAuth;
	}
	public void setDbMySqlAuth(DbSpec dbAuth) {
		type="mysql";
		this.dbAuth = dbAuth;
	}
	public void setDbRedshiftAuth(DbSpec dbAuth) {
		type="redshift";
		this.dbAuth = dbAuth;
	}
	public String getEndpointName() {
		return endpointName;
	}
	public void setEndpointName(String endpointName) {
		this.endpointName = endpointName;
	}
}
