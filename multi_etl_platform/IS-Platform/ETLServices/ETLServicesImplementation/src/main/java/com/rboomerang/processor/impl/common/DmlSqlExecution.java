package com.rboomerang.processor.impl.common;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.common.framework.javabeans.DbSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.ExecuteCommand;

public class DmlSqlExecution extends AbstractService {

	private static Logger logger = LoggerFactory.getLogger(DmlSqlExecution.class);
	private Status succeess = new Status(Status.STATUS_SUCCESS, "Execution DmlSqlExecution successful");
	private Status failure = new Status(Status.STATUS_ERROR, "Execution DmlSqlExecution failed");
	private static final String DML_SCRIPT = "scripts/DmlSql.sh";

	public DmlSqlExecution(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext, String runtimeDate)
			throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.info("DmlSqlExecution constructor called");
	}

	public DmlSqlExecution(String clientSpecpath, String validatorSpecPath, String runtimeDate) throws Exception {
		super(clientSpecpath, validatorSpecPath, runtimeDate);
		logger.info("DmlSqlExecution constructor called");
	}

	public DmlSqlExecution(String clientSpecpath, String validatorSpecPath) throws Exception {
		super(clientSpecpath, validatorSpecPath);
		logger.info("DmlSqlExecution constructor called");
	}

	@SuppressWarnings("static-access")
	@Override
	public Status service(ClientSpec clientSpec, JSONObject processorSpec) throws Exception {
		Status returnval = null;
		ExecuteCommand command = new ExecuteCommand();

		Object[] dbtype = getDbDetails((String) processorSpec.get(CommonConstants.ENDPOINT));
		logger.debug("sqlFilePath in processor spec: "+(String) processorSpec.get(CommonConstants.FILE_LOCATION));
		String sqlFilePath = this.getRuntimeContext().transformPath((String) processorSpec.get(CommonConstants.FILE_LOCATION));
		logger.debug("sqlFilePath actual path: "+(String) processorSpec.get(CommonConstants.FILE_LOCATION));
		DbSpec details = (DbSpec) dbtype[1];
		logger.debug("base dir of etl-services: "+this.getRuntimeContext().getETLServicesDirectory());
		String shFilePath = this.getRuntimeContext().getETLServicesDirectory() + "/" + DmlSqlExecution.DML_SCRIPT;

		logger.info("final DmlSql.sh file path"+shFilePath);
		logger.info("final sql file path"+sqlFilePath);
		StringBuilder execute = new StringBuilder();
		execute.append(sqlFilePath).append(CommonConstants.SPACE).append(details.getPasswd())
				.append(CommonConstants.SPACE).append(details.getHost()).append(CommonConstants.SPACE)
				.append(details.getDb()).append(CommonConstants.SPACE).append(details.getUname());

		String output = command.executeCommands(shFilePath, execute.toString());

		returnval = (output.contains("ERROR")) ? failure : succeess;
		returnval.setMessage(output);
		return returnval;
	}
}