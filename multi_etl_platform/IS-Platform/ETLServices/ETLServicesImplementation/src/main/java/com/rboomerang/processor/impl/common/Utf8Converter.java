package com.rboomerang.processor.impl.common;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.InputStreamHandler;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

public class Utf8Converter extends AbstractService {

	private static Logger logger = LoggerFactory.getLogger(Utf8Converter.class);
	public Utf8Converter(String clientSpecpath, String processorSpecPath) throws Exception
	{
		super(clientSpecpath, processorSpecPath);
		logger.info("Utf8Converter constructor called");
	}
	public Utf8Converter(String clientSpecpath, String processorSpecPath, String runtimeDate) throws Exception
	{
		super(clientSpecpath, processorSpecPath, runtimeDate);
		logger.info("Utf8Converter constructor called");
	}
	public Utf8Converter(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext, String runtimeDate) throws Exception 
	{
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.info("Utf8Converter constructor called");
	}
	@Override
	public Status service(ClientSpec clientSpec, JSONObject processorSpec)
			throws JSchException, SftpException, Exception
	{
		logger.info("Utf8 converter started");
		Status status=null;
		IEndPointResource endPointResource=null;
		IEndPointResource destEndPointResource=null;
		try
		{
			JSONArray sourceEndpointDetails=(JSONArray) processorSpec.get(CommonConstants.SOURCE);
			int fileCount=sourceEndpointDetails.size();
			String[] tempFiles=new String[fileCount];
			String[] fileNames=new String[fileCount];
			String[] feeds=new String[fileCount];
			int index;
			for(index=0;index<fileCount;index++)
			{
				JSONObject endpointDetails=(JSONObject)sourceEndpointDetails.get(index);
				String feedFileName = (String)endpointDetails.get(CommonConstants.FILENAME);
				
				String sourceDateSuffix = (String)endpointDetails.get(CommonConstants.FEED_DATE);
				String location = (String)endpointDetails.get(CommonConstants.LOCATION);
				
				if (sourceDateSuffix == null || sourceDateSuffix.isEmpty() )
				{
					sourceDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
				}
				feedFileName = replaceDateTag(feedFileName, sourceDateSuffix);
				feeds[index]=feedFileName;
				String endpointName = (String)endpointDetails.get(CommonConstants.ENDPOINTNAME);
				String endpointType = getTypeOfEndPoint(clientSpec,endpointName);
				HashMap<String, String> endPointResourceProperties = buildResourceProperties(endpointName, clientSpec,endpointType);
				endPointResource = EndPointResourceFactory.getInstance().getEndPointResource(endpointType,endPointResourceProperties);
				logger.debug("Downloading file from "+endPointResourceProperties);
				logger.info("Starting converting file "+feedFileName);
				File tempFile=File.createTempFile("tempFileSource",".csv");
				InputStream in=endPointResource.getFile(location, feedFileName);
				InputStreamHandler.writeInputStreamToFile(in, tempFile.getAbsolutePath());
				fileNames[index]=tempFile.getAbsolutePath();
				File outputTempFile=File.createTempFile("tempFiledest",".csv");
				PrintWriter out=new PrintWriter(new BufferedWriter(new FileWriter(outputTempFile.getAbsolutePath())));
			    BufferedReader bin=new BufferedReader(new InputStreamReader(new FileInputStream(tempFile.getAbsolutePath()),"UTF-8"));
			    int r;
			    while((r=bin.read())!=-1)
			    {
			    	if(r>127)
			    	 {
			    		out.print("");
			    	 }
			    	 else
			    	 {
			    		out.print((char)r);
			    	 }
			     }
			     out.close();
			     bin.close();
			     tempFiles[index]=outputTempFile.getAbsolutePath();
			     logger.info("Conversion done for the file "+feedFileName);
			     endPointResource.closeConnection();
			}
			
			//Defining common destination endPOint
			logger.info("Uploading files to destination");
			JSONObject destEndPointDetails=(JSONObject)processorSpec.get(CommonConstants.DESTINATION);
			String destEndpointName = (String)destEndPointDetails.get(CommonConstants.ENDPOINTNAME);
			String destEndpointType = getTypeOfEndPoint(clientSpec,destEndpointName);
			int count=0;
			for(String file:tempFiles)
			{
			HashMap<String, String> destEndPointResourceProperties = buildResourceProperties(destEndpointName, clientSpec,destEndpointType);
			destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(destEndpointType,destEndPointResourceProperties);
			String destLocation = (String)destEndPointDetails.get(CommonConstants.LOCATION);
			InputStream inputStream=new FileInputStream(file);
			destEndPointResource.writeFile(inputStream, destLocation,feeds[count++]);
			inputStream.close();
			destEndPointResource.closeConnection();
			}
			logger.info("Done!! All files are uploaded to destination");
			
			logger.debug("Deleting temporary files");
			for(String file:fileNames)
			{
				File f=new File(file);
				if(f.delete())
				{
					logger.debug("deleted temporary file");
				}
				else
				{
					logger.warn("Unable to delete temporary file");
				}
			}
			for(String file:tempFiles)
			{
				File f=new File(file);
				if(f.delete())
				{
					logger.debug("deleted temporary file");
				}
				else
				{
					logger.warn("Unable to delete temporary file");
				}
			}
			status = new Status(Status.STATUS_SUCCESS, "Utf8converter executed successfully!!");
		}
		catch(Exception e)
		{
			status=new Status(Status.STATUS_ERROR,"Error in converting file to utf8 character!!");
			if(endPointResource!=null)
			{
				endPointResource.closeConnection();
			}
			if(destEndPointResource!=null)
			{
				destEndPointResource.closeConnection();
			}
			e.printStackTrace();
		}
		
		return status;
	}
}
