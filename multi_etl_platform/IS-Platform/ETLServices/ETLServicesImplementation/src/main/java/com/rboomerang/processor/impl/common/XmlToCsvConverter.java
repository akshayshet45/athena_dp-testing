package com.rboomerang.processor.impl.common;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.Converter;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

public class XmlToCsvConverter extends AbstractService {
	private static final Logger logger = LoggerFactory.getLogger(XmlToCsvConverter.class);
	private static Properties props = System.getProperties();

	public XmlToCsvConverter(String clientSpecpath, String processorSpecPath) throws Exception {
		super(clientSpecpath, processorSpecPath);
		logger.info("XmlToCsvConverter constructor called");
	}

	public XmlToCsvConverter(String clientSpecpath, String processorSpecPath, String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeDate);
		logger.info("XmlToCsvConverter constructor called");
	}

	public XmlToCsvConverter(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext,
			String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.info("XmlToCsvConverter constructor called");
	}

	public Status service(ClientSpec clientSpec, JSONObject processorSpec)
			throws JSchException, SftpException, Exception {

		JSONObject list = (JSONObject) processorSpec.get(CommonConstants.LIST);
		JSONObject source = (JSONObject) list.get(CommonConstants.SOURCE);
		JSONObject dest = (JSONObject) list.get(CommonConstants.DESTINATION);
		String systemTempPath = (String) list.get("systemTempPath");

		if (systemTempPath != null) {
			props.setProperty("java.io.tmpdir", systemTempPath);
		}

		String sourceDateSuffix = (String) source.get(CommonConstants.FEED_DATE);
		String destDateSuffix = (String) dest.get(CommonConstants.FEED_DATE);
		if (sourceDateSuffix == null || sourceDateSuffix.isEmpty())
			sourceDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
		if (destDateSuffix == null || destDateSuffix.isEmpty())
			destDateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();

		// getting source Endpoint details
		String sourceEndPoint = (String) source.get(CommonConstants.ENDPOINTNAME);
		// getting source file path from spec file and replacing date month or
		// year from it!
		String sourcePath = (String) source.get(CommonConstants.PATH);
		// getting source file name from spec file and replacing feedDate from
		// it!
		String sourceFileName = (String) source.get(CommonConstants.FILE_NAME);

		// getting Destination Endpoint details
		String destEndPoint = (String) dest.get(CommonConstants.ENDPOINTNAME);
		// getting destination file path from spec file and replacing date month
		// or year from it!
		String destPath = (String) dest.get(CommonConstants.PATH);
		// getting destination file path from spec file and replacing feedDate
		// from it!
		String destFileName = (String) dest.get(CommonConstants.FILE_NAME);

		destPath = replaceDynamicFolders(destPath, destDateSuffix);
		sourcePath = replaceDynamicFolders(sourcePath, sourceDateSuffix);
		destFileName = replaceDateTag(destFileName, destDateSuffix);
		sourceFileName = replaceDateTag(sourceFileName, sourceDateSuffix);
		logger.info("Input File Source Endpoint :: " + sourceEndPoint);
		logger.info("Output File destination Endpoint:: " + destEndPoint);
		logger.info("Input File Source Path --> " + sourcePath + " :: FileName --> " + sourceFileName);
		logger.info("Output File destination --> " + destPath + " :: and FileName --> " + destFileName);

		String xslFilePath = this.getRuntimeContext().transformPath((String) source.get("xslFilePath"));
		String compression = (String) source.get("compression");
		String headers = (String) source.get("headers");
		boolean bigFile = (boolean) source.get("splitRequired");
		String splitVal = null;
		if (bigFile) {
			splitVal = (String) source.get("splitTag");
		}
		boolean isStream = true;
		if (compression.equals("zip") || compression.equals("gz"))
			isStream = false;
		// creating temporary file to download file from endpoint
		File sourceDownloadedFile = File.createTempFile("sourceFile", ".tmp");

		// getting source endpoint properties
		String sourceType = super.getTypeOfEndPoint(clientSpec, sourceEndPoint);
		if (sourceType.equals("sftp"))
			sourceFileName = makeRegex(sourceFileName);
		HashMap<String, String> sourceEndPointResourceProperties = super.buildResourceProperties(sourceEndPoint,
				clientSpec, sourceType);

		// making connection with source endpoint
		IEndPointResource sourceEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(sourceType,
				sourceEndPointResourceProperties);

		// downloading the sourceFile
		InputStream inputStream = sourceEndPointResource.getFile(sourcePath, sourceFileName);

		InputStream xmlInputStream = inputStream;

		if (compression.equals("zip")) {
			try {
				unzip(inputStream, sourceDownloadedFile.getAbsolutePath());
			} catch (Exception e) {
				if (e.getMessage().contains("Zip Contains multiple files"))
					return new Status(Status.STATUS_ERROR, sourceFileName + " at path " + sourcePath + " at "
							+ sourceEndPoint + " contains multiple files!");
				else if (e.getMessage().contains("zip is empty!"))
					return new Status(Status.STATUS_ERROR,
							sourceFileName + " at path " + sourcePath + " at " + sourceEndPoint + " is empty");
				else
					return new Status(Status.STATUS_ERROR, "Exception occured : " + e.getMessage());
			}

		}
		String xmlFilePath = sourceDownloadedFile.getAbsolutePath();
		File finalOutputFile = File.createTempFile("finalXmlOutput", ".tmp");
		if (bigFile) {
			if (!isStream) {
				File xmlSource = new File(xmlFilePath);
				writeInputStreamToFile(new FileInputStream(xmlSource), xslFilePath, splitVal, headers, finalOutputFile);
			} else {
				writeInputStreamToFile(xmlInputStream, xslFilePath, splitVal, headers, finalOutputFile);
			}
		} else {

			if (!isStream) {
				Converter.xmltocsv(xslFilePath, xmlFilePath, finalOutputFile.getAbsolutePath());
			} else {
				Converter.xmltocsv(xslFilePath, xmlInputStream, finalOutputFile.getAbsolutePath());
			}
		}
		logger.info("Conversion done!!!!");

		// getting destination end point properties
		String destType = getTypeOfEndPoint(clientSpec, destEndPoint);
		HashMap<String, String> destEndPointResourceProperties = super.buildResourceProperties(destEndPoint, clientSpec,
				destType);

		// making connection with source endpoint
		IEndPointResource destEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(destType,
				destEndPointResourceProperties);

		// uploading encrypted file
		destEndPointResource.writeFile(new FileInputStream(finalOutputFile), destPath, destFileName);

		sourceEndPointResource.closeConnection();
		destEndPointResource.closeConnection();

		// deleting all temporary files
		sourceDownloadedFile.delete();
		finalOutputFile.delete();
		logger.info("deleting all temp files!");

		return new Status(Status.STATUS_SUCCESS,
				"Execution successful for file " + sourceFileName + " in XMLToCSVConverter");
	}

	private void unzip(InputStream inputStream, String opFilePath) throws Exception {

		ZipInputStream zipIs = null;
		ZipEntry zEntry = null;

		zipIs = new ZipInputStream(inputStream);
		String fileName = null;
		zEntry = zipIs.getNextEntry();
		if (zEntry == null) {
			logger.error("zip is empty!");
			throw new Exception("zip is empty!");
		}
		byte[] tmp = new byte[4 * 1024];
		FileOutputStream fos = null;
		fileName = zEntry.getName();
		logger.debug("Extracting following " + fileName + " file to " + opFilePath);
		fos = new FileOutputStream(opFilePath);
		int size = 0;
		while ((size = zipIs.read(tmp)) != -1) {
			fos.write(tmp, 0, size);
		}
		fos.flush();
		fos.close();
		if (zipIs.getNextEntry() != null) {
			logger.error("zip contains more than one file or directories !");
			throw new Exception("Zip Contains multiple files");
		}
		zipIs.close();
	}

	public static void writeInputStreamToFile(InputStream inputStream, String xslFilePath, String splitVal,
			String headers, File finalOutputFile) throws Exception {
		int filecount = 0;
		File tempdir = File.createTempFile("tempXmlDirectory", "");
		tempdir.delete();
		tempdir.mkdir();

		String tempDirectoryPath = tempdir.getAbsolutePath() + "/";

		writeHeader(finalOutputFile, headers);
		logger.debug("temporary directory path : " + tempdir.getAbsolutePath());
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		String line = null;
		while (true) {
			line = reader.readLine();
			if (line == null)
				throw new Exception("Xml File is empty");
			if (line.contains("<" + splitVal + ">") || line.contains("<" + splitVal + " ")) {
				break;
			}

		}

		while (true) {
			if (line == null)
				break;
			BufferedWriter writer = new BufferedWriter(
					new FileWriter(tempDirectoryPath + "xml" + filecount + ".xml", true));
			writer.write(line + "\n");
			writer.close();
			line = reader.readLine();
			if (line == null)
				break;
			if (line.contains("</" + splitVal + ">")) {
				BufferedWriter writer1 = new BufferedWriter(
						new FileWriter(tempDirectoryPath + "xml" + filecount + ".xml", true));
				writer1.write(line);
				writer1.close();
				Converter.xmltocsv(xslFilePath, tempDirectoryPath + "xml" + filecount + ".xml",
						tempDirectoryPath + "xmlparsedfinal" + filecount + ".csv");
				appendFiles(finalOutputFile, tempDirectoryPath + "xmlparsedfinal" + filecount + ".csv", headers);
				(new File(tempDirectoryPath + "xml" + filecount + ".xml")).delete();
				(new File(tempDirectoryPath + "xmlparsedfinal" + filecount + ".csv")).delete();
				filecount++;
				line = reader.readLine();
			}
		}
		logger.debug("total splits : " + filecount);
		reader.close();
		deleteFileOrDirectory(tempdir);
	}

	public static void deleteFileOrDirectory(File element) {
		if (element.isDirectory()) {
			for (File sub : element.listFiles()) {
				sub.delete();
			}
		}
		element.delete();
	}

	private static void writeHeader(File finalOutputFile, String headers) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(finalOutputFile, true));
		writer.write(headers + "\n");
		writer.close();

	}

	private static void appendFiles(File finalOutputFile, String tempFilePath, String headers) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(new File(tempFilePath)));
		BufferedWriter writer = new BufferedWriter(new FileWriter(finalOutputFile, true));
		String line = null;
		while (true) {
			line = reader.readLine();
			if (line == null)
				break;
			if (!line.contains(headers) && !line.equals(""))
				writer.write(line + "\n");
		}
		writer.close();
		reader.close();

	}

}
