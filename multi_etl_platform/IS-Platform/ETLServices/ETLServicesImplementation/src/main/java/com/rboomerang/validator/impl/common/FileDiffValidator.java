package com.rboomerang.validator.impl.common;


import java.io.InputStream;
import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

public class FileDiffValidator  extends AbstractService  {

	private static Logger logger = LoggerFactory.getLogger(FileDiffValidator.class);

	
	public FileDiffValidator(String clientSpecpath, String processorSpecPath) throws Exception {
		super(clientSpecpath, processorSpecPath);
		logger.debug("FileDiffValidator constructor called");
	}
	public FileDiffValidator(String clientSpecpath, String processorSpecPath, String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath,runtimeDate);
		logger.debug("FileDiffValidator constructor called");
	}
	public FileDiffValidator(String clientSpecpath, String processorSpecPath,RuntimeContext runtimeContext, String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath,runtimeContext, runtimeDate);
		logger.debug("FileDiffValidator constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject validatorSpec)
			throws JSchException, SftpException, Exception {

		//obtaining Common elements from processor spec list 
		JSONObject list = (JSONObject)validatorSpec.get(CommonConstants.LIST);
		JSONArray source = (JSONArray)list.get(CommonConstants.SOURCE);

		JSONObject sourceFile1 = (JSONObject)source.get(0);
		JSONObject sourceFile2 = (JSONObject)source.get(1);

		String sourceEndPoint1 = (String)sourceFile1.get(CommonConstants.ENDPOINTNAME);
		String sourceEndPoint2 = (String)sourceFile2.get(CommonConstants.ENDPOINTNAME);
		String sourceDateSuffix1 = (String) sourceFile1.get(CommonConstants.FEED_DATE);
		String sourceDateSuffix2 = (String) sourceFile2.get(CommonConstants.FEED_DATE);
		
		if ( sourceDateSuffix1 == null || sourceDateSuffix1.isEmpty())
			sourceDateSuffix1 = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
		if (sourceDateSuffix2 == null || sourceDateSuffix2.isEmpty() )
			sourceDateSuffix2 = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();
		
		
		//if date format is not specified in processor spec it will take from client spec
		String sourcePath1 = (String)sourceFile1.get(CommonConstants.PATH);
		String fileName1 = (String)sourceFile1.get(CommonConstants.FILE_NAME);
		String sourcePath2 = (String)sourceFile2.get(CommonConstants.PATH);
		String fileName2 = (String)sourceFile2.get(CommonConstants.FILE_NAME);
		
		sourcePath1 = replaceDynamicFolders(sourcePath1, sourceDateSuffix1);
		sourcePath2 = replaceDynamicFolders(sourcePath2, sourceDateSuffix2);
		fileName1 = replaceDateTag(fileName1, sourceDateSuffix1);
		fileName2 = replaceDateTag(fileName2, sourceDateSuffix2);
		

		logger.info("\nFiles to be compared:::\n"+fileName1+"\n"+fileName2+"\n");

		//getting source endpoint properties for file 1		
		String sourceType1 = super.getTypeOfEndPoint(clientSpec, sourceEndPoint1);
		if (sourceType1.equals("sftp"))
			fileName1 = makeRegex(fileName1);
		
		HashMap<String, String> sourceEndPointResourceProperties1 = super.buildResourceProperties(sourceEndPoint1, clientSpec,sourceType1);

		//getting source endpoint properties for file 2		
		String sourceType2 = super.getTypeOfEndPoint(clientSpec, sourceEndPoint2);
		if (sourceType2.equals("sftp"))
			fileName2 = makeRegex(fileName2);
		
		HashMap<String, String> sourceEndPointResourceProperties2 = super.buildResourceProperties(sourceEndPoint2, clientSpec,sourceType2);

		//making connection with source endpoint
		IEndPointResource sourceEndPointResource1 = EndPointResourceFactory.getInstance().getEndPointResource(
				sourceType1, sourceEndPointResourceProperties1);

		//making connection with source endpoint
		IEndPointResource sourceEndPointResource2 = EndPointResourceFactory.getInstance().getEndPointResource(
				sourceType2, sourceEndPointResourceProperties2);

		//downloading the sourceFile
		InputStream iStream_File1 = sourceEndPointResource1.getFile(sourcePath1, fileName1);
		InputStream iStream_File2 = sourceEndPointResource2.getFile(sourcePath2, fileName2);

		boolean equalFlag = IOUtils.contentEquals( iStream_File1, iStream_File2 );
		
		iStream_File1.close();
		iStream_File2.close();
		sourceEndPointResource1.closeConnection();
		sourceEndPointResource2.closeConnection();
		logger.debug("Flag value for file comparison :"+equalFlag);
		
		if(equalFlag)
			return new Status(Status.STATUS_SUCCESS, "Files : "+fileName1+" and "+fileName2+" are same!!");
		else
			return new Status(Status.STATUS_ERROR, "Files : "+fileName1+" and "+fileName2+" are not same!!");
	}
}
