
package com.rboomerang.validator.impl.common;

import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

public class FileExistenceCheck extends AbstractService {
	private static Logger logger = LoggerFactory.getLogger(FileExistenceCheck.class);

	public FileExistenceCheck(String clientSpecpath, String validatorSpecPath, RuntimeContext runtimeContext, String runtimeDate)
			throws Exception {
		super(clientSpecpath, validatorSpecPath, runtimeContext, runtimeDate);
		logger.info("File Check constructor called");
	}
	public FileExistenceCheck(String clientSpecpath, String validatorSpecPath, String runtimeDate)
			throws Exception {
		super(clientSpecpath, validatorSpecPath,runtimeDate);
		logger.info("File Check constructor called");
	}
	public FileExistenceCheck(String clientSpecpath, String validatorSpecPath) throws Exception {
		super(clientSpecpath, validatorSpecPath);
		logger.info("File Check constructor called");
	}

	@Override
	public Status service(ClientSpec clientSpec, JSONObject validatorSpec)
			throws JSchException, SftpException, Exception {

		JSONArray filesToCheck = (JSONArray) validatorSpec.get(CommonConstants.LIST);
		StringBuilder filesFound = new StringBuilder();
		StringBuilder filesNotFound = new StringBuilder();

		for (int file = 0; file < filesToCheck.size(); file++) {
			JSONObject obj = (JSONObject) filesToCheck.get(file);

			String endpointName = (String) obj.get(CommonConstants.ENDPOINT);
			String location = (String) obj.get(CommonConstants.LOCATION);
			String fileName = (String) obj.get(CommonConstants.FILENAME);
			String dateSuffix = (String) obj.get(CommonConstants.FEED_DATE);
			
			if (dateSuffix.isEmpty() || dateSuffix == null)
				dateSuffix = clientSpec.getDateFormat() + CommonConstants.COMMA + clientSpec.getDaysAgo();

			String endpointType = getTypeOfEndPoint(clientSpec, endpointName);
			location = replaceDynamicFolders(location, dateSuffix);
			fileName = replaceDateTag(fileName, dateSuffix);
			
			if (endpointType.equals("sftp"))
				fileName = makeRegex(fileName);

			logger.info("endpointName " + endpointName);
			logger.info("endpointType " + endpointType);
			HashMap<String, String> endPointResourceProperties = buildResourceProperties(endpointName, clientSpec,
					endpointType);
			IEndPointResource endPointResource = EndPointResourceFactory.getInstance().getEndPointResource(endpointType,
					endPointResourceProperties);
			boolean fileFound = endPointResource.checkFileExistence(location, fileName);
			logger.info(fileName + " " + fileFound);

			if (fileFound)
				filesFound.append(location).append(fileName).append(CommonConstants.NEWLINE);
			else
				filesNotFound.append(location).append(fileName).append(CommonConstants.NEWLINE);
		}

		logger.info("notFoundFiles size - > " + filesNotFound.length());
		Status status = null;
		if (filesNotFound.toString().isEmpty())
			status = new Status(Status.STATUS_SUCCESS, filesFound.toString());
		else
			status = new Status(Status.STATUS_ERROR, filesNotFound.toString());

		logger.info("File Check execute called");

		return status;
	}

}