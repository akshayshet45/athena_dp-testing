package com.rboomerang.custom.usap;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.TreeMap;
import java.util.TreeSet;

import com.opencsv.CSVReader;

public class ProductUrlsPost {
	private static TreeMap<String, String> Price2 = new TreeMap<String, String>();
	private static TreeMap<String, String> Price3 = new TreeMap<String, String>();

	private static void populatePrice2() {
		Price2.put("partstrain.com", "1");
		Price2.put("usautoparts.net", "2");
		Price2.put("canadapartsonline.com", "3");
		Price2.put("bongo.partstrain.com", "4");
		Price2.put("bongo.usautoparts.net", "5");

		return;
	}

	private static void populatePrice3() {
		Price3.put("discountbodyparts.com", "1");
		Price3.put("discountautomirrors.com", "2");
		Price3.put("discountcatalyticconverters.com", "3");
		Price3.put("discountexhaustsystems.com", "4");
		Price3.put("discountbrakes.com", "5");
		Price3.put("discountautoradiator.com", "6");
		Price3.put("discountcarlights.com", "7");
		Price3.put("discountautoshocks.com", "8");
		Price3.put("discounto2sensor.com", "9");
		Price3.put("discountfuelsystems.com", "10");
		Price3.put("discountairintake.com", "11");

		return;
	}

	private static String getHeader() {
		String header = "";
		header += "product_id";
		header += "domain_name";
		header += "channel_name";
		header += "product_url";
		header += "priority";

		return header;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) throws IOException {
		CSVReader reader = null;

		populatePrice2();
		populatePrice3();

		File file = new File("USAP_PRODUCT_URLS.txt");

		if (!file.exists()) {
			file.createNewFile();
		}

		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);

		reader = new CSVReader(new FileReader(args[0]), '|');
		String[] nextLine;
		String header = getHeader();
		bw.write(header);

		TreeSet price2Cache = new TreeSet();
		TreeSet price3Cache = new TreeSet();
		
		while ((nextLine = reader.readNext()) != null) {

			String skuNow = "", channelNameNow = "", line = "";
			boolean goodLine = true;
			int cols = 0;

			for (String token : nextLine) {
				cols++;

				if (cols == 1)
					skuNow = token;
				if (cols == 3)
					channelNameNow = token;

				line += token;
				line += (cols == 5) ? "\n" : "|";
			}

			if (((channelNameNow.equalsIgnoreCase("Price2") && price2Cache.contains(skuNow)) ||
					(channelNameNow.equalsIgnoreCase("Price3") && price3Cache.contains(skuNow))) || (skuNow.equals("product_id")))
				goodLine = false;
			if (goodLine) {
				bw.write(line);
				if (channelNameNow.equalsIgnoreCase("Price2")) price2Cache.add(skuNow);
				if (channelNameNow.equalsIgnoreCase("Price3")) price3Cache.add(skuNow);
			}
		}

		bw.close();
		reader.close();
	}
}