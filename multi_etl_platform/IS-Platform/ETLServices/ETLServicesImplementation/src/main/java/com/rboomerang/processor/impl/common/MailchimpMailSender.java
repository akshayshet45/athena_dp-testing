package com.rboomerang.processor.impl.common;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.service.framework.AbstractService;
import com.rboomerang.utils.AWSAccess;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.endpoint.EndPointResourceFactory;
import com.rboomerang.utils.endpoint.IEndPointResource;

public class MailchimpMailSender extends AbstractService {
	private static Logger logger = LoggerFactory.getLogger(MailchimpMailSender.class);;
	private static final Status SUCCESS = new Status(Status.STATUS_SUCCESS, "Execution MailchimpMailSender successful");

	private static final String pattern = "\"id\":\\s*\"(.*?)\"";

	private static final String API_KEY = "apiKey";
	private static final String CONTENT_TYPE = "type";
	private static final String RECIEPIENTS_LIST = "reciepientsList";
	private static final String EMAIL_SUBJECT = "subject";
	private static final String LIST_ID = "listId";
	private static final String IMAGE_SOURCE = "image_source";
	private static final String FILE_SOURCE = "file_source";
	private static final String FILENAME = "fileName";
	private static final String LOCATION = "location";
	

	private static StringBuilder finalHTMLContent = new StringBuilder();

	public MailchimpMailSender(String clientSpecpath, String processorSpecPath) throws Exception {
		super(clientSpecpath, processorSpecPath);
		logger.debug("MailchimpMailSender constructor called");
	}
	
	public MailchimpMailSender(String clientSpecpath, String processorSpecPath, String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeDate);
		logger.debug("MailchimpMailSender constructor called");
	}

	public MailchimpMailSender(String clientSpecpath, String processorSpecPath, RuntimeContext runtimeContext,
			String runtimeDate) throws Exception {
		super(clientSpecpath, processorSpecPath, runtimeContext, runtimeDate);
		logger.debug("MailchimpMailSender constructor called");
	}

	public Status service(ClientSpec clientSpec, JSONObject processorSpec)
			throws JSchException, SftpException, Exception {

		logger.info("MailchimpMailSender Processor execution started");
		Status status = null;
		IEndPointResource endPointResource = null;
		IEndPointResource fileEndPointResource = null;
		Set<String> imageURLs = null;
		try {

			JSONArray imageSourceDetailsArray = (JSONArray) processorSpec.get(IMAGE_SOURCE);
			JSONArray fileSourceDetailsArray = (JSONArray) processorSpec.get(FILE_SOURCE);

			int imageSourceCount = imageSourceDetailsArray.size();
			String clientName = clientSpec.getClient();
			String apiKey = (String) processorSpec.get(API_KEY);
			String contentType = (String) processorSpec.get(CONTENT_TYPE);
			String recipients = (String) processorSpec.get(RECIEPIENTS_LIST);
			String campaignSubject = (String) processorSpec.get(EMAIL_SUBJECT);
			String listID = (String) processorSpec.get(LIST_ID);
			String campaignId = null;

			logger.info("api key : " + apiKey);
			logger.info("list ID : " + listID);

			campaignSubject = replaceDateTag(campaignSubject, "yyyyMMdd,0");

			// get Server Region for REST Calls
			String region = apiKey.split("-")[1];
			logger.info("Server Region : " + region);

			// Create New Campaign
			if (listID != null && listID != "") {
				campaignId = MailchimpMailSender.createCampaign(apiKey, listID, campaignSubject, region, clientName);
			} else {
				// Create list with given reciepients
				String listId = MailchimpMailSender.createList(apiKey, region);
				logger.info("List ID : " + listId);

				// Add reciepients to newly created lists
				for (String user : recipients.split(",")) {
					logger.info("Member ID for " + user + " :"
							+ MailchimpMailSender.addUserToList(apiKey, listId, user, region));
				}
				campaignId = MailchimpMailSender.createCampaign(apiKey, listId, campaignSubject, region, clientName);
			}

			logger.info("Campaign ID : " + campaignId);
			logger.info("contentType : " + contentType);

			for (int index = 0; index < imageSourceCount; index++) {

				JSONObject endpointDetails = (JSONObject) imageSourceDetailsArray.get(index);
				String feedFileName = (String) endpointDetails.get(FILENAME);
				String destDateSuffix = (String) endpointDetails.get(CommonConstants.FEED_DATE);
				campaignSubject = replaceDateTag(campaignSubject, destDateSuffix);
				String location = (String) endpointDetails.get(LOCATION);
				feedFileName = replaceDateTag(feedFileName, destDateSuffix);
				location = replaceDynamicFolders(location, destDateSuffix);

				logger.info("Attachment file name is " + feedFileName);

				String bucketName = null;

				String endpointName = (String) endpointDetails.get(CommonConstants.ENDPOINTNAME);
				String endpointType = getTypeOfEndPoint(clientSpec, endpointName);
				HashMap<String, String> endPointResourceProperties = buildResourceProperties(endpointName, clientSpec,
						endpointType);
				endPointResource = EndPointResourceFactory.getInstance().getEndPointResource(endpointType,
						endPointResourceProperties);
				bucketName = endPointResourceProperties.get("S3_BUCKET_NAME");
				logger.info(bucketName);

				if (contentType.equalsIgnoreCase("inlineTableau")) {

					// Get Images from S3
					logger.info("getting images from S3");
					imageURLs = MailchimpMailSender.getImageURLsFromS3(bucketName, location);
					logger.info(imageURLs.toString());

					// Add content to Campaign
					logger.info("Generating HTML Template");
					String campaignContent = MailchimpMailSender.addImageContentToCampaign(apiKey, campaignId,
							imageURLs, region, bucketName, location);

					logger.info(campaignContent);

				} else if (contentType.equalsIgnoreCase("attachment")) {

					JSONObject fileEndpointDetails = (JSONObject) fileSourceDetailsArray.get(0);

					String fileName = (String) fileEndpointDetails.get(FILENAME);
					String dateSuffix = (String) fileEndpointDetails.get(CommonConstants.FEED_DATE);
					String fileLocation = (String) fileEndpointDetails.get(CommonConstants.LOCATION);

					fileName = replaceDateTag(fileName, dateSuffix);

					String fileEndpointName = (String) fileEndpointDetails.get(CommonConstants.ENDPOINTNAME);
					String fileEndpointType = getTypeOfEndPoint(clientSpec, fileEndpointName);
					HashMap<String, String> fileEndPointResourceProperties = buildResourceProperties(fileEndpointName,
							clientSpec, fileEndpointType);
					fileEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(fileEndpointType,
							fileEndPointResourceProperties);
					fileLocation = replaceDynamicFolders(fileLocation, destDateSuffix);

					// Add content to Campaign
					logger.info("Generating HTML Template");
					String campaignContent = MailchimpMailSender.addAttachmentContentToCampaign(apiKey, campaignId,
							region, fileEndPointResource.getFile(fileLocation, fileName),fileName);
					logger.info(campaignContent);
					fileEndPointResource.closeConnection();
				} else if (contentType.equalsIgnoreCase("inlineTableau_and_attachment")) {

					JSONObject fileEndpointDetails = (JSONObject) fileSourceDetailsArray.get(0);

					String fileName = (String) fileEndpointDetails.get(FILENAME);
					String dateSuffix = (String) fileEndpointDetails.get(CommonConstants.FEED_DATE);
					String fileLocation = (String) fileEndpointDetails.get(CommonConstants.LOCATION);

					feedFileName = replaceDateTag(fileName, dateSuffix);

					String fileEndpointName = (String) fileEndpointDetails.get(CommonConstants.ENDPOINTNAME);
					String fileEndpointType = getTypeOfEndPoint(clientSpec, fileEndpointName);
					HashMap<String, String> fileEndPointResourceProperties = buildResourceProperties(fileEndpointName,
							clientSpec, fileEndpointType);
					fileEndPointResource = EndPointResourceFactory.getInstance().getEndPointResource(fileEndpointType,
							fileEndPointResourceProperties);
					fileLocation = replaceDynamicFolders(fileLocation, destDateSuffix);

					// Get Images from S3
					logger.info("getting images from S3");
					imageURLs = MailchimpMailSender.getImageURLsFromS3(bucketName, location);
					logger.info(imageURLs.toString());

					// Add content to Campaign
					String campaignContent = null;
					logger.info("Generating HTML Template");
					if (index == (imageSourceCount - 1)) {
						campaignContent = MailchimpMailSender.addImageAndAttachmentContentToCampaign(apiKey, campaignId,
								imageURLs, region, bucketName, location,
								fileEndPointResource.getFile(fileLocation, feedFileName),feedFileName);
					} else {
						campaignContent = MailchimpMailSender.addImageAndAttachmentContentToCampaign(apiKey, campaignId,
								imageURLs, region, bucketName, location, null,fileName);
					}

					logger.info(campaignContent);
					fileEndPointResource.closeConnection();

				} else {
					logger.info("wrong value in 'type' in JSON :" + contentType);
					throw new Exception();
				}
			}

			// Send Campaign
			String campaignSend = MailchimpMailSender.sendCampaign(apiKey, campaignId, region);
			logger.info("campaign send response : " + campaignSend);
			logger.info("Emails sent check reports .. !!");
			endPointResource.closeConnection();
		} catch (Exception e) {
			status = new Status(Status.STATUS_ERROR, "Error");
			e.printStackTrace();
			if (fileEndPointResource != null)
				fileEndPointResource.closeConnection();
			if (endPointResource != null)
				endPointResource.closeConnection();
			return status;
		}
		return SUCCESS;
	}

	public static String uploadImageOrFile(String apiKey, String ImageName, String ImageData, String region)
			throws IOException {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("name", ImageName);
		jsonObject.put("file_data", ImageData);

		String response = MailchimpMailSender.getMailChimpRESTResponse(
				"https://" + region + ".api.mailchimp.com/3.0/file-manager/files", apiKey, jsonObject.toString(), true,
				"POST");
		logger.info(response);

		Pattern r = Pattern.compile("full_size_url\":\\s*\"(.*?)\"");
		Matcher m = r.matcher(response);
		if (m.find()) {
			if (m.group(1).startsWith("http") && !m.group(1).endsWith("subscriptions"))
				return m.group(1);
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	public static String addAttachmentContentToCampaign(String apiKey, String campaignId, String region,
			InputStream fileData,String fileName) throws IOException {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int read = 0;
		while ((read = fileData.read(buffer, 0, buffer.length)) != -1) {
			baos.write(buffer, 0, read);
		}
		baos.flush();

		logger.info(DatatypeConverter.printBase64Binary(baos.toByteArray()));

		String fileURL = MailchimpMailSender.uploadImageOrFile(apiKey, fileName,
				DatatypeConverter.printBase64Binary(baos.toByteArray()), region);

		logger.info(fileURL);

		StringBuilder htmlContent = new StringBuilder();
		htmlContent.append("Click ").append("<a href='").append(fileURL + "' download target='_self'>").append("here")
				.append("</a> to download attachment .<br><br>");

		logger.info(htmlContent.toString());

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("html", htmlContent.toString());
		String jsonResponce = MailchimpMailSender.getMailChimpRESTResponse(
				"https://" + region + ".api.mailchimp.com/3.0/campaigns/" + campaignId + "/content", apiKey,
				jsonObject.toString(), true, "PUT");

		return jsonResponce;
	}

	
	
	public static String addImageAndAttachmentContentToCampaign(String apiKey, String campaignId, Set<String> imageURLs,
			String region, String bucketName, String location, InputStream fileData,String fileName) throws IOException {
		int count = 1;
		StringBuilder htmlContent = new StringBuilder();
		InputStream is = null;
		
		
		BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIAIRURPZIFIVCEQ6XQ",
				"5zcIcclxgjuilye8pnpMmtZSg6jT3OrBVbYjG2v6");
		AmazonS3 s3Client = new AmazonS3Client(awsCreds);
		ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(bucketName);
		ObjectListing objectListing;
		Iterator<String> imageURLsIterator = imageURLs.iterator();

		do {
			objectListing = s3Client.listObjects(listObjectsRequest);
			for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
				if (objectSummary.getKey().contains(location) && objectSummary.getKey().endsWith(".png")
						&& objectSummary.getKey().contains("tableau")) {
					S3Object object = s3Client
							.getObject(new GetObjectRequest(objectSummary.getBucketName(), objectSummary.getKey()));
					is = object.getObjectContent();
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					byte[] buffer = new byte[1024];
					int read = 0;
					while ((read = is.read(buffer, 0, buffer.length)) != -1) {
						baos.write(buffer, 0, read);
					}
					baos.flush();

					logger.info(DatatypeConverter.printBase64Binary(baos.toByteArray()));

					String imageURL = MailchimpMailSender.uploadImageOrFile(apiKey, "tableau" + count++ + ".png",
							DatatypeConverter.printBase64Binary(baos.toByteArray()), region);

					if (imageURLsIterator.hasNext()) {
						htmlContent.append("<a href='").append(imageURLsIterator.next())
								.append("'><img src='" + imageURL).append("'/></a><br><br>");
					}
					baos.close();
				}
				System.gc();

			}
			listObjectsRequest.setMarker(objectListing.getNextMarker());
		} while (objectListing.isTruncated());

		finalHTMLContent.append(htmlContent.toString());
		logger.info(finalHTMLContent.toString());
		if (fileData != null) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int read = 0;
			while ((read = fileData.read(buffer, 0, buffer.length)) != -1) {
				baos.write(buffer, 0, read);
			}
			baos.flush();

			logger.info(DatatypeConverter.printBase64Binary(baos.toByteArray()));

			String fileURL = MailchimpMailSender.uploadImageOrFile(apiKey, fileName,
					DatatypeConverter.printBase64Binary(baos.toByteArray()), region);

			logger.info(fileURL);

			finalHTMLContent.append("Click ").append("<a href='").append(fileURL + "' download target='_self'>")
					.append("here").append("</a> to download attachment .<br><br>");
		}

		logger.info(finalHTMLContent.toString());

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("html", finalHTMLContent.toString());
		String jsonResponce = MailchimpMailSender.getMailChimpRESTResponse(
				"https://" + region + ".api.mailchimp.com/3.0/campaigns/" + campaignId + "/content", apiKey,
				jsonObject.toString(), true, "PUT");

		return jsonResponce;
	}

	public static Set<String> getImageURLsFromS3(String bucketName, String location) throws IOException {
		Set<String> imageURLs = new HashSet<String>();
		BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIAIRURPZIFIVCEQ6XQ",
				"5zcIcclxgjuilye8pnpMmtZSg6jT3OrBVbYjG2v6");
		AmazonS3 s3Client = new AmazonS3Client(awsCreds);
		ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(bucketName);
		ObjectListing objectListing;
		InputStream is = null;
		do {
			objectListing = s3Client.listObjects(listObjectsRequest);
			for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
				if (objectSummary.getKey().contains(location) && objectSummary.getKey().contains("manifest")
						&& objectSummary.getKey().endsWith(".txt")) {
					S3Object object = s3Client
							.getObject(new GetObjectRequest(objectSummary.getBucketName(), objectSummary.getKey()));
					is = object.getObjectContent();
					BufferedReader br = null;
					StringBuilder sb = new StringBuilder();

					String line;
					try {

						br = new BufferedReader(new InputStreamReader(is));
						while ((line = br.readLine()) != null) {
							sb.append(line);
						}

						Pattern r = Pattern.compile("href=\"(.*?)\"");
						Matcher m = r.matcher(sb.toString());
						while (m.find()) {
							if (m.group(1).startsWith("http") && !m.group(1).endsWith("subscriptions"))
								imageURLs.add(m.group(1));
						}

					} finally {
						br.close();
						is.close();
					}
					break;
				}
			}
			listObjectsRequest.setMarker(objectListing.getNextMarker());
		} while (objectListing.isTruncated());
		return imageURLs;
	}

	@SuppressWarnings("unchecked")
	public static String addImageContentToCampaign(String apiKey, String campaignId, Set<String> imageURLs,
			String region, String bucketName, String location) throws IOException {
		int count = 1;
		StringBuilder htmlContent = new StringBuilder();
		InputStream is = null;

		BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIAIRURPZIFIVCEQ6XQ",
				"5zcIcclxgjuilye8pnpMmtZSg6jT3OrBVbYjG2v6");
		AmazonS3 s3Client = new AmazonS3Client(awsCreds);
		ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(bucketName);
		ObjectListing objectListing;
		Iterator<String> imageURLsIterator = imageURLs.iterator();

		do {
			objectListing = s3Client.listObjects(listObjectsRequest);
			for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
				if (objectSummary.getKey().contains(location) && objectSummary.getKey().endsWith(".png")
						&& objectSummary.getKey().contains("tableau")) {
					S3Object object = s3Client
							.getObject(new GetObjectRequest(objectSummary.getBucketName(), objectSummary.getKey()));
					is = object.getObjectContent();
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					byte[] buffer = new byte[1024];
					int read = 0;
					while ((read = is.read(buffer, 0, buffer.length)) != -1) {
						baos.write(buffer, 0, read);
					}
					baos.flush();

					logger.info(DatatypeConverter.printBase64Binary(baos.toByteArray()));

					String imageURL = MailchimpMailSender.uploadImageOrFile(apiKey, "tableau" + count++ + ".png",
							DatatypeConverter.printBase64Binary(baos.toByteArray()), region);

					if (imageURLsIterator.hasNext()) {
						htmlContent.append("<a href='").append(imageURLsIterator.next())
								.append("'><img src='" + imageURL).append("'/></a><br><br>");
					}
					baos.close();
				}
				System.gc();

			}
			listObjectsRequest.setMarker(objectListing.getNextMarker());
		} while (objectListing.isTruncated());

		finalHTMLContent.append(htmlContent.toString());
		logger.info(finalHTMLContent.toString());

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("html", finalHTMLContent.toString());
		String jsonResponce = MailchimpMailSender.getMailChimpRESTResponse(
				"https://" + region + ".api.mailchimp.com/3.0/campaigns/" + campaignId + "/content", apiKey,
				jsonObject.toString(), true, "PUT");

		return jsonResponce;
	}

	public static String sendCampaign(String apiKey, String campaignId, String region) throws IOException {

		String jsonResponce = MailchimpMailSender.getMailChimpRESTResponse(
				"https://" + region + ".api.mailchimp.com/3.0/campaigns/" + campaignId + "/actions/send", apiKey, null,
				false, "POST");
		logger.info(jsonResponce);
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(jsonResponce);
		if (m.find()) {
			return m.group(1);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static String createCampaign(String apiKey, String listId, String emailSubject, String region,
			String clientName) throws IOException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();

		JSONObject jsonObject = new JSONObject();

		JSONObject reciepientsObject = new JSONObject();
		reciepientsObject.put("list_id", listId);

		jsonObject.put("recipients", reciepientsObject);
		jsonObject.put("type", "regular");

		JSONObject settingsObject = new JSONObject();
		settingsObject.put("subject_line", emailSubject);
		settingsObject.put("title", clientName + "_" + emailSubject + "_" + dateFormat.format(date));
		settingsObject.put("reply_to", "no-reply@boomerangcommerce.com");
		settingsObject.put("from_name", "Boomerang Commerce");

		jsonObject.put("settings", settingsObject);

		String jsonResponce = MailchimpMailSender.getMailChimpRESTResponse(
				"https://" + region + ".api.mailchimp.com/3.0/campaigns", apiKey, jsonObject.toJSONString(), true,
				"POST");

		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(jsonResponce);
		if (m.find()) {
			return m.group(1);
		}
		return null;
	}

	@SuppressWarnings({ "unchecked", "unused" })
	private static String createList(String apiKey, String region) throws IOException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("name", "staples_ca_" + dateFormat.format(new Date()));
		jsonObject.put("permission_reminder", "you are recieving this as you are subscribed to merchent alerts");
		jsonObject.put("email_type_option", true);

		JSONObject contactJSON = new JSONObject();
		contactJSON.put("company", "Boomerang Commerce");
		contactJSON.put("address1", "6th Floor ,H M Vihba Tower, Near Forum, Adugodi Road");
		contactJSON.put("address2", "Koramangala");
		contactJSON.put("city", "Bangalore");
		contactJSON.put("state", "KARNATAKA");
		contactJSON.put("zip", "560029");
		contactJSON.put("country", "INDIA");
		contactJSON.put("phone", "");

		jsonObject.put("contact", contactJSON);

		JSONObject campaignDefaultsJson = new JSONObject();
		campaignDefaultsJson.put("from_name", "Boomerang Commerce");
		campaignDefaultsJson.put("from_email", "merchant-alerts@boomerangcommerce.com");
		campaignDefaultsJson.put("subject", "subject_from_list");
		campaignDefaultsJson.put("language", "en");
		jsonObject.put("campaign_defaults", campaignDefaultsJson);

		String jsonResponce = MailchimpMailSender.getMailChimpRESTResponse(
				"https://" + region + ".api.mailchimp.com/3.0/lists", apiKey, jsonObject.toJSONString(), true, "POST");
		Pattern r = Pattern.compile(pattern);

		Matcher m = r.matcher(jsonResponce);

		if (m.find()) {
			return m.group(1);
		}
		return null;
	}

	@SuppressWarnings({ "unchecked", "unused" })
	private static String addUserToList(String apiKey, String listId, String user, String region) throws IOException {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("email_address", user);
		jsonObject.put("status", "subscribed");

		String jsonResponce = MailchimpMailSender.getMailChimpRESTResponse(
				"https://" + region + ".api.mailchimp.com/3.0/lists/" + listId + "/members", apiKey,
				jsonObject.toJSONString(), true, "POST");

		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(jsonResponce);
		if (m.find()) {
			return m.group(1);
		}
		return null;
	}
	
	private static String getMailChimpRESTResponse(String URL, String apiKey, String jsonPayload,
			boolean isPayloadRequired, String requestType) throws IOException {
		HttpURLConnection conn = null;
		BufferedReader br = null;
		String response = null;
		OutputStream os = null;

		try {
			URL url = new URL(URL);
			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod(requestType);
			conn.setRequestProperty("Content-Type", "application/json");

			String userpass = "api_key" + ":" + apiKey;
			String basicAuth = "Basic " + DatatypeConverter.printBase64Binary(userpass.getBytes("UTF-8"));
			conn.setRequestProperty("Authorization", basicAuth);

			if (isPayloadRequired) {
				os = conn.getOutputStream();
				os.write(jsonPayload.getBytes());
				os.flush();
			}

			br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			response = IOUtils.toString(br);
		} finally {
			conn.disconnect();
			br.close();
			if (os != null)
				os.close();
		}

		return response;
	}

}