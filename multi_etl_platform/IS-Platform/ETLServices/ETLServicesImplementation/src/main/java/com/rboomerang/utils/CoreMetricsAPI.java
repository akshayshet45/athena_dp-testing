package com.rboomerang.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rboomerang.processor.impl.common.CoreMetricsProcessor;

/**
 * IBM Digital Analytical Data Exchange API (CoreMetrics) Generic Utility
 *
 */
public class CoreMetricsAPI 
{
	private String clientID=null;
	private String userName=null;
	private String secret=null;
	private String format="CSV";
	private String ENDPOINT = CommonConstants.COREMETRICS_ENDPOINT;
	private JSONObject query=null;
	private String outputFileName=null;
	private static final int maxResults=20000;
	private static final String error="<?xml";
	private static Logger logger = LoggerFactory.getLogger(CoreMetricsProcessor.class);
	public CoreMetricsAPI(String clientID,String userName,String secret,JSONObject queryParams)
	{
		logger.info("CoreMetricsAPI utility constructor called");
		this.clientID=clientID;
		this.userName=userName;
		this.secret=secret;
		this.query=queryParams;
	}
	
	public String generateMetricReport() throws IOException, ParseException
	{
		logger.debug("Reading query parameters");
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyyMMdd");
		Date startDate=dateFormat.parse(query.get("startDate").toString());
		Date endDate=dateFormat.parse(query.get("endDate").toString());
		String viewID=query.get("viewID").toString();
		String dataType=query.get("dataType").toString();
		String reportName=query.get("reportName").toString();
		logger.debug("Query parameters are "+ query.toJSONString());
		String connectionSpec=String.format("%s/%s?clientId=%s&username=%s&format=%s&userAuthKey=%s&language=en_US&viewID=%s&startRow=",dataType,reportName,clientID,userName,format,secret,viewID);
		Calendar start = Calendar.getInstance();
		start.setTime(startDate);
		Calendar end = Calendar.getInstance();
		end.setTime(endDate);
		end.add(Calendar.DATE,1);
		File file=File.createTempFile("temp",".csv");
		PrintWriter pw=null;
		pw=new PrintWriter(new BufferedWriter(new FileWriter(file,true)));
		boolean check=true;
		for (Date date = start.getTime(); start.before(end); start.add(Calendar.DATE, 1), date = start.getTime())
		{
		    logger.debug("Running report for "+dateFormat.format(date));
		    String feedDate=dateFormat.format(date);
		    int startRow=1;
		    while(true)
		    {	
		    	logger.info("Generation API request...");
		    	//Query for all results incrementing startRow for respective feedDate
			    URL url=new URL(ENDPOINT+connectionSpec+new Integer(startRow).toString()+"&period_a=D"+feedDate);
			    URLConnection connection=url.openConnection();
			    InputStream in=connection.getInputStream();
			    logger.info("Writing the response to temp file"+file.getAbsolutePath());
			    Scanner inputRead=new Scanner(in);
			    String headerLine=inputRead.nextLine();
			    int count=0;
			    //If API request response with an exception like "DATA NOT AVAILABLE" don't write xml error to output feed file
			    if(!headerLine.startsWith(error))
			    {
			    	//Writing the header once for the file
			    	if(check)
			    	{
			    	 pw.println(headerLine+",feedDate");
			    	 check=false;
			    	}
			    	while(inputRead.hasNextLine())
			    	{
			    		pw.println(inputRead.nextLine()+","+feedDate);
			    		count++;
			    	}
			    }
			    else
			    {
			    	logger.warn("DATA is not available for "+feedDate+"Please check the response below \n"+headerLine);
			    }
			    inputRead.close();
			    logger.debug("writing the data for feed "+feedDate+" to file done!! "+count+" lines");
			    //Repeating API request for the next 20K lines as CoreMetrics API at most output 20K for a request
			    if(count!=maxResults)
			    	break;
			    else
			    	startRow += (maxResults+1);
		    }
		}
		outputFileName=file.getAbsolutePath();
		logger.info("Report has been written to "+outputFileName);
		pw.close();
		return outputFileName;
	}
}
