package com.rboomerang.common.framework;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RuntimeContext {
	private static Logger logger = LoggerFactory.getLogger(RuntimeContext.class);
	private boolean remoteMode = false;
	private String baseDirectory = null;

	public RuntimeContext() {
	}

	
	public RuntimeContext(boolean remoteMode, String baseDirectory) {
		this.remoteMode = remoteMode;
		this.baseDirectory = baseDirectory;
	}
	
	public boolean isRemoteMode() {
		return remoteMode;
	}

	public String getBaseDirectory() {
		return baseDirectory;
	}
	public String getETLServicesDirectory() {
		if (this.remoteMode) {
		return baseDirectory+"/etl-services";
		}
		else
		{
			return System.getProperty("user.dir");
		}
	}

	public String transformPath(String path) {
		if (this.remoteMode) {
			// Append the baseDirectory to the path and return
			logger.debug("running on remote machine ::: path "+this.baseDirectory + "/" + path);
			return this.baseDirectory + "/" + path;
		}
		else
		{
			logger.debug("Running tests locally ::: path "+System.getProperty("user.dir")+"/"+path);
			return System.getProperty("user.dir")+"/"+path;
		}
		
//		else {
//			// Extract the file name from the path and return
//			return extractLocalPath(path);
//		}
	}

	@SuppressWarnings("unused")
	private static String extractLocalPath(String path) {
		String [] splitString = path.split("/");			
		String localPath = splitString[splitString.length - 1]; // take the last string
		
		return localPath;
	}
}
