package com.rboomerang.common.framework.javabeans;

public class S3Spec {
	private String bucketName;
	private String bucketNode;
	
	public String getBucketName() {
		return bucketName;
	}
	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}
	public String getBucketRegion() {
		return bucketNode;
	}
	public void setBucketRegion(String bucketNode) {
		this.bucketNode = bucketNode;
	}
	
}
