package com.rboomerang.notification.implementation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rboomerang.notification.Notifier;
import com.rboomerang.notification.Notifies;

public class NotifierImpl implements Notifier {

	private List<Notifies> successNotifier;
	private List<Notifies> failureNotifier;
	private static Logger logger = LoggerFactory.getLogger(NotifierImpl.class);

	public NotifierImpl() {
		successNotifier = new ArrayList<Notifies>();
		failureNotifier = new ArrayList<Notifies>();
		logger.debug("Constructor initialization complete");
	}

	@Override
	public void registerSuccessNotifier(Notifies notifies) {
		logger.debug("added new success notify :: " + notifies.getName());
		successNotifier.add(notifies);
	}

	@Override
	public void registerFailureNotifier(Notifies notifies) {
		logger.debug("added new failure notify :: " + notifies.getName());
		failureNotifier.add(notifies);
	}

	@Override
	public void notifySuccess(List<String> listNotifies, String subject,
			Map<String, String> body) throws IOException {
		if (listNotifies == null || listNotifies.size() == 0) {
			logger.info("No success notify in service/job spec");
		} else {
			for (Notifies notify : successNotifier) {
				if (listNotifies.contains(notify.getName())) {
					logger.debug(notify.getName()
							+ " is configured in service/job spec");
					notify.publish(subject, body);
				} else {
					logger.debug(notify.getName()
							+ " is not configured in service/job spec");
				}
			}
		}
	}

	@Override
	public void notifyFailure(List<String> listNotifies, String subject,
			Map<String, String> body) throws IOException {
		if (listNotifies == null || listNotifies.size() == 0) {
			logger.info("No failure notify in service/job spec");
		} else {
			for (Notifies notify : failureNotifier) {
				if (listNotifies.contains(notify.getName())) {
					logger.debug(notify.getName()
							+ " is configured in service/job spec");
					notify.publish(subject, body);
				} else {
					logger.debug(notify.getName()
							+ " is not configured in service/job spec");
				}
			}
		}
	}

}
