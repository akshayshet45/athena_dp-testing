package com.rboomerang.common.framework.javabeans;

public class AzkabanSpec {
	
	private String azkaban_host;
	private String azkaban_user;
	private String azkaban_pass;
	public String getAzkaban_host() {
		return azkaban_host;
	}
	public void setAzkaban_host(String azkaban_host) {
		this.azkaban_host = azkaban_host;
	}
	public String getAzkaban_user() {
		return azkaban_user;
	}
	public void setAzkaban_user(String azkaban_user) {
		this.azkaban_user = azkaban_user;
	}
	public String getAzkaban_pass() {
		return azkaban_pass;
	}
	public void setAzkaban_pass(String azkaban_pass) {
		this.azkaban_pass = azkaban_pass;
	}
	
	

}
