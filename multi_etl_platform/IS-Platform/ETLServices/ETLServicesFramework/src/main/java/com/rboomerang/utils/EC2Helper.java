package com.rboomerang.utils;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.StartInstancesRequest;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import com.amazonaws.services.ec2.model.Tag;

public class EC2Helper {
	private final Logger log = LoggerFactory.getLogger(EC2Helper.class);
	private AmazonEC2Client amazonEC2Client;
	private String InstanceID;
	private static final int WAIT_FOR_TRANSITION_INTERVAL = 4000;
	private static final int WAIT_FOR_INITIALIZATION = 7000;

	public EC2Helper(String instanceID, String ec2Endpoint) throws IOException {
		log.debug("Instance Id : " + instanceID);
		log.debug("Instance Endpoint : " + ec2Endpoint);
		AWSAccess awsAccess = new AWSAccess();
		this.amazonEC2Client = new AmazonEC2Client(awsAccess.getAWSAccess());
		this.amazonEC2Client.setEndpoint("https://ec2." + ec2Endpoint + ".amazonaws.com");
		this.InstanceID = instanceID;

		log.info("EC2 object initialized");
	}

	public String describeTag(String Key) throws InterruptedException {
		String value = null;

		List<String> instanceIds = new LinkedList<String>();
		instanceIds.add(this.InstanceID);
		DescribeInstancesRequest request = new DescribeInstancesRequest();
		request.setInstanceIds(instanceIds);
		DescribeInstancesResult result = null;
		boolean done = false;
		int count = 1;
		while (!done) {
			try {
				result = this.amazonEC2Client.describeInstances(request);
				done = true;
			} catch (AmazonServiceException e) {
				e.printStackTrace();
				log.debug("retry : " + count);
				count++;
				if (count < 10 && e.getMessage().contains("Request limit exceeded"))
					Thread.sleep(WAIT_FOR_TRANSITION_INTERVAL * count);
				else
					throw e;
			}
		}
		List<Reservation> reservations = result.getReservations();
		List<Tag> tags = null;
		List<Instance> instances;
		for (Reservation res : reservations) {
			instances = res.getInstances();
			for (Instance instance : instances) {
				log.debug("PublicIP from " + instance.getImageId() + " is " + instance.getPublicIpAddress() + " state "
						+ instance.getState() + " state code : " + instance.getState().getCode());
				tags = instance.getTags();

			}

		}
		if (tags.isEmpty()) {
			log.debug("unable to detect tags!");
			return null;
		} else {
			log.debug("Azkaban Machine tags --> " + tags);
			for (Tag tag : tags) {
				if (tag.getKey().equals("env"))
					return tag.getValue();
			}
		}
		return value;
	}

	private Integer describeState() throws Exception {
		List<String> instanceIds = new LinkedList<String>();
		instanceIds.add(this.InstanceID);
		DescribeInstancesRequest request = new DescribeInstancesRequest();
		request.setInstanceIds(instanceIds);
		DescribeInstancesResult result = null;

		boolean done = false;
		int count = 1;
		while (!done) {
			try {
				result = this.amazonEC2Client.describeInstances(request);
				done = true;
			} catch (AmazonServiceException e) {
				log.error("exception at retry " + count + " ", e);
				log.debug("retry : " + count);
				count++;
				if (count < 10 && e.getMessage().contains("Request limit exceeded"))
					Thread.sleep(WAIT_FOR_TRANSITION_INTERVAL * count);
				else
					throw e;
			}
		}

		List<Reservation> reservations = result.getReservations();

		List<Instance> instances;
		for (Reservation res : reservations) {
			instances = res.getInstances();
			for (Instance ins : instances) {
				log.debug("Instance state " + ins.getState());
				return ins.getState().getCode();
			}
		}
		throw new Exception("No running Instance with instance Id : " + this.InstanceID);
	}

	public String getIPAddress() throws Exception {
		List<String> instanceIds = new LinkedList<String>();
		instanceIds.add(this.InstanceID);
		DescribeInstancesRequest request = new DescribeInstancesRequest();
		request.setInstanceIds(instanceIds);
		DescribeInstancesResult result = null;
		boolean done = false;
		int count = 1;
		while (!done) {
			try {
				result = this.amazonEC2Client.describeInstances(request);
				done = true;
			} catch (AmazonServiceException e) {
				log.error("exception at retry " + count + " ", e);
				log.debug("retry : " + count);
				count++;
				if (count < 10 && e.getMessage().contains("Request limit exceeded"))
					Thread.sleep(WAIT_FOR_TRANSITION_INTERVAL * count);
				else
					throw e;
			}
		}
		List<Reservation> reservations = result.getReservations();

		List<Instance> instances;
		for (Reservation res : reservations) {
			instances = res.getInstances();
			for (Instance ins : instances) {

				if (ins.getState().getCode() == 32 || ins.getState().getCode() == 64 || ins.getState().getCode() == 80
						|| ins.getState().getCode() == 0) {
					log.error("Remote machine status is " + ins.getState().getCode());
					this.startRemoteMachine();
					boolean doneCheck = false;
					int countcheck = 1;
					while (!doneCheck) {
						try {
							result = this.amazonEC2Client.describeInstances(request);
							doneCheck = true;
						} catch (AmazonServiceException e) {
							log.error("exception at retry " + countcheck + " ", e);
							log.debug("retry : " + countcheck);
							countcheck++;
							if (countcheck < 10 && e.getMessage().contains("Request limit exceeded"))
								Thread.sleep(WAIT_FOR_TRANSITION_INTERVAL * countcheck);
							else
								throw e;
						}
					}
					List<Reservation> newReservations = result.getReservations();
					List<Instance> newInstances;
					for (Reservation reservation : newReservations) {
						newInstances = reservation.getInstances();
						for (Instance instance : newInstances) {
							log.debug("PublicIP from " + instance.getImageId() + " is " + instance.getPublicIpAddress()
									+ " state " + instance.getState() + " state code : "
									+ instance.getState().getCode());

							return instance.getPublicIpAddress();

						}
					}
				} else if (ins.getState().getCode() == 48) {
					log.error(this.InstanceID + " Machine state is terminated");
					throw new Exception(this.InstanceID + " Machine state is terminated");
				} else {
					log.debug("PublicIP from " + ins.getImageId() + " is " + ins.getPublicIpAddress() + " and state "
							+ ins.getState() + " state code : " + ins.getState().getCode());
					return ins.getPublicIpAddress();
				}
			}
		}
		throw new Exception("No running Instance with instance Id : " + this.InstanceID);
	}

	public void startRemoteMachine() throws Exception {
		Integer currentState = this.describeState();
		if (currentState == 16) {
			log.debug("machine is already running");
			return;
		}
		StartInstancesRequest startRequest = new StartInstancesRequest().withInstanceIds(this.InstanceID);
		this.amazonEC2Client.startInstances(startRequest);
		log.info("Starting instance '" + this.InstanceID + "':");
		waitForTransitionCompletion(16, currentState);

	}

	public void stopRemoteMachine() throws Exception {
		Integer currentState = this.describeState();
		if (currentState == 80 || currentState == 32 || currentState == 64) {
			log.debug("machine is already in following state : " + currentState);
			return;
		}
		StopInstancesRequest stopRequest = new StopInstancesRequest().withInstanceIds(this.InstanceID).withForce(true);
		this.amazonEC2Client.stopInstances(stopRequest);
		log.info("Stoping instance '" + this.InstanceID + "':");
		waitForTransitionCompletion(80, currentState);

	}

	private void waitForTransitionCompletion(Integer desiredState, Integer previousState) throws Exception {
		int waitCount = 20;
		Integer currentState;
		while (waitCount > 0) {
			currentState = this.describeState();
			if (desiredState == currentState) {
				log.debug(this.InstanceID + "  machine state : " + currentState);
				Thread.sleep(WAIT_FOR_INITIALIZATION);
				return;
			}
			Thread.sleep(WAIT_FOR_TRANSITION_INTERVAL);
			waitCount--;
		}
	}

}
