package com.rboomerang.common.framework;

import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rboomerang.common.framework.javabeans.AzkabanSpec;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.common.framework.javabeans.DbSpec;
import com.rboomerang.common.framework.javabeans.S3Spec;
import com.rboomerang.common.framework.javabeans.SftpSpec;
import com.rboomerang.notification.Notifier;
import com.rboomerang.notification.Notifies;
import com.rboomerang.notification.implementation.NotifierImpl;
import com.rboomerang.notification.implementation.NotifyMailImpl;
import com.rboomerang.notification.implementation.NotifySnsImpl;
import com.rboomerang.notification.implementation.NotifySqsImpl;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.RuntimeParametersUtility;

public class ClientSpecInitialization implements Closeable {
	private static Logger logger = LoggerFactory
			.getLogger(ClientSpecInitialization.class);

	private String clientSpecPath;
	private FileReader fileReader = null;
	private JSONParser parser = null;
	private JSONObject jsonObj = null;
	private ClientSpec clientSpec = null;
	private Notifier notifier = null;

	public Notifier getNotifier() {
		return notifier;
	}

	public ClientSpecInitialization(String clientSpecPath) throws IOException,
			ParseException, FileNotFoundException {
		this.clientSpecPath = clientSpecPath;

		initialzeClientSpec();
		logger.info("ClientSpecInitialization constructor called");
	}

	public ClientSpec getClientSpecObject() {
		logger.info("ClientSpecInitialization ClientSpec called");
		return clientSpec;
	}

	private SftpSpec createSftpAuth(JSONObject properties)
			throws ParseException {
		SftpSpec auth = new SftpSpec();

		auth.setHost((String) properties.get(ClientSpec.HOST));
		auth.setUname((String) properties.get(ClientSpec.USER));
		auth.setPasswd((String) properties.get(ClientSpec.PASS));

		logger.info("ClientSpecInitialization createAzkabanAuth called");
		return auth;
	}

	private void initialzeClientSpec() throws IOException, ParseException,
			FileNotFoundException {
		fileReader = new FileReader(this.clientSpecPath);
		parser = new JSONParser();
		Object jsonElement = parser.parse(fileReader);
		jsonObj = (JSONObject) jsonElement;
		jsonObj = (JSONObject) parser.parse(RuntimeParametersUtility
				.replaceRuntimeParameters(jsonObj.toString(),
						(String) jsonObj.get(ClientSpec.CLIENT)));

		logger.debug("client-spec path provided : " + clientSpecPath);
		// azkabanEnvTagPropertiesFile = new File(BASE_DIRECTORY+"/");

		JSONArray array = (JSONArray) jsonObj.get(ClientSpec.ENDPOINTS);

		clientSpec = new ClientSpec();
		clientSpec.setAzkabanName(((String) jsonObj
				.get(ClientSpec.AZKABAN_NAME)));
		for (int i = 0; i < array.size(); i++) {
			JSONObject obj = (JSONObject) array.get(i);
			String switchString = (String) obj.get(ClientSpec.ENDPOINT_TYPE);
			String name = (String) obj.get(ClientSpec.ENDPOINT_NAME);
			JSONObject properties = (JSONObject) obj
					.get(ClientSpec.ENDPOINT_PROPERTIES);
			switch (switchString) {
			case ClientSpec.ENDPOINT_AZKABAN:
				AzkabanSpec aauth = createAzkabanAuth(properties);
				clientSpec.setAzkabanAuthList(name, aauth);
				break;
			case ClientSpec.ENDPOINT_SFTP:
				SftpSpec sauth = createSftpAuth(properties);
				clientSpec.setSftpAuthList(name, sauth);
				break;
			case ClientSpec.ENDPOINT_REDSHIFT:
				DbSpec rauth = createDBAuth(properties);
				clientSpec.setRedshiftAuthList(name, rauth);
				break;
			case ClientSpec.ENDPOINT_MYSQL:
				DbSpec mauth = createDBAuth(properties);
				clientSpec.setMysqlAuthList(name, mauth);
				break;
			case ClientSpec.ENDPOINT_S3:
				S3Spec s3auth = createS3Auth(properties);
				clientSpec.setS3AuthList(name, s3auth);
				break;
			}
		}
		clientSpec.setFeedMetadata((String) jsonObj
				.get(ClientSpec.FEED_METADATA));
		clientSpec.setErrorEmail((String) jsonObj.get(ClientSpec.ERROR_EMAIL));
		clientSpec.setNotificationEmail((String) jsonObj
				.get(ClientSpec.NOTIFICATION_EMAIL));
		clientSpec.setPagerDuty((String) jsonObj.get(ClientSpec.PAGERDUTY_KEY));
		clientSpec.setClient((String) jsonObj.get(ClientSpec.CLIENT));

		clientSpec.setRemoteMachine((String) jsonObj
				.get(ClientSpec.REMOTE_MACHINE));
		clientSpec.setRemoteMachineEndpoint((String) jsonObj
				.get(ClientSpec.REMOTE_MACHINE_ENDPOINT));
		clientSpec.setAutoScalingGroupName((String) jsonObj
				.get(ClientSpec.AUTO_SCALING_GROUP_NAME));
		clientSpec.setAutoScalingGroupEndpoint((String) jsonObj
				.get(ClientSpec.AUTO_SCALING_GROUP_ENDPOINT));

		String feedDate = (String) jsonObj.get(ClientSpec.DATE);
		String format = feedDate.split(",")[0];
		String daysAgo = feedDate.split(",")[1];
		clientSpec.setDateFormat(format);
		clientSpec.setDaysAgo(daysAgo);

		notifier = new NotifierImpl();
		logger.info("Notifier object initialization");
		registerSuccessNotifiers();
		logger.info("Success notifiers registered");
		registerFailureNotifiers();
		logger.info("Failure notifiers registered");

		logger.info("ClientSpecInitialization initialzeClientSpec called");
	}

	private void registerSuccessNotifiers() {
		if (jsonObj.get("successEndpoints") != null) {
			logger.info("successEndpoints are configured");
			JSONArray array = (JSONArray) jsonObj.get("successEndpoints");
			for (Object object : array) {
				JSONObject obJsonObject = (JSONObject) object;
				JSONObject jsonProperties = null;
				Notifies notifies = null;

				switch (obJsonObject.get("type").toString()) {
				case CommonConstants.SQS:
					jsonProperties = (JSONObject) obJsonObject
							.get("properties");
					logger.info("successEndpoints SQS registered :: "
							+ obJsonObject.get("name").toString());
					notifies = new NotifySqsImpl(obJsonObject.get("name")
							.toString(), jsonProperties.get("name").toString(),
							jsonProperties.get("region").toString());
					notifier.registerSuccessNotifier(notifies);
					break;
				case CommonConstants.SNS:
					jsonProperties = (JSONObject) obJsonObject
							.get("properties");
					logger.info("successEndpoints SNS registered :: "
							+ obJsonObject.get("name").toString());
					notifies = new NotifySnsImpl(obJsonObject.get("name")
							.toString(), jsonProperties.get("name").toString(),
							jsonProperties.get("region").toString());
					notifier.registerSuccessNotifier(notifies);
					break;
				case CommonConstants.EMAIL:
					jsonProperties = (JSONObject) obJsonObject
							.get("properties");
					logger.info("successEndpoints EMAIL registered :: "
							+ obJsonObject.get("name").toString());
					notifies = new NotifyMailImpl(obJsonObject.get("name")
							.toString(), jsonProperties.get("mailList")
							.toString());
					notifier.registerSuccessNotifier(notifies);
					break;
				}
			}
		} else {
			logger.info("successEndpoints aren't configured");
		}
	}

	private void registerFailureNotifiers() {
		if (jsonObj.get("failureEndpoints") != null) {
			logger.info("failureEndpoints are configured");
			JSONArray array = (JSONArray) jsonObj.get("failureEndpoints");
			for (Object object : array) {
				JSONObject obJsonObject = (JSONObject) object;
				JSONObject jsonProperties = null;
				Notifies notifies = null;

				switch (obJsonObject.get("type").toString()) {
				case CommonConstants.SQS:
					jsonProperties = (JSONObject) obJsonObject
							.get("properties");
					logger.info("failureEndpoints SQS registered :: "
							+ obJsonObject.get("name").toString());
					notifies = new NotifySqsImpl(obJsonObject.get("name")
							.toString(), jsonProperties.get("name").toString(),
							jsonProperties.get("region").toString());
					notifier.registerFailureNotifier(notifies);
					break;
				case CommonConstants.SNS:
					jsonProperties = (JSONObject) obJsonObject
							.get("properties");
					logger.info("failureEndpoints SNS registered :: "
							+ obJsonObject.get("name").toString());
					notifies = new NotifySnsImpl(obJsonObject.get("name")
							.toString(), jsonProperties.get("name").toString(),
							jsonProperties.get("region").toString());
					notifier.registerFailureNotifier(notifies);
					break;
				case CommonConstants.EMAIL:
					jsonProperties = (JSONObject) obJsonObject
							.get("properties");
					logger.info("failureEndpoints EMAIL registered :: "
							+ obJsonObject.get("name").toString());
					notifies = new NotifyMailImpl(obJsonObject.get("name")
							.toString(), jsonProperties.get("mailList")
							.toString());
					notifier.registerFailureNotifier(notifies);
					break;
				case CommonConstants.PAGER_DUTY:
					jsonProperties = (JSONObject) obJsonObject
							.get("properties");
					logger.info("failureEndpoints PAGERDUTY registered :: "
							+ obJsonObject.get("name").toString());
					notifies = new NotifyMailImpl(obJsonObject.get("name")
							.toString(), jsonProperties.get("key").toString());
					notifier.registerFailureNotifier(notifies);
					break;
				}
			}
		} else {
			logger.info("failureEndpoints aren't configured");
		}
	}

	private AzkabanSpec createAzkabanAuth(JSONObject properties) {
		AzkabanSpec aauth = new AzkabanSpec();
		aauth.setAzkaban_host((String) properties.get(ClientSpec.AZKABAN_HOST));
		aauth.setAzkaban_user((String) properties.get(ClientSpec.AZKABAN_USER));
		aauth.setAzkaban_pass((String) properties.get(ClientSpec.AZKABAN_PASS));

		return aauth;
	}

	private S3Spec createS3Auth(JSONObject properties) {
		S3Spec s3auth = new S3Spec();

		s3auth.setBucketName((String) properties.get(ClientSpec.S3_BUCKET_NAME));
		s3auth.setBucketRegion((String) properties
				.get(ClientSpec.S3_BUCKET_REGION));

		logger.info("ClientSpecInitialization createS3Auth called");
		return s3auth;
	}

	private DbSpec createDBAuth(JSONObject properties) throws ParseException {
		DbSpec dbAuth = new DbSpec();
		dbAuth.setDb((String) properties.get(ClientSpec.DB));
		dbAuth.setHost((String) properties.get(ClientSpec.HOST));
		dbAuth.setPasswd((String) properties.get(ClientSpec.PASS));
		dbAuth.setUname((String) properties.get(ClientSpec.USER));

		logger.info("ClientSpecInitialization createDBAuth called");
		return dbAuth;
	}

	@Override
	public void close() throws IOException {
		if (fileReader != null)
			fileReader.close();
		logger.info("ClientSpecInitialization close called");
	}

}
