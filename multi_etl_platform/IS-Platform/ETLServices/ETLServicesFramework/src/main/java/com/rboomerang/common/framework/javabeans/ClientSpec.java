package com.rboomerang.common.framework.javabeans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ClientSpec {

	public static final String DB = "db";
	public static final String SFTP = "sftp";
	public static final String S3 = "s3";
	public static final String SNS = "sns";
	public static final String SQS = "sqs";
	public static final String MAIL = "email";
	public static final String PAGER_DUTY = "pagerDuty";
	
	public static final String FEED_METADATA = "feedMetadata";
	public static final String DATE = "date";
	public static final String CLIENT = "client";
	public static final String REMOTE_MACHINE = "remoteMachine";
	public static final String REMOTE_MACHINE_ENDPOINT = "remoteMachineEndpoint";
	public static final String AUTO_SCALING_GROUP_NAME = "autoScalingGroupName";
	public static final String AUTO_SCALING_GROUP_ENDPOINT="autoScalingGroupEndpoint";
	public static final String PAGERDUTY_KEY = "pagerdutyKey";
	public static final String NOTIFICATION_EMAIL = "notificationEmail";
	public static final String ERROR_EMAIL = "errorEmail";
	public static final String ENDPOINT_AZKABAN = "azkaban";
	public static final String ENDPOINT_MYSQL = "mysql";
	public static final String ENDPOINT_REDSHIFT = "redshift";
	public static final String AZKABAN_NAME = "azkaban_name";
	public static final String AZKABAN_HOST = "host";
	public static final String AZKABAN_USER = "username";
	public static final String AZKABAN_PASS = "password";
	public static final String ENDPOINT_SFTP = "sftp";
	public static final String ENDPOINT_PROPERTIES = "properties";
	public static final String ENDPOINT_NAME = "name";
	public static final String ENDPOINT_TYPE = "type";
	public static final String ENDPOINTS = "endpoints";

	public static final String PASS = "pass";
	public static final String USER = "user";
	public static final String HOST = "host";

	public static final String S3_BUCKET_NAME = "bucket_name";
	public static final String S3_BUCKET_REGION = "bucket_region";

	public static final String DATE_FORMAT_REGEX = "{{feed_date}}";
	public static final String CLIENT_TAG_REGEX = "{{clientTag}}";
	public static final String ENDPOINT_S3 = "s3";
	
	public static final String FAILURE_ENDPOINTS="failureEndpoints";
	public static final String SUCCESS_ENDPOINTS="successEndpoints";

	private HashMap<String, AzkabanSpec> azkabanAuthList = new HashMap<String, AzkabanSpec>();
	private HashMap<String, SftpSpec> sftpAuthList = new HashMap<String, SftpSpec>();
	private HashMap<String, DbSpec> redshiftAuthList = new HashMap<String, DbSpec>();
	private HashMap<String, DbSpec> mysqlAuthList = new HashMap<String, DbSpec>();
	private HashMap<String, S3Spec> s3AuthList = new HashMap<String, S3Spec>();

	private String pagerDuty;
	private String azkabanName;
	private String feedMetadata;
	private List<String> notificationEmail = new ArrayList<String>();
	private List<String> errorEmail = new ArrayList<String>();
	private String client;
	private String remoteMachine;
	private String remoteMachineEndpoint;

	private String autoScalingGroupName;
	private String autoScalingGroupEndpoint;
	

	public String getAzkabanName() {
		return azkabanName;
	}
	
	public void setAzkabanName(String azkabanName) {
		this.azkabanName = azkabanName;
	}
	
	
	public String getRemoteMachine() {
		return remoteMachine;
	}

	public void setRemoteMachine(String remoteMachine) {
		this.remoteMachine = remoteMachine;
	}
	public String getRemoteMachineEndpoint() {
		return remoteMachineEndpoint;
	}

	public void setRemoteMachineEndpoint(String remoteMachineEndpoint) {
		this.remoteMachineEndpoint = remoteMachineEndpoint;
	}

	
	public String getAutoScalingGroupName() {
		return autoScalingGroupName;
	}

	public void setAutoScalingGroupName(String autoScalingGroupName) {
		this.autoScalingGroupName = autoScalingGroupName;
	}
	public String getAutoScalingGroupEndpoint() {
		return autoScalingGroupEndpoint;
	}

	public void setAutoScalingGroupEndpoint(String autoScalingGroupEndpoint) {
		this.autoScalingGroupEndpoint = autoScalingGroupEndpoint;
	}


	private String date;
	private String dateFormat;
	private String daysAgo;

	public String getClient() {
		return client;
	}
	
	public HashMap<String, AzkabanSpec> getAzkabanAuthList() {
		return azkabanAuthList;
	}

	public void setAzkabanAuthList(String key, AzkabanSpec value) {
		azkabanAuthList.put(key, value);
	}

	public HashMap<String, SftpSpec> getSftpAuthList() {
		return sftpAuthList;
	}

	public void setSftpAuthList(HashMap<String, SftpSpec> sftpAuthList) {
		this.sftpAuthList = sftpAuthList;
	}

	public void setSftpAuthList(String key, SftpSpec value) {
		sftpAuthList.put(key, value);
	}

	public HashMap<String, DbSpec> getRedshiftAuthList() {
		return redshiftAuthList;
	}

	public void setRedshiftAuthList(HashMap<String, DbSpec> redshiftAuthList) {
		this.redshiftAuthList = redshiftAuthList;
	}

	public void setRedshiftAuthList(String key, DbSpec value) {
		redshiftAuthList.put(key, value);
	}

	public HashMap<String, DbSpec> getMysqlAuthList() {
		return mysqlAuthList;
	}

	public void setMysqlAuthList(HashMap<String, DbSpec> mysqlAuthList) {
		this.mysqlAuthList = mysqlAuthList;
	}

	public void setMysqlAuthList(String key, DbSpec value) {
		mysqlAuthList.put(key, value);
	}

	public String getFeedMetadata() {
		return feedMetadata;
	}

	public void setFeedMetadata(String feedMetadata) {
		this.feedMetadata = feedMetadata;
	}
	
	public String getPagerDuty() {
		return pagerDuty;
	}

	public void setPagerDuty(String pagerDuty) {
		this.pagerDuty = pagerDuty;
	}

	public List<String> getNotificationEmail() {
		return notificationEmail;
	}

	public void setNotificationEmail(List<String> notificationEmail) {
		this.notificationEmail = notificationEmail;
	}

	public void setNotificationEmail(String notificationEmail) {
		this.notificationEmail = Arrays.asList(notificationEmail
				.split("\\s*,\\s*"));
	}

	public List<String> getErrorEmail() {
		return errorEmail;
	}

	public void setErrorEmail(List<String> errorEmail) {
		this.errorEmail = errorEmail;
	}

	public void setErrorEmail(String errorEmail) {
		this.errorEmail = Arrays.asList(errorEmail.split("\\s*,\\s*"));
		;
	}

	public void setClient(String client) {
		// TODO Auto-generated method stub
		this.client = client;

	}
	
	public String getDaysAgo() {
		return daysAgo;
	}
	
	public void setDaysAgo(String daysAgo) {
		this.daysAgo = daysAgo;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the s3AuthList
	 */
	public HashMap<String, S3Spec> getS3AuthList() {
		return s3AuthList;
	}

	/**
	 * @param s3AuthList
	 *            the s3AuthList to set
	 */
	public void setS3AuthList(HashMap<String, S3Spec> s3AuthList) {
		this.s3AuthList = s3AuthList;
	}

	/**
	 * @param key as name and S3Spec object as value
	 *            the s3AuthList to set
	 */
	public void setS3AuthList(String key, S3Spec value) {
		this.s3AuthList.put(key, value);
	}

}
