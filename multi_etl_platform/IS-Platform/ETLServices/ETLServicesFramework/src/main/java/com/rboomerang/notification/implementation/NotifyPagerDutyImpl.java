package com.rboomerang.notification.implementation;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rboomerang.common.framework.Status;
import com.rboomerang.notification.Notifies;
import com.rboomerang.utils.SendEmail;

public class NotifyPagerDutyImpl implements Notifies {

	private String implName;
	private String pagerdutyKey;
	private static Logger logger = LoggerFactory.getLogger(NotifyPagerDutyImpl.class);
	
	public NotifyPagerDutyImpl(String implName, String pagerdutyKey) {
		this.implName = implName;
		this.pagerdutyKey = pagerdutyKey;
	}

	@Override
	public String getName() {
		logger.info("Pagerduty custom name :: " + implName);
		return implName;
	}

	@Override
	public Status publish(String suject, Map<String, String> body) {
		SendEmail.sendPagerDuty(pagerdutyKey, suject, body);
		logger.info("Subject passed to PD :: " + suject);
		logger.info("Body passed to PD :: " + body.toString());
		return new Status(Status.STATUS_SUCCESS, "message sent successfully");
	}

}
