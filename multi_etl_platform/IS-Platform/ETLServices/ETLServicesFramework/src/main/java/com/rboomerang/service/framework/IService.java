package com.rboomerang.service.framework;

import java.io.Closeable;
import java.util.HashMap;

import org.json.simple.JSONObject;

import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.common.framework.javabeans.MessageContentSpec;

public interface IService extends Closeable {
	public JSONObject getServiceSpec();
	public ClientSpec getClientSpec();
	
	public HashMap<String, MessageContentSpec> getMessageContent();

	public void execute(ClientSpec clientSpec,JSONObject serviceSpec, String execid, String jobName, String runtimeDate, String waitFrequency, String decoratorTimeout) throws Exception;
	
	public Status service(ClientSpec clientSpec,JSONObject validatorSpec) throws Exception;
	public void onSuccess(Status status,String notificationEmail) throws Exception;
	public void onWarning(Status status,String notificationEmail,String pagerdutyKey) throws Exception;
	public void onError(Status status,String notificationEmail,String pagerdutyKey) throws Exception;
	
	public RuntimeContext getRuntimeContext();
	
	
}
