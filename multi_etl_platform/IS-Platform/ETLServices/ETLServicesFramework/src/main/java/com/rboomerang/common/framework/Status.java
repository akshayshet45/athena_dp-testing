package com.rboomerang.common.framework;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;

// Class represents the status of the validator / processor executions
public class Status {
	// Constants for the predefined status code
	public static final int STATUS_SUCCESS = 0;
	public static final int STATUS_ERROR = -1;

	
	private int statusCode = 0;
	private String helpLink;
	private boolean abortRun;
	private String pagerdutyKey;
	private boolean pagerdutyAlert;
	private String message = new String();

	public Status (int statusCode, String message) {
		this.statusCode = statusCode;
		this.message = message;
	}
	
	public boolean getPagerdutyAlert() {
		return pagerdutyAlert;
	}
	
	public void setPagerdutyAlert(boolean pagerdutyAlert) {
		this.pagerdutyAlert = pagerdutyAlert;
	}
	
	public String getPagerdutyKey() {
		return pagerdutyKey;
	}
	
	public void setPagerdutyKey(String pagerdutyKey) {
		this.pagerdutyKey = pagerdutyKey;
	}
	
	public boolean getAbortRun() {
		return abortRun;
	}
	
	public void setAbortRun(boolean abortRun) {
		this.abortRun = abortRun;
	}
	
	public String getHelpLink() {
		return helpLink;
	}
	
	public void setHelpLink(String helpLink) {
		this.helpLink = helpLink;
	}
	
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	public boolean isStatusSuccess() {
		return this.statusCode == Status.STATUS_SUCCESS;	
	}
	
	
	public boolean isStatusError() {
		return this.statusCode == Status.STATUS_ERROR;
	}
}
