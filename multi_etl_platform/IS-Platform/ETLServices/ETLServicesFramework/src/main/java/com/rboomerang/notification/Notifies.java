package com.rboomerang.notification;

import java.io.IOException;
import java.util.Map;

import com.rboomerang.common.framework.Status;

public interface Notifies {
	public String getName();
	public Status publish(String suject, Map<String,String> body) throws IOException;
}
