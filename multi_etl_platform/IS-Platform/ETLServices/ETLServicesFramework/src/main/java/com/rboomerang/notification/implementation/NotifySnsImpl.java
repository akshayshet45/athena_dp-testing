package com.rboomerang.notification.implementation;

import java.util.Map;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.sns.model.PublishResult;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.service.utils.amazonUtils.AmazonSnsHelper;
import com.rboomerang.notification.Notifies;

public class NotifySnsImpl implements Notifies {

	private String implName;
	private String snsName;
	private String snsRegion;
	private AmazonSnsHelper amazonSnsHelper;
	private static Logger logger = LoggerFactory.getLogger(NotifySnsImpl.class);
	
	public NotifySnsImpl(String implName, String snsName, String snsRegion) {
		this.implName = implName;
		this.snsName = snsName;
		this.snsRegion = snsRegion;
	}
	
	private void init(){
		amazonSnsHelper = new AmazonSnsHelper(snsName, snsRegion);
		logger.debug("amazonSnsHelper object initialization complete in init");
	}

	@Override
	public String getName() {
		logger.info("Implementation custom name :: " + implName);
		return implName;
	}

	@Override
	public Status publish(String suject, Map<String, String> body) {
		init();
		PublishResult publish = amazonSnsHelper.publish(new JSONObject(body).toJSONString());
		if(publish !=null && publish.getMessageId()!=null){
			logger.debug("message published successfully :: " + publish.getMessageId());
			logger.debug("returning suceess status from publish method");
			return new Status(Status.STATUS_SUCCESS, "SNS message pushed successfully :: "+publish.getMessageId() );
		}else{
			logger.debug("message published unsuccessful");
			logger.debug("returning failure status from publish method");
			return new Status(Status.STATUS_ERROR, "SNS message pushed unsuccessful");
		}
		
	}

}
