package com.rboomerang.utils;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.tuple.Pair;


import com.amazonaws.util.StringUtils;

public class RegexUtility {
	public static boolean compareRegex(String regexString, String inputString) {

		Pattern pattern = Pattern.compile(regexString);

		Matcher matcher = pattern.matcher(inputString);

		boolean found = false;
		while (matcher.find()) {
			found = true;
		}
		
		return found;
	}

	/**
	 * 
	 * All queries strings having any runtime parameter present in the form of
	 * "${{prop}}" is replaced with corresponding runtime value from
	 * "properties". If a prop is not present in properties, it is assumed the
	 * value is null ("") and empty string value is replaced.
	 * 
	 * @param queries
	 * @param properties
	 * @return
	 */
	public static String applyFlowParams(String string, Properties properties) {
		if (string == null||properties==null) {
			return string;
		}
		
		List<Pair<String, String>> bcVariables = new ArrayList<Pair<String, String>>();

		for (Object key : properties.keySet()) {
			if (key != null && ((String) key).trim().length() > 0) {
				String value = (String) (properties.get(key) == null ? "" : properties.get(key));
				bcVariables.add(Pair.of("${{" + key + "}}", value));
			}
		}

		if (string != null) {
			for (Pair<String, String> var : bcVariables) {
				string = StringUtils.replace(string, var.getKey(), var.getValue());
			}

		}

		return string;
	}

	// replace unavailable runtime variables with empty string
	public static String replaceUnavailableFlowParams(String string) {
		string = string.replaceAll(CommonConstants.FLOW_PARAM_REGEX_TAG, "");
		return string;

	}
	public static Properties parsePropertiesString(String s) throws IOException {
	    if(s==null)
	    	return null;
	    Properties p = new Properties();
	    s=s.substring(1,s.length()-1);
	    for (String prop : s.split(",")) {
	    	p.load(new StringReader(prop));
		}
	    
	    return p;
	}
}