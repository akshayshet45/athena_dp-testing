package com.rboomerang.utils;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.http.client.ClientProtocolException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;
import com.squareup.pagerduty.incidents.NotifyResult;
import com.squareup.pagerduty.incidents.PagerDuty;
import com.squareup.pagerduty.incidents.Trigger;

public class SendEmail {
	private static Logger logger = LoggerFactory.getLogger(SendEmail.class);
	public static final String DEFAULT_ISPAGERDUTYEMAIL = "is-dev@boomerangcommerce.com";
	public static final String DEFAUT_PAGERDUTYKEY = "";

	public static void send(String to, String subject, String body) {
		// Recipient's email ID needs to be mentioned.

		// Sender's email ID needs to be mentioned
		final String from = "ops@boomerangcommerce.com";

		// Assuming you are sending email from localhost
		String host = "smtp.gmail.com";

		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		properties.setProperty("mail.smtp.host", host);
		properties.setProperty("mail.user", from);
		properties.setProperty("mail.password", "Naganat123");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "587");

		// Get the default Session object.
		Session session = Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, "Naganat123");
			}
		});

		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			for (String retval : to.split(","))
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(retval));

			// Set Subject: header field
			message.setSubject(subject);

			// Now set the actual message
			message.setText(body);

			// Send message
			Transport.send(message);
			logger.info("Sent message successfully....");
		} catch (MessagingException mex) {
			mex.printStackTrace();
		} catch (Exception err) {
			err.printStackTrace();
		}
	}

	public static void sendErrorMail(String to, String subject, String body, String clientTag) {
		// Recipient's email ID needs to be mentioned.

		// Sender's email ID needs to be mentioned
		final String from = "ops@boomerangcommerce.com";

		// Assuming you are sending email from localhost
		String host = "smtp.gmail.com";

		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		properties.setProperty("mail.smtp.host", host);
		properties.setProperty("mail.user", from);
		properties.setProperty("mail.password", "Naganat123");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "587");

		// Get the default Session object.
		Session session = Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, "Naganat123");
			}
		});

		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			for (String retval : to.split(","))
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(retval));

			// Set Subject: header field
			message.setSubject(subject);

			// Now set the actual message
			message.setText(body + "\n");

			// Send message
			Transport.send(message);
			logger.info("Sent message successfully....");
		} catch (MessagingException mex) {
			mex.printStackTrace();
			logger.error(mex.getMessage(), mex);
		} catch (Exception err) {
			err.printStackTrace();
			logger.error(err.getMessage(), err);
		}
	}

	public static void triggerPagerduty(String pdKey, String description, Map<String, String> details) throws ClientProtocolException, IOException {
		PagerDuty pagerDuty = PagerDuty.create(pdKey);
		
		Trigger trigger = new Trigger.Builder(description)
			    .withIncidentKey("ERROR")
			    .addDetails(details)
			    .build();
			pagerDuty.notify(trigger);
			
		NotifyResult result = pagerDuty.notify(trigger);
		logger.info("Notify Result of PagerDuty - " + result.message());
		
		return;
	}

	public static void sendPagerDuty(String pagerString, String description, Map<String, String> details) {
		try {
			pagerString = (pagerString.isEmpty() || pagerString == null) ? SendEmail.DEFAULT_ISPAGERDUTYEMAIL
					: pagerString;
			triggerPagerduty(pagerString, description, details);
		} catch (Exception e1) {
			e1.printStackTrace();
			logger.error(e1.getMessage(), e1);
		}
	}

	public static void main(String[] args) {
		sendPagerDuty("032d502fd80c44a5aceb093d833ad2c9", "Testing PagerDuty from Main - ", ImmutableMap.of("Ping", "Pong", "Kit", "Kat"));
	}
}
