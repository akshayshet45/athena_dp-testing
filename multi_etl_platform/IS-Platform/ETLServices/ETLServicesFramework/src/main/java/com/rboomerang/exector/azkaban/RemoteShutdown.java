package com.rboomerang.exector.azkaban;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rboomerang.common.framework.ClientSpecInitialization;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.utils.EC2Helper;

public class RemoteShutdown {
	private static Logger logger = LoggerFactory.getLogger(RemoteShutdown.class);
	public static void main(String[] args) throws Exception {
		logger.info("Remote Machine Shutdown Job called");
		ClientSpecInitialization initializer = new ClientSpecInitialization(JobExecutor.CLIENT_SPEC_JSON_PATH);
		ClientSpec clientSpec = initializer.getClientSpecObject();
		logger.info("client-spec Initialized");
		String instanceID= clientSpec.getRemoteMachine();
		String ec2Endpoint = clientSpec.getRemoteMachineEndpoint();
		logger.info("Remote Machine InstanceID : "+instanceID);
		logger.info("Remote Machine Endpoint : "+ec2Endpoint);
		initializer.close();
		EC2Helper ec2Helper = new EC2Helper(instanceID,ec2Endpoint);
		ec2Helper.stopRemoteMachine();
	}

}
