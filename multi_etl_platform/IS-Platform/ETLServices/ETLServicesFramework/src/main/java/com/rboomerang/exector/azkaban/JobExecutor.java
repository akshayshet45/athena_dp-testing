package com.rboomerang.exector.azkaban;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import azkaban.utils.Props;

import com.rboomerang.common.framework.RuntimeContext;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.common.framework.javabeans.MessageContentSpec;
import com.rboomerang.service.framework.IService;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.RegexUtility;
import com.rboomerang.utils.SendEmail;
import com.rboomerang.utils.StackTraceUtility;

public class JobExecutor {

	public static final String CLIENT_SPEC_JSON_PATH = "client-spec.json";
	public static final String SCHEDULE_SPEC_JSON_PATH = "configs/schedule-spec.json";
	private static final String SERVICE_CLASS_NAME = "service.class";
	private static final String SERVICE_PROPERTIES = "file.properties";
	private static final String IS_DECORATOR = "decorator";
	private static final String WAIT_FREQUENCY = "wait.frequency";
	private static final String DECORATOR_TIMEOUT = "decorator.timeout";
	private static final String JOB_EXECUTOR_ERROR = "no argument to JobExecutor";

	private static Logger logger = LoggerFactory.getLogger(JobExecutor.class);
	private static String azkaban_env_tag = null;
	private String clientSpecPath = null;
	private String serviceClassName = null;
	private String serviceSpecPath = null;
	private RuntimeContext runtimeContext = null;
	private String execid = null;
	private String jobName = null;
	private String runtimeDate = null;
	private String waitFrequency = null;
	private String decoratorTimeout = null;


	public JobExecutor(String serviceClassName, String clientSpecPath, String serviceSpecPath,
			RuntimeContext runtimeContext, String execid, String jobName, String runtimeDate, String waitFrequency, String decoratorTimeout) {
		this.serviceClassName = serviceClassName;
		this.clientSpecPath = clientSpecPath;
		this.serviceSpecPath = serviceSpecPath;
		this.runtimeContext = runtimeContext;
		this.execid = execid;
		this.jobName = jobName;
		this.runtimeDate = runtimeDate;
		this.waitFrequency = waitFrequency;
		this.decoratorTimeout = decoratorTimeout;
	}

	public void run() {
		ClientSpec clientSpec = null;
		JSONObject serviceSpec = null;
		HashMap<String, MessageContentSpec> messageContent = null;
		String clientSpecDate = "";

		try {
			IService serviceObj = getServiceObject();

			messageContent = serviceObj.getMessageContent();
			clientSpec = serviceObj.getClientSpec();
			serviceSpec = serviceObj.getServiceSpec();
			clientSpecDate = calculateSpecDate(this.runtimeDate, clientSpec);
			clientSpec.setDate(clientSpecDate);

			serviceObj.execute(clientSpec, serviceSpec, this.execid, this.jobName, this.runtimeDate, this.waitFrequency,
					this.decoratorTimeout);
			serviceObj.close();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			if (messageContent != null) {
				MessageContentSpec content = messageContent.get(MessageContentSpec.ERROR);
				SendEmail.sendErrorMail(StringUtils.join(clientSpec.getNotificationEmail(), ','),
						"["+clientSpec.getClient()+"] [#"+this.execid+"] "+content.getSubject()
								.replace(ClientSpec.DATE_FORMAT_REGEX, clientSpecDate),
						content.getBody()+"\n"+StackTraceUtility.stackTraceToString(e), clientSpec.getClient());
			} else
				SendEmail.sendErrorMail(SendEmail.DEFAULT_ISPAGERDUTYEMAIL,"["+clientSpec.getClient()+"] [#"+this.execid+"] "+ JOB_EXECUTOR_ERROR, JOB_EXECUTOR_ERROR+"\n"+StackTraceUtility.stackTraceToString(e), JOB_EXECUTOR_ERROR);
			System.exit(-1);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			if (messageContent != null) {
				MessageContentSpec content = messageContent.get(MessageContentSpec.ERROR);
				SendEmail.sendErrorMail(StringUtils.join(clientSpec.getNotificationEmail(), ','),
						"["+clientSpec.getClient()+"] [#"+this.execid+"] "+content.getSubject()
								.replace(ClientSpec.DATE_FORMAT_REGEX, clientSpecDate),
						content.getBody()+"\n"+StackTraceUtility.stackTraceToString(e), clientSpec.getClient());
			} else
				SendEmail.sendErrorMail(SendEmail.DEFAULT_ISPAGERDUTYEMAIL, "["+clientSpec.getClient()+"] [#"+this.execid+"] "+JOB_EXECUTOR_ERROR, JOB_EXECUTOR_ERROR+"\n"+StackTraceUtility.stackTraceToString(e), JOB_EXECUTOR_ERROR);
			System.exit(-1);
		}

	}
	
	private String calculateSpecDate(String runtimeDate, ClientSpec clientSpec) throws ParseException {
		String daysAgo = clientSpec.getDaysAgo();
		String dateFormat = clientSpec.getDateFormat();
		
		DateFormat format = new SimpleDateFormat(CommonConstants.DEFAULT_DATE_FORMAT, Locale.ENGLISH);
		Date date = format.parse(runtimeDate);
		
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, (-1) * Integer.parseInt(daysAgo));
		date.setTime(c.getTime().getTime());
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
		
		return simpleDateFormat.format(date);
	}

	private IService getServiceObject() {
		IService serviceObject = null;

		try {
			Constructor<?> serviceClass = Class.forName(this.serviceClassName).getConstructor(String.class,
					String.class, RuntimeContext.class, String.class);
			serviceObject = (IService) serviceClass.newInstance(this.clientSpecPath, this.serviceSpecPath,
					this.runtimeContext, this.runtimeDate);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			SendEmail.sendErrorMail(SendEmail.DEFAULT_ISPAGERDUTYEMAIL,"[#"+this.execid+"] "+JOB_EXECUTOR_ERROR, JOB_EXECUTOR_ERROR+"\n"+StackTraceUtility.stackTraceToString(e), JOB_EXECUTOR_ERROR);
			System.exit(-1);
		}

		return serviceObject;
	}

	// Program may be called with either 2 parameters or 3 parameters
	// If called for remote execution, then three parameters are passed:
	// clientSpecPath jobSpecPath remoteFlag
	// If called for normal execution, then two parameters are passed:
	// clientSpecPath jobSpecPath
	public static void main(String[] args) throws IOException {
		if (args.length < 2) {
			// Halt processing with error status -1
			logger.error("Terminating, Client Spec Path / Job Path not specified !!!");
			SendEmail.sendErrorMail(SendEmail.DEFAULT_ISPAGERDUTYEMAIL, JOB_EXECUTOR_ERROR, JOB_EXECUTOR_ERROR, JOB_EXECUTOR_ERROR);
			System.exit(CommonConstants.ERROR_CODE);
		}
		File azkabanFlowParamFile = File.createTempFile("azkabanFlowParamFile", ".properties");
		try {
			String clientSpecPath = args[0];
			String jobSpecPath = args[1];

			// If there is a third parameter, it is for the "remote flag" &
			// "base directory"
			String execid = null, jobName = null, runtimeDate = null;
			RuntimeContext runtimeContext = new RuntimeContext(false, null);
			Properties azkabanFlowParams = null;
			
			
			if (args.length == 9) {
			
				runtimeContext = new RuntimeContext(Boolean.valueOf(args[2]), args[3]);
				execid = args[4];
				jobName = args[5];
				runtimeDate = args[6];
				azkaban_env_tag = args[7];
				if (args[8]!=null && !args[8].equals(null) && !args[8].equals("") ) {
					logger.info("azkaban job properties on remote -- > "+args[8]);	
					azkabanFlowParams=RegexUtility.parsePropertiesString(args[8]);
				}
				
				if (azkaban_env_tag!=null) {
					System.setProperty(CommonConstants.AZKABAN_ENV_TAG, azkaban_env_tag);
				}
				if (azkabanFlowParams!=null) {
					azkabanFlowParams.store(new FileOutputStream(azkabanFlowParamFile), "azkabanFlowParam");
					System.setProperty(CommonConstants.AZKABAN_JOB_PROPERTIES_FILE_PATH,azkabanFlowParamFile.getAbsolutePath());
				}
			}
			
			clientSpecPath = runtimeContext.transformPath(clientSpecPath);
			jobSpecPath = runtimeContext.transformPath(jobSpecPath);
			logger.info("Reading clientSpecPath " + clientSpecPath);
			logger.info("Reading jobSpecPath " + jobSpecPath);

			// Load the Job spec, to get the validator Class and validatorSpec
			Props jobSpec = new Props(null, jobSpecPath);
			logger.info("jobSpec: " + jobSpec.toString());
			String serviceSpecPath = jobSpec.get(JobExecutor.SERVICE_PROPERTIES);
			String serviceClassName = jobSpec.get(JobExecutor.SERVICE_CLASS_NAME);
			
			String isDecorator = jobSpec.get(JobExecutor.IS_DECORATOR);
			String waitFrequency = null;
			String decoratorTimeout = null;

			// Process the validatorSpecPath depending on the runtime context
			serviceSpecPath = runtimeContext.transformPath(serviceSpecPath);
			logger.info("Reading serviceSpecPath " + serviceSpecPath);

 			JobExecutor jobExec = new JobExecutor(serviceClassName, clientSpecPath, serviceSpecPath, runtimeContext,
					execid, jobName, runtimeDate, null, null);

			if (isDecorator!=null && !isDecorator.equals(null) && isDecorator.equals("true")) {
				waitFrequency = jobSpec.get(JobExecutor.WAIT_FREQUENCY);
				decoratorTimeout = jobSpec.get(JobExecutor.DECORATOR_TIMEOUT);
				jobExec = new JobExecutor(serviceClassName, clientSpecPath, serviceSpecPath, runtimeContext, execid,
						jobName, runtimeDate, waitFrequency, decoratorTimeout);
			}

			jobExec.run();
			azkabanFlowParamFile.delete();
		} catch (Exception e) {
			logger.error("Error in executing Job Executor !!!");
			logger.error(e.getMessage(), e);
			SendEmail.sendErrorMail(SendEmail.DEFAULT_ISPAGERDUTYEMAIL,"[#"+args[4]+"] "+JOB_EXECUTOR_ERROR, JOB_EXECUTOR_ERROR+"\n"+StackTraceUtility.stackTraceToString(e), JOB_EXECUTOR_ERROR);
			azkabanFlowParamFile.delete();
			System.exit(CommonConstants.ERROR_CODE);
			
		}
	}

}