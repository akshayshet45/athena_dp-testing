package com.rboomerang.utils;

public final class CommonConstants {
	public static final int ERROR_CODE = -1;
    public static final int SUCCESS_CODE = 0;
    public static final String FLOW_PARAM_REGEX_TAG = "\\$\\{\\{.*?\\}\\}";
    public static final String AZKABAN_ENV_TAG="azkaban_env_tag";
    public static final String AZKABAN_JOB_PROPERTIES_FILE_PATH="azkaban_job_properties_file_path";
    public static final String BASE_DIRECTORY = "/home/ubuntu/is-platform";

    
    //New FrameWork NotificationMain
    public static final String SUCCESS_ENDPOINTS = "successEndpoints";
    public static final String FAILURE_ENDPOINTS = "failureEndpoints";
    
  // constants used for FileExistenceCheck

    public static final String AZKABAN_FLOW_NESTED_PATH="azkaban.flow.nested.path";
    public static final String AZKABAN_JOB_META_FILE="azkaban.job.metadata.file";
    public static final String JOB_EXECUTION_ENVIRONMENT = "job_execution_environment";
    // constants used for FileExistenceCheck

    public static final String NOTIFICATION_EMAIL = "notificationEmail";
    public static final String ERROR_EMAIL="errorEmail";
    public static final String LIST = "list";
    public static final String ENDPOINT = "endpoint";
    public static final String LOCATION = "location";
    public static final String FILENAME = "filename";
    public static final String FEED_DATE = "feed_date";
    public static final String HELP_LINK = "helpLink";
    public static final String ABORT_RUN = "abortRun";
    public static final String NOTIFY = "notify";
    public static final String PAGERDUTY_KEY = "pagerdutyKey";
    public static final String PAGERDUTY_ALERT = "pagerdutyAlert";
    public static final String YEAR = "{{feed_date.YYYY}}";
    public static final String MONTH = "{{feed_date.MM}}";
    public static final String DAY = "{{feed_date.DD}}";
    public static final String DEFAULT_DATE_FORMAT = "yyyyMMdd";
    public static final String REGEX_TAG = "\\{\\{regex\\}\\}";
    public static final String REGEX_STRING = "(.*)";
    public static final String COMPRESSION = "compression";
    
    public static final String FILE_LOCATION = "scriptPath";
    public static final String AZKABAN_EXEC_ID = "azkaban.flow.execid";
    public static final String AZKABAN_FEED_DATE = "FEED_DATE";
    public static final String AZKABAN_TIME_ZONE = "azkaban.flow.start.timezone";
    public static final String NEWLINE = "\n";
    public static final String TAB = "\t";
    public static final String COMMA = ",";
    public static final String SPACE = " ";
    public static final String LOG_LINK = "{{loglink}}";
    public static final String EXEC_ID = "{{execid}}";
    public static final String JOB_NAME = "{{jobname}}";
    public static final String HELP_LINK_TITLE = "Help Link";
    public static final String LOG_LINK_TITLE = "Log Link";
    
    public static final String FORMAT = "format";
    public static final String COPY_FEED_DATE = "copy_feed_date";
    public static final String AZKABAN_URL_SUFFIX = "http://host:8081/executor?execid={{execid}}&job={{jobname}}";
    public static final String SOURCE = "source";
    public static final String DESTINATION = "destination";
    public static final String PATH = "path";
    public static final String FILE_NAME = "fileName";
    public static final String DELIMITER = "delimiter";
    
    public static final String EMAIL_ID = "data@boomerangcommerce.com";
    public static final String EMAIL_PASSWORD = "boomerangdata";
    
    // =========== constants used in DDValidation.java ===========
    public static final String ENDPOINTNAME = "endpointName";
    public static final String TABLENAME = "tableName";
    public static final String NUMOFDAYS = "numOfDays";
    public static final String WARNINGRANGE = "warningRange";
    public static final String ERRORRANGE = "errorRange";
    public static final int PORTNUM = 5439;
    public static final String POSTGRESDRIVER = "org.postgresql.Driver";
    public static final String POSTGREJDBC = "jdbc:postgresql";
    public static final String COLUMNNAME = "columnName";
    
    // =========== constants used in MailAttachmentDownloader.java ===========
    
    public static final String DATE_FORMAT = "dateFormat";
    public static final String MAIL_ADDRESS = "emailId";
    public static final String MAIL_PASSWORD = "emailPassword";
    
    // ========== constants used in GoogleAnalytics Processor (GAProcessor.java), CoreMetrics, Omniture Processors===
    
    
    public static final String QUERY="query";
    public static final String COREMETRICS_ENDPOINT="https://welcome.coremetrics.com/analyticswebapp/api/1.0/";
    public static final String OMNITURE_ENDPOINT="https://api.omniture.com/admin/1.3/rest/";
    public static final String RECIPIENT_LIST = "recipientList";
    // ============ constants used in record count report processor ====================
    public static final String TABLE_NAMES =  "tableNames";
    public static final String MESSAGE_HEADING =  "messageHeading";
    public static final String ZERO_COUNT =  "zeroCount";
    public static final String COUNT_TABLE_NAMES =  "countTableNames";
    public static final String REPORT_TABLES =  "ReportTables";
    public static final String COUNT_TABLES =  "countTables";
    public static final String DATE_COLUMNS =  "dateColumns";
    //========== Constants used in ThresholdValidator Validator=========================== 
	public static final String VALIDATORNAME = "validatorName";
	public static final String VALIDATORTYPE = "validatorType";
	public static final Object ALERTRANGE = "alertRange";
	public static final Object PRIMARYKEY = "primaryKey";

	//====================New Alert Mechanism ======================================
	public static final String SNS="sns";
	public static final String SQS="sqs";
	public static final String EMAIL="email";
	public static final String PAGER_DUTY="pagerDuty";


	// ============ ScriptLite Executor constants ==================
	public static final String DML_SCRIPT="scripts/DmlSql.sh";
	public static final String SCRIPTLITE_DIR="/home/ubuntu/scriptlite";

}

