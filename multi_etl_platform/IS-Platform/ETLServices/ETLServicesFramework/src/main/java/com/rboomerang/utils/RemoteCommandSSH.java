package com.rboomerang.utils;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

/**
 * 
 * @author vishal
 *
 */
public class RemoteCommandSSH implements Closeable {

	private String host;
	private static Logger logger = LoggerFactory.getLogger(RemoteCommandSSH.class);
	private String user;
	private List<String> commands;
	private String privatekeyFilePath;
	private Session session;
	private String knownHostsFileName;

	public RemoteCommandSSH(String host, String user, String privatekeyFilePath, String command) {
		this(host, user, privatekeyFilePath, Lists.newArrayList(command), null);
	}

	public RemoteCommandSSH(String host, String user, String privatekeyFilePath, List<String> commands) {
		this(host, user, privatekeyFilePath, commands, null);
	}

	public RemoteCommandSSH(String host, String user, String privatekeyFilePath, List<String> commands,
			String knownHostsFileName) {
		this.host = host;
		this.user = user;
		this.commands = commands;
		this.privatekeyFilePath = privatekeyFilePath;
	}

	public List<String> getCommands() {
		return commands;
	}

	private void createSession() throws JSchException {
		JSch jsch = new JSch();
		if (privatekeyFilePath != null) {
			jsch.addIdentity(this.privatekeyFilePath);
		}
		session = jsch.getSession(this.user, this.host, 22);
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		config.put("MaxSessions", "100");
		// TODO add later
		if (knownHostsFileName != null) {
			jsch.setKnownHosts(knownHostsFileName);
		}
		session.setConfig(config);
		if (!session.isConnected()) {
			session.connect();
		}
	}

	public List<Integer> execute() throws Exception {
		return execute(null);
	}

	public List<Integer> execute(List<String> output) throws Exception {
		List<Integer> exitStatusCodes = Lists.newArrayList();

		logger.info(String.format("Creating connection for : host [%s], user [%s], privateKetFilePath [%s]", host, user,
				privatekeyFilePath));
		createSession();
		logger.info(String.format("Remote SSH session created for host [%s]", host));
		logger.info("Commands here -> " + commands);

		for (String command : commands) {
			logger.info("##### Executing command " + command);
			Integer exitStatus = executeInternal(command, output);
			logger.info("### Return value " + exitStatus);
			exitStatusCodes.add(exitStatus);
			if (exitStatus != 0) {
				logger.info("Execution of last batch of shell commands failed. Aborting subsequent executions.");
				break;
			}
		}
		logger.info("Before closing session in execute");
		closeSession();

		logger.info("AFTER closing session in execute");

		return exitStatusCodes;
	}

	private void closeSession() {
		if (session != null && session.isConnected()) {
			session.disconnect();
		}
	}

	private Integer executeInternal(String command, List<String> output) throws Exception {
		int exitStatus = 0;
	
			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);

			channel.setInputStream(null);

			((ChannelExec) channel).setErrStream(System.err);
			InputStream in = channel.getInputStream();

//			channel.connect();
			
			try{
				channel.connect();
				}
				catch(Exception e)
				{
					logger.error(e.getMessage());
					if (e.getMessage().contains("channel is not opened")) {
						channel.disconnect();
						closeSession();
						Thread.sleep(50000);
						createSession();
						channel = session.openChannel("exec");
						((ChannelExec) channel).setCommand(command);

						channel.setInputStream(null);

						((ChannelExec) channel).setErrStream(System.err);
						in = channel.getInputStream();
						channel.connect(60000);
					}
					else
						throw e;
				}
			
			// logger.info("Channel connected.");
			logger.info("\nExecuting commands : \n" + command + "\n");

			List<String> outputLines = Lists.newArrayList();
			byte[] tmp = new byte[1024];

			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					String line = new String(tmp, 0, i);
					outputLines.add(line);
					System.out.print(line);
				}
				if (channel.isClosed()) {
					exitStatus = channel.getExitStatus();
					if (exitStatus == 0) {
						logger.info("\nExecuted successfully. : " + exitStatus);
					} else {
						logger.info("Error while executing command. Exit status : " + exitStatus);
					}
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
					System.err.println("Channel Error : " + ee.getMessage());
					ee.printStackTrace();
				}
			}
			if (output != null) {
				output.addAll(outputLines);
			}
			try {
				in.close();
			} catch (Exception e) {
				System.err.print("Error in closing input stream.");
				e.printStackTrace();
			}
			channel.disconnect();
		logger.info("Channel closed");
		return exitStatus;
	}

	@SuppressWarnings("resource")
	public static void main(String[] argc) throws Exception {
		// String command = "sed -i -e \"s/\\xa7/,/g\"
		// /mnt/azkaban_workdir/QuillCurrentPricing_20150225.txt" ;
		// String command = "mysql -u quill_hive -pBoomerang123$ -h
		// db.prod.quill.rboomerang.com -e \"use cmn_new;\nshow tables;\"";
		String command = "ls -l";
		RemoteCommandSSH exec = new RemoteCommandSSH("ec2-54-81-112-60.compute-1.amazonaws.com", "hadoop",
				"/Users/yogesh/bc-virginia.pem", Lists.newArrayList(command));
		logger.info("exitStatusCodes : " + exec.execute());
	}

	@SuppressWarnings("resource")
	public static void main1(String[] argc) throws Exception {
		String command1 = "a='hello world'";
		String command2 = "echo $a";
		RemoteCommandSSH exec = new RemoteCommandSSH("localhost", "yogesh", "/Users/yogesh/.ssh/id_rsa",
				Lists.newArrayList(command1, command2));
		logger.info("exitStatusCodes : " + exec.execute());
	}

	@Override
	public void close() throws IOException {
		closeSession();
	}

}
