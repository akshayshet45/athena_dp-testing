package com.rboomerang.notification.implementation;

import java.io.IOException;
import java.util.Map;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.sqs.model.SendMessageResult;
import com.rboomerang.common.framework.Status;
import com.rboomerang.common.service.utils.amazonUtils.AmazonSqsHelper;
import com.rboomerang.notification.Notifies;
import com.rboomerang.utils.AWSAccess;

public class NotifySqsImpl implements Notifies {

	private String implName;
	private String snsName;
	private String snsRegion;
	private AmazonSqsHelper amazonSqsHelper;
	private static Logger logger = LoggerFactory.getLogger(NotifySqsImpl.class);

	public NotifySqsImpl(String implName, String snsName, String snsRegion) {
		this.implName = implName;
		this.snsName = snsName;
		this.snsRegion = snsRegion;
	}

	@Override
	public String getName() {
		return implName;
	}

	private void init() throws IOException {
		AWSAccess awsAccess = new AWSAccess();
		logger.debug("QueueName :: "+ snsName);
		logger.debug("QueueRegion :: "+ snsRegion);
		amazonSqsHelper = new AmazonSqsHelper(awsAccess.getAWSAccess(), snsRegion,
				snsName);
	}

	@Override
	public Status publish(String suject, Map<String, String> body) throws IOException {
		init();
		SendMessageResult sendMessage = amazonSqsHelper.sendMessage(
				new JSONObject(body).toJSONString(), amazonSqsHelper.getQueueUrl(), snsName);
		if (sendMessage != null && sendMessage.getMessageId() != null) {
			logger.info("Message sent successfully");
			logger.debug("Message Id :: " + sendMessage.getMessageId());
			return new Status(Status.STATUS_SUCCESS,
					"Amazon Sqs message sent successfully :: "
							+ sendMessage.getMessageId());
		} else {
			logger.info("Message sending failed");
			return new Status(Status.STATUS_ERROR,
					"Amazon Sqs message sent unsuccessful");
		}
	}

}
