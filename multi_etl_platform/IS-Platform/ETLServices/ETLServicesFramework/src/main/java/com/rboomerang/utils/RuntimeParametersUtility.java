package com.rboomerang.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RuntimeParametersUtility {
	private static Logger logger = LoggerFactory.getLogger(RuntimeParametersUtility.class);
	private static String azkabanEnvTag = null;
	private static String ocdPropertiesFile = null;
	private static String commonPropertyFile = null;
	private static Properties envSpecificProp = null;
	private static Properties commonProp = null;
	private static Properties azkabanFlowParameters = null;
	private InputStream inStream = null;
	private static final String ENV_PROPERTIES_DIRECTORY = "env_configs";

	public static String replaceRuntimeParameters(String parameterisedString, String clientTag)
			throws FileNotFoundException, IOException {
		logger.debug(" Parameterised String before modification -->>> " + parameterisedString);
		String azkabanFlowParametersFilePath = System.getProperty(CommonConstants.AZKABAN_JOB_PROPERTIES_FILE_PATH);

		if (azkabanFlowParametersFilePath != null) {
			logger.debug("AZKABAN_JOB_PROPERTIES_FILE_PATH : " + azkabanFlowParametersFilePath);
			azkabanFlowParameters = new Properties();
			azkabanFlowParameters.load(new FileInputStream(new File(azkabanFlowParametersFilePath)));
			parameterisedString = RegexUtility.applyFlowParams(parameterisedString, azkabanFlowParameters);
			logger.debug(" Parameterised String after replacing azkaban flow parameters  -->>> " + parameterisedString);

		}

		// following system property is set as system property in JobExecutor
		// main method.

		azkabanEnvTag = System.getProperty(CommonConstants.AZKABAN_ENV_TAG);
		StringBuilder ocdPropertiesFileLocation = new StringBuilder();
		StringBuilder commonPropertyFileLocation = new StringBuilder();
		if (azkabanEnvTag != null
				&& (azkabanEnvTag.equals("beta") || azkabanEnvTag.equals("qa") || azkabanEnvTag.equals("prod"))) {

			// system property JOB_EXECUTION_ENVIRONMENT is set to azkaban in
			// RemoteJobExecutor
			String execEnv = System.getProperty(CommonConstants.JOB_EXECUTION_ENVIRONMENT);
			if (execEnv != null && execEnv.equals("azkaban")) {

				ocdPropertiesFileLocation.append(ENV_PROPERTIES_DIRECTORY).append("/").append(azkabanEnvTag)
						.append(".properties");
				commonPropertyFileLocation.append(ENV_PROPERTIES_DIRECTORY).append("/").append("common")
						.append(".properties");

				logger.debug("job execution env azkaban and env specific property file path : "
						+ ocdPropertiesFileLocation.toString());
				logger.debug("job execution env azkaban and common property file path : "
						+ commonPropertyFileLocation.toString());

			} else {

				ocdPropertiesFileLocation.append(CommonConstants.BASE_DIRECTORY).append("/").append(clientTag)
						.append("/").append(ENV_PROPERTIES_DIRECTORY).append("/").append(azkabanEnvTag)
						.append(".properties");
				logger.debug("job execution env remote machine and env specific property file path : "
						+ ocdPropertiesFileLocation.toString());

				commonPropertyFileLocation.append(CommonConstants.BASE_DIRECTORY).append("/").append(clientTag)
						.append("/").append(ENV_PROPERTIES_DIRECTORY).append("/").append("common")
						.append(".properties");
				logger.debug("job execution env remote machine and common property file path : "
						+ commonPropertyFileLocation.toString());

			}
			
			ocdPropertiesFile = ocdPropertiesFileLocation.toString();
			commonPropertyFile=commonPropertyFileLocation.toString();
			File propFile = new File(ocdPropertiesFile);
			if (propFile.exists() && !propFile.isDirectory()) {
				envSpecificProp = new Properties();
				envSpecificProp.load(new FileInputStream(propFile));
			} else {
				logger.info("no property file specified at " + ocdPropertiesFile);
			}
			
			File commonPropFile = new File(commonPropertyFile);
			if (commonPropFile.exists() && !commonPropFile.isDirectory()) {
				commonProp = new Properties();
				commonProp.load(new FileInputStream(commonPropFile));
			} else {
				logger.info("no common property file specified at " + commonPropertyFile);
			}
		}
		
		if (commonProp!=null) {
			parameterisedString = RegexUtility.applyFlowParams(parameterisedString, commonProp);
		}
		
		if (envSpecificProp != null) {
			parameterisedString = RegexUtility.applyFlowParams(parameterisedString, envSpecificProp);
		}
		
		logger.debug(" Parameterised String after replacing values from env propeties  -->>> " + parameterisedString);
		parameterisedString = RegexUtility.replaceUnavailableFlowParams(parameterisedString);
		logger.debug(" Parameterised String after replacing all left over parameters by blank  -->>> "
				+ parameterisedString);
		return parameterisedString;

	}

	public Properties getAzkabanProp() throws IOException {
		logger.debug("getAzkabanProp method called");
		File azkaban = new File(System.getenv("JOB_PROP_FILE"));
		inStream = new FileInputStream(azkaban);
		Properties azkabanLogProperties = new Properties();
		azkabanLogProperties.load(inStream);
		return azkabanLogProperties;
	}

	public void close() throws IOException {
		logger.debug("getAzkabanProp close method called");
		inStream.close();
	}
}
