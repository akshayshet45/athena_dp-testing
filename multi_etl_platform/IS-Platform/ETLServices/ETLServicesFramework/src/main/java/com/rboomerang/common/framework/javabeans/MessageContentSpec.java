package com.rboomerang.common.framework.javabeans;

public class MessageContentSpec {
	
	public static final String ERROR="error";
	public static final String WARNING="warn";
	public static final String SUCCESS="success";
	public static final String ERROR_ONLY="errorOnly";
	private String subject;
	private String body;
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject,String feed_date, String clientTag) {
		this.subject = subject.replace("{{feed_date}}", feed_date).replace("{{clientTag}}", clientTag);
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	
}
