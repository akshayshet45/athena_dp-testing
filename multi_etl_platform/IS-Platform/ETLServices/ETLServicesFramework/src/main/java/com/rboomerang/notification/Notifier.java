package com.rboomerang.notification;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface Notifier {
	public void registerSuccessNotifier(Notifies notifies);
	public void registerFailureNotifier(Notifies notifies);
	void notifySuccess(List<String> listNotifies, String subject,
			Map<String, String> body) throws IOException;
	void notifyFailure(List<String> listNotifies, String subject,
			Map<String, String> body) throws IOException;
}
