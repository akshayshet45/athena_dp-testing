package com.rboomerang.utils;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSSessionCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;

public class AWSAccess {
	private final Logger log = LoggerFactory.getLogger(AWSAccess.class);
	private Properties prop;
	private AWSCredentialsProvider basicAWSCredentialsProvider = null;
	private AWSCredentials awsCredentials = null;

	public AWSCredentials getAWSAccess() throws IOException {
		try {
			basicAWSCredentialsProvider = new InstanceProfileCredentialsProvider();
			awsCredentials = basicAWSCredentialsProvider.getCredentials();
			String accessKey = awsCredentials.getAWSAccessKeyId();
			String secretKey = awsCredentials.getAWSSecretKey();
			log.debug("access key through IAM role : " + accessKey);
			log.debug("secret key through IAM role : " + secretKey);
			log.info("Access granted through IAM Role");

		} catch (Exception e) {
			AWSAccess awsAccess = new AWSAccess();
			awsCredentials=awsAccess.getAWSUserSpecificAccess();
		}
		return awsCredentials;
	}
	
	public AWSSessionCredentials getAWSAccessWithSession() throws IOException {
			InstanceProfileCredentialsProvider provider=new InstanceProfileCredentialsProvider();
			AWSSessionCredentials sessionCreds=(AWSSessionCredentials) provider.getCredentials();
			String accessKey = sessionCreds.getAWSAccessKeyId();
			String secretKey = sessionCreds.getAWSSecretKey();
			String securityToken = sessionCreds.getSessionToken();
			log.debug("access key through IAM role : " + accessKey);
			log.debug("secret key through IAM role : " + secretKey);
			log.debug("Session token through IAM role : " + securityToken);
			log.info("Access granted through IAM Role with session");
		return sessionCreds;
	}
	public AWSCredentials getAWSUserSpecificAccess() throws IOException {
		try {
			prop = new Properties();
			prop.load(AWSAccess.class.getClassLoader().getResourceAsStream("awsConfiguraion.properties"));
			String accessKey = (String) prop.get("aws-s3-identity");
			String secretKey = (String) prop.get("aws-s3-credential");
			awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
			log.debug("user specific Access Key : " + awsCredentials.getAWSAccessKeyId());
			log.debug("user specific secret key : " + awsCredentials.getAWSSecretKey());
			log.info("Access granted through user specific key");

		} catch (IOException ioEx) {
			log.error("Error wile reading awsConfiguraion.properties", ioEx);
			throw ioEx;
		}

		return awsCredentials;
	}
}
