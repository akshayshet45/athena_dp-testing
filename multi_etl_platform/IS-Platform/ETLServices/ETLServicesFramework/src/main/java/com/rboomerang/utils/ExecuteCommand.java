package com.rboomerang.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class ExecuteCommand
{
	public static String executeCommands(String shellLocation, String arguments) throws IOException, InterruptedException 
	{
		File tempScript = new File(shellLocation);
		String [] listArguments = arguments.split(" ");
		ProcessBuilder pb = new ProcessBuilder("bash", tempScript.toString(), listArguments[0], 
				listArguments[1], listArguments[2], listArguments[3], listArguments[4]).redirectErrorStream(true);
		Process process = pb.start();
		LogRedirector logScriptOutput=new LogRedirector(process.getInputStream());
		logScriptOutput.start();
		logScriptOutput.join();
		process.waitFor();
		StringBuffer output=new StringBuffer(logScriptOutput.getOutput());
		process.waitFor();
		if (process.exitValue() != 0)
		{
			output.append("ERROR");
		}
		else if(process.exitValue()==0)
		{
			output=new StringBuffer(output.toString().replaceAll("ERROR", ""));
		}
		return output.toString();
	}

	public static String executeCommand(String command) throws Exception 
	{

		Process p;
		p = Runtime.getRuntime().exec(command);
		LogRedirector logScriptOutput=new LogRedirector(p.getInputStream());
		logScriptOutput.start();
		logScriptOutput.join();
		p.waitFor();
		StringBuffer output=new StringBuffer(logScriptOutput.getOutput());
		if (p.exitValue() != 0)
		{
			output.append("ERROR");
		}
		else if(p.exitValue()==0)
		{
			output=new StringBuffer(output.toString().replaceAll("ERROR", ""));
		}
		return output.toString();
	}
	
	public static String executeAttributeCommands(List<String> command) throws InterruptedException, IOException 
	{
		ProcessBuilder pb = new ProcessBuilder(command).redirectErrorStream(true);
		Process process = pb.start();
		LogRedirector logScriptOutput=new LogRedirector(process.getInputStream());
		logScriptOutput.start();
		logScriptOutput.join();
		StringBuffer output=new StringBuffer(logScriptOutput.getOutput());
		process.waitFor();
		if (process.exitValue() != 0)
		{
			output.append("ERROR");
		}
		else if(process.exitValue()==0)
		{
			output=new StringBuffer(output.toString().replaceAll("ERROR", ""));
		}
		return output.toString();
	}
	
	public static void main(String[] args) throws Exception {
		/*System.out.println(executeCommands("/Users/kaushiksurikuchi/Documents/athena_is-platform/IS-Platform/ETLServices/scripts/DmlSql.sh", 
				"/Users/kaushiksurikuchi/Documents/athena_dataprocessing/dd/standard_dd.sql ins1GHTSokl one-kings-lane.cnjeis9czdgg.us-west-2.redshift.amazonaws.com insights okl"));*/
	}
}

class LogRedirector extends Thread
{
	  private InputStream inputStream;
	  private StringBuilder output=new StringBuilder();
	  private static Logger logger = LoggerFactory.getLogger(LogRedirector.class);
	  public LogRedirector(InputStream inputStream) 
	  {
	    this.inputStream = inputStream;
	  }
	  public String getOutput()
	  {
		  return output.toString();
	  }
	  public void run() 
	  {
	    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
	    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	    String line = null;
	    try 
	    {
	      while ((line = bufferedReader.readLine()) != null) 
	      {
	    	logger.info(line); 
	    	output.append(line +"\n");
	      }
	    } 
	    catch (IOException e) 
	    {
	      e.printStackTrace();
	    }
	  }
}
