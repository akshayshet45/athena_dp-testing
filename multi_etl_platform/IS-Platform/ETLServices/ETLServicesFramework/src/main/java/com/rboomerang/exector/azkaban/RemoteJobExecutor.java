package com.rboomerang.exector.azkaban;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.util.EC2MetadataUtils;
import com.google.common.collect.Lists;
import com.rboomerang.common.framework.ClientSpecInitialization;
import com.rboomerang.common.framework.javabeans.ClientSpec;
import com.rboomerang.utils.CommonConstants;
import com.rboomerang.utils.*;

import com.rboomerang.utils.SendEmail;

public class RemoteJobExecutor {
	private static Logger logger = LoggerFactory.getLogger(RemoteJobExecutor.class);

	private static final String USER = "ubuntu";
	private static final String JENKINS_PRIVATE_KEY = "jenkins_private.pem";
	private static final String EXECUTE_SCRIPT = "etl-services/scripts/execute_job.sh";
	private static final String BASE_DIRECTORY = "/home/ubuntu/is-platform";
	private static final String SPACE = " ";
	public static String projectName = null;
	private static final String REMOTE_ERROR = "no argument to RemoteJobExecutor";

	public static void main(String args[]) throws Exception {
		if (args.length == 0) {
			// JobSpec not passed. Halt processing with error status -1
			logger.error("Terminating, Job Path not specified !");
			SendEmail.sendErrorMail(SendEmail.DEFAULT_ISPAGERDUTYEMAIL, REMOTE_ERROR, REMOTE_ERROR, REMOTE_ERROR);
			System.exit(CommonConstants.ERROR_CODE);
		}

		String azkabanJobPropFilePath = System.getenv("JOB_PROP_FILE");
		logger.info("JOB_PROP_FILE Retrieved: " + azkabanJobPropFilePath);
		System.setProperty(CommonConstants.AZKABAN_JOB_PROPERTIES_FILE_PATH, azkabanJobPropFilePath);
		System.setProperty(CommonConstants.AZKABAN_ENV_TAG, getAzkabanMachineEnvironmentTag());
		System.setProperty(CommonConstants.JOB_EXECUTION_ENVIRONMENT, "azkaban");

		RuntimeParametersUtility runtimeParametersUtility = new RuntimeParametersUtility();
		Properties azkabanLogProperties = runtimeParametersUtility.getAzkabanProp();
		String runtimeFeedDate = azkabanLogProperties.getProperty(CommonConstants.AZKABAN_FEED_DATE);
		// logger.debug("azkaban properties :
		// "+azkabanLogProperties.toString());
		File permissionFile = null;
		RemoteCommandSSH remoteCommand = null;
		try {
			String server = getRemoteServer();

			if (runtimeFeedDate == null) {
				// String runtimeTimeZone =
				// azkabanLogProperties.getProperty(CommonConstants.AZKABAN_TIME_ZONE);
				String runtimeTimeZone = getTimeZoneFromScheduleSpec(args[0]);
				runtimeFeedDate = currentDateOnTimeZone(runtimeTimeZone);
			}

			String command = buildCommand(args[0], azkabanLogProperties.getProperty(CommonConstants.AZKABAN_EXEC_ID),
					runtimeFeedDate, azkabanLogProperties.toString());

			InputStream permissionInputStream = RemoteJobExecutor.class.getClassLoader()
					.getResourceAsStream(JENKINS_PRIVATE_KEY);
			permissionFile = File.createTempFile("jenkins_private", ".pem");
			logger.info("reading permission file from resources. writing it to " + permissionFile.getAbsolutePath());
			writeInputStreamToFile(permissionInputStream, permissionFile.getAbsolutePath());

			remoteCommand = new RemoteCommandSSH(server, USER, permissionFile.getAbsolutePath(),
					Lists.newArrayList(command));

			List<Integer> exitStatusCode = remoteCommand.execute();
			remoteCommand.close();
			logger.info("Remote Command returned exitStatusCodes : " + exitStatusCode);

			if (hasBadErrorCode(exitStatusCode)) {
				throw new Exception("Remote Command returned Error Status Code");
			}

		} catch (Exception e) {
			logger.error("Terminating, Job Error !");
			logger.error(e.getMessage(), e);
			SendEmail.sendErrorMail(SendEmail.DEFAULT_ISPAGERDUTYEMAIL,
					"[#" + azkabanLogProperties.getProperty(CommonConstants.AZKABAN_EXEC_ID) + "] " + REMOTE_ERROR,
					REMOTE_ERROR + "\n" + StackTraceUtility.stackTraceToString(e), REMOTE_ERROR);
			System.exit(CommonConstants.ERROR_CODE);
		} finally {
			if (remoteCommand != null)
				remoteCommand.close();
			runtimeParametersUtility.close();
			permissionFile.deleteOnExit();
		}
	}

	@SuppressWarnings("unchecked")
	private static String getTimeZoneFromScheduleSpec(String jobSpecPath) {

		String[] attributes = jobSpecPath.split("/");
		String projectName = attributes[0];
		String flowName = attributes[1];

		flowName += "-flows_" + projectName;
		logger.debug("applying timezone of flow : " + flowName);
		// Constructing the schedule-spec path
		StringBuilder scheduleSpecPath = new StringBuilder();
		scheduleSpecPath.append(projectName).append("/").append(JobExecutor.SCHEDULE_SPEC_JSON_PATH);

		JSONParser parser = new JSONParser();

		try {

			Object obj = parser.parse(new FileReader(JobExecutor.SCHEDULE_SPEC_JSON_PATH));

			JSONObject jsonObject = (JSONObject) obj;

			JSONObject azkaban = (JSONObject) jsonObject.get("azkaban");
			// String azkabanName = (String) azkaban.get("azkaban_name");
			JSONArray scheduleList = (JSONArray) azkaban.get("schedule_list");

			Iterator<JSONObject> iterator = scheduleList.iterator();

			while (iterator.hasNext()) {
				JSONObject schedule = (JSONObject) iterator.next();
				String currentProjectName = (String) schedule.get("project_name");
				String currentFlowName = (String) schedule.get("flow_name");
				if (currentProjectName.equals(projectName) && currentFlowName.equals(flowName)) {
					JSONObject time = (JSONObject) schedule.get("time");
					String timezone = (String) time.get("time_zone");
					logger.info("Finally, timezone retrieved - " + timezone);
					return timezone;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "PST";
	}

	private static String currentDateOnTimeZone(String timezone) {
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		date.setTime(convertCalendar(calendar, TimeZone.getTimeZone(timezone)).getTime().getTime());
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(CommonConstants.DEFAULT_DATE_FORMAT);
		return simpleDateFormat.format(date);
	}

	private static Calendar convertCalendar(final Calendar calendar, final TimeZone timeZone) {
		Calendar ret = new GregorianCalendar(timeZone);
		ret.setTimeInMillis(calendar.getTimeInMillis() + timeZone.getOffset(calendar.getTimeInMillis())
				- TimeZone.getDefault().getOffset(calendar.getTimeInMillis()));
		ret.getTime();
		return ret;
	}

	private static boolean hasBadErrorCode(List<Integer> exitCodes) {
		for (Integer element : exitCodes) {
			logger.info("Inside hasBadErrorCode: " + element.toString());
			if (element != 0)
				return true;
		}

		return false;
	}

	public static void writeInputStreamToFile(InputStream inputStream, String sourceFileName) throws IOException {

		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		BufferedWriter writer = new BufferedWriter(new FileWriter(sourceFileName));
		String line = null;
		while (true) {

			line = reader.readLine();
			if (line == null)
				break;
			writer.write(line + "\n");
		}
		writer.close();
		reader.close();
	}

	private static String buildCommand(String jobSpecPath, String execid, String runtimeDate, String azkabanProperties)
			throws InterruptedException, IOException {
		// Extracting the project and job names from the jobSpecPath
		String[] attributes = jobSpecPath.split("/");
		String projectName = attributes[0];
		String jobName = null;

		RuntimeParametersUtility runtimeParametersUtility = new RuntimeParametersUtility();
		Properties azkabanLogProperties = runtimeParametersUtility.getAzkabanProp();
		String azkabanFlowNestedPath = azkabanLogProperties.getProperty(CommonConstants.AZKABAN_FLOW_NESTED_PATH);

		if (azkabanFlowNestedPath != null && !azkabanFlowNestedPath.equals(null) && !azkabanFlowNestedPath.equals("")
				&& !azkabanFlowNestedPath.equals(" ")) {
			jobName = azkabanFlowNestedPath;
			logger.info("Job name passed from nestd path - " + jobName);
		} else {
			String azkabanJobMetaFileName = azkabanLogProperties.getProperty(CommonConstants.AZKABAN_JOB_META_FILE);
			String[] jobMetaDetails = azkabanJobMetaFileName.split("\\.");
			jobName = jobMetaDetails[jobMetaDetails.length - 2];
			logger.info("Job name passed from meta file name - " + jobName);
		}
		runtimeParametersUtility.close();

		logger.info("azkaban job properties -- > " + azkabanProperties);
		// Constructing the client-spec path
		StringBuilder clientSpecPath = new StringBuilder();
		clientSpecPath.append(projectName).append("/").append(JobExecutor.CLIENT_SPEC_JSON_PATH);

		// Constructing the command
		StringBuilder command = new StringBuilder();
		command.append("bash").append(SPACE).append(RemoteJobExecutor.BASE_DIRECTORY).append("/")
				.append(RemoteJobExecutor.EXECUTE_SCRIPT).append(SPACE).append(JobExecutor.class.getName())
				.append(SPACE).append(clientSpecPath.toString()).append(SPACE).append(jobSpecPath).append(SPACE)
				.append("true").append(SPACE).append(RemoteJobExecutor.BASE_DIRECTORY).append(SPACE).append(execid)
				.append(SPACE).append(jobName).append(SPACE).append(runtimeDate).append(SPACE)
				.append(System.getProperty(CommonConstants.AZKABAN_ENV_TAG)).append(" \"").append(azkabanProperties).append("\"");

		return command.toString();
	}

	private static String getClientName(ClientSpec clientSpec) {
		return clientSpec.getClient();
	}

	private static String getRemoteServer() throws Exception {
		// Read the machine name from client-spec;
		ClientSpecInitialization initializer = new ClientSpecInitialization(JobExecutor.CLIENT_SPEC_JSON_PATH);
		ClientSpec clientSpec = initializer.getClientSpecObject();
		String ipAddress = RemoteJobExecutor.getIPAddressRemoteServer(clientSpec);
		projectName = RemoteJobExecutor.getClientName(clientSpec);
		initializer.close();

		return ipAddress;
	}

	private static String getAzkabanMachineEnvironmentTag() throws Exception {
		String instanceId = EC2MetadataUtils.getInstanceId();
		String instanceEndPoint = EC2MetadataUtils.getEC2InstanceRegion();
		EC2Helper ec2Helper = new EC2Helper(instanceId, instanceEndPoint);
		String tag = ec2Helper.describeTag("env");
		return tag;
	}

	private static String getIPAddressRemoteServer(ClientSpec clientSpec) throws Exception {
		String autoScalingGroupName = clientSpec.getAutoScalingGroupName();
		String autoScalingGroupEndpoint = clientSpec.getAutoScalingGroupEndpoint();
		String instanceID = clientSpec.getRemoteMachine();
		String ec2Endpoint = clientSpec.getRemoteMachineEndpoint();
		String remoteIPAddress = null;
		EC2Helper ec2Helper = null;
		if (autoScalingGroupName != null && autoScalingGroupEndpoint != null && !autoScalingGroupName.equals("")
				&& !autoScalingGroupEndpoint.equals("")) {
			logger.debug("autoScalingGroup present in client-spec " + autoScalingGroupName);

			ASGHelper asgHelper = new ASGHelper(autoScalingGroupName, autoScalingGroupEndpoint);
			List<String> asgInstances = asgHelper.getInstances();
			if (asgInstances.size() > 0) {
				ec2Helper = new EC2Helper(asgInstances.get(0), autoScalingGroupEndpoint);
			} else {
				logger.debug("no instance found in autoScalingGroup so taking remote machine instance from client-spec by default");
				ec2Helper = new EC2Helper(instanceID, autoScalingGroupEndpoint);
			}
		} else {
			logger.debug("autoScalingGroup absent in client-spec , taking instance as " + instanceID);
			ec2Helper = new EC2Helper(instanceID, ec2Endpoint);
		}
		remoteIPAddress = ec2Helper.getIPAddress();
		return remoteIPAddress;
	}
}
