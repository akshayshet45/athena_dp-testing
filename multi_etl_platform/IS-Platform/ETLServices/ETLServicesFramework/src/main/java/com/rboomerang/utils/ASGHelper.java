package com.rboomerang.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.autoscaling.AmazonAutoScalingClient;
import com.amazonaws.services.autoscaling.model.AutoScalingGroup;
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsRequest;
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsResult;
import com.amazonaws.services.autoscaling.model.Instance;

public class ASGHelper {
	private final Logger log = LoggerFactory.getLogger(ASGHelper.class);
	private AmazonAutoScalingClient ascClient;
	private String autoScalingGroupName;
	private static final int WAIT_FOR_TRANSITION_INTERVAL = 4000;


	public ASGHelper(String asgName, String asgEndpoint) throws IOException {
		log.debug("Auto Scaling Group Name : " + asgName);
		log.debug("Auto Scaling Group Endpoint : " + asgEndpoint);

		AWSAccess awsAccess = new AWSAccess();
		this.ascClient = new AmazonAutoScalingClient(awsAccess.getAWSAccess());
		
		if (asgEndpoint.equalsIgnoreCase("us-west-2"))
			this.ascClient.setRegion(Region.getRegion(Regions.US_WEST_2));
		else if (asgEndpoint.equalsIgnoreCase("us-west-1"))
			this.ascClient.setRegion(Region.getRegion(Regions.US_WEST_1));
		else if (asgEndpoint.equalsIgnoreCase("us-east-1"))
			this.ascClient.setRegion(Region.getRegion(Regions.US_EAST_1));

		this.autoScalingGroupName = asgName;

		log.info("ASG object initialized");
	}

	public List<String> getInstances() throws InterruptedException {
		DescribeAutoScalingGroupsRequest asgReq = new DescribeAutoScalingGroupsRequest()
				.withAutoScalingGroupNames(this.autoScalingGroupName);
		DescribeAutoScalingGroupsResult res = null;
		boolean doneCheck = false;
		int countcheck = 1;
		while (!doneCheck) {
			try {
				res = this.ascClient.describeAutoScalingGroups(asgReq);
				doneCheck = true;
			} catch (AmazonServiceException e) {
				log.error("exception at retry " + countcheck + " ", e);
				log.debug("retry : " + countcheck);
				countcheck++;
				if (countcheck < 10 && e.getMessage().contains("Request limit exceeded"))
					Thread.sleep(WAIT_FOR_TRANSITION_INTERVAL * countcheck);
				else
					throw e;
			}
		}

		List<String> instanceIds = new ArrayList<String>();
		log.debug("no of instances supported in asg : " + res.getAutoScalingGroups().size());
		for (AutoScalingGroup asg : res.getAutoScalingGroups()) {
			for (Instance ins : asg.getInstances()) {
				String instance = ins.getInstanceId();
				log.debug("asg instance : " + instance);
				instanceIds.add(instance);
			}
		}

		return instanceIds;

	}

}
