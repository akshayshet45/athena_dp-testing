package com.rboomerang.notification.implementation;

import java.util.Map;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rboomerang.common.framework.Status;
import com.rboomerang.notification.Notifies;
import com.rboomerang.utils.SendEmail;

public class NotifyMailImpl implements Notifies {

	private String implName;
	private String mailList;
	private static Logger logger = LoggerFactory.getLogger(NotifyMailImpl.class);
	
	public NotifyMailImpl(String implName, String mailList) {
		this.implName = implName;
		this.mailList = mailList;
	}

	@Override
	public String getName() {
		logger.info("Custom name :: "+ implName);
		return implName;
	}

	@Override
	public Status publish(String subject, Map<String, String> body) {
		SendEmail.send(mailList, subject, new JSONObject(body).toJSONString());
		logger.debug("Mail send successfully");
		return new Status(Status.STATUS_SUCCESS,"Mail sent successfully");
	}

}
