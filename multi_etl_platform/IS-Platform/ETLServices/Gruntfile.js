'use strict';
module.exports = function(grunt){

	// configure tasks
	grunt.initConfig({
		// set any global variables here

		FRAMEWORK_ARTIFACT_NAME: "ETLServicesFramework",
		IMPLEMENTATION_ARTIFACT_NAME: "ETLServicesImplementation",
		ARTIFACT_VERSION: "0.0.1-SNAPSHOT",
		ARTIFACT_EXTENSION: ".jar",
		EC2_REMOTE_LOCATION: "/home/ubuntu/is-platform/etl-services",
		EC2_REMOTE_MACHINE: "", // will be read from the command line parameter

		exec: {
			build: 'mvn clean package -DskipTests',
			deploy: 'echo "Deploying"',
			deployToRemoteAndProject:'echo "Deploying To Project Directory and To remote Machine: <%=EC2_REMOTE_MACHINE%>"'	
		},

		remove: {
			oldArtificat: {
				options: {
					trace: true
				},
				fileList: ['./../../<%= grunt.option("project") %>/libs/*']
			}
		},

		rsync: {
		    options: {
		        args: ["--verbose"],
		        recursive: true,
		    },
		    localProjectImplementation: {
		        options: {
		            src: ['./ETLServicesImplementation/target/<%= grunt.config("IMPLEMENTATION_ARTIFACT_NAME") %>-<%= grunt.config("ARTIFACT_VERSION")%><%= grunt.config("ARTIFACT_EXTENSION") %>','./ETLServicesImplementation/target/lib/*.jar'],
					dest: './../../<%= grunt.option("project") %>/libs/implementation/',
		        }
		    },
		    localProjectFramework: {
		        options: {
		            src: ['./ETLServicesFramework/target/<%= grunt.config("FRAMEWORK_ARTIFACT_NAME") %>-<%= grunt.config("ARTIFACT_VERSION")%><%= grunt.config("ARTIFACT_EXTENSION") %>','./ETLServicesFramework/target/lib/*.jar'],
					dest: './../../<%= grunt.option("project") %>/libs/framework/',
		        }
		    },
		    localProjectScripts: {
		        options: {
		            src: ['./ETLServicesImplementation/scripts'],
					dest: './../../<%= grunt.option("project") %>/libs/implementation/',
		        }
		    },		    
		},
	});

	// Loading required plugins for Grunt
	require('load-grunt-tasks')(grunt);

	// Definition of tasks
	grunt.registerTask('validateParams', 'validateParams', function() {
		if (!grunt.option("project")) {
			grunt.fail.fatal('project must be specified like --project=name');
		}
	});

	grunt.registerTask('validateDeployParams', 'validateParams', function() {
		if (!grunt.option("project")) {
			grunt.fail.fatal('project must be specified like --project=name');
		}
	});

	grunt.registerTask('getRemoteMachine', 'getRemoteMachine', function() {
		var clientConfig = grunt.file.readJSON('./../../' + grunt.option("project") + '/client-spec.json');
		grunt.config.set("EC2_REMOTE_MACHINE", clientConfig.remoteMachine)
		console.log("Deploying to " + grunt.config("EC2_REMOTE_MACHINE"));
	});
	grunt.registerTask('deployInternalWithoutBuild', ['validateParams','rsync:localProjectFramework','rsync:localProjectImplementation','rsync:localProjectScripts']);
	grunt.registerTask('deployInternal', ['validateParams','exec:build','rsync:localProjectFramework','rsync:localProjectImplementation','rsync:localProjectScripts']);
	grunt.registerTask('remoteSyncUp',['validateDeployParams','getRemoteMachine','rsync:remoteMachine']);
	grunt.registerTask('UpdateLibs',['validateParams','getRemoteMachine','exec:build','remove:oldArtificat','exec:deployToRemoteAndProject', 'rsync:localProject','rsync:remoteMachine']);
	grunt.registerTask('build',['exec:build']);
	grunt.registerTask('addLibs',['validateDeployParams','remove:oldArtificat', 'rsync:localProject']);
};
