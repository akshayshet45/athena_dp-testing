#!/bin/bash
set -e

client=$1
CWD=$2
email_id=$3

p_w_d=`pwd`

instance_id=$(aws ec2 run-instances --image-id ami-6989a659 --iam-instance-profile Name="codedeploy-is"  --instance-type t2.micro --security-groups AWS-OpsWorks-Web-Server --output text --query 'Instances[*].InstanceId'  --user-data "`cat ${p_w_d}/startupScriptUserData.sh`")

echo instance-id =$instance_id

aws ec2 create-tags --resources $instance_id --tags Key=Name,Value=$1-remote-etl-machine Key=system,Value=is Key=own,Value=is-team Key=env,Value=prod Key=client,Value=$1
sleep 30;

while state=$(aws ec2 describe-instances --instance-ids $instance_id --output text --query 'Reservations[*].Instances[*].State.Name'); test "$state" = "pending"; do
  sleep 1; echo -n '.'
done;
echo " $state"

ip_address=$(aws ec2 describe-instances --instance-ids $instance_id --output text --query 'Reservations[*].Instances[*].PublicIpAddress')

if [ "$ip_address" == "" ];then   
   echo "EC2 remote machine creation failed for client: " $1 | mail -s "Remote EC2 machine creation failed" ${email_id}
   exit 1
fi

echo "EC2 remote machine sucessfully created for client: $client , ip_address: ${ip_address}"

echo "EC2 remote machine sucessfully created for client: $client , ip_address: ${ip_address}" | mail -s "Remote EC2 machine sucessfully created" ${email_id}
