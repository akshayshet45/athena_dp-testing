#!/bin/bash
apt-get update;
apt-get install -y --force-yes python-pip;
pip install awscli==1.7.47
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
rm -rf /tmp/custom_repo_pull && mkdir -p /tmp/custom_repo_pull
/usr/local/bin/aws s3 sync s3://athena-repo/athena_access-management/ /tmp/custom_repo_pull/ --region us-west-2
rc=$?
if [[ $rc != 0 ]]; then
echo "FALLING BACK TO UNFUDDLE" >> /tmp/startup.txt
/usr/bin/curl https://jenkins:jenkins@athena.unfuddle.com/api/v1/repositories/39/download?path=%2Fsetup.sh | bash >> /tmp/startup.txt 2>&1
exit
fi

tar -xvzf /tmp/custom_repo_pull/athena_access-management.tgz -C /tmp/custom_repo_pull/
cp /tmp/custom_repo_pull/athena_access-management/setup.sh /root/setup.sh
rm -rf /tmp/custom_repo_pull

chmod a+x /root/setup.sh
/root/setup.sh >> /tmp/startup.txt 2>&1
