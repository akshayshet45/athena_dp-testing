import sys

def _removeNonAscii(s): 
	if ord(s)<128:
		return s
	else:
		return ""

with open(sys.argv[1]) as f:
	while True:
		c = f.read(1)
		if not c:
			exit()
		sys.stdout.write(_removeNonAscii(c))
