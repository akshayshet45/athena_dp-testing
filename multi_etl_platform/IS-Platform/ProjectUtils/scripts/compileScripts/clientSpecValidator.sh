#!/bin/bash
set -e

#client='Gilt'
rootdir=`pwd`


echo "-----------------Starting ETL Spec validation  --------------------------"
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.rboomerang.validator.impl.ClientSpecValidator $rootdir
echo "-----------------Ended  ETL Spec  validation -----------------------------"

