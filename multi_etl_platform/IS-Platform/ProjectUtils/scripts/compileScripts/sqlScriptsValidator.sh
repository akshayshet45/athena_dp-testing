#!/bin/bash
set -e
client=$1

#client='Gilt'
rootdir=`pwd`


echo "-----------------Starting sql scripts validation--------------------------"
tablePath='etl/etl-files/tables'
schema_Path='schema'
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.rboomerang.validator.impl.SqlScriptsValidator $rootdir $tablePath $client $schema_Path
echo "-----------------Ended sql scripts validation -----------------------------"

