#!/bin/bash
set -e
client=$1

#client='Gilt'
rootdir=`pwd`


echo "-----------------Starting ETL Spec validation  --------------------------"
tablePath='etl/etl-files/etl-spec.json'
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.rboomerang.validator.impl.EtlSpecValidator $rootdir $tablePath $client
echo "-----------------Ended  ETL Spec  validation -----------------------------"

