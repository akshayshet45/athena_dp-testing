#!/bin/bash
set -e
client=$1

#client='Gilt'
rootdir=`pwd`



echo "-----------------Starting .json files validation for notification messages --------------------------"
echo "-------------------------- dd/flows/ validation -------------------------"
filePath='dd/flows'
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.rboomerang.validator.impl.NotificationMessageValidator $rootdir $filePath $client
echo "-------------------------- e2e/flows/ validation -------------------------"
filePath='e2e/flows'
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.rboomerang.validator.impl.NotificationMessageValidator  $rootdir $filePath $client
echo "-------------------------- etl/flows/ validation -------------------------"
filePath='etl/flows'
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.rboomerang.validator.impl.NotificationMessageValidator  $rootdir $filePath $client
echo "-------------------------- load-etl/flows/ validation -------------------------"
filePath='load-etl/flows'
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar  com.rboomerang.validator.impl.NotificationMessageValidator $rootdir $filePath $client
echo "-------------------------- output-processing/flows/ validation -------------------------"
filePath='output-processing/flows'
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar  com.rboomerang.validator.impl.NotificationMessageValidator $rootdir $filePath $client
echo "-------------------------- po/flows/ validation -------------------------"
filePath='po/flows'
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar  com.rboomerang.validator.impl.NotificationMessageValidator $rootdir $filePath $client
echo "-------------------------- post-etl/flows/ validation -------------------------"
filePath='post-etl/flows'
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar  com.rboomerang.validator.impl.NotificationMessageValidator $rootdir $filePath $client
echo "-------------------------- pre-etl/flows/ validation -------------------------"
filePath='pre-etl/flows'
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar  com.rboomerang.validator.impl.NotificationMessageValidator $rootdir $filePath $client
filePath='pre-etl/flows/properties'
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar  com.rboomerang.validator.impl.NotificationMessageValidator $rootdir $filePath $client
echo "----------------- .job files validation complete -----------------------------"

