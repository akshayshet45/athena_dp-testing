#!/bin/bash
set -e
client=$1

#client='Gilt'
rootdir=`pwd`


echo "-----------------Starting Json Tables validation for bucket sort key --------------------------"
tablePath='etl/etl-files/tables'
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.rboomerang.validator.impl.JsonTableValidator $rootdir $tablePath $client
echo "-----------------Ended Json Tables validation -----------------------------"

