#!/bin/bash
set -e
client=$1

#client='Gilt'
rootdir=`pwd`


echo "-----------------Starting Feed File validation for column Count --------------------------"
feedPath='etl/etl-files/feeds'
headerPath='feeds-metadata/header-files'
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.rboomerang.validator.impl.ColumnCountValidator $rootdir $feedPath $client $headerPath
echo "-----------------Ended Feed File validation for column count -----------------------------"

