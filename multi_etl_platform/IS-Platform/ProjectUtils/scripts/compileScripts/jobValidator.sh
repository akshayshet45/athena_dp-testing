#!/bin/bash
set -e
client=$1

#client='Gilt'
rootdir=`pwd`



#jar cfm ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProjects-0.0.1-SNAPSHOT-jar-with-dependencies.jar myManifest -C ../IS-Platform/ETLUtilsProjects/target/classes
#jar cvf  ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProjects-0.0.1-SNAPSHOT-jar-with-dependencies.jar myManifest -C ../IS-Platform/ETLUtilsProjects/target/classes/com/rboomerang/jobValidator/jobValidator.class
echo "-----------------Starting .job files validation--------------------------"
echo "-------------------------- dd/flows/ validation -------------------------"
filePath='dd/flows'
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.rboomerang.validator.impl.JobValidator $rootdir $filePath $client
echo "-------------------------- e2e/flows/ validation -------------------------"
filePath='e2e/flows'
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.rboomerang.validator.impl.JobValidator  $rootdir $filePath $client
echo "-------------------------- etl/flows/ validation -------------------------"
filePath='etl/flows'
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.rboomerang.validator.impl.JobValidator  $rootdir $filePath $client
echo "-------------------------- load-etl/flows/ validation -------------------------"
filePath='load-etl/flows'
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar  com.rboomerang.validator.impl.JobValidator $rootdir $filePath $client
echo "-------------------------- output-processing/flows/ validation -------------------------"
filePath='output-processing/flows'
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar  com.rboomerang.validator.impl.JobValidator $rootdir $filePath $client
echo "-------------------------- po/flows/ validation -------------------------"
filePath='po/flows'
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar  com.rboomerang.validator.impl.JobValidator $rootdir $filePath $client
echo "-------------------------- post-etl/flows/ validation -------------------------"
filePath='post-etl/flows'
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar  com.rboomerang.validator.impl.JobValidator $rootdir $filePath $client
echo "-------------------------- pre-etl/flows/ validation -------------------------"
filePath='pre-etl/flows'
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar  com.rboomerang.validator.impl.JobValidator $rootdir $filePath $client
echo "----------------- .job files validation complete -----------------------------"

