#!/bin/bash
set -e
  
project_dir=`pwd`
client="$(cat client-spec.json | jq ".client"|sed 's/\"//g')"
  
echo "Project directory : $project_dir"
echo "Client Name :  $client"
  
remoteMachineInstanceID="$(cat client-spec.json | jq ".remoteMachine"|sed 's/\"//g')"
remoteMachineInstanceEndpoint="$(cat client-spec.json | jq ".remoteMachineEndpoint"|sed 's/\"//g')"
  
echo "instanceID : $remoteMachineInstanceID"
echo "instanceEndpoint : $remoteMachineInstanceEndpoint"
  
remoteMachineIP="$(java -cp ./../IS-Platform/ProjectUtils/scripts/GetIPAddress-0.0.1-SNAPSHOT.jar GetIPAddress.GetIPAddress.GetRemoteIPAddress $remoteMachineInstanceID $remoteMachineInstanceEndpoint)"
  
echo "IP Address : $remoteMachineIP"
  
rm -rf libs
mkdir libs
mkdir libs/framework
  
chmod 600 ../IS-Platform/ETLServices/resources/jenkins_private.pem
  
echo "Syncing ETLServices Framework"
scp -i ../IS-Platform/ETLServices/resources/jenkins_private.pem ubuntu@"$remoteMachineIP":/home/ubuntu/is-platform/etl-services/framework/* ./libs/framework/

