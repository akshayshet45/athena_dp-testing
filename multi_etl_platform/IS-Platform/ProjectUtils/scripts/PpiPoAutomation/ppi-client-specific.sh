#!/bin/bash
set -e

client_name=$1
site_name=$2
client_url_pattern=$3
template_value=( "<template_scraper_siteName>" "<template_client_url_pattern>" "<template_database_name>" )
template_replacement=( "$site_name" "$client_url_pattern" "$client_name" )
filepath="temp1/${client_name}_config.yml"
template_location="../IS-Platform/ProjectUtils/scripts/PpiPoAutomation/ConfigTemplates/template_ppi_client-specific.yml
"
home_dir=$HOME

# Check if the config file, is already in git, if yes then exit to avoid rewrite of other configured
# valußes
if [ -e "$home_dir/athena_ppiconfig/ppi/vars/configs/client_specific/${client_name}_config.yml" ]
then
    echo "Error (PPI Client Specific) - ${client_name}_config.yml already exists in Repo, Cannot Override"
    exit 1
fi


#copy file from template to temp location
if [ -f $template_location ]; then
mkdir -p temp1
cp -f $template_location $filepath
else
echo "template file not found : $template_location"
exit 1
fi

#replace value part in copy of template file.
if [ -f $filepath ]; then
i=0
arraylenth=${#template_value[@]}

while [ $i -lt $arraylenth ]
do
#echo $i
echo "${template_replacement[$i]} is replacement of ${template_value[$i]}"
value=$(echo ${template_replacement[$i]} | sed 's/\//\\\//g')
sed -i.bak "s/${template_value[$i]}/$value/g" $filepath

#sed -i.bak "s/${template_value[$i]}/${template_replacement[$i]}/g" $filepath
i=`expr $i + 1`
done

else 
echo "file is not present : $filepath"
exit 1 
fi


#Copy config file into repo
if [ -f $filepath -a -d $home_dir/athena_ppiconfig/ppi/vars/configs/client_specific ]; then
cp -f $filepath $home_dir/athena_ppiconfig/ppi/vars/configs/client_specific/${client_name}_config.yml  
rm -rf temp1
else 
echo "$home_dir/athena_ppiconfig/ppi/vars/configs/client_specific is not exist, please clone/pull repo"
exit 1
fi


