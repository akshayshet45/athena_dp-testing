#!/bin/bash
set -o nounset
set -o errexit

Client=$1
Environment=$2

echo "Environment: $Environment";
echo "Client Name: $Client";

if [ "$Environment" == "Prod" ];
then
	curl "po-"$Client".rboomerang.com:8080/po-client/executeAllStrategies"
else
	curl "po-"$Environment"-"$Client".rboomerang.com:8080/po-client/executeAllStrategies"
fi

ret=$(echo $?)
if [ $ret -ne 0 ];
then
	echo "ERROR : $ret"
      	exit 1
else
        echo "po- $Client executed check PO-dashboard "
fi
