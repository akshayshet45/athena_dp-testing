USE COSMOS;
insert into COSMOS.CRITERIAS(MAP_COLUMN, MAP_TABLE,NAME, TYPE_ID, DROOLS_VARIABLE_NAME, META_DATA_TYPE_ID,is_parent) select T1.column_name,T1.table_name,concat('(P) ',T1.column_name),T3.TYPE_ID,camelCase(LCASE(T1.column_name)),1,1 from information_schema.columns T1 left outer join CRITERIAS T2 on T1.column_name = T2.MAP_COLUMN and T2.is_parent = 1 inner join MYSQL_TYPES T3 on T1.data_type = T3.name where table_schema ='COSMOS' and table_name='DATA_DICTIONARY' and T2.name is NULL;

