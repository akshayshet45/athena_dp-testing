#!/bin/bash
set -e


client_name=$1
env=$2
redshift_host=$3
redshift_username=$4
redshift_password=$5
redshift_url=$6
transport_host=$7
transport_username=$8
transport_password=$9


template_value=("<client>" "<template_redshift_host>" "<template_redshift_username>" "<template_redshift_password>" "<template_redshift_url>" "<template_transport_host>" "<template_transposrt_username>" "<template_transport_password>")
template_replacement=($client_name $redshift_host $redshift_username $redshift_password $redshift_url $transport_host $transport_username $transport_password)

filepath="temp2/${client_namze}_${env}_config.yml"
template_location="../IS-Platform/ProjectUtils/scripts/PpiPoAutomation/ConfigTemplates/template_ss_client_env.yml"
home_dir=$HOME

# Check if the config file, is already in git, if yes then exit to avoid rewrite of other configured
# values
if [ -e "$home_dir/athena_ssconfig/skuselector/vars/configs/client_env/$client_name/${client_name}_${env}_config.yml" ]
then
    echo "Error (SS client env) - ${client_name}_${env}_config.yml already exists in Repo, Cannot Override"
    exit 1
fi

#copy file from template to temp location
if [ -f $template_location ]; then
mkdir -p temp2
cp -f $template_location $filepath
else
echo "template file not found : $template_location"
exit 1;
fi

#replace value part in copy of template file.
if [ -f $filepath ]; then
i=0
arraylenth=${#template_value[@]}

while [ $i -lt $arraylenth ]
do
#echo $i
echo "${template_replacement[$i]} is replacement of ${template_value[$i]}"
value=$(echo ${template_replacement[$i]} | sed 's/\//\\\//g')
sed -i.bak "s/${template_value[$i]}/$value/g" $filepath
i=`expr $i + 1`
done

else 
echo "file is not present : $filepath"
exit 1 
fi



#Copy config file into repo
if [ -f $filepath -a -d $home_dir/athena_ssconfig/skuselector/vars/configs/client_env ]; then
mkdir -p $home_dir/athena_ssconfig/skuselector/vars/configs/client_env/$client_name
cp -f $filepath $home_dir/athena_ssconfig/skuselector/vars/configs/client_env/$client_name/${client_name}_${env}_config.yml  
rm -rf temp2
else 
echo "$home_dir/athena_ssconfig/skuselector/vars/configs/client_env/$client_name, please clone/pull repo"
exit 1
fi

