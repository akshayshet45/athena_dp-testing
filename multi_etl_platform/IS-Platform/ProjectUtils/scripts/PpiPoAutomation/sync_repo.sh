#!/bin/bash
set -e

current_dir=`pwd`
reponame=$1
home_dir=$HOME
echo "start "
cd $home_dir
if [ ! -d "$reponame" ]; then
git clone https://jenkins:jenkins@athena.unfuddle.com/git/$reponame/
echo "repo cloned -  $reponame"
else
cd $reponame
#git reset --hard HEAD
git clean -f
git remote set-url origin https://jenkins:jenkins@athena.unfuddle.com/git/$reponame/
git clean -f
git checkout master;
git pull origin master
cd $current_dir
echo "repo checked out -  $reponame"
fi
