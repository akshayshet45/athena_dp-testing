#!/bin/bash
set -e

client=$3
IS_repo=$2
Data_Pipeline_location=$1
current_dir=`pwd`
etl_path=""
etl_name=""
dev_environment=$4

data_platform_branch=$(cat client-spec.json  | jq ".data_platform_branch"|sed 's/\"//g')
if [ "$data_platform_branch" = "null" -o "$data_platform_branch" = "" ]; then
data_platform_branch="master"
fi
echo "--->Data Platform Branch :  $data_platform_branch"

cd $Data_Pipeline_location
if [ ! -d "athena_data-pipeline" ]; then
git clone http://jenkins:jenkins@athena.unfuddle.com/git/athena_data-pipeline/
git checkout $data_platform_branch
git pull origin $data_platform_branch
echo "repo cloned -  athena_data-pipeline"
else
cd athena_data-pipeline
git reset --hard HEAD
git clean -f
git remote set-url origin http://jenkins:jenkins@athena.unfuddle.com/git/athena_data-pipeline/
git clean -fd
git reset HEAD
git stash
git checkout $data_platform_branch;
git pull origin $data_platform_branch
cd $current_dir
echo "repo checked out -  athena_data-pipeline"
fi
count=$(cat client-spec.json  | jq ".etl_list | length")
client_spec_exist=$count
i=0

if [ $count -eq 0 ]; then
etl_name="$client"
etl_path="etl/etl-files"
job_prefix=""
count=1
fi


while [ $i -lt $count ]
do

if [ $client_spec_exist -ne 0 ]; then
etl_path=$(cat client-spec.json  | jq ".etl_list[$i].etl_path"|sed 's/\"//g')
echo $etl_path
etl_name=$(cat $etl_path/etl-spec.json | jq ".client"|sed 's/\"//g')
echo $etl_name
job_prefix=$(cat $etl_path/etl-spec.json | jq ".jobPrefix"|sed 's/\"//g')
fi

if [ ! -d "$etl_path"  ]; then
echo "please provide etl-files <tables> <feeds> and etl-spec.json on location: $etl_path"
exit 1
else
if [ -n "$etl_name" ]; then
if [ -n $job_prefix ] && [ "$job_prefix" != "null" ]; then
job_prefix_etl_name=$job_prefix$etl_name
else
job_prefix_etl_name="$etl_name"
fi
mkdir -p $Data_Pipeline_location/athena_data-pipeline/validators/ETL_CONFIGS/$job_prefix_etl_name
cp -rf $etl_path/* $Data_Pipeline_location/athena_data-pipeline/validators/ETL_CONFIGS/$job_prefix_etl_name
ls -l $Data_Pipeline_location/athena_data-pipeline/validators/ETL_CONFIGS/$job_prefix_etl_name
else
echo "please provide client name"
fi
fi

if [ "$dev_environment" == "test" ]; then
if [ -f "$Data_Pipeline_location/athena_data-pipeline/validators/ETL_CONFIGS/$job_prefix_etl_name/etl-spec.json" ]; then
rm -f "$Data_Pipeline_location/athena_data-pipeline/validators/ETL_CONFIGS/$job_prefix_etl_name/etl-spec.json"
fi
if [ -f "$etl_path/test-env-etl-spec.json" ]; then
cp -f $etl_path/test-env-etl-spec.json $Data_Pipeline_location/athena_data-pipeline/validators/ETL_CONFIGS/$job_prefix_etl_name/etl-spec.json
echo "--------> Enviroment = $dev_environment"
else
echo "test-client-etl-spec.json is not exist, Please provide $etl_path/test-env-etl-spec.json"
fi
fi
i=`expr $i + 1`
done


#cleaning part
if [ -d temp-zipfiles ]; then
        echo "removing temp-zipfiles"
        rm -rf temp-zipfiles
fi
