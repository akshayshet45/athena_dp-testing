#!/bin/bash
set -e

project_dir=`pwd`
client=$2
skip_validation_queries=$3
semantic_validation_flag=$5
etl_run_date=$6
rootdir=$1
etl_path=$4
#client='kishan'
#skip_validation_queries=true
#etl_run_date='20150725'
#rootdir=`pwd`
#rootdir=$rootdir

echo "home_dir = $rootdir"

querygendir="$rootdir/athena_data-pipeline/validators/QueryGen2"
wcreatordir="$rootdir/athena_data-pipeline/validators/workflow-creator"
wexecutordir="$rootdir/athena_data-pipeline/validators/workflow-executor"

zip_file=$wexecutordir/target/workflow-executor-0.0.1-SNAPSHOT-zip.zip

if [ -f $wexecutordir/target/workflow-executor-0.0.1-SNAPSHOT-zip.zip ]; then
rm -rf $wexecutordir/target/workflow-executor-0.0.1-SNAPSHOT-zip.zip
fi

cd $wcreatordir
mvn clean install -e -U

cd $querygendir
mvn clean install -e -U

cd $wexecutordir
cp -R ../QueryGen2/queryTemplates/* ./queryTemplates/
ls -l ../ETL_CONFIGS/$client
mvn clean package -e -U -Dboomerang.etl.config=../ETL_CONFIGS/$client -Dboomerang.etl.date=$etl_run_date -Dboomerang.skipvalidation=$skip_validation_queries -Dboomerang.semanticvalidation=$semantic_validation_flag
cd
if [ ! -f $wexecutordir/target/workflow-executor-0.0.1-SNAPSHOT-zip.zip ]; then
    echo "$wexecutordir/target/workflow-executor-0.0.1-SNAPSHOT-zip.zip not found!. Build may have failed"
    exit 1
else
if [ -d "$project_dir/temp-zipfiles/$etl_path" ]; then
rm -rf $project_dir/temp-zipfiles/$etl_path
fi
mkdir -p $project_dir/temp-zipfiles/$etl_path
cp -rf $wexecutordir/target/workflow-executor-0.0.1-SNAPSHOT-zip.zip $project_dir/temp-zipfiles/$etl_path
fi

