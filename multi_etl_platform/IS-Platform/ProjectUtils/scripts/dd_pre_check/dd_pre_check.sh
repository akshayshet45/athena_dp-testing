#!/bin/bash
set -e

redshift_name_input=$1
client=$2
#redshift_name_input="redshift"
#client="lowes"
cd /home/ubuntu/is-platform/$client
echo "client: $client"
count=$(cat client-spec.json | jq ".endpoints | length")
        i=0

        while [ $i -lt $count ]
        do
                name=$(cat client-spec.json | jq ".endpoints[$i] | .name" |sed 's/\"//g')
                type=$(cat client-spec.json | jq ".endpoints[$i] | .type" |sed 's/\"//g')


                if [ "$name" == "$redshift_name_input" -a "$type" == "redshift" ]; then
                        redshift_username=$(cat client-spec.json | jq ".endpoints[$i] | .properties.user" |sed 's/\"//g')
                        redshift_passwd=$(cat client-spec.json | jq ".endpoints[$i] | .properties.pass" |sed 's/\"//g')
                        redshift_host=$(cat client-spec.json | jq ".endpoints[$i] | .properties.host" |sed 's/\"//g')
                        redshift_db=$(cat client-spec.json | jq ".endpoints[$i] | .properties.db" |sed 's/\"//g')

                fi

                i=`expr $i + 1`
        done

echo "redshift_username : $redshift_username , "
echo "redshift_host : $redshift_host   , "
echo "redshift_db : $redshift_db   , "
echo "executing generate_std_dd.sh  "
cd scripts
bash generate_std_dd.sh $redshift_host $redshift_username $redshift_passwd $redshift_db
