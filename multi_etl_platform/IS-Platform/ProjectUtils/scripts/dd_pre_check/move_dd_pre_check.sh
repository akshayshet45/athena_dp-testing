#!/bin/bash
set -e

current_dir=`pwd`
home_dir=$HOME

if [ ! -f ../IS-Platform/ProjectUtils/scripts/dd_pre_check/dd_pre_check.sh ] ; then
echo "Please pull latest code from IS-Platform repo. ../IS-Platform/ProjectUtils/scripts/dd_pre_check/dd_pre_check.sh file is not present"
exit 1
fi


if [ ! -d scripts ]; then
mkdir -p scripts
fi
cp -f ../IS-Platform/ProjectUtils/scripts/dd_pre_check/dd_pre_check.sh scripts

if [ -f client-spec.json ]; then
client=$(cat client-spec.json | jq '.client'|sed 's/\"//g')
remoteEC2=$(cat client-spec.json | jq '.remoteMachine'|sed 's/\"//g')
remoteMachineEndpoint=$(cat client-spec.json | jq '.remoteMachineEndpoint'|sed 's/\"//g')
remoteIP=$(java -cp ./../IS-Platform/ProjectUtils/scripts/GetIPAddress-0.0.1-SNAPSHOT.jar GetIPAddress.GetIPAddress.GetRemoteIPAddress $remoteEC2 $remoteMachineEndpoint)
echo "remoteEC2  : $remoteEC2"
echo "remoteMachineEndpoint : $remoteMachineEndpoint"
echo "remoteIP   : $remoteIP"
else
    echo "$current_dir/client-spec.json is not exist"
fi
script="/scripts"
home="/home/ubuntu/is-platform/"
echo "home : $home"
if [ -n $remoteIP ]; then
echo "copying to $remoteIP:$home$client$script"
scp -i ~/.ssh/id_rsa scripts/dd_pre_check.sh  ubuntu@$remoteIP:$home$client$script
fi

cd $home_dir
if [ ! -d "athena_dataprocessing/" ]; then
git clone https://jenkins:jenkins@athena.unfuddle.com/git/athena_dataprocessing/
git checkout master
git pull origin master
echo "repo cloned -  athena_data-pipeline"
else
cd athena_dataprocessing
#git reset --hard HEAD
git clean -f
git remote set-url origin https://jenkins:jenkins@athena.unfuddle.com/git/athena_dataprocessing/
git clean -fd
git reset HEAD
git stash
git checkout master
git pull origin master
cd $current_dir
echo "repo checked out -  athena_data-pipeline"
fi

cp $home_dir/athena_dataprocessing/dd/generate_std_dd.sh scripts
cp $home_dir/athena_dataprocessing/dd/dd_pre_test.py scripts
cp $home_dir/athena_dataprocessing/dd/standard_dd.sql scripts

if [ -n $remoteIP ]; then
echo "copying to $remoteIP:$home$client$script"
scp -i ~/.ssh/id_rsa scripts/generate_std_dd.sh  ubuntu@$remoteIP:$home$client$script
scp -i ~/.ssh/id_rsa scripts/dd_pre_test.py  ubuntu@$remoteIP:$home$client$script
scp -i ~/.ssh/id_rsa scripts/standard_dd.sql  ubuntu@$remoteIP:$home$client$script
fi


