#!/bin/bash

set -e
azkaban_username="azkaban"
azkaban_passwd="azkaban"
project_name="$1"
flow_name="$2"
azkaban_host="http://ec2-54-90-127-6.compute-1.amazonaws.com"

login_response=$(curl -k -X POST --data "action=login&username=$azkaban_username&password=$azkaban_passwd" $azkaban_host:8081)

echo $login_response

status=$(echo $login_response | jq '."status"')
status=$(echo ${status:1:$(echo ${#status}) - 2})

echo $status

if [ "$status" == "success" ]
then
        echo "Azkaban login successfull."
else
        echo "======> ERROR in azkaban login. Check user/passwd or azkaban_host."
        exit 1
fi

session=$(echo $login_response | jq '."session.id"')
session=$(echo ${session:1:$(echo ${#session}) - 2})
echo "session : $session"

curl -k -X POST --data "session.id=$session&name=$project_name&description=$project_name" $azkaban_host:8081/manager?action=create


flow_response=$(curl -k --get --data "session.id=$session&ajax=fetchprojectflows&project=$project_name" $azkaban_host:8081/manager)

count=$(echo $flow_response | jq ".flows | length")
i=0
flow_flag="false"
echo "Available Flows :"
while [ $i -lt $count ]
do
        flow=$(echo $flow_response | jq ".flows[$i] | .flowId")
        flow=$(echo ${flow:1:$(echo ${#flow}) - 2})
        echo "project flow   = $flow"
        if [ "$flow" == "$flow_name" ]
        then
                flow_flag="found"
        fi
                i=`expr $i + 1`
done

if [ "$flow_flag" == "found" ]
then
    echo "selected project flow = $flow_name"
else
    echo "======> ERROR in fetching flow of project [$project_name] from azkaban. Flow name is empty, please check if azkaban project upload was successfully done."
    exit 1
fi

echo "executing project : $project_name"
execid=$(curl -k --get --data "session.id=$session" --data 'ajax=executeFlow' --data "project=$project_name" --data "flow=$flow_name" --data "successEmails=$success_emails" --data "failureEmails=$failure_emails" --data "notifyFailureFirst=true" --data "failureAction=finishPossible" --data "concurrentOption=ignore" --data "successEmailsOverride=true" --data "failureEmailsOverride=true" $azkaban_host:8081/executor | jq ".execid")

if [ "$execid" == "" ]
then
    echo "======> ERROR while starting project [$project_name] on azkaban."
    exit 1
else
        echo "Workflow [$flow_name] of project [$project_name] started successfully."
        echo "azkaban execution id : $execid"
fi

ret=$(echo $?)
if [ $ret -ne 0 ]
then
        echo "======> ERROR : $flow_name start failed"
        exit 1
else
        echo "$flow_name start successfull"
fi
