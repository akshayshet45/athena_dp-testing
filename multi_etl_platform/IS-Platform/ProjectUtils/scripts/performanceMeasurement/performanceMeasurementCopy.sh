#!/bin/bash
set -e

echo "------------------------- started Performance Measurement copy ------------------------"

client="$1"
current_dir=`pwd`
pmRepoName="athena_dataprocessing"
home_dir=$HOME
if [ ! -d $current_dir/output-processing ]; then 
	echo "output-processing is not configured for $client"
	exit 0
fi

if [ ! -f $current_dir/output-processing/output-processing-spec.json ]; then
	echo "$current_dir/output-processing/output-processing-spec.json file is missing"
fi
if [ -f $current_dir/output-processing/output-processing-spec.json ]; then
	custompmNameOnRepo=$(cat $current_dir/output-processing/output-processing-spec.json | jq '.performance_measurement.custom_script_name'|sed 's/\"//g')
fi
custompmname="$custompmNameOnRepo"

echo "start "

cd $home_dir
if [ ! -d "$pmRepoName" ]; then
	git clone https://jenkins:jenkins@athena.unfuddle.com/git/$pmRepoName/
	echo "repo cloned -  $pmRepoName"
else
	cd $pmRepoName
	#git reset --hard HEAD
	git clean -f
	git remote set-url origin https://jenkins:jenkins@athena.unfuddle.com/git/$pmRepoName/
	git clean -f
	git checkout master;
	git pull origin master

	echo "repo checked out -  $pmRepoName"
fi

if [ -f "$current_dir/output-processing/bootstrap.sql" ];then
rm -rf "$current_dir/output-processing/bootstrap.sql"
fi
if [ -f "$current_dir/output-processing/$custompmname" ];then
rm -rf "$current_dir/output-processing/$custompmname"
fi

if [ ! -f "$home_dir/$pmRepoName/measurement/bootstrap.sql" ]; then
	echo "file not found, filename: $home_dir/$pmRepoName/output-processing/bootstrap.sql"
fi
if [ -f "$home_dir/$pmRepoName/measurement/bootstrap.sql" ]; then
		cp  "$home_dir/$pmRepoName/measurement/bootstrap.sql" "$current_dir/output-processing"
fi
if [ ! -f "$home_dir/$pmRepoName/measurement/$custompmname" ]; then
	echo "file not found, filename: $home_dir/$pmRepoName/measurement/$custompmname"
fi
if [ -f "$home_dir/$pmRepoName/measurement/$custompmname" ]; then
		cp "$home_dir/$pmRepoName/measurement/$custompmname" "$current_dir/output-processing"
fi

if [ -f $current_dir/output-processing/bootstrap.sql -a  -f $current_dir/output-processing/$custompmname ]; then
echo "------------------------- Performance Measurement copy completed --------------------"
fi
