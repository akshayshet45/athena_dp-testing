#!/bin/bash
set -e

client=$1
pm_name=""
default_pm_name="bootstrap.sql"
echo $root_dir
current_dir=`pwd`


home="/home/ubuntu/is-platform/"
pm="/output-processing/"
if [ -f $current_dir/client-spec.json ]; then
remoteEC2=$(cat test-env-client-spec.json | jq '.remoteMachine'|sed 's/\"//g')
remoteMachineEndpoint=$(cat test-env-client-spec.json | jq '.remoteMachineEndpoint'|sed 's/\"//g')
echo "remoteEC2  : $remoteEC2"
echo "remoteMachineEndpoint : $remoteMachineEndpoint"
remoteIP=$(java -cp ./../IS-Platform/ProjectUtils/scripts/GetIPAddress-0.0.1-SNAPSHOT.jar GetIPAddress.GetIPAddress.GetRemoteIPAddress $remoteEC2 $remoteMachineEndpoint)

echo "remoteIP   : $remoteIP"
else
echo "$current_dir/client-spec.json is not exist"
fi
custompmNameOnRepo=""

if [ -f $current_dir/output-processing/output-processing-spec.json ]; then
        custompmNameOnRepo=$(cat $current_dir/output-processing/output-processing-spec.json | jq '.performance_measurement.custom_script_name'|sed 's/\"//g')
else
echo "$current_dir/output-processing/output-processing-spec.json not exist... provide script name which should be updated to $remoteIP"
fi

if [ -n $custompmNameOnRepo ];then
pm_name="$custompmNameOnRepo" 
echo "pm_name $pm_name"
scp -r $current_dir$pm$pm_name ubuntu@$remoteIP:$home$client$pm
scp -r $current_dir$pm$default_pm_name ubuntu@$remoteIP:$home$client$pm
else 
echo "custom_script_name is null in  $current_dir/output-processing/output-processing-spec.json"
fi



