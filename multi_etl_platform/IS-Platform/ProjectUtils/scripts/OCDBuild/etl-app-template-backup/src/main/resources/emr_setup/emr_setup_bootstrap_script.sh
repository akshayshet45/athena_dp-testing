#!/bin/bash

set -e
set -x

# Cleanup step
rm -rf /home/hadoop/.tmux
rm -rf /home/hadoop/.tmux.conf
rm -rf /home/hadoop/.s3cfg

project_name={{PROJECT_NAME}}
version={{PROJECT_VERSION}}

# project jar setup on emr
aws s3 cp s3://data-platform-system/emr-deployment/deployment_jars/$project_name/$version/${project_name}-${version}.jar ~/hive/auxlib/ 
aws s3 cp s3://data-platform-system/emr-deployment/deployment_jars/$project_name/$version/${project_name}-${version}.jar ~/hive/lib/ 

# create all hive functions (udf/udaf)
hive -e "create function test_udf as 'org.apache.hadoop.hive.contrib.udf.UDFRowSequence'"