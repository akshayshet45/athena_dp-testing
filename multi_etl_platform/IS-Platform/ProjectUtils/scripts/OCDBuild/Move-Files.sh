#!/bin/bash
set -e

client=$1
current_dir=`pwd`
etl_path=""
env_path="env_configs"
etl_name=""
dev_environment=$2
etl_app_template_path="../IS-Platform/ProjectUtils/scripts/OCDBuild/etl-app-template"

#cleaning part
if [ -d tmp-app ]; then
rm -rf tmp-app
fi


if [ -f "client-spec.json" ]; then

count=$(cat client-spec.json  | jq ".etl_list | length")
client_spec_exist=$count
i=0

if [ $count -eq 0 ]; then
etl_name="$client"
etl_path="etl/etl-files"
job_prefix=""
count=1
fi


while [ $i -lt $count ]
do

if [ $client_spec_exist -ne 0 ]; then
etl_path=$(cat client-spec.json  | jq ".etl_list[$i].etl_path"|sed 's/\"//g')
echo $etl_path
etl_name=$(cat $etl_path/etl-spec.json | jq ".client"|sed 's/\"//g')
echo $etl_name
job_prefix=$(cat $etl_path/etl-spec.json | jq ".jobPrefix"|sed 's/\"//g')
fi

if [ ! -d "$etl_path"  ]; then
echo "please provide etl-files <tables> <feeds> and etl-spec.json on location: $etl_path"
exit 1
else
if [ -n "$etl_name" ]; then
if [ -n $job_prefix ] && [ "$job_prefix" != "null" ]; then
job_prefix_etl_name=$job_prefix$etl_name
else
job_prefix_etl_name="$etl_name"
fi
mkdir -p tmp-app/$job_prefix_etl_name

cp -rf $etl_app_template_path tmp-app/$job_prefix_etl_name
etl_configs_path="tmp-app/$job_prefix_etl_name/etl-app-template/etl_configs"
env_config_path="tmp-app/$job_prefix_etl_name/etl-app-template/env_configs"
common_config_path="tmp-app/$job_prefix_etl_name/etl-app-template"
if [ ! -d "$etl_configs_path" ]; then
mkdir -p $etl_configs_path
fi

if [ ! -d "$env_config_path" ]; then
mkdir -p $env_config_path
fi

cp -rf $etl_path/* $etl_configs_path
cp -rf $env_path/* $env_config_path

#if [ -f "$env_config_path/common.properties" ]; then
#mv -f $env_config_path/common.properties $common_config_path 
#else
#echo "common.properties file is not provided at : $env_path"
#exit 1
#fi

else
echo "please provide client name"
fi
fi

if [ "$dev_environment" == "test" ]; then
if [ -f "$etl_configs_path/etl-spec.json" ]; then
rm -f "$etl_configs_path/etl-spec.json"
fi
if [ -f "$etl_path/test-env-etl-spec.json" ]; then
cp -f $etl_path/test-env-etl-spec.json $etl_configs_path/etl-spec.json
echo "--------> Enviroment = $dev_environment"
else
echo "test-client-etl-spec.json is not exist, Please provide $etl_path/test-env-etl-spec.json"
exit
fi
fi
i=`expr $i + 1`
done
else
echo "client-spec.json is not exist"
fi

#cleaning part
if [ -d temp-zipfiles ]; then
        rm -rf temp-zipfiles
fi
