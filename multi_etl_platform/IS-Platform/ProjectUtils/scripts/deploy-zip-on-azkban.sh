#!/bin/bash
set -e

default_azkaban_host_url="ec2-54-90-127-6.compute-1.amazonaws.com"
default_azkaban_username="azkaban"
default_azkaban_passwd="azkaban"

azkaban_host_url="$default_azkaban_host_url"
azkaban_username="$default_azkaban_username"
azkaban_passwd="$default_azkaban_passwd"
azkaban_host=http://$azkaban_host_url


if [ ! -f client-spec.json ]; then
	echo "client-spec.json is not present. please provide client-spec.json"
	exit 1
fi

if [ -d temp-zipfiles ]; then
        echo "removing temp-zipfiles"
	rm -rf temp-zipfiles
fi
azkaban_name=$(cat client-spec.json | jq '.azkaban_name'|sed 's/\"//g')

        count=$(cat client-spec.json | jq ".endpoints | length")
        i=0

        while [ $i -lt $count ]
        do
                name=$(cat client-spec.json | jq ".endpoints[$i] | .name" |sed 's/\"//g')
                type=$(cat client-spec.json | jq ".endpoints[$i] | .type" |sed 's/\"//g')


                if [ "$name" == "$azkaban_name" -a "$type" == "azkaban" ]; then
                        azkaban_username=$(cat client-spec.json | jq ".endpoints[$i] | .properties.username" |sed 's/\"//g')
                        azkaban_passwd=$(cat client-spec.json | jq ".endpoints[$i] | .properties.password" |sed 's/\"//g')
                        azkaban_host_url=$(cat client-spec.json | jq ".endpoints[$i] | .properties.host" |sed 's/\"//g')
                        azkaban_host=http://$azkaban_host_url
                fi

                i=`expr $i + 1`
        done

# Add user tag to the project name, if no target specified
USER_TAG=`id -un`
azkaban_project_name="$USER_TAG-$1"
if [ "$2" == "prod" ]; then
	azkaban_project_name="$1"
fi

client=$1
consolidated_zip_name='IS-Project-zip'
zip_file="zip-target/$consolidated_zip_name.zip"


if [ -f "zip-target/$consolidated_zip_name.zip" ]; then

login_response=$(curl -k -X POST --data "action=login&username=$azkaban_username&password=$azkaban_passwd" $azkaban_host:8081)

echo $login_response

status=$(echo $login_response | jq '."status"')
status=$(echo ${status:1:$(echo ${#status}) - 2})


if [ "$status" == "success" ]
then
        echo "Azkaban login successfull."
else
        echo "ERROR in azkaban login. Check user/passwd or azkaban_host."
        exit 1
fi

session=$(echo $login_response | jq '."session.id"')
session=$(echo ${session:1:$(echo ${#session}) - 2})
echo "session : $session"

curl -k -X POST --data "session.id=$session&name=$azkaban_project_name&description=$azkaban_project_name" $azkaban_host:8081/manager?action=create

echo " "
echo "--------> Uploading Project : $azkaban_project_name"
return_response=$(curl -k -H "Content-Type: multipart/mixed" -X POST --form "session.id=$session" --form 'ajax=upload' --form "file=@$zip_file;type=application/zip" --form "project=$azkaban_project_name" $azkaban_host:8081/manager)
echo "Response of Uploading Project:  $return_response"
error_flag=$(echo $return_response | jq ".error" |sed 's/\"//g')

echo " "
error_flag=$(echo $error_flag| awk -F'</li></ul>' '{print $1}'| awk -F'<ul><li>' '{print $2}')

i=1

if [ "$error_flag" == "null" -o "$error_flag" == "" ]; then
	echo "ETL deployment successfull on azkaban_host: $azkaban_host , azkaban_project_name: $azkaban_project_name"
else
	echo "ERROR : ETL deployment failed"
	while [ ! -z "$(echo $error_flag| awk -F'</li><li>' '{print $var}' var="$i")" ]
	do
		error_msg=$(echo $error_flag| awk -F'</li><li>' '{print $var}' var="$i")
		echo "Error- $i :"
		if [ ! -z "$(echo ${error_msg} |grep 'but can' |grep 't be found')" ]; then
			echo "Error Reason - Flows not present"
			echo "Error Massage:  $error_msg"
		elif [ ! -z "$(echo ${error_msg} | grep 'cannot find dependency')" ]; then
			echo "Error Reason - Can not Find Dependency "
			echo "Error Massage:  $error_msg"
		elif [ ! -z "$(echo ${error_msg} | grep 'Duplicate job')" ]; then
			echo "Error Reason - Duplicate job Found "
			echo "Error Massage:  $error_msg"
		else 
			echo "Error Massage:  $error_msg"	
		fi
		echo " "
		i=`expr $i + 1`
	done	
	exit 1;	
fi

else
echo "zip-target/$consolidated_zip_name.zip file is not generated please build again"
exit 1
fi
