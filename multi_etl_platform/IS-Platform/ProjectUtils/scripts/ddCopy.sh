#!/bin/bash
set -e

echo "------------------------- started Data Dictionary copy ------------------------"

client="$1"
#customdd="_custom_dd.sql"
current_dir=`pwd`
ddRepoName="athena_dataprocessing"
home_dir=$HOME
if [ ! -d $current_dir/dd ]; then 
	echo "dd is not configured for $client"
	exit 0
fi

if [ ! -f $current_dir/dd/dd-spec.json ]; then
	echo "$current_dir/dd/dd-spec.json file is missing"
	exit 1
fi
if [ -f $current_dir/dd/dd-spec.json ]; then
	customDDNameOnRepo=$(cat $current_dir/dd/dd-spec.json | jq '.customDDName'|sed 's/\"//g')
fi
customddname="$customDDNameOnRepo"

echo "start "

cd $home_dir
if [ ! -d "$ddRepoName" ]; then
	git clone http://jenkins:jenkins@athena.unfuddle.com/git/$ddRepoName/
	echo "repo cloned -  $ddRepoName"
else
	cd $ddRepoName
	#git reset --hard HEAD
	git clean -f
	git remote set-url origin http://jenkins:jenkins@athena.unfuddle.com/git/$ddRepoName/
	git clean -f
	git checkout master;
	git pull origin master

	echo "repo checked out -  $ddRepoName"
fi

rm -rf "$current_dir/dd/standard_dd.sql"
rm -rf "$current_dir/dd/$customddname"

if [ ! -f "$home_dir/$ddRepoName/dd/standard_dd.sql" ]; then
	echo "file not found, filename: $home_dir/$ddRepoName/dd/standard_dd.sql"
fi
if [ -f "$home_dir/$ddRepoName/dd/standard_dd.sql" ]; then
		cp  "$home_dir/$ddRepoName/dd/standard_dd.sql" "$current_dir/dd"
fi
if [ ! -f "$home_dir/$ddRepoName/dd/$customddname" ]; then
	echo "file not found, filename: $home_dir/$ddRepoName/dd/$customddname"
fi
if [ -f "$home_dir/$ddRepoName/dd/$customddname" ]; then
		cp "$home_dir/$ddRepoName/dd/$customddname" "$current_dir/dd"
fi



echo "------------------------- Data Dictionary copy completed --------------------"
