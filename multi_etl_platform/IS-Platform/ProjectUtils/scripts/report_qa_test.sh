client=$1
QA_test_automation=$HOME
current_dir=`pwd`
test_run_date=`date +'%Y%m%d'`

#client='kishan'
#IS_Project_name='IS-Project'


cd $QA_test_automation
if [ ! -d "athena_data-testing" ]; then
git clone https://jenkins:jenkins@athena.unfuddle.com/git/athena_data-testing/
echo "repo cloned -  athena_data-testing"
else
cd $QA_test_automation/athena_data-testing
git reset --hard HEAD
git clean -f
git remote set-url origin https://jenkins:jenkins@athena.unfuddle.com/git/athena_data-testing/
git checkout master;
git pull
echo "repo checked out -  athena_data-testing"
fi

if [ -d "HealthReport" -a -f "$current_dir/etl/etl-files/etl-spec.json" ]; then
cd HealthReport
echo "please wait till report get generated at location : $current_dir/reports/QA_ETL_test_result_$test_run_date.txt "
echo "Test Run Date: $test_run_date" >$current_dir/reports/QA_ETL_test_result_$test_run_date.txt 
echo "$current_dir/etl/etl-files/etl-spec.json"
mvn clean test -Dclientpath=$current_dir/etl/etl-files/etl-spec.json >>$current_dir/reports/QA_ETL_test_result_$test_run_date.txt
fi

cd $current_dir
