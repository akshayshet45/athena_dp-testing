#!/bin/bash
set -e

rootdir=`pwd`

echo "-----------------Starting Canvas Config Automation  --------------------------"
prodSpecConfigPath=$rootdir'/configs/product-spec.json'
echo "-----$prodSpecConfigPath---------"
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.rboomerang.automation.ansibleConfig.CanvasConfiguration "$prodSpecConfigPath"
echo "-----------------Ended Canvas Config Automation -----------------------------"

