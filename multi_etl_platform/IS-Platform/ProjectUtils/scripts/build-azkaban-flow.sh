client=$4
rootdir=$1
IS_zip_name=$2
repo=$3
rootdir=`pwd`

#client='kishan'
#IS_Project_name='IS-Project'
#IS_zip_name='IS-Project-0.0.1-SNAPSHOT-zip.zip'
#repo='athena_is-team-code'


mvn clean install


if [ ! -f "target/$IS_zip_name" ]; then
echo "build might failed as $IS_zip_name is not generated on location: $rootdir/$repo/$IS_Project_name/target/$IS_zip_name"
fi

cd $rootdir
