module.exports = function(grunt) {
	// configure tasks
	grunt.initConfig({

		// global properties used across all projects
		datenow:grunt.template.today('dd_mm_yyyy_HH_MM_ss'),
		clientConfig: grunt.file.readJSON('client-spec.json'),
		productConfig: grunt.file.readJSON('configs/product-spec.json'),
		EC2_REMOTE_LOCATION: "/home/ubuntu/is-platform",
		JARDUMP_FILENAME: './tmp/jardump.txt',
		ETLSERVICES_JAR: './libs/implementation/ETLServicesImplementation-0.0.1-SNAPSHOT.jar',

		exec: {
			ddProjectBuild:'mvn clean package;mkdir zip-target;cp target/IS-Project-0.0.1-SNAPSHOT-zip.zip zip-target/IS-Project-zip.zip;rm -rf target',
			fetchETLServicesFramework:'bash ../IS-Platform/ProjectUtils/scripts/GetETLServicesFramework.sh',
			removeOldLibs:'rm -rf libs;mkdir libs;mkdir libs/framework;mkdir libs/implementation;ssh ubuntu@"$(java -cp ./../IS-Platform/ProjectUtils/scripts/GetIPAddress-0.0.1-SNAPSHOT.jar GetIPAddress.GetIPAddress.GetRemoteIPAddress <%= clientConfig.remoteMachine %> <%= clientConfig.remoteMachineEndpoint %>)" \'rm -rf /home/ubuntu/is-platform/etl-services/*\' ',
			projectNameValidation:'echo "Project Name...${PWD##*/}"',
			buildETLServices:'grunt build --gruntfile ./../IS-Platform/ETLServices/Gruntfile.js',
			etlServicesSyncUpWithoutBuild:'grunt deployInternalWithoutBuild --gruntfile ./../IS-Platform/ETLServices/Gruntfile.js --project=${PWD##*/}',
			etlServicesSyncUp:'grunt deployInternal --gruntfile ./../IS-Platform/ETLServices/Gruntfile.js --project=${PWD##*/}',
			updateLibrariesRemoteAndLocal:'grunt UpdateLibs --gruntfile ./../IS-Platform/ETLServices/Gruntfile.js --project=${PWD##*/}',
			remoteLibsSyncUp:'grunt remoteSyncUp --gruntfile ./../IS-Platform/ETLServices/Gruntfile.js --project=${PWD##*/}',
			packageAzkabanFlows: 'bash ./../IS-Platform/ProjectUtils/scripts/build-flow.sh <%= clientConfig.client  %> <%= grunt.option("feedDate") %> <%= grunt.option("pullattmp") %>',
			packageAzkabanFlowsOCD: 'bash ../IS-Platform/ProjectUtils/scripts/OCDBuild/build-flow.sh ${PWD##*/}',
			createEtlTestReport: 'bash ./../IS-Platform/ProjectUtils/scripts/report_qa_test.sh <%= clientConfig.client  %>',
	    		compileDataplatformFlow: 'echo "Executing compile:compile-azkaban:compile_dataplatform_flow...!!"',
			compilePreEtlFlow: 'echo "Executing compile:compile-azkaban:compile_pre_etl_flow...!!"',
			compilePostEtlFlow: 'echo "Executing compile:compile-azkaban:compile_post_etl_flow...!!"',
			compileDdFlow: 'echo "Executing compile:compile-azkaban:compile_dd_flow...!!"',
			compilePoFlow: 'echo "Executing compile:compile-azkaban:compile_po_flow...!!"',
			compileOutputProcessingFlow: 'echo "Executing compile:compile-azkaban:compile_output_processing_flow...!!"',
			compileJenkins: 'echo "Executing compile_jenkins...!!"',
			compilePushmon: 'echo "Executing compile_pushmon...!!"',

			compilePoConfig: 'echo "Executing compile_po_config...!!"',
			compileSsConfig: 'echo "Executing compile_ss_config...!!"',
			compileCanvasConfig: 'echo "Executing compile_canvas_config...!!"',
			compilePpiConfig: 'echo "Executing compile_ppi_config...!!"',
			compileJobValidator: 'bash ./../IS-Platform/ProjectUtils/scripts/compileScripts/jobValidator.sh <%= clientConfig.client  %>',
			compileSqlScriptsValidator: 'bash ./../IS-Platform/ProjectUtils/scripts/compileScripts/sqlScriptsValidator.sh <%= clientConfig.client  %>',
			compileNotificationValidator: 'bash ./../IS-Platform/ProjectUtils/scripts/NotificationMsgValidator.sh <%= clientConfig.client  %>',
			compileJsonTableValidator: 'bash ./../IS-Platform/ProjectUtils/scripts/compileScripts/JsonTableValidator.sh <%= clientConfig.client  %>',
			compileColumnCountValidator: 'bash ./../IS-Platform/ProjectUtils/scripts/compileScripts/ColumnCountValidator.sh <%= clientConfig.client  %>',
			compileEtlSpecValidator: 'bash ./../IS-Platform/ProjectUtils/scripts/compileScripts/Etl-SpecValidator.sh <%= clientConfig.client  %>',
			compileClientSpecValidator: 'bash ./../IS-Platform/ProjectUtils/scripts/compileScripts/clientSpecValidator.sh',

			scheduleAzkabanFlows : 'bash ./../IS-Platform/ProjectUtils/scripts/schedulingAutomation/azkaban_automation.sh',
			deployAzkabanFlows: 'bash ./../IS-Platform/ProjectUtils/scripts/deploy-zip-on-azkban.sh <%= clientConfig.client  %> <%= grunt.option("target") %>',
			deployJenkins: 'echo "Executing deploy_jenkins...!!"',
			deployPushmon: 'echo "Executing deploy_pushmon...!!"',
			deploySql: 'echo "Executing deploy_sql...!!"',
			deployPoConfig: 'echo "Executing deploy_po_config...!!"',
			deploySsConfig: 'echo "Executing deploy_ss_config...!!"',
			deployCanvasConfig: 'echo "Executing deploy_canvas_config...!!"',
			deployPpiConfig: 'echo "Executing deploy_ppi_config...!!"',
			deployDDscriptCopy: 'bash ./../IS-Platform/ProjectUtils/scripts/ddCopy.sh <%= clientConfig.client %>',
			deployDDonEC2: 'bash ./../IS-Platform/ProjectUtils/scripts/deploy_dd_on_ec2.sh <%= clientConfig.client %>',
			deployMeasurementscriptCopy: 'bash ./../IS-Platform/ProjectUtils/scripts/performanceMeasurement/performanceMeasurementCopy.sh <%= clientConfig.client %>',
			deployMeasurementonEC2: 'bash ./../IS-Platform/ProjectUtils/scripts/performanceMeasurement/deploy_performance_measurement_on_ec2.sh <%= clientConfig.client %>',
			
			generateFeedMetadata: 'bash ./scripts/generate-metadata.sh',
			generateETLServicesJarDump: 'jar -tvf <%= grunt.config("ETLSERVICES_JAR") %> > <%= grunt.config("JARDUMP_FILENAME") %>',

			generateDroolsName:'bash ./../IS-Platform/ProjectUtils/scripts/PpiPoAutomation/po_fetch_droolname.sh',
			addDDtoUI:'bash ./../IS-Platform/ProjectUtils/scripts/PpiPoAutomation/po_criterias_table.sh',
			configurePushmon: 'bash ./../IS-Platform/ProjectUtils/scripts/schedulingAutomation/pushmon_automation.sh <%= clientConfig.client  %>',
			addPpiEtlToProject:'bash ./../IS-Platform/ProjectUtils/scripts/PpiPoAutomation/move_ppi_etl_to_project.sh <%= grunt.option("overwritePPIETL") %>',
			setupAzkabanFlowTrigger:'bash ./../IS-Platform/ProjectUtils/scripts/workFlowTrigger/MoveAzkabanFlowTrigger.sh',
			setupDdPreCheck:'bash ./../IS-Platform/ProjectUtils/scripts/dd_pre_check/move_dd_pre_check.sh',
			generateDdPreCheckConfigFile:'bash ./../IS-Platform/ProjectUtils/scripts/dd_pre_check/generate_dd_pre_check_config_file.sh',
				
			createInputOutputSheet:'java -cp "../IS-Platform/ETLCreationTool/target/ETLCreationTool-0.0.1-SNAPSHOT-jar-with-dependencies.jar" "com.boomerang.ISTeam.ETLCreationTool.ETLInputOutputSheetCreation" "<%= clientConfig.client  %>" "<%= datenow %>"',
			createMappingSheet:'python ../IS-Platform/ETLCreationTool/resources/config_generator/src/xls_workbook_generator/etl_workbook_generator.py "ETLCreationFiles/inputoutputspreadsheet/Iteration-<%= datenow %>/etl_input_output_definition_new.xlsx" "ETLCreationFiles/mappingspreadsheet/Iteration-<%= datenow %>/default_worksheet"',
			shareMappingSheet:'java -cp "../IS-Platform/ETLCreationTool/target/ETLCreationTool-0.0.1-SNAPSHOT-jar-with-dependencies.jar" "com.boomerang.ISTeam.ETLCreationTool.ETLMappingSheetProvider" "<%= clientConfig.client  %>" "<%= datenow %>"',
			generateETLJsonFiles:'java -cp "../IS-Platform/ETLCreationTool/target/ETLCreationTool-0.0.1-SNAPSHOT-jar-with-dependencies.jar" "com.boomerang.ISTeam.ETLCreationTool.ETLJsonFilesGenerator" "<%= clientConfig.client  %>" <%= grunt.option("overwrite") %>',
			preInstallForETLCreationTool:'bash ./../IS-Platform/ETLCreationTool/resources/prerequisite_installation/pre-installation.sh',
			generateAnsibleConfigFiles:'bash ./../IS-Platform/ProjectUtils/scripts/automation/ansible_config/ansible_automation.sh'
		},

		yaml_validator: {
			configs: {
        		src: ['configs/*.yml']
   			}
		},

		jsonlint: {
 			all: {
			    src: [ '**/*.json', '!node_modules/**' ]
			}
		},

		json_schema: {
			common: {
				options: {
					validateFormatsStrict: true
				},
				files: {
					'./../IS-Platform/ProjectUtils/scripts/schema/jenkins-schema.json': ['**/jenkins.json'],
					'./../IS-Platform/ProjectUtils/scripts/schema/pushmon-schema.json': ['**/pushmon.json'],
					'./../IS-Platform/ProjectUtils/scripts/schema/pre-etl-spec-schema.json': ['**/pre-etl-spec.json'],
					'./../IS-Platform/ProjectUtils/scripts/schema/post-etl-spec-schema.json': ['**/post-etl-spec.json'],
					'./../IS-Platform/ProjectUtils/scripts/schema/output-processing-spec-schema.json': ['**/output-processing-spec.json'],
					'./../IS-Platform/ProjectUtils/scripts/schema/schema-schema.json': ['**/schema-spec.json'],
					'./../IS-Platform/ProjectUtils/scripts/schema/client-schema.json': ['client-spec.json'],
					'./../IS-Platform/ProjectUtils/scripts/schema/properties-json-schema.json': ['**/properties/*.json'],
					'./../IS-Platform/ProjectUtils/scripts/schema/etl-spec-schema.json': ['**/etl-spec.json'],
				}
			},
			jobs: {
				options: {
					validateFormatsStrict: true
				},
				files: {
					'./../IS-Platform/ProjectUtils/scripts/schema/pre-etl-job-schema.json': ['tmp/**/pre-etl*.json'],
					'./../IS-Platform/ProjectUtils/scripts/schema/post-etl-job-schema.json': ['tmp/**/post-etl*.json'],
					'./../IS-Platform/ProjectUtils/scripts/schema/dd-job-schema.json': ['tmp/**/dd-*.json'],
					'./../IS-Platform/ProjectUtils/scripts/schema/po-job-schema.json': ['tmp/**/po-*.json'],
					'./../IS-Platform/ProjectUtils/scripts/schema/output-processing-job-schema.json': ['tmp/**/output-processing*.json']
				}
			}

		},
		propertiesToJSON: {
	        	jobs: {
			src: ['tmp/**/*.properties']
			}
		},
		copy: {
		  	jobs: {

			    files: [
			      {
			      	expand: true,
			      	src: ['pre-etl/**/*.job','post-etl/**/*.job','dd/**/*.job','po/**/*.job','output-processing/**/*.job'],
			      	dest: 'tmp/',
				    rename: function(dest, src) {
			          return dest + src.replace(/\.job$/, ".properties");
			        }
			      },
			    ],
			},
		},

		rsync: {
		    options: {
		        args: ["--verbose","--rsync-path='mkdir -p /home/ubuntu/is-platform && rsync'"],
		        recursive: true,
		        deleteAll: true
		    },
		    dist: {
		        options: {
		        	exclude: ["tmp","zip-target","libs","node_modules"],
		            src: '.',
		            dest: '<%= grunt.config("EC2_REMOTE_LOCATION") %>/<%= clientConfig.client %>',
		            host: 'ubuntu@"$(java -cp ./../IS-Platform/ProjectUtils/scripts/GetIPAddress-0.0.1-SNAPSHOT.jar GetIPAddress.GetIPAddress.GetRemoteIPAddress <%= clientConfig.remoteMachine %> <%= clientConfig.remoteMachineEndpoint %>)"'
		        }
		    },
		    library: {
		        options: {
		            src: ['./libs/implementation/*.jar','./libs/implementation/scripts'],
		            dest:  '<%= grunt.config("EC2_REMOTE_LOCATION") %>/etl-services',
		            host: 'ubuntu@"$(java -cp ./../IS-Platform/ProjectUtils/scripts/GetIPAddress-0.0.1-SNAPSHOT.jar GetIPAddress.GetIPAddress.GetRemoteIPAddress <%= clientConfig.remoteMachine %> <%= clientConfig.remoteMachineEndpoint %>)"'
		        }
		    },
		    framework: {
		        options: {
		            src: ['./libs/framework/*.jar'],
		            dest:  '<%= grunt.config("EC2_REMOTE_LOCATION") %>/etl-services/framework',
		            host: 'ubuntu@"$(java -cp ./../IS-Platform/ProjectUtils/scripts/GetIPAddress-0.0.1-SNAPSHOT.jar GetIPAddress.GetIPAddress.GetRemoteIPAddress <%= clientConfig.remoteMachine %> <%= clientConfig.remoteMachineEndpoint %>)"'
		        }
		    },
		},

		search: {
			distkeyCheck: {
		        files: {
		            src: ["schema/*.sql"]
		        },
		        options: {
		        	searchString: /^.*?\bcreate\b(?!.*distkey).*?$/m,
		            logFormat: "console",
		            onComplete: function(matches){
		            	console.log(matches.numMatches);
		            	if(matches.numMatches!=0){
		            		grunt.fail.fatal('distkey is missing');
		            	}
		            }
		        }
		    },

		    sortkeyCheck: {
		        files: {
		            src: ["schema/*.sql"]
		        },
		        options: {
		        	searchString: /^.*?\bcreate\b(?!.*sortkey).*?$/m,
		            logFormat: "console",
		            onComplete: function(matches){
		            	console.log(matches.numMatches);
		            	if(matches.numMatches!=0){
		            		grunt.fail.fatal('sortkey is missing');
		            	}
		            }
		        }
		    },

	        checkForValidatorClass: {
	            files: {
	                src: ["**/*.job"]
	            },
	            options: {
	                searchString: /validator.class=(.*)/g,
	                logFile: './tmp/validator-classes.txt',
	                onMatch: function(match) {
	                	var className = match.match.replace('validator.class=','').replace(/\./g,'/');
	                	if (global.jarDump.indexOf(className) == -1) {
	                		console.log(className + ' Not Found In Job ' + match.file);
	                		global.validationFlag = false;
	                	}
	                },
	            }
	        },

	        checkForProcessorClass: {
	            files: {
	                src: ["**/*.job"]
	            },
	            options: {
	                searchString: /processor.class=(.*)/g,
	                logFile: './tmp/processor-classes.txt',
	                onMatch: function(match) {
	                	var className = match.match.replace('processor.class=','').replace(/\./g,'/');
	                	if (global.jarDump.indexOf(className) == -1) {
	                		console.log(className + ' Not Found In Job ' + match.file);
	                		global.processorFlag = false;
	                	}
	                },
	            }
	        },
	    },
	});

	// Definition of tasks - Internal
	grunt.registerTask("setGlobalVariablesForClassCheck", "", function () {
    	global.validationFlag = true;
    	global.processorFlag = true;
    	global.jarDump = grunt.file.read(grunt.config("JARDUMP_FILENAME"));
    });

	grunt.registerTask("failIfNotFound", "", function () {
    	if (!global.validationFlag) {
    		grunt.fail.fatal("Configured Validator Class Not Found");
    	}

    	if (!global.processorFlag) {
    		grunt.fail.fatal("Configured Processor Class Not Found");
    	}
    });

	// Definition of tasks
	grunt.registerTask('addPpiEtlToProject',['exec:addPpiEtlToProject']);
	grunt.registerTask('generateFeedMetadata', ['exec:generateFeedMetadata']);
	grunt.registerTask('addDDtoUI', ['exec:addDDtoUI']);
	grunt.registerTask('packageInternal', ['exec:packageAzkabanFlows']);
	grunt.registerTask('packageInternalOCD', ['exec:packageAzkabanFlowsOCD']);
	grunt.registerTask('compileAzkabanDataplatformFlow', ['exec:compileDataplatformFlow']);
	grunt.registerTask('scheduleAzkabanFlows', ['exec:scheduleAzkabanFlows']);
	grunt.registerTask('compileAzkabanISFlows', ['copy:jobs','propertiesToJSON:jobs', 'json_schema:jobs']);
	grunt.registerTask('compileAzkaban', ['compileAzkabanDataplatformFlow', 'compileAzkabanISFlows']);
    //grunt.registerTask('compileETLServiceClasses', ['exec:generateETLServicesJarDump','setGlobalVariablesForClassCheck','search:checkForValidatorClass', 'search:checkForProcessorClass','failIfNotFound']);
    grunt.registerTask('compileETLServiceClasses', ['search:checkForValidatorClass', 'search:checkForProcessorClass']);
	grunt.registerTask('compileCommon', [ 'compileETLServiceClasses']);

	grunt.registerTask('compileSql', []);
	grunt.registerTask('compileConfigs', ['exec:compilePoConfig', 'exec:compileSsConfig', 'exec:compileCanvasConfig', 'exec:compilePpiConfig','exec:compileJsonTableValidator']);
    grunt.registerTask('compile', ['jsonlint:all','compileAzkaban', 'exec:compileJobValidator', 'compileSql', 'compileConfigs']);
	//grunt.registerTask('compile', ['compileAll','compileCommon']);


	grunt.registerTask('deployJenkins', ['exec:deployJenkins']);
	grunt.registerTask('deployPushmon', ['exec:deployPushmon']);
	grunt.registerTask('deploySql', ['exec:deploySql']);
	grunt.registerTask('deployConfigs', ['exec:deployPoConfig', 'exec:deploySsConfig', 'exec:deployCanvasConfig', 'exec:deployPpiConfig']);
	grunt.registerTask('deployInternal', ['exec:deployAzkabanFlows', 'deployJenkins', 'deployPushmon', 'deploySql', 'deployConfigs']);
	grunt.registerTask('testReport', ['exec:createEtlTestReport']);
	grunt.registerTask('buildInternal', ['compile', 'packageInternal', 'deployInternal']);
	
	grunt.registerTask('generateMappingSheet',['exec:createInputOutputSheet','exec:createMappingSheet','exec:shareMappingSheet']);
	grunt.registerTask('generateETLJsonFiles',['exec:generateETLJsonFiles']);
	grunt.registerTask('installETLCreationTool',['exec:preInstallForETLCreationTool']);
	grunt.registerTask('setupAzkabanFlowTrigger',['exec:setupAzkabanFlowTrigger']);
	grunt.registerTask('setupDdPreCheck',['exec:setupDdPreCheck','setupAzkabanFlowTrigger']);
	grunt.registerTask('generateDdPreCheckConfigFile',['exec:generateDdPreCheckConfigFile']);
	grunt.registerTask('ETLServiceBuild', ['exec:buildETLServices']);
	grunt.registerTask('default', ['buildInternal']);
	grunt.registerTask('addLibsWithoutBuild',['exec:projectNameValidation','exec:removeOldLibs','exec:etlServicesSyncUpWithoutBuild']);
grunt.registerTask('addLibsWithoutRemove',['exec:projectNameValidation','exec:etlServicesSyncUp','rsync:library','rsync:framework']);
	grunt.registerTask('addLibs',['exec:projectNameValidation','exec:removeOldLibs','exec:etlServicesSyncUp','rsync:library','rsync:framework']);
	grunt.registerTask('ETLServicesSync',['exec:projectNameValidation','exec:updateLibrariesRemoteAndLocal']);
	grunt.registerTask('getETLServicesJar',['exec:fetchETLServicesFramework']);
	grunt.registerTask('deployETL',['getETLServicesJar','packageInternal','deployInternal']);
	grunt.registerTask('deployETLOCD',['getETLServicesJar','packageInternalOCD','deployInternal']);
	//grunt.registerTask('deployETLServices',['rsync:dist','exec:remoteLibsSyncUp']);
	grunt.registerTask('deployETLServices',['exec:deployDDscriptCopy','exec:deployMeasurementscriptCopy','compile','rsync:dist']);
	grunt.registerTask('deployETLServicesNoCompile',['rsync:dist','rsync:library']);
	grunt.registerTask('deployAll',['deployETLServices','deployETL']);
	grunt.registerTask('deployAllOCD',['deployETLServices','deployETLOCD']);
	grunt.registerTask('createAnsibleConfigFiles', ['exec:generateAnsibleConfigFiles']);
	grunt.registerTask('deployDDServiceProject',['getETLServicesJar','rsync:dist','exec:ddProjectBuild','deployInternal']);

};
