package com.boomerang.ISTeam.JsonGenerator;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.json.simple.JSONArray;
import org.json.simple.JSONValue;


public class feed_spec 
{
	String feed_name;
	String feed_id;
	List<feed_column> columns = new ArrayList<feed_column>();
	String Source_endpoint;
	String Source_path;
	String Source_arguments;
	String File_type;
	String Delimiter;
	String Enclosed_by;
	String Escaped_by;
	String line_delim="\\n";
	String Compression;
	String Total_column_count;
	String Skip_header;
	String Filter_criteria;
	String destination;
	String failOnColumnCountMismatch = "true";
	String nullFormat = "null";
	String Client="";
	
	public String getClient() {
		return Client;
	}
	public void setClient(String client) {
		Client = client;
	}
	public String getFeed_name() {
		return feed_name;
	}
	public void setFeed_name(String feed_name) {
		this.feed_name = feed_name;
	}
	public String getFeed_id() {
		return feed_id;
	}
	public void setFeed_id(String feed_id) {
		this.feed_id = feed_id;
	}
	public String getSource_endpoint() {
		return Source_endpoint;
	}
	public void setSource_endpoint(String source_endpoint) {
		Source_endpoint = source_endpoint;
	}
	public String getSource_path() {
		return Source_path;
	}
	public void setSource_path(String source_path) {
		Source_path = source_path;
	}
	public String getSource_arguments() {
		return Source_arguments;
	}
	public void setSource_arguments(String source_arguments) {
		Source_arguments = source_arguments;
	}
	public String getFile_type() {
		return File_type;
	}
	public void setFile_type(String file_type) {
		File_type = file_type;
	}
	public String getDelimiter() {
		return Delimiter;
	}
	public void setDelimiter(String delimiter) {
		Delimiter = delimiter;
	}
	public String getEnclosed_by() {
		return Enclosed_by;
	}
	public void setEnclosed_by(String enclosed_by) {
		Enclosed_by = enclosed_by;
	}
	public String getEscaped_by() {
		return Escaped_by;
	}
	public void setEscaped_by(String escaped_by) {
		Escaped_by = escaped_by;
	}
	public String getLine_delim() {
		return line_delim;
	}
	public void setLine_delim(String line_delim) {
		this.line_delim = line_delim;
	}
	public String getCompression() {
		return Compression;
	}
	public void setCompression(String compression) {
		Compression = compression;
	}
	public String getTotal_column_count() {
		return Total_column_count;
	}
	public void setTotal_column_count(String total_column_count) {
		Total_column_count = total_column_count;
	}
	public String getSkip_header() {
		return Skip_header;
	}
	public void setSkip_header(String skip_header) {
		Skip_header = skip_header;
	}
	public String getFilter_criteria() {
		return Filter_criteria;
	}
	public void setFilter_criteria(String filter_criteria) {
		Filter_criteria = filter_criteria;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getFailOnColumnCountMismatch() {
		return failOnColumnCountMismatch;
	}
	public void setFailOnColumnCountMismatch(String failOnColumnCountMismatch) {
		this.failOnColumnCountMismatch = failOnColumnCountMismatch;
	}
	public String getNullFormat() {
		return nullFormat;
	}
	public void setNullFormat(String nullFormat) {
		this.nullFormat = nullFormat;
	}

	public void addColumnToList(String column_name, String column_type, String column_pos)
	{
		feed_column fd_col = new feed_column();
		fd_col.setColumn_name(column_name);
		fd_col.setColumn_type(column_type);
		fd_col.setColumn_position(column_pos);
		columns.add(fd_col);
		//System.out.println(column_name+" added to feed");
	}
	
	public List<feed_column> getColumnList()
	{
		return columns;
	}
	
	public void displayColumns()
	{
		feed_column col=null;
		for(int i=0;i<columns.size();i++)
		{
			col=columns.get(i);
			/*System.out.println("Column "+(i+1)+" is:");
		    System.out.println("column_name: "+col.getColumn_name());
		    System.out.println("column_type: "+col.getColumn_type());
		    System.out.println("column_index: "+col.getColumn_position());*/
			
		}
	}
	
	public void displayConfigs()
	{
		/*System.out.println("feed_name: "+ this.getFeed_name());
		System.out.println("feed_id: "+ this.getFeed_id());
		System.out.println("Source_endpoint: "+ this.getSource_endpoint());
		System.out.println("Source_path: "+ this.getSource_path());
		System.out.println("Source_arguments: "+ this.getSource_arguments());
		System.out.println("File_type: "+ this.getFile_type());
		System.out.println("Delimiter: "+ this.getDelimiter());
		System.out.println("Enclosed_by: "+ this.getEnclosed_by());
		System.out.println("Escaped_by: "+ this.getEscaped_by());
		System.out.println("line_delim: "+ this.getLine_delim());
		System.out.println("Compression: "+ this.getCompression());
		System.out.println("Total_column_count: "+ this.getTotal_column_count());
		System.out.println("Skip_header: "+ this.getSkip_header());
		System.out.println("Filter_criteria: "+ this.getFilter_criteria());
		System.out.println("destination: "+ this.getDestination());
		System.out.println("failOnColumnCountMismatch: "+ this.getFailOnColumnCountMismatch());
		System.out.println("nullFormat: "+ this.getNullFormat());*/
		this.displayColumns();
	}
	
	public void set_feed_spec_values(org.apache.poi.ss.usermodel.Sheet sheet, String sheet_name)
	{
		try
		{
			Row row;
			Cell cell;
			
			String feed_name_id= sheet_name.substring(sheet_name.lastIndexOf(".")+1);
			feed_name_id="feed_"+feed_name_id;
			//System.out.println("Feed_name = "+feed_name_id);
			//Set feed_name and feed_id
			this.setFeed_name(feed_name_id+".json");
			this.setFeed_id(feed_name_id);
			
			row=sheet.getRow(2);

			//Set Source_endpoint;
			//cell=row.getCell(3);
			//this.setSource_endpoint(read_etl_conf_excel.get_cell_data(row,4));
			this.setSource_endpoint(read_etl_conf_excel.get_cell_data(row,3));
			
			//Set Source_path;
			//cell=row.getCell(4);
			//this.setSource_path(read_etl_conf_excel.get_cell_data(row,4));
			this.setSource_path(read_etl_conf_excel.get_cell_data(row,4));
			
			//Set Source_arguments;
			//cell=row.getCell(5);
			this.setSource_arguments(read_etl_conf_excel.get_cell_data(row,5));
			
			
			//Set File_type;
			//cell=row.getCell(6);
			this.setFile_type(read_etl_conf_excel.get_cell_data(row,6));
			
			
			//Set Delimiter;
			//cell=row.getCell(7);
			this.setDelimiter(read_etl_conf_excel.get_cell_data(row,7));
			
			
			//Set Enclosed_by;
			//cell=row.getCell(8);
			this.setEnclosed_by(read_etl_conf_excel.get_cell_data(row,8));
			
			
			//Set Escaped_by;
			//cell=row.getCell(9);
			this.setEscaped_by(read_etl_conf_excel.get_cell_data(row,9));
			
			
			//Set line_delim="\n";
			
			
			//Set Compression;
			//cell=row.getCell(10);
			this.setCompression(read_etl_conf_excel.get_cell_data(row,10));
			
			
			//Set Total_column_count;
			//cell=row.getCell(11);
			this.setTotal_column_count(read_etl_conf_excel.get_cell_data(row,11));
			
			
			//Set Skip_header;
			//cell=row.getCell(12);
			this.setSkip_header(read_etl_conf_excel.get_cell_data(row,12));
			
			
			//Set Filter_criteria;
			//cell=row.getCell(13);
			this.setFilter_criteria(read_etl_conf_excel.get_cell_data(row,13));
			
			
			//Set destination;
			this.setDestination("feed/${{ID}}");
			
			
			boolean notNull= true;
			int rowCounter=2;
			String col_name;
			String col_type;
			String col_pos;
			try
			{
			while(notNull)
			{
				col_name="";
				col_type="";
				col_pos="";
				row=sheet.getRow(rowCounter);
				cell=row.getCell(0);
				if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK)
				{
					notNull=false;
				}
				else
				{
					col_name=read_etl_conf_excel.get_cell_data(row,0);
					
					//cell=row.getCell(1);
					col_type=read_etl_conf_excel.get_cell_data(row,1);
					
					//cell=row.getCell(2);
					col_pos=read_etl_conf_excel.get_cell_data(row,2);
					
					this.addColumnToList(col_name, col_type, col_pos);
					
					rowCounter++;
				}
				
			}
			}catch(NullPointerException ne)
			{
				//System.out.println("END of Feed");
			}
			
			//String failOnColumnCountMismatch = "true";
			//String nullFormat = "null";
		}
		catch(Exception e)
		{
			//System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public String feed_spec_json(String file)
	{
		Map json=new LinkedHashMap();
		LinkedHashMap src = new LinkedHashMap();
		LinkedHashMap delim = new LinkedHashMap();
		List<LinkedHashMap> s3 = new ArrayList<LinkedHashMap>();
		List<LinkedHashMap> sf = new ArrayList<LinkedHashMap>();
		String newLinechar = "\\\n".toString();
		json.put("id", this.getFeed_id());
		
		src.put("path", this.getSource_path());
		src.put("endpoint", this.getSource_endpoint());
		json.put("source", src);
		
		json.put("destination", this.getDestination());
		json.put("type", this.getFile_type());
		json.put("columnCount", new Integer ((int) Float.parseFloat(this.getTotal_column_count())));
		json.put("failOnColumnCountMismatch", this.getFailOnColumnCountMismatch());
		if(this.getSkip_header().trim().equalsIgnoreCase("true"))
		{
			json.put("skipLines", new Integer(1));
		}
		else
		{
			json.put("skipLines", new Integer(0));
		}
		
		json.put("compression", this.getCompression());
		json.put("nullFormat", this.getNullFormat());
		
		delim.put("field", this.getDelimiter());
		if(! this.getEscaped_by().trim().equals(""))
		{
			delim.put("escaped",this.getEscaped_by());
		}
		
		delim.put("line", this.getLine_delim());
		
		if(! this.getEnclosed_by().trim().equals(""))
		{
			delim.put("enclosed", this.getEnclosed_by());
		}
		
		
		json.put("delimiter", delim);
		
		JSONArray jsonArray = new JSONArray();
		LinkedHashMap s3_tmp;
		LinkedHashMap sf_tmp;
		String col_name="";
		String col_type="";
		String col_position="";
		feed_column fcol; 
		for(int i=0;i<columns.size();i++)
		{
			s3_tmp=new LinkedHashMap();
			sf_tmp=new LinkedHashMap();
			
			fcol=columns.get(i);
			
			col_name=fcol.getColumn_name();
			col_type=fcol.getColumn_type();
			col_position=fcol.getColumn_position();
			int pos= (int) Double.parseDouble(col_position);
			s3_tmp.put("name",col_name);
			s3_tmp.put("position", pos);
			s3_tmp.put("type", col_type);
			s3.add(s3_tmp);
			jsonArray.add(s3.get(i));
		}
		
		json.put("columns", jsonArray);
		
		if(! this.getFilter_criteria().trim().equals(""))
		{
			json.put("whereClause", this.getFilter_criteria());
		}
		else
		{
			json.put("whereClause", "null");
		}
		String jsonText = JSONValue.toJSONString(json);
		jsonText =  jsonText;
		return jsonText;
			
	}
	
}
