package com.boomerang.ISTeam.ETLCreationTool;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.services.drive.Drive;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.Link;
import com.google.gdata.data.batch.BatchOperationType;
import com.google.gdata.data.batch.BatchStatus;
import com.google.gdata.data.batch.BatchUtils;
import com.google.gdata.data.docs.DocumentListEntry;
import com.google.gdata.data.spreadsheet.CellEntry;
import com.google.gdata.data.spreadsheet.CellFeed;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.util.ServiceException;

public class ETLMappingSheetProvider {
	private static Logger logger;
	private final String MAPPINGSPREADSHEETPATH = "ETLCreationFiles/mappingspreadsheet";
	private final String ETL_VERSION = "ETLVersion";
	private final String SPREADSHEET_DOC_ID = "spreadSheetDocId";
	private final String VERSION_CHANGE_FLAG = "versionChangeFlag";
	private final String MAIL_ID = "mailId";
	private final String WORKSHEETNAMEPREFIXONGDRIVE = "table.";

	public static void main(String[] args) throws Exception {

		/* Step-0: Initialization */
		String client = args[0];
		String etlVersion = args[1];

		logger = LoggerFactory.getLogger(ETLMappingSheetProvider.class);
		ETLUtility etl_utility_obj = new ETLUtility();

		GoogleHelpClass ghc = new GoogleHelpClass();
		ETLMappingSheetProvider etlmapobj = new ETLMappingSheetProvider();

		String etl_creation_spec_path = etl_utility_obj
				.getETL_CREATION_SPEC_PATH();
		String mail_id = etlmapobj.getMailId(etl_creation_spec_path,
				etlVersion, etl_utility_obj);
		String destfolderid = etlmapobj.createFoldersOnGdrive(ghc, client,
				etlVersion, mail_id);
		String SourcePath = etlmapobj.MAPPINGSPREADSHEETPATH + "/Iteration-"
				+ etlVersion + "/default_worksheet/" + client
				+ "_etl_workbook.xlsx";
		String destinationFileName = client + "_etl_workbook.xlsx";

		/* Step-1: Uploads mapping spreadsheet to Gdrive */
		DocumentListEntry gdriveFileEntry = ghc.uploadDoc(destfolderid,
				SourcePath, destinationFileName);
		String gdriveFileid = gdriveFileEntry.getDocId();

		/*
		 * Step-2: Create/Put Default Hive Expression in mapping spreadsheet on
		 * Gdrive
		 */
		etlmapobj.setDefaultHiveExpression(etl_utility_obj, gdriveFileid, ghc);
		logger.info("Default Hive Expression is generated successfully ");

		/*
		 * Step-3: copy previous version worksheets to new version worksheet
		 * using google spreadsheet api
		 */
		JSONObject etl_version_spec_object = null;
		if (etlmapobj.fileExists(etl_utility_obj.getETL_VERSION_SPEC_PATH())) {
			etl_version_spec_object = etl_utility_obj.get_etl_version_spec();
		}
		if (etl_version_spec_object != null) {
			String prev_spreadsheet_id = (String) etl_version_spec_object
					.get(etlmapobj.SPREADSHEET_DOC_ID);
			if (prev_spreadsheet_id != null) {
				etlmapobj.moveWorksheet(gdriveFileid, prev_spreadsheet_id, ghc);
			}
		}

		/* Step-4: share mapping spreadsheet with user */
		ghc.ShareDOC(gdriveFileid, mail_id);
		logger.info(destinationFileName
				+ " is shared with you. check your mail box ,user: " + mail_id);
		logger.info("ETL Iteration: " + etlVersion);

		/*
		 * Step-5: Updating etl_version_spec. This etl_version_spec contains
		 * version number, list of Json files for current version, previous
		 * versions. etl_version_spec is contract between
		 * "grunt generateETLJsonFiles" and grunt generateMappingSheet command
		 */
		String etl_version_spec_path = etl_utility_obj
				.getETL_VERSION_SPEC_PATH();
		String backup_version_spec_path = etl_utility_obj
				.getBACKUP_VERSION_SPEC_PATH();
		etlmapobj.updateVersionConfig(etlVersion, etl_version_spec_path,
				gdriveFileid, backup_version_spec_path, etl_utility_obj);

		/*
		 * Step-6: Reporting it
		 */
		ReportInfo report = new ReportInfo();
		report.execute(etlVersion, null, null,
				"Generating Mapping Sheet/WorkBook", client, "-");

	}

	private Boolean fileExists(String path) {
		File file = new File(path);
		if (file.isFile() && file.exists()) {
			return true;
		} else {
			return false;
		}

	}

	void moveWorksheet(String current_spreadsheet_id,
			String prev_spreadsheet_id, GoogleHelpClass ghc)
			throws IOException, ServiceException, GeneralSecurityException {
		SpreadsheetService spreadsheet_service = ghc
				.getspreadsheetServiceObject();
		SpreadsheetEntry current_spreadsheet_entry = ghc
				.getspreadsheet(current_spreadsheet_id);

		List<WorksheetEntry> src_worksheet_list = ghc
				.getListOfAllWorksheet(prev_spreadsheet_id);
		List<WorksheetEntry> dest_worksheet_list = ghc
				.getListOfAllWorksheet(current_spreadsheet_id);

		for (WorksheetEntry src_worksheet : src_worksheet_list) {
			List<CellInfoVO> cellInfoList = new ArrayList<CellInfoVO>();
			for (WorksheetEntry dest_worksheet : dest_worksheet_list) {
				if (src_worksheet
						.getTitle()
						.getPlainText()
						.equalsIgnoreCase(
								dest_worksheet.getTitle().getPlainText())) {

					URL currentworksheetFeedUrl = current_spreadsheet_entry
							.getWorksheetFeedUrl();

					URL src_cellFeedUrl = src_worksheet.getCellFeedUrl();
					URL dest_cellFeedUrl = dest_worksheet.getCellFeedUrl();

					CellFeed src_cellFeed = null;
					CellFeed dest_cellFeed = null;
					try {
						src_cellFeed = spreadsheet_service.getFeed(
								src_cellFeedUrl, CellFeed.class);
						dest_cellFeed = spreadsheet_service.getFeed(
								dest_cellFeedUrl, CellFeed.class);
					} catch (IOException e) {
						logger.error("IO Exception while getting cellFeed from spreadsheetService in setDefaultHiveExpression function,  Exception:"
								+ e.getMessage());
						throw e;
					} catch (ServiceException e) {
						logger.error("ServiceException while getting cellFeed from spreadsheetService in setDefaultHiveExpression function,  Exception:"
								+ e.getMessage());
						throw e;
					}
					List<CellEntry> cellEntries = src_cellFeed.getEntries();
					CellEntry dest_cellEntry = null;
					for (CellEntry cellEntry : cellEntries) {

						com.google.gdata.data.spreadsheet.Cell cell = cellEntry
								.getCell();
						int row_num = cell.getRow();
						int col_num = cell.getCol();
						if (cell != null) {
							cellInfoList.add(new CellInfoVO(row_num, col_num,
									cell.getValue()));
						}

					}

					CellFeed batchRequest = new CellFeed();
					for (CellInfoVO cellAddr : cellInfoList) {

						CellEntry batchEntry = new CellEntry(cellAddr.getRow(),
								cellAddr.getCol(), cellAddr.getVal());
						batchEntry.setId(String.format("%s/%s", dest_worksheet
								.getCellFeedUrl().toString(), cellAddr
								.getIdString()));
						BatchUtils.setBatchId(batchEntry, cellAddr.idString);
						BatchUtils.setBatchOperationType(batchEntry,
								BatchOperationType.UPDATE);
						batchRequest.getEntries().add(batchEntry);
					}

					CellFeed cellFeed1 = spreadsheet_service.getFeed(
							dest_cellFeedUrl, CellFeed.class);
					Link batchLink = cellFeed1.getLink(Link.Rel.FEED_BATCH,
							Link.Type.ATOM);

					spreadsheet_service.setHeader("If-Match", "*");
					CellFeed batchResponse = spreadsheet_service.batch(new URL(
							batchLink.getHref()), batchRequest);
					spreadsheet_service.setHeader("If-Match", null);
					boolean isSuccess = true;
					for (CellEntry entry : batchResponse.getEntries()) {
						String batchId = BatchUtils.getBatchId(entry);
						if (!BatchUtils.isSuccess(entry)) {
							isSuccess = false;
							BatchStatus status = BatchUtils
									.getBatchStatus(entry);
							logger.error("%s failed (%s) %s", batchId,
									status.getReason(), status.getContent());
						}
					}

				}
			}

		}

	}

	void backupSpecMgmt(String etl_version_spec_path,
			String backup_version_spec_path) throws IOException {

		Path source = Paths.get(etl_version_spec_path);
		Path copyPath = Paths.get(backup_version_spec_path);
		try {
			Files.copy(source, copyPath, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			logger.error("IOException while copying file  from Source : "
					+ etl_version_spec_path + "to Dest Path:"
					+ backup_version_spec_path + " in copyNewFile Function "
					+ " Exception: " + e.getMessage());
			throw e;
		}
	}

	private void updateVersionConfig(String etlVersion,
			String etl_version_spec_path, String gdriveFileid,
			String backup_version_spec_path, ETLUtility etl_utility_obj)
			throws IOException, ParseException {
		JSONObject jsonObject = null;
		File f = new File(etl_version_spec_path);
		if (!(f.exists() & f.isFile())) {
			try {
				f.createNewFile();
				jsonObject = new JSONObject();
				backupSpecMgmt(etl_version_spec_path, backup_version_spec_path);
			} catch (IOException e) {
				logger.error("IO Exception while creating file at : "
						+ etl_version_spec_path + " Exception:"
						+ e.getMessage());
				throw e;
			}
		} else {
			backupSpecMgmt(etl_version_spec_path, backup_version_spec_path);

			jsonObject = etl_utility_obj.get_etl_version_spec();
		}

		jsonObject.put(ETL_VERSION, etlVersion);
		jsonObject.put(SPREADSHEET_DOC_ID, gdriveFileid);
		jsonObject.put(VERSION_CHANGE_FLAG, "true");

		try {

			FileWriter file = new FileWriter(etl_version_spec_path);
			file.write(jsonObject.toJSONString());
			file.flush();
			file.close();

		} catch (IOException e) {
			backupSpecMgmt(backup_version_spec_path, etl_version_spec_path);
		}

	}

	private String getMailId(String etl_creation_spec_path, String etlVersion,
			ETLUtility etl_utility_obj) throws IOException, ParseException {

		JSONObject jsonObject = etl_utility_obj.get_etl_creation_spec();
		String mailid = (String) jsonObject.get(MAIL_ID);
		return mailid;
	}

	private String createFoldersOnGdrive(GoogleHelpClass ghc, String client,
			String etlVersion, String mail_id) throws Exception {

		Drive driveService = ghc.getDriveService();
		String ETLCreationFilesid = ghc.getOrCreateFolder(driveService,
				"ETLCreationFiles", null);
		ghc.ShareDOC(ETLCreationFilesid, mail_id);
		String clientfolderid = ghc.getOrCreateFolder(driveService, client,
				ETLCreationFilesid);
		String etlVersionid = ghc.getOrCreateFolder(driveService, etlVersion,
				clientfolderid);
		return etlVersionid;
	}

	public void setDefaultHiveExpression(ETLUtility etl_utility_obj,
			String gdriveFileid, GoogleHelpClass ghc) throws IOException,
			ServiceException, ParseException, GeneralSecurityException {
		SpreadsheetService spreadsheetService = ghc
				.getspreadsheetServiceObject();

		JSONObject etl_spec_json_object = etl_utility_obj
				.getDefaultHiveExpressionSpec();
		Set keyset = etl_spec_json_object.keySet();

		SpreadsheetEntry spreadsheetEntry = ghc.getspreadsheet(gdriveFileid);
		URL spread_sheet_url = spreadsheetEntry.getWorksheetFeedUrl();
		List<WorksheetEntry> worksheets = spreadsheetEntry.getWorksheets();
		int row_num = -1;

		for (WorksheetEntry worksheet : worksheets) {
			List<CellInfoVO> cellInfoList = new ArrayList<CellInfoVO>();

			String Worksheername = worksheet.getTitle().getPlainText();
			if (Worksheername.startsWith(WORKSHEETNAMEPREFIXONGDRIVE)) {
				URL cellFeedUrl = worksheet.getCellFeedUrl();
				CellFeed cellFeed = null;
				CellFeed dest_cellFeed = null;
				try {
					cellFeed = spreadsheetService.getFeed(cellFeedUrl,
							CellFeed.class);
					dest_cellFeed = spreadsheetService.getFeed(cellFeedUrl,
							CellFeed.class);
				} catch (IOException e) {
					logger.error("IO Exception while getting cellFeed from spreadsheetService in setDefaultHiveExpression function,  Exception:"
							+ e.getMessage());
					throw e;
				} catch (ServiceException e) {
					logger.error("ServiceException while getting cellFeed from spreadsheetService in setDefaultHiveExpression function,  Exception:"
							+ e.getMessage());
					throw e;
				}
				List<CellEntry> cellEntries = cellFeed.getEntries();
				// CellEntry cellEntry3=null;
				// CellEntry cellEntry5=null;
				int col_num = 3;
				for (CellEntry cellEntry : cellEntries) {

					com.google.gdata.data.spreadsheet.Cell cell = cellEntry
							.getCell();
					if (cell != null)
						if (keyset.contains(cell.getValue())) {
							String value = (String) etl_spec_json_object
									.get(cell.getValue());

							row_num = cell.getRow();
							col_num = 3;
							cellInfoList.add(new CellInfoVO(row_num, col_num,
									"FROM_EXPRESSION"));
							col_num = 5;
							cellInfoList.add(new CellInfoVO(row_num, col_num,
									value));

						}

				}
				CellFeed batchRequest = new CellFeed();
				for (CellInfoVO cellAddr : cellInfoList) {

					CellEntry batchEntry = new CellEntry(cellAddr.getRow(),
							cellAddr.getCol(), cellAddr.getVal());
					batchEntry.setId(String.format("%s/%s", worksheet
							.getCellFeedUrl().toString(), cellAddr
							.getIdString()));
					BatchUtils.setBatchId(batchEntry, cellAddr.idString);
					BatchUtils.setBatchOperationType(batchEntry,
							BatchOperationType.UPDATE);
					batchRequest.getEntries().add(batchEntry);
				}

				CellFeed cellFeed1 = spreadsheetService.getFeed(cellFeedUrl,
						CellFeed.class);
				Link batchLink = cellFeed1.getLink(Link.Rel.FEED_BATCH,
						Link.Type.ATOM);

				spreadsheetService.setHeader("If-Match", "*");
				CellFeed batchResponse = spreadsheetService.batch(new URL(
						batchLink.getHref()), batchRequest);
				spreadsheetService.setHeader("If-Match", null);
				boolean isSuccess = true;
				for (CellEntry entry : batchResponse.getEntries()) {
					String batchId = BatchUtils.getBatchId(entry);
					if (!BatchUtils.isSuccess(entry)) {
						isSuccess = false;
						BatchStatus status = BatchUtils.getBatchStatus(entry);
						logger.error("%s failed (%s) %s", batchId,
								status.getReason(), status.getContent());
					}
				}

			}

		}
	}

}