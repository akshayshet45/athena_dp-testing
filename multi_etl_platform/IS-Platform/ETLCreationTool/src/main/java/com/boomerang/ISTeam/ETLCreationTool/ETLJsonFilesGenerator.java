package com.boomerang.ISTeam.ETLCreationTool;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boomerang.ISTeam.JsonGenerator.read_etl_conf_excel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;

public class ETLJsonFilesGenerator {

	private static Logger logger;
	private final String VERSION_CHANGE_FLAG = "versionChangeFlag";
	private final String SPREADSHEET_DOC_ID = "spreadSheetDocId";
	private final String ETL_VERSION = "ETLVersion";
	private final String MAPPINGSPREADSHEETPATH = "ETLCreationFiles/mappingspreadsheet";
	private final String TMPJSONFILESPATH = "ETLCreationFiles/Configs";
	private final String GENERATE_JSONS_AT = "generateJsonsAt";
	private final String PREVIOUS_VERSION_FILE_LIST = "previousVersionsFileList";
	private final String CURRENT_VERSION_GENERATED_FILES = "currentVersionGeneratedFiles";
	private final String NEWLY_ADDED_FILES_IN_ETL = "newlyAddedFilesInEtl";

	public static void main(String[] args) throws Exception {

		String overwrite = null;
		if (args.length > 1) {
			overwrite = args[1];
		}
		logger = LoggerFactory.getLogger(ETLJsonFilesGenerator.class);
		ETLJsonFilesGenerator json_generator_obj = new ETLJsonFilesGenerator();
		GoogleHelpClass ghc = new GoogleHelpClass();
		String client = args[0];

		ETLUtility etl_utility_obj = new ETLUtility();
		String etl_version_spec_path = etl_utility_obj
				.getETL_VERSION_SPEC_PATH();
		String backup_version_spec_path = etl_utility_obj
				.getBACKUP_VERSION_SPEC_PATH();

		JSONObject etl_version_spec = etl_utility_obj.get_etl_version_spec();
		String etlVersion = (String) etl_version_spec
				.get(json_generator_obj.ETL_VERSION);
		String version_change_flag = (String) etl_version_spec
				.get(json_generator_obj.VERSION_CHANGE_FLAG);
		String spreadsheet_doc_id = (String) etl_version_spec
				.get(json_generator_obj.SPREADSHEET_DOC_ID);
		String destPath = json_generator_obj.MAPPINGSPREADSHEETPATH
				+ "/Iteration-" + etlVersion + "/" + client
				+ "_etl_workbook.xlsx";
		String destconfigfilepath = json_generator_obj.TMPJSONFILESPATH
				+ "/Iteration-" + etlVersion;
		json_generator_obj.createFolderIFNotExist(destconfigfilepath);

		ghc.downloadFile(spreadsheet_doc_id, destPath);
		logger.info("modified spreadsheet, Doc_id: " + spreadsheet_doc_id
				+ " is downloaded at " + destPath);

		HiveJdbcQueryChecker querychecker = new HiveJdbcQueryChecker();
		querychecker.checkSyntax(destPath);

		/*
		 * //String command =
		 * "bash ../IS-Platform/ETLCreationTool/resources/config_generator/json_creator/etl_json_creation.sh "
		 * // + destPath + " " + destconfigfilepath; //String output =
		 * json_generator_obj.executeCommand(command); //logger.info(output);
		 */
		read_etl_conf_excel jsonGenerator = new read_etl_conf_excel();
		jsonGenerator.execute(destPath, destconfigfilepath);

		List<String> currentVersionFileList = json_generator_obj
				.listFilesInFolder(destconfigfilepath);
		logger.info("Generated File LIST : " + currentVersionFileList);

		List<String> PreviousVersionsFileList = json_generator_obj
				.getFileList(etl_version_spec,
						json_generator_obj.PREVIOUS_VERSION_FILE_LIST);
		logger.info("Previous Versions File List : " + PreviousVersionsFileList);
		List<String> current_version_file_list_fromJson = json_generator_obj
				.getFileList(etl_version_spec,
						json_generator_obj.CURRENT_VERSION_GENERATED_FILES);
		List<String> newly_added_files_list = json_generator_obj.getFileList(
				etl_version_spec, json_generator_obj.NEWLY_ADDED_FILES_IN_ETL);

		List<String> finalFileList = json_generator_obj.getFinalFileList(
				currentVersionFileList, PreviousVersionsFileList);
		JSONObject etl_creation_spec_obj = etl_utility_obj
				.get_etl_creation_spec();
		String generate_jsons_at_path = (String) etl_creation_spec_obj
				.get(json_generator_obj.GENERATE_JSONS_AT);

		List<String> copiedfiles = json_generator_obj.copyJsonFiles(overwrite,
				destconfigfilepath, generate_jsons_at_path, finalFileList,
				currentVersionFileList, newly_added_files_list);

		logger.info("Added files in ETL:  " + copiedfiles);
		if (version_change_flag.equals("true")) {
			json_generator_obj.updateVersionSpec(etl_version_spec,
					PreviousVersionsFileList, currentVersionFileList,
					etl_version_spec_path, backup_version_spec_path,
					copiedfiles, current_version_file_list_fromJson);
		}
		/*
		 * Reporting it
		 */
		ReportInfo report = new ReportInfo();
		report.execute(etlVersion, currentVersionFileList, copiedfiles,
				"Generating ETL Json Files", client, overwrite);

	}

	private List<String> copyJsonFiles(String overwrite,
			String destconfigfilepath, String generate_jsons_at_path,
			List<String> finalFileList, List<String> currentVersionFileList,
			List<String> newly_added_files_list) throws IOException {
		List<String> copiedfiles = null;
		boolean feed_flag = true;
		boolean table_flag = true;
		boolean etl_spec_flag = true;
		boolean replace_flag = false;

		if (overwrite != null) {
			if (overwrite.equals("") || overwrite.equals("null")) {
				overwrite = null;
			}
		}

		if (overwrite == null) {
			copiedfiles = copyFile(destconfigfilepath, generate_jsons_at_path,
					finalFileList, feed_flag, table_flag, etl_spec_flag,
					replace_flag);
		} else if (overwrite.equals("all")) {
			logger.info("Copying all json files");
			replace_flag = true;
			copiedfiles = copyFile(destconfigfilepath, generate_jsons_at_path,
					currentVersionFileList, feed_flag, table_flag,
					etl_spec_flag, replace_flag);
		} else if (overwrite.equals("new")) {

			logger.info("Copying new json files for this Iteration");
			replace_flag = true;
			copiedfiles = copyFile(destconfigfilepath, generate_jsons_at_path,
					newly_added_files_list, feed_flag, table_flag,
					etl_spec_flag, replace_flag);
		} else if (overwrite.equals("allfeeds")) {

			logger.info("Copying all <feeds>.json files");
			replace_flag = true;
			table_flag = false;
			etl_spec_flag = false;
			copiedfiles = copyFile(destconfigfilepath, generate_jsons_at_path,
					currentVersionFileList, feed_flag, table_flag,
					etl_spec_flag, replace_flag);

		} else if (overwrite.equals("alltables")) {
			logger.info("Copying all <tables>.json files");
			replace_flag = true;
			feed_flag = false;
			etl_spec_flag = false;
			copiedfiles = copyFile(destconfigfilepath, generate_jsons_at_path,
					currentVersionFileList, feed_flag, table_flag,
					etl_spec_flag, replace_flag);
		} else {
			replace_flag = true;
			table_flag = true;
			feed_flag = true;
			etl_spec_flag = true;
			String overwrite1 = overwrite.trim();
			List<String> items = Arrays.asList(overwrite1.split("\\s*,\\s*"));
			List<String> filelist = new ArrayList();
			List<String> junkinput = new ArrayList();
			for (String item : items) {
				if (currentVersionFileList.contains("table_" + item + ".json")) {
					filelist.add("table_" + item + ".json");
				}
				if (currentVersionFileList.contains("feed_" + item + ".json")) {
					filelist.add("feed_" + item + ".json");
				}
				if (!(currentVersionFileList
						.contains("table_" + item + ".json") || currentVersionFileList
						.contains("feed_" + item + ".json"))) {
					junkinput.add(item);
				}
			}
			if (!junkinput.isEmpty()) {
				logger.warn("Warning : These overwrite input Values are filtered as junk :"
						+ junkinput);
			}
			if (!filelist.isEmpty()) {
				copiedfiles = copyFile(destconfigfilepath,
						generate_jsons_at_path, filelist, feed_flag,
						table_flag, etl_spec_flag, replace_flag);
			}
		}

		return copiedfiles;
	}

	private void updateVersionSpec(JSONObject etl_version_spec,
			List<String> PreviousVersionsFileList,
			List<String> currentVersionFileList, String etl_version_spec_path,
			String backup_version_spec_path, List<String> copiedfiles,
			List<String> current_version_file_list_fromJson) throws IOException {

		Gson gson = new GsonBuilder().create();
		JsonArray copiedfilesJsonArray = gson.toJsonTree(copiedfiles)
				.getAsJsonArray();
		JsonArray currentVersionFileJsonArray = gson.toJsonTree(
				currentVersionFileList).getAsJsonArray();

		current_version_file_list_fromJson.removeAll(PreviousVersionsFileList);

		PreviousVersionsFileList.addAll(current_version_file_list_fromJson);

		JsonArray PreviousVersionsFileJsonArray = gson.toJsonTree(
				PreviousVersionsFileList).getAsJsonArray();
		etl_version_spec.put(PREVIOUS_VERSION_FILE_LIST,
				PreviousVersionsFileJsonArray);
		etl_version_spec.put(CURRENT_VERSION_GENERATED_FILES,
				currentVersionFileJsonArray);
		etl_version_spec.put(NEWLY_ADDED_FILES_IN_ETL, copiedfilesJsonArray);
		etl_version_spec.put(VERSION_CHANGE_FLAG, "false");

		try {

			FileWriter file = new FileWriter(etl_version_spec_path);
			file.write(etl_version_spec.toJSONString());
			file.flush();
			file.close();

		} catch (IOException e) {
			if (new File(backup_version_spec_path).isFile()) {
				new ETLMappingSheetProvider().backupSpecMgmt(
						backup_version_spec_path, etl_version_spec_path);
			}
			logger.error("IOException while updating file  for Path : "
					+ etl_version_spec_path + " in updateVersionSpec Function "
					+ " Exception: " + e.getMessage());
			throw e;
		}

	}

	private List<String> copyFile(String sourcepath, String destPath,
			List<String> fileList, boolean feed_flag, boolean table_flag,
			boolean client_spec_flag, boolean replace_flag) throws IOException {
		List<String> copiedfiles = new ArrayList<String>();
		createFolderIFNotExist(destPath + "/tables");
		createFolderIFNotExist(destPath + "/feeds");

		Path source;
		Path copyPath = null;
		String destpathstring;

		for (String file : fileList) {
			source = FileSystems.getDefault().getPath(sourcepath, file);

			if (file.startsWith("table_")) {
				if (!table_flag) {
					continue;
				}
				destpathstring = destPath + "/tables";

			} else if (file.startsWith("feed_")) {
				if (!feed_flag) {
					continue;
				}
				destpathstring = destPath + "/feeds";

			} else if (file.startsWith("etl-spec")) {
				if (!client_spec_flag) {
					continue;
				}
				destpathstring = destPath;

			} else {
				continue;
			}
			File destfile = new File(destpathstring + "/" + file);
			if (destfile.exists() && destfile.isFile() && (!replace_flag)) {
				logger.warn("Skip copying file : " + destpathstring + "/"
						+ file + " , the file is already exist");
				continue;
			}
			try {
				copyPath = FileSystems.getDefault().getPath(destpathstring,
						file);
				Files.copy(source, copyPath,
						StandardCopyOption.REPLACE_EXISTING);
				copiedfiles.add(file);

			} catch (IOException e) {
				logger.error("IOException while copying file  from Source : "
						+ sourcepath + "/" + file + "to Dest Path:"
						+ destpathstring + " in copyNewFile Function "
						+ " Exception: " + e.getMessage());
				throw e;
			}

		}

		return copiedfiles;
	}

	private List<String> getFinalFileList(List<String> currentVersionFileList,
			List<String> PreviousVersionFileList) {
		List<String> finalFileList = new ArrayList<String>();
		finalFileList.addAll(currentVersionFileList);
		finalFileList.removeAll(PreviousVersionFileList);

		return finalFileList;

	}

	private List<String> getFileList(JSONObject etl_version_spec,
			String jsonarraykey) {
		List<String> PreviousVersionFileList = new ArrayList<String>();
		JSONArray previousVerFileList = (JSONArray) etl_version_spec
				.get(jsonarraykey);
		if (previousVerFileList != null) {
			int len = previousVerFileList.size();
			for (int i = 0; i < len; i++) {
				PreviousVersionFileList.add(previousVerFileList.get(i)
						.toString());
			}
		}
		return PreviousVersionFileList;
	}

	private List<String> listFilesInFolder(String destconfigfilepath) {
		createFolderIFNotExist(destconfigfilepath);

		File folder = new File(destconfigfilepath);
		File[] listOfFiles = folder.listFiles();
		List<String> filelist = new ArrayList<String>();
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				filelist.add(listOfFiles[i].getName());
			}
		}

		return filelist;
	}

	private boolean createFolderIFNotExist(String path) {

		File tmpdir = new File(path);
		boolean existflag = tmpdir.exists();
		if (!existflag) {
			existflag = tmpdir.mkdirs();
		}
		return existflag;
	}

	private String executeCommand(String command) throws Exception {

		StringBuffer output = new StringBuffer();

		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					p.getInputStream()));

			String line = "";
			while ((line = reader.readLine()) != null) {
				output.append(line + "\n");
			}

		} catch (Exception e) {
			logger.error("Exception while building process for command : "
					+ command + " in executeCommand Function " + " Exception: "
					+ e.getMessage());
			throw e;
		}

		return output.toString();

	}

}
