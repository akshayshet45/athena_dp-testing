package com.boomerang.ISTeam.JsonGenerator;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import static java.lang.System.out;
public class etl_spec {
	
	String Project_name;
	String Client;
	String Environment;
	String System;
	String Owner;
	String Pagerduty_key;
	String etl_start_pushmon_trigger_url;
	String etl_end_pushmon_trigger_url;
	
	List<spec_endpoint> endpoints = new ArrayList<spec_endpoint>();
	
	public String getProject_name() {
		return Project_name;
	}
	public void setProject_name(String project_name) {
		Project_name = project_name;
	}
	public String getClient() {
		return Client;
	}
	public void setClient(String client) {
		Client = client;
	}
	public String getEnvironment() {
		return Environment;
	}
	public void setEnvironment(String environment) {
		Environment = environment;
	}
	public String getSystem() {
		return System;
	}
	public void setSystem(String system) {
		System = system;
	}
	public String getOwner() {
		return Owner;
	}
	public void setOwner(String owner) {
		Owner = owner;
	}
	public String getPagerduty_key() {
		return Pagerduty_key;
	}
	public void setPagerduty_key(String pagerduty_key) {
		Pagerduty_key = pagerduty_key;
	}
	public String getEtl_start_pushmon_trigger_url() {
		return etl_start_pushmon_trigger_url;
	}
	public void setEtl_start_pushmon_trigger_url(
			String etl_start_pushmon_trigger_url) {
		this.etl_start_pushmon_trigger_url = etl_start_pushmon_trigger_url;
	}
	public String getEtl_end_pushmon_trigger_url() {
		return etl_end_pushmon_trigger_url;
	}
	public void setEtl_end_pushmon_trigger_url(String etl_end_pushmon_trigger_url) {
		this.etl_end_pushmon_trigger_url = etl_end_pushmon_trigger_url;
	}
	
	
	public void addEndpointToList(String e_name, String e_type, String e_host, String e_user, String e_pass, String e_db)
	{
		spec_endpoint endp = new spec_endpoint();
		endp.setName(e_name);
		endp.setType(e_type);
		endp.setHost(e_host);
		endp.setUser(e_user);
		endp.setPassword(e_pass);
		endp.setDb(e_db);
		endpoints.add(endp);
		//out.println("Endpoint in Method"+endpoints.size());
	}
	
	public void set_etl_spec_values(org.apache.poi.ss.usermodel.Sheet sheet)
	{
		try
		{
			Row row;
			Cell cell;
			
			row=sheet.getRow(2);
			//Get Project_name
			
			//cell=row.getCell(6);
			//this.setProject_name(read_etl_conf_excel.cellToString(cell));
			this.setProject_name(read_etl_conf_excel.get_cell_data(row, 6));
			
			//Get Client Name
			//cell=row.getCell(7);
			//this.setClient(read_etl_conf_excel.cellToString(cell));
			this.setClient(read_etl_conf_excel.get_cell_data(row, 7));
			
			//Get Environment
			//cell=row.getCell(8);
			//this.setEnvironment(read_etl_conf_excel.cellToString(cell));
			this.setEnvironment(read_etl_conf_excel.get_cell_data(row, 8));
					
			//Get System;
			//cell=row.getCell(9);
			//this.setSystem(read_etl_conf_excel.cellToString(cell));
			this.setSystem(read_etl_conf_excel.get_cell_data(row, 9));
			
			//Get Owner;
			//cell=row.getCell(10);
			//this.setOwner(read_etl_conf_excel.cellToString(cell));
			this.setOwner(read_etl_conf_excel.get_cell_data(row, 10));
					
			//Get Pagerduty_key;
			//cell=row.getCell(11);
			//this.setPagerduty_key(read_etl_conf_excel.cellToString(cell));
			this.setPagerduty_key(read_etl_conf_excel.get_cell_data(row, 11));
			
			//Get etl_start_pushmon_trigger_url;
			//cell=row.getCell(12);
			//this.setEtl_start_pushmon_trigger_url(read_etl_conf_excel.cellToString(cell));
			this.setEtl_start_pushmon_trigger_url(read_etl_conf_excel.get_cell_data(row, 12));
			
			//Get etl_end_pushmon_trigger_url;
			//cell=row.getCell(13);
			//this.setEtl_end_pushmon_trigger_url(read_etl_conf_excel.cellToString(cell));
			this.setEtl_end_pushmon_trigger_url(read_etl_conf_excel.get_cell_data(row, 13));
			
			String e_name="";
			String e_type="";
			String e_host="";
			String e_user="";
			String e_pass="";
			String e_db="";
			boolean notNull=true;
			int rowCounter=2;
			
			//Getting End Points
			while(notNull)
			{
					row=sheet.getRow(rowCounter);
					cell=row.getCell(0);
					if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK)
					{
						notNull=false;
					}
					else
					{
						//Get Name
						e_name=read_etl_conf_excel.get_cell_data(row, 0);
						//e_name=read_etl_conf_excel.cellToString(cell);
						
						//GEt type;
						//cell=row.getCell(1);
						//e_type=read_etl_conf_excel.cellToString(cell);
						e_type=read_etl_conf_excel.get_cell_data(row, 1);
						
						//GEt host;
						//cell=row.getCell(2);
						//e_host=read_etl_conf_excel.cellToString(cell);
						e_host=read_etl_conf_excel.get_cell_data(row, 2);
								
						//Get user;
						//cell=row.getCell(3);
						//e_user=read_etl_conf_excel.cellToString(cell);
						e_user=read_etl_conf_excel.get_cell_data(row, 3);
						
						//Get password;
						//cell=row.getCell(4);
						//e_pass=read_etl_conf_excel.cellToString(cell);
						e_pass=read_etl_conf_excel.get_cell_data(row, 4);
						
						//Get db;
						//cell=row.getCell(5);
						//e_db=read_etl_conf_excel.cellToString(cell);
						e_db=read_etl_conf_excel.get_cell_data(row, 5);
						
						this.addEndpointToList(e_name, e_type, e_host, e_user, e_pass, e_db);
						e_name=""; e_type=""; e_host=""; e_user=""; e_pass=""; e_db="";
						rowCounter++;
					}
			}
			
		}
		catch(Exception e)
		{
			e.getStackTrace();
		}	
	}
	
	
	public String etl_spec_josn(String file, List<feed_spec> feedSpec, List<table_spec> tableSpec)
	{
		Map json=new LinkedHashMap();
		LinkedHashMap az = new LinkedHashMap();
		List<LinkedHashMap> s3 = new ArrayList<LinkedHashMap>();
		List<LinkedHashMap> sf = new ArrayList<LinkedHashMap>();
		LinkedHashMap rs = new LinkedHashMap();
		
		
		json.put("client", this.getClient());
		
		feed_spec fcol=null;
		JSONArray jsonArrayF = new JSONArray();
		for(int i=0;i<feedSpec.size();i++)
		{
			fcol=feedSpec.get(i);
			jsonArrayF.add(fcol.getFeed_name());
		}
		json.put("feeds", jsonArrayF);
		
		JSONArray jsonArrayT = new JSONArray();
		table_spec tcol=null;
		
		for(int i=0;i<tableSpec.size();i++)
		{
			tcol=tableSpec.get(i);
			jsonArrayT.add(tcol.getTable_name());
		}
		json.put("tables", jsonArrayT);
		
		az.put("host","azkaban-db-prod-oregon.ckr6oyyfiopw.us-west-2.rds.amazonaws.com");
		az.put("db","dpstats");
		az.put("user","azkaban");
		az.put("pass","azkaban123");
		json.put("dataplatformDest", az);
		
		
		String e_name="";
		String e_type="";
		String e_host="";
		String e_user="";
		String e_pass="";
		String e_db="";
		spec_endpoint endp;
		
		JSONArray jsonArray = new JSONArray();
		LinkedHashMap s3_tmp;
		LinkedHashMap sf_tmp;
		//out.println("Endpoint in Json"+endpoints.size());
		for(int i=0;i<endpoints.size();i++)
		{
			s3_tmp=new LinkedHashMap();
			
			sf_tmp=new LinkedHashMap();
			
			
			endp=endpoints.get(i);
			e_name=endp.getName();
			e_type=endp.getType();
			e_host=endp.getHost();
			e_user=endp.getUser();
			e_pass=endp.getPassword();
			e_db=endp.getDb();
			
			if(e_type.equals("s3"))
			{
				s3_tmp.put("name", e_name);
				s3_tmp.put("type",e_type);
				s3.add(s3_tmp);
			}
			else if(e_type.equals("redshift") || e_type.equals("mysql"))
			{
				sf=new ArrayList<LinkedHashMap>();
				e_host=e_host.trim()+":5439";
				sf_tmp.put("host",e_host );
				sf_tmp.put("db", e_db);
				sf_tmp.put("user", e_user);
				sf_tmp.put("pass", e_pass);
				sf.add(sf_tmp);
				
				s3_tmp.put("name",e_name);
				s3_tmp.put("type",e_type);
				s3_tmp.put("properties",sf.get(0));
				s3.add(s3_tmp);
			}
			else if(e_type.equals("sftp"))
			{
				sf=new ArrayList<LinkedHashMap>();
				e_host="sftp://"+e_host.trim();
				sf_tmp.put("host",e_host );
				sf_tmp.put("user", e_user);
				sf_tmp.put("pass", e_pass);
				sf.add(sf_tmp);
				
				s3_tmp.put("name",e_name);
				s3_tmp.put("type",e_type);
				s3_tmp.put("properties",sf.get(0));
				s3.add(s3_tmp);
			}
			
			
			jsonArray.add(s3.get(i));
		}
		json.put("endpoints", jsonArray);
		json.put("logglyKey", "fb0db062-9ac1-4b2a-90be-13c29cf16b4c");
		json.put("pagerdutyKey", this.getPagerduty_key());
		json.put("skipValidation", new Boolean(false));
		json.put("s3Bucket", "data-platform-insights");
		json.put("emrClientTag", this.getClient());
		json.put("version", new Integer(0));
		String jsonText = JSONValue.toJSONString(json); 
		jsonText =  jsonText.replace("/", "");
		return jsonText;
	}
	
}
