package com.boomerang.ISTeam.ETLCreationTool;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.datanucleus.store.query.QueryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;

public class HiveJdbcQueryChecker {

	private static Logger logger;
	private final String LOG4JPATH = "../IS-Platform/ETLCreationTool/log4j.properties";
	private final String WORKSHEETNAMEPREFIX = "table.";
	private final String CONNECTION_STRING = "jdbc:hive2://dev.dp.rboomerang.com:10000/";

	public void checkSyntax(String SourcePath) throws SQLException, IOException {
		PropertyConfigurator.configure(LOG4JPATH);
		HiveJdbcQueryChecker query_checker = new HiveJdbcQueryChecker();
		logger = LoggerFactory.getLogger(HiveJdbcQueryChecker.class);
		Connection connection = query_checker.createConnection();
		query_checker.checkExpressionSyntax(connection, SourcePath);
		logger.info("Hive Expression Syntax is correct in " + SourcePath);
	}

	private String getCellValue(XSSFCell cell) {

		String cellvalue = null;
		Date celldate;
		double celldouble;
		switch (cell.getCellType()) {
		case XSSFCell.CELL_TYPE_STRING:
			cellvalue = cell.getRichStringCellValue().getString();
			break;
		case XSSFCell.CELL_TYPE_NUMERIC:
			if (DateUtil.isCellDateFormatted(cell)) {
				celldate = cell.getDateCellValue();
				DateFormat dateFormat = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				cellvalue = dateFormat.format(celldate);
			} else {
				celldouble = cell.getNumericCellValue();
				cellvalue=""+celldouble;
			}
			break;
		case XSSFCell.CELL_TYPE_BOOLEAN:
			cellvalue=""+cell.getBooleanCellValue();
			break;
		case XSSFCell.CELL_TYPE_FORMULA:
			cellvalue=cell.getCellFormula();
			break;
		}
		return cellvalue;
	}

	private void checkExpressionSyntax(Connection connection, String SourcePath)
			throws IOException, SQLException {
		XSSFWorkbook workbook = getworkbookObject(SourcePath);
		for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
			String sheetname = workbook.getSheetName(i);
			if (sheetname.startsWith(WORKSHEETNAMEPREFIX)) {
				// System.out.println(sheetname);
				XSSFSheet worksheet = workbook.getSheetAt(i);
				XSSFCell cell = null;

				int j = 2;
				String query = "";
				while (true) {
					cell = worksheet.getRow(j).getCell(2);
					String cellvalue = getCellValue(cell);
					if (cellvalue == null || cellvalue.equals("")
							|| cell == null) {
						break;
					}
					if (cellvalue.equalsIgnoreCase("FROM_EXPRESSION")) {
						try {

							query = getCellValue(worksheet.getRow(j).getCell(4));
							executeQuery(connection, query);
						} catch (SQLException e) {
							logger.error("Reason for Exception : Syntaxt is wrong at Expression : "
									+ query
									+ " in "
									+ sheetname
									+ " for Column Name : "
									+ getCellValue(worksheet.getRow(j).getCell(0)));
											
							throw e;
						}

					}
					j++;
				}

			}
		}
	}

	private XSSFWorkbook getworkbookObject(String SourcePath)
			throws IOException {
		

		File xslxfile = new File(SourcePath);
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(xslxfile);
		} catch (FileNotFoundException e) {
			logger.error("FileNotFoundException "
					+ " in insertDataIntoInputOutputFile Function, File Location: "
					+ SourcePath + " Exception: " + e.getMessage());

			throw e;
		}

		XSSFWorkbook workbook = null;

		try {
			workbook = new XSSFWorkbook(fis);
		} catch (IOException e) {
			logger.error("IOException in insertDataIntoInputOutputFile Function during creating object of XSSFWorkbook, "

					+ " Exception: " + e.getMessage());
			throw e;

		}
		return workbook;
	}

	private void executeQuery(Connection connection, String query)
			throws SQLException {
		QueryResult result = null;

		Statement stmt = null;
		long startMillis;
		try {

			connection = createConnection();
			stmt = connection.createStatement();
			String sql = "EXPLAIN EXTENDED select " + query;
			ResultSet res = stmt.executeQuery(sql);
			if (res.next()) {
				res.getString(1);
			}
		}

		catch (SQLException e) {

			if (e.getMessage().contains("ParseException")) {
				logger.error("HIVE reports ParseException : " + e.getMessage());
				throw e;
			} else {
				// logger.warn("Warning :" + e.getMessage());
			}
		} finally {
			try {
				if (stmt != null && !stmt.isClosed()) {
					stmt.close();
				}
			} catch (Exception e) {
				// LOGGER.error("Error while trying to close stmt", e);
			}
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (Exception e) {
				// LOGGER.error("Error while trying to close connection", e);
			}
		}

	}

	private Connection createConnection() throws SQLException {

		String connectionString = CONNECTION_STRING;

		try {
			Class.forName("org.apache.hive.jdbc.HiveDriver");
			Connection con = DriverManager.getConnection(connectionString,
					"hadoop", "");
			// LOGGER.info("Connected successfully");
			return con;
		} catch (ClassNotFoundException e) {
			throw Throwables.propagate(e);
		}
	}

}
