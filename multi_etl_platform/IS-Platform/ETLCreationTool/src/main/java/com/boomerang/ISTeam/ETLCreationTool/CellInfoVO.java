package com.boomerang.ISTeam.ETLCreationTool;

public class CellInfoVO {
    public int row;
    public int col;
    public String val;
    public String idString;
  
    public CellInfoVO(int row, int col, String val) {
        this.row = row;
        this.col = col;
        this.val = val;
        this.idString = String.format("R%sC%s", row, col);
    }
  
    public int getRow() {
        return row;
    }
  
    public void setRow(int row) {
        this.row = row;
    }
  
    public int getCol() {
        return col;
    }
  
    public void setCol(int col) {
        this.col = col;
    }
  
    public String getVal() {
        return val;
    }
  
    public void setVal(String val) {
        this.val = val;
    }
  
    public String getIdString() {
        return idString;
    }
  
    public void setIdString(String idString) {
        this.idString = idString;
    }
}


