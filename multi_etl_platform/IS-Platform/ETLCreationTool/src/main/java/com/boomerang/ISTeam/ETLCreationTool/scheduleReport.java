package com.boomerang.ISTeam.ETLCreationTool;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.services.drive.Drive;
import com.google.gdata.util.ServiceException;

public class scheduleReport {
	private static Logger logger = LoggerFactory
			.getLogger(scheduleReport.class);

	public static void main(String[] args) throws Exception {
		scheduleReport report = new scheduleReport();
		ETLUtility utility = new ETLUtility();
		JSONObject schedule_spec_obj = utility.get_schedule_report_spec();
		
		int datediff = Integer.parseInt(((String)schedule_spec_obj.get("dateDiff")).trim());
		String subject = ((String)schedule_spec_obj.get("subject")).trim();
		int skip_header=Integer.parseInt(((String)schedule_spec_obj.get("skipHeaderOfAttachment")).trim());
		String mail_to=((String)schedule_spec_obj.get("mailTo")).trim();
		
		
		GoogleHelpClass ghc = new GoogleHelpClass();
		
		JSONObject json_obj = utility.get_report_spec();
		String gdrive_file_name = (String) json_obj.get("gdriveFileName");
		String folder_name = (String) json_obj.get("gdriveFolderName");
		String Download_Dest_path = (String) json_obj.get("downloadDestPath");
		String emailid = (String) json_obj.get("emailId");

		Drive driveService = ghc.getDriveService();
		String folderid = ghc
				.getOrCreateFolder(driveService, folder_name, null);

		com.google.api.services.drive.model.File gdrive_file = ghc
				.getFileFromFolder(gdrive_file_name, folderid);
		String tmp_download_dir = "tmp";
		String datenow = report.getCurrentDate();
		String attached_file_name = "Etl_Creation_Report_" + datenow + ".xlsx";
		report.getAttachment(gdrive_file.getId(), tmp_download_dir,
				attached_file_name, ghc,datediff, skip_header);
		String tempFiles[]=new String[1]; 
		tempFiles[0]=tmp_download_dir+"/"+attached_file_name;
		String outputFileName[]=new String[1];
		outputFileName[0]=attached_file_name;
		String body = "Hi Team, \n \n Report for ETL Creation Tool: \n Please click below link to access Report for ETL Creation Tool \n https://drive.google.com/a/boomerangcommerce.com/folderview?id="
				+ folderid+"\n \n Please Find Attached file containing weeky usage report of ETL Creation Tool \n \n Thanks, \n ETL Creation Tool";
		 report.sendMail(mail_to, subject, body, tempFiles,
				 outputFileName);
	}

	private String getCurrentDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Date date = new Date();
		return dateFormat.format(date);
	}

	private void getAttachment(String fileid, String tmp_download_dir,
			String attached_file_name, GoogleHelpClass ghc,int datediff,int skip_header) throws IOException,
			GeneralSecurityException, ServiceException {
		String tmpfilename = "tmpfile.csv";
		File downloaddir = new File(tmp_download_dir);
		if (!downloaddir.exists()) {
			downloaddir.mkdirs();
		}
		File downloadfile = new File(tmp_download_dir + "/" + tmpfilename);
		if (downloadfile.exists() && downloadfile.isFile()) {
			downloadfile.delete();
		}
		File xslxfile = new File(tmp_download_dir + "/" + attached_file_name);
		if (xslxfile.exists() & xslxfile.isFile()) {
			xslxfile.delete();
		}
		try {
			downloadfile.createNewFile();
			xslxfile.createNewFile();
		} catch (IOException e) {
			logger.error("IOException while creating downloadfile " + e);
			throw e;
		}

		ghc.downloadCsvFile(fileid, tmp_download_dir + "/" + tmpfilename);
		csvToXLSX(tmp_download_dir + "/" + tmpfilename,tmp_download_dir + "/" +attached_file_name,datediff,skip_header);

		if (downloadfile.exists() && downloadfile.isFile()) {
			downloadfile.delete();
		}
		if (downloaddir.exists() && downloaddir.isDirectory()) {
			downloaddir.delete();
		}
		
	}

	private void csvToXLSX(String csvFileAddress, String xlsxFileAddress,int datediff,int skip_header) {
		try {
			
			// String csvFileAddress = "test.csv"; //csv file address
			// String xlsxFileAddress = "test.xlsx"; //xlsx file address
			XSSFWorkbook workBook = new XSSFWorkbook();
			XSSFSheet sheet = workBook.createSheet("sheet1");
			String currentLine = null;
			int RowNum = 0;
			BufferedReader br = new BufferedReader(new FileReader(
					csvFileAddress));
			Boolean in_range = null;
			while ((currentLine = br.readLine()) != null) {
				if (RowNum <= skip_header-1) {
					String str[] = currentLine.split(",");
					RowNum++;
					XSSFRow currentRow = sheet.createRow(RowNum);
					for (int i = 0; i < str.length; i++) {
						currentRow.createCell(i).setCellValue(str[i]);
					}
				} else {
					String str[] = currentLine.split(",");
					String csvdate = str[0];
					if (csvdate != null && (!csvdate.trim().equals(""))) {
						in_range = checkDateRange(csvdate, datediff);
					}
					if (in_range) {
						RowNum++;
						XSSFRow currentRow = sheet.createRow(RowNum);
						for (int i = 0; i < str.length; i++) {
							currentRow.createCell(i).setCellValue(str[i]);
						}
					}
				}
			}
			FileOutputStream fileOutputStream = new FileOutputStream(
					xlsxFileAddress);
			workBook.write(fileOutputStream);
			fileOutputStream.close();
		} catch (Exception ex) {
			logger.error("Exception while converting csv to xslx file");
		}
	}

	private Boolean checkDateRange(String csvdate, int datediff)
			throws ParseException {

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");

		Date datefromcsv = null;
		try {
			datefromcsv = dateFormat.parse(csvdate);
		} catch (ParseException e) {
			logger.error("ParseException : not able to parse date from csv");
			throw e;
		}
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, datediff);
		Date startdate = cal.getTime();
		
		Calendar endcal = Calendar.getInstance();
		endcal.add(Calendar.DATE, +1);
		Date enddate = endcal.getTime();
		return (startdate.compareTo(datefromcsv)
				* datefromcsv.compareTo(enddate) > 0);

	}

	private void sendMail(String to, String subject, String body,
			String tempFiles[], String outputFileName[])
			throws AddressException, MessagingException {
		final String from = "ops@boomerangcommerce.com";

		String host = "smtp.gmail.com";

		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		properties.setProperty("mail.smtp.host", host);
		properties.setProperty("mail.user", from);
		properties.setProperty("mail.password", "Naganat123");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "587");

		// Get the default Session object.
		Session session = Session.getDefaultInstance(properties,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(from, "Naganat123");
					}
				});
		// Create a default MimeMessage object.
		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress(from));
		// Set To: header field of the header.
		for (String retval : to.split(",")) {
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					retval));
		}
		// Set Subject: header field
		message.setSubject(subject);
		// Now set the actual message
		BodyPart messageText = new MimeBodyPart();
		messageText.setText(body);
		// Adding Attachment

		Multipart multipart = new MimeMultipart();
		int iter;
		int count = 0;
		if (tempFiles != null && outputFileName != null) {
			count = tempFiles.length;
			for (iter = 0; iter < count; iter++) {
				logger.info("Adding attachment.." + outputFileName[iter]);
				File file = new File(tempFiles[iter]);
				BodyPart messageBodyPart = new MimeBodyPart();
				DataSource source = new FileDataSource(file.getAbsolutePath());
				messageBodyPart.setDataHandler(new DataHandler(source));
				messageBodyPart.setFileName(outputFileName[iter]);
				multipart.addBodyPart(messageBodyPart);
				logger.info("Successfully attached!!");

			}
		}
		multipart.addBodyPart(messageText);
		message.setContent(multipart);
		// Send message
		Transport.send(message);
		logger.info("Sent message successfully....");
		if (tempFiles != null && outputFileName != null) {
			count = tempFiles.length;
			for (iter = 0; iter < count; iter++) {
				File temp = new File(tempFiles[iter]);
				if (temp.delete()) {
					logger.debug("Deleted the temporary File ");
				} else {
					logger.warn("unable to delete the temporary file");
				}
			}
		}
	}

}