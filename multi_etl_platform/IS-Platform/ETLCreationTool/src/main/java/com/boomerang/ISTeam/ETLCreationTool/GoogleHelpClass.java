package com.boomerang.ISTeam.ETLCreationTool;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Permission;
import com.google.gdata.client.docs.DocsService;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.Category;

import com.google.gdata.data.PlainTextConstruct;
import com.google.gdata.data.docs.DocumentListEntry;
import com.google.gdata.data.docs.FolderEntry;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.SpreadsheetFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.util.ServiceException;

public class GoogleHelpClass {
	private static Logger logger = LoggerFactory
			.getLogger(GoogleHelpClass.class);
	private static final String APPLICATION_NAME = "IS-Project";

	/** Directory to store user credentials for this application. */
	private static final java.io.File DATA_STORE_DIR = new java.io.File(
			System.getProperty("user.home"),
			".credentials/drive-java-quickstart");

	/** Global instance of the {@link FileDataStoreFactory}. */

	/** Global instance of the JSON factory. */
	private static final JsonFactory JSON_FACTORY = JacksonFactory
			.getDefaultInstance();

	/** Global instance of the HTTP transport. */
	private static HttpTransport HTTP_TRANSPORT;

	/** Global instance of the scopes required by this quickstart. */

	static {
		try {
			HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

		} catch (Throwable t) {
			t.printStackTrace();
			System.exit(1);
		}
	}

	public static void main(String[] args) throws Exception {

		GoogleHelpClass ghc = new GoogleHelpClass();
		DocsService docsService = new DocsService("IS-Project");
		GoogleCredential credential = ghc.getGoogleCredential();
		docsService.setOAuth2Credentials(credential);

	}

	SpreadsheetService getspreadsheetServiceObject()
			throws GeneralSecurityException, IOException {
		SpreadsheetService spreadsheetService = new SpreadsheetService(
				"IS-Project");
		GoogleCredential credential = getGoogleCredential();
		spreadsheetService.setOAuth2Credentials(credential);
		return spreadsheetService;
	}

	DocsService getDocServiceObject() throws GeneralSecurityException,
			IOException {
		DocsService docsService = new DocsService("IS-Project");
		GoogleCredential credential = getGoogleCredential();
		docsService.setOAuth2Credentials(credential);
		return docsService;
	}

	private GoogleCredential getGoogleCredential()
			throws GeneralSecurityException, IOException {
		final JsonFactory jsonFactory = GsonFactory.getDefaultInstance();
		HttpTransport httpTransport;
		GoogleCredential credential = null;
		try {
			httpTransport = GoogleNetHttpTransport.newTrustedTransport();

			credential = new GoogleCredential.Builder()
					.setTransport(httpTransport)
					.setJsonFactory(jsonFactory)
					.setServiceAccountId(
							"50524428372-compute@developer.gserviceaccount.com")
					.setServiceAccountPrivateKeyFromP12File(
							new File(
									"../IS-Platform/ETLCreationTool/src/main/resources/IS-Project-04668bd1b34a.p12"))
					.setServiceAccountScopes(
							Arrays.asList(DriveScopes.DRIVE,
									"https://spreadsheets.google.com/feeds",
									"https://docs.google.com/feeds")).build();
			// new
			// File("../IS-Platform/ETLCreationTool/src/main/resources/IS-Project-04668bd1b34a.p12"))
		} catch (IOException e) {
			logger.error("Exception while executing to getGoogleCredential,"
					+ " exception: " + e.getMessage());
			throw e;
		}
		return credential;
	}

	SpreadsheetEntry getspreadsheet(String gdriveFileid) throws IOException,
			ServiceException, GeneralSecurityException {
		String spreadsheetURL = "https://spreadsheets.google.com/feeds/spreadsheets/"
				+ gdriveFileid;

		SpreadsheetService spreadsheetService = new SpreadsheetService(
				"IS-Project");
		GoogleCredential credential = getGoogleCredential();
		spreadsheetService.setOAuth2Credentials(credential);
		SpreadsheetEntry spreadsheetEntry;
		try {
			spreadsheetEntry = spreadsheetService.getEntry(new URL(
					spreadsheetURL), SpreadsheetEntry.class);
		} catch (MalformedURLException e) {
			logger.error("MalformedURLException while gettting spreadsheetEntry from spreadsheetURL : ,"
					+ spreadsheetURL + " exception: " + e.getMessage());
			throw e;
		} catch (IOException e) {
			logger.error("IOException while gettting spreadsheetEntry from spreadsheetURL : ,"
					+ spreadsheetURL + " exception: " + e.getMessage());
			throw e;
		} catch (ServiceException e) {
			logger.error("ServiceException while gettting spreadsheetEntry from spreadsheetURL : ,"
					+ spreadsheetURL + " exception: " + e.getMessage());
			throw e;
		}
		return spreadsheetEntry;
	}

	List<com.google.api.services.drive.model.File> getListofAllSpreadssheet(
			String folderid) throws IOException, ServiceException,
			GeneralSecurityException {
		String folder_id = folderid;
		Drive service = getDriveService();
		List<com.google.api.services.drive.model.File> result = new ArrayList<com.google.api.services.drive.model.File>();
		String q = "'" + folderid + "' in parents";
		String orderQuery = "createdTime desc,modifiedTime desc";
		Files.List request = service.files().list().setQ(q)
				.setOrderBy(orderQuery);

		do {
			try {
				com.google.api.services.drive.model.FileList files = request
						.execute();

				result.addAll(files.getFiles());
				request.setPageToken(files.getNextPageToken());
			} catch (IOException e) {
				logger.error("IOException occurred at getListofAllSpreadssheet() : " + e);
				request.setPageToken(null);
			}
		} while (request.getPageToken() != null
				&& request.getPageToken().length() > 0);

		return result;
	}
	
 	private void deleteAllFilesNamed(String file_name,String folder_id) throws IOException, ServiceException, GeneralSecurityException{
 		List<com.google.api.services.drive.model.File> all_files=getListofAllSpreadssheet(folder_id);
 		for(com.google.api.services.drive.model.File file :all_files){
 			if(file.getName().equalsIgnoreCase(file_name)){
 				deleteFile(file.getId());
 			}
 		}
 	}

	com.google.api.services.drive.model.File getFileFromFolder(
			String file_name, String folder_id) throws IOException,
			ServiceException, GeneralSecurityException {
		List<com.google.api.services.drive.model.File> spreadsheets = getListofAllSpreadssheet(folder_id);
		List<com.google.api.services.drive.model.File> filelist = new ArrayList<com.google.api.services.drive.model.File>();
		for (com.google.api.services.drive.model.File sheetentry : spreadsheets) {
			if (sheetentry.getName().equals(file_name)) {
				filelist.add(sheetentry);

			}
		}
		if (filelist.isEmpty()) {
			return null;
		}
		return filelist.get(0);

	}

	List<WorksheetEntry> getListOfAllWorksheet(String gdriveFileid)
			throws IOException, ServiceException, GeneralSecurityException {
		SpreadsheetEntry spreadsheet = getspreadsheet(gdriveFileid);
		List<WorksheetEntry> worksheets = spreadsheet.getWorksheets();
		return worksheets;
	}

	DocumentListEntry uploadDoc(String destinationFolderId, String SourcePath,
			String destinationFileName) throws GeneralSecurityException,
			IOException, ServiceException {
		String folder_id = destinationFolderId;
		DocsService docsService = new DocsService("IS-Project");
		GoogleCredential credential = getGoogleCredential();
		docsService.setOAuth2Credentials(credential);

		URL google_drive_feed_url = null;
		try {
			google_drive_feed_url = new URL(
					"https://docs.google.com/feeds/default/private/full/"
							+ (folder_id == null ? "" : "folder%3A" + folder_id
									+ "/contents"));
		} catch (MalformedURLException e) {
			logger.error("Exception while executing to create new URL in createFolder Function, URL: "
					+ google_drive_feed_url
					+ "folder parentId:"
					+ folder_id
					+ " exception: " + e.getMessage());
			throw e;
		}
		DocumentListEntry documentListEntry = new com.google.gdata.data.docs.SpreadsheetEntry();
		try {
			documentListEntry = docsService.insert(google_drive_feed_url,
					documentListEntry);
		} catch (IOException e) {
			logger.error("Exception while executing to uploadDoc " + "URL : "
					+ google_drive_feed_url + " exception: " + e.getMessage());
			throw e;
		} catch (ServiceException e) {
			logger.error("Exception while executing to uploadDoc " + "URL : "
					+ google_drive_feed_url + " exception: " + e.getMessage());
			throw e;
		}
		DocumentListEntry uploadFile = new DocumentListEntry();
		uploadFile.setTitle(new PlainTextConstruct(destinationFileName));// FILE_NAME_DISPLAY_IN_DRIVE
		uploadFile.setFile(new File(SourcePath), "application/*");
		try {
			uploadFile = docsService.insert(google_drive_feed_url, uploadFile);
		} catch (IOException e) {
			logger.error("Exception while executing to uploadDoc " + "URL : "
					+ google_drive_feed_url + " exception: " + e.getMessage());
			throw e;
		} catch (ServiceException e) {
			logger.error("Exception while executing to uploadDoc " + "URL : "
					+ google_drive_feed_url + " exception: " + e.getMessage());
			throw e;
		}
		deleteAllFilesNamed("untitled",folder_id);
		return uploadFile;

	}

	public String createFolder(String title, String folder_id)
			throws ServiceException, GeneralSecurityException, IOException {
		FolderEntry newEntry = new FolderEntry();
		DocsService docsService = new DocsService("IS-Project");
		GoogleCredential credential = getGoogleCredential();
		docsService.setOAuth2Credentials(credential);
		Category category = new Category(
				"http://schemas.google.com/docs/2007#folder");
		newEntry.getCategories().add(category);

		String spreadSheetTitle = title;
		newEntry.setTitle(new PlainTextConstruct(spreadSheetTitle));

		URL url = null;
		try {
			url = new URL("https://docs.google.com/feeds/default/private/full/"
					+ (folder_id == null ? "" : "folder%3A" + folder_id
							+ "/contents"));
		} catch (MalformedURLException e1) {
			logger.error("Exception while executing to create new URL in createFolder Function, URL: "
					+ url
					+ "folder parentId:"
					+ folder_id
					+ " exception: "
					+ e1.getMessage());
			throw e1;
		}
		FolderEntry fEntry = null;
		try {
			fEntry = docsService.insert(url, newEntry);
		} catch (IOException e) {
			logger.error("Exception while executing to createFolder, folderName: "
					+ title
					+ " parentId:"
					+ folder_id
					+ " exception: "
					+ e.getMessage());
			throw e;

		}

		String fString = fEntry.getDocId();
		return fString;
	}

	public String getOrCreateFolder(Drive service, String folderName,
			String parentId) throws Exception {
		com.google.api.services.drive.model.File result = null;
		// check if the folder exists already
		String folderid = "";

		try {

			String query = "mimeType='application/vnd.google-apps.folder' and trashed=false";
			// add parent param to the query if needed

			if (parentId != null) {
				query = query + " and '" + parentId + "' in parents";
			}
			Files.List request = service.files().list().setQ(query);
			FileList files = request.execute();
			for (com.google.api.services.drive.model.File f : files.getFiles()) {

				if (f.getName().equals(folderName)) {

					folderid = f.getId();
					return folderid;
				}
			}

			// create new folder
			folderid = createFolder(folderName, parentId);

		} catch (Exception e) {
			logger.error("Exception while executing to getOrCreateFolder, folderName: "
					+ folderName
					+ " parentId:"
					+ parentId
					+ " exception:"
					+ e.getMessage());
			throw e;
		}
		return folderid;
	}

	public Drive getDriveService() throws IOException, GeneralSecurityException {
		Credential credential = getGoogleCredential();
		return new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
				.setApplicationName(APPLICATION_NAME).build();
	}

	void downloadFile(String doc_id, String DestinationPath)
			throws GeneralSecurityException, ServiceException, IOException {
		String mimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		downloadFile(doc_id, DestinationPath, mimeType);
	}

	void downloadFile(String doc_id, String DestinationPath, String mimeType)
			throws GeneralSecurityException, ServiceException, IOException {

		String sourceFileID = doc_id;
		Drive driveService = getDriveService();
		OutputStream outputStream;
		String fileId = sourceFileID;
		File downloadFile = new File(DestinationPath);
		if (!downloadFile.isFile()) {
			try {
				downloadFile.createNewFile();
			} catch (IOException e) {
				logger.error("Exception while executing downloadFile,IO Exception while creating file at : "
						+ downloadFile + " exception:" + e.getMessage());
				throw e;
			}
		}

		try {
			outputStream = new FileOutputStream(downloadFile);
		} catch (FileNotFoundException e1) {
			logger.error("Exception while executing downloadFile, downloadFile: "
					+ downloadFile + " exception:" + e1.getMessage());
			throw e1;
		}
		try {
			driveService.files().export(fileId, mimeType)
					.executeMediaAndDownloadTo(outputStream);
		} catch (IOException e) {
			logger.error("Exception while executing downloadFile, doc_id: "
					+ doc_id + " exception:" + e.getMessage());
			throw e;
		}

	}

	void ShareDOC(String fileid, String emailid)
			throws GeneralSecurityException, IOException {

		Drive driveService = getDriveService();
		String fileId = fileid;
		JsonBatchCallback<Permission> callback = new JsonBatchCallback<Permission>() {

			public void onFailure(GoogleJsonError e, HttpHeaders responseHeaders)
					throws IOException {
				// Handle error
				logger.error("Exception while executing ShareDOC"
						+ " exception:" + e.getMessage());
			}

			public void onSuccess(Permission permission,
					HttpHeaders responseHeaders) throws IOException {

				// System.out.println("Permission ID: " + permission.getId());
			}

		};

		BatchRequest batch = driveService.batch();
		try {
			Permission userPermission = new Permission().setType("user")
					.setRole("writer").setEmailAddress(emailid);
			driveService.permissions().create(fileId, userPermission)
					.setFields("id").queue(batch, callback);

			Permission domainPermission = new Permission().setType("domain")
					.setRole("reader").setDomain("boomerangcommerce.com");
			driveService.permissions().create(fileId, domainPermission)
					.setFields("id").queue(batch, callback);

			batch.execute();
		} catch (IOException e1) {
			logger.error("Exception while executing ShareDOC, fileid: "
					+ fileid + "email id" + emailid + " exception:"
					+ e1.getMessage());
			throw e1;

		}
	}

	public void downloadCsvFile(String doc_id, String DestinationPath)
			throws GeneralSecurityException, ServiceException, IOException {
		Drive driveService = getDriveService();
		String mimeType = "text/csv";
		downloadFile(doc_id, DestinationPath, mimeType);

	}

	void deleteFile(String fileId) throws GeneralSecurityException {
		try {
			Drive driveService = getDriveService();
			driveService.files().delete(fileId).execute();
		} catch (IOException e) {
			logger.error("Error occurred while deleting file, file id : "
					+ fileId + " " + e);
		}
	}
	/*
	 * public com.google.api.services.drive.model.File insertCsvFile( String
	 * destFileName, String parentId, String srcCsvPath) throws IOException,
	 * GeneralSecurityException {
	 * 
	 * String mimeType =
	 * "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	 * com.google.api.services.drive.model.File file = insertFile( destFileName,
	 * parentId, mimeType, srcCsvPath);
	 * 
	 * return file; }
	 * 
	 * public com.google.api.services.drive.model.File updateCsvFile( String
	 * destFileid, String srcCsvPath) throws IOException,
	 * GeneralSecurityException {
	 * 
	 * String mimeType =
	 * "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	 * com.google.api.services.drive.model.File file = updateFile(destFileid,
	 * mimeType, srcCsvPath);
	 * 
	 * return file; }
	 *//**
	 * Insert new file.
	 *
	 * @param service
	 *            Drive API service instance.
	 * @param title
	 *            Title of the file to insert, including the extension.
	 * @param description
	 *            Description of the file to insert.
	 * @param parentId
	 *            Optional parent folder's ID.
	 * @param mimeType
	 *            MIME type of the file to insert.
	 * @param filename
	 *            Filename of the file to insert.
	 * @return Inserted file metadata if successful, {@code null} otherwise.
	 * @throws GeneralSecurityException
	 * @throws IOException
	 */
	/*
	 * public com.google.api.services.drive.model.File insertFile(String title,
	 * String parentId, String mimeType, String filepath) throws IOException,
	 * GeneralSecurityException { // File's metadata.
	 * 
	 * Drive driveService = getDriveService();
	 * com.google.api.services.drive.model.File body = new
	 * com.google.api.services.drive.model.File(); body.setName(title);
	 * body.setParents(Collections.singletonList(parentId));
	 * body.setMimeType(mimeType);
	 * 
	 * // File's content. java.io.File fileContent = new java.io.File(filepath);
	 * FileContent mediaContent = new FileContent(mimeType, fileContent);
	 * com.google.api.services.drive.model.File file; try { if (parentId != null
	 * && parentId != "") {
	 * System.out.println("in  parentId != null && parentId != "); file =
	 * driveService.files().create(body, mediaContent)
	 * .setFields("id, parents").execute(); } else {
	 * System.out.println("in else part"); file =
	 * driveService.files().create(body, mediaContent) .execute(); }
	 * 
	 * // Uncomment the following line to print the File ID. //
	 * System.out.println("File ID: " + file.getId());
	 * 
	 * return file; } catch (IOException e) {
	 * logger.error("IOException occur during creating file on gdrive " + e);
	 * throw e;
	 * 
	 * }
	 * 
	 * }
	 */

	/**
	 * Update an existing file's metadata and content.
	 *
	 * @param service
	 *            Drive API service instance.
	 * @param fileId
	 *            ID of the file to update.
	 * @param newTitle
	 *            New title for the file.
	 * @param newDescription
	 *            New description for the file.
	 * @param newMimeType
	 *            New MIME type for the file.
	 * @param newFilename
	 *            Filename of the new content to upload.
	 * @param newRevision
	 *            Whether or not to create a new revision for this file.
	 * @return Updated file metadata if successful, {@code null} otherwise.
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	/*
	 * public com.google.api.services.drive.model.File updateFile(String fileId,
	 * String newMimeType, String newFilename) throws IOException,
	 * GeneralSecurityException { Drive driveService = getDriveService(); try {
	 * // First retrieve the file from the API.
	 * com.google.api.services.drive.model.File file = driveService
	 * .files().get(fileId).execute();
	 * 
	 * // File's new metadata. file.setMimeType(newMimeType);
	 * 
	 * // File's new content. java.io.File fileContent = new
	 * java.io.File(newFilename); FileContent mediaContent = new
	 * FileContent(newMimeType, fileContent);
	 * 
	 * // Send the request to the API. com.google.api.services.drive.model.File
	 * updatedFile = driveService .files().update(fileId, file,
	 * mediaContent).execute();
	 * 
	 * return updatedFile; } catch (IOException e) {
	 * logger.error("IOException occur during updating file on gdrive. file_id= "
	 * + fileId); logger.error("" + e); throw e; } }
	 */

	/*
	 * public static int widToGid(String worksheetId) {
	 * 
	 * boolean idIsNewStyle = worksheetId.length() > 3;
	 * 
	 * // if the id is in the new style, first strip the first character before
	 * // converting worksheetId = idIsNewStyle ? worksheetId.substring(1) :
	 * worksheetId;
	 * 
	 * // determine the integer to use for bitwise XOR int xorValue =
	 * idIsNewStyle ? 474 : 31578;
	 * 
	 * // convert to gid return Integer.parseInt(worksheetId, 36) ^ xorValue;
	 * 
	 * } private void deleteAllWorksheet() throws GeneralSecurityException,
	 * IOException, ServiceException {
	 * 
	 * DocsService docsService = new DocsService("RedshoftTooling");
	 * GoogleCredential credential = getGoogleCredential();
	 * docsService.setOAuth2Credentials(credential);
	 * 
	 * // Define the URL to request. This should never change. URL
	 * SPREADSHEET_FEED_URL = new URL(
	 * "https://spreadsheets.google.com/feeds/spreadsheets/private/full");
	 * 
	 * // Make a request to the API and get all spreadsheets. SpreadsheetFeed
	 * feed = docsService.getFeed(SPREADSHEET_FEED_URL, SpreadsheetFeed.class);
	 * List<SpreadsheetEntry> spreadsheets = feed.getEntries();
	 * 
	 * for(SpreadsheetEntry se : spreadsheets){
	 * if(se.getTitle().getPlainText().equals("tables")){ WorksheetFeed wf =
	 * docsService.getFeed(se.getWorksheetFeedUrl(), WorksheetFeed.class);
	 * 
	 * List<WorksheetEntry> worksheets = wf.getEntries();
	 * 
	 * for(WorksheetEntry we : worksheets){ System.out.println(we.getId()); } }
	 * } }
	 * 
	 * private void addWorksheet() throws IOException, ServiceException,
	 * GeneralSecurityException {
	 * 
	 * DocsService docsService = new DocsService("RedshoftTooling");
	 * GoogleCredential credential = getGoogleCredential();
	 * docsService.setOAuth2Credentials(credential);
	 * 
	 * // TODO: Authorize the service object for a specific user (see other //
	 * sections)
	 * 
	 * // Define the URL to request. This should never change. URL
	 * SPREADSHEET_FEED_URL = new URL(
	 * "https://spreadsheets.google.com/feeds/spreadsheets/private/full");
	 * 
	 * // Make a request to the API and get all spreadsheets. SpreadsheetFeed
	 * feed = docsService.getFeed(SPREADSHEET_FEED_URL, SpreadsheetFeed.class);
	 * List<SpreadsheetEntry> spreadsheets = feed.getEntries();
	 * 
	 * SpreadsheetEntry spreadsheet = null;
	 * 
	 * for(SpreadsheetEntry e : spreadsheets){
	 * if(e.getTitle().getPlainText().equals("tables")){ spreadsheet = e; break;
	 * } }
	 * 
	 * // Create a local representation of the new worksheet. WorksheetEntry
	 * worksheet = new WorksheetEntry(); worksheet.setTitle(new
	 * PlainTextConstruct("22")); worksheet.setColCount(10);
	 * worksheet.setRowCount(10);
	 * 
	 * 
	 * URL worksheetFeedUrl = spreadsheet.getWorksheetFeedUrl(); WorksheetEntry
	 * ee = docsService.insert(worksheetFeedUrl, worksheet);
	 * 
	 * 
	 * // Write header line into Spreadsheet URL cellFeedUrl =
	 * ee.getCellFeedUrl(); CellFeed cellFeed = docsService.getFeed(cellFeedUrl,
	 * CellFeed.class);
	 * 
	 * CellEntry cellEntry = new CellEntry(1, 1, "headline1");
	 * cellFeed.insert(cellEntry); cellEntry = new CellEntry(1, 2, "headline2");
	 * cellFeed.insert(cellEntry); cellEntry = new CellEntry(2, 1, "val1");
	 * cellFeed.insert(cellEntry); cellEntry = new CellEntry(2, 2, "val2");
	 * cellFeed.insert(cellEntry);
	 * 
	 * com.google.api.services.drive.model.File body = new
	 * com.google.api.services.drive.model.File();
	 * 
	 * body.setName("groupon.csv"); body.setDescription("A test document");
	 * body.setMimeType("text/csv");
	 * body.setParents(Arrays.asList("0B0zsMG7RZXtYdHd2TDBfQzNnVGc"));
	 * 
	 * java.io.File fileContent = new
	 * java.io.File("/Users/kishan/Tools/repo/athena_gargantua/app/tables.csv");
	 * FileContent mediaContent = new FileContent("text/csv", fileContent);
	 * 
	 * 
	 * Drive service = new GoogleHelpClass().getDriveService();
	 * 
	 * 
	 * //com.google.api.services.drive.model.File f =
	 * service.files().create(body, mediaContent).execute();
	 * 
	 * 
	 * //ShareDOC(f.getId(), "kishan@boomerangcommerce.com");
	 * 
	 * }
	 * 
	 * private void modifyWorksheet() throws IOException, ServiceException {
	 * 
	 * SpreadsheetService service = new SpreadsheetService(
	 * "MySpreadsheetIntegration-v1");
	 * 
	 * // TODO: Authorize the service object for a specific user (see other //
	 * sections)
	 * 
	 * // Define the URL to request. This should never change. URL
	 * SPREADSHEET_FEED_URL = new URL(
	 * "https://spreadsheets.google.com/feeds/spreadsheets/private/full");
	 * 
	 * // Make a request to the API and get all spreadsheets. SpreadsheetFeed
	 * feed = service.getFeed(SPREADSHEET_FEED_URL, SpreadsheetFeed.class);
	 * List<SpreadsheetEntry> spreadsheets = feed.getEntries();
	 * 
	 * if (spreadsheets.size() == 0) { // TODO: There were no spreadsheets, act
	 * accordingly. }
	 * 
	 * // TODO: Choose a spreadsheet more intelligently based on your // app's
	 * needs. SpreadsheetEntry spreadsheet = spreadsheets.get(0);
	 * System.out.println(spreadsheet.getTitle().getPlainText());
	 * 
	 * // Get the first worksheet of the first spreadsheet. // TODO: Choose a
	 * worksheet more intelligently based on your // app's needs. WorksheetFeed
	 * worksheetFeed = service.getFeed( spreadsheet.getWorksheetFeedUrl(),
	 * WorksheetFeed.class); List<WorksheetEntry> worksheets =
	 * worksheetFeed.getEntries(); WorksheetEntry worksheet = worksheets.get(0);
	 * 
	 * // Update the local representation of the worksheet.
	 * worksheet.setTitle(new PlainTextConstruct("Updated Worksheet"));
	 * worksheet.setColCount(5); worksheet.setRowCount(15);
	 * 
	 * // Send the local representation of the worksheet to the API for //
	 * modification. worksheet.update();
	 * 
	 * }
	 * 
	 * SpreadsheetEntry createspreadSheet(String title, String folder_id) throws
	 * MalformedURLException, IOException, ServiceException,
	 * GeneralSecurityException { SpreadsheetEntry new_spreadsheet = new
	 * SpreadsheetEntry();
	 * 
	 * DocsService docsService = new DocsService("IS-Project"); GoogleCredential
	 * credential = getGoogleCredential();
	 * docsService.setOAuth2Credentials(credential);
	 * 
	 * Category category = new Category(
	 * "http://schemas.google.com/docs/2007#spreadsheet");
	 * new_spreadsheet.getCategories().add(category);
	 * 
	 * String spreadSheetTitle = title; new_spreadsheet.setTitle(new
	 * PlainTextConstruct(spreadSheetTitle));
	 * 
	 * return docsService.insert(new URL(
	 * "https://docs.google.com/feeds/default/private/full/" + (folder_id ==
	 * null ? "" : "folder%3A" + folder_id + "/contents")), new_spreadsheet); }
	 * 
	 * 
	 * 
	 * 
	 * void retriveListOfSpreadsheet() throws IOException, ServiceException,
	 * GeneralSecurityException { SpreadsheetService service = new
	 * SpreadsheetService("RedshoftTooling"); GoogleCredential credential =
	 * getGoogleCredential(); service.setOAuth2Credentials(credential); // TODO:
	 * Authorize the service object for a specific user (see other // sections)
	 * 
	 * // Define the URL to request. This should never change. URL
	 * SPREADSHEET_FEED_URL = new URL(
	 * "https://spreadsheets.google.com/feeds/spreadsheets/private/full");
	 * 
	 * //https://docs.google.com/spreadsheets/d/19n_Bgfy-
	 * X4l7hWzAgZDI7cX7mDHpTzzwBH4A0pDB8Nc/pubhtml
	 * 
	 * // Make a request to the API and get all spreadsheets. SpreadsheetFeed
	 * feed = service.getFeed(SPREADSHEET_FEED_URL, SpreadsheetFeed.class);
	 * 
	 * List<SpreadsheetEntry> spreadsheets = feed.getEntries();
	 * 
	 * // Iterate through all of the spreadsheets returned for (SpreadsheetEntry
	 * spreadsheet : spreadsheets) { // Print the title of this spreadsheet to
	 * the screen System.out.println(spreadsheet.getTitle().getPlainText());
	 * 
	 * } }
	 * 
	 * void modifyTitleAndSize(WorksheetEntry worksheet, int columnSize, int
	 * rowSize) throws IOException, ServiceException {
	 * 
	 * // Update the local representation of the worksheet.
	 * worksheet.setTitle(new PlainTextConstruct("Updated Worksheet"));
	 * worksheet.setColCount(columnSize); worksheet.setRowCount(rowSize);
	 * 
	 * // Send the local representation of the worksheet to the API for //
	 * modification. worksheet.update(); }
	 * 
	 * void deleteWorksheet(WorksheetEntry worksheet) throws IOException,
	 * ServiceException { worksheet.delete(); }
	 */

}
