package com.boomerang.ISTeam.JsonGenerator;

import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class read_etl_conf_excel {

	/** Creates a new instance of POIExcelReader */
	public read_etl_conf_excel() {
	}

	public void execute(String xlspath, String etldirectory) throws Exception {
		etl_spec etlSpec = new etl_spec();
		List<feed_spec> feedSpec = new ArrayList<feed_spec>();
		List<table_spec> tableSpec = new ArrayList<table_spec>();
		feed_spec feedTemp;
		table_spec tableTemp;
		// String
		// xlsPath="/Users/rohitsharma/Downloads/kmart_etl_workbook_generated.xlsx";//test_prod_etl_workbook.xlsx";
		String xlsPath = xlspath;
		// String
		// etlDirectory="/Users/rohitsharma/Documents/work/NewDataPlatform/excelETL/kmart";
		String etlDirectory = etldirectory;
		String Client = "";
		// FileWriter fileWriter=null;
		BufferedWriter fileWriter = null;
		InputStream inputStream = null;

		try {
			inputStream = new FileInputStream(xlsPath);
		} catch (FileNotFoundException e) {
			// System.out.println ("File not found in the specified path.");
			e.printStackTrace();
		}

		POIFSFileSystem fileSystem = null;

		try {
			Boolean row_noNulls = true;
			Boolean column_noNulls = true;
			File myFile = new File(xlsPath);
			FileInputStream fis = new FileInputStream(myFile);
			org.apache.poi.ss.usermodel.Workbook workBook = WorkbookFactory
					.create(fis);
			org.apache.poi.ss.usermodel.Sheet sheet = workBook.getSheetAt(0);
			// XSSFSheet sheet = workBook.getSheetAt(0);
			Iterator<Row> rows = sheet.rowIterator();
			Row row;
			Cell cell;
			for (int i = 0; i < workBook.getNumberOfSheets(); i++) {
				if (i == 0) {
					// System.out.println("ETL Spec Sheet name: " +
					// workBook.getSheetName(i));
					sheet = workBook.getSheetAt(i);
					etlSpec.set_etl_spec_values(sheet);
					Client = etlSpec.getClient();
				} else if (workBook.getSheetName(i).startsWith("feed.")) {
					// System.out.println("Feed Sheet name: " +
					// workBook.getSheetName(i));
					sheet = workBook.getSheetAt(i);
					feedTemp = new feed_spec();
					feedTemp.setClient(Client);
					feedTemp.set_feed_spec_values(sheet,
							workBook.getSheetName(i));
					feedSpec.add(feedTemp);
					// System.out.println("Added 1 Feed");
					String feedspec = feedTemp.feed_spec_json(xlsPath);
					String feedJson = read_etl_conf_excel
							.toPrettyFormat(feedspec);
					fileWriter = new BufferedWriter(
							new OutputStreamWriter(new FileOutputStream(
									etlDirectory + "/" + feedTemp.getFeed_id()
											+ ".json"), "UTF-8"));
					fileWriter.write(feedJson.replace(
							"\"whereClause\": \"null\"",
							"\"whereClause\": null"));
					// fileWriter = new
					// FileWriter(etlDirectory+"/"+feedTemp.getFeed_id()+".json");
					// fileWriter.append(feedJson);
					fileWriter.flush();
					fileWriter.close();
				} else if (workBook.getSheetName(i).startsWith("table.")) {
					// System.out.println("Feed Sheet name: " +
					// workBook.getSheetName(i));
					sheet = workBook.getSheetAt(i);
					tableTemp = new table_spec();
					// System.out.println("Added 1 Table");
					tableTemp.setClient(Client);
					tableTemp.set_table_spec_values(sheet,
							workBook.getSheetName(i));
					tableSpec.add(tableTemp);

					String tabletemp = tableTemp.table_spec_json(xlsPath);
					String tableJson = read_etl_conf_excel
							.toPrettyFormat(tabletemp);

					tableJson = tableJson.replace("\"whereClause\": \"null\"",
							"\"whereClause\": null");
					tableJson = tableJson.replace("\"groupBy\": \"null\"",
							"\"groupBy\": null");

					fileWriter = new BufferedWriter(new OutputStreamWriter(
							new FileOutputStream(etlDirectory + "/"
									+ tableTemp.getTable_id() + ".json"),
							"UTF-8"));
					fileWriter.write(tableJson.replace("\"order\": \"null\"",
							"\"order\": null"));
					fileWriter.flush();
					fileWriter.close();
				}
			}

			String etlspec = etlSpec
					.etl_spec_josn(xlsPath, feedSpec, tableSpec);
			String etlspecJson = read_etl_conf_excel.toPrettyFormat(etlspec);
			// fileWriter = new FileWriter(etlDirectory+"/etl-spec.json");
			// fileWriter.append(etlspecJson);
			fileWriter = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(etlDirectory + "/etl-spec.json"),
					"UTF-8"));
			fileWriter.write(etlspecJson);
			fileWriter.flush();
			fileWriter.close();
		} catch (Exception e) {
			throw e;
		}

	}

	public static Vector readExcelFile(String fileName) {
		/**
		 * --Define a Vector --Holds Vectors Of Cells
		 */
		Vector cellVectorHolder = new Vector();

		try {
			/** Creating Input Stream **/

			File myFile = new File("");
			FileInputStream fis = new FileInputStream(myFile);

			// Finds the workbook instance for XLSX file
			XSSFWorkbook myWorkBook = new XSSFWorkbook(fis);

			// Return first sheet from the XLSX workbook
			XSSFSheet mySheet = myWorkBook.getSheetAt(0);

			// Get iterator to all the rows in current sheet
			// Iterator<Row> rowIterator = mySheet.iterator();
			Iterator<Row> rowIterator = mySheet.iterator();

			/** We now need something to iterate through the cells. **/
			Iterator rowIter = mySheet.rowIterator();

			while (rowIter.hasNext()) {
				Row myRow = (Row) rowIter.next();
				if (myRow.getCell(0) != null) {
					Iterator cellIter = myRow.cellIterator();
					Vector cellStoreVector = new Vector();
					while (cellIter.hasNext()) {
						Cell myCell = (Cell) cellIter.next();
						cellStoreVector.addElement(myCell);
					}
					cellVectorHolder.addElement(cellStoreVector);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cellVectorHolder;
	}

	private static void printCellDataToConsole(Vector dataHolder) {

		for (int i = 0; i < dataHolder.size(); i++) {
			Vector cellStoreVector = (Vector) dataHolder.elementAt(i);
			for (int j = 0; j < cellStoreVector.size(); j++) {
				Cell myCell = (Cell) cellStoreVector.elementAt(j);
				String stringCellValue = myCell.toString();
				// System.out.print(stringCellValue+"\t");
			}
			// System.out.println();
		}
	}

	public static String cellToString(Cell cell) {
		int type;
		Object result = null;
		type = cell.getCellType();

		switch (type) {

		case Cell.CELL_TYPE_NUMERIC: // numeric value in Excel
		case Cell.CELL_TYPE_FORMULA: // precomputed value based on formula
			result = cell.getNumericCellValue();
			break;
		case Cell.CELL_TYPE_STRING: // String Value in Excel
			result = cell.getStringCellValue();
			break;
		case Cell.CELL_TYPE_BLANK:
			result = "";
			break;
		case Cell.CELL_TYPE_BOOLEAN: // boolean value
			result = cell.getBooleanCellValue();
			break;
		case Cell.CELL_TYPE_ERROR:
		default:
			throw new RuntimeException(
					"There is no support for this type of cell");
		}

		if (result.equals(null)) {
			return "";
		} else if (result.toString().equals("--NA--")) {
			return "";
		} else {
			return result.toString();
		}

	}

	public static String get_cell_data(Row row, int pos) {
		Cell cell = row.getCell(pos);
		if (cell == null) {
			return "";
		} else {
			int type;
			Object result = null;
			type = cell.getCellType();

			switch (type) {

			case Cell.CELL_TYPE_NUMERIC: // numeric value in Excel
			case Cell.CELL_TYPE_FORMULA: // precomputed value based on formula
				DataFormatter fmt = new DataFormatter();
				result= fmt.formatCellValue(cell);
				//result = cell.getNumericCellValue();
				break;
			case Cell.CELL_TYPE_STRING: // String Value in Excel
				result = cell.getStringCellValue();
				break;
			case Cell.CELL_TYPE_BLANK:
				result = "";
				break;
			case Cell.CELL_TYPE_BOOLEAN: // boolean value
				result = cell.getBooleanCellValue();
				break;
			case Cell.CELL_TYPE_ERROR:
			default:
				throw new RuntimeException(
						"There is no support for this type of cell");
			}

			if (result.equals(null)) {
				return "";
			} else if (result.toString().equals("--NA--")) {
				return "";
			} else {
				return result.toString();
			}
		}

	}

	public static void display_etl_spec(etl_spec etlSpec) {
		/*
		 * System.out.println("Project_name: "+ etlSpec.getProject_name());
		 * System.out.println("Client: "+ etlSpec.getClient());
		 * System.out.println("Environment: "+ etlSpec.getEnvironment());
		 * System.out.println("System: "+ etlSpec.getSystem());
		 * System.out.println("Owner: "+ etlSpec.getOwner());
		 * System.out.println("Pagerduty_key: "+ etlSpec.getPagerduty_key());
		 * System.out.println("etl_start_pushmon_trigger_url: "+
		 * etlSpec.getEtl_start_pushmon_trigger_url());
		 * System.out.println("etl_end_pushmon_trigger_url: "+
		 * etlSpec.getEtl_end_pushmon_trigger_url());
		 */
	}

	/**
	 * Convert a JSON string to pretty print version
	 * 
	 * @param jsonString
	 * @return
	 */
	public static String toPrettyFormat(String jsonString) {
		JsonParser parser = new JsonParser();
		JsonObject json = parser.parse(jsonString).getAsJsonObject();

		Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting()
				.create();
		String prettyJson = gson.toJson(json);

		return prettyJson.replace("\\\\n", "\\n").replace("\\\\t", "\\t"); // .replace("\"\\\"","\"\\\\\"");
	}

}
