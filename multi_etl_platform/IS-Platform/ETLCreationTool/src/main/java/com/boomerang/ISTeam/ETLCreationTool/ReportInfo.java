package com.boomerang.ISTeam.ETLCreationTool;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.services.drive.Drive;
import com.google.gdata.data.docs.DocumentListEntry;
import com.google.gdata.util.ServiceException;

public class ReportInfo {

	private static Logger logger = LoggerFactory.getLogger(ReportInfo.class);

	public void execute(String iteration_id, List<String> files_generated_list,
			List<String> new_file_added_list, String action, String client,
			String overwrite) throws Exception {
		ETLUtility utility = new ETLUtility();
		JSONObject json_obj = utility.get_report_spec();
		String gdrive_file_name = (String) json_obj.get("gdriveFileName");
		String folder_name = (String) json_obj.get("gdriveFolderName");
		String Download_Dest_path = (String) json_obj.get("downloadDestPath");
		String emailid = (String) json_obj.get("emailId");

		String files_generated = convertListToString(files_generated_list);
		String new_file_added = convertListToString(new_file_added_list);

		GoogleHelpClass ghc = new GoogleHelpClass();
		Drive driveService = ghc.getDriveService();
		String folderid = ghc
				.getOrCreateFolder(driveService, folder_name, null);
		String gdrive_file_id = getFileIdOnGdrive(ghc, gdrive_file_name,
				folderid);
		DownloadFile(ghc, gdrive_file_id, Download_Dest_path);
		updateCsv(gdrive_file_id, Download_Dest_path, iteration_id,
				files_generated, new_file_added, action, client, overwrite);
		DocumentListEntry docEntry = ghc.uploadDoc(folderid,
				Download_Dest_path, gdrive_file_name);
		ghc.ShareDOC(folderid, emailid);
		ghc.ShareDOC(docEntry.getDocId(), emailid);
		if (docEntry != null) {
			if (docEntry.getDocId() != null && gdrive_file_id != null) {
				ghc.deleteFile(gdrive_file_id);
			}
		}
		cleanUp(Download_Dest_path);
	}

	private String convertListToString(List<String> list) {
		StringBuilder rString = new StringBuilder();
		if (list == null) {
			return "-";
		}
		if (list.isEmpty()) {
			return "-";
		}

		for (String each : list) {
			rString.append(each).append("  ");
		}

		return rString.toString();

	}

	private String getFileIdOnGdrive(GoogleHelpClass ghc,
			String gdrive_file_name, String folderid) throws Exception {
		Drive driveService = ghc.getDriveService();

		com.google.api.services.drive.model.File gdrive_file = ghc
				.getFileFromFolder(gdrive_file_name, folderid);
		if (gdrive_file == null) {
			return null;
		}
		String gdrive_file_id = gdrive_file.getId();
		return gdrive_file_id;
	}

	private void DownloadFile(GoogleHelpClass ghc, String gdrive_file_id,
			String Download_Dest_path) throws GeneralSecurityException,
			ServiceException, IOException {

		File downloadFile = new File(Download_Dest_path);
		int index = Download_Dest_path.lastIndexOf("/");
		if (index > -1) {
			String dirpath = Download_Dest_path.substring(0, index);
			File dir = new File(dirpath);
			if (!dir.exists()) {
				dir.mkdir();
			}
		}

		if (downloadFile.isFile()) {
			try {
				downloadFile.delete();
				downloadFile.createNewFile();
			} catch (IOException e) {
				logger.error("Exception while executing downloadFile,IO Exception while creating file at : "
						+ downloadFile + " exception:" + e.getMessage());
				throw e;
			}
		}
		if (!downloadFile.isFile()) {
			try {
				downloadFile.createNewFile();
			} catch (IOException e) {
				logger.error("Exception while executing downloadFile,IO Exception while creating file at : "
						+ downloadFile + " exception:" + e.getMessage());
				throw e;
			}
		}

		if (gdrive_file_id != null && (!gdrive_file_id.equalsIgnoreCase(""))) {
			ghc.downloadCsvFile(gdrive_file_id, Download_Dest_path);
		}

	}

	private void updateCsv(String gdrive_file_id, String Download_Dest_path,
			String iteration_id, String files_generated, String new_file_added,
			String action, String client, String overwrite) throws IOException {

		String header = "Date Time, Client, IP Address, User Name, Action,Files Generated, Newly Added Files, Iteration, Overwrite Flag";
		String newinfo = createcsvString(iteration_id, files_generated,
				new_file_added, action, client, overwrite);
		FileWriter fw;
		BufferedWriter bw;
		try {
			fw = new FileWriter(Download_Dest_path, true);
			bw = new BufferedWriter(fw);
		} catch (IOException e) {
			logger.error("IOException error occur while updating csv" + e);
			throw e;
		}
		if (gdrive_file_id == null || gdrive_file_id.equals("")) {
			bw.append("\n");
			bw.append(header);
			bw.append("\n");
			bw.append(newinfo);

		} else {
			bw.append("\n");
			bw.append(newinfo);

		}
		bw.flush();
		bw.close();

	}

	private String createcsvString(String iteration_id, String files_generated,
			String new_file_added, String action, String client,
			String overwrite) {
		String user_name = System.getProperty("user.name");
		Date datenow = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
		String date = dateFormat.format(datenow);
		String ip_address = null;
		try {
			ip_address = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			logger.error("Error while getting local IP Address");
		}

		String returnString = date + "," + client + "," + ip_address + ","
				+ user_name + "," + action + "," + files_generated + ","
				+ new_file_added + ",Iteration_" + iteration_id + ","
				+ overwrite;
		return returnString;
	}

	private void cleanUp(String Download_Dest_path) throws IOException {
		if (Download_Dest_path != null && (!Download_Dest_path.equals(""))) {
			File downloadFile = new File(Download_Dest_path);
			if (downloadFile != null) {
				if (downloadFile.isFile() && downloadFile.exists()) {
					downloadFile.delete();
				}
			}
		}
	}
}
