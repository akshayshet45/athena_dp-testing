package com.boomerang.ISTeam.JsonGenerator;
public class table_columns {
	String column_name;
	String column_type;
	String Source_mapping_type;
	String Source_feed_column;
	String expression;
	String is_primary_key;
	
	public String getColumn_name() {
		return column_name;
	}
	public void setColumn_name(String column_name) {
		this.column_name = column_name;
	}
	public String getColumn_type() {
		return column_type;
	}
	public void setColumn_type(String column_type) {
		this.column_type = column_type;
	}
	public String getSource_mapping_type() {
		return Source_mapping_type;
	}
	public void setSource_mapping_type(String source_mapping_type) {
		Source_mapping_type = source_mapping_type;
	}
	public String getSource_feed_column() {
		return Source_feed_column;
	}
	public void setSource_feed_column(String source_feed_column) {
		Source_feed_column = source_feed_column;
	}
	public String getExpression() {
		return expression;
	}
	public void setExpression(String expression) {
		this.expression = expression;
	}
	public String getIs_primary_key() {
		return is_primary_key;
	}
	public void setIs_primary_key(String is_primary_key) {
		this.is_primary_key = is_primary_key;
	}
	
	
}
