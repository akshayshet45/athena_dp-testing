package com.boomerang.ISTeam.ETLCreationTool;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ETLUtility {

	private final String ETL_VERSION_SPEC_PATH = "configs/etl_version_spec.json";
	private final String CLIENT_SPEC_PATH = "client-spec.json";
	private final String ETL_CREATION_SPEC_PATH = "configs/etl-creation-spec.json";
	private final String BACKUP_VERSION_SPEC_PATH = "configs/etl_version_spec_backup.json";
	private final String DEFAULT_HIVE_EXPRESSION_SPEC_PATH = "../IS-Platform/ETLCreationTool/src/main/resources/default_hive_expression_spec.json";
	private final String REPORT_SPEC_PATH="../IS-Platform/ETLCreationTool/src/main/resources/report_spec.json";
	private final String SCHEDULE_REPORT_SPEC_PATH = "../IS-Platform/ETLCreationTool/src/main/resources/schedule_report_spec.json";
	
	

	public String getSCHEDULE_REPORT_SPEC_PATH() {
		return SCHEDULE_REPORT_SPEC_PATH;
	}

	public String getREPORT_SPEC_PATH() {
		return REPORT_SPEC_PATH;
	}

	public String getETL_VERSION_SPEC_PATH() {
		return ETL_VERSION_SPEC_PATH;
	}

	public String getCLIENT_SPEC_PATH() {
		return CLIENT_SPEC_PATH;
	}

	public String getETL_CREATION_SPEC_PATH() {
		return ETL_CREATION_SPEC_PATH;
	}

	public String getBACKUP_VERSION_SPEC_PATH() {
		return BACKUP_VERSION_SPEC_PATH;
	}

	public String getDEFAULT_HIVE_EXPRESSION_SPEC_PATH() {
		return DEFAULT_HIVE_EXPRESSION_SPEC_PATH;
	}

	private static Logger logger = LoggerFactory
			.getLogger(ETLInputOutputSheetCreation.class);

	JSONObject get_schedule_report_spec() throws IOException, ParseException {

		JSONObject jsonObject = generic_spec_provider(SCHEDULE_REPORT_SPEC_PATH);
		return jsonObject;

	}
	JSONObject get_report_spec() throws IOException, ParseException {

		JSONObject jsonObject = generic_spec_provider(REPORT_SPEC_PATH);
		return jsonObject;

	}
	
	JSONObject get_etl_spec() throws IOException, ParseException {

		JSONObject jsonObject = generic_spec_provider(CLIENT_SPEC_PATH);
		return jsonObject;

	}

	JSONObject getDefaultHiveExpressionSpec() throws IOException,
			ParseException {

		JSONObject jsonObject = generic_spec_provider(DEFAULT_HIVE_EXPRESSION_SPEC_PATH);
		return jsonObject;
	}

	JSONObject get_etl_version_spec() throws IOException, ParseException {

		JSONObject jsonObject = generic_spec_provider(ETL_VERSION_SPEC_PATH);
		return jsonObject;

	}

	JSONObject get_etl_creation_spec() throws IOException, ParseException {

		JSONObject jsonObject = generic_spec_provider(ETL_CREATION_SPEC_PATH);
		return jsonObject;
	}

	private JSONObject generic_spec_provider(String spec_path)
			throws IOException, ParseException {

		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(new FileReader(spec_path));
		} catch (FileNotFoundException e) {
			logger.error("FileNotFoundException in generic_spec_provider Function, File Name : "
					+ spec_path + "  Exception: " + e.getMessage());
			throw e;
		} catch (IOException e) {
			logger.error("IOException in generic_spec_provider Function, File Name : "
					+ spec_path + "  Exception: " + e.getMessage());
			throw e;
		} catch (ParseException e) {
			logger.error("IOException in generic_spec_provider Function, File Name : "
					+ spec_path + "  Exception: " + e.getMessage());
			throw e;
		}
		JSONObject jsonObject = (JSONObject) obj;
		return jsonObject;
	}

	public static void main(String[] args) throws IOException {

	}
}
