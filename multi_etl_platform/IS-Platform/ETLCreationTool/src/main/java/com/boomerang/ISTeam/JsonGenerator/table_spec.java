package com.boomerang.ISTeam.JsonGenerator;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.json.simple.JSONArray;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class table_spec {
	Logger logger = LoggerFactory.getLogger(table_spec.class);
	String table_name;
	String table_id;
	List<table_columns> columns = new ArrayList<table_columns>();
	String source_feed_join_type;
	List<String> source_feeds = new ArrayList<String>();
	List<String> join_columns = new ArrayList<String>();
	// String join_columns;
	String filter_criteria;
	String enable_tablewise_unique_constrain;
	String enable_dedup_on_pk_columns;
	String dedup_order;
	String is_temp_table;
	String target_endpoint;
	String target_table;
	String destination;
	String Client = "";

	public String getEnable_tablewise_unique_constrain() {
		return enable_tablewise_unique_constrain;
	}

	public String getDedup_order() {
		return dedup_order;
	}

	public void setEnable_tablewise_unique_constrain(
			String enable_tablewise_unique_constrain) {
		this.enable_tablewise_unique_constrain = enable_tablewise_unique_constrain;
	}

	public void setDedup_order(String dedup_order) {
		this.dedup_order = dedup_order;
	}

	public String getClient() {
		return Client;
	}

	public void setClient(String client) {
		Client = client;
	}

	public String getTable_name() {
		return table_name;
	}

	public void setTable_name(String table_name) {
		this.table_name = table_name;
	}

	public String getTable_id() {
		return table_id;
	}

	public void setTable_id(String table_id) {
		this.table_id = table_id;
	}

	public String getSource_feed_join_type() {
		return source_feed_join_type;
	}

	public void setSource_feed_join_type(String source_feed_join_type) {
		this.source_feed_join_type = source_feed_join_type;
	}

	public String getFilter_criteria() {
		return filter_criteria;
	}

	public void setFilter_criteria(String filter_criteria) {
		this.filter_criteria = filter_criteria;
	}

	public String getEnable_dedup_on_pk_columns() {
		return enable_dedup_on_pk_columns;
	}

	public void setEnable_dedup_on_pk_columns(String enable_dedup_on_pk_columns) {
		this.enable_dedup_on_pk_columns = enable_dedup_on_pk_columns;
	}

	public String getIs_temp_table() {
		return is_temp_table;
	}

	public void setIs_temp_table(String is_temp_table) {
		this.is_temp_table = is_temp_table;
	}

	public String getTarget_endpoint() {
		return target_endpoint;
	}

	public void setTarget_endpoint(String target_endpoint) {
		this.target_endpoint = target_endpoint;
	}

	public String getTarget_table() {
		return target_table;
	}

	public void setTarget_table(String target_table) {
		this.target_table = target_table;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public void addColumnToList(String column_name, String column_type,
			String source_mapping_type, String source_feed_col, String exp,
			String is_primary) {
		table_columns fd_col = new table_columns();
		fd_col.setColumn_name(column_name);
		fd_col.setColumn_type(column_type);
		fd_col.setSource_mapping_type(source_mapping_type);
		fd_col.setSource_feed_column(source_feed_col);
		fd_col.setExpression(exp);
		fd_col.setIs_primary_key(is_primary);
		columns.add(fd_col);
		// System.out.println(column_name+" added to Table");
	}

	public List<table_columns> getColumnList() {
		return columns;
	}

	public void addSourceTable(String src_table) {
		source_feeds.add(src_table);
	}

	public void addJoinCol(String join_col) {
		join_columns.add(join_col);
	}

	public void displayColumns() {
		table_columns col = null;
		for (int i = 0; i < columns.size(); i++) {
			col = columns.get(i);
			/*
			 * System.out.println("Column "+(i+1)+" is:");
			 * System.out.println("column_name: "+col.getColumn_name());
			 * System.out.println("column_type: "+col.getColumn_type());
			 * System.out
			 * .println("Source_mapping_type: "+col.getSource_mapping_type());
			 * System
			 * .out.println("Source_feed.column: "+col.getSource_feed_column());
			 * System.out.println("Expression: "+col.getExpression());
			 * System.out.println("is_primary_key: "+col.getIs_primary_key());
			 */
		}
	}

	public void set_table_spec_values(org.apache.poi.ss.usermodel.Sheet sheet,
			String sheet_name) {
		try {
			Row row;
			Cell cell;

			String table_name_id = sheet_name.substring(sheet_name
					.lastIndexOf(".") + 1);
			// System.out.println("table_name = "+table_name_id);
			// Set table_name and table_id
			table_name_id="table_"+table_name_id;
			this.setTable_name(table_name_id + ".json");
			this.setTable_id(table_name_id);

			row = sheet.getRow(2);

			// List<String> source_feeds = new ArrayList<String>();

			// Set source_feed_join_type;
			// cell=row.getCell(6);
			this.setSource_feed_join_type(read_etl_conf_excel.get_cell_data(
					row, 6));

			// Set filter_criteria;
			// cell=row.getCell(9);
			this.setFilter_criteria(read_etl_conf_excel.get_cell_data(row, 9));

			// Set enable_dedup_on_pk_columns;
			// cell=row.getCell(10);
			this.setEnable_tablewise_unique_constrain(read_etl_conf_excel
					.get_cell_data(row, 10));

			// Set is_temp_table;
			// cell=row.getCell(11);
			this.setIs_temp_table(read_etl_conf_excel.get_cell_data(row, 11));

			// Set target_endpoint;
			// cell=row.getCell(12);
			this.setTarget_endpoint(read_etl_conf_excel.get_cell_data(row, 12));

			// Set target_table;
			// cell=row.getCell(13);
			this.setTarget_table(read_etl_conf_excel.get_cell_data(row, 13));

			// set enable_dedup_on_pk_columns
			this.setEnable_dedup_on_pk_columns(read_etl_conf_excel
					.get_cell_data(row, 14));

			// set dedup_order
			this.setDedup_order(read_etl_conf_excel.get_cell_data(row, 15));

			// Set destination;
			this.setDestination("table/${{ID}}");

			boolean notNull = true;
			int rowCounter = 2;
			String column_name;
			String column_type;
			String source_mapping_type;
			String source_feed_col;
			String exp;
			String is_primary;
			try {
				while (notNull) {
					column_name = "";
					column_type = "";
					source_mapping_type = "";
					source_feed_col = "";
					exp = "";
					is_primary = "";

					row = sheet.getRow(rowCounter);
					cell = row.getCell(0);
					if (cell == null
							|| cell.getCellType() == Cell.CELL_TYPE_BLANK) {
						notNull = false;
					} else {
						column_name = read_etl_conf_excel.get_cell_data(row, 0);

						// cell=row.getCell(1);
						column_type = read_etl_conf_excel.get_cell_data(row, 1);

						// cell=row.getCell(2);
						source_mapping_type = read_etl_conf_excel
								.get_cell_data(row, 2);

						// cell=row.getCell(3);
						source_feed_col = read_etl_conf_excel.get_cell_data(
								row, 3);

						// cell=row.getCell(4);
						exp = read_etl_conf_excel.get_cell_data(row, 4);

						// cell=row.getCell(5);
						is_primary = read_etl_conf_excel.get_cell_data(row, 5);

						this.addColumnToList(column_name, column_type,
								source_mapping_type, source_feed_col, exp,
								is_primary);

						rowCounter++;
					}
				}

			} catch (NullPointerException ne) {

			}
			notNull = true;
			rowCounter = 2;
			String feed_name;
			String join;

			try {
				while (notNull) {
					feed_name = "";
					row = sheet.getRow(rowCounter);
					cell = row.getCell(7);
					if (cell == null
							|| cell.getCellType() == Cell.CELL_TYPE_BLANK) {
						notNull = false;
					} else {
						feed_name = read_etl_conf_excel.get_cell_data(row, 7);
						this.addSourceTable(feed_name);

						// cell=row.getCell(8);
						join = read_etl_conf_excel.get_cell_data(row, 8);
						this.addJoinCol(join);
						rowCounter++;
					}
				}
			} catch (NullPointerException ne) {

			}

		} catch (Exception e) {
			// System.out.println(e.getMessage());
			// System.out.println(this.getTable_name());
			e.printStackTrace();
		}
	}

	private String getPrimaryKey() {
		String primarykey = "";
		StringBuilder primarykeystr = new StringBuilder("");
		List<table_columns> column_pojos = this.getColumnList();
		for (table_columns column_pojo : column_pojos) {
			if (column_pojo.getIs_primary_key().equalsIgnoreCase("TRUE")) {
				primarykeystr.append(",").append(column_pojo.getColumn_name());
			}
		}
		if (primarykeystr.length() > 0) {
			if (primarykeystr.charAt(0) == ',') {
				primarykeystr.deleteCharAt(0);
			}
		}
		primarykey = primarykeystr.toString();
		return primarykey;
	}

	// private Cell getCellValue(Row row, int i) {
	// Cell cell = row.getCell(99);
	// return cell == null ? "";
	// }
	public String table_spec_json(String file) throws Exception {
		Map json = new LinkedHashMap();
		LinkedHashMap hconf = new LinkedHashMap();
		LinkedHashMap delim = new LinkedHashMap();
		List<LinkedHashMap> s3 = new ArrayList<LinkedHashMap>();
		List<LinkedHashMap> sf = new ArrayList<LinkedHashMap>();

		String primaryKey = this.getPrimaryKey();
		json.put("id", this.getTable_id());
		json.put("destination", this.getDestination());

		hconf.put("bucketSortKey", columns.get(0).getColumn_name());
		hconf.put("bucketCount", 10);
		json.put("hiveConfig", hconf);

		json.put("warningPercLimit", new Integer(80));

		JSONArray jsonArray = new JSONArray();
		LinkedHashMap s3_tmp;
		LinkedHashMap sf_tmp;

		String col_name = "";
		String col_type = "";
		String src_mapping_type = "";
		String src_feed_column = "";
		String exp = "";
		String is_pri_key = "";

		table_columns tcol;

		for (int i = 0; i < columns.size(); i++) {
			s3_tmp = new LinkedHashMap();
			sf_tmp = new LinkedHashMap();

			tcol = columns.get(i);

			col_name = tcol.getColumn_name();
			col_type = tcol.getColumn_type();
			src_mapping_type = tcol.getSource_mapping_type();
			src_feed_column = tcol.getSource_feed_column();
			exp = tcol.getExpression();
			is_pri_key = tcol.getIs_primary_key();

			s3_tmp.put("name", col_name);
			if (src_mapping_type.equalsIgnoreCase("FROM_EXPRESSION")
					|| src_mapping_type.equals("")) {
				sf_tmp.put("expr", exp);
			} else {
				sf_tmp.put("expr", src_feed_column);
			}
			sf_tmp.put("type", col_type);
			s3_tmp.put("source", sf_tmp);
			s3.add(s3_tmp);
			jsonArray.add(s3.get(i));
		}

		json.put("columns", jsonArray);
		s3_tmp = new LinkedHashMap();
		JSONArray row = new JSONArray();
		JSONArray table = new JSONArray();
		s3_tmp = new LinkedHashMap();
		s3_tmp.put("row", row);
		s3_tmp.put("table", table);
		json.put("validations", s3_tmp);

		JSONArray feeds = new JSONArray();
		s3_tmp = new LinkedHashMap();
		for (int i = 0; i < source_feeds.size(); i++) {
			sf_tmp = new LinkedHashMap();
			sf_tmp.put("name", source_feeds.get(i));
			sf_tmp.put("column", join_columns.get(i));
			feeds.add(sf_tmp);
		}
		if (this.getSource_feed_join_type().contains("join")) {
			s3_tmp.put("type", "join");
			s3_tmp.put("joinType", this.getSource_feed_join_type());
		} else if (this.getSource_feed_join_type().contains("union")) {
			s3_tmp.put("type", "union");
		}
		s3_tmp.put("feedSource", feeds);

		if (!this.getFilter_criteria().trim().equals("")) {
			s3_tmp.put("whereClause", this.getFilter_criteria());
		} else {
			s3_tmp.put("whereClause", "null");
		}
		if (!this.getEnable_dedup_on_pk_columns().trim().equals("")) {
			LinkedHashMap dedup_object = new LinkedHashMap();
			if (this.getEnable_dedup_on_pk_columns().trim()
					.equalsIgnoreCase("true")) {
				String primary_keys = primaryKey;

				if (!primary_keys.trim().equalsIgnoreCase("")) {
					dedup_object.put("columns", primary_keys);

					if (!this.getDedup_order().trim().equalsIgnoreCase("")) {
						dedup_object.put("order", this.getDedup_order());
					} else {
						dedup_object.put("order", "null");
					}

					s3_tmp.put("dedup", dedup_object);
				} else {
					Exception primary_key_not_found = new Exception(
							"Primary Key Not Found Exception, Please provide primary key to enable dedup");
					logger.error("Primary Key Not Found Exception, Please provide primary key to enable dedup in table."
							+ this.getTable_id());
					throw primary_key_not_found;
				}
			}

		}
		s3_tmp.put("groupBy", "null");

		json.put("tableSourcesConfig", s3_tmp);

		JSONArray end_points = new JSONArray();
		LinkedHashMap tg_tmp = new LinkedHashMap();
		LinkedHashMap ep_tmp = new LinkedHashMap();

		tg_tmp.put("endpoint", this.getTarget_endpoint());
		ep_tmp.put("table", this.getTarget_table());
		String gt = "<".toString();
		String lt = ">".toString();

		if (this.getEnable_tablewise_unique_constrain().trim()
				.equalsIgnoreCase("TRUE")) {
			if ((!primaryKey.trim().equalsIgnoreCase("")) && primaryKey != null) {
				ep_tmp.put("target_pk_columns", primaryKey);
			} else {
				Exception primary_key_not_found = new Exception(
						"Primary Key Not Found Exception, Please provide primary key to enable target_pk_columns");
				logger.error("Primary Key Not Found Exception, Please provide primary key to enable target_pk_columns in table."
						+ this.getTable_id());
				throw primary_key_not_found;
			}
		} else {
			logger.warn("target_pk_columns is not enabled. you might have duplicate values in : "
					+ this.getTarget_table());
		}
		String s = "Insert into <conf.table> select * from <conf.tempTable>;  drop table if exists <conf.tempTable> ;";

		ep_tmp.put("tableTransferTemplate", s);
		tg_tmp.put("args", ep_tmp);
		end_points.add(tg_tmp);

		if (!(this.getIs_temp_table().equalsIgnoreCase("true"))) {
			json.put("targets", end_points);
		} else {
			json.put("isTempTable", true);
		}
		String jsonText = JSONValue.toJSONString(json);
		jsonText = jsonText;
		return jsonText;
	}
}
