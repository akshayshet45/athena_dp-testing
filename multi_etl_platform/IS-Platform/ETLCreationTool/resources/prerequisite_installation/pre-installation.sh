#!/bin/bash
set -e

echo "Step1 : Building ETLCreationTool"

cd ../IS-Platform/ETLCreationTool
mvn clean install --quiet

echo "Step2: Installing packages"

echo "Installing/Upgrading pip Package"
sudo easy_install pip
sudo pip install --upgrade pip

echo "Installing openpyxl package , version: 2.2.0b1 ,using command:  python -m pip install openpyxl==2.2.0b1 "
sudo python -m pip install openpyxl==2.2.0b1

echo "Installing paramiko package , version: 1.16.0, using through command:  python -m pip install paramiko==1.16.0 "
sudo python -m pip install paramiko==1.16.0

echo "Installing psycopg2 package , version: 2.6.1, using command:  python -m pip install psycopg2==2.6.1 "
sudo python -m pip install psycopg2==2.6.1

echo "Installing MySQL-python package, using command:  sudo easy_install MySQL-python "
sudo easy_install MySQL-python

echo "Installing smart_open package, using command:  sudo pip install smart_open "
sudo pip install smart_open

echo "Installing twisted package, version: 15.5.0, using command:  sudo pip install twisted "
sudo pip install twisted==15.5.0

echo "Installing zope.interface package, , version: 4.1.3, using command:  sudo pip install zope.interface==4.1.3"
sudo pip install zope.interface==4.1.3




