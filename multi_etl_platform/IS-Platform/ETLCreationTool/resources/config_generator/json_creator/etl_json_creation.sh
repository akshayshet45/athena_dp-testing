#!/bin/bash
export SSHPASS=password

rootdir=`pwd`
#mail_scriptdir="$rootdir/office_depot/encryption/mail"

error_exit()
{

#       ----------------------------------------------------------------
#       Function for exit due to fatal program error
#               Accepts 1 argument:
#                       string containing descriptive error message
#       ----------------------------------------------------------------

    subject="ERROR : ETL Creation Excel to JSON"
    message="ERROR : ETL Creation Excel to JSON: ${1} on : `date +%Y-%m-%dT%H-%M-%S`"
   # /usr/bin/python "${mail_scriptdir}/send_mail.py" "${subject}" "${message}" 'ops@boomerangcommerce.com' 'etl@boomerangcommerce.com'
        exit 1
}

excel_output_file=${1}
report_directory=${2}

echo "excel_output_file: $excel_output_file"
echo "report_directory: $report_directory"

cd $rootdir

if [ -f "$excel_output_file" ]; then
    java -Xmx4096m -Xms512m -XX:PermSize=128m -XX:MaxPermSize=128m -Xss256k -XX:+AggressiveOpts -XX:+UseCompressedOops -XX:NewRatio=2 -XX:+PrintTenuringDistribution -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:+HeapDumpOnOutOfMemoryError -Xloggc:/tmp/garbage.log -jar ../IS-Platform/ETLCreationTool/resources/config_generator/json_creator/etl_excel_to_json.jar ${excel_output_file} ${report_directory} > /tmp/jsoncreation.log
    #java -cp "../IS-Platform/ETLCreationTool/target/ETLCreationTool-0.0.1-SNAPSHOT-jar-with-dependencies.jar" "com.boomerang.ISTeam.JsonGenerator.read_etl_conf_excel" ${excel_output_file} ${report_directory}
else
	error_exit "File : $excel_output_file not present"
fi

echo done
