from openpyxl import Workbook
from openpyxl.compat import range
from openpyxl.worksheet.datavalidation import DataValidation, ValidationType
from openpyxl.styles import Color, PatternFill, Font, Border
from openpyxl.styles import colors
from openpyxl.cell import Cell
from openpyxl import load_workbook
import csv
import sys

#script start
etl_workbook_path = sys.argv[1]
etl_workbook = load_workbook(etl_workbook_path)
input_sheet = input_wb.get_sheet_by_name("Input")
output_sheet = input_wb.get_sheet_by_name("Output")
etl_project = input_sheet['A2'].value

dest_filename = '/Users/yogesh/Downloads/' + etl_project + '_workbook.xlsx'
feeds = get_all_feeds(input_sheet)
tables = get_all_tables(output_sheet)

workbook = Workbook()
etl_spec_worksheet = workbook.active
#create etl_spec
create_etl_spec_xl_sheet(etl_spec_worksheet, etl_project)

#create one xl sheet per feed
for feed in feeds :
	create_feed_xl_sheet(feed, workbook)

#create one xl sheet per table
for table in tables :
	create_table_xl_sheet(table, workbook, feeds)

workbook.save(filename = dest_filename)
