create table IF NOT EXISTS temp.temp_item_cost_kishan1213_flywaytest(
	sku varchar(500) not null,
	channel_id Integer not null,
	effective_date timestamp not null,
	po_cost numeric(10,2) encode delta32k not null,
	shipping_cost numeric(10,2) encode delta32k default NULL,
	variable_cost numeric(10,2) encode delta32k default NULL,
	feed_date timestamp not null)
;
