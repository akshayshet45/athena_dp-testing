#!/bin/bash
set -e

FILENAME=$1
PASS=$2
HOST=$3
DB=$4
UNAME=$5
	
echo " "
if [ ! -f $FILENAME ]; then
	echo "File not found"
	exit -1
fi

PGPASSWORD=$PASS /usr/local/bin/psql -h $HOST -d $DB -U $UNAME -p 5439 -a -w --field-separator ";" -f $FILENAME;
exit 0