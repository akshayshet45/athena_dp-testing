

drop table if exists client.concat_catlog_item_price;
create table client.concat_catlog_item_price as(select i.sku,i.channel_id,i.effective_date,list_price,reg_price,offer_price,title,product_url,image_url, i.feed_date, i.creation_date from base.item_prices i , base.boomerang_catalog c 
where i.sku=c.sku and i.feed_date=(Select max(feed_date) from base.item_prices) limit 10);
