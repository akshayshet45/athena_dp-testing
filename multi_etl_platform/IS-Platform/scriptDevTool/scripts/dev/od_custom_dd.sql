\set ON_ERROR_STOP 1
\timing on

drop table if exists temp.promotions_custom;
create table temp.promotions_custom as(
	select distinct a.sku, a.channel_id
	from base.promotions a inner join
	(select sku, channel_id  from base.promotions
	where getdate() between (promo_start_date - 49) and promo_end_date and trunc(feed_date) = trunc(getdate()) group by sku,channel_id ) b  
  	on a.sku=b.sku and a.channel_id = b.channel_id and a.promo_id  not in (select distinct promo_id from base.dd_promo_exclusion)
	where getdate() < promo_end_date and trunc(a.feed_date) = (select trunc(max(feed_date)) from base.boomerang_catalog)
);


drop table if exists temp.child_sku;
create table temp.child_sku as 
select distinct sku
from 
(select distinct a.child_sku as sku from base.sku_relationships a 	
where a.relationship_name = 2 and  trunc(a.feed_date) = (select max(trunc(feed_date)) as max_date from base.sku_relationships ) 
group by sku
) ;


drop table if exists temp.performance_order_custom;
create table temp.performance_order_custom	 as
	select distinct sku,channel_id, page_views, unique_visits, gross_orders,gross_units, conv_rate,
	avg_daily_page_views, avg_daily_unique_visits,
	avg_daily_gross_orders,gross_revenue, gross_margin,
	ntile(100) over(partition by merch_l3_name order by page_views desc) as dept_views_tile,
	ntile(100) over(partition by merch_l4_name order by page_views desc) as class_views_tile,
	ntile(100) over(partition by merch_l3_name order by unique_visits desc) as dept_visit_tile,
	ntile(100) over(partition by merch_l4_name order by unique_visits desc) as class_visit_tile,
	ntile(100) over(partition by merch_l3_name order by conv_rate desc) as dept_conv_rate_tile,
	ntile(100) over(partition by merch_l4_name order by conv_rate desc) as class_conv_rate_tile
from
(
	select tbc.sku, tbc.channel_id, tbc.merch_l3_name, tbc.merch_l4_name,
	case when a.page_views is null then 0 else a.page_views end as page_views,
	case when a.unique_visits is null then 0 else a.unique_visits end as unique_visits,
	case when a.gross_orders is null then 0 else a.gross_orders end as gross_orders,
	case when a.gross_units is null then 0 else a.gross_units end as gross_units,
	case when a.conv_rate is null then 0 else a.conv_rate end as conv_rate,
	case when a.gross_revenue is null then 0 else a.gross_revenue end as gross_revenue,
	case when a.gross_margin is null then 0 else a.gross_margin end as gross_margin,
	case when a.last_week_avg_daily_page_views is null then 0 else a.last_week_avg_daily_page_views end as avg_daily_page_views,
	case when a.last_week_avg_daily_unique_visits is null then 0 else a.last_week_avg_daily_unique_visits end as avg_daily_unique_visits,
	case when a.last_week_avg_daily_gross_orders is null then 0 else a.last_week_avg_daily_gross_orders end as avg_daily_gross_orders
	from
	base.std_data_dictionary tbc
	left join
	(
	select a.sku,a.channel_id,
	case when sum(a.gross_revenue) is null then 0 else sum(a.gross_revenue) end as gross_revenue,
	case when sum(a.gross_revenue - a.gross_units*b.vendor_subsidized_po_cost) is null then 0 else sum(a.gross_revenue - a.gross_units*b.vendor_subsidized_po_cost) end as gross_margin,
	case when sum(a.page_views) is null then 0 else sum(a.page_views) end as page_views,
	case when sum(a.unique_visits) is null then 0 else sum(a.unique_visits) end as unique_visits,
	case when sum(a.gross_orders) is null then 0 else sum(a.gross_orders) end  as gross_orders,
	case when sum(a.gross_units) is null then 0 else sum(a.gross_units) end as gross_units,
	case when sum(a.unique_visits) = 0 then 0 else (sum(a.gross_orders)/sum(a.unique_visits))*100 end as conv_rate,
	(sum(a.page_views)/7) as last_week_avg_daily_page_views, sum(a.unique_visits)/7 as last_week_avg_daily_unique_visits,
	(sum(a.gross_orders)/7) as last_week_avg_daily_gross_orders
	from base.performance_order a
	inner join base.item_cost b on a.sku = b.sku  and trunc(a.transaction_Date) = (trunc(b.feed_date)-1 )
	where a.transaction_date between dateadd(day,-7, trunc(getdate())) and  dateadd(day,-1, trunc(getdate()))
	group by a.sku,a.channel_id) a
	on a.sku = tbc.sku and a.channel_id = tbc.channel_id
);


drop table if exists temp.margin_rate_custom;
create table temp.Margin_rate_custom as
select  distinct sku,channel_id, case when margin_rate = -99999 then null else margin_rate end as margin_rate,
ntile(100) over(partition by merch_l3_name order by margin_rate desc) as dept_margin_tile,
ntile(100) over(partition by merch_l4_name order by margin_rate desc) as class_margin_tile
from
(select 
tbc.sku,tbc.channel_id, tbc.merch_l3_name, tbc.merch_l4_name,
case when tp.margin_rate is null then -99999 else tp.margin_rate end as margin_rate
from
base.std_data_dictionary tbc 
left join 
(
	select distinct tip.sku,tip.channel_id,
	case when tip.reg_price = 0 then -99999 else ((tip.reg_price-tic.vendor_subsidized_po_cost)/tip.reg_price)  end as margin_rate
	from base.item_prices tip 
	inner join base.item_cost tic on tip.sku = tic.sku and tip.channel_id = tic.channel_id and tip.effective_date = tic.effective_date and tip.feed_date = (select trunc(max(feed_date)) from base.boomerang_catalog)
) tp on tp.sku = tbc.sku and tp.channel_id = tbc.channel_id
);


drop table if exists temp.gross_margin;
create table temp.gross_margin as (
    select a.sku, a.channel_id, (sum(b.gross_revenue) - sum(b.gross_units*c.vendor_subsidized_po_cost)) as gross_margin_in_x_days 
	from base.std_data_dictionary a 
	inner join base.performance_order b 
	on a.sku = b.sku and a.channel_id = b.channel_id
	inner join base.item_cost c 
	on a.sku = c.sku and b.transaction_Date = c.effective_date
	and b.transaction_date >= CURRENT_DATE - (select value from base.dd_parameters 
	where element = 'gross_margin_num_day' and created = (select max(created) from base.dd_parameters where element = 'gross_margin_num_day')):: INTERVAL
	group by a.sku, a.channel_id
);


drop table if exists temp.gross_margin_mean;
create table temp.gross_margin_mean as (
    select a.sku, a.channel_id,
	(sum(b.gross_revenue) - sum(b.gross_units*c.vendor_subsidized_po_cost))/(CURRENT_DATE::date - (CURRENT_DATE - (select value
	from base.dd_parameters where element = 'margin_mean_num_days'
	and created = (select max(created) from base.dd_parameters where element = 'margin_mean_num_days'))::INTERVAL)::date) as margin_mean_in_x_days
	from base.std_data_dictionary a
		inner join base.performance_order b on a.sku = b.sku and a.channel_id = b.channel_id
	and b.transaction_date >= CURRENT_DATE - (select value from base.dd_parameters where element = 'margin_mean_num_days'
	and created = (select max(created) from base.dd_parameters where element = 'margin_mean_num_days')):: INTERVAL
	inner join base.item_cost c on a.sku = c.sku and a.channel_id = c.channel_id and b.transaction_Date = c.effective_date
  group by  a.sku, a.channel_id
);


drop table if exists temp.gross_margin_std_dev;
create table temp.gross_margin_std_dev as (
    select sku,channel_id, stddev(total_gross_margin) as margin_stddev_in_x_days
	from 
	(select a.sku, a.channel_id,
	(sum(b.gross_revenue) - sum(b.gross_units*c.vendor_subsidized_po_cost)) as total_gross_margin
	from base.std_data_dictionary a
		inner join base.performance_order b on a.sku = b.sku and a.channel_id = b.channel_id
	and b.transaction_date >= CURRENT_DATE - (select value from base.dd_parameters where element = 'margin_std_dev_num_days'
	and created = (select max(created) from base.dd_parameters where element = 'margin_std_dev_num_days')::date)::INTERVAL
	inner join base.item_cost c on a.sku = c.sku and a.channel_id = c.channel_id and b.transaction_Date = c.effective_date
  group by  a.sku, a.channel_id)
  group by sku,channel_id
);






drop table if exists temp.last_week_metrics;
create table  temp.last_week_metrics as
select tbc.sku,tbc.channel_id,
	case when a.page_views is null then 0 else a.page_views end as page_views_last_week,
	case when a.unique_visits is null then 0 else a.unique_visits end as unique_visits_last_week,
	case when a.gross_units is null then 0 else a.gross_units end as gross_units_last_week,
	case when a.gross_revenue is null then 0 else a.gross_revenue end as gross_revenue_last_week,
	case when a.gross_margin is null then 0 else a.gross_margin end as gross_margin_last_week,
	case when a.gross_orders is null then 0 else a.gross_orders end as gross_orders_last_week
from
	base.std_data_dictionary tbc
	left join
	(
	select a.sku,a.channel_id,
	case when sum(page_views)  is null then 0 else sum(page_views) end as page_views,
	case when sum(unique_visits)  is null then 0 else sum(unique_visits) end as unique_visits,
	case when sum(gross_units) is null then 0 else sum(gross_units) end as gross_units,
	case when sum(gross_revenue) is null then 0 else sum(gross_revenue) end as gross_revenue,
	case when sum(gross_revenue - a.gross_units*b.vendor_subsidized_po_cost) is null then 0 else sum(gross_revenue - a.gross_units*b.vendor_subsidized_po_cost) end as gross_margin,
	case when sum(gross_orders) is null then 0 else sum(gross_orders) end  as gross_orders
	from base.performance_order a
	inner join base.item_cost b
	on a.sku = b.sku and a.channel_id = b.channel_id and trunc(a.transaction_Date) = (trunc(b.effective_date) )
	where a.transaction_date between dateadd(day,-14, trunc(current_date)) and  dateadd(day, -8,current_date)
	group by a.sku, a.channel_id
	)
a
	on a.sku = tbc.sku	and tbc.channel_id = a.channel_id	;

	
	
	
-------------------------  Comp metrics _---------------------------------------------------

drop table if exists temp.comp1_sas;
create table temp.comp1_sas as(
    select client_sku as sku, comp_sku, comp_name, comp_offer_price, comp_in_stock::integer as comp_in_stock, comp_is_add_on::integer as comp_is_add_on , comp_list_price
	from cm.pricing_adjusted_cm where comp_name = 'amazon.com' and comp_status = 'SAS' and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm )
);


drop table if exists temp.comp1_fbr;
create table temp.comp1_fbr as(
    select client_sku as sku, comp_sku, comp_name, comp_offer_price, comp_in_stock::integer as comp_in_stock, comp_is_add_on::integer as comp_is_add_on, comp_list_price
	from cm.pricing_adjusted_cm where comp_name = 'amazon.com' and comp_status = 'FBR' and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm )
);


drop table if exists temp.comp1_3pt;
create table temp.comp1_3pt as(
    select client_sku as sku, comp_sku, comp_name, comp_offer_price, comp_in_stock::integer as comp_in_stock, comp_is_add_on::integer as comp_is_add_on, comp_list_price
	from cm.pricing_adjusted_cm where comp_name = 'amazon.com' and comp_status = '3PT' and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm )
);


drop table if exists temp.comp1_unkn;
create table temp.comp1_unkn as(
    select client_sku as sku, comp_sku, comp_name, comp_offer_price, comp_in_stock::integer as comp_in_stock, comp_is_add_on::integer as comp_is_add_on, comp_list_price
	from cm.pricing_adjusted_cm where comp_name = 'amazon.com' and comp_status = 'UNKN' and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm )
);


drop table if exists temp.comp2_sas;
create table temp.comp2_sas as(
    select client_sku as sku, comp_sku, comp_name, comp_offer_price, comp_in_stock::integer as comp_in_stock, comp_is_add_on::integer as comp_is_add_on, comp_list_price
	from cm.pricing_adjusted_cm where comp_name = 'staples.com' and comp_status = 'SAS' and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm )
);


drop table if exists temp.comp2_fbr;
create table temp.comp2_fbr as(
    select client_sku as sku, comp_sku, comp_name, comp_offer_price, comp_in_stock::integer as comp_in_stock, comp_is_add_on::integer as comp_is_add_on, comp_list_price
	from cm.pricing_adjusted_cm where comp_name = 'staples.com' and comp_status = 'FBR' and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm )
);


drop table if exists temp.comp2_3pt;
create table temp.comp2_3pt as(
    select client_sku as sku, comp_sku, comp_name, comp_offer_price, comp_in_stock::integer as comp_in_stock, comp_is_add_on::integer as comp_is_add_on, comp_list_price
	from cm.pricing_adjusted_cm where comp_name = 'staples.com' and comp_status = '3PT' and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm )
);


drop table if exists temp.comp2_unkn;
create table temp.comp2_unkn as(
    select client_sku as sku, comp_sku, comp_name, comp_offer_price, comp_in_stock::integer as comp_in_stock, comp_is_add_on::integer as comp_is_add_on, comp_list_price
	from cm.pricing_adjusted_cm where comp_name = 'staples.com' and comp_status = 'UNKN' and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm )
);



drop table if exists temp.comp3_sas;
create table temp.comp3_sas as(
    select client_sku as sku, comp_sku, comp_name, comp_offer_price, comp_in_stock::integer as comp_in_stock, comp_is_add_on::integer as comp_is_add_on, comp_list_price
	from cm.pricing_adjusted_cm where comp_name = 'bestbuy.com' and comp_status = 'SAS' and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm )
);


drop table if exists temp.comp3_fbr;
create table temp.comp3_fbr as(
    select client_sku as sku, comp_sku, comp_name, comp_offer_price, comp_in_stock::integer as comp_in_stock, comp_is_add_on::integer as comp_is_add_on, comp_list_price
	from cm.pricing_adjusted_cm where comp_name = 'bestbuy.com' and comp_status = 'FBR' and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm )
);


drop table if exists temp.comp3_3pt;
create table temp.comp3_3pt as(
    select client_sku as sku, comp_sku, comp_name, comp_offer_price, comp_in_stock::integer as comp_in_stock, comp_is_add_on::integer as comp_is_add_on, comp_list_price
	from cm.pricing_adjusted_cm where comp_name = 'bestbuy.com' and comp_status = '3PT' and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm )
);


drop table if exists temp.comp3_unkn;
create table temp.comp3_unkn as(
    select client_sku as sku, comp_sku, comp_name, comp_offer_price, comp_in_stock::integer as comp_in_stock, comp_is_add_on::integer as comp_is_add_on, comp_list_price
	from cm.pricing_adjusted_cm where comp_name = 'bestbuy.com' and comp_status = 'UNKN' and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm )
);



drop table if exists temp.comp4_sas;
create table temp.comp4_sas as(
    select client_sku as sku, comp_sku, comp_name, comp_offer_price, comp_in_stock::integer as comp_in_stock, comp_is_add_on::integer as comp_is_add_on, comp_list_price
	from cm.pricing_adjusted_cm where comp_name = 'walmart.com' and comp_status = 'SAS' and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm )
);


drop table if exists temp.comp4_fbr;
create table temp.comp4_fbr as(
    select client_sku as sku, comp_sku, comp_name, comp_offer_price, comp_in_stock::integer as comp_in_stock, comp_is_add_on::integer as comp_is_add_on, comp_list_price
	from cm.pricing_adjusted_cm where comp_name = 'walmart.com' and comp_status = 'FBR' and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm )
);


drop table if exists temp.comp4_3pt;
create table temp.comp4_3pt as(
    select client_sku as sku, comp_sku, comp_name, comp_offer_price, comp_in_stock::integer as comp_in_stock, comp_is_add_on::integer as comp_is_add_on, comp_list_price
	from cm.pricing_adjusted_cm where comp_name = 'walmart.com' and comp_status = '3PT' and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm )
);


drop table if exists temp.comp4_unkn;
create table temp.comp4_unkn as(
    select client_sku as sku, comp_sku, comp_name, comp_offer_price, comp_in_stock::integer as comp_in_stock, comp_is_add_on::integer as comp_is_add_on, comp_list_price
	from cm.pricing_adjusted_cm where comp_name = 'walmart.com' and comp_status = 'UNKN' and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm )
);



drop table if exists temp.comp1_sas_last;
create table temp.comp1_sas_last as(
select a.client_sku as sku, b.comp_offer_price, (trunc(a.comp_scrape_date) - trunc(b.comp_scrape_date)) as days_since_last_price
	from cm.pricing_adjusted_cm a
  inner join
    cm.pricing_adjusted_cm b
    on a.client_sku = b.client_sku and a.comp_sku = b.comp_sku 
   and  a.comp_name = 'amazon.com' and a.comp_status = 'SAS' and b.comp_status = 'SAS'
  and a.feed_date = (select max(feed_date) from cm.pricing_adjusted_cm )
  and b.feed_date = (select max(c.feed_date) from cm.pricing_adjusted_cm c where b.comp_status = 'SAS' and b.client_sku = c.client_sku and
      b.comp_sku = c.comp_sku and  c.feed_date < a.feed_date and c.comp_offer_price != a.comp_offer_price )
);


drop table if exists temp.comp2_sas_last;
create table temp.comp2_sas_last as(
select a.client_sku as sku, b.comp_offer_price, (trunc(a.comp_scrape_date) - trunc(b.comp_scrape_date)) as days_since_last_price
	from cm.pricing_adjusted_cm a
  inner join
    cm.pricing_adjusted_cm b
    on a.client_sku = b.client_sku and a.comp_sku = b.comp_sku 
   and  a.comp_name = 'staples.com' and a.comp_status = 'SAS' and b.comp_status = 'SAS'
  and a.feed_date = (select max(feed_date) from cm.pricing_adjusted_cm )
  and b.feed_date = (select max(c.feed_date) from cm.pricing_adjusted_cm c where b.comp_status = 'SAS' and b.client_sku = c.client_sku and
      b.comp_sku = c.comp_sku and  c.feed_date < a.feed_date and c.comp_offer_price != a.comp_offer_price )
);

drop table if exists temp.comp3_sas_last;
create table temp.comp3_sas_last as(
select a.client_sku as sku, b.comp_offer_price, (trunc(a.comp_scrape_date) - trunc(b.comp_scrape_date)) as days_since_last_price
	from cm.pricing_adjusted_cm a
  inner join
    cm.pricing_adjusted_cm b
    on a.client_sku = b.client_sku and a.comp_sku = b.comp_sku 
   and  a.comp_name = 'bestbuy.com' and a.comp_status = 'SAS' and b.comp_status = 'SAS'
  and a.feed_date = (select max(feed_date) from cm.pricing_adjusted_cm )
  and b.feed_date = (select max(c.feed_date) from cm.pricing_adjusted_cm c where b.comp_status = 'SAS' and b.client_sku = c.client_sku and
      b.comp_sku = c.comp_sku and  c.feed_date < a.feed_date and c.comp_offer_price != a.comp_offer_price )
);


drop table if exists temp.comp4_sas_last;
create table temp.comp4_sas_last as(
select a.client_sku as sku, b.comp_offer_price, (trunc(a.comp_scrape_date) - trunc(b.comp_scrape_date)) as days_since_last_price
	from cm.pricing_adjusted_cm a
  inner join
    cm.pricing_adjusted_cm b
    on a.client_sku = b.client_sku and a.comp_sku = b.comp_sku 
   and  a.comp_name = 'walmart.com' and a.comp_status = 'SAS' and b.comp_status = 'SAS'
  and a.feed_date = (select max(feed_date) from cm.pricing_adjusted_cm )
  and b.feed_date = (select max(c.feed_date) from cm.pricing_adjusted_cm c where b.comp_status = 'SAS' and b.client_sku = c.client_sku and
      b.comp_sku = c.comp_sku and  c.feed_date < a.feed_date and c.comp_offer_price != a.comp_offer_price )
);


drop table if exists temp.comp_all;
create table temp.comp_all as
select distinct a.sku, a.channel_id, 
coalesce(c1a.comp_sku, c1b.comp_sku, c1c.comp_sku,c1d.comp_sku) as amazon_product_id , 
coalesce(c1a.comp_name, c1b.comp_name, c1c.comp_name,c1d.comp_name) as amazon_name, 
coalesce(c1a.comp_list_price, c1b.comp_list_price, c1c.comp_list_price,c1d.comp_list_price) as amazon_product_price, 
c1a.comp_offer_price as amazon_exact_price,
c1b.comp_offer_price as amazon_fbr_offer_price, c1c.comp_offer_price as amazon_third_party_offer_price, c1d.comp_offer_price as amazon_unkn_offer_price,
coalesce(c1a.comp_in_stock, c1b.comp_in_stock, c1c.comp_in_stock,c1d.comp_in_stock) as amazon_in_stock, 
coalesce(c1a.comp_is_add_on, c1b.comp_is_add_on, c1c.comp_is_add_on,c1d.comp_is_add_on) as amazon_is_add_on,
coalesce(c2a.comp_sku, c2b.comp_sku, c2c.comp_sku,c2d.comp_sku) as staples_product_id , 
coalesce(c2a.comp_name, c2b.comp_name, c2c.comp_name,c2d.comp_name) as staples_name, 
coalesce(c2a.comp_list_price, c2b.comp_list_price, c2c.comp_list_price,c2d.comp_list_price) as staples_product_price, 
c2a.comp_offer_price as staples_exact_price,
c2b.comp_offer_price as staples_fbr_offer_price, c2c.comp_offer_price as staples_third_party_offer_price, c2d.comp_offer_price as staples_unkn_offer_price,
coalesce(c2a.comp_in_stock, c2b.comp_in_stock, c2c.comp_in_stock,c2d.comp_in_stock) as staples_in_stock, 
coalesce(c2a.comp_is_add_on, c2b.comp_is_add_on, c2c.comp_is_add_on,c2d.comp_is_add_on) as staples_is_add_on,
coalesce(c3a.comp_sku, c3b.comp_sku, c3c.comp_sku,c3d.comp_sku) as bestbuy_product_id , 
coalesce(c3a.comp_name, c3b.comp_name, c3c.comp_name,c3d.comp_name) as bestbuy_name, 
coalesce(c3a.comp_list_price, c3b.comp_list_price, c3c.comp_list_price,c3d.comp_list_price) as bestbuy_product_price, 
c3a.comp_offer_price as bestbuy_exact_price,
c3b.comp_offer_price as bestbuy_fbr_offer_price, c3c.comp_offer_price as bestbuy_third_party_offer_price, c3d.comp_offer_price as bestbuy_unkn_offer_price,
coalesce(c3a.comp_in_stock, c3b.comp_in_stock, c3c.comp_in_stock,c3d.comp_in_stock) as bestbuy_in_stock, 
coalesce(c3a.comp_is_add_on, c3b.comp_is_add_on, c3c.comp_is_add_on,c3d.comp_is_add_on) as bestbuy_is_add_on,
coalesce(c4a.comp_sku, c4b.comp_sku, c4c.comp_sku,c4d.comp_sku) as walmart_product_id , 
coalesce(c4a.comp_name, c4b.comp_name, c4c.comp_name,c4d.comp_name) as walmart_name, 
coalesce(c4a.comp_list_price, c4b.comp_list_price, c4c.comp_list_price,c4d.comp_list_price) as walmart_product_price, 
c4a.comp_offer_price as walmart_exact_price,
c4b.comp_offer_price as walmart_fbr_offer_price, c4c.comp_offer_price as walmart_third_party_offer_price, c4d.comp_offer_price as walmart_unkn_offer_price,
coalesce(c4a.comp_in_stock, c4b.comp_in_stock, c4c.comp_in_stock,c4d.comp_in_stock) as walmart_in_stock, 
coalesce(c4a.comp_is_add_on, c4b.comp_is_add_on, c4c.comp_is_add_on,c4d.comp_is_add_on) as walmart_is_add_on,
c1l.comp_offer_price as amazon_last_known_price,  c1l.days_since_last_price as amazon_days_since_last_price,
c2l.comp_offer_price as staples_last_known_price, c2l.days_since_last_price as staples_days_since_last_price,
c3l.comp_offer_price as bestbuy_last_known_price, c3l.days_since_last_price as bestbuy_days_since_last_price,
c4l.comp_offer_price as walmart_last_known_price, c4l.days_since_last_price as walmart_days_since_last_price

from base.std_data_dictionary a  
left join temp.comp1_sas c1a 
on a.sku = c1a.sku 
left join temp.comp1_fbr c1b
on a.sku = c1b.sku 
left join temp.comp1_3pt c1c
on a.sku = c1c.sku 
left join temp.comp1_unkn c1d 
on a.sku = c1d.sku 
left join temp.comp2_sas c2a 
on a.sku = c2a.sku 
left join temp.comp2_fbr c2b
on a.sku = c2b.sku 
left join temp.comp2_3pt c2c
on a.sku = c2c.sku 
left join temp.comp2_unkn c2d 
on a.sku = c2d.sku 
left join temp.comp3_sas c3a 
on a.sku = c3a.sku 
left join temp.comp3_fbr c3b
on a.sku = c3b.sku 
left join temp.comp3_3pt c3c
on a.sku = c3c.sku 
left join temp.comp3_unkn c3d 
on a.sku = c3d.sku 	
left join temp.comp4_sas c4a 
on a.sku = c4a.sku 
left join temp.comp4_fbr c4b
on a.sku = c4b.sku 
left join temp.comp4_3pt c4c
on a.sku = c4c.sku 
left join temp.comp4_unkn c4d 
on a.sku = c4d.sku 
left join temp.comp1_sas_last c1l
on a.sku = c1l.sku 
left join temp.comp2_sas_last c2l
on a.sku = c2l.sku 
left join temp.comp3_sas_last c3l 
on a.sku = c3l.sku 
left join temp.comp4_sas_last c4l
on a.sku = c4l.sku ;
	

drop table if exists temp.optimizer_prices;	
create table temp.optimizer_prices as 
select sku, channel_id,optimizer_suggested_price
from base.optimizer_prices 
where feed_date = (select max(feed_date) from base.optimizer_prices) ;	




drop table if exists temp.elasticity;
create table temp.elasticity as (
	select distinct a.sku, a.elasticity 
	from base.elasticity a,
	(select trunc(max(creation_date)) max_date,sku  from base.elasticity group by sku ) b  
  where a.sku=b.sku and trunc(a.creation_date) = b.max_date 
	);
	
	
------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------	
	
	
drop table if exists dev.custom_data_dictionary;
create table dev.custom_data_dictionary as
select distinct a.sku, a.channel_id,
  case when b.sku is null then 0 else 1 end as on_promo,
  case when c.sku is null then 1 else 0 end is_head_sku,
  d.dept_visit_tile, d.class_visit_tile, d.dept_views_tile, d.class_views_tile, d.dept_conv_rate_tile, d.class_conv_rate_tile,
  d.avg_daily_page_views, d.avg_daily_unique_visits, d.avg_daily_gross_orders,
	d.page_views, d.unique_visits, d.gross_orders,d.gross_units, d.conv_rate,d.gross_revenue, d.gross_margin,
	e.margin_rate as margin, e.dept_margin_tile, e.class_margin_tile,
  f.gross_margin_in_x_days, g.margin_mean_in_x_days, h.margin_stddev_in_x_days,
  i.page_views_last_week, i.unique_visits_last_week, i.gross_units_last_week, i.gross_orders_last_week, i.gross_revenue_last_week, i.gross_margin_last_week,
  a.dd_date, t.test_control,m.min_margin,
comp.amazon_product_id,comp.amazon_name,comp.amazon_product_price,comp.amazon_exact_price,comp.amazon_fbr_offer_price,
comp.amazon_third_party_offer_price,comp.amazon_unkn_offer_price,comp.amazon_in_stock,comp.amazon_is_add_on,
comp.staples_product_id,comp.staples_name,comp.staples_product_price,comp.staples_exact_price,comp.staples_fbr_offer_price,
comp.staples_third_party_offer_price,comp.staples_unkn_offer_price,comp.staples_in_stock,comp.staples_is_add_on,
comp.bestbuy_product_id,comp.bestbuy_name,comp.bestbuy_product_price,comp.bestbuy_exact_price,comp.bestbuy_fbr_offer_price,
comp.bestbuy_third_party_offer_price,comp.bestbuy_unkn_offer_price,comp.bestbuy_in_stock,comp.bestbuy_is_add_on,
comp.walmart_product_id,comp.walmart_name,comp.walmart_product_price,comp.walmart_exact_price,comp.walmart_fbr_offer_price,
comp.walmart_third_party_offer_price,comp.walmart_unkn_offer_price,comp.walmart_in_stock,comp.walmart_is_add_on,
comp.amazon_last_known_price,comp.amazon_days_since_last_price,
comp.staples_last_known_price,comp.staples_days_since_last_price,comp.bestbuy_last_known_price,
comp.bestbuy_days_since_last_price,comp.walmart_last_known_price,comp.walmart_days_since_last_price,
opt.optimizer_suggested_price, elast.elasticity,
null as Gross_revenue_mean_pre, null as Gross_Margin_mean_pre, null as Gross_units_mean_pre, null as Gross_revenue_stddev, null as Gross_margin_stddev, null as Gross_units_stddev,
case when a.reg_price = 0 then null else ((a.reg_price - comp.amazon_exact_price)/ a.reg_price)*100 end as amazon_diff,
case when a.reg_price = 0 then null else ((a.reg_price - comp.staples_exact_price)/a.reg_price)*100 end as staples_diff,
case when a.reg_price = 0 then null else ((a.reg_price - comp.bestbuy_exact_price)/a.reg_price)*100 end as bestbuy_diff,
case when a.reg_price = 0 then null else ((a.reg_price - comp.walmart_exact_price)/a.reg_price)*100 end as walmart_diff

from base.std_data_dictionary a
left join temp.promotions_custom b on a.sku = b.sku and a.channel_id = b.channel_id
left join temp.child_sku c  on a.sku = c.sku
left join temp.performance_order_custom d on a.sku = d.sku and a.channel_id = d.channel_id
left join temp.margin_rate_custom e on a.sku = e.sku and a.channel_id = e.channel_id
left join temp.gross_margin f on a.sku = f.sku and a.channel_id = f.channel_id
left join  temp.gross_margin_mean g on a.sku = g.sku and a.channel_id = g.channel_id
left join temp.gross_margin_std_dev h on a.sku = h.sku and a.channel_id = h.channel_id
left join temp.last_week_metrics i on a.sku = i.sku and a.channel_id = i.channel_id
left join office_depot.test_control_history t on a.sku = t.sku 
left join base.min_margin m on a.sku = m.sku
left join temp.comp_all comp on a.sku = comp.sku and a.channel_id = comp.channel_id
left join temp.optimizer_prices opt on a.sku = opt.sku and a.channel_id = opt.channel_id
left join temp.elasticity as elast on a.sku = elast.sku;




-------------------- Merging std_dd with custom tables to get final DD 

drop table if exists dev.data_dictionary_interim ; 
create table dev.data_dictionary_interim as 
select a.sku, a.title, a.channel_id, a.upc, a.brand, a.is_marketplace, a.is_privatelabel, a.size, a.ean,a.isbn,a.manufacturer,a.merch_l1_name as division_name, a.merch_l1_id as division_id,
a.merch_l2_name as group_name, a.merch_l2_id as group_id, a.merch_l3_name as dept_name, a.merch_l3_id as dept_id, a.merch_l4_name as class_name , a.merch_l4_id as class_id, a.merch_l5_name as sub_class_name,
a.merch_l5_id as sub_class_id, a.product_url, a.map_price, a.list_price, a.reg_price, a.offer_price,
a.msrp as msrp, a.on_map as on_map, a.po_cost as po_cost, a.vendor_subsidized_po_cost as vendor_subsidized_po_cost,
  a.date, a.day_of_week,
  a.last_price as last_price, a.days_since_last_price_change as days_since_last_price_change,
  a.last_pre_price_suggestion_price, a.days_since_last_price_suggestion, a.last_suggested_price,
  a.conversion_rate_in_x_days,
  a.net_margin_in_x_days,
  a.revenue_mean_in_x_days, a.rev_stddev_in_x_days,
  a.gross_revenue_in_x_days, a.gross_orders_in_x_days, a.page_views_in_x_days, a.num_price_changes_in_x_days,  a.asp_x_days,

  b.on_promo as sku_in_promo, b.is_head_sku,
  b.dept_visit_tile, b.class_visit_tile, b.dept_views_tile, b.class_views_tile, b.dept_conv_rate_tile, b.class_conv_rate_tile,b.dept_margin_tile, b.class_margin_tile,
  b.avg_daily_page_views as last_week_avg_daily_page_views, b.avg_daily_unique_visits as last_week_avg_daily_unique_visits, b.avg_daily_gross_orders as last_week_avg_daily_gross_orders,
	b.page_views, b.unique_visits, b.gross_orders,b.gross_units, b.conv_rate,b.gross_revenue, b.gross_margin,b.margin, (a.reg_price - a.vendor_subsidized_po_cost) as abs_margin, 
  b.gross_margin_in_x_days, b.margin_mean_in_x_days, b.margin_stddev_in_x_days,
  b.page_views_last_week, b.unique_visits_last_week, b.gross_units_last_week, b.gross_orders_last_week, b.gross_revenue_last_week, b.gross_margin_last_week,
  a.dd_date, b.test_control,b.min_margin,
  b.amazon_product_id,b.amazon_name,b.amazon_product_price,b.amazon_exact_price,b.amazon_fbr_offer_price,
b.amazon_third_party_offer_price,b.amazon_unkn_offer_price,b.amazon_in_stock,b.amazon_is_add_on as amazon_is_addon,
b.staples_product_id,b.staples_name,b.staples_product_price,b.staples_exact_price,b.staples_fbr_offer_price,
b.staples_third_party_offer_price,b.staples_unkn_offer_price,b.staples_in_stock,b.staples_is_add_on as staples_is_addon,
b.bestbuy_product_id,b.bestbuy_name,b.bestbuy_product_price,b.bestbuy_exact_price,b.bestbuy_fbr_offer_price,
b.bestbuy_third_party_offer_price,b.bestbuy_unkn_offer_price,b.bestbuy_in_stock,b.bestbuy_is_add_on as bestbuy_is_addon,
b.walmart_product_id,b.walmart_name,b.walmart_product_price,b.walmart_exact_price,b.walmart_fbr_offer_price,
b.walmart_third_party_offer_price,b.walmart_unkn_offer_price,b.walmart_in_stock,b.walmart_is_add_on as walmart_is_addon,
b.amazon_last_known_price,b.amazon_days_since_last_price,
b.staples_last_known_price,b.staples_days_since_last_price,b.bestbuy_last_known_price,
b.bestbuy_days_since_last_price,b.walmart_last_known_price,b.walmart_days_since_last_price,
b.optimizer_suggested_price, b.elasticity,
b.Gross_revenue_mean_pre, b.Gross_Margin_mean_pre, b.Gross_units_mean_pre, b.Gross_revenue_stddev, b.Gross_margin_stddev, b.Gross_units_stddev,
b.amazon_diff, b.staples_diff, b.bestbuy_diff, b.walmart_diff
from 
	base.std_data_dictionary a 
	left join 
	dev.custom_data_dictionary b 
	on a.sku = b.sku and a.channel_id = b.channel_id ;
	
	
delete from dev.data_dictionary;
insert into dev.data_dictionary (Select * from dev.data_dictionary_interim);


----------------------- Versioning

delete from dev.data_dictionary_version_new where dd_date = (select max(feed_date) from base.boomerang_catalog) ;

insert into dev.data_dictionary_version_new 
(select * from dev.data_dictionary ) ;

