package com.boomerang.isteam.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.FlywayException;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boomerang.isteam.common.javabeans.DbSpec;
import com.boomerang.isteam.exception.DbEndPointNotFoundException;
import com.boomerang.isteam.exception.DbParameterNotFoundException;
import com.boomerang.isteam.exception.InvalidValueException;
import com.boomerang.isteam.exception.JsonKeyNotFoundException;
import com.boomerang.isteam.exception.JsonParseException;
import com.boomerang.isteam.utils.CommonUtility;
import com.boomerang.isteam.utils.GenericSpecReader;

public class MigrationManager {

	public static final String REDSHIFT_MIGRATION_SCRIPTS_LOCATION = "filesystem:migration";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_RESET = "\u001B[0m";
	private static final Logger LOGGER = LoggerFactory
			.getLogger(MigrationManager.class);

	private List<Flyway> clientList;

	public MigrationManager() {
		clientList = new ArrayList<Flyway>();
	}

	public static void main(String[] args) throws IOException, ParseException,
			DbParameterNotFoundException, DbEndPointNotFoundException,
			JsonParseException, JsonKeyNotFoundException, InvalidValueException {
		String env = null;
		if (args.length >= 1) {

			if (!args[0].isEmpty()
					&& (args[0].equalsIgnoreCase("dev") || args[0]
							.equalsIgnoreCase("prod"))) {
				env = args[0].toLowerCase();
			} else {
				LOGGER.error("env value is not valid, Please provide '-D env=dev' or prod while runing commmand");
				throw new InvalidValueException(
						" env value is not valid, Please provide '-D env=dev' or prod while runing commmand");
			}
		}
		MigrationManager migrateMngr = new MigrationManager();
		migrateMngr.execute(env);
	}

	public void execute(String env) throws IOException, ParseException,
			DbParameterNotFoundException, DbEndPointNotFoundException,
			JsonParseException, JsonKeyNotFoundException, InvalidValueException {

		System.out.println(ANSI_GREEN
				+ "Start Migration/Table Creation for Environment: " + env
				+ ANSI_RESET);

		migrate(env);
	}

	private void runAllMigrations() {
		for (Flyway flyway : clientList) {
			StringBuilder log = new StringBuilder("Migration/Table Creation ");
			int result = -1;
			try {
				flyway.repair();
				result = flyway.migrate();

				if (result >= 0) {
					log.append("are successful ");
					LOGGER.info(log.toString());
					System.out
							.println(ANSI_GREEN + log.toString() + ANSI_RESET);
				} else {
					log.append("have failed with result: " + result);
					LOGGER.error(log.toString());
				}

			} catch (FlywayException e) {
				log.append("have failed with Exception: " + e.getMessage());
				LOGGER.error(log.toString());
				e.printStackTrace();
				throw e;
			}
		}
	}

	private void addFlywayClient(Flyway flyway) {
		clientList.add(flyway);
	}

	private static Flyway getFlywayClient(String location, String env,
			String dbUrl, String username, String password) throws IOException,
			ParseException, JsonParseException, JsonKeyNotFoundException {
		Flyway flyway = new Flyway();
		flyway.setBaselineOnMigrate(true);
		flyway.setLocations(location + "/" + env);
		GenericSpecReader specReader = new GenericSpecReader();
		JSONObject client_spec = specReader.get_client_spec("dev");
		String devSchema = specReader.getSanboxSchema(client_spec);
		String projectName = specReader.getProjectName(client_spec);
		if (env.equals("dev")) {
			flyway.setSchemas(devSchema);
			flyway.setTable("scriptlet_matadata_" + projectName);
		} else {
			flyway.setTable("scriptlet_matadata_" + projectName);
		}
		flyway.setDataSource(dbUrl, username, password);
		return flyway;
	}

	public void migrate(String env) throws IOException, ParseException,
			DbParameterNotFoundException, DbEndPointNotFoundException,
			JsonParseException, JsonKeyNotFoundException {
		CommonUtility utility = new CommonUtility();
		DbSpec db = utility.getDbBeans("env_redshift", env);
		String dbUrl = "jdbc:redshift://" + db.getHost() + ":5439/"
				+ db.getDb();
		String username = db.getUname();
		String password = db.getPasswd();
		addFlywayClient(getFlywayClient(REDSHIFT_MIGRATION_SCRIPTS_LOCATION,
				env, dbUrl, username, password));
		runAllMigrations();
	}

}
