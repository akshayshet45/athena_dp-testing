package com.boomerang.isteam.dev_environment;

import java.io.IOException;
import java.sql.SQLException;

import org.json.simple.parser.ParseException;

import com.boomerang.isteam.common.DropManager;
import com.boomerang.isteam.common.MigrationManager;
import com.boomerang.isteam.common.SqlExecutor;
import com.boomerang.isteam.exception.DbEndPointNotFoundException;
import com.boomerang.isteam.exception.DbParameterNotFoundException;
import com.boomerang.isteam.exception.InvalidValueException;
import com.boomerang.isteam.exception.JsonKeyNotFoundException;
import com.boomerang.isteam.exception.JsonParseException;

public class DevMain {

	public static void main(String[] args) throws DbParameterNotFoundException,
			DbEndPointNotFoundException, JsonParseException,
			JsonKeyNotFoundException, ClassNotFoundException, IOException,
			SQLException, ParseException, InterruptedException,
			InvalidValueException {

		String filePath = args[0];
		filePath="scripts/dev/"+filePath;
		String env="dev";
		/*
		 * Step-0: Drop the tables in temp schema which is not created by Sanbox
		 * User
		 */
		//DropManager dropMngr = new DropManager();
		//dropMngr.execute(filePath);

		/*
		 * Step-1: Create/migrate table version using Flyway
		 */
		MigrationManager migrateMngr = new MigrationManager();
		migrateMngr.execute(env);
		
		/*
		 * Step-2: Execute SQL Script
		 */
		String endPointCredential = "sanboxCredential";
		SqlExecutor sqlexecuton = new SqlExecutor();

		sqlexecuton.execute(filePath, endPointCredential);

	}

}
