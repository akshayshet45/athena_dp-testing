package com.boomerang.isteam.common;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boomerang.isteam.common.javabeans.DbSpec;

public class SqlSyntaxChecker {
	private final Logger logger = LoggerFactory
			.getLogger(SqlSyntaxChecker.class);

	public static void main(String[] args) {

	}
	
	public void checkSqlSyntax(DbSpec db,String dbType, String scriptName) throws ClassNotFoundException, IOException, ParseException, SQLException{
		
		Connection connection=DBMetaDataProvider.getConnectionDB(dbType, db).getConnection();
	}

	private void executeQuery(Connection connection, String query)
			throws SQLException {

		Statement stmt = null;
		long startMillis;
		try {

			stmt = connection.createStatement();
			String sql = "EXPLAIN EXTENDED select " + query;
			ResultSet res = stmt.executeQuery(sql);
			if (res.next()) {
				res.getString(1);
			}
		}

		catch (SQLException e) {

			if (e.getMessage().contains("ParseException")) {
				logger.error("REDSHIFT PARSER reports ParseException : " + e.getMessage());
				throw e;
			} else {
				// logger.warn("Warning :" + e.getMessage());
			}
		} finally {
			try {
				if (stmt != null && !stmt.isClosed()) {
					stmt.close();
				}
			} catch (Exception e) {
				// LOGGER.error("Error while trying to close stmt", e);
			}
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (Exception e) {
				// LOGGER.error("Error while trying to close connection", e);
			}
		}

	}

}
