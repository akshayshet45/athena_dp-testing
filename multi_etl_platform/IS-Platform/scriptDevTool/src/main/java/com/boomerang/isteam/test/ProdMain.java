package com.boomerang.isteam.test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.sql.SQLException;

import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boomerang.isteam.common.DropManager;
import com.boomerang.isteam.common.MigrationManager;
import com.boomerang.isteam.common.SqlExecutor;
import com.boomerang.isteam.exception.DbEndPointNotFoundException;
import com.boomerang.isteam.exception.DbParameterNotFoundException;
import com.boomerang.isteam.exception.InvalidValueException;
import com.boomerang.isteam.exception.JsonKeyNotFoundException;
import com.boomerang.isteam.exception.JsonParseException;
import com.boomerang.isteam.exception.SanBoxSchemaReplacementNotFoundException;
import com.boomerang.isteam.utils.CommonUtility;

public class ProdMain {
	private static Logger logger = LoggerFactory.getLogger(CommonUtility.class);

	public static void main(String[] args) throws ClassNotFoundException,
			IOException, ParseException, SQLException, JsonParseException,
			JsonKeyNotFoundException, DbParameterNotFoundException,
			DbEndPointNotFoundException, InterruptedException, SanBoxSchemaReplacementNotFoundException, InvalidValueException {
		String srcScriptPath = args[0];
		srcScriptPath="scripts/dev/"+srcScriptPath;
		String endPointCredential = null;
		if (args.length <= 1) {
			endPointCredential = "prodCredential";
		} else {
			if (args[1] != null && (!args[1].equalsIgnoreCase("null"))) {
				endPointCredential = args[1];
			} else {
				endPointCredential = "prodCredential";
			}
		}
		// System.out.println("endPointCredential : "+endPointCredential);
		ProdMain prodMain=new ProdMain();
		prodMain.execute(srcScriptPath, endPointCredential, false);

	}

	public void execute(String srcScriptPath, String endPointCredential,
			boolean remoteExeFlag) throws ClassNotFoundException, IOException,
			ParseException, SQLException, JsonParseException,
			JsonKeyNotFoundException, DbParameterNotFoundException,
			DbEndPointNotFoundException, InterruptedException, SanBoxSchemaReplacementNotFoundException, InvalidValueException {

		
		/*
		 * Step-1: Create/migrate table version using Flyway
		 */
		String env="prod";
		MigrationManager migrateMngr = new MigrationManager();
		migrateMngr.execute(env);
		
		
		/*
		 * Step-2: Schema Replacer
		 */
		String prodDir = "scripts/prod/";
		// String srcScriptPath ="scripts/sample.sql";
		String delimeter = ";";
		SchemaReplacer replacer = new SchemaReplacer();
		replacer.execute(prodDir, srcScriptPath, delimeter);
		
		/*
		 * Step-3 : Prod Simulator
		 */
		ProdSimulator ps = new ProdSimulator();
		String tempDirPath = "scripts/prod/simulation";

		File srcFile = new File(srcScriptPath);
		String fileName = srcFile.getName();

		String scriptPath = prodDir + fileName;
		String transactionBegin = "begin;";
		String transactionEnd = "rollback;";

		Path destScriptPath = ps.insertTransaction(scriptPath, delimeter,
				tempDirPath, transactionBegin, transactionEnd);
		
		if (remoteExeFlag == false) {
			SqlExecutor executor = new SqlExecutor();
			executor.execute(destScriptPath.toString(), endPointCredential);

			File tempFile = destScriptPath.toFile();
			tempFile.delete();
		}

	}

}
