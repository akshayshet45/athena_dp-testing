package com.boomerang.isteam.common;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boomerang.isteam.common.javabeans.DbSpec;
import com.boomerang.isteam.exception.DbEndPointNotFoundException;
import com.boomerang.isteam.exception.DbParameterNotFoundException;
import com.boomerang.isteam.exception.JsonKeyNotFoundException;
import com.boomerang.isteam.exception.JsonParseException;
import com.boomerang.isteam.utils.CommonUtility;
import com.boomerang.isteam.utils.GenericSpecReader;
import com.boomerang.isteam.utils.SqlHelper;
import com.boomerang.isteam.utils.SqlTableFinder;

public class DropManager {
	private static String DROP = "drop table if exists ";
	private final Logger logger = LoggerFactory.getLogger(DropManager.class);
	public static void main(String[] args) throws ClassNotFoundException,
			IOException, ParseException, SQLException {
		
	}

	public void execute(String filePath) throws IOException,ClassNotFoundException,SQLException,ParseException,DbParameterNotFoundException,DbEndPointNotFoundException,JsonParseException,JsonKeyNotFoundException   {
		HashMap<String, String> finalDropTableMap = new HashMap<String, String>();
		String path = filePath;
		SqlTableFinder tableFinder = new SqlTableFinder();
		ArrayList<String> dropTableList;
		try {
			dropTableList = tableFinder.getDropTableList(path);
		} catch (IOException e) {
			logger.error("Exception while creating DropTableList");
			throw e;
		}
		//System.out.println(dropTableList);

		HashMap<String, String> tableOwnerMap;
		try {
			//tableOwnerMap = DBMetaDataProvider.getTableOwnerMap();
			tableOwnerMap = DBMetaDataProvider.getTempTableOwnerMap();
		} catch (ClassNotFoundException | SQLException | ParseException
				| DbParameterNotFoundException | DbEndPointNotFoundException
				| JsonParseException | JsonKeyNotFoundException e) {
			logger.error("Exception while creating Map<table_name,Owner>");
			throw e;
		}
		//System.out.println(tableOwnerMap);

		String schema = "";
		String table = "";
		String tempSchema = "";
		GenericSpecReader specReader=new GenericSpecReader(); 
		JSONObject clientSpec=specReader.get_client_spec("dev");
		
		if (clientSpec.containsKey("tempSchema")) {
			tempSchema = (String) clientSpec.get("tempSchema");
		}
		DbSpec sanboxDbJavaBean = DBMetaDataProvider.getSanboxDbJavaBean();
		String sanboxUsername = sanboxDbJavaBean.getUname().trim();
		String sanboxHost = sanboxDbJavaBean.getHost().trim();

		
		HashMap<String, JSONObject> endpoint = specReader.getEndpointSpecMap(clientSpec);
		
		
		Set<String> endpointNames = endpoint.keySet();
		CommonUtility utility = new CommonUtility();

		for (String dropTable : dropTableList) {

			String[] dropSchemaTable = dropTable.split("\\.");
			//System.out.println("dropTable : " + dropTable);
			//System.out.println(dropSchemaTable[1]);
			schema = dropSchemaTable[0].trim();
			table = dropSchemaTable[1].trim();

			//System.out.println("schema: " + schema);
			//System.out.println("temp schema" + tempSchema + "  " + schema);
			if (schema.equalsIgnoreCase(tempSchema)) {
				//System.out.println("tableOwnerMap: "+tableOwnerMap);
				if (tableOwnerMap.containsKey(dropTable.trim())) {
					String trueOwner = tableOwnerMap.get(dropTable.trim());
					//System.out.println(trueOwner + "   " + sanboxUsername);
					if (!trueOwner.equals(sanboxUsername)) {
						finalDropTableMap.put(dropTable, trueOwner);

						Boolean EPflag = false;
						for (String endpointName : endpointNames) {
							DbSpec db = utility.getDbBeans(endpointName,"dev");
							if (db.getHost().equals(sanboxHost)
									&& db.getUname()
											.equalsIgnoreCase(trueOwner)
									&& EPflag != true) {
								JSONObject endpointObj = endpoint
										.get(endpointName);
								SqlHelper sqlJdbc = DBMetaDataProvider
										.getConnectionDB((String) endpointObj
												.get("type"), db);
								String dropStmt = DROP + dropTable;
								sqlJdbc.executeUpdate(dropStmt);
								//System.out.println("Yes we are in " + dropStmt);
								EPflag = true;
							}
						}
						
						DbSpec db = utility.getDbBeans("env_redshift","prod");
						if (db.getHost().equals(sanboxHost)
								&& db.getUname()
										.equalsIgnoreCase(trueOwner)
								&& EPflag != true) {
							JSONObject endpointObj = endpoint
									.get("env_redshift");
							SqlHelper sqlJdbc = DBMetaDataProvider
									.getConnectionDB((String) endpointObj
											.get("type"), db);
							String dropStmt = DROP + dropTable;
							sqlJdbc.executeUpdate(dropStmt);
							//System.out.println("Yes we are in 2" + dropStmt);
							EPflag = true;
						}
						
						
						if (EPflag==false){
							logger.error("please provide credential for user : "+trueOwner + " ,host : "+sanboxHost+" in endpoints at client-spec.json or manually delete the table : "+dropTable);	
							throw new DbEndPointNotFoundException("please provide credential for user : "+trueOwner + " ,host : "+sanboxHost+" in endpoints at client-spec.json or manually delete the table : "+dropTable);
						}
					}
				}
			}
		}

	}
}
