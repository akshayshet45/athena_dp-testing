package com.boomerang.isteam.exception;

import java.io.IOException;

public class RemoteMachineEndPointNotFoundException extends IOException {

	public RemoteMachineEndPointNotFoundException(String s) {
		super(s);
	}
}
