package com.boomerang.isteam.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class SqlTableFinder {

	public static void main(String[] args){

		/*
		 * String s =
		 * "drop table if exists temp.promotions_custom  ok1 ok huge;";
		 * SqlTableFinder stf= new SqlTableFinder(); stf.matchDropKeyword(s);
		 */
		/*
		 * String path = "scripts/od_custom_dd.sql"; SqlTableFinder stf= new
		 * SqlTableFinder(); stf.getDropTableList(path);
		 */

	/*	CommonUtility utility = new CommonUtility();
		DbSpec db = utility.getDbBeans("sanbox");
		System.out.println(db.getHost());*/
		
		
		/*String schema="base";
		String sqlLine="create table temp.promotions_custom as base select distinct a.sku, a.channel_id from base.promotions a inner join (select sku, channel_id  from base.promotions where getdate() between (promo_start_date - 49) and promo_end_date and trunc(feed_date) = trunc(getdate()) group by sku,channel_id ) b   on a.sku=b.sku and a.channel_id = b.channel_id and a.promo_id  not in (select distinct promo_id from base.dd_promo_exclusion) where getdate() < promo_end_date and trunc(a.feed_date) = (select trunc(max(feed_date)) base.boomerang_catalog1 from base.boomerang_catalog) )".trim().toLowerCase();
		SqlTableFinder stf= new SqlTableFinder();
		//System.out.println(stf.getTables(schema, sqlLine));
*/	}

	public ArrayList<String> getDropTableList(String path) throws IOException {
		String delimeter = ";";
		ArrayList<String> dropTableList = new ArrayList<String>();
		CommonUtility utility = new CommonUtility();
		ArrayList<String> scriptLineList = utility.readFile(path, delimeter);
		for (String scriptLine : scriptLineList) {
			String droptable = matchDropKeyword(scriptLine);
			if (droptable != null && droptable != "") {
				dropTableList.add(droptable);
			}
		}
		return dropTableList;

	}

	String matchDropKeyword(String sqlLine) {

		// String s =
		// "drop table if exists temp.promotions_custom  ok1 ok huge;".trim().toLowerCase();
		sqlLine = sqlLine.trim().toLowerCase();
		String s1 = "";
		String s2 = "";
		boolean matchflag = false;
		Pattern P;
		Matcher M = null;
		P = Pattern.compile("^drop\\s+table\\s+if\\s+exists\\s+(.*);");
		M = P.matcher(sqlLine);
		matchflag = M.find();
		if (matchflag) {
			s1 = M.group(1).trim();
			//System.out.println(s1);
		} else {
			P = Pattern.compile("^drop\\s+table\\s+(.*);");
			//System.out.println("in else");
			M = P.matcher(sqlLine);
			matchflag = M.find();
			if (matchflag) {
				s1 = M.group(1).trim();
				//System.out.println(s1);
			}
		}
		if (matchflag) {
			Pattern P1 = Pattern.compile("^[a-zA-Z0-9_]+\\.[a-zA-Z0-9_]+");
			Matcher M1 = P1.matcher(s1);
			if (M1.find()) {
				s2 = M1.group(0).trim();
				//System.out.println(s2.trim());
				return s2;
			}
		}
		return null;

	}
	
	public ArrayList<String> getTables(String schema, String sqlLine){
		ArrayList<String> tableList = new ArrayList<String>();
		
		Pattern P1 = Pattern.compile(schema.trim()+"\\.[a-zA-Z0-9_]+");
		Matcher M1 = P1.matcher(sqlLine);
		while (M1.find()) {
			String table = M1.group();
			tableList.add(table.trim());
		}
		
		return tableList;
	}

}
