package com.boomerang.isteam.exception;

public class DeploymentArtifactNotFoundException extends Exception {
	public DeploymentArtifactNotFoundException(String s) {
		super(s);
	}
}
