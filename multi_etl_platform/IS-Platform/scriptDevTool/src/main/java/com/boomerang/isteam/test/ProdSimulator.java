package com.boomerang.isteam.test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

import org.json.simple.parser.ParseException;

import com.boomerang.isteam.utils.CommonUtility;

public class ProdSimulator {

	public static void main(String[] args) throws IOException, ParseException {
/*		ProdSimulator ps = new ProdSimulator();
		String tempDirPath = "Temp/";
		String scriptPath = "prod/scripts/sample.sql";
		String delimeter = ";";
		String transactionBegin = "begin;";
		String transactionEnd = "rollback;";
		Path destScriptPath = ps.insertTransaction(scriptPath, delimeter,
				tempDirPath, transactionBegin, transactionEnd);
		String endPointCredential="prodCredential";
		SqlExecutor executor = new SqlExecutor();
		executor.execute(destScriptPath.toString(),endPointCredential);*/
	}

	Path insertTransaction(String scriptPath, String delimeter,
			String tempDirPath, String transactionBegin, String transactionEnd) throws IOException {
		CommonUtility utility = new CommonUtility();
		ArrayList<String> scriptlines = utility.readFile(scriptPath.toString(),
				delimeter);
		Path destScriptPath = utility.createEmptyFile(scriptPath, tempDirPath);
		FileWriter fw = null;
		try {
			fw = new FileWriter(destScriptPath.toString());

			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(transactionBegin);
			bw.flush();
			bw.newLine();
			bw.flush();
			for (String scriptLine : scriptlines) {

				if (scriptLine == null || scriptLine.trim().equals("")) {
					//System.out.println("in if condition====");
					bw.newLine();
					bw.flush();
				} else {
					bw.write(scriptLine);
					bw.newLine();
					bw.flush();
				}

			}
			bw.newLine();
			bw.flush();
			bw.write(transactionEnd);
			bw.flush();
			bw.close();
		} catch (IOException e) {
			throw new IOException("IOException while inserting Transaction in "+destScriptPath.toString()+" \r\n"+e);
		}
		return destScriptPath;
	}
}
