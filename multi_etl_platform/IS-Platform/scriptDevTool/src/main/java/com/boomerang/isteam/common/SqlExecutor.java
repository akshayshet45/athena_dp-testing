package com.boomerang.isteam.common;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boomerang.isteam.common.javabeans.DbSpec;
import com.boomerang.isteam.exception.DbEndPointNotFoundException;
import com.boomerang.isteam.exception.DbParameterNotFoundException;
import com.boomerang.isteam.exception.JsonKeyNotFoundException;
import com.boomerang.isteam.exception.JsonParseException;
import com.boomerang.isteam.utils.CommonUtility;
import com.boomerang.isteam.utils.ExecuteCommand;
import com.boomerang.isteam.utils.GenericSpecReader;

public class SqlExecutor {
	private static Logger logger = LoggerFactory.getLogger(SqlExecutor.class);

	public static void main(String[] args) {
		/*
		 * SqlExecutor sqlexecuton = new SqlExecutor();
		 * sqlexecuton.execute(args[0]);
		 */
	}

	public void execute(String scriptName, String credential)
			throws IOException, ParseException, JsonParseException,
			DbParameterNotFoundException, DbEndPointNotFoundException, JsonKeyNotFoundException, InterruptedException {
		System.out.println("-->ScriptName: " + scriptName);
		System.out
				.println("=========================================================================");
		System.out
				.println("=========================================================================\n");
		System.out.println("OUTPUT OF SCRIPT : ");
		System.out.println("-----------------");
		
		String workingDir = System.getProperty("user.dir");
		String sqlFilePath = workingDir + "/" + scriptName;
		String shFilePath = "resources/DmlSql-0.0.1.sh";
		String cmdFilePath="resources/windows_script_executor-0.0.1.cmd";
		//JSONObject clientSpecObj = GenericSpecReader.get_client_spec();
		String dbEndPointName;
		DbSpec db = null;
		CommonUtility utility = new CommonUtility();
		if (credential == "sanboxCredential") {
			
				dbEndPointName = "env_redshift";
				db = utility.getDbBeans(dbEndPointName,"dev");
			
		} else if (credential == "prodCredential") {
			
				dbEndPointName = "env_redshift";
				db = utility.getDbBeans(dbEndPointName,"prod");
			
		} else {
			db = utility.getDbBeans(credential,"dev");
		}
		String password = db.getPasswd();
		String host = db.getHost();
		String database = db.getDb();
		String username = db.getUname();
		String returnval = null;
		ExecuteCommand command = new ExecuteCommand();
		StringBuilder execute = new StringBuilder();
		execute.append(sqlFilePath).append(CommonConstants.SPACE)
				.append(password).append(CommonConstants.SPACE).append(host)
				.append(CommonConstants.SPACE).append(database)
				.append(CommonConstants.SPACE).append(username);

		String output = null;
		
			try {
				output = command.executeCommands(shFilePath,cmdFilePath, execute.toString());
			} catch (InterruptedException e) {
				logger.error(""+e);
				throw e;
			}
		

		returnval = (output.toUpperCase().contains("ERROR")) ? "failure"
				: "succeess";
		logger.info("Execution Status: " + returnval);
		System.out
				.println("\n=========================================================================");
		System.out
				.println("=========================================================================");

	}
}
