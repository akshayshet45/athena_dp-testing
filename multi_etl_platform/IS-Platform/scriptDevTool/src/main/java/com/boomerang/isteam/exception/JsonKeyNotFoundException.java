package com.boomerang.isteam.exception;

public class JsonKeyNotFoundException extends Exception {
	public JsonKeyNotFoundException(String s) {
		super(s);
	}
}