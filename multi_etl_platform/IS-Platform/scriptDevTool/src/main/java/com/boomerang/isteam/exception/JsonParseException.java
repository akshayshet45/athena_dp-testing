package com.boomerang.isteam.exception;

public class JsonParseException extends Exception {
	public JsonParseException(String s) {
		super(s);
	}
}
