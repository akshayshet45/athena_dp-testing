package com.boomerang.isteam.exception;

public class DbParameterNotFoundException extends Exception {
	public DbParameterNotFoundException(String s) {
		super(s);
	}
}
