package com.boomerang.isteam.exception;

import java.io.IOException;

public class PrivateKeyIOException extends IOException {
	public PrivateKeyIOException(String s) {
		super(s);
	}
}