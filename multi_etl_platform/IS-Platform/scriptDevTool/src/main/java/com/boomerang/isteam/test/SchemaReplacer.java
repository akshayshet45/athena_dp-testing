package com.boomerang.isteam.test;

import com.boomerang.isteam.common.DBMetaDataProvider;
import com.boomerang.isteam.exception.DbEndPointNotFoundException;
import com.boomerang.isteam.exception.DbParameterNotFoundException;
import com.boomerang.isteam.exception.JsonKeyNotFoundException;
import com.boomerang.isteam.exception.JsonParseException;
import com.boomerang.isteam.exception.SanBoxSchemaReplacementNotFoundException;
import com.boomerang.isteam.utils.CommonUtility;
import com.boomerang.isteam.utils.GenericSpecReader;
import com.boomerang.isteam.utils.SqlTableFinder;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SchemaReplacer {
	private final Logger logger = LoggerFactory.getLogger(SchemaReplacer.class);

	public static void main(String[] args) {
		// SchemaReplacer replacer = new SchemaReplacer();
		// replacer.execute(prodDir,srcScriptPath,delimeter);
	}

	public void execute(String prodDir, String srcScriptPath, String delimeter)
			throws IOException, ParseException, ClassNotFoundException,
			SQLException, JsonParseException, JsonKeyNotFoundException,
			DbParameterNotFoundException, DbEndPointNotFoundException,
			SanBoxSchemaReplacementNotFoundException {
		CommonUtility utility = new CommonUtility();
		// String prodDir = "Prod/";
		// String srcScriptPath = "scripts/sample.sql";
		// String delimeter = ";";
		GenericSpecReader specReader = new GenericSpecReader();
		JSONObject client_spec = specReader.get_client_spec("dev");
		String schema = specReader.getSanboxSchema(client_spec);
		String tempSchema = specReader.getTempSchema(client_spec);
		String tempDevSchema = specReader.getTempDevSchema(client_spec);
		Path destScriptPath = utility.createEmptyFile(srcScriptPath, prodDir);
		ArrayList<String> scriptlines;
		try {
			scriptlines = utility.readFile(srcScriptPath.toString(), delimeter);
		} catch (IOException e1) {
			logger.error("Exception occur during reading file from "
					+ srcScriptPath.toString());
			throw e1;
		}
		// System.out.println(scriptlines);
		try {
			replaceSchema(schema, utility, scriptlines, destScriptPath,
					tempSchema, tempDevSchema);
		} catch (ClassNotFoundException | IOException | ParseException
				| SQLException | DbParameterNotFoundException
				| DbEndPointNotFoundException | JsonParseException
				| JsonKeyNotFoundException e) {
			logger.error("Exception occur during replacing 'sanbox schema' with 'prod schema'");
			throw e;
		}
	}

	private void replaceSchema(String schema, CommonUtility utility,
			ArrayList<String> scriptlines, Path destScriptPath,
			String tempSchema, String tempDevSchema) throws IOException,
			ClassNotFoundException, ParseException, SQLException,
			DbParameterNotFoundException, DbEndPointNotFoundException,
			JsonParseException, JsonKeyNotFoundException,
			SanBoxSchemaReplacementNotFoundException {

		SqlTableFinder stf = new SqlTableFinder();
		ArrayList<String> allSchemaLists = DBMetaDataProvider.getSchemaList();
		Set<String> alltableList = DBMetaDataProvider.getTableOwnerMap()
				.keySet();
		FileWriter fw;
		try {
			fw = new FileWriter(destScriptPath.toString());
		} catch (IOException e) {
			throw new IOException("Not Able to Write on File at : "
					+ destScriptPath.toString() + "\r\n" + e);
		}
		BufferedWriter bw = new BufferedWriter(fw);
		for (String scriptLine : scriptlines) {
			String writeLine = "";
			ArrayList<String> sanboxTableLists = stf.getTables(schema,
					scriptLine);
			// System.out.println("sanboxTableLists " + sanboxTableLists);
			if (scriptLine == null || scriptLine.trim().equals("")) {
				// System.out.println("in if condition====");
				try {
					bw.newLine();
					bw.flush();
				} catch (IOException e) {
					throw new IOException("Not Able to Write on File at : "
							+ destScriptPath.toString() + "\r\n" + e);
				}

			} else {

				if (sanboxTableLists != null && (!sanboxTableLists.isEmpty())) {
					HashMap<String, String> replaceMap = getReplaceMap(
							sanboxTableLists, allSchemaLists, alltableList);
					String replacedScriptLine = replace(scriptLine, replaceMap);
					writeLine = replacedScriptLine;

				} else {
					writeLine = scriptLine;

				}
				writeLine = writeLine.replace(tempDevSchema+".", tempSchema+".");
				bw.write(writeLine);
				bw.newLine();
				bw.flush();
			}

		}
		bw.close();
	}

	private HashMap<String, String> getReplaceMap(
			ArrayList<String> sanboxTableLists,
			ArrayList<String> allSchemaLists, Set<String> alltableList)
			throws ClassNotFoundException, IOException, ParseException,
			SQLException, SanBoxSchemaReplacementNotFoundException {

		HashMap<String, String> replaceMap = new HashMap<String, String>();
		ArrayList<String> unmatchedTableList = new ArrayList<String>();
		for (String sanboxTable : sanboxTableLists) {
			Boolean matchFound = false;
			for (String schema : allSchemaLists) {

				String[] temp = sanboxTable.trim().split("\\.");
				String srcTable = temp[1];
				String replaceBy = schema.toLowerCase() + "."
						+ srcTable.toLowerCase();
				if (alltableList.contains(replaceBy)) {
					replaceMap.put(sanboxTable, replaceBy);
					matchFound = true;
				}

			}
			if (matchFound == false) {
				unmatchedTableList.add(sanboxTable);
			}

		}
		if (!unmatchedTableList.isEmpty()) {
			logger.error("No Match Found Exception for "
					+ unmatchedTableList
					+ " ,Please Create listed tables in your production Schema.");
			throw new SanBoxSchemaReplacementNotFoundException(
					"No Match Found Exception for "
							+ unmatchedTableList
							+ " ,Please Create listed tables in your production Schema.");
		}

		// System.out.println("replaceMap : " + replaceMap);
		return replaceMap;
	}

	private String replace(String scriptLine, HashMap<String, String> replaceMap) {
		String newScriptLine = scriptLine;
		Set<String> sanboxTables = replaceMap.keySet();
		for (String sanboxTable : sanboxTables) {
			String prodTable = replaceMap.get(sanboxTable);
			newScriptLine = newScriptLine.replace(sanboxTable, prodTable);
		}
		// System.out.println(newScriptLine);
		return newScriptLine;
	}
}
