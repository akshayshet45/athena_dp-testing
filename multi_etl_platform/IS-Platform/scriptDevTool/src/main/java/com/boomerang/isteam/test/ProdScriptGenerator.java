package com.boomerang.isteam.test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.sql.SQLException;

import org.json.simple.parser.ParseException;

import com.boomerang.isteam.exception.DbEndPointNotFoundException;
import com.boomerang.isteam.exception.DbParameterNotFoundException;
import com.boomerang.isteam.exception.JsonKeyNotFoundException;
import com.boomerang.isteam.exception.JsonParseException;
import com.boomerang.isteam.exception.SanBoxSchemaReplacementNotFoundException;

public class ProdScriptGenerator {
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_RESET = "\u001B[0m";
	public static void main(String[] args) throws ClassNotFoundException,
			IOException, ParseException, SQLException, JsonParseException,
			JsonKeyNotFoundException, DbParameterNotFoundException,
			DbEndPointNotFoundException, SanBoxSchemaReplacementNotFoundException {
		String srcScriptPath = args[0];
		srcScriptPath="scripts/dev/"+srcScriptPath;
		/*
		 * Step-1: Schema Replacer
		 */
		String prodDir = "scripts/prod/";
		// String srcScriptPath ="scripts/sample.sql";
		String delimeter = ";";
		SchemaReplacer replacer = new SchemaReplacer();
		replacer.execute(prodDir, srcScriptPath, delimeter);

		/*
		 * Step-2: Prod Simulator
		 */
		ProdSimulator ps = new ProdSimulator();
		String tempDirPath = "scripts/prod/simulation";

		File srcFile = new File(srcScriptPath);
		String fileName = srcFile.getName();

		String scriptPath = prodDir + fileName;
		String transactionBegin = "begin;";
		String transactionEnd = "rollback;";

		Path destScriptPath = ps.insertTransaction(scriptPath, delimeter,
				tempDirPath, transactionBegin, transactionEnd);
		File destScriptFile = destScriptPath.toFile();
		if (destScriptFile.exists()) {
			System.out.println(ANSI_GREEN+"--> Production Script Generated at: "
					+ destScriptPath+ANSI_RESET);
		}
	}

}
