package com.boomerang.isteam.exception;

public class DbEndPointNotFoundException extends Exception {
	public DbEndPointNotFoundException(String s) {
		super(s);
	}
}
