package com.boomerang.isteam.utils;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.InstanceState;
import com.amazonaws.services.ec2.model.Reservation;
import com.boomerang.isteam.exception.PrivateKeyIOException;
import com.boomerang.isteam.exception.RemoteMachineIOException;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;


import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RemoteMachineDeployer {
	private static String HOST = "127.0.0.1";
	private static int PORT = 22;
	private static String USER = "ubuntu";
	private static String PRIVATE_KEY_LOCATION = "jenkins_private.pem";
	private final static Logger logger = LoggerFactory
			.getLogger(RemoteMachineDeployer.class);

	public static void execute(String instance, String zone, String copyFrom,
			String copyTo) throws InterruptedException,
			NoSuchAlgorithmException, JSchException, SftpException, IOException {
		// String copyFrom = "target/scriptDevTool-0.0.1.jar";
		// String copyTo = "/home/ubuntu/";
		RemoteMachineDeployer remotedeployer = new RemoteMachineDeployer();
		String ip = remotedeployer.findIP(instance, zone);
		remotedeployer.copyonRemoteMachine(ip, copyFrom, copyTo);
	}

	public String findIP(String instance, String zone)
			throws InterruptedException {
		AWSCredentials myCredentials = new BasicAWSCredentials(
				String.valueOf("AKIAJTSCDKCLTKZEJYNQ"),
				String.valueOf("frIanJtEt0K9IYKrfdH5vXdiOk+fnYkSSY6sPj/p"));
		AmazonEC2Client ec2Client = new AmazonEC2Client(myCredentials);
		ec2Client.setEndpoint("https://ec2." + zone + ".amazonaws.com");
		List<String> instanceIds = new LinkedList<String>();
		instanceIds.add(instance);
		DescribeInstancesRequest request = new DescribeInstancesRequest();
		request.setInstanceIds(instanceIds);

		DescribeInstancesResult result = null;
		boolean done = false;
		int count = 1;
		while (!done) {
			try {
				result = ec2Client.describeInstances(request);
				done = true;
			} catch (AmazonServiceException e) {
				count++;
				if ((count < 30)
						&& (e.getMessage().contains("Request limit exceeded"))) {
					Thread.sleep(200 * count);
				} else {
					throw e;
				}
			}
		}
		List<Reservation> reservations = result.getReservations();

		InstanceState instanceState = new InstanceState();
		instanceState.setCode(Integer.valueOf(80));
		for (Reservation res : reservations) {
			List<Instance> instances = res.getInstances();
			Iterator localIterator2 = instances.iterator();
			if (localIterator2.hasNext()) {
				Instance ins = (Instance) localIterator2.next();
				String ip = ins.getPublicIpAddress();
				logger.info("Remote Machine IPAddress : " + ip);
				return ip;
			}
		}
		return null;
	}

	public void copyonRemoteMachine(String ip, String copyFrom, String copyTo)
			throws IOException, NoSuchAlgorithmException, JSchException,RemoteMachineIOException {

		// String copyFrom = "target/scriptDevTool-0.0.1.jar";
		// String copyTo = "/home/ubuntu/";
		HOST = ip;

		JSch jSch = new JSch();
		final byte[] privateKey = getPrivateKeyAsByteStream();
		// final byte[] password = "Windows121".getBytes();

		KeyFactory f = KeyFactory.getInstance("RSA");
		// System.out.println(f.getProvider().getName());
		try {
			jSch.addIdentity(USER, privateKey, null, new byte[0]);
		} catch (JSchException e1) {
			logger.error("Not able to Authenticate USER: " + USER
					+ "\r\n" + e1);
			throw new JSchException("Not able to Authenticate USER: " + USER
					+ "\r\n" + e1);
		}

		Session session;
		Channel channel = null;
		try {
			session = jSch.getSession(USER, HOST, PORT);

			Properties config = new Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect(5000);
			channel = session.openChannel("sftp");
		} catch (JSchException e1) {
			logger.error("Not able to create session using USER:" + USER
					+ "\r\n" + e1);
			throw new JSchException("Not able to create session using USER:" + USER
					+ "\r\n" + e1);
		}
		ChannelSftp sftp = (ChannelSftp) channel;
		try {
			sftp.connect();
		} catch (JSchException e1) {
			logger.error("Not able to create connection using USER:" + USER
					+ "\r\n" + e1);
			throw new JSchException("Not able to create connection using USER:" + USER
					+ "\r\n" + e1);
		}
		try {

			String createdir = copyTo;

			if (copyTo.endsWith("/")) {
				createdir = createdir.substring(0, createdir.length() - 1);
			}

			String[] folders = createdir.split("/");
			if (copyTo.startsWith("/")) {
				sftp.cd("/");
			}
			for (String folder : folders) {
				if (folder.length() > 0) {
					try {
						sftp.cd(folder);
					} catch (SftpException e) {
						sftp.mkdir(folder);
						sftp.cd(folder);
					}
				}
			}
			sftp.put(copyFrom, copyTo);

		} catch (SftpException e) {
			logger.error("Not able to copy on Remote Machine from "+copyFrom+ " to "+copyTo
					+ "\r\n" + e);
			throw new RemoteMachineIOException("Not able to copy on Remote Machine from "+copyFrom+ " to "+copyTo
					+ "\r\n" + e);
		}

		sftp.disconnect();
		session.disconnect();

	}

	private static byte[] getPrivateKeyAsByteStream() throws IOException {
		

		InputStream is = RemoteMachineDeployer.class.getClassLoader()
				.getResourceAsStream(PRIVATE_KEY_LOCATION);

		byte[] bytes = IOUtils.toByteArray(is);
		try {
			is.close();
		} catch (IOException e) {
			logger.error("Can not get PrivateKey As ByteStream from Path : "
					+ PRIVATE_KEY_LOCATION + " \r\n" + e);
			throw new PrivateKeyIOException(
					"Can not get PrivateKey As ByteStream from Path : "
							+ PRIVATE_KEY_LOCATION + " \r\n" + e);
		}
		return bytes;

	}
}




