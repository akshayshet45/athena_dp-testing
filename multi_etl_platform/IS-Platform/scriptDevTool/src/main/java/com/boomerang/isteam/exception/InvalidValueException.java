package com.boomerang.isteam.exception;

public class InvalidValueException extends Exception {

	public InvalidValueException(String s) {
		super(s);
	}
}
