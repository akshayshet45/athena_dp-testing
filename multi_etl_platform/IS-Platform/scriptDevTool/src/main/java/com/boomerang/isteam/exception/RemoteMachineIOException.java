package com.boomerang.isteam.exception;

import java.io.IOException;

public class RemoteMachineIOException extends IOException {
	public RemoteMachineIOException(String s) {
		super(s);
	}
}
