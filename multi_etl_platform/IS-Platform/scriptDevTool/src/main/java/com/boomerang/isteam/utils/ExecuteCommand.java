package com.boomerang.isteam.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ProcessBuilder.Redirect;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExecuteCommand {

	public String executeCommands(String shellLocation, String cmdFilePath,
			String arguments) throws IOException, InterruptedException {

		// String executionType = null;
		ProcessBuilder pb = null;
		String[] listArguments = arguments.split(" ");
		if (System.getProperty("os.name").toLowerCase().contains("windows")) {
			File tempScript = new File(cmdFilePath);
			pb = new ProcessBuilder("cmd", "/c", tempScript.toString(),
					listArguments[0], listArguments[1], listArguments[2],
					listArguments[3], listArguments[4]).redirectOutput(
					Redirect.PIPE).redirectErrorStream(true);

		} else {
			File tempScript = new File(shellLocation);
			pb = new ProcessBuilder("bash", tempScript.toString(),
					listArguments[0], listArguments[1], listArguments[2],
					listArguments[3], listArguments[4]).redirectOutput(
					Redirect.PIPE).redirectErrorStream(true);
		}

		Process process = pb.start();
		LogRedirector logScriptOutput = new LogRedirector(
				process.getInputStream());
		logScriptOutput.start();
		logScriptOutput.join();
		process.waitFor();
		StringBuffer output = new StringBuffer(logScriptOutput.getOutput());
		process.waitFor();
		if (process.exitValue() != 0) {
			output.append("ERROR");
		} else if (process.exitValue() == 0) {
			output = new StringBuffer(output.toString().replaceAll("ERROR", ""));
		}
		return output.toString();
	}

	public String executeCommand(String command) throws Exception {

		Process p;
		p = Runtime.getRuntime().exec(command);
		LogRedirector logScriptOutput = new LogRedirector(p.getInputStream());
		logScriptOutput.start();
		logScriptOutput.join();
		p.waitFor();
		StringBuffer output = new StringBuffer(logScriptOutput.getOutput());
		if (p.exitValue() != 0) {
			output.append("ERROR");
		} else if (p.exitValue() == 0) {
			output = new StringBuffer(output.toString().replaceAll("ERROR", ""));
		}
		return output.toString();
	}

	public String executeAttributeCommands(List<String> command)
			throws InterruptedException, IOException {
		ProcessBuilder pb = new ProcessBuilder(command)
				.redirectErrorStream(true);
		Process process = pb.start();
		LogRedirector logScriptOutput = new LogRedirector(
				process.getInputStream());
		logScriptOutput.start();
		logScriptOutput.join();
		StringBuffer output = new StringBuffer(logScriptOutput.getOutput());
		process.waitFor();
		if (process.exitValue() != 0) {
			output.append("ERROR");
		} else if (process.exitValue() == 0) {
			output = new StringBuffer(output.toString().replaceAll("ERROR", ""));
		}
		return output.toString();
	}

	public static void main(String[] args) throws Exception {

	}
}

class LogRedirector extends Thread {
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_RESET = "\u001B[0m";
	private InputStream inputStream;
	private StringBuilder output = new StringBuilder();
	private static Logger logger = LoggerFactory.getLogger(LogRedirector.class);

	public LogRedirector(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public String getOutput() {
		return output.toString();
	}

	public void run() {
		InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		String line = null;
		try {
			while ((line = bufferedReader.readLine()) != null) {
				if (line.contains("ERROR:")) {
					// logger.warn(line);
					System.out.println(ANSI_RED + "\n" + line + "\n"
							+ ANSI_RESET);
				} else {
					// logger.info(line);
					if ((!line.contains("PGCLIENTENCODING"))
							&& (!line.contains("PGPASSWORD"))
							&& (!line.contains("psql"))) {
						System.out.print(line+" ");

						if (line.contains(";")
								|| line.toLowerCase().contains("time")) {
							System.out.println("");
						}

						if (StringUtils.isAllUpperCase(StringUtils
								.deleteWhitespace(line))) {
							System.out.println("\n");
						}
					}
				}
				output.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
