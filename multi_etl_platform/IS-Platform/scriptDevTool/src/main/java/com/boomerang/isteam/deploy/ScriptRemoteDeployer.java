package com.boomerang.isteam.deploy;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boomerang.isteam.exception.DeploymentArtifactNotFoundException;
import com.boomerang.isteam.exception.JsonParseException;
import com.boomerang.isteam.exception.RemoteMachineEndPointNotFoundException;
import com.boomerang.isteam.utils.GenericSpecReader;
import com.boomerang.isteam.utils.RemoteMachineDeployer;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

public class ScriptRemoteDeployer {
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_RESET = "\u001B[0m";
	private final static Logger logger = LoggerFactory
			.getLogger(ScriptRemoteDeployer.class);

	public static void main(String[] args) throws IOException, ParseException,
			NoSuchAlgorithmException, InterruptedException, JSchException,
			SftpException, JsonParseException,
			DeploymentArtifactNotFoundException {
		String env;
		if (args.length > 0) {
			env = args[0];
		} else {
			env = null;
		}
		String srcDir = "target/";
		 String filePrefix = "scriptDevTool";
		//String filePrefix = "ScriptLite";
		GenericSpecReader specReader=new GenericSpecReader(); 
		JSONObject client_spec=specReader.get_client_spec(env);
		String copyTo = "/home/ubuntu/ScriptLite/temp";
		if(client_spec.containsKey("projectName")){
			String projName=((String) client_spec.get("projectName")).trim();
			copyTo=copyTo+"/"+projName;
		}
		
		ScriptRemoteDeployer scriptDeployer = new ScriptRemoteDeployer();
		String copyFrom = scriptDeployer.getcopyFrom(srcDir, filePrefix);
		if (copyFrom != null && (!copyFrom.trim().isEmpty())) {
			scriptDeployer.execute(copyFrom, copyTo, env);
			logger.info("Deployed Artifact: " + copyFrom);
			System.out.println(ANSI_GREEN+"Deployed Artifact From : "+copyFrom+ ", To : "+copyTo+" "+ANSI_RESET);
		} else {
			logger.error("Please run 'Mvn Clean Install' to generate artifact for deployment, "
					+ filePrefix
					+ ".--.--.--.jar file not found at Dir: "
					+ srcDir);
			throw new DeploymentArtifactNotFoundException(
					"Please run 'Mvn Clean Install' to generate artifact for deployment, "
							+ filePrefix
							+ ".--.--.--.jar file not found at Dir: " + srcDir);
		}

	}

	private String getcopyFrom(String srcDir, String filePrefix) {
		final String filePrefix1 = filePrefix;
		final File folder = new File(srcDir);
		final File[] files = folder.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(final File dir, final String name) {
				return name.matches(filePrefix1
						+ "-[0-9]+\\.[0-9]+\\.[0-9]+\\.jar");
			}
		});
		for (File file : files) {
			return file.getPath();
		}
		return null;

	}

	public void execute(String copyFrom, String copyTo, String env)
			throws InterruptedException, JSchException, JsonParseException,
			IOException, ParseException, NoSuchAlgorithmException,
			SftpException {

		// String copyFrom = "target/scriptDevTool-0.0.1.jar";

		JSONObject client_spec = null;
		String remoteMachine = null;
		String remoteMachineEndpoint = null;
		GenericSpecReader specReader=new GenericSpecReader();
		if (env != null) {
			client_spec = specReader.get_client_spec(env);
		} else {
			client_spec = specReader.get_client_spec("dev");
		}

		if (client_spec.containsKey("remoteMachine")) {
			remoteMachine = (String) client_spec.get("remoteMachine");
		} else {
			logger.error("remoteMachine" + " is not found  in " + env
					+ ".property file. Please provide the Parameter value");
			throw new RemoteMachineEndPointNotFoundException(
					"remoteMachine"
							+ " is not found  in "
							+ "<your_environment>.property file. Please provide the Parameter value");
		}
		if (client_spec.containsKey("remoteMachineEndpoint")) {
			remoteMachineEndpoint = (String) client_spec
					.get("remoteMachineEndpoint");
		} else {
			logger.error("remoteMachineEndpoint" + " is not found  in " + env
					+ ".property file. Please provide the Parameter value");
			throw new RemoteMachineEndPointNotFoundException(
					 "remoteMachineEndpoint"
							+ " is not found  in "
							+"<your_environment>.property file. Please provide the Parameter value");
		}

		if (remoteMachine != null && remoteMachineEndpoint != null) {
			try {
				RemoteMachineDeployer.execute(remoteMachine,
						remoteMachineEndpoint, copyFrom, copyTo);
			} catch (NoSuchAlgorithmException | SftpException | IOException e) {
				logger.error("Can not Deploy files from :" + copyFrom + " to: "
						+ copyTo + "on remote machine");
				throw e;
			}
		} else {
			logger.error("remoteMachine"
					+ " or "
					+ "remoteMachineEndpoint"
					+ " are not found  in "
					+ env
					+ ".property file/<environment>.property file. Please provide the Parameter value");
			throw new RemoteMachineEndPointNotFoundException(
					"remoteMachine"
							+ " or "
							+ "remoteMachineEndpoint"
							+ " are not found  in "
							+ env
							+ ".property file/<your_environment>.property file. Please provide the Parameter value");
		}

	}

}
