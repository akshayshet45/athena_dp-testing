package com.boomerang.isteam.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boomerang.isteam.common.javabeans.DbSpec;

/**
 * SqlHelper Class is to provide the basic interface for executing Sqls.
 *
 * @author Vishal Prakash Narain Srivastava
 * @version 1.0
 * @since 2015-10-12
 */
public class SqlHelper {
	private final Logger logger = LoggerFactory.getLogger(SqlHelper.class);
	public static final String TYPE_REDSHIFT = "redshift";
	public static final String TYPE_MYSQL = "mysql";
	private static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
	private static final String POSTGRE_DRIVER = "com.amazon.redshift.jdbc41.Driver";

	private DbSpec spec;
	private String DB_DRIVER;
	private String DB_URL = "jdbc:{{TYPE}}://{{HOST}}/{{DATABASE}}";

	private Connection connection = null;

	public Connection getConnection() {
		return connection;
	}

	private Statement stmt = null;

	public SqlHelper(DbSpec spec, String type) throws ClassNotFoundException,
			SQLException {
		this.spec = spec;
		switch (type) {
		case TYPE_REDSHIFT:
			DB_DRIVER = POSTGRE_DRIVER;
			DB_URL = DB_URL.replace("{{TYPE}}", "redshift")
					.replace("{{HOST}}", (this.spec.getHost()) + ":5439")
					.replace("{{DATABASE}}", this.spec.getDb());
			break;
		case TYPE_MYSQL:
			DB_DRIVER = MYSQL_DRIVER;
			DB_URL = DB_URL.replace("{{TYPE}}", "mysql")
					.replace("{{HOST}}", this.spec.getHost() + ":3306")
					.replace("{{DATABASE}}", this.spec.getDb());
			break;
		}
		Class.forName(DB_DRIVER);

		logger.info("Connecting to database...");
		try {
			connection = DriverManager.getConnection(DB_URL,
					this.spec.getUname(), this.spec.getPasswd());
		} catch (SQLException e) {
			logger.error("Not able to get DB Connection for DB URL: " + DB_URL
					+ " , User:" + this.spec.getUname() + " \r\n " + e);
			throw new SQLException("Not able to get DB Connection for DB URL: "
					+ DB_URL + " , User:" + this.spec.getUname() + " \r\n " + e);
		}

	}

	public int executeUpdate(String sql) throws SQLException {
		int flag = -1;
		try {
			stmt = connection.createStatement();
			flag = stmt.executeUpdate(sql);
		} catch (SQLException e) {
			logger.error(
					"Error while exeuting SqlHelper.executeUpdate at SQLException",
					e);
			throw new SQLException(
					"Exception while exeuting SqlHelper.executeUpdate \r\n "
							+ e);
		} finally {
			try {
				close();
			} catch (SQLException e) {
				logger.error(
						"Error while closing stmt and conn SqlHelper.executeQuery at Exception",
						e);
			}
		}
		return flag;
	}

	public boolean executeQuery(String sql) throws SQLException {
		boolean flag = false;
		try {
			stmt = connection.createStatement();
			flag = stmt.execute(sql);
		} catch (SQLException e) {
			logger.error(
					"Error while exeuting SqlHelper.executeQuery at SQLException",
					e);
			throw new SQLException(
					"Exception while exeuting SqlHelper.executeQuery  \r\n "
							+ e);
		} finally {
			try {
				close();
			} catch (SQLException e) {
				logger.error(
						"Error while closing stmt and conn SqlHelper.executeQuery at Exception",
						e);
			}
		}
		return flag;
	}

	public ResultSet getSelectData(String sql) throws SQLException {
		ResultSet resultSet = null;
		try {
			stmt = connection.createStatement();
			resultSet = stmt.executeQuery(sql);
		} catch (SQLException e) {
			logger.error("SQLException while exeuting SqlHelper.getSelectData  \r\n "
					+ e);
			throw new SQLException(
					"SQLException while exeuting SqlHelper.getSelectData  \r\n "
							+ e);
		}
		return resultSet;
	}

	public ResultSet getSelectData(String sql, int rowCount)
			throws SQLException {
		ResultSet resultSet = null;
		try {
			stmt = connection.createStatement();
			stmt.setMaxRows(rowCount);
			resultSet = stmt.executeQuery(sql);
		} catch (SQLException e) {
			logger.error("SQLException while exeuting SqlHelper.getSelectData  \r\n "
							+ e);
			throw new SQLException(
					"SQLException while exeuting SqlHelper.getSelectData  \r\n "
							+ e);
		} finally {
			try {
				close();
			} catch (SQLException e) {
				logger.error(
						"Error while closing stmt and conn SqlHelper.getSelectData at Exception",
						e);
			}
		}
		return resultSet;
	}

	public void close() throws SQLException {
		if (!stmt.isClosed())
			stmt.close();
		if (!connection.isClosed())
			connection.close();
	}
}
