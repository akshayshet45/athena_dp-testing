package com.boomerang.isteam.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boomerang.isteam.exception.JsonKeyNotFoundException;
import com.boomerang.isteam.exception.JsonParseException;

public class GenericSpecReader {

	public static void main(String[] args) {

		/*
		 * JSONObject client_spec = GenericSpecReader.get_client_spec(); String
		 * envSpecPath = "env_configs/dev.properties"; Properties prop =
		 * getPropertySpec(envSpecPath); String jsonModified =
		 * RegexUtility.applyFlowParams( client_spec.toJSONString(), prop);
		 * System.out.println(jsonModified);
		 */
		// JSONObject obj= get_client_spec();
		// System.out.println(obj.toJSONString());
		// System.out.println(getWhiteListSchema());
	}

	private static final String CLIENT_SPEC_PATH = "client-spec.json";
	private static final String ENV_SPECS_LIST = "env_configs/dev.properties,env_configs/prod.properties,env_configs/common.properties";
	private static Logger logger = LoggerFactory
			.getLogger(GenericSpecReader.class);

	public static String getCLIENT_SPEC_PATH() {
		return CLIENT_SPEC_PATH;
	}

	/*
	 * public JSONObject get_client_spec() throws IOException,
	 * JsonParseException, ParseException {
	 * 
	 * JSONObject jsonObject = generic_spec_provider(CLIENT_SPEC_PATH); String[]
	 * envFilePaths = splitstring(ENV_SPECS_LIST, ","); for (String envFilePath
	 * : envFilePaths) { try { jsonObject = replaceProperties(jsonObject,
	 * envFilePath.trim()); } catch (JsonParseException e) {
	 * logger.error("Can Not get Client_spec JSONObject, using " + envFilePath +
	 * "credential \r\n" + e); throw new JsonParseException(
	 * "Can Not get Client_spec JSONObject, using " + envFilePath +
	 * "credential \r\n" + e); } } return jsonObject;
	 * 
	 * }
	 */
	public JSONObject get_client_spec(String env) throws IOException,
			ParseException, JsonParseException {
		String commonEnvSpec = "env_configs/common.properties";
		String envSpec = "env_configs/" + env + ".properties";
		envSpec = envSpec + "," + commonEnvSpec;
		String globleSpec = "../common.properties";
		File f = new File(globleSpec);
		if (f.exists()) {
			envSpec = envSpec + "," + globleSpec;
		}

		JSONObject jsonObject = generic_spec_provider(CLIENT_SPEC_PATH);
		String[] envFilePaths = splitstring(envSpec, ",");
		for (String envFilePath : envFilePaths) {
			try {
				jsonObject = replaceProperties(jsonObject, envFilePath.trim());
			} catch (JsonParseException e) {
				logger.error("Can Not get Client_spec JSONObject, using "
						+ envFilePath + "credential \r\n" + e);
				throw new JsonParseException(
						"Can Not get Client_spec JSONObject, using "
								+ envFilePath + "credential \r\n" + e);

			}
		}
		return jsonObject;

	}

	public HashMap<String, JSONObject> getEndpointSpecMap(JSONObject clientSpec)
			throws IOException, ParseException, JsonParseException {
		HashMap<String, JSONObject> endpointMap = new HashMap<String, JSONObject>();

		JSONArray endpointsarray = (JSONArray) clientSpec.get("endpoints");
		int n = 0;
		while (n < endpointsarray.size()) {
			JSONObject endpoint = (JSONObject) endpointsarray.get(n);
			endpointMap.put((String) endpoint.get("name"), endpoint);
			n++;
		}

		return endpointMap;

	}

	/*
	 * public static String getSanboxCredential() throws IOException,
	 * ParseException, JsonParseException, JsonKeyNotFoundException {
	 * 
	 * JSONObject client_spec = get_client_spec(); if
	 * (client_spec.containsKey("sanboxCredential")) { return ((String)
	 * client_spec.get("sanboxCredential")); } else {
	 * logger.error("Key 'sanboxCredential' is not found in client-spec"); throw
	 * new
	 * JsonKeyNotFoundException("Key 'sanboxCredential' is not found in client-spec"
	 * ); } }
	 */
	public String getSanboxSchema(JSONObject client_spec) throws IOException,
			ParseException, JsonParseException, JsonKeyNotFoundException {

		// JSONObject client_spec = get_client_spec("dev");
		if (client_spec.containsKey("sanboxSchema")) {
			return ((String) client_spec.get("sanboxSchema"));
		} else {
			logger.error("Key 'sanboxSchema' is not found in client-spec");
			throw new JsonKeyNotFoundException(
					" Key 'sanboxSchema' is not found in client-spec");
		}
	}

	public String getTempSchema(JSONObject client_spec) throws IOException,
			ParseException, JsonParseException, JsonKeyNotFoundException {

		// JSONObject client_spec = get_client_spec("dev");
		if (client_spec.containsKey("tempSchema")) {
			return ((String) client_spec.get("tempSchema"));
		} else {
			logger.error("Key 'tempSchema' is not found in client-spec");
			throw new JsonKeyNotFoundException(
					" Key 'tempSchema' is not found in client-spec");
		}
	}

	public String getTempDevSchema(JSONObject client_spec) throws IOException,
			ParseException, JsonParseException, JsonKeyNotFoundException {

		// JSONObject client_spec = get_client_spec("dev");
		if (client_spec.containsKey("tempDevSchema")) {
			return ((String) client_spec.get("tempDevSchema"));
		} else {
			logger.error("Key 'tempDevSchema' is not found in client-spec");
			throw new JsonKeyNotFoundException(
					" Key 'tempDevSchema' is not found in client-spec");
		}
	}

	public String getProjectName(JSONObject client_spec) throws IOException,
			ParseException, JsonParseException, JsonKeyNotFoundException {

		// JSONObject client_spec = get_client_spec("dev");
		if (client_spec.containsKey("projectName")) {
			return ((String) client_spec.get("projectName"));
		} else {
			logger.error("Key 'projectName' is not found in client-spec");
			throw new JsonKeyNotFoundException(
					" Key 'projectName' is not found in client-spec");
		}
	}

	public ArrayList<String> getWhiteListSchema(JSONObject client_spec)
			throws IOException, ParseException, JsonParseException {

		// JSONObject client_spec = get_client_spec("dev");
		if (client_spec.containsKey("whiteListSchema")) {
			ArrayList<String> whiteList = new ArrayList<String>();
			String whiteListSchema = ((String) client_spec
					.get("whiteListSchema")).trim();
			if (whiteListSchema.isEmpty() || whiteListSchema.equals("")) {
				return null;
			} else {
				String[] whiteListArray = splitstring(whiteListSchema, ",");
				for (String whiteListElement : whiteListArray) {
					whiteList.add(whiteListElement.trim());
				}
			}
			return whiteList;
		} else {
			logger.info("whiteListSchema is not found in client-spec");
		}
		return null;
	}

	private JSONObject generic_spec_provider(String spec_path)
			throws IOException, ParseException {

		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(new FileReader(spec_path));
		} catch (FileNotFoundException e) {
			logger.error("FileNotFoundException in generic_spec_provider Function, File Name : "
					+ spec_path + "  Exception: " + e.getMessage());
			throw e;
		} catch (IOException e) {
			logger.error("IOException in generic_spec_provider Function, File Name : "
					+ spec_path + "  Exception: " + e.getMessage());
			throw e;
		} catch (ParseException e) {
			logger.error("ParseException in generic_spec_provider Function, File Name : "
					+ spec_path + "  Exception: " + e.getMessage());
			throw e;
		}
		JSONObject jsonObject = (JSONObject) obj;
		return jsonObject;
	}

	public Properties getPropertySpec(String filePath) {
		File file = new File(filePath);
		FileInputStream fileInput = null;
		try {
			fileInput = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Properties prop = new Properties();
		try {
			prop.load(fileInput);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop;
	}

	private JSONObject replaceProperties(JSONObject client_spec,
			String envSpecPath) throws JsonParseException {
		// String envSpecPath = "env_configs/dev.properties";
		Properties prop = getPropertySpec(envSpecPath);
		String jsonModified = RegexUtility.applyFlowParams(
				client_spec.toJSONString(), prop);
		// System.out.println(jsonModified);
		JSONParser parser = new JSONParser();
		JSONObject jsonSpec = null;
		try {
			jsonSpec = (JSONObject) parser.parse(jsonModified);
		} catch (ParseException e) {
			throw new JsonParseException("Can Not Parse Json String:"
					+ jsonModified + "\r\n" + e);
		}
		return jsonSpec;
	}

	private String[] splitstring(String str, String delimeter) {
		String[] list = str.split(delimeter);
		return list;

	}

}
