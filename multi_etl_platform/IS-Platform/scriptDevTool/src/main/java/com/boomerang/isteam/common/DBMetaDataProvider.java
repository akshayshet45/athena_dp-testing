package com.boomerang.isteam.common;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import com.boomerang.isteam.common.javabeans.DbSpec;
import com.boomerang.isteam.exception.DbEndPointNotFoundException;
import com.boomerang.isteam.exception.DbParameterNotFoundException;
import com.boomerang.isteam.exception.JsonKeyNotFoundException;
import com.boomerang.isteam.exception.JsonParseException;
import com.boomerang.isteam.utils.CommonUtility;
import com.boomerang.isteam.utils.GenericSpecReader;
import com.boomerang.isteam.utils.SqlHelper;

public class DBMetaDataProvider {

	private static final String SCHEMA = "SELECT n.nspname AS \"Name\","
			+ " pg_catalog.pg_get_userbyid(n.nspowner) AS \"Owner\","
			+ " pg_catalog.array_to_string(n.nspacl, '\n') AS \"Access privileges\","
			+ " pg_catalog.obj_description(n.oid, 'pg_namespace') AS \"Description\""
			+ " FROM pg_catalog.pg_namespace n"
			+ " WHERE n.nspname !~ '^pg_' AND n.nspname <> 'information_schema'"
			+ " ORDER BY 1";
	private static String TABLEOWNER = "select schemaname,tablename,tableowner from pg_tables where schemaname=";

	public static void main(String[] args) {
		/*
		 * HashMap<String, String> tableMap=getTableOwnerMap();
		 * System.out.println(tableMap);
		 */
	}

	public static ArrayList<String> getSchemaList()
			throws ClassNotFoundException, IOException, ParseException,
			SQLException, DbParameterNotFoundException,
			DbEndPointNotFoundException, JsonParseException,
			JsonKeyNotFoundException {
		SqlHelper sqlHelper = getConnectionDB();
		ResultSet rs = sqlHelper.getSelectData(SCHEMA);
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		ArrayList<String> schemaList = new ArrayList<String>();
		while (rs.next()) {

			String columnValue = rs.getString("name").trim();
			schemaList.add(columnValue);
		}
		return schemaList;
	}

	public static HashMap<String, String> getTempTableOwnerMap()
			throws SQLException, ClassNotFoundException, IOException,
			ParseException, DbParameterNotFoundException,
			DbEndPointNotFoundException, JsonParseException,
			JsonKeyNotFoundException {
		String schemaName = "";
		String tableName = "";
		String tableOwner = "";
		String name = "";
		SqlHelper sqlHelper = getConnectionDB();
		HashMap<String, String> tableMap = new HashMap<String, String>();

		GenericSpecReader specReader = new GenericSpecReader();
		JSONObject client_spec = specReader.get_client_spec("dev");

		String TempSchemaName = "";
		// System.out.println("schemaList :" + schemaList);
		if (client_spec.containsKey("tempSchema")) {
			TempSchemaName = (String) client_spec.get("tempSchema");
		}
		// System.out.println("SchemaName"+SchemaName);
		String tableOwnerCommand = TABLEOWNER + "'" + TempSchemaName + "'";
		// System.out.println("TABLEOWNER : " + tableOwnerCommand);
		ResultSet rs = sqlHelper.getSelectData(tableOwnerCommand);
		while (rs.next()) {
			schemaName = rs.getString("schemaname").trim();
			tableName = rs.getString("tablename").trim();
			tableOwner = rs.getString("tableowner").trim();
			name = schemaName + "." + tableName;
			tableMap.put(name, tableOwner);
		}

		return tableMap;
	}

	public static HashMap<String, String> getTableOwnerMap()
			throws SQLException, ClassNotFoundException, IOException,
			ParseException, DbParameterNotFoundException,
			DbEndPointNotFoundException, JsonParseException,
			JsonKeyNotFoundException {
		String schemaName = "";
		String tableName = "";
		String tableOwner = "";
		String name = "";
		SqlHelper sqlHelper = getConnectionDB();
		HashMap<String, String> tableMap = new HashMap<String, String>();

		GenericSpecReader specReader = new GenericSpecReader();
		JSONObject client_spec = specReader.get_client_spec("dev");
		ArrayList<String> whiteListSchema = specReader
				.getWhiteListSchema(client_spec);

		ArrayList<String> schemaList = new ArrayList<String>();
		if (whiteListSchema != null) {
			System.out.println("whiteListSchema : " + whiteListSchema);
			schemaList.addAll(whiteListSchema);
		} else {
			ArrayList<String> schemaListTemp = getSchemaList();
			schemaList.addAll(schemaListTemp);
		}
		// System.out.println("schemaList :" + schemaList);
		for (String SchemaName : schemaList) {
			// System.out.println("SchemaName"+SchemaName);
			String tableOwnerCommand = TABLEOWNER + "'" + SchemaName + "'";
			// System.out.println("TABLEOWNER : " + tableOwnerCommand);
			ResultSet rs = sqlHelper.getSelectData(tableOwnerCommand);
			while (rs.next()) {
				schemaName = rs.getString("schemaname").trim();
				tableName = rs.getString("tablename").trim();
				tableOwner = rs.getString("tableowner").trim();
				name = schemaName + "." + tableName;
				tableMap.put(name, tableOwner);
			}
		}
		return tableMap;
	}

	public static SqlHelper getConnectionDB() throws IOException,
			ParseException, DbParameterNotFoundException,
			DbEndPointNotFoundException, JsonParseException,
			JsonKeyNotFoundException, ClassNotFoundException, SQLException {

		String type = "redshift";
		DbSpec db = getSanboxDbJavaBean();
		SqlHelper sqlHelper = new SqlHelper(db, type);
		return sqlHelper;
	}

	public static SqlHelper getConnectionDB(String type, DbSpec db)
			throws IOException, ParseException, SQLException,
			ClassNotFoundException {
		SqlHelper sqlHelper = new SqlHelper(db, type);
		return sqlHelper;
	}

	public static DbSpec getSanboxDbJavaBean() throws IOException,
			ParseException, DbParameterNotFoundException,
			DbEndPointNotFoundException, JsonParseException,
			JsonKeyNotFoundException {
		CommonUtility utility = new CommonUtility();
		DbSpec db = utility.getDbBeans("env_redshift", "dev");
		return db;
	}
}
