package com.boomerang.isteam.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boomerang.isteam.common.javabeans.DbSpec;
import com.boomerang.isteam.exception.DbEndPointNotFoundException;
import com.boomerang.isteam.exception.DbParameterNotFoundException;
import com.boomerang.isteam.exception.JsonParseException;

public class CommonUtility {
	private static Logger logger = LoggerFactory.getLogger(CommonUtility.class);

	public ArrayList<String> readFile(String path, String delimeter)
			throws IOException {
		BufferedReader in;
		ArrayList<String> scriptLineList = new ArrayList<String>();
		try {
			in = new BufferedReader(new FileReader(path));
			String scriptLine = "";
			while ((scriptLine = in.readLine()) != null) {
				if (scriptLine.trim().equalsIgnoreCase(delimeter.trim())) {
					scriptLineList.add(scriptLine);
				} else {
					if (scriptLine.trim().contains(delimeter)) {
						String[] templist = scriptLine.trim().split(delimeter);
						int i=0;
						for (i = 0; i < templist.length-1; i++) {
							String line = templist[i];
							line = line.trim() + delimeter;
							scriptLineList.add(line);
						}
						if (scriptLine.trim().endsWith(delimeter)) {
							String line=templist[i].trim()+delimeter;
							scriptLineList.add(line);
						}else{
							scriptLineList.add(templist[i]);
						}
						

						/*
						 * for (String line : templist) { line = line.trim() +
						 * delimeter; scriptLineList.add(line);
						 */

						// System.out.println(line);

					} else {
						scriptLineList.add(scriptLine);
					}
				}

			}
			in.close();
			//System.out.println(scriptLineList);
		} catch (FileNotFoundException e1) {
			logger.error("FileNotFound at " + path + ", Exception : \r\n" + e1);
			throw new FileNotFoundException("FileNotFound at " + path
					+ ", Exception : \r\n" + e1);
		} catch (IOException e) {
			logger.error("IOException occur during reading file from " + path
					+ ", Exception : \r\n" + e);
			throw new IOException("IOException occur during reading file from "
					+ path + ", Exception : \r\n" + e);
		}
		return scriptLineList;
	}

	public DbSpec getDbBeans(String dbEndPointName, String env)
			throws IOException, ParseException, DbParameterNotFoundException,
			DbEndPointNotFoundException, JsonParseException {

		DbSpec dbObject = null;
		GenericSpecReader specReader = new GenericSpecReader();
		JSONObject client_spec = specReader.get_client_spec(env);
		Map<String, JSONObject> endpointMap = specReader
				.getEndpointSpecMap(client_spec);
		JSONObject endpointdata;
		String errorMsg = "";
		if (endpointMap.containsKey(dbEndPointName)) {
			endpointdata = endpointMap.get(dbEndPointName);

			dbObject = new DbSpec();
			JSONObject properties = (JSONObject) endpointdata.get("properties");
			if (properties.containsKey("host")) {
				String host = (String) properties.get("host");
				if (host != null && (!host.trim().equalsIgnoreCase(""))) {
					dbObject.setHost((String) properties.get("host"));
				} else {
					errorMsg = errorMsg + "value of " + "'host'"
							+ " is null for endpoint:  " + dbEndPointName;
				}
			} else {
				errorMsg = errorMsg + " 'host'"
						+ " is not provided for endpoint:  " + dbEndPointName;
			}
			if (properties.containsKey("user")) {
				String user = (String) properties.get("user");
				if (user != null && (!user.trim().equalsIgnoreCase(""))) {
					dbObject.setUname((String) properties.get("user"));
				} else {
					errorMsg = errorMsg + " ,value of " + "'user'"
							+ " is null for endpoint:  " + dbEndPointName;
				}
			} else {
				errorMsg = errorMsg + " ,'user'" + " is not provided in "
						+ dbEndPointName;
			}
			if (properties.containsKey("pass")) {
				String pass = (String) properties.get("pass");
				if (pass != null && (!pass.trim().equalsIgnoreCase(""))) {
					dbObject.setPasswd((String) properties.get("pass"));
				} else {
					errorMsg = errorMsg + " ,value of " + "'pass'"
							+ " is null for endpoint:  " + dbEndPointName;
				}
			} else {
				errorMsg = errorMsg + " ,'pass'"
						+ " is not provided for endpoint: " + dbEndPointName;
			}
			if (properties.containsKey("db")) {
				String db = (String) properties.get("db");
				if (db != null && (!db.trim().equalsIgnoreCase(""))) {
					dbObject.setDb((String) properties.get("db"));
				} else {
					errorMsg = errorMsg + " ,value of " + "'db'"
							+ " is null for endpoint:  " + dbEndPointName;
				}
			} else {
				errorMsg = errorMsg + " ,'db'"
						+ " is not provided for endpoint: " + dbEndPointName;
			}
			if (errorMsg != null && (!errorMsg.trim().equalsIgnoreCase(""))) {
				logger.error("Error:" + errorMsg);
				throw new DbParameterNotFoundException("Error:" + errorMsg);
			}

		} else {
			logger.error(dbEndPointName + " is not found at "
					+ GenericSpecReader.getCLIENT_SPEC_PATH());
			throw new DbEndPointNotFoundException(dbEndPointName
					+ " is not found at "
					+ GenericSpecReader.getCLIENT_SPEC_PATH());
		}
		return dbObject;
	}

	public boolean createFolder(String path) {

		File tmpdir = new File(path);
		boolean existflag = tmpdir.exists();
		if (!existflag) {
			existflag = tmpdir.mkdirs();
		}
		return existflag;
	}

	public Path createEmptyFile(String srcFilePath, String destPathString)
			throws IOException {

		File srcFile = new File(srcFilePath);
		String fileName = srcFile.getName();
		if (srcFile.isDirectory()) {
			logger.error("Please provide Script Path with File Name "
					+ "your input is:" + srcFilePath);
			throw new IOException("Please provide Script Path with File Name "
					+ "your input is:" + srcFilePath);
		}
		if (!(srcFile.isFile() && srcFile.exists())) {
			logger.error("File not Exist at" + srcFilePath);
			throw new IOException("File not Exist at" + srcFilePath);
		}

		Path copyPath = FileSystems.getDefault().getPath(destPathString,
				fileName);
		// System.out.println("copyPath "+copyPath.toString()+"  "+copyPath.getParent().toString());
		createFolder(copyPath.getParent().toString());
		Path path = null;
		try {
			File destFile = new File(copyPath.toString());
			if (destFile.isFile() && destFile.exists()) {
				destFile.delete();
			}

			path = Files.createFile(copyPath);
		} catch (IOException e) {
			logger.error("Error while creating file at " + copyPath.toString());
			throw new IOException("Error while creating file at "
					+ copyPath.toString() + "\r\n" + e);
		}
		return path;

	}

}
