--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = home_depot, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = true;

--
-- Name: ai_scraping_input; Type: TABLE; Schema: home_depot; Owner: hdmaster; Tablespace: 
--

CREATE TABLE base.ai_input (
    competitor character varying(600),
    subcategories character varying(600),
   search_term character varying(600),
   category_link_for_scraping_skus character varying(600),
   count integer,
   scraping_skus_google_link character varying(600),
   matching character varying(600),
   validation character varying(600),
   tags character varying(500)
   feed_date timestamp without time zone NOT NULL,
   creation_date timestamp without time zone NOT NULL
  );


ALTER TABLE ai_scraping_input OWNER TO hdmaster;

--
-- Name: ai_scraping_input_competitor_key; Type: CONSTRAINT; Schema: home_depot; Owner: hdmaster; Tablespace: 
--

ALTER TABLE ONLY ai_scraping_input
    ADD CONSTRAINT ai_scraping_input_competitor_key UNIQUE (competitor, subcategories);


--
-- Name: ai_scraping_input; Type: ACL; Schema: home_depot; Owner: hdmaster
--

REVOKE ALL ON TABLE ai_scraping_input FROM PUBLIC;
REVOKE ALL ON TABLE ai_scraping_input FROM hdmaster;
GRANT ALL ON TABLE ai_scraping_input TO hdmaster;
GRANT SELECT ON TABLE ai_scraping_input TO GROUP boomerang_users_read_only;
GRANT ALL ON TABLE ai_scraping_input TO GROUP application_write_only;
GRANT SELECT ON TABLE ai_scraping_input TO GROUP application_read_only;


--
-- PostgreSQL database dump complete
--

