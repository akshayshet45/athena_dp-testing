#!/bin/bash
set -e
set -x

echo "Printing from the prescript"
echo "Creating the connection to the CLI from prescript"

export SNOWSQL_PWD="Testuser123"
sf_connection="bin/snowsql  -a boomerangcommercedev -u test_write_only -w DEMO_WH -d ETL_INTEGRATION "

$sf_connection -q "select count(*) from base.dd_parameters"
$sf_connection -q "drop table if exists public.created_from_cli"
$sf_connection -q "create table if not exists public.created_from_cli as select * from base.dd_parameters where 1=2"


echo "Successfully executed the prescript"
