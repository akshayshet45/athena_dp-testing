#!/bin/bash

set -e

project_name="akshay_kohls"
project_schedule="04/04/2016,5.30.AM.PDT,1d"
project_flow_to_be_schedule="end-to-end-flows_kohls"
flow_parameters="key1=value1,key2=value2"
success_emails="akshay@boomerangcommerce.com"
failure_emails="akshay@boomerangcommerce.com"

#==============================================================================>

# internal parameters
project_zip_file_path="/tmp/${project_name}/${project_name}.zip"

# deploy project on azkaban 
python /home/ubuntu/azk_deployment_scripts/deploy_azkaban_project.py $project_name $project_zip_file_path $project_schedule $project_flow_to_be_schedule $flow_parameters $success_emails $failure_emails  

sudo rm $project_zip_file_path

echo "$project_name application deployed and scheduled successfully"
