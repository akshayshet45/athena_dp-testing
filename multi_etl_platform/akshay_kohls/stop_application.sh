#!/bin/bash

set -e

project_name="akshay_kohls"
project_root_dir="/tmp/$project_name"
sudo rm -r $project_root_dir
mkdir -p $project_root_dir

echo "$project_name application clean job was successfully finished">/tmp/kohlsOCDTest.log
