drop table if exists client.blocked_item;

create table if not exists client.blocked_item ( sku             character varying(500),       
 division        character varying(500)       ,
 blocked_reason  character varying(500)       ,
 feed_date       timestamp without time zone  not null,
 creation_date   timestamp without time zone ,
 format_id character varying(500));
 
 