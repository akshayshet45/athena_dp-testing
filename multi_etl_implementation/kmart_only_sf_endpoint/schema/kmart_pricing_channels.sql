\set ON_ERROR_STOP 1

drop table if exists kmart.latest_catalog_channel_v2_temp1;
create table kmart.latest_catalog_channel_v2_temp1 as
select k.*, case when m.merch_l1_id is not null then 'Major' else 'Minor' end as category_desc
	from
	kmart.full_load_boomerang_catalog k
	left join
	kmart.major_categories m
	on k.merch_l1_id = m.merch_l1_id
	and k.merch_l2_id = m.merch_l2_id
	and k.merch_l4_id = m.merch_l4_id
where k.feed_date = (select max(feed_date)
	from kmart.full_load_boomerang_catalog);

-- Get latest TC data
drop table if exists kmart.latest_tc;
create table kmart.latest_tc as
select * from kmart.test_control where feed_date = (select max(feed_date) from kmart.test_control);

truncate table kmart.latest_catalog_channel_v2;
insert into kmart.latest_catalog_channel_v2
select c.*, t.channel_id, t.tc, t.cluster
from
kmart.latest_catalog_channel_v2_temp1 c
inner join kmart.latest_tc t
on c.merch_l1_id = t.merch_l1_id;

-- Offer flex
truncate table kmart.temp_v_latest_offer_v2;
insert into kmart.temp_v_latest_offer_v2
	select * from kmart.req_load_offer_flexibility
	where --feed_date = (select max(feed_date) from kmart.full_offer_flexibility	) and
	offer_start_date_time <= current_date - extract(weekday from current_date) + 7 
	and offer_end_date_time >= current_date - extract(weekday from current_date) + 13
	and is_offshore = 'No';

delete from kmart.temp_v_latest_offer_v2 where sku in
	(select distinct sku from kmart.temp_v_latest_offer_v2 where
		(is_pictured = 'Y' or shc_channel in (1,14)) and is_offshore = 'No');

-- Get latest WOS file
truncate table kmart.latest_wos;
insert into kmart.latest_wos
	select * from kmart.weeks_of_supply where feed_date = greatest((select max(feed_date) from kmart.weeks_of_supply), current_date - 7);

-- Get latest TPR file
truncate table kmart.latest_tpr;
insert into kmart.latest_tpr
	select * from kmart.tpr_list where feed_date = (select max(feed_date) from kmart.tpr_list);

-- Find sales in last week
truncate table kmart.wk_item_sales_v2;
Insert into  kmart.wk_item_sales_v2
	select merch_l1_id, merch_l2_id, merch_l4_id, p.sku, 
	category_desc, p.channel_id, sum(gross_units) as sales
	from base.performance_order p
	inner join kmart.latest_catalog_channel_v2 k
	on p.sku = k.sku
	and p.channel_id = k.channel_id
	and p.transaction_date between current_date - extract(weekday from current_date) - 7 and current_date - extract(weekday from current_date) - 1
group by 1,2,3,4,5,6;



-- Find expected sales in last week

drop table if exists kmart.sales_all_week_temp;
create table kmart.sales_all_week_temp as select sku, sum(sales) as itm_sales
		from kmart.wk_item_sales_v2
		group by 1;

drop table if exists kmart.sales_major_cat_temp;
create table kmart.sales_major_cat_temp as 
	select merch_l1_id, merch_l2_id, merch_l4_id, channel_id, sum(sales) as str_sales
		from kmart.wk_item_sales_v2
		where category_desc = 'Major'
		group by 1,2,3,4;


drop table if exists kmart.sales_minor_cat_temp;
create table kmart.sales_minor_cat_temp as
	select merch_l1_id, channel_id, sum(sales) as str_sales
		from kmart.wk_item_sales_v2
		where category_desc = 'Minor'
		group by 1,2;


drop table if exists kmart.sales_major_all_store_temp;
create table kmart.sales_major_all_store_temp as
	select merch_l1_id, merch_l2_id, merch_l4_id, sum(sales) as cat_sales
		from kmart.wk_item_sales_v2
		where category_desc = 'Major'
		group by 1,2,3;

drop table if exists kmart.sales_minor_all_store_temp;
create table kmart.sales_minor_all_store_temp as
	select merch_l1_id, sum(sales) as cat_sales
		from kmart.wk_item_sales_v2
		where category_desc = 'Minor'
		group by 1;



truncate table kmart.sales_velocity_v2;
insert into kmart.sales_velocity_v2
	select w.merch_l1_id, w.sku, w.channel_id,
	case when coalesce(mjr.cat_sales,mnr.cat_sales,0) = 0 then 0 else
		coalesce(itm_sales,0) * coalesce(mjrst.str_sales,mnrst.str_sales)/coalesce(mjr.cat_sales,mnr.cat_sales)
	end as sales_velocity
	from
	kmart.latest_catalog_channel_v2 w
	left join -- Sales of item in all stores
	kmart.sales_all_week_temp itmsls
	on w.sku = itmsls.sku
	left join -- Sales in major categories in each store
	kmart.sales_major_cat_temp mjrst
	on w.merch_l1_id = mjrst.merch_l1_id
	and w.merch_l2_id = mjrst.merch_l2_id
	and w.merch_l4_id = mjrst.merch_l4_id
	and w.channel_id = mjrst.channel_id
	left join -- Sales in all minor categories in each store
	kmart.sales_minor_cat_temp mnrst
	on w.merch_l1_id = mnrst.merch_l1_id
	and w.channel_id = mnrst.channel_id
	left join -- Sales in major categories in all stores
	kmart.sales_major_all_store_temp mjr
	on w.merch_l1_id = mjr.merch_l1_id
	and w.merch_l2_id = mjr.merch_l2_id
	and w.merch_l4_id = mjr.merch_l4_id
	left join -- Sales in all minor categories in all stores
	kmart.sales_minor_all_store_temp mnr
	on w.merch_l1_id = mnr.merch_l1_id;

-- Find sell through for WOS items

drop table if exists kmart.s_through_temp;
create table kmart.s_through_temp as
	select k.merch_l1_id, k.sku, k.channel_id, sum(qty) as qty
	from kmart.latest_catalog_channel_v2 k
	inner join kmart.temp_v_latest_inventory i
	on k.sku = i.sku
	and k.channel_id = i.channel_id
	group by 1,2,3
	having sum(qty) > 0;

truncate table kmart.sell_through_v2;
insert into kmart.sell_through_v2
	select v.merch_l1_id, v.sku, v.channel_id,
	case when coalesce(sales_velocity,0) = 0 then 0 else qty/sales_velocity end as sell_through -- should we be setting this to 0, check with Sam
	from kmart.sales_velocity_v2 v
	right join
	kmart.s_through_temp i
	on v.sku = i.sku
	and v.merch_l1_id = i.merch_l1_id
	and v.channel_id = i.channel_id
 --inner join kmart.weeks_of_supply w
 --on i.shc_item_id = w.item_id
 ;

-- Get ST buckets

drop table if exists kmart.s_through_pct_temp;
create table kmart.s_through_pct_temp as
	select sku, channel_id, percent_rank() over (partition by sku order by sell_through) as pct
	from kmart.sell_through_v2;

truncate table kmart.sell_through_pct_v2;
insert into kmart.sell_through_pct_v2
select c.sku, c.channel_id, pct, case when pct < 0.33 then 1 when pct < 0.66 then 2 else 3 end as st_bucket
from
	kmart.latest_catalog_channel_v2 c
	left join
	kmart.s_through_pct_temp st
	on c.sku = st.sku
	and c.channel_id = st.channel_id;


-- Create pricing channels
-- 10000+ are regular pricing clusters
-- 20000+ are regular control clusters
-- 30000+ are WOS clusters
-- 40000+ are WOS test and control clusters
truncate table kmart.latest_pricing_groups_v2;
	
	-- All items not in WOS
	insert into kmart.latest_pricing_groups_v2
		select distinct case when tc = 'T' then 10000+cluster else 20000+cluster end, 
		channel_id, sku, current_date
		from kmart.latest_catalog_channel_v2 k
		where sku not in
		(select distinct sku from kmart.latest_wos)
		and sku not in
		(select distinct sku from kmart.latest_tpr);

	-- All items where price has to be same
	insert into kmart.latest_pricing_groups_v2
		select distinct 30000, channel_id, sku, current_date
		from kmart.latest_catalog_channel_v2 k
		where sku in
		(select distinct sku from kmart.latest_wos where price_code = 'S');
	
	-- All items where only test is priced
	insert into kmart.latest_pricing_groups_v2
		select distinct 30001, channel_id, sku, current_date
		from kmart.latest_catalog_channel_v2 k
		where sku in
		(select distinct sku from kmart.latest_wos where price_code = 'T')
		and tc = 'T';

	-- All items where only control is priced
	insert into kmart.latest_pricing_groups_v2
		select distinct 30002, channel_id, sku, current_date
		from kmart.latest_catalog_channel_v2 k
		where sku in
		(select distinct sku from kmart.latest_wos where price_code = 'C')
		and tc = 'C';

	-- All items where test is priced higher
	insert into kmart.latest_pricing_groups_v2
		select distinct case when tc = 'T' then 30003 else 30004 end, channel_id, sku, current_date
		from kmart.latest_catalog_channel_v2 k
		where sku in
		(select distinct sku from kmart.latest_wos where price_code = 'H');

	-- All items where test is priced higher
	insert into kmart.latest_pricing_groups_v2
		select distinct case when tc = 'C' then 30005 else 30006 end, channel_id, sku, current_date
		from kmart.latest_catalog_channel_v2 k
		where sku in
		(select distinct sku from kmart.latest_wos where price_code = 'L');

	-- All items where price is customized to store

	drop table if exists kmart.lpg_price_customized_store_temp;
	create table kmart.lpg_price_customized_store_temp as
		select distinct sku, channel_id from kmart.latest_catalog_channel_v2
		where sku in
		(select distinct sku from kmart.latest_wos where price_code = 'V');

	insert into kmart.latest_pricing_groups_v2
		select distinct 
		case when st_bucket = 3 then 30009
		when st_bucket = 2 then 30008
		else 30007 end, k.channel_id, k.sku, current_date
		from kmart.sell_through_pct_v2 p
		right join
		kmart.lpg_price_customized_store_temp k
		on p.sku = k.sku
		and p.channel_id = k.channel_id;

	-- All items which have to be priced by cluster
	insert into kmart.latest_pricing_groups_v2
		select distinct 40000+cluster, channel_id, sku, current_date
		from kmart.latest_catalog_channel_v2 k
		where sku in
		(select distinct sku from kmart.latest_wos where price_code = 'G');

	-- All items which are TPR
	insert into kmart.latest_pricing_groups_v2
		select distinct 
		case when tc = 'T' then 50000+cluster else 60000+cluster end, 
		channel_id, sku, current_date
		from kmart.latest_catalog_channel_v2 k
		where sku in
		(select distinct sku from kmart.latest_tpr);

-- Create cost/price table


truncate table kmart.latest_cost_price_v2;
insert into kmart.latest_cost_price_v2
	select distinct c.sku, c.channel_id, po_cost, reg_price,
	min(case when dl_typ_cd = 'PCPT' then dl_val else reg_price*(1-dl_val/100) end) as offer_price,
	max(reg_price*(1 - floor_min_discount/100)) as floor_min_discount, min(reg_price*(1 - ceiling_max_discount/100)) as ceiling_max_discount,
	'N' as price_code from kmart.temp_v_latest_cost c 
	inner join
	kmart.temp_v_latest_offer_v2 p
        on c.sku = p.sku
        and c.channel_id = p.channel_id
        and p.dl_typ_cd in ('PCPT','PCPO')
        and p.shc_channel in (1,14,35)
        and shc_channel = 35
group by 1,2,3,4;
	
-- Insert TPR
insert into kmart.latest_cost_price_v2
	select sku, cs2.channel_id, po_cost, reg_price,
	offer_price, floor_min_discount, ceiling_max_discount, price_code
	from
	(select merch_l1_id, cs.*
	from
	(select sku, channel_id, po_cost, min(reg_price) as reg_price,
	min(reg_price) as offer_price, NULL as floor_min_discount, NULL as ceiling_max_discount,
	'R' as price_code from kmart.temp_v_latest_cost
	where sku in (select distinct sku from kmart.latest_tpr)
	group by 1,2,3) cs
	inner join kmart.latest_catalog_channel_v2_temp1 c
	on cs.sku = c.sku) cs2
	inner join
	kmart.latest_tc tc
	on cs2.channel_id = tc.channel_id
	and cs2.merch_l1_id = tc.merch_l1_id;

-- Remove WOS in promo
delete from kmart.latest_cost_price_v2 where sku in (select distinct sku from kmart.latest_wos);

-- Add WOS from promo
insert into kmart.latest_cost_price_v2
	select w.sku, c.channel_id, po_cost, reg_price, reg_price, NULL, NULL, price_code
	from kmart.temp_v_latest_cost c 
	inner join
	kmart.latest_wos w
    on c.sku = w.sku
    inner join kmart.latest_catalog_channel_v2 cc
    on c.sku = cc.sku
    and c.channel_id = cc.channel_id;


-- Add cost list
drop table if exists kmart.l_pricing_channels_t1;
create table kmart.l_pricing_channels_t1 as
	select distinct sku, po_cost, offer_price, floor_min_discount, ceiling_max_discount, price_code, reg_price,
			dense_rank() over (partition by sku order by po_cost, offer_price, floor_min_discount, price_code, reg_price) cost_num
			from kmart.latest_cost_price_v2;

drop table if exists kmart.l_pricing_channels_temp2;
create table kmart.l_pricing_channels_temp2 as
	select cs.sku, cs.po_cost, cs.offer_price, cs.floor_min_discount, cs.ceiling_max_discount, 
		cs.price_code, channel_id, cs.reg_price, cost_num
		from
		kmart.latest_cost_price_v2 cs
			inner join
		kmart.l_pricing_channels_t1 cs_n
		on cs.sku = cs_n.sku
		and cs.po_cost = cs_n.po_cost
		and cs.offer_price = cs_n.offer_price
		and cs.reg_price = cs_n.reg_price
		and coalesce(cs.floor_min_discount,0) = coalesce(cs_n.floor_min_discount,0)
		and coalesce(cs.ceiling_max_discount,0) = coalesce(cs_n.ceiling_max_discount,0)
		and cs.price_code = cs_n.price_code;

truncate table kmart.latest_pricing_channels;
insert into kmart.latest_pricing_channels
	select pricing_channel_id*1000+cost_num as pricing_channel_id, g.store_num, g.sku, c.po_cost, 
	c.offer_price, reg_price, floor_min_discount, ceiling_max_discount, price_code, current_date as created
	from kmart.latest_pricing_groups_v2 g
	inner join
	kmart.l_pricing_channels_temp2 c
on g.sku = c.sku
and g.store_num = c.channel_id;

-- Insert into pricing channels
delete from kmart.pricing_store_group where created = current_date;

-- Add pricing channels
insert into kmart.pricing_store_group
select * from kmart.latest_pricing_channels;