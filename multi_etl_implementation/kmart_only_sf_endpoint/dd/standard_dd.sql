\set ON_ERROR_STOP 1
\timing on

drop table if exists temp.boomerang_catalog;
create table temp.boomerang_catalog as(
    select distinct a.sku, title, channel_id, upc, merch_l1_name, merch_l1_id, merch_l2_name, merch_l2_id, merch_l3_name,
    merch_l3_id, merch_l4_name,merch_l4_id, merch_l5_name,merch_l5_id, brand,is_marketplace,is_privatelabel,
    null as size, ean, isbn, manufacturer, a.line, gl_l1_name,gl_l1_id, gl_l2_name,gl_l2_id, gl_l3_name, gl_l3_id,
    gl_l4_name,gl_l4_id, gl_l5_name,gl_l5_id, product_url, creation_date, feed_date
    from base.boomerang_catalog a where (a.feed_date) = ((select max(feed_date) from base.boomerang_catalog))
);

drop table if exists temp.item_prices;
create table temp.item_prices as (
  select distinct a.sku, channel_id, map_price, list_price, reg_price, offer_price, msrp, on_map, break_point_price1, break_point_qty1, break_point_price2, break_point_qty2, break_point_price3, break_point_qty3, break_point_price4, break_point_qty4, break_point_price5, break_point_qty5 from base.item_prices a
  where (a.effective_date) = ((select max(effective_date) from base.item_prices))
);

drop table if exists temp.item_cost;
create table temp.item_cost as(
    select distinct a.sku,po_cost,shipping_cost, variable_cost, channel_id, vendor_subsidized_po_cost from base.item_cost a
    where (a.effective_date) = ((select max(effective_date) from base.item_cost))
);

drop table if exists temp.item_cost_yesterday;
create table temp.item_cost_yesterday as(
    select distinct a.sku,po_cost,shipping_cost, variable_cost, channel_id, vendor_subsidized_po_cost from base.item_cost a
    where (a.effective_date) = (dateadd(day, -1, (select max(effective_date) from base.item_cost)))
);

drop table if exists temp.promotions;
create table temp.promotions as (
    select distinct p.sku, p.channel_id, case
     when CURRENT_DATE > promo_start_date and CURRENT_DATE < promo_end_date then 1
     else  0
     end as  on_promo,
     p.promo_start_date as next_promo_date,
     case
     when p.promo_start_date is null then null
     else p.promo_start_date::date - CURRENT_DATE::date
     end as days_till_next_promo
     from base.promotions p where feed_date = (select max(feed_date) from base.promotions) and promo_start_date = (select min(promo_start_date) from base.promotions where base.promotions.sku = p.sku and base.promotions.channel_id = p.channel_id)
);

drop table if exists temp.comp1;
create table temp.comp1 as(
    select client_sku as sku, comp_sku, comp_name, comp_name as comp_website, comp_offer_price as sas_offer_price, comp_shipping_price as sas_shipping_charge, comp_url as product_page_url, comp_in_stock::integer as unkn_in_stock, comp_scrape_date as scrape_date from cm.pricing_adjusted_cm where comp_name = (select value from base.dd_parameters where element = 'comp1' and created = (select max(created) from base.dd_parameters where element = 'comp1') and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm cpac)
));

drop table if exists temp.comp2;
create table temp.comp2 as(
    select client_sku as sku, comp_sku, comp_name, comp_name as comp_website, comp_offer_price as sas_offer_price, comp_shipping_price as sas_shipping_charge, comp_url as product_page_url, comp_in_stock::integer as unkn_in_stock, comp_scrape_date as scrape_date from cm.pricing_adjusted_cm where comp_name = (select value from base.dd_parameters where element = 'comp2' and created = (select max(created) from base.dd_parameters where element = 'comp2') and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm cpac)
));

drop table if exists temp.comp3;
create table temp.comp3 as(
    select client_sku as sku, comp_sku, comp_name, comp_name as comp_website, comp_offer_price as sas_offer_price, comp_shipping_price as sas_shipping_charge, comp_url as product_page_url, comp_in_stock::integer as unkn_in_stock, comp_scrape_date as scrape_date from cm.pricing_adjusted_cm where comp_name = (select value from base.dd_parameters where element = 'comp3' and created = (select max(created) from base.dd_parameters where element = 'comp3') and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm cpac)
));

drop table if exists temp.comp4;
create table temp.comp4 as(
    select client_sku as sku, comp_sku, comp_name, comp_name as comp_website, comp_offer_price as sas_offer_price, comp_shipping_price as sas_shipping_charge, comp_url as product_page_url, comp_in_stock::integer as unkn_in_stock, comp_scrape_date as scrape_date from cm.pricing_adjusted_cm where comp_name = (select value from base.dd_parameters where element = 'comp4' and created = (select max(created) from base.dd_parameters where element = 'comp4') and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm cpac)
));

drop table if exists temp.comp5;
create table temp.comp5 as(
    select client_sku as sku, comp_sku, comp_name, comp_name as comp_website, comp_offer_price as sas_offer_price, comp_shipping_price as sas_shipping_charge, comp_url as product_page_url, comp_in_stock::integer as unkn_in_stock, comp_scrape_date as scrape_date from cm.pricing_adjusted_cm where comp_name = (select value from base.dd_parameters where element = 'comp5' and created = (select max(created) from base.dd_parameters where element = 'comp5') and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm cpac)
));


drop table if exists temp.competition;
create table temp.competition as(
select distinct b.sku,
    tc1.comp_sku as comp1_sku, tc1.comp_name as comp1_name, tc1.comp_website as comp1_website, tc1.sas_offer_price as comp1_price, tc1.sas_shipping_charge as comp1_shipping_charge, tc1.product_page_url as comp1_product_page_url, tc1.unkn_in_stock as comp1_in_stock,
    tc2.comp_sku as comp2_sku, tc2.comp_name as comp2_name, tc2.comp_website as comp2_website, tc2.sas_offer_price as comp2_price, tc2.sas_shipping_charge as comp2_shipping_charge, tc2.product_page_url as comp2_product_page_url, tc2.unkn_in_stock as comp2_in_stock,
    tc3.comp_sku as comp3_sku, tc3.comp_name as comp3_name, tc3.comp_website as comp3_website, tc3.sas_offer_price as comp3_price, tc3.sas_shipping_charge as comp3_shipping_charge, tc3.product_page_url as comp3_product_page_url, tc3.unkn_in_stock as comp3_in_stock,
    tc4.comp_sku as comp4_sku, tc4.comp_name as comp4_name, tc4.comp_website as comp4_website, tc4.sas_offer_price as comp4_price, tc4.sas_shipping_charge as comp4_shipping_charge, tc4.product_page_url as comp4_product_page_url, tc4.unkn_in_stock as comp4_in_stock,
    tc5.comp_sku as comp5_sku, tc5.comp_name as comp5_name, tc5.comp_website as comp5_website, tc5.sas_offer_price as comp5_price, tc5.sas_shipping_charge as comp5_shipping_charge, tc5.product_page_url as comp5_product_page_url, tc5.unkn_in_stock as comp5_in_stock
    from (select (max(bc.creation_date)) max_date, bc.sku from temp.boomerang_catalog bc group by bc.sku) b
    left join temp.comp1 tc1 on b.sku=tc1.sku and tc1.scrape_date >= (CURRENT_DATE - (select value from base.dd_parameters where element = 'comp_validity' and created = (select max(created) from base.dd_parameters where element = 'comp_validity'))::INTERVAL)
    left join temp.comp2 tc2 on b.sku=tc2.sku and tc2.scrape_date >= (CURRENT_DATE - (select value from base.dd_parameters where element = 'comp_validity' and created = (select max(created) from base.dd_parameters where element = 'comp_validity'))::INTERVAL)
    left join temp.comp3 tc3 on b.sku=tc3.sku and tc3.scrape_date >= (CURRENT_DATE - (select value from base.dd_parameters where element = 'comp_validity' and created = (select max(created) from base.dd_parameters where element = 'comp_validity'))::INTERVAL)
    left join temp.comp4 tc4 on b.sku=tc4.sku and tc4.scrape_date >= (CURRENT_DATE - (select value from base.dd_parameters where element = 'comp_validity' and created = (select max(created) from base.dd_parameters where element = 'comp_validity'))::INTERVAL)
    left join temp.comp5 tc5 on b.sku=tc5.sku and tc5.scrape_date >= (CURRENT_DATE - (select value from base.dd_parameters where element = 'comp_validity' and created = (select max(created) from base.dd_parameters where element = 'comp_validity'))::INTERVAL)
);

drop table if exists temp.last_suggested_price;
create table temp.last_suggested_price as (
    select sku, channel_id, price_pre_suggestion as last_pre_price_suggestion_price,  case
    when bpch.price_suggestion_date is null then null
    else CURRENT_DATE::date - bpch.price_suggestion_date::date
    end as days_since_last_price_suggestion, suggested_price as last_suggested_price  from base.price_change_history bpch where bpch.price_suggestion_date = (select max(price_suggestion_date) from base.price_change_history where base.price_change_history.sku = bpch.sku and base.price_change_history.channel_id = bpch.channel_id)
);

drop table if exists temp.todays_price;
create table temp.todays_price as (select * from base.item_prices where effective_date = (select max(effective_date) from base.item_prices));

drop table if exists temp.last_price;
create table temp.last_price as (
    select bip.sku, bip.channel_id, bip.offer_price as last_price, CURRENT_DATE::date - bip.feed_date::date as days_since_last_price_change from base.item_prices bip, temp.todays_price bip2 where bip.sku = bip2.sku and bip.channel_id = bip2.channel_id and bip.effective_date < bip2.effective_date and bip.effective_date = (select max(effective_date) from base.item_prices bip3 where bip3.sku = bip2.sku and bip3.channel_id = bip2.channel_id and bip3.offer_price != bip2.offer_price)
);

drop table if exists temp.conversion;
create table temp.conversion as (
    select temp.boomerang_catalog.sku, temp.boomerang_catalog.channel_id,
    case
    when sum(page_views) = 0 or sum(page_views) is null then null
    else sum(gross_units)/sum(page_views)
    end as conversion_rate_in_x_days from temp.boomerang_catalog, base.performance_order where temp.boomerang_catalog.sku = base.performance_order.sku and temp.boomerang_catalog.channel_id = base.performance_order.channel_id and base.performance_order.transaction_date >= CURRENT_DATE - (select value from base.dd_parameters where element = 'conv_num_days' and created = (select max(created) from base.dd_parameters where element = 'conv_num_days'))::INTERVAL group by temp.boomerang_catalog.sku, temp.boomerang_catalog.channel_id
);

drop table if exists temp.gross_margin;
create table temp.gross_margin as (
    select temp.boomerang_catalog.sku, temp.boomerang_catalog.channel_id, sum(gross_revenue) - sum(gross_cost) as gross_margin_in_x_days from temp.boomerang_catalog, base.performance_order where temp.boomerang_catalog.sku = base.performance_order.sku and temp.boomerang_catalog.channel_id = base.performance_order.channel_id and base.performance_order.transaction_date >= CURRENT_DATE - (select value from base.dd_parameters where element = 'gross_margin_num_day' and created = (select max(created) from base.dd_parameters where element = 'gross_margin_num_day'))::INTERVAL group by temp.boomerang_catalog.sku, temp.boomerang_catalog.channel_id
);

drop table if exists temp.gross_revenue;
create table temp.gross_revenue as (
    select temp.boomerang_catalog.sku, temp.boomerang_catalog.channel_id, sum(gross_revenue) as gross_revenue_in_x_days from temp.boomerang_catalog, base.performance_order where temp.boomerang_catalog.sku = base.performance_order.sku and temp.boomerang_catalog.channel_id = base.performance_order.channel_id and base.performance_order.transaction_date >= CURRENT_DATE - (select value from base.dd_parameters where element = 'gross_rev_num_day' and created = (select max(created) from base.dd_parameters where element = 'gross_rev_num_day'))::INTERVAL group by temp.boomerang_catalog.sku, temp.boomerang_catalog.channel_id
);

drop table if exists temp.gross_orders;
create table temp.gross_orders as (
    select temp.boomerang_catalog.sku, temp.boomerang_catalog.channel_id, sum(gross_orders) as gross_orders_in_x_days from temp.boomerang_catalog, base.performance_order where temp.boomerang_catalog.sku = base.performance_order.sku and temp.boomerang_catalog.channel_id = base.performance_order.channel_id and base.performance_order.transaction_date >= CURRENT_DATE - (select value from base.dd_parameters where element = 'gross_orders_num_day' and created = (select max(created) from base.dd_parameters where element = 'gross_orders_num_day'))::INTERVAL group by temp.boomerang_catalog.sku, temp.boomerang_catalog.channel_id
);

drop table if exists temp.page_views;
create table temp.page_views as (
    select temp.boomerang_catalog.sku, temp.boomerang_catalog.channel_id, sum(page_views) as page_views_in_x_days from temp.boomerang_catalog, base.performance_order where temp.boomerang_catalog.sku = base.performance_order.sku  and temp.boomerang_catalog.channel_id = base.performance_order.channel_id and base.performance_order.transaction_date >= CURRENT_DATE - (select value from base.dd_parameters where element = 'page_views_num_day' and created = (select max(created) from base.dd_parameters where element = 'page_views_num_day'))::INTERVAL group by temp.boomerang_catalog.sku, temp.boomerang_catalog.channel_id
);

drop table if exists temp.net_margin;
create table temp.net_margin as (
    select temp.boomerang_catalog.sku, temp.boomerang_catalog.channel_id, sum(net_revenue) - sum(net_cost) as net_margin_in_x_days from temp.boomerang_catalog, base.performance_fulfillment where temp.boomerang_catalog.sku = base.performance_fulfillment.sku and temp.boomerang_catalog.channel_id = base.performance_fulfillment.channel_id and base.performance_fulfillment.transaction_date >= CURRENT_DATE - (select value from base.dd_parameters where element = 'net_margin_num_days' and created = (select max(created) from base.dd_parameters where element = 'net_margin_num_days'))::INTERVAL  group by temp.boomerang_catalog.sku, temp.boomerang_catalog.channel_id
);


drop table if exists temp.gross_margin_mean;
create table temp.gross_margin_mean as (
    select temp.boomerang_catalog.sku, temp.boomerang_catalog.channel_id, (sum(gross_revenue) - sum(gross_cost))/(CURRENT_DATE::date - (CURRENT_DATE - (select value from base.dd_parameters where element = 'margin_mean_num_days' and created = (select max(created) from base.dd_parameters where element = 'margin_mean_num_days'))::INTERVAL)::date) as margin_mean_in_x_days from temp.boomerang_catalog, base.performance_order where temp.boomerang_catalog.sku = base.performance_order.sku and temp.boomerang_catalog.channel_id = base.performance_order.channel_id and base.performance_order.transaction_date >= CURRENT_DATE - (select value from base.dd_parameters where element = 'margin_mean_num_days' and created = (select max(created) from base.dd_parameters where element = 'margin_mean_num_days'))::INTERVAL group by temp.boomerang_catalog.sku, temp.boomerang_catalog.channel_id
);



drop table if exists temp.gross_revenue_mean;
create table temp.gross_revenue_mean as (
    select temp.boomerang_catalog.sku, temp.boomerang_catalog.channel_id, (sum(gross_revenue))/(CURRENT_DATE::date - (CURRENT_DATE - (select value from base.dd_parameters where element = 'rev_mean_num_days' and created = (select max(created) from base.dd_parameters where element = 'rev_mean_num_days'))::INTERVAL)::date) as revenue_mean_in_x_days from temp.boomerang_catalog, base.performance_order where temp.boomerang_catalog.sku = base.performance_order.sku and temp.boomerang_catalog.channel_id = base.performance_order.channel_id and base.performance_order.transaction_date >= CURRENT_DATE - (select value from base.dd_parameters where element = 'rev_mean_num_days' and created = (select max(created) from base.dd_parameters where element = 'rev_mean_num_days'))::INTERVAL group by temp.boomerang_catalog.sku, temp.boomerang_catalog.channel_id
);

drop table if exists temp.gross_revenue_std_dev;
create table temp.gross_revenue_std_dev as (
    select temp.boomerang_catalog.sku, temp.boomerang_catalog.channel_id, stddev(total_gross_revenue) as rev_stddev_in_x_days from temp.boomerang_catalog,
    (select sku, channel_id, sum(gross_revenue) as total_gross_revenue from base.performance_order
     where base.performance_order.transaction_date >= CURRENT_DATE - (select value from base.dd_parameters where element = 'rev_std_dev_num_days' and created = (select max(created) from base.dd_parameters where element = 'rev_std_dev_num_days'))::INTERVAL

       group by  sku, channel_id, extract(WEEK from transaction_date)
   ) as po where temp.boomerang_catalog.sku = po.sku and temp.boomerang_catalog.channel_id = po.channel_id
   group by temp.boomerang_catalog.sku, temp.boomerang_catalog.channel_id
);

drop table if exists temp.gross_margin_std_dev;
create table temp.gross_margin_std_dev as (
    select temp.boomerang_catalog.sku, temp.boomerang_catalog.channel_id, stddev(total_gross_margin) as margin_stddev_in_x_days from temp.boomerang_catalog,
    (select sku, channel_id, sum(gross_revenue) - sum(gross_cost) as total_gross_margin from base.performance_order
     where base.performance_order.transaction_date >= CURRENT_DATE - (select value from base.dd_parameters where element = 'margin_std_dev_num_days' and created = (select max(created) from base.dd_parameters where element = 'margin_std_dev_num_days'))::INTERVAL           group by  sku, channel_id, extract(WEEK from transaction_date)
   ) as po where temp.boomerang_catalog.sku = po.sku and temp.boomerang_catalog.channel_id = po.channel_id
   group by temp.boomerang_catalog.sku, temp.boomerang_catalog.channel_id
);


drop table if exists temp.asp;
create table temp.asp as (
    select temp.boomerang_catalog.sku, temp.boomerang_catalog.channel_id, case
    when sum(gross_units) = 0 then offer_price
    else sum(gross_revenue)/sum(gross_units)
    end as asp_x_days
     from temp.boomerang_catalog
     left join base.performance_order on temp.boomerang_catalog.sku = base.performance_order.sku and temp.boomerang_catalog.channel_id = base.performance_order.channel_id
     left join base.item_prices  on temp.boomerang_catalog.sku = base.item_prices.sku and temp.boomerang_catalog.channel_id = base.item_prices.channel_id
     where base.performance_order.transaction_date >= CURRENT_DATE - (select value from base.dd_parameters where element = 'asp_num_days' and created = (select max(created) from base.dd_parameters where element = 'asp_num_days'))::INTERVAL and
     base.item_prices.effective_date = (select max(effective_date) from base.item_prices)
     group by temp.boomerang_catalog.sku, temp.boomerang_catalog.channel_id, offer_price
);

drop table if exists temp.agg_comp;
create table temp.agg_comp as (
    select sku,
    case
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp1_price,999999) then comp1_price
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp2_price,999999) then comp2_price
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp3_price,999999) then comp3_price
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp4_price,999999) then comp4_price
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp5_price,999999) then comp5_price
    end as min_comp_price,
    case
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp1_price,999999) then comp1_name
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp2_price,999999) then comp2_name
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp3_price,999999) then comp3_name
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp4_price,999999) then comp4_name
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp5_price,999999) then comp5_name
    end as min_comp_name,
    case
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp1_price,999999) then comp1_product_page_url
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp2_price,999999) then comp2_product_page_url
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp3_price,999999) then comp3_product_page_url
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp4_price,999999) then comp4_product_page_url
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp5_price,999999) then comp5_product_page_url
    end as min_comp_url,
    case
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp1_price,999999) then comp1_shipping_charge
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp2_price,999999) then comp2_shipping_charge
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp3_price,999999) then comp3_shipping_charge
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp4_price,999999) then comp4_shipping_charge
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp5_price,999999) then comp5_shipping_charge
    end as min_comp_shipping,
    case
        when greatest(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp1_price,999999) then comp1_price
        when greatest(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp2_price,999999) then comp2_price
        when greatest(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp3_price,999999) then comp3_price
        when greatest(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp4_price,999999) then comp4_price
        when greatest(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) = coalesce(comp5_price,999999) then comp5_price
    end as max_comp_price
from temp.competition
where coalesce(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price) is not null
);

drop table if exists temp.is_child;
create table temp.is_child as(
    select distinct child_sku as sku
    from base.sku_relationships a where a.feed_date = (select max(feed_date) from base.sku_relationships)
);

drop table if exists temp.is_parent;
create table temp.is_parent as(
    select distinct parent_sku as sku
    from base.sku_relationships a where (a.feed_date) = (select max(feed_date) from base.sku_relationships)
);

drop table if exists temp.sku_relation_ships;
create table temp.sku_relation_ships as (
    select bc.sku,
    case when ic.sku is null then FALSE else TRUE end as is_child,
    case when ip.sku is null then FALSE else TRUE end as is_parent
     from (select distinct sku from temp.boomerang_catalog) bc
    left join temp.is_child ic on bc.sku = ic.sku
    left join temp.is_parent ip on bc.sku = ip.sku
);

drop table if exists temp.num_price_changes;
create table temp.num_price_changes as (
    select bc.sku, bc.channel_id, count(*) as num_price_changes_in_x_days from temp.boomerang_catalog bc, base.price_change_history bpch where bc.sku = bpch.sku and bc.channel_id = bpch.channel_id and bpch.price_suggestion_date >= CURRENT_DATE - (select value from base.dd_parameters where element = 'price_chan_num_days' and created = (select max(created) from base.dd_parameters where element = 'price_chan_num_days'))::INTERVAL group by bc.sku, bc.channel_id
);

drop table if exists temp.hct_perf_data;
create table temp.hct_perf_data as (
	select sku, channel_id,
	case when sum(page_views) is null then 0 else sum(page_views) end as agg_page_views,
	case when sum(unique_visits) is null then 0 else sum(unique_visits) end as agg_unique_visits,
    case when sum(gross_revenue) is null then 0 else sum(gross_revenue) end as agg_gross_revenue,
    case when sum(gross_units) is null then 0 else sum(gross_units) end as agg_gross_units,
    case when sum(gross_revenue)-sum(gross_cost) is null then 0 else sum(gross_revenue)-sum(gross_cost) end as agg_gross_margin
	from base.performance_order
	where trunc(transaction_date) >= CURRENT_DATE - (select value from base.dd_parameters where element = 'hct_num_days' and created = (select max(created) from base.dd_parameters where element = 'hct_num_days'))::INTERVAL
	group by sku, channel_id
);

drop table if exists temp.hct_merch_l1_name;
create table temp.hct_merch_l1_name as (
    select tbc.sku, tbc.channel_id,
    ntile(100) over(partition by merch_l1_name order by agg_page_views ) as cat_l1_page_views_tile,
    ntile(100) over(partition by merch_l1_name order by agg_unique_visits ) as cat_l1_unique_visits_tile,
    ntile(100) over(partition by merch_l1_name order by agg_gross_revenue ) as cat_l1_gross_revenue_tile,
    ntile(100) over(partition by merch_l1_name order by agg_gross_units ) as cat_l1_gross_units_tile,
    ntile(100) over(partition by merch_l1_name order by agg_gross_margin ) as cat_l1_gross_margin_tile from temp.boomerang_catalog tbc, temp.hct_perf_data thpd where tbc.sku = thpd.sku and tbc.channel_id = thpd.channel_id
);

drop table if exists temp.hct_std;
create table temp.hct_std as (
    select sku, channel_id,
    case when cat_l1_page_views_tile <= (select value from base.dd_parameters where element = 'hct_tail_limit' and created = (select max(created) from base.dd_parameters where element = 'hct_tail_limit'))::decimal then 'TAIL'
    when cat_l1_page_views_tile > (select value from base.dd_parameters where element = 'hct_tail_limit' and created = (select max(created) from base.dd_parameters where element = 'hct_tail_limit'))::decimal and cat_l1_page_views_tile <= (select value from base.dd_parameters where element = 'hct_head_limit' and created = (select max(created) from base.dd_parameters where element = 'hct_head_limit'))::decimal then 'CORE'
    when cat_l1_page_views_tile > (select value from base.dd_parameters where element = 'hct_head_limit' and created = (select max(created) from base.dd_parameters where element = 'hct_head_limit'))::decimal then 'HEAD' end as hct_page_views,
    case when cat_l1_unique_visits_tile <= (select value from base.dd_parameters where element = 'hct_tail_limit' and created = (select max(created) from base.dd_parameters where element = 'hct_tail_limit'))::decimal then 'TAIL'
    when cat_l1_unique_visits_tile > (select value from base.dd_parameters where element = 'hct_tail_limit' and created = (select max(created) from base.dd_parameters where element = 'hct_tail_limit'))::decimal and cat_l1_unique_visits_tile <= (select value from base.dd_parameters where element = 'hct_head_limit' and created = (select max(created) from base.dd_parameters where element = 'hct_head_limit'))::decimal  then 'CORE'
    when cat_l1_unique_visits_tile > (select value from base.dd_parameters where element = 'hct_head_limit' and created = (select max(created) from base.dd_parameters where element = 'hct_head_limit'))::decimal  then 'HEAD' end as hct_unique_visits,
    case when cat_l1_gross_revenue_tile <= (select value from base.dd_parameters where element = 'hct_tail_limit' and created = (select max(created) from base.dd_parameters where element = 'hct_tail_limit'))::decimal then 'TAIL'
    when cat_l1_gross_revenue_tile > (select value from base.dd_parameters where element = 'hct_tail_limit' and created = (select max(created) from base.dd_parameters where element = 'hct_tail_limit'))::decimal and cat_l1_gross_revenue_tile <= (select value from base.dd_parameters where element = 'hct_head_limit' and created = (select max(created) from base.dd_parameters where element = 'hct_head_limit'))::decimal  then 'CORE'
    when cat_l1_gross_revenue_tile > (select value from base.dd_parameters where element = 'hct_head_limit' and created = (select max(created) from base.dd_parameters where element = 'hct_head_limit'))::decimal  then 'HEAD' end as hct_gross_revenue,
    case when cat_l1_gross_units_tile <= (select value from base.dd_parameters where element = 'hct_tail_limit' and created = (select max(created) from base.dd_parameters where element = 'hct_tail_limit'))::decimal then 'TAIL'
    when cat_l1_gross_units_tile > (select value from base.dd_parameters where element = 'hct_tail_limit' and created = (select max(created) from base.dd_parameters where element = 'hct_tail_limit'))::decimal and cat_l1_gross_units_tile <= (select value from base.dd_parameters where element = 'hct_head_limit' and created = (select max(created) from base.dd_parameters where element = 'hct_head_limit'))::decimal  then 'CORE'
    when cat_l1_gross_units_tile > (select value from base.dd_parameters where element = 'hct_head_limit' and created = (select max(created) from base.dd_parameters where element = 'hct_head_limit'))::decimal  then 'HEAD' end as hct_gross_units,
    case when cat_l1_gross_margin_tile <= (select value from base.dd_parameters where element = 'hct_tail_limit' and created = (select max(created) from base.dd_parameters where element = 'hct_tail_limit'))::decimal then 'TAIL'
    when cat_l1_gross_margin_tile > (select value from base.dd_parameters where element = 'hct_tail_limit' and created = (select max(created) from base.dd_parameters where element = 'hct_tail_limit'))::decimal and cat_l1_gross_margin_tile <= (select value from base.dd_parameters where element = 'hct_head_limit' and created = (select max(created) from base.dd_parameters where element = 'hct_head_limit'))::decimal  then 'CORE'
    when cat_l1_gross_margin_tile > (select value from base.dd_parameters where element = 'hct_head_limit' and created = (select max(created) from base.dd_parameters where element = 'hct_head_limit'))::decimal  then 'HEAD' end as hct_gross_margin
     from temp.hct_merch_l1_name
);

drop table if exists temp.hct;
create table temp.hct as (
     select sku, channel_id, case
     when   (select value from base.dd_parameters where element = 'hct_factor' and created = (select max(created) from base.dd_parameters where element = 'hct_factor')) = 'page_views' then hct_page_views
     when   (select value from base.dd_parameters where element = 'hct_factor' and created = (select max(created) from base.dd_parameters where element = 'hct_factor')) = 'unique_visits' then hct_unique_visits
     when   (select value from base.dd_parameters where element = 'hct_factor' and created = (select max(created) from base.dd_parameters where element = 'hct_factor')) = 'gross_revenue' then hct_gross_revenue
     when   (select value from base.dd_parameters where element = 'hct_factor' and created = (select max(created) from base.dd_parameters where element = 'hct_factor')) = 'gross_units' then hct_gross_units
     when   (select value from base.dd_parameters where element = 'hct_factor' and created = (select max(created) from base.dd_parameters where element = 'hct_factor')) = 'gross_margin' then hct_gross_margin
     end as hct from temp.hct_std
);

drop table if exists base.std_data_dictionary cascade;
create table base.std_data_dictionary as (
  select tbc.sku as sku, tbc.title as title, cast(tbc.channel_id as integer) as channel_id, tbc.upc as upc,tbc.brand as brand,
  tbc.is_marketplace as is_marketplace,tbc.is_privatelabel as is_privatelabel, tbc.size, tbc.ean, tbc.isbn, tbc.manufacturer, tbc.line, tbc.gl_l1_name, tbc.gl_l1_id,
  tbc.gl_l2_name, tbc.gl_l2_id, tbc.gl_l3_name, tbc.gl_l3_id, tbc.gl_l4_name, tbc.gl_l4_id, tbc.gl_l5_name, tbc.gl_l5_id,
  tbc.merch_l1_id , tbc.merch_l1_name, tbc.merch_l2_id , tbc.merch_l2_name, tbc.merch_l3_id , tbc.merch_l3_name,
  tbc.merch_l4_id, tbc.merch_l4_name, tbc.merch_l5_id, tbc.merch_l5_name, tbc.product_url,
  tip.map_price as map_price,tip.list_price as list_price, tip.reg_price as reg_price,
  tip.offer_price as offer_price, tip.offer_price as current_price, tip.msrp as msrp, tip.on_map as on_map, tip.break_point_price1 as break_point_price1 , tip.break_point_qty1 as break_point_qty1,
  tip.break_point_price2 as break_point_price2, tip.break_point_qty2 as break_point_qty2, tip.break_point_price3 as break_point_price3, tip.break_point_qty3 as break_point_qty3,
  tip.break_point_price4 as break_point_price4, tip.break_point_qty4 as break_point_qty4, tip.break_point_price5 as break_point_price5, tip.break_point_qty5 as break_point_qty5,
  tic.po_cost as po_cost, tic.shipping_cost as shipping_cost, tic.variable_cost as variable_cost, tic.vendor_subsidized_po_cost as vendor_subsidized_po_cost,
  ticy.po_cost as po_cost_yesterday, ticy.shipping_cost as shipping_cost_yesterday, ticy.variable_cost as variable_cost_yesterday, ticy.vendor_subsidized_po_cost as vendor_subsidized_po_cost_yesterday,
  NULL as elasticity,
  tc.comp1_sku, tc.comp1_name, tc.comp1_website, tc.comp1_price, tc.comp1_shipping_charge, tc.comp1_product_page_url, tc.comp1_in_stock,
  tc.comp2_sku, tc.comp2_name, tc.comp2_website, tc.comp2_price, tc.comp2_shipping_charge, tc.comp2_product_page_url, tc.comp2_in_stock,
  tc.comp3_sku, tc.comp3_name, tc.comp3_website, tc.comp3_price, tc.comp3_shipping_charge, tc.comp3_product_page_url, tc.comp3_in_stock,
  tc.comp4_sku, tc.comp4_name, tc.comp4_website, tc.comp4_price, tc.comp4_shipping_charge, tc.comp4_product_page_url, tc.comp4_in_stock,
  tc.comp5_sku, tc.comp5_name, tc.comp5_website, tc.comp5_price, tc.comp5_shipping_charge, tc.comp5_product_page_url, tc.comp5_in_stock,
  tac.min_comp_price, tac.min_comp_name, tac.min_comp_url, tac.min_comp_shipping, tac.max_comp_price, tsrs.is_parent, tsrs.is_child,
  NULL as tc, NULL as pre_date_start, NULL as pre_date_end, NULL as post_date_start, NULL as yoy,
  current_date as Date, extract(dayofweek from current_date) as day_of_week,
  case
  when tip.offer_price = 0 or tip.offer_price is null then null
  else (tip.offer_price - tic.po_cost)/tip.offer_price
  end as margin_rate,
  (select max(feed_date) from temp.boomerang_catalog) as dd_date,
  current_date as feed_date,
  tp.on_promo as on_promo, tp.next_promo_date as next_promo_date, tp.days_till_next_promo as days_till_next_promo,
  lp.last_price as last_price, lp.days_since_last_price_change as days_since_last_price_change,
  lsp.last_pre_price_suggestion_price, lsp.days_since_last_price_suggestion, lsp.last_suggested_price,
  conversion_rate_in_x_days, gross_margin_in_x_days,
  net_margin_in_x_days, margin_mean_in_x_days,
  revenue_mean_in_x_days, rev_stddev_in_x_days,
  margin_stddev_in_x_days, gross_revenue_in_x_days, gross_orders_in_x_days, page_views_in_x_days,     num_price_changes_in_x_days, hct, cat_l1_page_views_tile, cat_l1_unique_visits_tile, cat_l1_gross_revenue_tile,
  cat_l1_gross_units_tile, cat_l1_gross_margin_tile, asp_x_days
  from temp.boomerang_catalog tbc
  left join temp.item_prices tip on tbc.sku = tip.sku and tbc.channel_id = tip.channel_id
  left join temp.item_cost tic on tbc.sku = tic.sku and tbc.channel_id = tic.channel_id
  left join temp.promotions tp on tbc.sku = tp.sku and tbc.channel_id = tp.channel_id
  left join temp.item_cost_yesterday ticy on tbc.sku = ticy.sku and tbc.channel_id = ticy.channel_id
  left join temp.competition tc on tbc.sku = tc.sku
  left join temp.last_price lp on tbc.sku = lp.sku and tbc.channel_id = lp.channel_id
  left join temp.last_suggested_price lsp on tbc.sku = lsp.sku and tbc.channel_id = lsp.channel_id
  left join temp.conversion co on tbc.sku = co.sku and tbc.channel_id = co.channel_id
  left join temp.gross_margin gm on tbc.sku = gm.sku and tbc.channel_id = gm.channel_id
  left join temp.gross_revenue gr on tbc.sku = gr.sku and tbc.channel_id = gr.channel_id
  left join temp.gross_orders go on tbc.sku = go.sku and tbc.channel_id = go.channel_id
  left join temp.page_views pv on tbc.sku = pv.sku and tbc.channel_id = pv.channel_id
  left join temp.net_margin nm on tbc.sku = nm.sku and tbc.channel_id = nm.channel_id
  left join temp.gross_margin_mean gmm on tbc.sku = gmm.sku and tbc.channel_id = gmm.channel_id
  left join temp.gross_revenue_mean grm on tbc.sku = grm.sku and tbc.channel_id = grm.channel_id
  left join temp.gross_revenue_std_dev grsd on tbc.sku = grsd.sku and tbc.channel_id = grsd.channel_id
  left join temp.gross_margin_std_dev gmsd on tbc.sku = gmsd.sku and tbc.channel_id = gmsd.channel_id
  left join temp.agg_comp tac on tbc.sku = tac.sku
  left join temp.sku_relation_ships tsrs on tbc.sku = tsrs.sku
  left join temp.num_price_changes tnpc on tbc.sku = tnpc.sku and tbc.channel_id = tnpc.channel_id
  left join temp.hct thct on tbc.sku = thct.sku and tbc.channel_id = thct.channel_id
  left join temp.hct_merch_l1_name thmln on tbc.sku = thmln.sku and tbc.channel_id = thmln.channel_id
  left join temp.asp tasp on tbc.sku = tasp.sku and tbc.channel_id = tasp.channel_id
);
