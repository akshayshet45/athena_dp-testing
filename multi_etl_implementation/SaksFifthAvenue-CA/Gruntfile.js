'use strict';
module.exports = function(grunt){

	// configure tasks
	grunt.initConfig({

		// global properties used across all projects
		clientConfig: grunt.file.readJSON('client-spec.json'),
	});

	// Loading required plugins for Grunt
        require('time-grunt')(grunt);
	require('load-grunt-tasks')(grunt);
	grunt.loadTasks('./../ProjectUtils/is-grunt-tasks');
};
