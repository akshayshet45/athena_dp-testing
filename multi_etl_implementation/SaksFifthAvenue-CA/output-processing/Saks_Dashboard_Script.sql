\set ON_ERROR_STOP 1
\timing on
  
--select feed_date,creation_date from cm.pricing_adjusted_cm_custom group by 1,2 order by 1
DROP TABLE IF EXISTS client.Category_SKU_in_scope;
CREATE TABLE client.Category_SKU_in_scope AS (
    SELECT
      category_label_1,
      sku
    FROM client.custom_hierarchy_catalog
    WHERE feed_date = (SELECT max(feed_date) FROM client.custom_hierarchy_catalog)
    AND category_label_1 IN ('Shoes','Handbags')
    GROUP BY 1,2
);

INSERT INTO client.Category_SKU_in_scope
    SELECT
      category_label_1,
      sku
    FROM client.custom_hierarchy_catalog
    WHERE feed_date = (SELECT max(feed_date) FROM client.custom_hierarchy_catalog)
    AND category_label_1 = 'Women''s Apparel'
    GROUP BY 1,2;

  
drop table if exists temp.item_prices_latest_med_list_price_cm;
CREATE TABLE temp.item_prices_latest_med_list_price_cm AS (
    SELECT
      sku_raw sku,
      list_price,
      count(*)
    FROM (SELECT sku_raw,upc,list_price FROM client.custom_item_prices
          WHERE feed_date = (SELECT max(feed_date) from client.custom_item_prices)
          AND list_price is not null
          GROUP BY 1,2,3) a
    GROUP BY 1,2
);
  
drop table if exists temp.item_prices_latest_med_list_price_rank_cm;
CREATE TABLE temp.item_prices_latest_med_list_price_rank_cm AS (
    SELECT
      sku,
      list_price,
      count,
      row_number() over (partition by sku order by count desc)
    FROM temp.item_prices_latest_med_list_price_cm
);
  
  
drop table if exists temp.cm_pricing_adjusted_latest_pre;
CREATE TABLE temp.cm_pricing_adjusted_latest_pre AS (
    SELECT
      feed_date,
      client_sku,
      comp_sku,
      comp_color,
      comp_size,
      comp_name,
      comp_title,
      comp_brand,
      comp_scrape_date,
      comp_url,
      comp_pdt_rating,
      comp_pdt_reviews,
      comp_in_stock,
      comp_offer_price,
      CASE WHEN comp_list_price is null AND b.list_price is not null THEN b.list_price
           WHEN comp_list_price is null AND b.list_price is null THEN comp_offer_price
           ELSE comp_list_price END comp_list_price
    FROM (SELECT * FROM cm.pricing_adjusted_cm_custom WHERE feed_date = (select max(feed_date) from cm.pricing_adjusted_cm_custom)
             AND substring(client_sku from 1 for 13) IN (select sku from client.Category_SKU_in_scope group by 1)
             AND matching_approval_status = 'APPROVED'
        ) a
    LEFT JOIN (SELECT * FROM temp.item_prices_latest_med_list_price_rank_cm WHERE row_number = 1) b
    ON substring(a.client_sku from 1 for 13) = b.sku
);
  
drop table if exists temp.cm_pricing_adjusted_latest;
CREATE TABLE temp.cm_pricing_adjusted_latest AS (
    SELECT
      feed_date,
      substring(client_sku from 1 for 13) sku,
      comp_sku,
      comp_color,
      comp_size,
      comp_name,
      comp_title,
      comp_brand,
      comp_scrape_date,
      comp_url,
      comp_pdt_rating,
      comp_pdt_reviews,
      comp_in_stock,
      comp_offer_price,
      comp_list_price
    FROM temp.cm_pricing_adjusted_latest_pre
    GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
);
  
--select * from temp.cm_pricing_adjusted_latest where sku = '0400087719065'
--select * from cm.pricing_adjusted_cm_custom where feed_date= (select max(feed_date) from cm.pricing_adjusted_cm_custom) limit 10;

DROP TABLE IF EXISTS temp.comp_stock_flag;
CREATE TABLE temp.comp_stock_flag AS (
    SELECT
      feed_date,
      comp_name,
      comp_sku,
      comp_in_stock
    FROM temp.cm_pricing_adjusted_latest
    GROUP BY 1,2,3,4
);

DROP TABLE IF EXISTS temp.comp_stock_flag_1;
CREATE TABLE temp.comp_stock_flag_1 AS (
    SELECT
      feed_date,
      comp_name,
      comp_sku,
      comp_in_stock,
      CASE WHEN comp_in_stock IS TRUE THEN 1
           WHEN comp_in_stock IS FALSE THEN 2
           WHEN comp_in_stock IS NULL THEN 3
      END comp_stock_rank
    FROM temp.comp_stock_flag
);

DROP TABLE IF EXISTS temp.comp_stock_flag_2;
CREATE TABLE temp.comp_stock_flag_2 AS (
    SELECT
      a.*,
      Row_number() over (partition by feed_date, comp_name,comp_sku order by comp_stock_rank)
    FROM temp.comp_stock_flag_1 a
);

  
drop table if exists temp.cm_pricing_adjusted_latest_1;
CREATE TABLE temp.cm_pricing_adjusted_latest_1 AS (
    SELECT
      feed_date,
      sku,
      comp_sku,
      comp_name,
      max(comp_scrape_date) comp_scrape_date,
      max(comp_pdt_rating) comp_pdt_rating,
      max(comp_pdt_reviews) comp_pdt_reviews,
      min(comp_list_price) min_comp_list_price,
      max(comp_list_price) max_comp_list_price,
      min(comp_offer_price) min_comp_offer_price,
      max(comp_offer_price) max_comp_offer_price,
      count(DISTINCT comp_color || comp_size) total_variant_count,
      count(DISTINCT comp_color) color_count,
      count(DISTINCT comp_size) size_count
    FROM temp.cm_pricing_adjusted_latest
    GROUP BY 1,2,3,4
);

-- Removing multiple URLs for the same comp_sku, same comp_scrape_date to keep just one for all variants

drop table if exists temp.cm_pricing_adjusted_inter;
CREATE TABLE temp.cm_pricing_adjusted_inter AS (
    SELECT
      feed_date,
      sku,
      comp_sku,
      comp_name,
      comp_scrape_date,
      comp_url,
      comp_title,
      comp_brand
    FROM temp.cm_pricing_adjusted_latest
    GROUP BY 1,2,3,4,5,6,7,8
);

drop table if exists temp.cm_pricing_adjusted_inter_1;
CREATE TABLE temp.cm_pricing_adjusted_inter_1 AS (
    SELECT
      a.*,
      row_number() over (partition by feed_date,sku,comp_name,comp_sku,comp_scrape_date order by comp_url desc)
    FROM temp.cm_pricing_adjusted_inter a
);

drop table if exists temp.cm_pricing_adjusted_latest_2;
CREATE TABLE temp.cm_pricing_adjusted_latest_2 AS (
    SELECT
      a.feed_date,
      a.sku,
      a.comp_sku,
      a.comp_name,
      b.comp_title,
      b.comp_brand,
      b.comp_url,
      a.comp_scrape_date,
      a.comp_pdt_rating,
      a.comp_pdt_reviews,
      a.min_comp_list_price,
      a.max_comp_list_price,
      a.min_comp_offer_price,
      a.max_comp_offer_price,
      a.total_variant_count,
      a.color_count,
      a.size_count
    FROM temp.cm_pricing_adjusted_latest_1 a
    INNER JOIN (SELECT * FROM temp.cm_pricing_adjusted_inter_1 WHERE row_number = 1) b
    ON a.feed_date = b.feed_date
    AND a.sku = b.sku
    AND a.comp_sku = b.comp_sku
    AND a.comp_scrape_date = b.comp_scrape_date
    AND a.comp_name = b.comp_name
);


  
-- Getting Mode Price
drop table if exists temp.cm_pricing_adjusted_med_offer_price;
CREATE TABLE temp.cm_pricing_adjusted_med_offer_price AS (
    SELECT
      feed_date,
      sku,
      comp_sku,
      comp_name,
      comp_offer_price,
      count(*)
    FROM temp.cm_pricing_adjusted_latest
    WHERE comp_offer_price is not null
    GROUP BY 1,2,3,4,5
);
  
drop table if exists temp.cm_pricing_adjusted_med_offer_price_rank;
CREATE TABLE temp.cm_pricing_adjusted_med_offer_price_rank AS (
    SELECT
      feed_date,
      sku,
      comp_sku,
      comp_name,
      comp_offer_price,
      count,
      row_number() over (partition by feed_date,sku,comp_sku,comp_name order by count desc)
    FROM temp.cm_pricing_adjusted_med_offer_price
);
  
drop table if exists temp.cm_pricing_adjusted_med_list_price;
CREATE TABLE temp.cm_pricing_adjusted_med_list_price AS (
    SELECT
      feed_date,
      sku,
      comp_sku,
      comp_name,
      comp_list_price,
      count(*)
    FROM temp.cm_pricing_adjusted_latest
    WHERE comp_list_price is not null
    GROUP BY 1,2,3,4,5
);
  
drop table if exists temp.cm_pricing_adjusted_med_list_price_rank;
CREATE TABLE temp.cm_pricing_adjusted_med_list_price_rank AS (
    SELECT
      feed_date,
      sku,
      comp_sku,
      comp_name,
      comp_list_price,
      count,
      row_number() over (partition by feed_date,sku,comp_sku,comp_name order by count desc)
    FROM temp.cm_pricing_adjusted_med_list_price
);
  
  
DROP TABLE IF EXISTS temp.cm_pricing_adjusted_latest_unique_size;
CREATE TABLE temp.cm_pricing_adjusted_latest_unique_size AS (
    SELECT
      feed_date,
      sku,
      comp_sku,
      comp_name,
      lower(trim(comp_size)) size
    FROM temp.cm_pricing_adjusted_latest
    GROUP BY 1,2,3,4,5
);
  
DROP TABLE IF EXISTS temp.cm_pricing_adjusted_latest_size_list;
CREATE TABLE temp.cm_pricing_adjusted_latest_size_list AS (
    SELECT
      feed_date,
      sku,
      comp_sku,
      comp_name,
      listagg(size,',') within group (order by size) size_list
    FROM temp.cm_pricing_adjusted_latest_unique_size
    GROUP BY 1,2,3,4
);
  
DROP TABLE IF EXISTS temp.cm_pricing_adjusted_latest_unique_color;
CREATE TABLE temp.cm_pricing_adjusted_latest_unique_color AS (
    SELECT
      feed_date,
      sku,
      comp_sku,
      comp_name,
      lower(trim(comp_color)) color
    FROM temp.cm_pricing_adjusted_latest
    GROUP BY 1,2,3,4,5
);
  
DROP TABLE IF EXISTS temp.cm_pricing_adjusted_latest_color_list;
CREATE TABLE temp.cm_pricing_adjusted_latest_color_list AS (
    SELECT
      feed_date,
      sku,
      comp_sku,
      comp_name,
      listagg(color,',') within group (order by color) color_list
    FROM temp.cm_pricing_adjusted_latest_unique_color
    GROUP BY 1,2,3,4
);
  
DROP TABLE IF EXISTS temp.cm_pricing_adjusted_all_data;
CREATE TABLE temp.cm_pricing_adjusted_all_data AS (
    SELECT
      a.feed_date,
      a.sku,
      a.comp_sku,
      a.comp_name,
      comp_title,
      comp_brand,
      a.comp_url,
      comp_scrape_date,
      comp_pdt_rating,
      comp_pdt_reviews,
      CASE WHEN f.comp_in_stock is TRUE THEN 1 
           WHEN f.comp_in_stock is FALSE THEN 0
           ELSE null 
      END comp_in_stock,
      CASE WHEN min_comp_offer_price > min_comp_list_price THEN min_comp_offer_price ELSE min_comp_list_price END min_comp_list_price,
      CASE WHEN max_comp_offer_price > max_comp_list_price THEN max_comp_offer_price ELSE max_comp_list_price END max_comp_list_price,
      CASE WHEN c.comp_offer_price > b.comp_list_price THEN c.comp_offer_price ELSE b.comp_list_price END med_comp_list_price,
      min_comp_offer_price,
      max_comp_offer_price,
      c.comp_offer_price med_comp_offer_price,
      total_variant_count,
      color_count,
      d.color_list,
      size_count,
      e.size_list
    FROM temp.cm_pricing_adjusted_latest_2 a
    LEFT JOIN (SELECT * FROM temp.cm_pricing_adjusted_med_list_price_rank WHERE row_number = 1) b
    ON a.sku = b.sku
    AND  a.comp_sku = b.comp_sku
    AND a.comp_name = b.comp_name
    AND a.feed_date = b.feed_date
    LEFT JOIN (SELECT * FROM temp.cm_pricing_adjusted_med_offer_price_rank WHERE row_number = 1) c
    ON a.sku = c.sku
    AND  a.comp_sku = c.comp_sku
    AND a.comp_name = c.comp_name
    AND a.feed_date = c.feed_date
    INNER JOIN temp.cm_pricing_adjusted_latest_color_list d
    ON a.sku = d.sku
    AND  a.comp_sku = d.comp_sku
    AND a.comp_name = d.comp_name
    AND a.feed_date = d.feed_date
    INNER JOIN temp.cm_pricing_adjusted_latest_size_list e
    ON a.sku = e.sku
    AND  a.comp_sku = e.comp_sku
    AND a.comp_name = e.comp_name
    AND a.feed_date = e.feed_date
    INNER JOIN (SELECT * FROM temp.comp_stock_flag_2 WHERE row_number = 1) f
    ON a.comp_sku = f.comp_sku
    AND a.comp_name = f.comp_name
    AND a.feed_date = f.feed_date
);
  
--select * from temp.cm_pricing_adjusted_all_data
  
--select count(*),count(distinct sku || comp_sku) from temp.cm_pricing_adjusted_all_data limit 10;
-- 8663 8663
  
-- Deduping Comp Data
DROP TABLE IF EXISTS temp.cm_pricing_adjusted_rank_comp_sku;
CREATE TABLE temp.cm_pricing_adjusted_rank_comp_sku AS (
  SELECT
    feed_date,
    sku,
    comp_name,
    comp_sku,
    comp_pdt_reviews,
    row_number() over (partition BY feed_date,comp_name,sku order by coalesce(comp_pdt_reviews,0) desc)
  FROM temp.cm_pricing_adjusted_all_data
);
  
--select * from temp.cm_pricing_adjusted_rank_comp_sku where sku = '0400088797185'
  
-- Removing Dupes
DROP TABLE IF EXISTS temp.cm_pricing_adjusted_all_data_no_dupes;
CREATE TABLE temp.cm_pricing_adjusted_all_data_no_dupes AS (
  SELECt
    a.*
  FROM temp.cm_pricing_adjusted_all_data a
  INNER JOIN (SELECT * FROM temp.cm_pricing_adjusted_rank_comp_sku WHERE row_number = 1) b
  ON a.feed_date = b.feed_date
  AND a.comp_name = b.comp_name
  AND a.sku = b.sku
  AND a.comp_sku = b.comp_sku
);
  
--select * from temp.cm_pricing_adjusted_all_data_no_dupes where sku = '0400088522949'
  
DROP TABLE IF EXISTS temp.catalog_latest_md;
CREATE TABLE temp.catalog_latest_md AS (
    SELECT
      a.feed_date,
      a.sku,
      coalesce(a.title,'') title,
      a.product_url,
      a.upc,
      coalesce(a.description,'') description,
      lower(a.brand) brand,
      coalesce(manufacturer,'') manufacturer,
      b.sku_raw,
      coalesce(b.color,'') color,
      coalesce(b.size,'') size
    FROM (SELECT * FROM base.boomerang_catalog where feed_date = (select max(feed_date) from base.boomerang_catalog)) a
    INNER JOIN (SELECT * FROM client.custom_boomerang_catalog where feed_date = (select max(feed_date) from client.custom_boomerang_catalog)) b
    ON a.sku = b.sku
    WHERE substring(a.sku from 1 for 13) IN (select sku from client.Category_SKU_in_scope group by 1));
  
--select * from temp.only_sku_md where sku ilike '%0400087623483%' order by feed_date desc
  
--SELECT * FROM base.boomerang_catalog where substring(sku from 1 for 13) = '0471948370447' order by feed_date feed_date = (select max(feed_date) from base.boomerang_catalog)and
  
DROP TABLE IF EXISTS temp.catalog_latest_unique_md;
CREATE TABLE temp.catalog_latest_unique_md AS (
    SELECT
      feed_date,
      sku_raw,
      title,
      product_url,
      description,
      brand,
      manufacturer,
      count(distinct color || size) client_total_variant_count,
      count(distinct color) client_color_count,
      count(distinct size) client_size_count,
      row_number() over (PARTITION BY  feed_date,sku_raw, product_url order by client_total_variant_count desc)
    FROM temp.catalog_latest_md
    GROUP BY 1,2,3,4,5,6,7
);
  
--select * from temp.catalog_latest_unique_md where sku_raw IN ('0445913639617','0445913638795') limit 10;
  
DROP TABLE IF EXISTS temp.catalog_latest_unique_md_dedup;
CREATE TABLE temp.catalog_latest_unique_md_dedup AS (
    SELECT
      *
    FROM temp.catalog_latest_unique_md
    WHERE row_number = 1
);
  
--select count(*) from temp.catalog_latest_unique_md
  
-- select count(*) from temp.catalog_latest_unique where row_number = 1
-- select sku_raw,product_url,count(*) from temp.catalog_latest_unique group by 1,2 having count(*)>1 order by 3 desc limit 100
-- select count(*),count(distinct sku_raw) from temp.catalog_latest_unique
-- select * from temp.catalog_latest_unique where product_url = 'http://www.saksfifthavenue.com/main/ProductDetail.jsp?PRODUCT<>prd_id=845524446721126&site_refer='
-- 63961  63934
  
DROP TABLE IF EXISTS temp.catalog_latest_unique_size_md;
CREATE TABLE temp.catalog_latest_unique_size_md AS (
    SELECT
      feed_date,
      sku_raw,
      product_url,
      title,
      description,
      brand,
      manufacturer,
      lower(trim(size)) size
    FROM temp.catalog_latest_md
    GROUP BY 1,2,3,4,5,6,7,8
);
  
DROP TABLE IF EXISTS temp.catalog_latest_size_md;
CREATE TABLE temp.catalog_latest_size_md AS (
    SELECT
      feed_date,
      sku_raw,
      product_url,
      title,
      description,
      brand,
      manufacturer,
      listagg(size,',') within group (order by size) size_list
    FROM temp.catalog_latest_unique_size_md
    GROUP BY 1,2,3,4,5,6,7
);
  
DROP TABLE IF EXISTS temp.catalog_latest_unique_color_md;
CREATE TABLE temp.catalog_latest_unique_color_md AS (
    SELECT
      feed_date,
      sku_raw,
      product_url,
      title,
      description,
      brand,
      manufacturer,
      lower(trim(color)) color
    FROM temp.catalog_latest_md
    GROUP BY 1,2,3,4,5,6,7,8
);
  
DROP TABLE IF EXISTS temp.catalog_latest_color_md;
CREATE TABLE temp.catalog_latest_color_md AS (
    SELECT
      feed_date,
      sku_raw,
      product_url,
      title,
      description,
      brand,
      manufacturer,
      listagg(color,',') within group (order by color) color_list
    FROM temp.catalog_latest_unique_color_md
    GROUP BY 1,2,3,4,5,6,7
);
  
DROP TABLE IF EXISTS temp.catalog_latest_unique_upc_md;
CREATE TABLE temp.catalog_latest_unique_upc_md AS (
    SELECT
      feed_date,
      sku_raw,
      product_url,
      title,
      description,
      brand,
      manufacturer,
      upc
    FROM temp.catalog_latest_md
    GROUP BY 1,2,3,4,5,6,7,8
);
  
--
  
--select * from temp.catalog_latest_unique_upc_md limit 10;
  
DROP TABLE IF EXISTS temp.catalog_latest_upc_md;
CREATE TABLE temp.catalog_latest_upc_md AS (
    SELECT
      feed_date,
      sku_raw,
      product_url,
      title,
      description,
      brand,
      manufacturer,
      listagg(upc,';') within group (order by upc) sak_id
    FROM temp.catalog_latest_unique_upc_md
    GROUP BY 1,2,3,4,5,6,7
);
  
--select * from temp.catalog_latest_upc_md limit 10;
  
--select * from temp.catalog_latest_color_md where sku_raw = '0404399613955'
  
DROP TABLE IF EXISTS temp.catalog_latest_color_size_agg_md;
CREATE TABLE temp.catalog_latest_color_size_agg_md AS (
    SELECT
      a.feed_date,
      a.sku_raw,
      a.title,
      a.product_url,
      a.description,
      a.brand,
      a.manufacturer,
      client_total_variant_count,
      b.color_list,
      client_color_count,
      c.size_list,
      client_size_count,
      d.sak_id
    FROM temp.catalog_latest_unique_md_dedup a
    INNER JOIN temp.catalog_latest_color_md b
    ON a.feed_date = b.feed_date
    AND a.sku_raw = b.sku_raw
    AND a.product_url = b.product_url
    AND a.feed_date = b.feed_date
    AND a.description = b.description
    AND a.title = b.title
    AND a.manufacturer = b.manufacturer
    INNER JOIN temp.catalog_latest_size_md c
    ON a.feed_date = c.feed_date
    AND a.sku_raw = c.sku_raw
    AND a.product_url = c.product_url
    AND a.brand = c.brand
    AND a.description = c.description
    AND a.title = c.title
    AND a.manufacturer = c.manufacturer
    INNER JOIN temp.catalog_latest_upc_md d
    ON a.feed_date = d.feed_date
    AND a.sku_raw = d.sku_raw
    AND a.product_url = d.product_url
    AND a.brand = d.brand
    AND a.description = d.description
    AND a.title = d.title
    AND a.manufacturer = d.manufacturer
);
  
--select count(*) from temp.catalog_latest_unique_md
--select * from temp.catalog_latest_color_size_agg_md where sku_raw = '0471948370447'
  
drop table if exists temp.catalog_hierarchy_merch_unique;
CREATE TABLE temp.catalog_hierarchy_merch_unique AS (
    SELECT
      sku,
      coalesce(divison,'') divison,
      coalesce(group_name,'') group_name,
      coalesce(buyer,'') buyer,
      coalesce(dept,'') dept,
      coalesce(manufacturer,'') manufacturer,
      coalesce(class,'') class_name,
      coalesce(subclass,'') subclass,
      coalesce(fashion_style,'') fashion_style
    FROM client.custom_hierarchy_catalog
    where feed_date = (select max(feed_date) from client.custom_hierarchy_catalog)
    GROUP BY 1,2,3,4,5,6,7,8,9
);
  
--select * from temp.catalog_hierarchy_merch_unique limit 1000;
  
drop table if exists temp.item_prices_latest;
CREATE TABLE temp.item_prices_latest AS (
    SELECT
      sku_raw sku,
      max(list_price) max_list_price,
      min(list_price) min_list_price,
      max(offer_price) max_offer_price,
      min(offer_price) min_offer_price
    FROM client.custom_item_prices
    WHERE feed_date = (SELECT max(feed_date) from client.custom_item_prices)
    GROUP BY 1
);
  
drop table if exists temp.item_prices_latest_med_offer_price;
CREATE TABLE temp.item_prices_latest_med_offer_price AS (
    SELECT
      sku_raw sku,
      offer_price,
      count(*)
    FROM (SELECT sku_raw,upc,offer_price FROM client.custom_item_prices
          WHERE feed_date = (SELECT max(feed_date) from client.custom_item_prices)
          AND offer_price is not null
          GROUP BY 1,2,3) a
    GROUP BY 1,2
);
  
drop table if exists temp.item_prices_latest_med_offer_price_rank;
CREATE TABLE temp.item_prices_latest_med_offer_price_rank AS (
    SELECT
      sku,
      offer_price,
      count,
      row_number() over (partition by sku order by count desc)
    FROM temp.item_prices_latest_med_offer_price
);
  
  
drop table if exists temp.item_prices_latest_med_list_price;
CREATE TABLE temp.item_prices_latest_med_list_price AS (
    SELECT
      sku_raw sku,
      list_price,
      count(*)
    FROM (SELECT sku_raw,upc,list_price FROM client.custom_item_prices
          WHERE feed_date = (SELECT max(feed_date) from client.custom_item_prices)
          AND list_price is not null
          GROUP BY 1,2,3) a
    GROUP BY 1,2
);
  
drop table if exists temp.item_prices_latest_med_list_price_rank;
CREATE TABLE temp.item_prices_latest_med_list_price_rank AS (
    SELECT
      sku,
      list_price,
      count,
      row_number() over (partition by sku order by count desc)
    FROM temp.item_prices_latest_med_list_price
);
  
DROP TABLE IF EXISTS temp.catalog_size_color_w_merch_hierarchy;
CREATE TABLE temp.catalog_size_color_w_merch_hierarchy AS (
    SELECT
      date(current_date) feed_date,
      a.sku_raw sku,
      a.title,
      a.product_url,
      a.description,
      a.brand,
      a.manufacturer manufacturer_catalog,
      a.client_total_variant_count,
      a.color_list,
      a.client_color_count,
      a.size_list,
      a.client_size_count,
      a.sak_id,
      b.divison,
      b.group_name,
      b.buyer,
      b.dept,
      b.manufacturer,
      b.class_name,
      b.subclass,
      b.fashion_style,
      CASE WHEN c.min_offer_price > c.min_list_price THEN c.min_offer_price ELSE c.min_list_price END client_min_list_price,
      CASE WHEN c.max_offer_price > c.max_list_price THEN c.max_offer_price ELSE c.max_list_price END client_max_list_price,
      CASE WHEN e.offer_price > d.list_price THEN e.offer_price ELSE d.list_price END client_med_list_price,
      c.min_offer_price client_min_offer_price,
      c.max_offer_price client_max_offer_price,
      e.offer_price client_med_offer_price
    FROM temp.catalog_latest_color_size_agg_md a
    LEFT JOIN temp.catalog_hierarchy_merch_unique b
    ON a.sku_raw = b.sku
    LEFT JOIN  temp.item_prices_latest c
    ON a.sku_raw = c.sku
    LEFT JOIN (SELECT * FROM temp.item_prices_latest_med_list_price_rank WHERE row_number = 1) d
    ON a.sku_raw = d.sku
    LEFT JOIN (SELECT * FROM temp.item_prices_latest_med_offer_price_rank WHERE row_number = 1) e
    ON a.sku_raw = e.sku
);
  
--DROP TABLE IF EXISTS client.saks_markdown_final;

DELETE FROM client.saks_markdown_final
WHERE feed_date = (SELECT max(feed_date) FROM temp.catalog_size_color_w_merch_hierarchy);
  
DROP TABLE IF EXISTS client.saks_markdown_final_backup;
CREATE TABLE client.saks_markdown_final_backup AS (
  SELECT
    *
  FROM client.saks_markdown_final
);
  
--select feed_date,count(*),count(distinct sku) from client.saks_markdown_final_backup group by 1 order by 1
  
INSERT INTO client.saks_markdown_final
  SELECT
    a.*,
    b.comp_name            comp1_name,
    b.comp_sku             comp1_sku,
    b.comp_title           comp1_title,
    b.comp_brand           comp1_brand,
    b.comp_url             comp1_url,
    dateadd(hour,-4,b.comp_scrape_date) comp1_scrape_date,
    b.comp_pdt_rating      comp1_pdt_rating,
    b.comp_pdt_reviews     comp1_pdt_reviews,
    b.comp_in_stock     comp1_in_stock,
    b.min_comp_list_price  min_comp1_list_price,
    b.max_comp_list_price  max_comp1_list_price,
    b.med_comp_list_price  med_comp1_list_price,
    b.min_comp_offer_price min_comp1_offer_price,
    b.max_comp_offer_price max_comp1_offer_price,
    b.med_comp_offer_price med_comp1_offer_price,
    b.total_variant_count  comp1_total_variant_count,
    b.color_list           comp1_color_list,
    b.color_count          comp1_color_count,
    b.size_list            comp1_size_list,
    b.size_count           comp1_size_count,
    CASE WHEN b.comp_sku IS NULL
      THEN 'N'
    ELSE 'Y' END           carried_by_comp1,
    c.comp_name            comp2_name,
    c.comp_sku             comp2_sku,
    c.comp_title           comp2_title,
    c.comp_brand           comp2_brand,
    c.comp_url             comp2_url,
    dateadd(hour,-4,c.comp_scrape_date) comp2_scrape_date,
    c.comp_pdt_rating      comp2_pdt_rating,
    c.comp_pdt_reviews     comp2_pdt_reviews,
    c.comp_in_stock     comp2_in_stock,
    c.min_comp_list_price  min_comp2_list_price,
    c.max_comp_list_price  max_comp2_list_price,
    c.med_comp_list_price  med_comp2_list_price,
    c.min_comp_offer_price min_comp2_offer_price,
    c.max_comp_offer_price max_comp2_offer_price,
    c.med_comp_offer_price med_comp2_offer_price,
    c.total_variant_count  comp2_total_variant_count,
    c.color_list           comp2_color_list,
    c.color_count          comp2_color_count,
    c.size_list            comp2_size_list,
    c.size_count           comp2_size_count,
    CASE WHEN c.comp_sku IS NULL
      THEN 'N'
    ELSE 'Y' END           carried_by_comp2,
    d.comp_name            comp3_name,
    d.comp_sku             comp3_sku,
    d.comp_title           comp3_title,
    d.comp_brand           comp3_brand,
    d.comp_url             comp3_url,
    dateadd(hour,-4,d.comp_scrape_date) comp3_scrape_date,
    d.comp_pdt_rating      comp3_pdt_rating,
    d.comp_pdt_reviews     comp3_pdt_reviews,
    d.comp_in_stock     comp3_in_stock,
    d.min_comp_list_price  min_comp3_list_price,
    d.max_comp_list_price  max_comp3_list_price,
    d.med_comp_list_price  med_comp3_list_price,
    d.min_comp_offer_price min_comp3_offer_price,
    d.max_comp_offer_price max_comp3_offer_price,
    d.med_comp_offer_price med_comp3_offer_price,
    d.total_variant_count  comp3_total_variant_count,
    d.color_list           comp3_color_list,
    d.color_count          comp3_color_count,
    d.size_list            comp3_size_list,
    d.size_count           comp3_size_count,
    CASE WHEN d.comp_sku IS NULL
      THEN 'N'
    ELSE 'Y' END           carried_by_comp3,
    'Refreshed on ' + date(dateadd(hour,-4,b.comp_scrape_date)) :: varchar comp1_refresh_flag,
    'Refreshed on ' + date(dateadd(hour,-4,c.comp_scrape_date)) :: varchar comp2_refresh_flag,
    'Refreshed on ' + date(dateadd(hour,-4,d.comp_scrape_date)) :: varchar comp3_refresh_flag,
    e.comp_name            comp4_name,
    e.comp_sku             comp4_sku,
    e.comp_title           comp4_title,
    e.comp_brand           comp4_brand,
    e.comp_url             comp4_url,
    dateadd(hour,-4,e.comp_scrape_date) comp4_scrape_date,
    e.comp_pdt_rating      comp4_pdt_rating,
    e.comp_pdt_reviews     comp4_pdt_reviews,
    e.comp_in_stock     comp4_in_stock,
    e.min_comp_list_price  min_comp4_list_price,
    e.max_comp_list_price  max_comp4_list_price,
    e.med_comp_list_price  med_comp4_list_price,
    e.min_comp_offer_price min_comp4_offer_price,
    e.max_comp_offer_price max_comp4_offer_price,
    e.med_comp_offer_price med_comp4_offer_price,
    e.total_variant_count  comp4_total_variant_count,
    e.color_list           comp4_color_list,
    e.color_count          comp4_color_count,
    e.size_list            comp4_size_list,
    e.size_count           comp4_size_count,
    CASE WHEN e.comp_sku IS NULL
      THEN 'N'
    ELSE 'Y' END           carried_by_comp4,
    'Refreshed on ' + date(dateadd(hour,-4,e.comp_scrape_date)) :: varchar comp4_refresh_flag,
 -- comp5
    f.comp_name            comp5_name,
    f.comp_sku             comp5_sku,
    f.comp_title           comp5_title,
    f.comp_brand           comp5_brand,
    f.comp_url             comp5_url,
    dateadd(hour,-4,f.comp_scrape_date) comp5_scrape_date,
    f.comp_pdt_rating      comp5_pdt_rating,
    f.comp_pdt_reviews     comp5_pdt_reviews,
    f.comp_in_stock     comp5_in_stock,
    f.min_comp_list_price  min_comp5_list_price,
    f.max_comp_list_price  max_comp5_list_price,
    f.med_comp_list_price  med_comp5_list_price,
    f.min_comp_offer_price min_comp5_offer_price,
    f.max_comp_offer_price max_comp5_offer_price,
    f.med_comp_offer_price med_comp5_offer_price,
    f.total_variant_count  comp5_total_variant_count,
    f.color_list           comp5_color_list,
    f.color_count          comp5_color_count,
    f.size_list            comp5_size_list,
    f.size_count           comp5_size_count,
    CASE WHEN f.comp_sku IS NULL
      THEN 'N'
    ELSE 'Y' END           carried_by_comp5,
    'Refreshed on ' + date(dateadd(hour,-4,f.comp_scrape_date)) :: varchar comp5_refresh_flag,
 --comp6
    g.comp_name            comp6_name,
    g.comp_sku             comp6_sku,
    g.comp_title           comp6_title,
    g.comp_brand           comp6_brand,
    g.comp_url             comp6_url,
    dateadd(hour,-4,g.comp_scrape_date) comp6_scrape_date,
    g.comp_pdt_rating      comp6_pdt_rating,
    g.comp_pdt_reviews     comp6_pdt_reviews,
    g.comp_in_stock     comp6_in_stock,
    g.min_comp_list_price  min_comp6_list_price,
    g.max_comp_list_price  max_comp6_list_price,
    g.med_comp_list_price  med_comp6_list_price,
    g.min_comp_offer_price min_comp6_offer_price,
    g.max_comp_offer_price max_comp6_offer_price,
    g.med_comp_offer_price med_comp6_offer_price,
    g.total_variant_count  comp6_total_variant_count,
    g.color_list           comp6_color_list,
    g.color_count          comp6_color_count,
    g.size_list            comp6_size_list,
    g.size_count           comp6_size_count,
    CASE WHEN g.comp_sku IS NULL
      THEN 'N'
    ELSE 'Y' END           carried_by_comp6,
    'Refreshed on ' + date(dateadd(hour,-4,g.comp_scrape_date)) :: varchar comp6_refresh_flag,
  --comp7
    h.comp_name            comp7_name,
    h.comp_sku             comp7_sku,
    h.comp_title           comp7_title,
    h.comp_brand           comp7_brand,
    h.comp_url             comp7_url,
    dateadd(hour,-4,h.comp_scrape_date) comp7_scrape_date,
    h.comp_pdt_rating      comp7_pdt_rating,
    h.comp_pdt_reviews     comp7_pdt_reviews,
    h.comp_in_stock     comp7_in_stock,
    h.min_comp_list_price  min_comp7_list_price,
    h.max_comp_list_price  max_comp7_list_price,
    h.med_comp_list_price  med_comp7_list_price,
    h.min_comp_offer_price min_comp7_offer_price,
    h.max_comp_offer_price max_comp7_offer_price,
    h.med_comp_offer_price med_comp7_offer_price,
    h.total_variant_count  comp7_total_variant_count,
    h.color_list           comp7_color_list,
    h.color_count          comp7_color_count,
    h.size_list            comp7_size_list,
    h.size_count           comp7_size_count,
    CASE WHEN h.comp_sku IS NULL
      THEN 'N'
    ELSE 'Y' END           carried_by_comp7,
    'Refreshed on ' + date(dateadd(hour,-4,h.comp_scrape_date)) :: varchar comp7_refresh_flag,
  --comp8
    i.comp_name            comp8_name,
    i.comp_sku             comp8_sku,
    i.comp_title           comp8_title,
    i.comp_brand           comp8_brand,
    i.comp_url             comp8_url,
    dateadd(hour,-4,i.comp_scrape_date) comp8_scrape_date,
    i.comp_pdt_rating      comp8_pdt_rating,
    i.comp_pdt_reviews     comp8_pdt_reviews,
    i.comp_in_stock     comp8_in_stock,
    i.min_comp_list_price  min_comp8_list_price,
    i.max_comp_list_price  max_comp8_list_price,
    i.med_comp_list_price  med_comp8_list_price,
    i.min_comp_offer_price min_comp8_offer_price,
    i.max_comp_offer_price max_comp8_offer_price,
    i.med_comp_offer_price med_comp8_offer_price,
    i.total_variant_count  comp8_total_variant_count,
    i.color_list           comp8_color_list,
    i.color_count          comp8_color_count,
    i.size_list            comp8_size_list,
    i.size_count           comp8_size_count,
    CASE WHEN i.comp_sku IS NULL
      THEN 'N'
    ELSE 'Y' END           carried_by_comp8,
    'Refreshed on ' + date(dateadd(hour,-4,i.comp_scrape_date)) :: varchar comp8_refresh_flag,
  --comp9
    j.comp_name            comp9_name,
    j.comp_sku             comp9_sku,
    j.comp_title           comp9_title,
    j.comp_brand           comp9_brand,
    j.comp_url             comp9_url,
    dateadd(hour,-4,j.comp_scrape_date) comp9_scrape_date,
    j.comp_pdt_rating      comp9_pdt_rating,
    j.comp_pdt_reviews     comp9_pdt_reviews,
    j.comp_in_stock     comp9_in_stock,
    j.min_comp_list_price  min_comp9_list_price,
    j.max_comp_list_price  max_comp9_list_price,
    j.med_comp_list_price  med_comp9_list_price,
    j.min_comp_offer_price min_comp9_offer_price,
    j.max_comp_offer_price max_comp9_offer_price,
    j.med_comp_offer_price med_comp9_offer_price,
    j.total_variant_count  comp9_total_variant_count,
    j.color_list           comp9_color_list,
    j.color_count          comp9_color_count,
    j.size_list            comp9_size_list,
    j.size_count           comp9_size_count,
    CASE WHEN j.comp_sku IS NULL
      THEN 'N'
    ELSE 'Y' END           carried_by_comp9,
    'Refreshed on ' + date(dateadd(hour,-4,j.comp_scrape_date)) :: varchar comp9_refresh_flag,
  --comp10
    k.comp_name            comp10_name,
    k.comp_sku             comp10_sku,
    k.comp_title           comp10_title,
    k.comp_brand           comp10_brand,
    k.comp_url             comp10_url,
    dateadd(hour,-4,k.comp_scrape_date) comp10_scrape_date,
    k.comp_pdt_rating      comp10_pdt_rating,
    k.comp_pdt_reviews     comp10_pdt_reviews,
    k.comp_in_stock     comp10_in_stock,
    k.min_comp_list_price  min_comp10_list_price,
    k.max_comp_list_price  max_comp10_list_price,
    k.med_comp_list_price  med_comp10_list_price,
    k.min_comp_offer_price min_comp10_offer_price,
    k.max_comp_offer_price max_comp10_offer_price,
    k.med_comp_offer_price med_comp10_offer_price,
    k.total_variant_count  comp10_total_variant_count,
    k.color_list           comp10_color_list,
    k.color_count          comp10_color_count,
    k.size_list            comp10_size_list,
    k.size_count           comp10_size_count,
    CASE WHEN k.comp_sku IS NULL
      THEN 'N'
    ELSE 'Y' END           carried_by_comp10,
    'Refreshed on ' + date(dateadd(hour,-4,k.comp_scrape_date)) :: varchar comp10_refresh_flag 
  FROM temp.catalog_size_color_w_merch_hierarchy a
  LEFT JOIN temp.cm_pricing_adjusted_all_data_no_dupes b
  ON a.sku = b.sku
  AND b.comp_name = (select comp_name from client.comp_list where comp_id = 'comp1')
  LEFT JOIN temp.cm_pricing_adjusted_all_data_no_dupes c
  ON a.sku = c.sku
  AND c.comp_name = (select comp_name from client.comp_list where comp_id = 'comp2')
  LEFT JOIN temp.cm_pricing_adjusted_all_data_no_dupes d
  ON a.sku = d.sku
  AND d.comp_name = (select comp_name from client.comp_list where comp_id = 'comp3')
  LEFT JOIN temp.cm_pricing_adjusted_all_data_no_dupes e
  ON a.sku = e.sku
  AND e.comp_name = (select comp_name from client.comp_list where comp_id = 'comp4')
  LEFT JOIN temp.cm_pricing_adjusted_all_data_no_dupes f
  ON a.sku = f.sku
  AND f.comp_name = (select comp_name from client.comp_list where comp_id = 'comp5')
  LEFT JOIN temp.cm_pricing_adjusted_all_data_no_dupes g
  ON a.sku = g.sku
  AND g.comp_name = (select comp_name from client.comp_list where comp_id = 'comp6')
  LEFT JOIN temp.cm_pricing_adjusted_all_data_no_dupes h
  ON a.sku = h.sku
  AND h.comp_name = (select comp_name from client.comp_list where comp_id = 'comp7')
  LEFT JOIN temp.cm_pricing_adjusted_all_data_no_dupes i
  ON a.sku = i.sku
  AND i.comp_name = (select comp_name from client.comp_list where comp_id = 'comp8')
  LEFT JOIN temp.cm_pricing_adjusted_all_data_no_dupes j
  ON a.sku = j.sku
  AND j.comp_name = (select comp_name from client.comp_list where comp_id = 'comp9')
  LEFT JOIN temp.cm_pricing_adjusted_all_data_no_dupes k
  ON a.sku = k.sku
  AND k.comp_name = (select comp_name from client.comp_list where comp_id = 'comp10');
