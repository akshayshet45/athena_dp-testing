#!/bin/bash
set -e
azkaban_host=$1
azkaban_username=$2
azkaban_passwd=$3
azkaban_project=$4
flow=$5
donotfail=$6


login_response=$(curl -k -X POST --data "action=login&username=$azkaban_username&password=$azkaban_passwd" $azkaban_host:8081)
echo "Login response:: "$login_response

status=$(echo $login_response | jq '."status"')
status=$(echo ${status:1:$(echo ${#status}) - 2})
echo $status

if [ "$status" == "success" ]
then
echo "Azkaban login successfull."
else
echo "======> ERROR in Azkaban login. Check Azkaban user-name/password or azkaban_host."
exit 1
fi

session=$(echo $login_response | jq '."session.id"')
session=$(echo ${session:1:$(echo ${#session}) - 2})
echo "session : $session"
runflag="true"

execid_response=$(curl -k --get --data "session.id=$session" --data 'ajax=executeFlow' --data "project=$azkaban_project" --data "flow=$flow"  --data "notifyFailureFirst=true" --data "failureAction=finishPossible" --data "concurrentOption=ignore" $azkaban_host:8081/executor)
execid=$(echo $execid_response | jq ".execid")

if [ "$execid" == "null" ]; then
echo "Not able to execute $flow , response : $execid_response"
exit 1
fi

while [ $runflag = "true" ]
do
execIDs=$(curl -k --data "session.id=$session&ajax=getRunning&project=$azkaban_project&flow=$flow" $azkaban_host:8081/executor)
i=0

while [ $i -lt ${#execIDs[@]} ]
do
runningexecid=$(echo $execIDs | jq ".execIds[$i]" |sed 's/\"//g')
echo "runningexecid $runningexecid"
echo "execid : $execid"
if [ "$runningexecid" != "$execid" ]; then
runflag="false"
fi   
i=`expr $i + 1`
done

if [ "$runflag" = "true" ]; then
sleep 30s
fi
done
runstatus=""
executedlist=$(curl -k --get --data "session.id=$session&ajax=fetchFlowExecutions&project=$azkaban_project&flow=$flow&start=0&length=100" $azkaban_host:8081/manager)
j=0
while [ $j -lt ${#executedlist[@]} ]
do
executedexecid=$(echo $executedlist | jq ".executions[$j].execId" |sed 's/\"//g')
if [ "$executedexecid" = "$execid" ]; then
runstatus=$(echo $executedlist | jq ".executions[$j].status" |sed 's/\"//g')
fi
j=`expr $j + 1`
done

echo "runstatus  : $runstatus"

if [ "$runstatus" = "SUCCEEDED" ]; then
echo "Execution id: $execid  of  --> Project: $azkaban_project  for  --> Flow: $flow --> finished successfully"
else
echo "Failed : Execution id: $execid  of Project: $azkaban_project for Flow: $flow failed"
if [ "$donotfail" != "true" ]; then
echo "exit with error"
exit 1
fi
fi

