create table if not exists custom_performance(
sku character varying(50) encode lzo not null,
transaction_date timestamp without time zone encode runlength not null,
gross_handling_charges numeric(10,2) encode delta32k ,
new_customers numeric(10,2) encode delta32k ,
cart_adds integer encode lzo ,
channel_id integer encode lzo not null,
feed_date timestamp without time zone  not null,
creation_date timestamp without time zone  not null)
distkey(sku)
sortkey(feed_date,sku,transaction_date,channel_id);

create table if not exists custom_catalogs(
sku character varying(50) encode text255 not null,
channel_id integer encode mostly16 not null,
sku_status character varying(100) encode text255 ,
part_name character varying(100) encode text255 ,
part_id integer encode lzo ,
brand_id integer encode lzo ,
year integer encode mostly16 ,
make character varying(100) encode lzo ,
model character varying(100) encode lzo ,
submodel character varying(100) encode lzo ,
engine_type character varying(100) encode lzo ,
title character varying(1000) encode lzo ,
team_name character varying(100) encode lzo ,
team_id integer encode lzo ,
sku_mover_type character varying(100) encode lzo ,
clearance_type character varying(100) encode lzo ,
worldpac_flag integer encode lzo ,
truck_freight_flag integer encode lzo ,
ford_patent integer encode lzo ,
core_cost numeric(7,2)  ,
core_price numeric(7,2)  ,
po_cost_price numeric(7,2)  ,
fifo_cost numeric(7,2)  ,
ds_cost numeric(7,2)  ,
ss_cost numeric(7,2)  ,
brand_type character varying(100) encode text255 ,
map_type character varying(100) encode text255 ,
rubix_vendor_name character varying(100) encode text255 ,
rubix_cost numeric(7,2)  ,
always_on integer encode lzo ,
is_kill integer encode mostly16 ,
is_active integer encode mostly16 ,
parent_product_id integer encode mostly16 ,
stockship integer encode lzo ,
delivery_channel integer encode lzo ,
handlingrevenue numeric(7,2) encode delta32k ,
shipping_revenue numeric(7,2) encode delta32k ,
extra_shipping numeric(7,2) encode delta32k ,
list_price numeric(7,2) encode delta32k ,
sku_number character varying(100) encode text255 ,
feed_date timestamp without time zone  ,
creation_date timestamp without time zone encode runlength ,
usap_sku character varying(100) encode text255 ,
kit integer encode mostly16 ,
iswarranty character varying(10) encode text255 ,
vendor_name character varying(255) encode lzo ,
pf_id character varying(1000) encode lzo ,
pf_code character varying(1000) encode lzo ,
pf_name character varying(1000) encode lzo ,
channel_sku_mover_type character varying(1000) encode lzo ,
born_date timestamp without time zone  ,
pricing_status character varying(100) encode text255 )
distkey(sku)
sortkey(feed_date,sku);

create table if not exists global_markup(
bucket_from numeric(10,2) encode delta32k not null,
bucket_to numeric(10,2) encode delta32k not null,
mark_up numeric(10,2) encode delta32k not null,
feed_date timestamp without time zone  not null,
creation_date timestamp without time zone encode runlength )
distkey(feed_date)
sortkey(bucket_from,bucket_to,feed_date);

create table if not exists part_target_margin(
part_name character varying(100) encode lzo ,
start_cost numeric(12,6) encode delta32k ,
end_cost numeric(12,6) encode delta32k ,
target_margin numeric(12,6) encode delta32k ,
feed_date timestamp without time zone  ,
creation_date timestamp without time zone encode runlength ,
brand_name character varying(30)  )
distkey(part_name)
sortkey(part_name,start_cost,feed_date);


create table if not exists part_brand_target_margin(
part_name character varying(100) encode lzo ,
brand_name character varying(100) encode lzo ,
start_cost numeric(12,6) encode delta32k ,
end_cost numeric(12,6) encode delta32k ,
target_margin numeric(12,6) encode delta32k ,
feed_date timestamp without time zone  ,
creation_date timestamp without time zone encode runlength )
distkey(part_name)
sortkey(part_name,brand_name,start_cost,feed_date);


create table if not exists channel_markup(
start_cost numeric(18,6) encode delta32k ,
end_cost numeric(18,6) encode delta32k ,
price1 numeric(18,6) encode delta32k ,
price2 numeric(18,6) encode delta32k ,
price3 numeric(18,6) encode delta32k ,
price4 numeric(18,6) encode delta32k ,
price8 numeric(18,6) encode delta32k ,
jcw numeric(18,6) encode delta32k ,
stylin numeric(18,6) encode delta32k ,
feed_date timestamp without time zone  ,
creation_date timestamp without time zone encode runlength )
distkey(start_cost)
sortkey(start_cost,end_cost,feed_date);


create table if not exists ebay_data(
usap_sku character varying(225) encode lzo not null,
usap_productid character varying(225) encode lzo ,
usap_brand character varying(225) encode lzo ,
usap_current_price numeric(10,2) encode mostly32 ,
usap_current_shipping_price numeric(10,2) encode mostly32 ,
usap_qty_sold_year numeric(10,2) encode mostly32 ,
usap_sales_last7days numeric(10,2) encode mostly32 ,
usap_sales_last30days numeric(10,2) encode mostly32 ,
usap_title character varying(225) encode lzo ,
usap_ebay_itemid character varying(225) encode lzo ,
usap_image character varying(225) encode lzo ,
usap_ebay_categoryid character varying(225) encode lzo not null,
usap_ebay_category_name character varying(225) encode lzo ,
usap_total_inventory numeric(10,2) encode mostly32 ,
usap_b2b_desc character varying(225) encode lzo ,
usap_dno character varying(225) encode lzo ,
usap_sku_status character varying(225) encode lzo ,
usap_condition character varying(225) encode lzo ,
usap_sku_comment character varying(2000) encode lzo ,
usap_map_price numeric(10,2) encode mostly32 ,
usap_promo_price numeric(10,2) encode mostly32 ,
comp_name character varying(225) encode lzo ,
comp_ranking character varying(225) encode lzo ,
comp_sku character varying(225) encode lzo ,
competitor_price numeric(10,2) encode mostly32 ,
competitor_shipping_price numeric(10,2) encode mostly32 ,
comp_qty_sold numeric(10,2) encode mostly32 ,
comp_sales_last7days numeric(10,2) encode mostly32 ,
comp_sales_last30days numeric(10,2) encode mostly32 ,
comp_title character varying(225) encode lzo ,
comp_ebay_itemid character varying(225) encode lzo ,
comp_image character varying(225) encode lzo ,
comp_ebay_categoryid character varying(225) encode lzo ,
comp_ebay_category_name character varying(225) encode lzo ,
comp_brand character varying(225) encode lzo ,
comp_upc character varying(225) encode lzo ,
comp_manufacturer_sku character varying(225) encode lzo ,
comp_listing_duration character varying(225) encode lzo ,
comp_listing_end_date character varying(225) encode lzo ,
comp_condition character varying(225) encode lzo ,
last_udpate_date character varying(255) encode lzo ,
last_updt_comp_name character varying(225) encode lzo ,
last_updt_comp_price character varying(255) encode lzo ,
last_updt_comp_ebayid character varying(225) encode lzo ,
last_updt_date_mapped character varying(255) encode lzo ,
usr_name_last_edited character varying(225) encode lzo ,
usr_notes character varying(2000) encode lzo ,
ams_inventoryid character varying(225) encode lzo ,
usap_sec_ebay_categoryid character varying(225) encode lzo ,
usap_sec_ebay_cat_name character varying(225) encode lzo ,
comp_sec_ebay_categoryid character varying(225) encode lzo ,
comp_sec_ebay_cat_name character varying(225) encode lzo ,
feed_date timestamp without time zone  ,
creation_date timestamp without time zone  )
distkey(usap_sku)
sortkey(usap_sku,feed_date,creation_date);

create table if not exists markup_bucket_part_brand(
bucket_from numeric(10,2) encode delta32k ,
bucket_to numeric(10,2) encode delta32k ,
brand_name character varying(500) encode lzo ,
part_name character varying(100) encode lzo ,
apw numeric(10,2) encode delta32k ,
cp numeric(10,2) encode delta32k ,
ps numeric(10,2) encode delta32k ,
dss numeric(10,2) encode delta32k ,
jcw numeric(10,2) encode delta32k ,
sty numeric(10,2) encode delta32k ,
feed_date timestamp without time zone encode runlength ,
creation_date timestamp without time zone encode runlength ,
ebay numeric(10,2)  )
distkey(feed_date)
sortkey(bucket_from,bucket_to,brand_name);

create table if not exists inventory (
	sku character varying(500) not null encode LZO,
	usap_sku character varying(500) not null encode LZO,
	stockship_inventory integer encode MOSTLY16,
	dropship_inventory integer encode MOSTLY16,
	feed_date timestamp without time zone encode runlength,
	creation_date timestamp without time zone encode runlength
);