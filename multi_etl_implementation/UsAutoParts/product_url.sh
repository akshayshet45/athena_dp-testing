#!/bin/bash
# Vishal Prakash Narain Srivastava on 2016-09-07 - Purpose of this script is to execute jar which will run map reduce job on EMR
# for picking product URLs for channels as per priority.
# JAR code is located at athena_is-team-code/usautoparts_product_url_map_reduce and jar with depedency is being copied to S3 for execution.

set -e
set -x

function error_exit()
{
	if [[ $1 = 0 ]]; then
		echo "SUCCESS"
		exit 0
	else
		echo "FAILURE"
		exit 1
	fi
}

export TZ=America/Los_Angeles
curr_day="$(date +'%d')"
curr_month="$(date +'%m')"
curr_year="$(date +'%Y')"
curr_date="$(date +'%Y%m%d')"

cd /mnt

sudo mkdir -p usautoparts/product_urls

sudo chown -R ec2-user:ec2-user usautoparts

cd usautoparts/product_urls

lftp -u usautoparts-sftp-user,U4\!p@ts4Q sftp://sftp.usautoparts.rboomerang.com <<- EOD
cd /prod/input/product_urls
mget USAP_PRODUCT_URLS_$curr_date.txt.gz
EOD

if [ ! -f USAP_PRODUCT_URLS_$curr_date.txt.gz ]; then
	echo "Feed Mising"
	error_exit 1
fi

gzip -d USAP_PRODUCT_URLS_$curr_date.txt.gz
sed -i 1d USAP_PRODUCT_URLS_$curr_date.txt
aws s3 cp USAP_PRODUCT_URLS_$curr_date.txt s3://etl-backup-all-clients/us-auto-parts/product_url_raw_feed/$curr_year/$curr_month/$curr_day/input/
aws s3 cp s3://etl-backup-all-clients/us-auto-parts/jars/producturl.mapreduce-1.0-jar-with-dependencies.jar .
hadoop jar producturl.mapreduce-1.0-jar-with-dependencies.jar s3://etl-backup-all-clients/us-auto-parts/product_url_raw_feed/$curr_year/$curr_month/$curr_day/input/USAP_PRODUCT_URLS_$curr_date.txt s3://etl-backup-all-clients/us-auto-parts/product_url_raw_feed/$curr_year/$curr_month/$curr_day/output/
returnVal=$?
error_exit $returnVal
