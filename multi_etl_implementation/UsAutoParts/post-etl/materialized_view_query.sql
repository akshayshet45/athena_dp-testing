\set ON_ERROR_STOP 1
\timing on

drop table if exists pa_metadata.measurement_performance_order_materialized;
create table pa_metadata.measurement_performance_order_materialized as
select * from base.measurement_performance_order;