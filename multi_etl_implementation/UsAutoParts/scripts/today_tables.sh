#!/bin/bash

set -e

rm boomerang_catalog.txt custom_catalog.txt custom_performance.txt global_markup.txt item_cost.txt item_prices.txt performance_fulfillment.txt performance_order.txt sku_relationships.txt part_target_margin.txt part_brand_target_margin.txt channel_markup.txt | true


PGPASSWORD="ins1GHtsUSAP" psql -h insights-usautoparts.cnjeis9czdgg.us-west-2.redshift.amazonaws.com -d insights -U usapmaster -p 5439 <<- EOD

unload('select count(*) from base.boomerang_catalog where feed_date = CURRENT_DATE') 
to 's3://data-platform-insights/usap_alerts/boomerang_catalog/skus/'
credentials 'aws_access_key_id=AKIAJDUJW6ETMIYWGOIA;aws_secret_access_key=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'
null 'null'
delimiter ',' 
escape allowoverwrite parallel off;

unload('select count(*) from usautoparts.custom_catalogs where feed_date = CURRENT_DATE') 
to 's3://data-platform-insights/usap_alerts/custom_catalog/skus/'
credentials 'aws_access_key_id=AKIAJDUJW6ETMIYWGOIA;aws_secret_access_key=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'
null 'null'
delimiter ',' 
escape allowoverwrite parallel off;

unload('select count(*) from usautoparts.custom_performance where feed_date = CURRENT_DATE') 
to 's3://data-platform-insights/usap_alerts/custom_performance/skus/'
credentials 'aws_access_key_id=AKIAJDUJW6ETMIYWGOIA;aws_secret_access_key=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'
null 'null'
delimiter ',' 
escape allowoverwrite parallel off;

unload('select count(*) from usautoparts.global_markup where feed_date = CURRENT_DATE') 
to 's3://data-platform-insights/usap_alerts/global_markup/skus/'
credentials 'aws_access_key_id=AKIAJDUJW6ETMIYWGOIA;aws_secret_access_key=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'
null 'null'
delimiter ',' 
escape allowoverwrite parallel off;

unload('select count(*) from base.item_cost where feed_date = CURRENT_DATE') 
to 's3://data-platform-insights/usap_alerts/item_cost/skus/'
credentials 'aws_access_key_id=AKIAJDUJW6ETMIYWGOIA;aws_secret_access_key=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'
null 'null'
delimiter ',' 
escape allowoverwrite parallel off;

unload('select count(*) from base.item_prices where feed_date = CURRENT_DATE') 
to 's3://data-platform-insights/usap_alerts/item_prices/skus/'
credentials 'aws_access_key_id=AKIAJDUJW6ETMIYWGOIA;aws_secret_access_key=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'
null 'null'
delimiter ',' 
escape allowoverwrite parallel off;

unload('select count(*) from base.performance_fulfillment where feed_date = CURRENT_DATE') 
to 's3://data-platform-insights/usap_alerts/performance_fulfillment/skus/'
credentials 'aws_access_key_id=AKIAJDUJW6ETMIYWGOIA;aws_secret_access_key=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'
null 'null'
delimiter ',' 
escape allowoverwrite parallel off;

unload('select count(*) from base.performance_order where feed_date = CURRENT_DATE') 
to 's3://data-platform-insights/usap_alerts/performance_order/skus/'
credentials 'aws_access_key_id=AKIAJDUJW6ETMIYWGOIA;aws_secret_access_key=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'
null 'null'
delimiter ',' 
escape allowoverwrite parallel off;

unload('select count(*) from base.sku_relationships where feed_date = CURRENT_DATE') 
to 's3://data-platform-insights/usap_alerts/sku_relationships/skus/'
credentials 'aws_access_key_id=AKIAJDUJW6ETMIYWGOIA;aws_secret_access_key=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'
null 'null'
delimiter ',' 
escape allowoverwrite parallel off;

unload('select count(*) from usautoparts.part_target_margin where feed_date = CURRENT_DATE') 
to 's3://data-platform-insights/usap_alerts/part_target_margin/skus/'
credentials 'aws_access_key_id=AKIAJDUJW6ETMIYWGOIA;aws_secret_access_key=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'
null 'null'
delimiter ',' 
escape allowoverwrite parallel off;

unload('select count(*) from usautoparts.part_brand_target_margin where feed_date = CURRENT_DATE') 
to 's3://data-platform-insights/usap_alerts/part_brand_target_margin/skus/'
credentials 'aws_access_key_id=AKIAJDUJW6ETMIYWGOIA;aws_secret_access_key=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'
null 'null'
delimiter ',' 
escape allowoverwrite parallel off;

unload('select count(*) from usautoparts.channel_markup where feed_date = CURRENT_DATE') 
to 's3://data-platform-insights/usap_alerts/channel_markup/skus/'
credentials 'aws_access_key_id=AKIAJDUJW6ETMIYWGOIA;aws_secret_access_key=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'
null 'null'
delimiter ','
escape allowoverwrite parallel off;

\q
EOD

# Extracting files

aws s3 mv s3://data-platform-insights/usap_alerts/boomerang_catalog/skus/000 boomerang_catalog.txt
echo "boomerang_catalog" >> boomerang_catalog.txt 

aws s3 mv s3://data-platform-insights/usap_alerts/custom_catalog/skus/000 custom_catalog.txt
echo "custom_catalog" >> custom_catalog.txt

aws s3 mv s3://data-platform-insights/usap_alerts/custom_performance/skus/000 custom_performance.txt
echo "custom_performance" >> custom_performance.txt

aws s3 mv s3://data-platform-insights/usap_alerts/global_markup/skus/000 global_markup.txt
echo "global_markup" >> global_markup.txt

aws s3 mv s3://data-platform-insights/usap_alerts/item_cost/skus/000 item_cost.txt
echo "item_cost" >> item_cost.txt

aws s3 mv s3://data-platform-insights/usap_alerts/item_prices/skus/000 item_prices.txt
echo "item_prices" >> item_prices.txt

aws s3 mv s3://data-platform-insights/usap_alerts/performance_fulfillment/skus/000 performance_fulfillment.txt
echo "performance_fulfillment" >> performance_fulfillment.txt

aws s3 mv s3://data-platform-insights/usap_alerts/performance_order/skus/000 performance_order.txt
echo "performance_order" >> performance_order.txt

aws s3 mv s3://data-platform-insights/usap_alerts/sku_relationships/skus/000 sku_relationships.txt
echo "sku_relationships" >> sku_relationships.txt

aws s3 mv s3://data-platform-insights/usap_alerts/part_target_margin/skus/000 part_target_margin.txt
echo "part_target_margin" >> part_target_margin.txt

aws s3 mv s3://data-platform-insights/usap_alerts/part_brand_target_margin/skus/000 part_brand_target_margin.txt
echo "part_brand_target_margin" >> part_brand_target_margin.txt

aws s3 mv s3://data-platform-insights/usap_alerts/channel_markup/skus/000 channel_markup.txt
echo "channel_markup" >> channel_markup.txt

# Copying contents to return txt

touch today_tables.txt

cat boomerang_catalog.txt custom_catalog.txt custom_performance.txt global_markup.txt item_cost.txt item_prices.txt performance_fulfillment.txt performance_order.txt sku_relationships.txt part_target_margin.txt part_brand_target_margin.txt channel_markup.txt > today_tables.txt

cat today_tables.txt 

exit