#!/bin/bash

set -e

rm -f *.csv | true

# Check if Target_Margin_Part_Brand file is present for today, else create from previous day's feed

lftp -u usautoparts-sftp-user,U4\!p@ts4Q sftp://sftp.usautoparts.rboomerang.com <<- EOD
	cd /prod/input/target_margin
		mget Target_Margin_Part_Brand.csv
		mget USAP_TARGET_MARGIN_PART_BRAND_$1.csv
		mget USAP_TARGET_MARGIN_PART_BRAND_$2.csv
		
		mget Target_Margin_Part.csv
		mget USAP_TARGET_MARGIN_PART_$1.csv
		mget USAP_TARGET_MARGIN_PART_$2.csv
	cd
	exit
EOD

if [ ! -f USAP_TARGET_MARGIN_PART_BRAND_$1.csv ]; then
    
    if [ ! -f USAP_TARGET_MARGIN_PART_BRAND_$2.csv ]; then
    	mv Target_Margin_Part_Brand.csv USAP_TARGET_MARGIN_PART_BRAND_$1.csv
    else
		mv USAP_TARGET_MARGIN_PART_BRAND_$2.csv USAP_TARGET_MARGIN_PART_BRAND_$1.csv
	fi
	
	lftp -u usautoparts-sftp-user,U4\!p@ts4Q sftp://sftp.usautoparts.rboomerang.com <<- EOD
		cd /prod/test/input/target_margin
			mput USAP_TARGET_MARGIN_PART_BRAND_$1.csv
		cd
		exit
	EOD
fi

# Check if Target_Margin_Part file is present for today, else create from previous day's feed

if [ ! -f USAP_TARGET_MARGIN_PART_$1.csv ]; then
    
    if [ ! -f USAP_TARGET_MARGIN_PART_$2.csv ]; then
    	mv Target_Margin_Part.csv USAP_TARGET_MARGIN_PART_$1.csv
    else
		mv USAP_TARGET_MARGIN_PART_$2.csv USAP_TARGET_MARGIN_PART_$1.csv
	fi
	
	lftp -u usautoparts-sftp-user,U4\!p@ts4Q sftp://sftp.usautoparts.rboomerang.com <<- EOD
		cd /prod/test/input/target_margin
			mput USAP_TARGET_MARGIN_PART_$1.csv
		cd
		exit
	EOD
fi

rm -f *.csv | true

exit