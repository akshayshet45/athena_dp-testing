#!/bin/bash
set -e

cd /home/ubuntu/is-platform/UsAutoParts/scripts/RScripts/
cp {reversion_history.sh,USAP_Generic_Reversion.R} /mnt/R_Execution/
cd /mnt/R_Execution
rm -f reversion_outoutput_file.csv
rm -rf /home/ubuntu/R/x86_64-pc-linux-gnu-library/3.2/00LOCK-stringi
Rscript USAP_Generic_Reversion.R

bash reversion_history.sh

rm -rf USAP_Generic_Reversion.R
rm -rf reversion_history.sh

exit
