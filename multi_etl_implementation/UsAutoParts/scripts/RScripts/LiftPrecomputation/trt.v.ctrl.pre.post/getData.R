library(data.table);
source("utils/BQueryRedshift.R");

getData<-function(weekday.offset,factors.str,msrmts.str,exp_id.str,dates, options) {
  weekly.msrmts=data.table(with(options,BQueryRedshift(sprintf("set wlm_query_slot_count to 3;
select sku,
                                                                   dateadd(day, %d, DATE_TRUNC('w',  transaction_date)) as rptweek,
                                                                   count(distinct transaction_date) as ndates,
                                                                   case
                                                                   when prevspost = 'pre' and tc = 'T' then 'T0'
                                                                   when prevspost = 'pre' and tc = 'C' then 'C0'
                                                                   when prevspost = 'post' and tc = 'T' then 'T'
                                                                   when prevspost = 'post' and tc = 'C' then 'C'
                                                                   end as tc1, prevspost,
                                                                   AVG(offer_price) as offer_price, %s, %s  from base.measurement_dictionary where %s and prevspost is not null
                                                                   and excluded is not TRUE
                                                                   group by sku, rptweek,prevspost, tc, %s",
                                                                   weekday.offset,factors.str,msrmts.str,exp_id.str,factors.str),options=options))); #on_promo is not TRUE and

  setnames(weekly.msrmts, c("tc1","rptweek"), c("tc","rpt.week"));

  return(weekly.msrmts);
}
