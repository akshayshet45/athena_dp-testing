Paste <- function(..., sep='') {
  paste(..., sep=sep)
}

sku.lifts <- function(baseline, sku, msrmts, options) {
    sku_post_t = as.data.frame(sku[tc == 'T'])[,-seq(2)];
    colnames(sku_post_t)=c("sku", Paste("post_t_",msrmts));

    if(!nrow(sku_post_t)) {
        print(sprintf("Number of rows in post period for skus is null for exp_id: %d", options$exp_id));
        return(NULL)
    }

    sku_pre_t = as.data.frame(sku[tc == 'T0'])[,-seq(2)];
    colnames(sku_pre_t)=c("sku",Paste("pre_t_",msrmts));

    sku_pre_c = as.data.frame(sku[tc == 'C0'])[,-seq(2)];
    colnames(sku_pre_c)=c("sku",Paste("pre_c_",msrmts));

    sku_post_c = as.data.frame(sku[tc == 'C'])[,-seq(2)];
    colnames(sku_post_c)=c("sku",Paste("post_c_",msrmts));


    test = merge(sku_post_t, sku_pre_t, by='sku');
    control = merge(sku_post_c, sku_pre_c, by='sku');

    final = merge(test, control, by = "sku");


    for (msrmt in c("gross_revenue","gross_margin","gross_units","unique_visits")){
      final[,Paste(msrmt,"_bsl")]=final[,Paste("pre_t_",msrmt)] * final[,Paste("post_c_",msrmt)] / final[,Paste("pre_c_",msrmt)];
      final[,Paste(msrmt,"_lift")]=final[,Paste("post_t_",msrmt)] - final[,Paste(msrmt,"_bsl")];
      final[,Paste(msrmt,"_lift_perc")]=final[,Paste(msrmt,"_lift")] / final[,Paste(msrmt,"_bsl")];
      final[is.na(final[,Paste(msrmt,"_lift_perc")]),Paste(msrmt,"_lift_perc")]=0;
    }

    return(final)
}