library(tidyr);
source("utils/BQueryRedshift.R");

getData<-function(weekday.offset,factors.str,msrmts.str,exp_id.str,dates, options) {
  
  
  dates$post_date_start_lag=as.character(as.Date(dates$post_date_start)-365-15); #-15
  dates$post_date_end_lag=as.character(as.Date(dates$post_date_end)-365+15);#+15
  
  
  weekly.msrmts = with(options,BQueryRedshift(sprintf("set wlm_query_slot_count to 3; select sku,
                                                      dateadd(day, %d, DATE_TRUNC('w',  transaction_date)) as rptweek,
                                                      count(distinct transaction_date) as ndates,
                                                      case
                                                      when trunc(transaction_date) >= '%s' and trunc(transaction_date) <= '%s' then 'post.yoy'
                                                      when trunc(transaction_date) >= '%s' and trunc(transaction_date) <= '%s' then 'post'
                                                      end as tc1,
                                                      EXTRACT(WEEK FROM transaction_date) as rptwoy,
                                                      avg(offer_price) as offer_price, %s, %s from base.measurement_dictionary where %s and
                                                      and excluded is not TRUE group by sku, rptweek, rptwoy, tc1, %s",
                                                      weekday.offset,
                                                      dates$post_date_start_lag, dates$post_date_end_lag,
                                                      dates$post_date_start, dates$post_date_end,
                                                      factors.str,msrmts.str,exp_id.str,
                                                      factors.str),options=options)); #on_promo is not TRUE 
  
  #>>>>>Temporary
  weekly.msrmts=data.table(weekly.msrmts);
  weekly.msrmts[,sku_merch_name:=paste(sku,merch_l1_name,merch_l2_name,merch_l3_name,sep="_")];
  setkey(weekly.msrmts,sku_merch_name,rptweek);
  sku_merch_names=unique(weekly.msrmts$sku_merch_name);
  rpt.ty.weeks=seq.Date(as.Date(min(weekly.msrmts$rptweek)),as.Date(max(weekly.msrmts$rptweek)),by="week");
  rpt.ly.weeks=as.Date(rpt.ty.weeks)-4*7*12;
  rpt.weeks=c(rpt.ly.weeks,rpt.ty.weeks);
  weekly.msrmts[,sku:=NULL];
  weekly.msrmts[,merch_l1_name:=NULL];
  weekly.msrmts[,merch_l2_name:=NULL];
  weekly.msrmts[,merch_l3_name:=NULL];
  
  weekly.msrmts=weekly.msrmts[CJ(sku_merch_names,rpt.weeks)]%>%
    separate(sku_merch_name,into=c("sku","merch_l1_name","merch_l2_name","merch_l3_name"),sep="_");
  weekly.msrmts[,rptwoy:=as.numeric(format(rptweek,"%W"))];
  weekly.msrmts[,ndates:=7];
  weekly.msrmts[is.na(unique_visits),unique_visits:=0];
  weekly.msrmts[is.na(gross_margin),gross_margin:=0];
  weekly.msrmts[is.na(gross_revenue),gross_revenue:=0];
  weekly.msrmts[is.na(gross_units),gross_units:=0];
  weekly.msrmts[,exp_id:=2];
  weekly.msrmts[,exp_name:='2'];
  weekly.msrmts[,strategy:='2'];
  weekly.msrmts[,tc1:="post"];
  weekly.msrmts[rptweek %in% rpt.ly.weeks,tc1:="post.yoy"];
  
  weekly.msrmts[,merch_l1_id:=as.numeric(factor(weekly.msrmts$merch_l1_name))];
  weekly.msrmts[,merch_l2_id:=as.numeric(factor(weekly.msrmts$merch_l2_name))];
  weekly.msrmts[,merch_l3_id:=as.numeric(factor(weekly.msrmts$merch_l3_name))];
  weekly.msrmts=as.data.frame(weekly.msrmts);
  #<<<<<Temporary
  
  
  # remove unwanted data
  weekly.msrmts = weekly.msrmts[!is.na(weekly.msrmts$tc1), ]
  weekly.msrmts$prevspost = weekly.msrmts$tc1;
  weekly.msrmts$rptweek = as.Date(weekly.msrmts$rptweek);
  
  
  # Ensure rpt.woys in post period exist in the post lagged period
  # Create 53rd week data in last year if it does not exist but exists this year
  
  post.rpt.woys=as.numeric(unique(weekly.msrmts[weekly.msrmts$tc1=='post',]$rptwoy));
  post.lag.rpt.woys=as.numeric(unique(weekly.msrmts[weekly.msrmts$tc1=='post.yoy',]$rptwoy));
  for (rw in post.rpt.woys) {
    if (!is.element(rw, post.lag.rpt.woys) & rw==53){
      tmp=subset(weekly.msrmts,tc1=='post.yoy' & rptwoy==rw-1);
      tmp=tmp%>%mutate(rptwoy=rw);
      weekly.msrmts=rbind(weekly.msrmts,tmp);
    }
  }
  #Have same rpt.woys in pre and post periods this year and last year
  post.weekly.msrmts=subset(weekly.msrmts,tc1=='post');
  post.yoy.weekly.msrmts=subset(weekly.msrmts,tc1=='post.yoy');
  post.yoy.weekly.msrmts=subset(post.yoy.weekly.msrmts,rptwoy %in% unique(post.weekly.msrmts$rptwoy));
  weekly.msrmts=rbind(post.yoy.weekly.msrmts,post.weekly.msrmts);
  
  
  weekly.msrmts=data.table(weekly.msrmts);
  setnames(weekly.msrmts,c("rptweek","rptwoy","tc1"),c("rpt.week","rpt.woy","tc") );
  
  
  return(data.table(weekly.msrmts));
}