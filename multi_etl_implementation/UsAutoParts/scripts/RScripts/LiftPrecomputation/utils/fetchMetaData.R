fetchMetaData<-function(options,exp_id=1){

  exp_id.str=paste("exp_id=",exp_id,sep="");

  #                                    date_trunc('day',post_date_end) as post_date_end

  if (options$exp_type!="yoy"){
    dates=with(options,BQueryRedshift(sprintf("select distinct date_trunc('day',pre_date_start) as pre_date_start,
                                    date_trunc('day',pre_date_end) as pre_date_end,
                                    date_trunc('day',post_date_start) as post_date_start,
                                    date_trunc('day',post_date_end) as post_date_end
                                    from base.measurement_dictionary
                                    where pre_date_start is not NULL and %s",exp_id.str),options=options));
  } else {
    dates=with(options,BQueryRedshift(sprintf("select distinct
                                              date_trunc('day',post_date_start) as post_date_start,
                                              date_trunc('day',post_date_end) as post_date_end
                                              from base.measurement_dictionary where %s",exp_id.str),options=options));
  }

  if (is.null(dates$post_date_end)|is.na(dates$post_date_end)) {
    if (options$exp_type!="yoy"){
      dates$post_date_end = BQueryRedshift(sprintf("select CURRENT_DATE-(select value from base.dd_parameters where element='lag')::INTERVAL as post_date_end") ,options=options);
    } else {
      dates$post_date_end = BQueryRedshift(sprintf("select max(transaction_date) as post_date_end from base.measurement_dictionary where %s",exp_id.str) ,options=options)[1,1];
    }
  }
  else {
          dates$post_date_end = BQueryRedshift(sprintf("select LEAST(max(transaction_date), max(post_date_end)) as post_date_end from base.measurement_dictionary where %s",exp_id.str) ,options=options)[1,1];
      }


  #dates$pre_date_start_lag=as.character(as.Date(dates$pre_date_start)-365-7);
  #dates$pre_date_end_lag=as.character(as.Date(dates$pre_date_end)-365+7);
  #dates$post_date_start_lag=as.character(as.Date(dates$post_date_start)-365-7);
  #dates$post_date_end_lag=as.character(as.Date(dates$post_date_end)-365-7);

  return(dates);
}