
format.top.bottom<-function(tb, pm, lvl){
  #stop();
  original.cols=colnames(tb);
  print(original.cols)
  original.cols=original.cols[original.cols!="rpt.week"];
  print(original.cols)

  #exp_id
  tb$exp_id=pm$exp_id;
  print(pm$exp_id)

  #start and end date
  tb$start_date="";
  tb$end_date="";
  tb[tb$rpt.week!="",]$start_date=tb[tb$rpt.week!="",]$rpt.week;
  tb[tb$rpt.week!="",]$end_date=as.character(as.Date(tb[tb$rpt.week!="",]$rpt.week)+6);
  tb[tb$rpt.week=="",]$start_date=min(tb[tb$rpt.week!="",]$start_date);
  tb[tb$rpt.week=="",]$end_date=max(tb[tb$rpt.week!="",]$end_date);

  #factors
  tb[,pm$all.factors]=NA;
  tb[,names(lvl)]=lvl;

  #level
  tb$level="";
  tb[tb$rpt.week=="",]$level=paste(names(lvl),collapse=",");
  tb[tb$rpt.week!="",]$level=paste(c(names(lvl),"week"),collapse=",");
  tb=tb[,c("start_date","end_date",pm$all.factors,"level",original.cols)]
  tb
}
