
format.summary<-function(smy, pm, lvl, resolution="week"){
  #exp_id
  smy$exp_id=pm$subset$exp_id;

  #start and end date
  smy$start_date="";
  smy$end_date="";
  if (any(smy$rpt.week!="")){
    if (resolution=="week"){
      smy[smy$rpt.week!="",]$start_date=as.character(smy[smy$rpt.week!="",]$rpt.week);
      smy[smy$rpt.week!="",]$end_date=as.character(as.Date(smy[smy$rpt.week!="",]$rpt.week)+6);
    } else if (resolution=="month"){
      smy[smy$rpt.week!="",]$start_date=as.character(as.Date(smy[smy$rpt.week!="",]$rpt.week)-3*7);
      smy[smy$rpt.week!="",]$end_date=as.character(as.Date(smy[smy$rpt.week!="",]$rpt.week)+6);
    } else if (resolution=="quarter"){
      smy[smy$rpt.week!="",]$start_date=as.character(as.Date(smy[smy$rpt.week!="",]$rpt.week)-12*7);
      smy[smy$rpt.week!="",]$end_date=as.character(as.Date(smy[smy$rpt.week!="",]$rpt.week)+6);
    }
    if (any(smy$rpt.week=="")){
      smy[smy$rpt.week=="",]$start_date=min(smy[smy$rpt.week!="",]$start_date);
      smy[smy$rpt.week=="",]$end_date=max(smy[smy$rpt.week!="",]$end_date);
    }
  }

  #factors
  smy[,pm$all.factors]=NA;
  if (length(lvl)==1){
    smy[,names(lvl)]=lvl;
  } else {
    for (k in seq(length(lvl))){
      smy[,names(lvl)[k]]=lvl[k];
      smy[,names(lvl)[k]]=lvl[k];
    }
  }

  #level
  smy$level="";
  smy$level_value="";
  lvl.indices=which(!(names(lvl) %in% c("exp_id")));
  if (any(smy$rpt.week=="")){
    smy[smy$rpt.week=="",]$level=paste(names(lvl),collapse=",");
    smy[smy$rpt.week=="",]$level_value=paste(lvl[lvl.indices],collapse=",");
  }
  if (any(smy$rpt.week!="")){
    smy[smy$rpt.week!="",]$level=paste(c(names(lvl),resolution),collapse=",");
    smy[smy$rpt.week!="",]$level_value=paste(lvl[lvl.indices],collapse=",");
  }

  #Pvaluessmy
  smy[,paste("pval",pm$msrmts,sep="_")]=as.numeric(rep(NA,length(pm$msrmts)));

  for (lift.cols in paste("lift",pm$msrmts,sep="_")){
    if (any(is.infinite(smy[,lift.cols]))){
      rows=is.infinite(smy[,lift.cols]);
      smy[rows,lift.cols]=NA;
    }
  }

  for (perc.cols in paste("prc_lift",pm$msrmts,sep="_")){
    if (any(is.infinite(smy[,perc.cols]))){
      rows=is.infinite(smy[,perc.cols]);
      smy[rows,perc.cols]=NA;
    }
  }


  #Final
  smy=smy[,c("start_date","end_date",pm$all.factors,"level","level_value","nskus",paste("ttl",pm$msrmts,sep="_"),
             paste("pre.ttl",pm$msrmts,sep="_"),paste("bsl.ttl",pm$msrmts,sep="_"),
             paste("lift",pm$msrmts,sep="_"),paste("prc_lift",pm$msrmts,sep="_"),paste("pval",pm$msrmts,sep="_"))];

  colnames(smy)=c("start_date","end_date",pm$all.factors,"level","level_value","no_skus",paste(pm$msrmts,"post",sep="_"),
                  paste(pm$msrmts,"pre",sep="_"),paste(pm$msrmts,"bsl",sep="_"),
                  paste(pm$msrmts,"increment",sep="_"),paste(pm$msrmts,"increment_perc",sep="_"),paste("ss",pm$msrmts,sep="_"));

  smy

}
