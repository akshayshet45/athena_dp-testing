subset.by.time<-function(x,tp,ntp,options){

  time.columns=c("rpt.woy","rpt.week","time.point");


  if (tp!="") {

    rpt.week=as.character(x[time.point==tp]$rpt.week[1]);
    obs=copy(x[time.point==tp]);
    obs[,(time.columns):=NULL];

  } else {

    rpt.week="";
    if (length(grep("pre vs post",options$exp_type))) {

      obs.now.trt=copy(x[,lapply(.SD, na.weighted.mean, w=weights.now.trt),by=c("sku","tc"),.SDcols=colnames(x)[grep("now.trt",colnames(x))]]);
      obs.now.ctl=copy(x[,lapply(.SD, na.weighted.mean, w=weights.now.ctl),by=c("sku","tc"),.SDcols=colnames(x)[grep("now.ctl",colnames(x))]]);
      obs.thn=copy(x[,lapply(.SD, na.mean),by=c("sku","tc"),.SDcols=colnames(x)[grep(".thn.",colnames(x))]]);
      obs=merge(merge(obs.now.trt,obs.now.ctl,by=c("sku","tc"),all.x=TRUE,all.y=FALSE),obs.thn,by=c("sku","tc"),all.x=TRUE,all.y=FALSE);

      weight.columns=c("weights.now.trt","weights.now.ctl");
      if (ntp<=2) {
        obs[,(weight.columns):=1];
      }

    } else {

      obs.now=copy(x[,lapply(.SD, na.weighted.mean, w=weights.now),by=c("sku","tc"),.SDcols=colnames(x)[grep(".now",colnames(x))]]);
      obs.thn=copy(x[,lapply(.SD, na.mean),by=c("sku","tc"),.SDcols=colnames(x)[grep(".thn",colnames(x))]]);
      obs=merge(obs.now,obs.thn,by=c("sku","tc"),all.x=TRUE,all.y=FALSE);

      weight.columns=c("weights.now");
      if (ntp<=2) {
        obs[,(weight.columns):=1];
      }
    }#exp_type

  } #tp=="" or o.w.

  return(list(obs=obs,rpt.week=rpt.week));
}