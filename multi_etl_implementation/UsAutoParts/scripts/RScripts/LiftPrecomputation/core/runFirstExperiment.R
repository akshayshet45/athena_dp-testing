library(tictoc);
library(doParallel);
library(foreach);
source("utils/BQueryRedshift.R");
source("core/estMsrmtEffects.R");

runFirstExperiments <- function(model, options) {
    BQueryRedshift("select * from base.lifts limit 0", options = options);
    BQueryRedshift("select * from base.skulevellifts limit 0", options = options);

    exps = BQueryRedshift("select distinct exp_id, exp_type from base.measurement_dictionary", options = options);

    exps=split(exps,exps$exp_id);
    
    exps$`1` <- NULL
    exps$`2` <- NULL
    exps$`3` <- NULL
    exps$`215` <- NULL
    exps$`216` <- NULL
    exps$`220` <- NULL
    if(options$parallel == TRUE){
        registerDoParallel(cores=2)
        foreach(exp=iter(exps, by='row')) %dopar% computeLifts(exp=exp, options=options);
    } else {
        lapply(exps, computeLifts, options = options)
    }
}

computeLifts <- function(exp, options) {
    print(exp);
    tic();
    options = append(options, list(n=10, rpt.wday="Mon", nclusters=4, exp_type=exp$exp_type, verbose=FALSE, exp_id = exp$exp_id));

    op = list(factors=c("exp_id","exp_name","strategy","channel_id",
                        "merch_l1_id","merch_l2_id","merch_l3_id","merch_l4_id","merch_l5_id",
                        "merch_l1_name","merch_l2_name","merch_l3_name","merch_l4_name","merch_l5_name"));

    estMsrmtEffects(model=model, op=op, options=options);
    toc();
}