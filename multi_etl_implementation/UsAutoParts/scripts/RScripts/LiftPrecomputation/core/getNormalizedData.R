source("utils/fetchMetaData.R");

getNormalizedData<-function(pm,options){

  if (options$exp_type=="pre vs post yoy") {
    source("pre.post.yoy/getData.R");
  } else if (options$exp_type=="pre vs post") {
    source("pre.post/getData.R");
  } else if (options$exp_type=="yoy"){
    source("yoy/getData.R");
  }else if (options$exp_type =="test vs control stores") {
    source("trt.v.ctrl.store.pre.post/getData.R");
  }
   else if (options$exp_type == "test vs control skus"){
    source("trt.v.ctrl.pre.post/getData.R");
  }else
  {
      stop(sprintf("invalid experiment type: %s", options$exp_type))
  }

  dates=fetchMetaData(options=options,exp_id=pm$exp_id);

  #save(dates, file = 'date.Rdata')
  #stop();
  #Creating strings to aid in query
  wday.offset=switch(options$rpt.wday,Mon=0,Tue=1,Wed=2,Thu=3,Fri=4,Sat=5,Sun=6);
  factors.str=paste(pm$all.factors,collapse=", ");
  msrmts.str=with(pm,paste("sum(",msrmts,") as ", msrmts,sep="",collapse=", "));
  exp_id.str=paste("exp_id=",options$exp_id,sep="");

  weekly.msrmts=getData(weekday.offset=wday.offset,factors.str,msrmts.str,exp_id.str,dates, options);


  return(weekly.msrmts);
}
