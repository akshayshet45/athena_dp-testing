source("account.scripts/common.R")
source("core/runAllExperiments.R")

model=formula('gross_revenue+gross_margin+gross_units+unique_visits~exp_id+exp_id:merch_l1_id+exp_id:merch_l1_id:merch_l2_id+exp_id:merch_l1_id:merch_l2_id:merch_l3_id');

args = commandArgs(trailingOnly=TRUE)
if (length(args) != 4){
   print("usage Rscript account.scripts/SPLS.R <host> <username> <password> <db>")
   stop('invalid arguments')
}

options = list(client='sears', hostname = args[1], username = args[2], password = args[3], db = args[4], parallel = FALSE, rpt.wday="Mon")

runAllExperiments(model, options = options);