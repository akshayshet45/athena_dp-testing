Paste <- function(..., sep='') {
  paste(..., sep=sep)
}

sku.lifts <- function(baseline, sku, ndates, msrmts, options) {
  
  
  sku_post_t = as.data.frame(sku[tc == 'post']);
  sku_post_c = as.data.frame(sku[tc =='pre']);
  colnames(sku_post_t)=c("prevspost", "tc", "sku", Paste("post_t_",msrmts)); 
  colnames(sku_post_c)=c("prevspost", "tc", "sku", Paste("post_c_",msrmts)); 
  
  sku_pre_t = as.data.frame(sku[tc == 'post.yoy'])[,-seq(2)];
  sku_pre_c = as.data.frame(sku[tc =='pre.yoy'])[,-seq(2)];
  colnames(sku_pre_t)=c("sku", Paste("pre_t_",msrmts));
  colnames(sku_pre_c)=c("sku", Paste("pre_c_",msrmts));
  
  test = merge(sku_post_t, sku_pre_t, by = 'sku');
  control = merge(sku_post_c, sku_pre_c, by="sku");
  final = merge(test, control, by="sku");
  
  
  for (msrmt in c("gross_revenue","gross_margin","gross_units","unique_visits")){
    
    final[,Paste(msrmt,"_bsl")]=final[,Paste("pre_t_",msrmt)] * final[,Paste("post_c_",msrmt)] / final[,Paste("pre_c_",msrmt)];
    final[,Paste(msrmt,"_lift")]=final[,Paste("post_t_",msrmt)] - final[,Paste(msrmt,"_bsl")];
    final[,Paste(msrmt,"_lift_perc")]=final[,Paste(msrmt,"_lift")] / final[,Paste(msrmt,"_bsl")];
    final[is.na(final[,Paste(msrmt,"_lift_perc")]),Paste(msrmt,"_lift_perc")]=0;
  }
  
  
  return(final)
}