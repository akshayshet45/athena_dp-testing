source("utils/BQueryRedshift.R");

getData<-function(weekday.offset,factors.str,msrmts.str,exp_id.str,dates, options) {

  dates$pre_date_start_lag=as.character(as.Date(dates$pre_date_start)-365); #-15
  dates$pre_date_end_lag=as.character(as.Date(dates$pre_date_end)-365); #+15
  dates$post_date_start_lag=as.character(as.Date(dates$post_date_start)-365); #-15
  dates$post_date_end_lag=as.character(as.Date(dates$post_date_end)-365);#+15
  
  # save(list = ls(all.names = TRUE), file = "fromData.RData");
  # stop();
  # load(file='fromData.RData');

  weekly.msrmts = with(options,BQueryRedshift(sprintf("set wlm_query_slot_count to 3; select sku,
                                       dateadd(day, %d, DATE_TRUNC('w',  transaction_date)) as rptweek,
                                       count(distinct transaction_date) as ndates,
                                       case
                                       when trunc(transaction_date) >= '%s' and trunc(transaction_date) <= '%s' then 'pre.yoy'
                                       when trunc(transaction_date) >= '%s' and trunc(transaction_date) <= '%s' then 'post.yoy'
                                       when trunc(transaction_date) >= '%s' and trunc(transaction_date) <= '%s' then 'pre'
                                       when trunc(transaction_date) >= '%s' and trunc(transaction_date) <= '%s' then 'post'
                                       end as
                                       tc1,
                                       transaction_date,
                                       EXTRACT(WEEK FROM transaction_date) as rptwoy,
                                       avg(offer_price) as offer_price, %s, %s from base.measurement_dictionary where %s and
                                       excluded is not TRUE and case
                                       when trunc(transaction_date) >= '%s' and trunc(transaction_date) <= '%s' then 'pre.yoy'
                                       when trunc(transaction_date) >= '%s' and trunc(transaction_date) <= '%s' then 'post.yoy'
                                       when trunc(transaction_date) >= '%s' and trunc(transaction_date) <= '%s' then 'pre'
                                       when trunc(transaction_date) >= '%s' and trunc(transaction_date) <= '%s' then 'post'
                                       end is not NULL group by sku, transaction_date, tc1, %s",
                                                      weekday.offset,
                                                      dates$pre_date_start_lag, dates$pre_date_end_lag,
                                                      dates$post_date_start_lag, dates$post_date_end_lag,
                                                      dates$pre_date_start, dates$pre_date_end,
                                                      dates$post_date_start, dates$post_date_end,
                                                      factors.str,msrmts.str,exp_id.str,
                                                      dates$pre_date_start_lag, dates$pre_date_end_lag,
                                                      dates$post_date_start_lag, dates$post_date_end_lag,
                                                      dates$pre_date_start, dates$pre_date_end,
                                                      dates$post_date_start, dates$post_date_end,
                                                      factors.str),options=options)); #on_promo is not TRUE and

  # remove unwanted data
  weekly.msrmts = weekly.msrmts[!is.na(weekly.msrmts$tc1), ]
  weekly.msrmts$prevspost = weekly.msrmts$tc1;
  weekly.msrmts$rptweek = as.Date(weekly.msrmts$rptweek);


  # Ensure rpt.woys in post period exist in the post lagged period
  # Create 53rd week data in last year if it does not exist but exists this year

 # post.rpt.woys=as.numeric(unique(weekly.msrmts[weekly.msrmts$tc1=='post',]$rptwoy));
 # post.lag.rpt.woys=as.numeric(unique(weekly.msrmts[weekly.msrmts$tc1=='post.yoy',]$rptwoy));
 # for (rw in post.rpt.woys) {
 #   if (!is.element(rw, post.lag.rpt.woys) & rw==53){
 #     tmp=subset(weekly.msrmts,tc1=='post.yoy' & rptwoy==rw-1);
 #     tmp=tmp%>%mutate(rptwoy=rw);
 #     weekly.msrmts=rbind(weekly.msrmts,tmp);
 #   }
 # }

 #aggregate(weekly.msrmts$gross_revenue, by=list(tc1), FUN = sum, na.rm = TRUE);
  #Have same rpt.woys in pre and post periods this year and last year
  #post.weekly.msrmts=subset(weekly.msrmts,tc1=='post');
  #post.yoy.weekly.msrmts=subset(weekly.msrmts,tc1=='post.yoy');
  #pre.weekly.msrmts=subset(weekly.msrmts,tc1=='pre');
  #pre.yoy.weekly.msrmts=subset(weekly.msrmts,tc1=='pre.yoy');
  #post.yoy.weekly.msrmts=subset(post.yoy.weekly.msrmts,rptwoy %in% unique(post.weekly.msrmts$rptwoy));
  #pre.yoy.weekly.msrmts=subset(pre.yoy.weekly.msrmts,rptwoy %in% unique(pre.weekly.msrmts$rptwoy));
  #weekly.msrmts=rbind(pre.yoy.weekly.msrmts,post.yoy.weekly.msrmts,pre.weekly.msrmts,post.weekly.msrmts);


  #weekly.msrmts=data.table(weekly.msrmts);
  setnames(weekly.msrmts,c("rptweek","rptwoy","tc1"),c("rpt.week","rpt.woy","tc"));

  return(data.table(weekly.msrmts));
}