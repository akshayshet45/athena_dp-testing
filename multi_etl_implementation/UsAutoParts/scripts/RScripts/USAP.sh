#!/bin/bash
set -e

cd /mnt/split_file
rm -rf LiftPrecomputation
cd /home/ubuntu/is-platform/UsAutoParts/scripts/RScripts/
cp -r LiftPrecomputation /mnt/split_file

cd /mnt/split_file/LiftPrecomputation

Rscript account.scripts/USAP.R insights-usautoparts.cnjeis9czdgg.us-west-2.redshift.amazonaws.com dp_write_only udInZkvcl4QqpYz insights


exit