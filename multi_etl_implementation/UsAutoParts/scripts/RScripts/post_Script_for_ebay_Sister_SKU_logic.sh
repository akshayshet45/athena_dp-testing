#!/bin/bash

set -e

utcDate="$(date +'%Y%m%d')"
utcYear="$(date +'%Y')"
utcMonth="$(date +'%m')"
utcDay="$(date +'%d')"

echo "$utcDate"
echo "$utcYear"
echo "$utcMonth"
echo "$utcDay"

export TZ=America/Los_Angeles
date="$(date +'%Y%m%d')"
year="$(date +'%Y')"
month="$(date +'%m')"
day="$(date +'%d')"

cd /home/ubuntu/is-platform/UsAutoParts/scripts/RScripts/
cp .muttrc /mnt/split_file/
cd /mnt/split_file

JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64/jre/

rm -rf *.zip
rm -rf *.csv || echo "no rm required.... good to go"

today=`aws s3 ls s3://po-data/usautoparts-data/pricing-output/$utcYear/$utcMonth/$utcDay/run1/boomerangoutput_priceoutputfile_$utcYear$utcMonth$utcDay.csv  | wc -l`
b=1
a=$today
echo "today : $today"

if [ $a -ge $b ]
then
        aws s3 cp s3://po-data/usautoparts-data/pricing-output/$utcYear/$utcMonth/$utcDay/run1/boomerangoutput_priceoutputfile_$utcYear$utcMonth$utcDay.csv boomerang_priceoutputfile_$utcYear$utcMonth$utcDay.csv
#mv boomerangoutput_priceoutputfile_$utcYear$utcMonth$utcDay.csv boomerang_priceoutputfile1_$date.csv
		aws s3 cp boomerang_priceoutputfile_$utcYear$utcMonth$utcDay.csv s3://etl-backup-all-clients/us-auto-parts/after-po/boomerang_priceoutputfile_$utcYear$utcMonth$utcDay.csv
else
        aws s3 cp s3://po-data/usautoparts-data/pricing-output/$year/$month/$day/run1/boomerangoutput_priceoutputfile_$year$month$day.csv boomerang_priceoutputfile_$year$month$day.csv
#mv boomerangoutput_priceoutputfile_$year$month$day.csv boomerang_priceoutputfile1_$date.csv
		aws s3 cp boomerang_priceoutputfile_$year$month$day.csv s3://etl-backup-all-clients/us-auto-parts/after-po/boomerang_priceoutputfile_$year$month$day.csv
        echo "USAP Yesterdays output file pick" | mutt -s "USAP Yesterdays output file pick" usap-etl-alert@boomerangcommerce.pagerduty.com -F .muttrc
fi

#R < post_Script_for_ebay_Sister_SKU_logic.R --no-save --args $date

#aws s3 cp boomerang_priceoutputfile_$date.csv s3://etl-backup-all-clients/us-auto-parts/after-po/boomerang_priceoutputfile_$date.csv

rm -f *.csv
#rm -f post_Script_for_ebay_Sister_SKU_logic.R
rm -f .muttrc

exit
