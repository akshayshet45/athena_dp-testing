packages <- c("dplyr", "foreach", "doParallel", "data.table", "stringr", "tictoc", "RPostgreSQL")

for(pack in packages) {
    if(!require(pack, character.only = TRUE)){
        install.packages(pack,repos="http://cran.rstudio.com/");
        require(pack, character.only = TRUE);
    }
}

source("core/runAllExperiments.R")
source("core/runFirstExperiment.R")

model=formula('gross_revenue+gross_margin+gross_units+unique_visits~ \n
              exp_id+exp_id:strategy+exp_id:strategy:channel_id+exp_id:strategy:team_name+exp_id:strategy:part_name');

args = commandArgs(trailingOnly=TRUE)
if (length(args) != 4){
   print("usage Rscript account.scripts/USAP.R <host> <username> <password> <db>")
   stop('invalid arguments')
}


options = list(client='USAP', hostname = args[1], username = args[2], password = args[3], db = args[4], parallel = FALSE)


runFirstExperiments(model, options = options);
