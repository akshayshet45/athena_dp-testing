import sys 
import os
import csv 
import pprint
import re
import string

line_count=sys.argv[1]
count=int(line_count)
count=count-3
counter=0

with open('measurement.sql') as f:
	content = f.readlines()
	for s in content:
		counter=1+counter
		if 'character' in s:
            		s="".join(s.split("\n"))
            		s += ' encode lzo'
        	if 'int' in s:
            		s="".join(s.split('\n'))
            		s += ' encode delta32k'
        	if 'numeric' in s:
            		s="".join(s.split('\n'))
            		s += ' encode mostly32'
        	if 'timestamp' in s:
            		s="".join(s.split('\n'))
            		s += ' encode DELTA'
        	if counter <= count and counter >= 3:
            		s += ',\n'
        	else:
            		s += '\n'
        	print s
