#!/bin/bash

set -e

files=`aws s3 ls s3://rboom-sears-apparel-data/usap_dd/ | awk -F ' ' '{print $4}'`

for file in `echo $files | sed 's/\n/ /g'`
do
	echo $file
	aws s3 rm s3://rboom-sears-apparel-data/usap_dd/$file
done

cd /home/ubuntu/is-platform/UsAutoParts/scripts/OP_Scripts
cp {sql_create.py,unload_prod_to_client_dd.sql} /mnt/unload_dd/
cd /mnt/unload_dd/

PGPASSWORD=udInZkvcl4QqpYz psql -h insights-usautoparts.cnjeis9czdgg.us-west-2.redshift.amazonaws.com  -d insights -U dp_write_only -p 5439 -c "UNLOAD ('select * from base.data_dictionary where feed_date=(select max(feed_date) from base.data_dictionary)') to 's3://rboom-sears-apparel-data/usap_dd/dd_' credentials 'aws_access_key_id=AKIAJV7TRIHYZIC45NWQ;aws_secret_access_key=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' ALLOWOVERWRITE"

rm -rf measurement.sql

echo "drop table if exists base.data_dictionary_temp; create table base.data_dictionary_temp(" >> measurement.sql

PGPASSWORD="udInZkvcl4QqpYz" psql -h insights-usautoparts.cnjeis9czdgg.us-west-2.redshift.amazonaws.com  -d insights -U dp_write_only -p 5439 -c "\d base.data_dictionary" | sed "s/|//g" | sed "s/-//g" | sed "s/+//g" | tail -n+3 >> measurement.sql

echo ");" >> measurement.sql

count=`cat measurement.sql | wc -l`

python sql_create.py $count > ab.sql

PGPASSWORD="dNzZ6Lxj85GavUBs" psql -h usap-external.cnjeis9czdgg.us-west-2.redshift.amazonaws.com  -d usap -U defaultuser -p 5439 -a -w -f ab.sql

PGPASSWORD="dNzZ6Lxj85GavUBs" psql -h usap-external.cnjeis9czdgg.us-west-2.redshift.amazonaws.com  -d usap -U defaultuser -p 5439 -c "copy base.data_dictionary_temp from 's3://rboom-sears-apparel-data/usap_dd/' credentials 'aws_access_key_id=AKIAJV7TRIHYZIC45NWQ;aws_secret_access_key=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' delimiter '|' null 'NULL';"

PGPASSWORD="dNzZ6Lxj85GavUBs" psql -h usap-external.cnjeis9czdgg.us-west-2.redshift.amazonaws.com  -d usap -U defaultuser -p 5439 -c "delete from base.data_dictionary where feed_date in (select max(feed_date) from base.data_dictionary_temp);"

PGPASSWORD="dNzZ6Lxj85GavUBs" psql -h usap-external.cnjeis9czdgg.us-west-2.redshift.amazonaws.com  -d usap -U defaultuser -p 5439 -c "delete from base.data_dictionary where feed_date < current_date - 15;"

PGPASSWORD="dNzZ6Lxj85GavUBs" psql -h usap-external.cnjeis9czdgg.us-west-2.redshift.amazonaws.com  -d usap -U defaultuser -p 5439 -c "insert into base.data_dictionary select * from base.data_dictionary_temp;"

PGPASSWORD="dNzZ6Lxj85GavUBs" psql -h usap-external.cnjeis9czdgg.us-west-2.redshift.amazonaws.com  -d usap -U defaultuser -p 5439 -c "drop table base.data_dictionary_temp;"


rm ab.sql measurement.sql

rm -rf sql_create.py
rm -rf unload_prod_to_client_dd.sql



exit
