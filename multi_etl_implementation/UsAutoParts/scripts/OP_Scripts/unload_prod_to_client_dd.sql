\set ON_ERROR_STOP 1

delete from base.data_dictionary where feed_date in (select max(feed_date) from base.data_dictionary_temp);

delete from base.data_dictionary where feed_date < current_date - 15;

insert into base.data_dictionary select * from base.data_dictionary_temp;

drop table base.data_dictionary_temp;
