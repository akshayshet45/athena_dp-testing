#!/bin/bash

set -e

rm -f *.txt.gz *.txt | true

export TZ=America/Los_Angeles
curr_date="$(date +'%Y%m%d')"
#DATE="$(date +'%Y%m%d')"
#curr_date=`/bin/date --date="$DATE -1 day" +%Y%m%d`

cd /home/ubuntu/is-platform/UsAutoParts/scripts
cp {ProductUrls_POST.jar,ProductUrls_PRE.jar} /mnt/usap_scripts/
cd /mnt/usap_scripts

lftp -u usautoparts-sftp-user,U4\!p@ts4Q sftp://sftp.usautoparts.rboomerang.com <<- EOD
	cd /prod/input/product_urls
		mget USAP_PRODUCT_URLS_$curr_date.txt.gz
		lcd
	cd
exit

exit

EOD

if [ ! -f USAP_PRODUCT_URLS_$curr_date.txt.gz ]; then
    echo "Feed Missing"
else
	gzip -d USAP_PRODUCT_URLS_$curr_date.txt.gz
	java -d64 -Xms512m -Xmx4g -jar ProductUrls_PRE.jar USAP_PRODUCT_URLS_$curr_date.txt
	sort -k 5,5 -k 1,1 -k 3,3 --field-separator='|' ProductUrls_Processed.txt > temp.txt && rm ProductUrls_Processed.txt && mv temp.txt ProductUrls_Processed.txt
	java -d64 -Xms512m -Xmx4g -jar ProductUrls_POST.jar ProductUrls_Processed.txt
	mv USAP_PRODUCT_URLS.txt USAP_PRODUCT_URLS_$curr_date.txt
	aws s3 cp USAP_PRODUCT_URLS_$curr_date.txt s3://etl-backup-all-clients/us-auto-parts/product_urls/
	rm *.txt
	rm *.jar
fi

exit
