#!/bin/bash

set -e
cd /home/ubuntu/is-platform/UsAutoParts/scripts/MIssing30percent
rm -f *.txt.gz
rm -f *.txt
cp {30perChange.py,FileCheck30perChange.jar,SendMail.jar,SendMailJar.jar} /mnt/usap_scripts/
cd /mnt/usap_scripts

# A slicker error handling routine

# I put a variable in my scripts named PROGNAME which
# holds the name of the program being run.  You can get this
# value from the first item on the command line ($0).

PROGNAME=$(basename $0)

function skip_command()
{

#       ----------------------------------------------------------------
#       Function for exit due to fatal program error
#               Accepts 1 argument:
#                       string containing descriptive error message
#       ----------------------------------------------------------------


        echo -e  "$1 skipping"
}

#rm status_file.log || skip_command "rm status_file.log"
#touch status_file.log

function error_exit()
{

#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------


	echo "$1" >> status_file.log
}

# Example call of the error_exit function.  Note the inclusion
# of the LINENO environment variable.  It contains the current
# line number.
export TZ=America/Los_Angeles
data_param="$(date +'%Y%m%d')"
#DATE="$(date +'%Y%m%d')"
#data_param=`/bin/date --date="$DATE -1 day" +%Y%m%d`

aws s3 cp s3://etl-backup-all-clients/usap_test/product_urls/filehistoryLog/filehistory.log.old  filehistory.log.old  

echo "[INFO] channel item master test start"
zip_name="USAP_CHANNEL_ITEM_MASTER_$data_param.txt.gz"
unzip_name="USAP_CHANNEL_ITEM_MASTER_$data_param.txt"
ftp_dir="/prod/input/channel_item_master"
name="USAP_CHANNEL_ITEM_MASTER"

lftp -u usautoparts-sftp-user,U4\!p@ts4Q sftp://sftp.usautoparts.rboomerang.com -e "cd $ftp_dir; get $zip_name; bye;" || error_exit "$zip_name not found"

if [ ! -f $zip_name ]; then
	echo "[WARN] File not found!"
	echo "`date +%Y%m%d`,$name,0">>filehistory.log
else
	gzip -d $zip_name
	count=`cat $unzip_name | wc -l`
	echo "`date +%Y%m%d`,$name,$count">>filehistory.log
	echo "[INFO] File found!"
fi

echo "[INFO] channel item master test stop"

echo "[INFO] item master test start"
zip_name="USAP_ITEM_MASTER_$data_param.txt.gz"
unzip_name="USAP_ITEM_MASTER_$data_param.txt"
ftp_dir="/prod/input/item_master"
name="USAP_ITEM_MASTER"
lftp -u usautoparts-sftp-user,U4\!p@ts4Q sftp://sftp.usautoparts.rboomerang.com -e "cd $ftp_dir; get $zip_name; bye;" || error_exit "$zip_name not found"

if [ ! -f $zip_name ]; then
        echo "[WARN] File not found!"
	echo "`date +%Y%m%d`,$name,0">>filehistory.log
else
        gzip -d $zip_name
        count=`cat $unzip_name | wc -l`
        echo "`date +%Y%m%d`,$name,$count">>filehistory.log
        echo "[INFO] File found!"
fi

echo "[INFO] item master test stop"


echo "[INFO] fact_performance test start"
zip_name="USAP_FACT_PERFORMANCE_$data_param.txt.gz"
unzip_name="USAP_FACT_PERFORMANCE_$data_param.txt"
ftp_dir="/prod/input/fact_performance"
name="USAP_FACT_PERFORMANCE"
lftp -u usautoparts-sftp-user,U4\!p@ts4Q sftp://sftp.usautoparts.rboomerang.com -e "cd $ftp_dir; get $zip_name; bye;" || error_exit "$zip_name not found"

if [ ! -f $zip_name ]; then
        echo "[WARN] File not found!"
	echo "`date +%Y%m%d`,$name,0">>filehistory.log
else
        gzip -d $zip_name
        count=`cat $unzip_name | wc -l`
        echo "`date +%Y%m%d`,$name,$count">>filehistory.log
        echo "[INFO] File found!"
fi

echo "[INFO] fact_performance test stop"

echo "[INFO] fact_returns test start"
zip_name="USAP_FACT_RETURNS_$data_param.txt.gz"
unzip_name="USAP_FACT_RETURNS_$data_param.txt"
ftp_dir="/prod/input/fact_returns"
name="USAP_FACT_RETURNS"
lftp -u usautoparts-sftp-user,U4\!p@ts4Q sftp://sftp.usautoparts.rboomerang.com -e "cd $ftp_dir; get $zip_name; bye;" || error_exit "$zip_name not found"

if [ ! -f $zip_name ]; then
        echo "[WARN] File not found!"
	echo "`date +%Y%m%d`,$name,0">>filehistory.log
else
        gzip -d $zip_name
        count=`cat $unzip_name | wc -l`
        echo "`date +%Y%m%d`,$name,$count">>filehistory.log
        echo "[INFO] File found!"
fi

echo "[INFO] fact_returns test stop"

echo "[INFO] markup test start"
zip_name="USAP_MARKUP_$data_param.txt.gz"
unzip_name="USAP_MARKUP_$data_param.txt"
ftp_dir="/prod/input/markup"
name="USAP_MARKUP"
lftp -u usautoparts-sftp-user,U4\!p@ts4Q sftp://sftp.usautoparts.rboomerang.com -e "cd $ftp_dir; get $zip_name; bye;" || error_exit "$zip_name not found"

if [ ! -f $zip_name ]; then
	echo "`date +%Y%m%d`,$name,0">>filehistory.log
        echo "[WARN] File not found!"
else
        gzip -d $zip_name
        count=`cat $unzip_name | wc -l`
        echo "`date +%Y%m%d`,$name,$count">>filehistory.log
        echo "[INFO] File found!"
fi

echo "[INFO] markup test stop"

echo "[INFO] sister_sku test start"
zip_name="USAP_SISTER_SKU_$data_param.txt.gz"
unzip_name="USAP_SISTER_SKU_$data_param.txt"
ftp_dir="/prod/input/sister_sku"
name="USAP_SISTER_SKU"
lftp -u usautoparts-sftp-user,U4\!p@ts4Q sftp://sftp.usautoparts.rboomerang.com -e "cd $ftp_dir; get $zip_name; bye;" || error_exit "$zip_name not found"

if [ ! -f $zip_name ]; then
	echo "`date +%Y%m%d`,$name,0">>filehistory.log
        echo "[WARN] File not found!"
else
        gzip -d $zip_name
        count=`cat $unzip_name | wc -l`
        echo "`date +%Y%m%d`,$name,$count">>filehistory.log
        echo "[INFO] File found!"
fi

echo "[INFO] sister_sku test stop"

rm -f *.txt.gz
rm -f *.txt

python3 30perChange.py "usautoparts-alerts@boomerangcommerce.com" "USAP [Warning] Row count in files changed by more than 30% on `date +%Y%m%d`"

rm filehistory.log.old
mv filehistory.log filehistory.log.old
aws s3 cp filehistory.log.old  s3://etl-backup-all-clients/usap_test/product_urls/filehistoryLog/filehistory.log.old


if [ ! -f status_file.log ]; then
	counter=0
else
	counter=`cat status_file.log | wc -l`
	status_file=`cat status_file.log`
fi

if [ $counter -eq "0" ];then
	java -jar SendMail.jar "usautoparts-alerts@boomerangcommerce.com" "USAP Feed Status - $data_param" "Dear Customer, \n All feeds are present."
else
	java -jar SendMail.jar "usautoparts-alerts@boomerangcommerce.com" "USAP Feed Status - $data_param" "Dear Customer, \n Following feeds are missing. \n\n $status_file."
fi

rm filehistory.log.old
rm *.jar
rm *.py
