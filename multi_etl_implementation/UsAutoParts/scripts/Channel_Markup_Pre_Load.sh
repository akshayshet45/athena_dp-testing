#!/bin/bash

set -e

rm -f *.csv | true

# Check if Channel_Markup file is present for today, else create from previous day's feed

lftp -u usautoparts-sftp-user,U4\!p@ts4Q sftp://sftp.usautoparts.rboomerang.com <<- EOD
	cd /prod/input/markup
		mget global_markup.csv
		mget USAP_CHANNEL_MARKUP_$1.csv
		mget USAP_CHANNEL_MARKUP_$2.csv
	cd
	exit
EOD

if [ ! -f USAP_CHANNEL_MARKUP_$1.csv ]; then
    
    if [ ! -f USAP_CHANNEL_MARKUP_$2.csv ]; then
    	mv global_markup.csv USAP_CHANNEL_MARKUP_$1.csv
    else
		mv USAP_CHANNEL_MARKUP_$2.csv USAP_CHANNEL_MARKUP_$1.csv
	fi
	
	lftp -u usautoparts-sftp-user,U4\!p@ts4Q sftp://sftp.usautoparts.rboomerang.com <<- EOD
		cd /prod/test/input/markup
			mput USAP_CHANNEL_MARKUP_$1.csv
		cd
		exit
	EOD
fi

rm -f *.csv | true

exit