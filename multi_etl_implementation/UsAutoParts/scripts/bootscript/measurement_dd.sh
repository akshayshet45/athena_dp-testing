#!/bin/bash

set -e

project_name="usap-bootstrap"
success_emails="ops@boomerangcommerce.com,chandru@boomerangcommerce.com"
failure_emails="usap-measurement-dd@boomerangcommerce.pagerduty.com"
skip_validation_queries="true"
azkaban_username=azkaban
azkaban_passwd=azkaban

login_response=$(curl -k -X POST --data "action=login&username=$azkaban_username&password=$azkaban_passwd" http://ec2-54-90-127-6.compute-1.amazonaws.com:8081)

echo $login_response

status=$(echo $login_response | jq '."status"')
status=$(echo ${status:1:$(echo ${#status}) - 2})

echo $status

if [ "$status" == "success" ]
then
        echo "Azkaban login successfull."
else
		echo "======> ERROR in azkaban login. Check user/passwd or azkaban_host."
        exit 1
fi

session=$(echo $login_response | jq '."session.id"')
session=$(echo ${session:1:$(echo ${#session}) - 2})
echo "session : $session"

curl -k -X POST --data "session.id=$session&name=$project_name&description=$project_name" http://ec2-54-90-127-6.compute-1.amazonaws.com:8081/manager?action=create

echo "uploading project : $project_name"
curl -k -i -H "Content-Type: multipart/mixed" -X POST --form "session.id=$session" --form 'ajax=upload' --form "file=@$zip_file;type=application/zip" --form "project=$project_name" http://ec2-54-90-127-6.compute-1.amazonaws.com:8081/manager

echo "uploaded project successfully"

flow_response=$(curl -k --get --data "session.id=$session&ajax=fetchprojectflows&project=$project_name" http://ec2-54-90-127-6.compute-1.amazonaws.com:8081/manager)

echo "Flow response : $flow_response"

num_flows=$(echo $flow_response | jq ".flows | length")

if [ $num_flows -ne 1 ]
then
    echo "======> ERROR : Wrong number of flows in project [$project_name] = $num_flows. Number of flows should be exactly 1. Something seems broken in ETL config. Check etl-spec.json."
    exit 1
fi

flow=$(echo $flow_response | jq ".flows[0] | .flowId")
flow=$(echo ${flow:1:$(echo ${#flow}) - 2})

if [ "$flow" == "" ]
then
    echo "======> ERROR in fetching flow of project [$project_name] from azkaban. Flow name is empty, please check if azkaban project upload was successfully done."
    exit 1
else
	echo "project flow = $flow"
fi

echo "executing project : $project_name"
execid=$(curl -k --get --data "session.id=$session" --data 'ajax=executeFlow' --data "project=$project_name" --data "flow=$flow" --data "successEmails=$success_emails" --data "failureEmails=$failure_emails" --data "notifyFailureFirst=true" --data "failureAction=finishPossible" --data "concurrentOption=ignore" --data "successEmailsOverride=true" --data "failureEmailsOverride=true" http://ec2-54-90-127-6.compute-1.amazonaws.com:8081/executor | jq ".execid")

if [ "$execid" == "" ]
then
    echo "======> ERROR while starting project [$project_name] on azkaban."
    exit 1
else
	echo "\nWorkflow [$flow] of project [$project_name] started successfully."
	echo "azkaban execution id : $execid"
fi

ret=$(echo $?)
if [ $ret -ne 0 ]
then
        echo "======> ERROR : ETL start failed"
        exit 1
else
        echo "ETL start successfull"
fi
