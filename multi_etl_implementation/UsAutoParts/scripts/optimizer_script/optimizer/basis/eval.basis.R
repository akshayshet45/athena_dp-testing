eval.basis<-function(x,fm) {
  
  
  N=length(x);
  p=length(fm$knots);
  d=matrix(NA, N, p);
  for (i in seq(p)) {
    d[,i]=abs(x-fm$knots[i]);
  }
  basis=switch(fm$subtype,
               gaussian=exp(-d^2/(2*fm$beta^2)),
               mquad=sqrt(d^2+fm$beta^2),
               imquad=1/sqrt(d^2+fm$beta^2))-0.247;
  
  
  return(basis);
  
}