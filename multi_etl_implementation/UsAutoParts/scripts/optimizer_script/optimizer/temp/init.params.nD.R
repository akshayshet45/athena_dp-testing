init.params<-function(options,nskus){
  
  if (options$fit$form=="seasonal") {
    I.fixed=c(1,1,1);
    I.random=rep(rep(1,1+options$fit$nbasis+1),nskus);
  } else if (options$fit$form=="linear") {
    I.fixed=c(1,1);
    I.random=rep(c(1,1),nskus);
  } else {
    I.fixed=c(1,1);
    I.random=rep(rep(1,1+options$fit$nbasis),nskus); #1+
  }
  
  states=list(g0=c(options$fit$g01*I.fixed,options$fit$g02*I.random),
              g=c(options$fit$g01/options$fit$scale1*I.fixed,options$fit$g02/options$fit$scale2*I.random));
  
  states$mt.mixed=rep(0,length(states$g0));
  states$Ct.mixed=1*diag(states$g0);
  states$sigma2=1;
  states$Nt=0;
  
  return(states);
}