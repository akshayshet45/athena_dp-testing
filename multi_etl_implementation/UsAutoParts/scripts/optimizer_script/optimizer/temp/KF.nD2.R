library(matrixcalc);
source("basis/eval.basis.R");
source("core/UD.R");
source("core/thornton.R");
source("core/bierman.R");

KF<-function(matobj,state,from,to,all.options){
  
  nskus=nrow(matobj$log_units);
  Nt=state$params$Nt;
  mt.mixed=state$params$mt.mixed;
  Ct.mixed=state$params$Ct.mixed;
  ud.Ct.mixed=UD(Ct.mixed);
  sigma2=state$params$sigma2;
  g=state$params$g;
  
  I=diag(nskus);
  G=diag(g);
  Ig=diag(length(g));
  yts=matrix(NA,nrow=nskus,ncol=to-from+1);
  ythats=matrix(NA,nrow=nskus,ncol=to-from+1);
  mts=matrix(NA,nrow=length(mt.mixed),ncol=to-from+1);
  Cts=matrix(NA,nrow=length(mt.mixed),ncol=to-from+1);
  sigma2s=rep(NA,to-from+1);
  for (j in seq(from,to)){ 
    print(j)
    yt=matobj$log_units[,j];
    prc=matobj$prices[,j];
    ssn=matobj$ssn[,j];
    
    if (all.options$fit$form=="seasonal"){
      Ft.fixed=cbind(1,prc,ssn);
      Ft.random=as.matrix(Reduce("bdiag",lapply(prc,function(x) cbind(1,eval.basis(x,state$bsobj),ssn[1]))));
    } else if (all.options$fit$form=="linear") {
      Ft.fixed=cbind(1,prc);
      Ft.random=as.matrix(Reduce("bdiag",lapply(prc,function(x) cbind(1,x)))); #cbind(1,)
    } else {
      Ft.fixed=cbind(1,prc);
      Ft.random=as.matrix(Reduce("bdiag",lapply(prc,function(x) cbind(1,eval.basis(x,state$bsobj))))); #cbind(1,)
    }
    Ft=cbind(Ft.fixed,Ft.random);
    Wt.mixed.D=sigma2*G; Wt.mixed.U=Ig;
    tic();
    thorn = thornton(x_post=mt.mixed,U_post=ud.Ct.mixed$U,D_post=ud.Ct.mixed$D,Uq=Wt.mixed.U,Dq=Wt.mixed.D);
    # thorn = thornton(xin=mt.mixed,Uin=ud.Ct.mixed$U,Din=ud.Ct.mixed$D,Gin=Wt.mixed.U,Q=Wt.mixed.D);
    toc();
#     Ag=cbind(ud.Ct.mixed$U, Wt.mixed.U);
#     Dw=bdiag(ud.Ct.mixed$D, Wt.mixed.D);
#     wgs(Ag,diag(Dw))
#     

    tic();
    for (k in seq(nskus)) {
      bier=bierman(z=yt[k],R=sigma2,H=Ft[k,],x_prior=thorn$x_prior,U_prior=thorn$PU_prior,D_prior=thorn$PD_prior);
      thorn$x_prior=bier$x_post;
      thorn$PU_prior=bier$U_post;
      thorn$PD_prior=bier$D_post;
    }
    toc();
    # sigma2=as.numeric(Ntold*sigma2old/Nt+t(et)%*%iQt%*%et/Nt);
    mt.mixed=bier$x_post;
    ythat=Ft%*%mt.mixed;
    ud.Ct.mixed$U=bier$U_post;
    ud.Ct.mixed$D=bier$D_post;
    Ct.mixed=ud.Ct.mixed$U%*%ud.Ct.mixed$D%*%t(ud.Ct.mixed$U);
    # Ct.mixed=(Ct.mixed+t(Ct.mixed))/2;
#     if (!is.positive.definite(Ct.mixed)){
#       Ct.mixed=as.matrix(nearPD(Ct.mixed)$mat);
#     }

    yts[,j-from+1]=yt;
    ythats[,j-from+1]=ythat;
    mts[,j-from+1]=mt.mixed;
    Cts[,j-from+1]=diag(Ct.mixed);
    sigma2s[j-from+1]=sigma2;
    
  }#j
  
  
  state$params=list(mt.mixed=mt.mixed,Ct.mixed=Ct.mixed,g=g,sigma2=sigma2,Nt=Nt);
  
  state$yts=cbind(state$yts,yts);
  state$ythats=cbind(state$ythats,ythats);
  state$mts=cbind(state$mts,mts);
  # print(dim(state$Cts));
  # print(dim(Cts));
  state$Cts=cbind(state$Cts,Cts);
  state$sigma2s=c(state$sigma2s,sigma2s);
  
  return(state);
}
