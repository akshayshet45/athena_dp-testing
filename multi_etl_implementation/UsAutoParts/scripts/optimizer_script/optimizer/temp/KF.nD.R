source("basis/eval.basis.R");

KF<-function(matobj,state,from,to,all.options){
  
  nskus=nrow(matobj$log_units);
  Nt=state$params$Nt;
  mt.mixed=state$params$mt.mixed;
  Ct.mixed=state$params$Ct.mixed;
  sigma2=state$params$sigma2;
  g=state$params$g;
  
  
  yts=matrix(NA,nrow=nskus,ncol=to-from+1);
  ythats=matrix(NA,nrow=nskus,ncol=to-from+1);
  mts=matrix(NA,nrow=length(mt.mixed),ncol=to-from+1);
  Cts=matrix(NA,nrow=length(mt.mixed),ncol=to-from+1);
  sigma2s=rep(NA,to-from+1);
  for (j in seq(from,to)){ 
    yt=matobj$log_units[,j];
    prc=matobj$prices[,j];
    ssn=matobj$ssn[,j];
    
    if (all.options$fit$form=="seasonal"){
      Ft.fixed=cbind(1,prc,ssn);
      Ft.random=as.matrix(Reduce("bdiag",lapply(prc,function(x) cbind(1,eval.basis(x,state$bsobj),ssn[1]))));
    } else if (all.options$fit$form=="linear") {
      Ft.fixed=cbind(1,prc);
      Ft.random=as.matrix(Reduce("bdiag",lapply(prc,function(x) cbind(1,x)))); #cbind(1,)
    } else {
      Ft.fixed=cbind(1,prc);
      Ft.random=as.matrix(Reduce("bdiag",lapply(prc,function(x) cbind(1,eval.basis(x,state$bsobj))))); #cbind(1,)
    }

    Ft=cbind(Ft.fixed,Ft.random);
    W.mixed=sigma2*diag(g);
    Rt=Ct.mixed+W.mixed;
    
    Qt=Ft%*%Rt%*%t(Ft)+sigma2*diag(nskus);
    ythat=Ft%*%mt.mixed;
    et=yt-ythat;
    iQt=try(solve(Qt));
    if (class(iQt)=="try-error"){
      Qt=as.matrix(nearPD(Qt)$mat);
      iQt=solve(Qt);
    }
    At=Rt%*%t(Ft)%*%iQt;
    
    Ntold=Nt;
    Nt=Ntold+nskus;
    sigma2old=sigma2;
    # sigma2=as.numeric(Ntold*sigma2old/Nt+t(et)%*%iQt%*%et/Nt);
    
    mt.mixed=mt.mixed+At%*%et;
    Ct.mixed=Rt-At%*%Qt%*%t(At);
    
    yts[,j-from+1]=yt;
    ythats[,j-from+1]=ythat;
    mts[,j-from+1]=mt.mixed;
    Cts[,j-from+1]=diag(Ct.mixed);
    sigma2s[j-from+1]=sigma2;
    
  }#j
  
  
  state$params=list(mt.mixed=mt.mixed,Ct.mixed=Ct.mixed,g=g,sigma2=sigma2,Nt=Nt);
  
  state$yts=cbind(state$yts,yts);
  state$ythats=cbind(state$ythats,ythats);
  state$mts=cbind(state$mts,mts);
  # print(dim(state$Cts));
  # print(dim(Cts));
  state$Cts=cbind(state$Cts,Cts);
  state$sigma2s=c(state$sigma2s,sigma2s);
  
  return(state);
}
