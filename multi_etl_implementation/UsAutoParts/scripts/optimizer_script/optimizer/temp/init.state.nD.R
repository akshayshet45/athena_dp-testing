source("core/init.params.nD.R");
source("basis/create.basis.R");
source("core/seasonal.basis.R");
source("basis/get.price.range.R");

init.state<-function(sales.df,start.date,stop.date,options){
  
  state=list();
  state$last.obs.date=start.date;
  state$first.obs.date=stop.date;
  state$prc.range=get.price.range(sales.df$price,padding=0.15);
  state$prc.grid=with(state,seq(prc.range[1],prc.range[2],length.out=100));
  state$bsobj=create.basis(rangeval=state$prc.range,nbasis=options$fit$nbasis,
                           beta=diff(state$prc.range)/(options$fit$nbasis-1)/2);
  state$basis=eval.basis(state$prc.grid,state$bsobj);
  state$ssnbasis=seasonal.basis(sales.df);
  
  
  state$skus=unique(sales.df$sku);
  nskus=length(state$skus);
  state$params=init.params(options,nskus);
  
  
  state$yts=NULL;
  state$ythats=NULL;
  state$mts=state$params$mt.mixed;
  state$Cts=diag(state$params$Ct.mixed);
  state$sigma2s=1;
  
  return(state);
}