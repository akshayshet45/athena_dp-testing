start <- function(account) {
  
  ###### Make changes only to this part #####
  configs = list(
    list(
      exp_id = 1,
      objective = "margin",
      explore = 4*7,
      constraints=list(min_margin="min_margin", max_price_change=0.20, maintain="revenue"),
      account=account),
  )
  ###### Make changes only to this part #####
  
  for(config in configs){
    print(sprintf("Experiment: %d",config$exp_id));
    optimize(options=config);
    print("==============")
    # plot.performance(options=config,type="dept");
  }
  
}