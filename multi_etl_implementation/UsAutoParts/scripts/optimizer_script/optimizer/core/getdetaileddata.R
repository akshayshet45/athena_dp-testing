source("utils/queryRedshift.R");

getdetaileddata<-function(opt,dates,epsilon=0.1) {
  
  exp_id=opt$exp_id;
  from=dates$pre.start.date;
  to=dates$stop.date;
  #Do not put this filter mdd.gross_units is not NULL otherwise we may get skus with no prices
  query.str=sprintf("select  mdd.merch_l3_name dept, (clustdata.cluster||'_'||mdd.sku) as clusterxsku, trunc(mdd.transaction_date) as date, 
                     avg(mdd.offer_price) as price, avg(mdd.vendor_subsidized_po_cost) as cost, avg(mdd.gross_units) as units,
                     avg(mdd.page_views) as page_views, avg(mdd.gross_orders) as gross_orders, avg(unique_visits) as unique_visits, 
                     avg(mdd.min_margin) as min_margin
                     from base.measurement_dictionary mdd, measurement.optimizer_clusters clustdata where
                     clustdata.exp_id=%d and 
                     clustdata.sku=mdd.sku and 
                     mdd.tc='T' and 
                     trunc(mdd.transaction_date)>='%s' and 
                     trunc(mdd.transaction_date)<='%s' 
                    group by clustdata.cluster, mdd.merch_l3_name, mdd.sku, mdd.transaction_date",
                    exp_id, from, to);#, base.min_margin mm  #clustdata.sku=mm.sku and
  sales.by.cluster=data.table(queryRedshift(query.str,options=opt));
  
  #Safety
  sales.by.cluster[cost==0,cost:=NA];
  sales.by.cluster[price==0,price:=NA];
  sales.by.cluster[is.na(units),units:=NA];
  setkey(sales.by.cluster,clusterxsku,date);
  
  
  #Get all dates
  date.range=seq.Date(as.Date(from),as.Date(to),by="day");
  
  #Get all skus
  query.str=sprintf("select (cluster||'_'||sku) as clusterxsku from measurement.optimizer_clusters where exp_id=%d",exp_id);
  clusterxskus=data.table(queryRedshift(query.str,options=opt));
  clusterxskus=unique(clusterxskus$clusterxsku);
  
  #Expand sales.by.cluster for all dates and SKUs
  sales.by.cluster=sales.by.cluster[CJ(clusterxskus,date.range)]%>%
    mutate(cluster.and.sku=clusterxsku)%>%
    separate(clusterxsku,into=c("cluster","sku"));
  
  n1=length(unique(sales.by.cluster$sku))*length(unique(sales.by.cluster$date));
  n2=nrow(sales.by.cluster);
  if (n2>n1){
    print(paste(n2,n1));
    stop("Duplicates in sales");
  }
  
  
  sales.by.cluster=copy(sales.by.cluster);
  sales.by.cluster[is.na(units),units:=0];
  sales.by.cluster[,log_units:=log(units+epsilon)];
  sales.by.cluster[is.na(log_units),log_units:=0];
  sales.by.cluster[,dow:=factor(format(date,"%w"))];
  sales.by.cluster[,cluster:=as.numeric(cluster)];
  sales.by.cluster[,sku:=factor(sku)];
  
  sales.by.cluster=sales.by.cluster[order(cluster,sku,date)];
  
  #Exclude skus with no prices
  prices.skus=as.data.frame(sales.by.cluster[,list(price=mean(price,na.rm=TRUE)),by="sku"]);
  good.skus=prices.skus$sku[which(is.finite(prices.skus$price))];
  sales.by.cluster=sales.by.cluster[sku %in% good.skus];
  
  #Fill forward price and cost  
  variables=c("price","cost","min_margin");
  sales.by.cluster[,(variables):=lapply(.SD,na.locf),.SDcols=variables,by="cluster.and.sku"];
  sales.by.cluster[,(variables):=lapply(.SD,na.locf,fromLast=TRUE),.SDcols=variables,by="cluster.and.sku"];
  avg.min.margin=sales.by.cluster[,list(avg_min_margin=mean(min_margin,na.rm=TRUE)),by="cluster"];
  sales.by.cluster=merge(sales.by.cluster,avg.min.margin,by="cluster");
  sales.by.cluster[is.na(min_margin),min_margin:=avg_min_margin];
  
  #Price in pre
  pre.sales.by.cluster=sales.by.cluster[date==(as.Date(dates$post.start.date)-2)]%>%
    mutate(pre.price=price)%>%
    select(cluster,sku,pre.price);
  sales.by.cluster=merge(sales.by.cluster,pre.sales.by.cluster,by=c("cluster","sku"));
    
  
  # sales.by.cluster[,min.price:=cost*(1+min.margin.pct)];
  sales.by.cluster[,min.pre.price:=pre.price*(1-opt$constraints$min_from_last_price)];
  sales.by.cluster[,min.margin.price:=cost/(1-min_margin)];
  sales.by.cluster[,min.price:=pmax(min.pre.price,min.margin.price)];
  sales.by.cluster[,min.margin.pct:=min.price/cost-1];
  
  
  #Reverse calculation of max margin
  sales.by.cluster[,max.price:=pre.price+(pre.price-min.price)*0.6,by="sku"];
  # sales.by.cluster[,max.price:=pmax(max.price,pre.price*(1+opt$constraints$min_max_price)),by="sku"];
  sales.by.cluster[,max.price:=pmax(max.price,pre.price*(1+0.15))];
  sales.by.cluster[,max.margin.pct:=(max.price/cost-1)];
  sales.by.cluster[,diff.margin.pct:=max.margin.pct-min.margin.pct]
  
  
 return(sales.by.cluster);
}