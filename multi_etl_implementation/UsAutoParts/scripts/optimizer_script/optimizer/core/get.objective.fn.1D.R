source("core/get.mixed.design.1D.R");

get.objective.fn<-function(env,opt,price.push.lag=3){
  
  to=env$ind$to;
  ndays.explore.effective=env$ind$ndays.explore+price.push.lag;
  print(sprintf("ndays.explore.effective: %3.2f",ndays.explore.effective));
  if (ndays.explore.effective<opt$explore) {
    lambda=max(0.5,2/ndays.explore.effective^0.5);
  } else {
    lambda=0.125;
  }
  
  skus=rownames(env$matobj$log_units);
  nskus=length(skus);
  Design=get.mixed.design(form=opt$fit$form,
                          grd=env$state$prc.grid,bs=env$state$basis,ssn=env$matobj$ssn[1,to]);
  
  #Mixed effects design matrix for obj
  obj.fns=list();
  for (j in seq(nskus)){
    
    min.price=env$matobj$costs[j,to]*(1+env$matobj$min.margin.pct[j,to]);
    max.price=env$matobj$costs[j,to]*(1+env$matobj$max.margin.pct[j,to]);
    # print(paste(skus[j],env$matobj$costs[j,to],min.price,max.price))
    subset.indices=(env$state$prc.grid>=min.price & env$state$prc.grid<=max.price);
    subset.Design=Design[subset.indices,];
    subset.price.grid=env$state$prc.grid[subset.indices];
    
    if (sum(subset.indices)==1){
      subset.Design=matrix(subset.Design,nrow=1);
    } else if (sum(subset.indices)==0){
      obj.fn=NULL;
      next;
    }
    
    if (opt$objective=="revenue"){
      if (opt$constraints$maintain=="margin"){
        objective.wt=0.25;
      } else if (opt$constraints$maintain=="none"){ 
        objective.wt=0;
      } else {
        stop("Set constraints$maintain to margin, revenue or none");
      }
    } else {
      if (opt$constraints$maintain=="revenue"){
        objective.wt=0.75;
      } else if (opt$constraints$maintain=="none"){ 
        objective.wt=1;
      } else {
        stop("Set constraints$maintain to margin, revenue or none");
      }
    }
    
    if (objective.wt==0){
      lprice.fn=log(subset.price.grid);
    } else if (objective.wt==1){
      lprice.fn=log(subset.price.grid-env$matobj$costs[j,to]);
    } else {
      lprice.fn=objective.wt*log(subset.price.grid-env$matobj$costs[j,to])+(1-objective.wt)*log(subset.price.grid);  
    }
    
    obj.fn=with(env$state$params,
                get.mm.prediction(subset.Design,mt.mixed[,j],Ct.mixed[,,j],sigma2));
    obj.fn=data.table(sku=skus[j],price=subset.price.grid,
                      blunits.fit=obj.fn$blunits.fit,
                      blunits.se=obj.fn$blunits.se,
                      blcostfn.fit=obj.fn$blunits.fit+lprice.fn,
                      blcostfn.u=obj.fn$blunits.fit+lprice.fn+lambda*obj.fn$blunits.se);
    # print(range(obj.fn$price));
    obj.fns[[j]]=obj.fn;
  }
  
  obj.fns=rbindlist(obj.fns);
  obj.fns[,sku:=factor(sku,levels=skus)];
  obj.fns[,blunits.u:=blunits.fit+lambda*blunits.se];
  obj.fns[,blunits.l:=blunits.fit-lambda*blunits.se];
  obj.fns[,blcostfn.u:=blcostfn.fit+lambda*blunits.se];
  obj.fns[,blcostfn.l:=blcostfn.fit-lambda*blunits.se];
  obj.fns=as.data.frame(obj.fns);
  
  return(obj.fns);
  
}



get.mm.prediction<-function(XZ,mt,Ct,sigma2){
  
  blunits.fit=as.numeric(XZ%*%mt);
  blunits.se=sqrt(diag(XZ%*%Ct%*%t(XZ))); #+sigma2
  blunits.se[is.na(blunits.se)]=0;
  
  return(list(blunits.fit=blunits.fit,blunits.se=blunits.se));
}

