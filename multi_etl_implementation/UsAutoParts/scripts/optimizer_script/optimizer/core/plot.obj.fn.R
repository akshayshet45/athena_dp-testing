
plot.obj.fn<-function(env,obj,proposal,last.proposals,type=1,n=20,opt){
  
  proposal.lag1=subset(last.proposals$lag1,sku %in% proposal$sku)%>%arrange(sku);
  proposal.lag2=subset(last.proposals$lag2,sku %in% proposal$sku)%>%arrange(sku);
  
  proposal=merge(proposal,proposal.lag1[,c("sku","price")],by="sku",suffixes=c("",".lag1"),all.x=TRUE);
  proposal=merge(proposal,proposal.lag2[,c("sku","price")],by="sku",suffixes=c("",".lag2"),all.x=TRUE);
  
  if (opt$plot){
    dfobj=with(env$matobj,data.frame(sku=rep(rownames(log_units),each=ncol(log_units)),log_units=as.numeric(t(log_units)),price=as.numeric(t(prices))));
    dfobj$log_revenue=log(dfobj$price)+dfobj$log_units;
    dfobj$sku=factor(dfobj$sku,levels=rownames(env$matobj$log_units));
    
    if (type==1){
      p1=xyplot(log_units~price|sku,data=dfobj,subset=sku %in% levels(obj$sku)[1:n],layout=c(5,4),as.table=TRUE,pch=20,scales=list(alternating=1));
      p3=xyplot(blunits.fit2+blunits.u2~price|sku,data=obj,subset=sku %in% levels(obj$sku)[1:n],layout=c(5,4),ylab="log_units",main=sprintf("Cluster %d, log_units",env$cluster),
                type="l",col=c("red","red","red"),lwd=c(2,1,1),lty=c(1,2,2),as.table=TRUE,scales=list(alternating=1),
                panel=panel.superpose,panel.groups=function(x,y,group.number,...){
                  panel.abline(h=log(0.1));
                  panel.xyplot(x,y,...);
                  if (group.number==2){
                    index=which.min(abs(x-proposal[panel.number(),"price"]));
                    xx=x[index];
                    yy=y[index];
                    panel.xyplot(rep(xx,2),c(log(0.1),yy),type="l",pch=20,col="black")
                    panel.xyplot(xx,yy,type="p",pch=20,col="black")
                    
                    index=which.min(abs(x-proposal[panel.number(),"price.lag1"]));
                    xx=x[index];
                    yy=y[index];
                    panel.xyplot(rep(xx,2),c(log(0.1),yy),type="l",pch=20,col="gray50")
                    panel.xyplot(xx,yy,type="p",pch=20,col="gray50")
                    
                    index=which.min(abs(x-proposal[panel.number(),"price.lag2"]));
                    xx=x[index];
                    yy=y[index];
                    panel.xyplot(rep(xx,2),c(log(0.1),yy),type="l",pch=20,col="gray80")
                    panel.xyplot(xx,yy,type="p",pch=20,col="gray80")
                  }
                })#,layout=c(5,4)
      print(p3+p1);
      
    } else {
      p1=xyplot(log_revenue~price|sku,data=dfobj,subset=sku %in% levels(dfobj$sku)[1:n],layout=c(5,4),as.table=TRUE,pch=20,scales=list(alternating=1));
      p3=xyplot(blcostfn.fit2+blcostfn.u2~price|sku,data=obj,subset=sku %in% levels(obj$sku)[1:n],ylab="log_costfn",main=sprintf("Cluster %d, log_costfn",env$cluster),
                type="l",col=c("red","red","red"),lwd=c(2,1,1),lty=c(1,2,2),layout=c(5,4),as.table=TRUE,scales=list(alternating=1),
                panel=panel.superpose,panel.groups=function(x,y,group.number,...){
                  panel.abline(h=log(0.1));
                  panel.xyplot(x,y,...);
                  if (group.number==2){
                    index=which.min(abs(x-proposal[panel.number(),"price"]));
                    xx=x[index];
                    yy=y[index];
                    panel.xyplot(rep(xx,2),c(log(0.1),yy),type="l",pch=20,col="black")
                    panel.xyplot(xx,yy,type="p",pch=20,col="black")
                    
                    index=which.min(abs(x-proposal[panel.number(),"price.lag1"]));
                    xx=x[index];
                    yy=y[index];
                    panel.xyplot(rep(xx,2),c(log(0.1),yy),type="l",pch=20,col="gray50")
                    panel.xyplot(xx,yy,type="p",pch=20,col="gray50")
                    
                    index=which.min(abs(x-proposal[panel.number(),"price.lag2"]));
                    xx=x[index];
                    yy=y[index];
                    panel.xyplot(rep(xx,2),c(log(0.1),yy),type="l",pch=20,col="gray80")
                    panel.xyplot(xx,yy,type="p",pch=20,col="gray80")
                  }
                })
      print(p3+p1);
      
    }
  }
  
  return(obj);
}