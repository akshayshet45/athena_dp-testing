
library(reshape2);

locf.variable<-function(y,fl=FALSE){
  if (any(is.finite(y))){
    y[1]=y[is.finite(y)][1];
    y=as.numeric(na.locf(zoo(y),fromLast = fl));
  }
  return(y);
}

df2mat<-function(x,state){

  x=merge(x,state$ssnbasis,by="dow",all.x=TRUE);

  log_units=acast(x,sku~date,value.var="log_units",na.mean,drop=FALSE);
  prices=acast(x,sku~date,value.var="price",na.mean,drop=FALSE);
  min.margin.pct=acast(x,sku~date,value.var="min.margin.pct",na.mean,drop=FALSE);
  max.margin.pct=acast(x,sku~date,value.var="max.margin.pct",na.mean,drop=FALSE);
  costs=acast(x,sku~date,value.var="cost",na.mean,drop=FALSE);
  ssn=acast(x,sku~date,value.var="ssn",na.mean,drop=FALSE);

  #Moving this step to getdata
  #Replace missing values by 0
  # log_units[is.na(log_units)]=0;
  #Filling forward values for price and cstvar
#   prices=t(apply(prices,1,locf.variable));
#   prices=t(apply(prices,1,locf.variable,fl=TRUE));
#   costs=t(apply(costs,1,locf.variable));
#   costs=t(apply(costs,1,locf.variable,fl=TRUE));
#

  return(list(log_units=log_units,prices=prices,min.margin.pct=min.margin.pct,max.margin.pct=max.margin.pct,costs=costs,ssn=ssn))
}


