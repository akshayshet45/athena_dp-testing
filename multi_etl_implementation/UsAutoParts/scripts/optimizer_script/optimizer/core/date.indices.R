date.indices<-function(matobj,state){
  
  dates=colnames(matobj$log_units);
  from=which(dates==state$first.obs.date);
  to=which(dates==state$last.obs.date);
  ndays=as.numeric(as.Date(state$last.obs.date)-as.Date(state$first.obs.date)+1);
  ndays.explore=as.numeric(as.Date(state$last.obs.date)-as.Date(state$first.run.date)+1);
  
  list(from=from,to=to,ndays=ndays,ndays.explore=ndays.explore)
  
}