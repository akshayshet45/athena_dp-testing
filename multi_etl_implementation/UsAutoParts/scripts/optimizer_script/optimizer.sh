#!/bin/bash
set -e

cd /mnt/split_file
rm -rf optimizer
cd /home/ubuntu/is-platform/UsAutoParts/scripts/optimizer_script/
cp -r optimizer /mnt/split_file

cd /mnt/split_file/optimizer

Rscript account.scripts/common.R insights-usautoparts.cnjeis9czdgg.us-west-2.redshift.amazonaws.com dp_write_only udInZkvcl4QqpYz insights USAP


exit