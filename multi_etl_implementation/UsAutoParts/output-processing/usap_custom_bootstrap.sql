\set ON_ERROR_STOP 1
\timing on

drop table if exists temp.distinct_sku_channel_id;

create table temp.distinct_sku_channel_id as
    select sku, channel_id, merch_l2_id from base.boomerang_catalog where feed_date = (select max(feed_date) from base.boomerang_catalog) and (sku, channel_id) in (select distinct sku, channel_id from base.experiment_skus);

drop table if exists temp.max_feed_date_exp_skus;
create table temp.max_feed_date_exp_skus as
  select sku, channel_id, max(feed_date) as feed_date
    from base.data_dictionary
      WHERE sku+channel_id in (select sku+channel_id from base.experiment_skus)
  GROUP BY 1,2;


drop table if exists temp.sku_level_details_exp_skus;
create table temp.sku_level_details_exp_skus as
select b.sku, b.channel_id, usap_sku, part_name, team_name, channel_name
from base.data_dictionary a
  inner join temp.max_feed_date_exp_skus b
  on a.sku = b.sku
  and a.channel_id = b.channel_id
  and a.feed_date = b.feed_date
GROUP BY 1,2,3,4,5,6;


DROP TABLE IF EXISTS temp_custom.hct_views_data;
create table temp_custom.hct_views_data AS
  (select sku, channel_id,
     sum(page_views) as agg_page_views,
     sum(unique_visits) as agg_unique_visits,
     sum(gross_revenue) as revenue
   from base.performance_order
   where transaction_date > (select max(transaction_date) - 28 from base.performance_order)
     AND page_views is not null
   group by 1,2);


DROP TABLE IF EXISTS temp_custom.hct_flags;
CREATE TABLE temp_custom.hct_flags as
select sku, channel_id, hct as custom_hct
  FROM
    (select *,
                  case when channel_id = 18 and percent_visits < 33 then 'Head'
                  when channel_id = 18 and percent_visits > 33 and percent_visits < 67 then 'Core'
                  when channel_id != 18 and percent_views < 33 then 'Head'
                  when channel_id != 18 and percent_views > 33 and percent_visits < 67 then 'Core'
                  else 'Tail' end as hct

FROM
(select a.sku, a.channel_id, a.agg_page_views, a.agg_unique_visits, a.revenue,
        a.running_sum_views, b.total_views, a.running_sum_views/b.total_views*100 as percent_views,
        a.running_sum_visits, b.total_visits, a.running_sum_visits/b.total_visits*100 as percent_visits

  FROM
(select *,
      sum(agg_page_views) OVER (PARTITION BY channel_id ORDER BY agg_page_views DESC ROWS UNBOUNDED PRECEDING ) as running_sum_views,
      sum(agg_unique_visits) OVER (PARTITION BY channel_id ORDER BY agg_unique_visits DESC ROWS UNBOUNDED PRECEDING ) as running_sum_visits

  FROM temp_custom.hct_views_data
ORDER BY channel_id, agg_page_views DESC ) a
left join (select channel_id, sum(agg_page_views) as total_views, sum(agg_unique_visits) as total_visits from temp_custom.hct_views_data GROUP BY 1) b
on a.channel_id = b.channel_id
ORDER BY channel_id, percent_views));


drop table if exists temp.sku_details;
create table temp.sku_details as
  select a.sku, a.channel_id, b.usap_sku, b.part_name, b.team_name, c.custom_hct, b.channel_name
  FROM
    base.experiment_skus a
    left join temp.sku_level_details_exp_skus b
    on a.sku = b.sku
      AND a.channel_id = b.channel_id
    left join temp_custom.hct_flags c
    on a.sku = c.sku
      AND a.channel_id = c.channel_id
GROUP BY 1,2,3,4,5,6,7;


drop table if exists base.measurement_dictionary;
create table base.measurement_dictionary
    sortkey(transaction_date)
    as
  select bsmd.*, usap_sku, part_name, team_name, sd.custom_hct, sd.channel_name
  from base.std_measurement_dictionary bsmd
    left join temp.sku_details sd
        ON bsmd.sku = sd.sku
        AND bsmd.channel_id = sd.channel_id;


drop table if EXISTS temp.distinct_sku_channel_id;
drop table if EXISTS temp.sku_level_details_exp_skus;
drop table if EXISTS temp_custom.hct_views_data;
drop table if EXISTS temp_custom.hct_flags;
drop table if EXISTS temp.sku_details;
