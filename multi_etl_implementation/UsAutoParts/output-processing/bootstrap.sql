\set ON_ERROR_STOP 1
\timing on

drop table if exists temp.performance_order;
create table temp.performance_order
    sortkey(sku, channel_id, transaction_date) as (
    select * from base.measurement_performance_order
);

drop table if exists temp.competitive_info;
create table temp.competitive_info
    sortkey(sku, comp_website, feed_date) as
select client_sku as sku, comp_sku, comp_name, comp_name as comp_website, comp_scrape_date as scrape_date, comp_offer_price as sas_offer_price, comp_in_stock as sas_in_stock, feed_date from  cm.pricing_adjusted_cm where client_sku in (select distinct sku from base.experiment_skus) and matching_approval_status = 'APPROVED';

drop table if exists temp.md_min_pre_date_start;
create table temp.md_min_pre_date_start as
    (
        select least(a.min_pre_date_start, b.min_pre_date_start, c.min_pre_date_start) as min_pre_date_start from
        (select
        min(pre_date_start) as min_pre_date_start from base.experiment_skus where yoy = FALSE) a,
        (select
        dateadd('day', -365, min(pre_date_start)) as min_pre_date_start from base.experiment_skus where yoy = TRUE) b,
        (select
        dateadd('day', -365, min(post_date_start)) as min_pre_date_start from base.experiment_skus where yoy = TRUE) c
    );

--drop table if exists temp.md_promotion_dates_temp;
--create table temp.md_promotion_dates_temp as (
--    select base.performance_order.sku, base.performance_order.channel_id, base.performance_order.transaction_date, case
--    when promo_id is null then 0
--    else 1
--    end as on_promo from base.performance_order, base.promotions, temp.min_pre_date_start
--    where base.performance_order.sku = base.promotions.sku and base.performance_order.channel_id = base.promotions.channel_id and base.performance_order.transaction_date >= promo_start_date and base.performance_order.transaction_date <= promo_end_date and base.promotions.feed_date = (select min(feed_date) from base.promotions bp where bp.feed_date >= base.performance_order.transaction_date) and base.performance_order.transaction_date >= temp.min_pre_date_start.min_pre_date_start and (base.performance_order.sku) in (select sku from temp.total_skus)
--);

drop table if exists temp.transaction_dates_base ;
create table temp.transaction_dates_base as
       select distinct transaction_date from temp.performance_order where transaction_date >= (select min_pre_date_start from temp.md_min_pre_date_start );


drop table if exists temp.all_exp_sku_dates;
create table  temp.all_exp_sku_dates
  distkey(exp_id)
  sortkey(sku, channel_id, transaction_date)
  as
           (select base.experiment_skus.exp_id, sku, channel_id, tc, date_created, pre_date_start, pre_date_end, post_date_start, post_date_end, yoy, is_active, transaction_date from base.experiment_skus
               inner join temp.transaction_dates_base on TRUE = TRUE and ( base.experiment_skus.post_date_end is NULL or base.experiment_skus.post_date_end > CURRENT_DATE)
               inner join base.experiment on (base.experiment.is_default =  TRUE or base.experiment.is_default = NULL) and base.experiment.exp_id = base.experiment_skus.exp_id);


drop table if exists temp.all_exp_sku_dates1;
create table temp.all_exp_sku_dates1 as
    select * from temp.all_exp_sku_dates;

delete from temp.all_exp_sku_dates1 using
    (select distinct exp_id, sku, channel_id, transaction_date from base.std_measurement_dictionary) b
    where temp.all_exp_sku_dates1.exp_id = b.exp_id and temp.all_exp_sku_dates1.sku = b.sku
    and temp.all_exp_sku_dates1.channel_id = b.channel_id and temp.all_exp_sku_dates1.transaction_date = b.transaction_date;

drop table if exists measurement.experiment;
create table measurement.experiment as (select * from base.experiment);

drop table if exists measurement.experiment_skus;
create table measurement.experiment_skus as (select * from base.experiment_skus);

drop table if exists measurement.excluded_dates;
create table measurement.excluded_dates as (select * from base.excluded_dates);

drop table if exists temp.all_sku_dates;
create table temp.all_sku_dates
    sortkey(sku, channel_id, transaction_date)
    as
    (select distinct sku, channel_id, transaction_date from temp.all_exp_sku_dates1);

drop table if exists temp.all_sku_promo;
create table temp.all_sku_promo
    sortkey(sku, channel_id, promo_id) as
 (select distinct a.sku,a.channel_id, a.promo_id, a.promo_start_date, a.promo_end_date
 from base.promotions a
 where (a.sku, a.channel_id) in (select sku,channel_id from base.experiment_skus)
and a.promo_start_date >= (select min_pre_date_start from temp.md_min_pre_date_start));


drop table if exists temp.md_promotion_dates_temp;
create table temp.md_promotion_dates_temp AS
(select distinct a.sku, a.channel_id, a.transaction_date, case when a.transaction_date between b.promo_start_date and b.promo_end_date then 1 else 0 end as on_promo
  from temp.all_sku_dates a
left join temp.all_sku_promo b
    on a.sku = b.sku and a.channel_id = b.channel_id and a.transaction_date <= b.promo_end_date);

drop table if exists temp.md_promotion_dates;
create table temp.md_promotion_dates
    sortkey(sku, channel_id, transaction_date) as
 (select sku, channel_id, transaction_date, case when sum(on_promo)>0 then 1 else 0 end as on_promo
   from
     temp.md_promotion_dates_temp
   group by sku, channel_id, transaction_date);


drop table if exists temp.price_dates_1;
create table temp.price_dates_1 as
(
    select a.sku, a.channel_id, a.transaction_date, b.map_price, b.offer_price
    from temp.all_sku_dates a
  inner join base.item_prices b
      on a.transaction_date = b.effective_date and a.sku = b.sku and a.channel_id = b.channel_id

);

drop table if exists temp.min_effective_date_price;
create table temp.min_effective_date_price as (
    select sku, channel_id, min(effective_date) as min_effective_date from base.item_prices group by sku, channel_id
);

drop table if exists temp.price_dates_2;
create table temp.price_dates_2 as
(
    select a.sku, a.channel_id, a.transaction_date, b.map_price, b.offer_price
    from temp.all_sku_dates a
    inner join base.item_prices b
    on a.sku = b.sku and a.channel_id = b.channel_id
      inner join temp.min_effective_date_price c
       on b.sku = c.sku and a.transaction_date < c.min_effective_date and b.effective_date = c.min_effective_date and b.channel_id = c.channel_id
);

drop table if exists temp.price_dates_3_1;
create table  temp.price_dates_3_1 as (
    select distinct  a.sku, a.channel_id, a.transaction_date
          from temp.all_sku_dates a
          left join base.item_prices b
          on a.sku = b.sku and (a.channel_id = b.channel_id) and a.transaction_date = b.effective_date where b.effective_date is null
);

drop table if exists temp.price_dates_3_2;
create table  temp.price_dates_3_2 as (
    select a.sku, a.channel_id, a.transaction_date from temp.price_dates_3_1 a
    inner join temp.min_effective_date_price b on a.sku = b.sku and a.channel_id = b.channel_id and a.transaction_date > b.min_effective_date
);


drop table if exists temp.best_effective_date_price;
create table temp.best_effective_date_price as (
    select a.sku, a.channel_id, a.transaction_date, b.effective_date from temp.price_dates_3_2 a
    inner join base.item_prices b on a.sku = b.sku and a.channel_id = b.channel_id and a.transaction_date > b.effective_date
);

drop table if exists temp.best_effective_date_price_1;
create table temp.best_effective_date_price_1 as (
     select sku, channel_id, transaction_date, max(effective_date) as best_effective_date from temp.best_effective_date_price group by sku, channel_id, transaction_date
);

drop table if exists temp.price_dates_3;
create table temp.price_dates_3 as (
    select c.sku, c.channel_id, c.transaction_date, d.map_price, d.offer_price
      from
     temp.best_effective_date_price_1 c
     inner join base.item_prices d
     on c.sku = d.sku and c.channel_id = d.channel_id and c.best_effective_date = d.effective_date
);

--drop table if exists temp.price_dates_3;
--create table temp.price_dates_3 as
--(
--    select c.sku, c.channel_id, c.transaction_date, d.map_price, d.offer_price
--      from
--     (select distinct  a.sku, a.channel_id, a.transaction_date
--     from temp.all_sku_dates a
--     left join base.item_prices b
--     on a.sku = b.sku and a.channel_id = b.channel_id and a.transaction_date = b.effective_date where b.effective_date is null) c
--     inner join base.item_prices d
--     on c.sku = d.sku and c.channel_id = d.channel_id
--     where d.effective_date = (select max(bp.effective_date) from base.item_prices bp where c.transaction_date>= bp.effective_date and c.sku = bp.sku and c.channel_id = bp.channel_id)
--);

 drop table if exists temp.md_item_price_dates;
 create table temp.md_item_price_dates
     sortkey(sku, channel_id, transaction_date) as
  (
select * from temp.price_dates_1
)
  union
  (
      select * from temp.price_dates_2
  )
 union
 ( select * from temp.price_dates_3
 ) ;


 drop table if exists temp.cost_dates_1;
 create table temp.cost_dates_1 as (
     select a.sku, a.channel_id, a.transaction_date, po_cost, shipping_cost, variable_cost, vendor_subsidized_po_cost
     from temp.all_sku_dates a
   inner join base.item_cost b
       on a.transaction_date = b.effective_date and a.sku = b.sku and (a.channel_id = b.channel_id or b.channel_id = -1)
 );


 drop table if exists temp.min_effective_date_cost;
 create table temp.min_effective_date_cost as (
     select sku, channel_id, min(effective_date) as min_effective_date from base.item_cost group by sku, channel_id
 );

 drop table if exists temp.cost_dates_2;
 create table temp.cost_dates_2 as (
     select a.sku, a.channel_id, a.transaction_date, po_cost, shipping_cost, variable_cost, vendor_subsidized_po_cost
     from temp.all_sku_dates a
     inner join base.item_cost b
     on a.sku = b.sku and (a.channel_id = b.channel_id or b.channel_id = -1)
       inner join temp.min_effective_date_cost c
        on b.sku = c.sku and a.transaction_date <= c.min_effective_date and b.effective_date = c.min_effective_date and (b.channel_id = c.channel_id or c.channel_id = -1)
 );

 drop table if exists temp.cost_dates_3_1;
 create table  temp.cost_dates_3_1 as (
     select distinct  a.sku, a.channel_id, a.transaction_date
           from temp.all_sku_dates a
           left join base.item_cost b
           on a.sku = b.sku and (a.channel_id = b.channel_id or b.channel_id = -1) and a.transaction_date = b.effective_date where b.effective_date is null
 );

 drop table if exists temp.cost_dates_3_2;
 create table  temp.cost_dates_3_2 as (
     select a.sku, a.channel_id, a.transaction_date from temp.cost_dates_3_1 a
     inner join temp.min_effective_date_cost b on a.sku = b.sku and (a.channel_id = b.channel_id or b.channel_id = -1) and a.transaction_date > b.min_effective_date
 );


 drop table if exists temp.best_effective_date_cost;
 create table temp.best_effective_date_cost as (
     select a.sku, a.channel_id, a.transaction_date, b.effective_date from temp.cost_dates_3_2 a
     inner join base.item_cost b on a.sku = b.sku and (a.channel_id = b.channel_id or b.channel_id = -1) and a.transaction_date > b.effective_date
 );

 drop table if exists temp.best_effective_date_cost_1;
 create table temp.best_effective_date_cost_1 as (
      select sku, channel_id, transaction_date, max(effective_date) as best_effective_date from temp.best_effective_date_cost group by sku, channel_id, transaction_date
 );

 drop table if exists temp.cost_dates_3;
 create table temp.cost_dates_3 as (
     select c.sku, c.channel_id, c.transaction_date, po_cost, shipping_cost, variable_cost, vendor_subsidized_po_cost
       from
      temp.best_effective_date_cost_1 c
      inner join base.item_cost d
      on c.sku = d.sku and (c.channel_id = d.channel_id or d.channel_id = -1) and c.best_effective_date = d.effective_date
 );

 drop table if exists temp.md_item_cost_dates;
 create table temp.md_item_cost_dates
     sortkey(sku, channel_id, transaction_date) as
  (
      select * from temp.cost_dates_1
  )
  union
  (
      select * from temp.cost_dates_2

  )
 union
 (
     select * from temp.cost_dates_3
  );

  --drop table if exists temp.catalog_dates_1;
  --create table temp.catalog_dates_1 as (
  --      select temp.all_sku_dates.sku, temp.all_sku_dates.channel_id, temp.all_sku_dates.transaction_date, title, brand, is_marketplace, is_privatelabel, gl_l1_id, gl_l1_name, gl_l2_id, gl_l2_name, gl_l3_id, gl_l3_name, gl_l4_id, gl_l4_name, gl_l5_id, gl_l5_name, merch_l1_id, merch_l1_name, merch_l2_id, merch_l2_name, merch_l3_id, merch_l3_name, merch_l4_id, merch_l4_name, merch_l5_id, merch_l5_name from temp.all_sku_dates inner join base.boomerang_catalog on  temp.all_sku_dates.sku = base.boomerang_catalog.sku and (temp.all_sku_dates.channel_id = base.boomerang_catalog.channel_id or base.boomerang_catalog.channel_id = -1) and temp.all_sku_dates.transaction_date = base.boomerang_catalog.feed_date
  --);

  --drop table if exists temp.min_feed_date_catalog;
  --create table temp.min_feed_date_catalog as (
    --  select sku, channel_id, min(feed_date) as min_feed_date from base.boomerang_catalog group by sku, channel_id
  --);

  --drop table if exists temp.catalog_dates_2;
  --create table temp.catalog_dates_2 as (
--      select a.sku, a.channel_id, a.transaction_date, title, brand, is_marketplace, is_privatelabel, gl_l1_id, gl_l1_name, gl_l2_id, gl_l2_name, gl_l3_id, gl_l3_name, gl_l4_id, gl_l4_name, gl_l5_id, gl_l5_name, merch_l1_id, merch_l1_name, merch_l2_id, merch_l2_name, merch_l3_id, merch_l3_name, merch_l4_id, merch_l4_name, merch_l5_id, merch_l5_name
 --     from temp.all_sku_dates a
  --    inner join base.boomerang_catalog b
--      on a.sku = b.sku and (a.channel_id = b.channel_id or b.channel_id = -1)
 --       inner join temp.min_feed_date_catalog c
  --       on b.sku = c.sku and a.transaction_date < c.min_feed_date and b.feed_date = c.min_feed_date and (b.channel_id = c.channel_id or c.channel_id = -1)
  --);


  --drop table if exists temp.catalog_dates_3_1;
 -- create table  temp.catalog_dates_3_1 as (
   --   select distinct  a.sku, a.channel_id, a.transaction_date
     --       from temp.all_sku_dates a
       --     left join base.boomerang_catalog b
         --   on a.sku = b.sku and (a.channel_id = b.channel_id or b.channel_id = -1) and a.transaction_date = b.feed_date where b.feed_date is null
  --);

  --drop table if exists temp.catalog_dates_3_2;
  --create table  temp.catalog_dates_3_2 as (
--      select a.sku, a.channel_id, a.transaction_date from temp.catalog_dates_3_1 a
  --    inner join temp.min_feed_date_catalog b on a.sku = b.sku and (a.channel_id = b.channel_id or b.channel_id = -1) and a.transaction_date > b.min_feed_date
  --);


 -- drop table if exists temp.best_feed_date_catalog;
 -- create table temp.best_feed_date_catalog as (
 --     select a.sku, a.channel_id, a.transaction_date, b.feed_date from temp.catalog_dates_3_2 a
 --     inner join base.boomerang_catalog b on a.sku = b.sku and (a.channel_id = b.channel_id or b.channel_id = -1) and a.transaction_date > b.feed_date
 -- );

 -- drop table if exists temp.best_feed_date_catalog_1;
 -- create table temp.best_feed_date_catalog_1 as (
 --      select sku, channel_id, transaction_date, max(feed_date) as best_feed_date from temp.best_feed_date_catalog group by sku, channel_id, transaction_date
 -- );

 -- drop table if exists temp.catalog_dates_3;
 -- create table temp.catalog_dates_3 as (
 --     select c.sku, c.channel_id, c.transaction_date, title, brand, is_marketplace, is_privatelabel, gl_l1_id, gl_l1_name, gl_l2_id, gl_l2_name, gl_l3_id, gl_l3_name, gl_l4_id, gl_l4_name, gl_l5_id, gl_l5_name, merch_l1_id, merch_l1_name, merch_l2_id, merch_l2_name, merch_l3_id, merch_l3_name, merch_l4_id, merch_l4_name, merch_l5_id, merch_l5_name
  --      from
--       temp.best_feed_date_catalog_1 c
  --     inner join base.boomerang_catalog d
--       on c.sku = d.sku and (c.channel_id = d.channel_id or d.channel_id = -1) and c.best_feed_date = d.feed_date
 -- );


-- drop table if exists temp.catalog_dates_3;
--  create table temp.catalog_dates_3 as (
--      select c.sku, c.channel_id, c.transaction_date, title, brand, is_marketplace, is_privatelabel, gl_l1_id, gl_l1_name, gl_l2_id, gl_l2_name, gl_l3_id, gl_l3_name, gl_l4_id, gl_l4_name, gl_l5_id, gl_l5_name, merch_l1_id, merch_l1_name, merch_l2_id, merch_l2_name, merch_l3_id, merch_l3_name, merch_l4_id, merch_l4_name, merch_l5_id, merch_l5_name
--        from
--       (select distinct  a.sku, a.channel_id, a.transaction_date
--       from temp.all_sku_dates a
--       left join base.boomerang_catalog b
--       on a.sku = b.sku and (a.channel_id = b.channel_id or b.channel_id = -1) and a.transaction_date = b.feed_date where b.feed_date is null) c
--       inner join base.boomerang_catalog d
--       on c.sku = d.sku and (c.channel_id = d.channel_id or d.channel_id = -1)
--       where d.feed_date = (select max(bp.feed_date) from base.boomerang_catalog bp where c.transaction_date>= bp.feed_date and c.sku = bp.sku and (c.channel_id = bp.channel_id or bp.channel_id = -1)
--   )
--  );

--drop table if exists temp.md_boomerang_cat;
--create table temp.md_boomerang_cat
 --   sortkey(sku, channel_id, transaction_date) as (
 --   (
 --       select * from temp.catalog_dates_1
 --   )
 --   union
 --   (
 --       select * from temp.catalog_dates_2
 --   )
 --   union
 --   (
 --       select * from temp.catalog_dates_3
 --   )
 --);


 --drop table if exists temp.md_boomerang_cat;
 --create table temp.md_boomerang_cat
 --    sortkey(sku, channel_id, transaction_date) as (
 --        select a.sku, a.channel_id, a.transaction_date, title, brand, is_marketplace, is_privatelabel, gl_l1_id, gl_l1_name, gl_l2_id, gl_l2_name, gl_l3_id, gl_l3_name, gl_l4_id, gl_l4_name, gl_l5_id, gl_l5_name, merch_l1_id, merch_l1_name, merch_l2_id, merch_l2_name, merch_l3_id, merch_l3_name, merch_l4_id, merch_l4_name, merch_l5_id, merch_l5_name from temp.all_sku_dates a
 --        left join base.boomerang_catalog b
 --        on a.sku = b.sku and (a.channel_id = b.channel_id or b.channel_id = -1) and b.feed_date = (select max(feed_date) from base.boomerang_catalog)
 --    );

drop table if exists temp.md_last_suggested_price_dates;
create table temp.md_last_suggested_price_dates
    sortkey(sku, channel_id, transaction_date) as (
    select temp.all_sku_dates.sku, temp.all_sku_dates.channel_id, temp.all_sku_dates.transaction_date, suggested_price as last_suggested_price, price_pre_suggestion as last_price_pre_suggestion, datediff('day', price_suggestion_date::date, transaction_date::date) as days_since_last_price_suggestion, price_suggestion_date  from temp.all_sku_dates, base.price_change_history
        where temp.all_sku_dates.sku = base.price_change_history.sku and temp.all_sku_dates.channel_id = base.price_change_history.channel_id and base.price_change_history.price_suggestion_date = (select max(price_suggestion_date) from base.price_change_history bp where bp.price_suggestion_date <= temp.all_sku_dates.transaction_date and bp.sku = temp.all_sku_dates.sku)
);

drop table if exists temp.md_comp_during_last_price_suggestion;
create table temp.md_comp_during_last_price_suggestion
    sortkey(sku, comp_website, transaction_date) as (
    select distinct bci1.sku, comp_sku, comp_website, scrape_date, sas_offer_price, transaction_date, price_suggestion_date from temp.competitive_info bci1, temp.md_last_suggested_price_dates where bci1.sku = temp.md_last_suggested_price_dates.sku and temp.md_last_suggested_price_dates.price_suggestion_date = bci1.feed_date
);

drop table if exists temp.md_comp_during_last_price_suggestion_full;
create table temp.md_comp_during_last_price_suggestion_full
    sortkey(sku, channel_id, transaction_date) as (
    select temp.md_last_suggested_price_dates.sku, temp.md_last_suggested_price_dates.channel_id, temp.md_last_suggested_price_dates.transaction_date, comp_sku, comp_website, scrape_date, sas_offer_price, temp.md_last_suggested_price_dates.price_suggestion_date from temp.md_last_suggested_price_dates, temp.md_comp_during_last_price_suggestion where temp.md_last_suggested_price_dates.sku = temp.md_comp_during_last_price_suggestion.sku and temp.md_comp_during_last_price_suggestion.transaction_date = (select min(transaction_date) from temp.md_comp_during_last_price_suggestion tcd where temp.md_last_suggested_price_dates.sku = tcd.sku and tcd.transaction_date >= temp.md_last_suggested_price_dates.transaction_date)
);

drop table if exists temp.md_comp1_temp;
create table temp.md_comp1_temp as (
    select bpo.sku, bpo.channel_id, bpo.transaction_date, bci.scrape_date as last_comp1_scrape_date, bci.sas_offer_price as comp1_price,
tcd.scrape_date as last_comp1_scrape_date_before_price_suggestion, tcd.sas_offer_price as comp1_price_before_price_suggestion
     from temp.all_sku_dates bpo
    left join temp.competitive_info bci on bpo.sku = bci.sku and bci.comp_website = (select value from base.dd_parameters where element = 'comp1' and created = (select max(created) from base.dd_parameters where element = 'comp1')) and bci.feed_date = bpo.transaction_date
    left join temp.md_comp_during_last_price_suggestion_full tcd on bpo.sku = tcd.sku and tcd.comp_website = (select value from base.dd_parameters where element = 'comp1' and created = (select max(created) from base.dd_parameters where element = 'comp1')) and tcd.transaction_date = bpo.transaction_date
);

drop table if exists temp.md_comp1;
create table temp.md_comp1
    sortkey(sku, channel_id, transaction_date) as (
    select distinct sku, channel_id, transaction_date, last_comp1_scrape_date, comp1_price, last_comp1_scrape_date_before_price_suggestion, comp1_price_before_price_suggestion from temp.md_comp1_temp
);

drop table if exists temp.md_comp2_temp;
create table temp.md_comp2_temp as (
    select bpo.sku, bpo.channel_id, bpo.transaction_date, bci.scrape_date as last_comp2_scrape_date, bci.sas_offer_price as comp2_price,
    tcd.scrape_date as last_comp2_scrape_date_before_price_suggestion, tcd.sas_offer_price as comp2_price_before_price_suggestion
     from temp.all_sku_dates bpo
    left join temp.competitive_info bci on bpo.sku = bci.sku and bci.comp_website = (select value from base.dd_parameters where element = 'comp2' and created = (select max(created) from base.dd_parameters where element = 'comp2')) and bci.feed_date = bpo.transaction_date
    left join temp.md_comp_during_last_price_suggestion_full tcd on bpo.sku = tcd.sku and tcd.comp_website = (select value from base.dd_parameters where element = 'comp2' and created = (select max(created) from base.dd_parameters where element = 'comp2')) and tcd.transaction_date = bpo.transaction_date
);

drop table if exists temp.md_comp2;
create table temp.md_comp2
    sortkey(sku, channel_id, transaction_date) as (
    select distinct sku, channel_id, transaction_date, last_comp2_scrape_date, comp2_price, last_comp2_scrape_date_before_price_suggestion, comp2_price_before_price_suggestion from temp.md_comp2_temp
);

drop table if exists temp.md_comp3_temp;
create table temp.md_comp3_temp as (
    select bpo.sku, bpo.channel_id, bpo.transaction_date, bci.scrape_date as last_comp3_scrape_date, bci.sas_offer_price as comp3_price, tcd.scrape_date as last_comp3_scrape_date_before_price_suggestion, tcd.sas_offer_price as comp3_price_before_price_suggestion from temp.all_sku_dates bpo
    left join temp.competitive_info bci on bpo.sku = bci.sku and bci.comp_website = (select value from base.dd_parameters where element = 'comp3' and created = (select max(created) from base.dd_parameters where element = 'comp3')) and bci.feed_date = bpo.transaction_date
    left join temp.md_comp_during_last_price_suggestion_full tcd on bpo.sku = tcd.sku and tcd.comp_website = (select value from base.dd_parameters where element = 'comp3' and created = (select max(created) from base.dd_parameters where element = 'comp3')) and tcd.transaction_date = bpo.transaction_date
);

drop table if exists temp.md_comp3;
create table temp.md_comp3
    sortkey(sku, channel_id, transaction_date) as (
    select distinct sku, channel_id, transaction_date, last_comp3_scrape_date, comp3_price, last_comp3_scrape_date_before_price_suggestion, comp3_price_before_price_suggestion from temp.md_comp3_temp
);

drop table if exists temp.md_comp4_temp;
create table temp.md_comp4_temp as (
    select bpo.sku, bpo.channel_id, bpo.transaction_date, bci.scrape_date as last_comp4_scrape_date, bci.sas_offer_price as comp4_price, tcd.scrape_date as last_comp4_scrape_date_before_price_suggestion, tcd.sas_offer_price as comp4_price_before_price_suggestion  from temp.all_sku_dates bpo
    left join temp.competitive_info bci on bpo.sku = bci.sku and bci.comp_website = (select value from base.dd_parameters where element = 'comp4' and created = (select max(created) from base.dd_parameters where element = 'comp4')) and bci.feed_date = bpo.transaction_date
    left join temp.md_comp_during_last_price_suggestion_full tcd on bpo.sku = tcd.sku and tcd.comp_website = (select value from base.dd_parameters where element = 'comp4' and created = (select max(created) from base.dd_parameters where element = 'comp4')) and tcd.transaction_date = bpo.transaction_date
);

drop table if exists temp.md_comp4;
create table temp.md_comp4
    sortkey(sku, channel_id, transaction_date) as (
    select distinct sku, channel_id, transaction_date, last_comp4_scrape_date, comp4_price, last_comp4_scrape_date_before_price_suggestion, comp4_price_before_price_suggestion from temp.md_comp4_temp
);

drop table if exists temp.md_comp5_temp;
create table temp.md_comp5_temp as (
    select bpo.sku, bpo.channel_id, bpo.transaction_date, bci.scrape_date as last_comp5_scrape_date, bci.sas_offer_price as comp5_price, tcd.scrape_date as last_comp5_scrape_date_before_price_suggestion, tcd.sas_offer_price as comp5_price_before_price_suggestion from temp.all_sku_dates bpo
    left join temp.competitive_info bci on bpo.sku = bci.sku and bci.comp_website = (select value from base.dd_parameters where element = 'comp5' and created = (select max(created) from base.dd_parameters where element = 'comp5')) and bci.feed_date = bpo.transaction_date
    left join temp.md_comp_during_last_price_suggestion_full tcd on bpo.sku = tcd.sku and tcd.comp_website = (select value from base.dd_parameters where element = 'comp5' and created = (select max(created) from base.dd_parameters where element = 'comp5')) and tcd.transaction_date = bpo.transaction_date
);

drop table if exists temp.md_comp5;
create table temp.md_comp5
    sortkey(sku, channel_id, transaction_date) as (
    select distinct sku, channel_id, transaction_date, last_comp5_scrape_date, comp5_price, last_comp5_scrape_date_before_price_suggestion, comp5_price_before_price_suggestion from temp.md_comp5_temp
);

--drop table if exists base.std_measurement_dictionary cascade;
drop table if exists temp.std_md;

create table temp.std_md as
     (select base.experiment.exp_id, base.experiment.exp_name, base.experiment.exp_type, temp.all_exp_sku_dates1.transaction_date, temp.all_exp_sku_dates1.sku, temp.all_exp_sku_dates1.channel_id, temp.all_exp_sku_dates1.TC, temp.all_exp_sku_dates1.pre_date_start, temp.all_exp_sku_dates1.pre_date_end, temp.all_exp_sku_dates1.post_date_start, temp.all_exp_sku_dates1.post_date_end, base.experiment.strategy as strategy,
temp.performance_order.gross_units, temp.performance_order.gross_orders, temp.performance_order.gross_revenue, temp.performance_order.gross_cost, temp.performance_order.gross_revenue - temp.performance_order.gross_cost as gross_margin,  temp.performance_order.page_views, temp.performance_order.unique_visits,
CASE
WHEN temp.performance_order.gross_units = 0 THEN NULL
ELSE temp.performance_order.gross_revenue/temp.performance_order.gross_units
END
 as asp,
 CASE
 WHEN temp.performance_order.page_views = 0 THEN NULL
 ELSE temp.performance_order.gross_units/temp.performance_order.page_views
 END
  as conversion,
  on_promo,
  excluded_dates.excluded, excluded_dates.reason as excluded_reason, title,
  brand, is_marketplace, is_privatelabel, gl_l1_id, gl_l1_name, gl_l2_id, gl_l2_name, gl_l3_id, gl_l3_name, gl_l4_id, gl_l4_name, gl_l5_id, gl_l5_name, merch_l1_id, merch_l1_name, merch_l2_id, merch_l2_name, merch_l3_id, merch_l3_name, merch_l4_id, merch_l4_name, merch_l5_id, merch_l5_name, map_price, offer_price, hct, num_comp, last_comp1_scrape_date, comp1_price, last_comp3_scrape_date, comp3_price, last_comp4_scrape_date, comp4_price, last_comp2_scrape_date, comp2_price, last_comp5_scrape_date, comp5_price, po_cost, shipping_cost, variable_cost, vendor_subsidized_po_cost, suggested_price, price_pre_suggestion, NULL as suggested_price_type, last_suggested_price, last_price_pre_suggestion, days_since_last_price_suggestion,
  last_comp1_scrape_date_before_price_suggestion, comp1_price_before_price_suggestion,
  last_comp2_scrape_date_before_price_suggestion, comp2_price_before_price_suggestion,
  last_comp3_scrape_date_before_price_suggestion, comp3_price_before_price_suggestion,
  last_comp4_scrape_date_before_price_suggestion, comp4_price_before_price_suggestion,
  last_comp5_scrape_date_before_price_suggestion, comp5_price_before_price_suggestion,
  CASE
  WHEN temp.all_exp_sku_dates1.transaction_date >= temp.all_exp_sku_dates1.pre_date_start and temp.all_exp_sku_dates1.transaction_date <=  temp.all_exp_sku_dates1.pre_date_end then 'pre'
  WHEN temp.all_exp_sku_dates1.transaction_date >= temp.all_exp_sku_dates1.post_date_start and
  (temp.all_exp_sku_dates1.transaction_date is null or temp.all_exp_sku_dates1.transaction_date <= temp.all_exp_sku_dates1.post_date_end or temp.all_exp_sku_dates1.post_date_end is null)
  then 'post'
  end as prevspost,
   extract (w from temp.all_exp_sku_dates1.transaction_date) as weekno, extract  (mon from temp.all_exp_sku_dates1.transaction_date) as monthno, extract ( doy from temp.all_exp_sku_dates1.transaction_date) as dayno, extract ( y from temp.all_exp_sku_dates1.transaction_date) as year
   from
   base.experiment
   inner join temp.all_exp_sku_dates1 on base.experiment.exp_id = temp.all_exp_sku_dates1.exp_id
   left outer join  temp.performance_order  on temp.performance_order.sku = temp.all_exp_sku_dates1.sku and temp.performance_order.channel_id = temp.all_exp_sku_dates1.channel_id and temp.all_exp_sku_dates1.transaction_date = temp.performance_order.transaction_date
   left outer join base.excluded_dates on base.excluded_dates.exp_id = temp.all_exp_sku_dates1.exp_id and temp.all_exp_sku_dates1.sku = base.excluded_dates.sku and temp.all_exp_sku_dates1.channel_id =  base.excluded_dates.channel_id and base.excluded_dates.excluded_date = temp.all_exp_sku_dates1.transaction_date
   left outer join temp.md_promotion_dates on temp.md_promotion_dates.sku = temp.all_exp_sku_dates1.sku and temp.md_promotion_dates.channel_id = temp.all_exp_sku_dates1.channel_id and temp.all_exp_sku_dates1.transaction_date = temp.md_promotion_dates.transaction_date
   left outer join base.boomerang_catalog on temp.all_exp_sku_dates1.sku = base.boomerang_catalog.sku and (temp.all_exp_sku_dates1.channel_id = base.boomerang_catalog.channel_id or base.boomerang_catalog.channel_id = -1) and base.boomerang_catalog.feed_date = (select max(feed_date) from base.boomerang_catalog)
   left outer join temp.md_item_price_dates on temp.all_exp_sku_dates1.sku = temp.md_item_price_dates.sku and temp.all_exp_sku_dates1.channel_id = temp.md_item_price_dates.channel_id and temp.all_exp_sku_dates1.transaction_date = temp.md_item_price_dates.transaction_date
   left outer join base.hct on temp.all_exp_sku_dates1.sku = base.hct.sku
   left outer join (select base.matching_info.sku, count(*) as num_comp from base.matching_info where base.matching_info.feed_date = (select max(feed_date) from base.matching_info) group by base.matching_info.sku) p on p.sku = temp.all_exp_sku_dates1.sku
   left outer join temp.md_comp1 on temp.md_comp1.sku = temp.all_exp_sku_dates1.sku and temp.md_comp1.channel_id = temp.all_exp_sku_dates1.channel_id and temp.md_comp1.transaction_date = temp.all_exp_sku_dates1.transaction_date
   left outer join temp.md_comp2 on temp.md_comp2.sku = temp.all_exp_sku_dates1.sku and temp.md_comp2.channel_id = temp.all_exp_sku_dates1.channel_id and temp.md_comp2.transaction_date = temp.all_exp_sku_dates1.transaction_date
   left outer join temp.md_comp3 on temp.md_comp3.sku = temp.all_exp_sku_dates1.sku and temp.md_comp3.channel_id = temp.all_exp_sku_dates1.channel_id and temp.md_comp3.transaction_date = temp.all_exp_sku_dates1.transaction_date
   left outer join temp.md_comp4 on temp.md_comp4.sku = temp.all_exp_sku_dates1.sku and temp.md_comp4.channel_id = temp.all_exp_sku_dates1.channel_id and temp.md_comp4.transaction_date = temp.all_exp_sku_dates1.transaction_date
   left outer join temp.md_comp5 on temp.md_comp5.sku = temp.all_exp_sku_dates1.sku and temp.md_comp5.channel_id = temp.all_exp_sku_dates1.channel_id and temp.md_comp5.transaction_date = temp.all_exp_sku_dates1.transaction_date
   left outer join temp.md_item_cost_dates on temp.all_exp_sku_dates1.sku = temp.md_item_cost_dates.sku and temp.all_exp_sku_dates1.channel_id = temp.md_item_cost_dates.channel_id and temp.all_exp_sku_dates1.transaction_date = temp.md_item_cost_dates.transaction_date
   left outer join base.price_change_history on temp.all_exp_sku_dates1.sku = base.price_change_history.sku and temp.all_exp_sku_dates1.channel_id = base.price_change_history.channel_id and temp.all_exp_sku_dates1.transaction_date = base.price_change_history.price_suggestion_date
   left outer join temp.md_last_suggested_price_dates on temp.all_exp_sku_dates1.sku = temp.md_last_suggested_price_dates.sku and temp.all_exp_sku_dates1.channel_id = temp.md_last_suggested_price_dates.channel_id and temp.all_exp_sku_dates1.transaction_date = temp.md_last_suggested_price_dates.transaction_date);



   insert into base.std_measurement_dictionary
       (exp_id, exp_name, exp_type, transaction_date, sku, channel_id, TC, pre_date_start, pre_date_end, post_date_start, post_date_end, strategy, gross_units, gross_orders, gross_revenue, gross_cost, gross_margin, page_views, unique_visits, asp, conversion, on_promo, excluded, excluded_reason, title, brand, is_marketplace, is_privatelabel, gl_l1_id, gl_l1_name, gl_l2_id, gl_l2_name, gl_l3_id, gl_l3_name, gl_l4_id, gl_l4_name, gl_l5_id, gl_l5_name, merch_l1_id, merch_l1_name, merch_l2_id, merch_l2_name, merch_l3_id, merch_l3_name, merch_l4_id, merch_l4_name, merch_l5_id, merch_l5_name, map_price, offer_price, hct, num_comp, last_comp1_scrape_date, comp1_price, last_comp3_scrape_date, comp3_price, last_comp4_scrape_date, comp4_price, last_comp2_scrape_date, comp2_price, last_comp5_scrape_date, comp5_price, po_cost, shipping_cost, variable_cost, vendor_subsidized_po_cost, suggested_price, price_pre_suggestion, suggested_price_type, last_suggested_price, last_price_pre_suggestion, days_since_last_price_suggestion,
     last_comp1_scrape_date_before_price_suggestion, comp1_price_before_price_suggestion,
     last_comp2_scrape_date_before_price_suggestion, comp2_price_before_price_suggestion,
     last_comp3_scrape_date_before_price_suggestion, comp3_price_before_price_suggestion,
     last_comp4_scrape_date_before_price_suggestion, comp4_price_before_price_suggestion,
     last_comp5_scrape_date_before_price_suggestion, comp5_price_before_price_suggestion, prevspost, weekno, monthno, dayno, year) (select exp_id, exp_name, exp_type, transaction_date, sku, channel_id, TC, pre_date_start, pre_date_end, post_date_start, post_date_end, strategy, gross_units, gross_orders, gross_revenue, gross_cost, gross_margin, page_views, unique_visits, asp, conversion, on_promo, excluded, excluded_reason, title, brand, is_marketplace, is_privatelabel, gl_l1_id, gl_l1_name, gl_l2_id, gl_l2_name, gl_l3_id, gl_l3_name, gl_l4_id, gl_l4_name, gl_l5_id, gl_l5_name, merch_l1_id, merch_l1_name, merch_l2_id, merch_l2_name, merch_l3_id, merch_l3_name, merch_l4_id, merch_l4_name, merch_l5_id, merch_l5_name, map_price, offer_price, hct, num_comp, last_comp1_scrape_date, comp1_price, last_comp3_scrape_date, comp3_price, last_comp4_scrape_date, comp4_price, last_comp2_scrape_date, comp2_price, last_comp5_scrape_date, comp5_price, po_cost, shipping_cost, variable_cost, vendor_subsidized_po_cost, suggested_price, price_pre_suggestion, suggested_price_type, last_suggested_price, last_price_pre_suggestion, days_since_last_price_suggestion,
     last_comp1_scrape_date_before_price_suggestion, comp1_price_before_price_suggestion,
     last_comp2_scrape_date_before_price_suggestion, comp2_price_before_price_suggestion,
     last_comp3_scrape_date_before_price_suggestion, comp3_price_before_price_suggestion,
     last_comp4_scrape_date_before_price_suggestion, comp4_price_before_price_suggestion,
     last_comp5_scrape_date_before_price_suggestion, comp5_price_before_price_suggestion, prevspost, weekno, monthno, dayno, year from temp.std_md);


drop table if exists temp.pre_t_perfs;
create table temp.pre_t_perfs as
    select  exp_id, sku, channel_id, sum(gross_revenue) as revenue, sum(gross_revenue) - sum(gross_cost) as margin,     sum(gross_units) as units, sum(page_views) as page_views from base.std_measurement_dictionary where tc = 'T' and prevspost = 'pre' and (excluded is NULL or excluded = FALSE) group by exp_id, sku, channel_id;

drop table if exists temp.pre_c_perfs_sku;
create table temp.pre_c_perfs_sku as
        select  exp_id, sku, channel_id, sum(gross_revenue) as revenue, sum(gross_revenue) - sum(gross_cost) as margin,     sum(gross_units) as units, sum(page_views) as page_views from base.std_measurement_dictionary where tc = 'C' and prevspost = 'pre' and (excluded is NULL or excluded = FALSE) group by exp_id, sku, channel_id;

drop table if exists temp.post_c_perfs_weekly;
create table temp.post_c_perfs_weekly as
    select  dateadd(day, -1, DATE_TRUNC('w', transaction_date)) week, sum(gross_revenue) as revenue, sum(gross_revenue) - sum(gross_cost) as margin, sum(gross_units) as units, sum(page_views) as page_views from base.std_measurement_dictionary where tc = 'C' and prevspost = 'post' group by dateadd(day, -1, DATE_TRUNC('w',  transaction_date));

drop table if exists temp.post_c_perfs;
create table temp.post_c_perfs as
        select  exp_id, sum(gross_revenue) as revenue, sum(gross_revenue) - sum(gross_cost) as margin, sum(gross_units) as units, sum(page_views) as page_views from base.std_measurement_dictionary where tc = 'C' and prevspost = 'post' and  (excluded is NULL or excluded = FALSE) group by exp_id;


drop table if exists temp.pre_c_perfs;
create table temp.pre_c_perfs as
        select  sum(gross_revenue) as revenue, sum(gross_revenue) - sum(gross_cost) as margin, sum(gross_units) as units, sum(page_views) as page_views from base.std_measurement_dictionary where tc = 'C' and prevspost = 'pre' and  (excluded is NULL or excluded = FALSE) group by exp_id;


drop table if exists temp.yoy_t_perfs;
create table temp.yoy_t_perfs as
    select sum(gross_revenue)/count(distinct sku) as normalized_revenue, (sum(gross_revenue) - sum(gross_cost))/count(distinct sku) as normalized_margin, sum(gross_units)/count(distinct sku) as normalized_units, sum(page_views)/count(distinct sku) as page_views from base.std_measurement_dictionary;


    drop table if exists temp.competitive_info;
    drop table if exists temp.md_min_pre_date_start;
    drop table if exists temp.transaction_dates_base ;
    drop table if exists temp.all_exp_sku_dates1;
    drop table if exists temp.all_sku_dates;
    drop table if exists temp.all_sku_promo;
    drop table if exists temp.md_promotion_dates_temp;
    drop table if exists temp.md_promotion_dates;
    drop table if exists temp.md_item_price_dates;
    drop table if exists temp.md_item_cost_dates;
    drop table if exists temp.md_boomerang_cat;
    drop table if exists temp.md_last_suggested_price_dates;
    drop table if exists temp.md_comp_during_last_price_suggestion;
    drop table if exists temp.md_comp_during_last_price_suggestion_full;
    drop table if exists temp.md_comp1_temp;
    drop table if exists temp.md_comp1;
    drop table if exists temp.md_comp2_temp;
    drop table if exists temp.md_comp2;
    drop table if exists temp.md_comp3_temp;
    drop table if exists temp.md_comp3;
    drop table if exists temp.md_comp4_temp;
    drop table if exists temp.md_comp4;
    drop table if exists temp.md_comp5_temp;
    drop table if exists temp.md_comp5;

--create view measurement.std_measurement_dictionary as (select * from base.std_measurement_dictionary);