\set ON_ERROR_STOP 1
\timing on

drop table if EXISTS temp_custom.item_cost_latest;
create table temp_custom.item_cost_latest(
  sku VARCHAR(255) encode lzo,
  channel_id int encode delta,
  po_cost DECIMAL(10,2) encode delta32k,
  feed_date TIMESTAMP encode delta
)
  distkey(channel_id)
  sortkey(sku,channel_id,po_cost,feed_date);

insert into temp_custom.item_cost_latest
  select  sku,channel_id,po_cost,feed_date from base.item_cost where feed_date=(select max(feed_date) from base.item_cost);

update derived.item_cost_history
  set second_last_cost = previous_cost,
  second_last_cost_start_date = previous_cost_start_date,
  previous_cost = current_cost,
  previous_cost_start_date = current_cost_start_date,
  current_cost = ic.po_cost,
  current_cost_start_date = ic.feed_date
from temp_custom.item_cost_latest ic
where ic.sku = item_cost_history.sku
and ic.channel_id = item_cost_history.channel_id
and ic.po_cost <> current_cost;

insert into derived.item_cost_history (sku,channel_id,current_cost,current_cost_start_date)
select ic.sku, ic.channel_id, ic.po_cost, ic.feed_date
     from temp_custom.item_cost_latest ic
     where not exists ( select * from derived.item_cost_history ich where ich.sku = ic.sku and ich.channel_id = ic.channel_id);

drop table if EXISTS temp_custom.item_price_latest;
create table temp_custom.item_price_latest(
  sku VARCHAR(255) encode lzo,
  channel_id int encode delta,
  offer_price DECIMAL(10,2) encode delta32k,
  feed_date TIMESTAMP encode delta
)
  distkey(channel_id)
  sortkey(sku,channel_id,offer_price,feed_date);

insert into temp_custom.item_price_latest
  select  sku,channel_id,offer_price,feed_date from base.item_prices where feed_date=(select max(feed_date) from base.item_cost);

update derived.item_price_history
  set second_last_price = previous_price,
  second_last_price_start_date = previous_price_start_date,
  previous_price = current_price,
  previous_price_start_date = current_price_start_date,
  current_price = ip.offer_price,
  current_price_start_date = ip.feed_date
from temp_custom.item_price_latest ip
where ip.sku = item_price_history.sku
and ip.channel_id = item_price_history.channel_id
and current_price <> ip.offer_price;

insert into derived.item_price_history (sku,channel_id,current_price,current_price_start_date)
     select ip.sku, ip.channel_id, ip.offer_price, ip.feed_date
     from temp_custom.item_price_latest ip
     where not exists ( select * from derived.item_price_history iph where iph.sku = ip.sku and iph.channel_id = ip.channel_id);


drop table if exists temp_custom.custom_catalogs;
create table temp_custom.custom_catalogs as
  select
    cc.sku,
    cc.channel_id,
    cc.feed_date,
    cc.sku_status,
    cc.part_name,
    cc.model,
    cc.sku_mover_type,
    cc.clearance_type,
    cc.worldpac_flag,
    cc.truck_freight_flag,
    cc.map_type,
    cc.stockship,
    cc.delivery_channel,
    cc.sku_number,
    cc.ford_patent,
    cc.usap_sku,
    cc.core_cost,
    cc.core_price,
    cc.po_cost_price,
    cc.fifo_cost,
    cc.ds_cost,
    cc.ss_cost,
    cc.extra_shipping,
    cc.shipping_revenue,
    cc.handlingrevenue as handling_revenue,
    cc.kit,
    cc.iswarranty,
    cc.vendor_name,
	cc.pricing_status,
	cc.team_name
  from usautoparts.custom_catalogs cc
  where feed_date = (select max(feed_date) from base.boomerang_catalog);

drop table if exists temp_custom.revenue_6_months;
create table temp_custom.revenue_6_months as
  select
  sku,
  channel_id,
  sum(gross_revenue) as revenue_6_months
from base.performance_order
where transaction_date > (select dateadd(day, -184, max(feed_date)) from base.boomerang_catalog)
group by sku, channel_id;

drop table if exists temp_custom.revenue_6_months_all_channels;
create table temp_custom.revenue_6_months_all_channels as
  select
  sku,
  sum(revenue_6_months) as revenue_6_months_all_channels
from temp_custom.revenue_6_months
group by sku;

drop table if exists temp_custom.handling_charges;
create table temp_custom.handling_charges as
  select
  sku,
  channel_id,
  gross_handling_charges
from usautoparts.custom_performance
where feed_date = (select max(feed_date) from base.boomerang_catalog);

drop table if exists temp_custom.target_margin_today;
create table temp_custom.target_margin_today as
select
t1.sku,
t1.channel_id,
coalesce(t3.target_margin, t4.target_margin) as target_margin_today
from base.boomerang_catalog t1
inner join usautoparts.custom_catalogs t2 on t1.sku = t2.sku and t1.channel_id = t2.channel_id and t1.feed_date = t2.feed_date
inner join base.item_cost ic on t1.sku = ic.sku and t1.channel_id = ic.channel_id and t1.feed_date = ic.feed_date
left join usautoparts.part_brand_target_margin t3 on t2.part_name = t3.part_name and t1.brand = t3.brand_name and ic.po_cost between t3.start_cost and (t3.end_cost - 0.001) and t1.feed_date = t3.feed_date
left join usautoparts.part_target_margin t4 on t2.part_name = t4.part_name and ic.po_cost between t4.start_cost and (t4.end_cost - 0.001) and t1.feed_date = t4.feed_date
where t1.feed_date = (select max(feed_date) from base.boomerang_catalog);

drop table if exists temp_custom.target_margin_yesterday;
create table temp_custom.target_margin_yesterday as
select
t1.sku,
t1.channel_id,
coalesce(t3.target_margin, t4.target_margin) as target_margin_yesterday
from base.boomerang_catalog t1
inner join usautoparts.custom_catalogs t2 on t1.sku = t2.sku and t1.channel_id = t2.channel_id and t1.feed_date = t2.feed_date
inner join base.item_cost ic on t1.sku = ic.sku and t1.channel_id = ic.channel_id and t1.feed_date = ic.feed_date
left join usautoparts.part_brand_target_margin t3 on t2.part_name = t3.part_name and t1.brand = t3.brand_name and ic.po_cost between t3.start_cost and (t3.end_cost-0.001) and t1.feed_date = t3.feed_date
left join usautoparts.part_target_margin t4 on t2.part_name = t4.part_name and ic.po_cost between t4.start_cost and (t4.end_cost - 0.001) and t1.feed_date = t4.feed_date
where t1.feed_date = (select dateadd(day, -1, max(feed_date)) from base.boomerang_catalog);

drop table if exists temp_custom.parent_sku_min_cost;
create table temp_custom.parent_sku_min_cost as
select sr.child_sku, ic.channel_id, min(ic.po_cost) as parent_sku_min_cost
from base.sku_relationships sr, base.item_cost ic, temp_custom.revenue_6_months tc_r6
where sr.parent_sku = ic.sku
and sr.feed_date = ic.feed_date
and sr.parent_sku = tc_r6.sku
and ic.channel_id = tc_r6.channel_id
and sr.feed_date = (select max(feed_date) from base.boomerang_catalog)
and COALESCE(tc_r6.revenue_6_months,0) > 0
group by sr.child_sku, ic.channel_id;

drop table if exists temp_custom.parent_sku_min_price;
create table temp_custom.parent_sku_min_price as
select sr.child_sku, ip.channel_id, min(ip.offer_price) as parent_sku_min_price
from base.sku_relationships sr, base.item_prices ip, temp_custom.revenue_6_months tc_r6
where sr.parent_sku = ip.sku
and sr.feed_date = ip.feed_date
and sr.parent_sku = tc_r6.sku
and ip.channel_id = tc_r6.channel_id
and sr.feed_date = (select max(feed_date) from base.boomerang_catalog)
and COALESCE(tc_r6.revenue_6_months,0) > 0
group by sr.child_sku, ip.channel_id;

drop table if exists temp_custom.days_for_current_cost;
create table temp_custom.days_for_current_cost as
select
  sku,
  channel_id,
  datediff(day, current_cost_start_date, current_date+1) as days_for_current_cost
from derived.item_cost_history;

drop table if exists temp_custom.current_price_attributes;
create table temp_custom.current_price_attributes as
select
  iph.sku,
  iph.channel_id,
  datediff(day, current_price_start_date, to_date((select max(feed_date) from base.item_prices),'yyyy-mm-dd')) as days_for_current_price,
  datediff(day, current_price_start_date, (current_price_start_date + cast((to_date((select max(feed_date) from base.performance_order),'yyyy-mm-dd') - to_date(current_price_start_date,'yyyy-mm-dd')) / 7 AS INTEGER) * 7)) as days_to_be_considered,
  current_price_start_date,
  (select max(feed_date) from base.item_prices) as item_prices_max_feed_date
 from derived.item_price_history iph;

drop table if exists temp_custom.current_price_details;
create table temp_custom.current_price_details as
 select
 tc_cpa.sku,
 tc_cpa.channel_id,
 tc_cpa.days_for_current_price,
 tc_cpa.days_to_be_considered,
 CASE
 WHEN days_to_be_considered > 0 THEN sum(gross_revenue)/tc_cpa.days_to_be_considered
 ELSE NULL END as norm_sales_current_price,
 CASE
 WHEN days_to_be_considered > 0 THEN sum(gross_units)/tc_cpa.days_to_be_considered
 ELSE NULL END as norm_units_current_price,
 CASE
    WHEN days_to_be_considered > 0 THEN sum(gross_revenue - (po_cost - shipping_cost - variable_cost) * gross_units)/tc_cpa.days_to_be_considered
 ELSE NULL END as norm_vcm_current_price
 from temp_custom.current_price_attributes tc_cpa, base.performance_order perf, base.item_cost ic
 where tc_cpa.sku = perf.sku
 and perf.sku = ic.sku
 and tc_cpa.channel_id = perf.channel_id
 and perf.channel_id = ic.channel_id
 and perf.transaction_date between current_price_start_date and (current_price_start_date+tc_cpa.days_to_be_considered-1)
 and perf.transaction_date = ic.feed_date
 group by tc_cpa.sku, tc_cpa.channel_id, tc_cpa.days_for_current_price, tc_cpa.days_to_be_considered;

drop table if exists temp_custom.previous_price_attributes;
create table temp_custom.previous_price_attributes as
select
  iph.sku,
  iph.channel_id,
  iph.previous_price,
  datediff(day, previous_price_start_date, current_price_start_date) as days_for_previous_price,
  datediff(day, previous_price_start_date, (previous_price_start_date + cast((to_date(current_price_start_date,'yyyy-mm-dd') - to_date(previous_price_start_date,'yyyy-mm-dd')) / 7 AS INTEGER) * 7)) as days_to_be_considered,
  previous_price_start_date,
  current_price_start_date
 from derived.item_price_history iph;

drop table if exists temp_custom.previous_price_details;
create table temp_custom.previous_price_details as
 select
 tc_ppa.sku,
 tc_ppa.channel_id,
 tc_ppa.previous_price,
 tc_ppa.days_for_previous_price,
 tc_ppa.days_to_be_considered,
 CASE
 WHEN days_to_be_considered > 0 THEN sum(gross_revenue)/tc_ppa.days_to_be_considered
 ELSE NULL END as norm_sales_previous_price,
 CASE
 WHEN days_to_be_considered > 0 THEN sum(gross_units)/tc_ppa.days_to_be_considered
 ELSE NULL END as norm_units_previous_price,
 CASE
 WHEN days_to_be_considered > 0 THEN sum(gross_revenue - (po_cost - shipping_cost - variable_cost) * gross_units)/tc_ppa.days_to_be_considered
 ELSE NULL END as norm_vcm_previous_price
 from temp_custom.previous_price_attributes tc_ppa, base.performance_order perf, base.item_cost ic
 where tc_ppa.sku = perf.sku
 and perf.sku = ic.sku
 and tc_ppa.channel_id = perf.channel_id
 and perf.channel_id = ic.channel_id
 and perf.transaction_date between previous_price_start_date and (previous_price_start_date+tc_ppa.days_to_be_considered-1)
 and perf.transaction_date = ic.feed_date
 group by tc_ppa.sku, tc_ppa.channel_id, tc_ppa.previous_price, tc_ppa.days_for_previous_price, tc_ppa.days_to_be_considered;

drop table if exists temp_custom.item_channel_details;
create table temp_custom.item_channel_details as
  select
  bc.sku,
  bc.channel_id,
  ch.value as channel_name
  from base.boomerang_catalog bc
  left join usautoparts.channel_hierarchy ch on bc.channel_id = ch.id
  where bc.feed_date = (select max(feed_date) from base.boomerang_catalog);

drop table if exists temp_custom.shipping_revenue_details_today;
create table temp_custom.shipping_revenue_details_today as
  select
       ic.sku,
       ic.channel_id,
       case icd.channel_name
       when 'price2'
       then shipping_cost
       when 'price4'
       then shipping_cost
       else null end as shipping_revenue1_today,
       case icd.channel_name
       when 'price2'
       then shipping_cost*0.24
       when 'price4'
       then shipping_cost*0.24
       else null end as handling_revenue1_today,
       case icd.channel_name
       when 'price3'
       then shipping_cost
       else null end as shipping_revenue3_today,
       case icd.channel_name
       when 'price3'
       then shipping_cost*0.24
       else null end as handling_revenue3_today
  from
  base.item_cost ic
  left join temp_custom.item_channel_details icd on ic.channel_id = icd.channel_id and ic.sku = icd.sku
  where ic.feed_date = (select max(feed_date) from base.boomerang_catalog);


drop table if exists temp_custom.shipping_revenue_details_yesterday;
create table temp_custom.shipping_revenue_details_yesterday as
  select
       ic.sku,
       ic.channel_id,
       case icd.channel_name
       when 'price2'
       then shipping_cost
       when 'price4'
       then shipping_cost
       else null end as shipping_revenue1_yesterday,
       case icd.channel_name
       when 'price2'
       then shipping_cost*0.24
       when 'price4'
       then shipping_cost*0.24
       else null end as handling_revenue1_yesterday,
       case icd.channel_name
       when 'price3'
       then shipping_cost
       else null end as shipping_revenue3_yesterday,
       case icd.channel_name
       when 'price3'
       then shipping_cost*0.24
       else null end as handling_revenue3_yesterday
  from
  base.item_cost ic
  left join temp_custom.item_channel_details icd on ic.channel_id = icd.channel_id and ic.sku = icd.sku
  where ic.feed_date = (select max(feed_date)-1 from base.boomerang_catalog);

drop table if exists temp_custom.item_po_cost_markup_details;
create table temp_custom.item_po_cost_markup_details as
  select
  ic.sku,
  ic.channel_id,
  ic.po_cost,
  gm.mark_up
  from base.item_cost ic, usautoparts.global_markup gm
  where ic.po_cost between gm.bucket_from and (gm.bucket_to - 0.001)
  and ic.feed_date = (select max(feed_date) from base.boomerang_catalog)
  and gm.feed_date = (select max(feed_date) from base.boomerang_catalog);

drop table if exists temp_custom.global_markup_details;
create table temp_custom.global_markup_details as
 select
 ic.sku,
 ic.channel_id,
 ic.po_cost,
 case when ic.channel_id = 11 then cm.price1
 when ic.channel_id = 12 then cm.price2
 when ic.channel_id = 13 then cm.price3
 when ic.channel_id = 14 then cm.price4
 when ic.channel_id = 15 then 0
 when ic.channel_id = 16 then cm.price8
 when ic.channel_id = 17 then cm.stylin
 when ic.channel_id = 18 then cm.jcw
 end as global_markup
from base.item_cost ic, usautoparts.channel_markup cm
where ic.po_cost between cm.start_cost and coalesce(cm.end_cost,9999999)
and ic.feed_date = (select max(feed_date) from base.boomerang_catalog)
and cm.feed_date = (select max(feed_date) from base.boomerang_catalog);


drop table if exists temp_custom.cost_changed_flag;
create table temp_custom.cost_changed_flag as
  select
  ic_today.sku,
  ic_today.channel_id,
  case when ic_today.po_cost <> ic_yesterday.po_cost then 1 else 0 end as cost_changed_flag
  from
    base.item_cost ic_today, base.item_cost ic_yesterday
    where ic_today.sku = ic_yesterday.sku
    and ic_today.channel_id = ic_yesterday.channel_id
    and ic_today.feed_date = (select max(feed_date) from base.boomerang_catalog)
    and ic_yesterday.feed_date = (select max(feed_date)-1 from base.boomerang_catalog);


drop table if exists temp_custom.reversion_flag_parameters;
create table temp_custom.reversion_flag_parameters as
  select
    iph.sku,
    iph.channel_id,
    iph.current_price,
    iph.second_last_price,
    iph.second_last_price_start_date,
    ic_second_last_price_start_date.po_cost as cost_on_second_last_price_date,
    current_cost.po_cost as current_cost
  from
  derived.item_price_history iph, base.item_cost ic_second_last_price_start_date, base.item_cost current_cost
  where iph.sku = ic_second_last_price_start_date.sku
  and iph.channel_id = ic_second_last_price_start_date.channel_id
  and iph.sku = current_cost.sku
  and iph.channel_id = current_cost.channel_id
  and ic_second_last_price_start_date.feed_date = iph.second_last_price_start_date
  and current_cost.feed_date = (select max(feed_date) from base.boomerang_catalog);

drop table if exists temp_custom.channel_offer_prices;
create table temp_custom.channel_offer_prices as
SELECT sku, avg(offer_price_price1) as offer_price_price1, avg(offer_price_price2) as offer_price_price2, avg(offer_price_price3) as offer_price_price3, avg(offer_price_price4) as offer_price_price4, avg(offer_price_price6) as offer_price_price6, avg(offer_price_price8) as offer_price_price8, avg(offer_price_stylin) as offer_price_stylin, avg(offer_price_jcw) as offer_price_jcw FROM
(select ip.sku,
  case icd.channel_name when 'price1' then ip.offer_price else null end as offer_price_price1,
  case icd.channel_name when 'price2' then ip.offer_price else null end as offer_price_price2,
  case icd.channel_name when 'price3' then ip.offer_price else null end as offer_price_price3,
  case icd.channel_name when 'price4' then ip.offer_price else null end as offer_price_price4,
  case icd.channel_name when 'price6' then ip.offer_price else null end as offer_price_price6,
  case icd.channel_name when 'price8' then ip.offer_price else null end as offer_price_price8,
  case icd.channel_name when 'stylin' then ip.offer_price else null end as offer_price_stylin,
  case icd.channel_name when 'jcw' then ip.offer_price else null end as offer_price_jcw
  from
  base.item_prices ip
  left join temp_custom.item_channel_details icd on ip.channel_id = icd.channel_id and ip.sku = icd.sku
  where ip.feed_date = (select max(feed_date) from base.boomerang_catalog)
)
group by sku;


DROP TABLE if exists temp_custom.reversion_statistical_significance;
CREATE TABLE temp_custom.reversion_statistical_significance AS
  SELECT sku, channel_id, sum(reversion_flag) as reversion_flag FROM base.statistical_significance
  WHERE feed_date = (SELECT max(feed_date) from base.statistical_significance)
  GROUP BY 1,2
;

drop table if exists temp_custom.ebay_comp_data;
CREATE TABLE temp_custom.ebay_comp_data AS
(
  SELECT
  a.usap_productid,
  a.feed_date,
  greatest((c.po_cost + c.shipping_cost + c.variable_cost) / .95,
           (c.po_cost + c.shipping_cost) / .76, 3) AS min_margin,
  max(a.usap_current_price) as ebay_data_usap_price,
  max(usap_sales_last7days) as ebay_usap_sales_last7days,
  max(usap_sales_last30days) as ebay_usap_sales_last30days,
  min(case when (a.competitor_price+coalesce(a.competitor_shipping_price,0)) > a.usap_current_price then (a.competitor_price+coalesce(a.competitor_shipping_price,0)) end) as ebay_min_comp_price_above_usap,
  min(a.competitor_price+coalesce(a.competitor_shipping_price,0))                                  AS ebay_min_comp_price,
  max(a.competitor_price+coalesce(a.competitor_shipping_price,0))                                  AS ebay_max_comp_price,
  min(coalesce(a.competitor_shipping_price,0))                                  AS ebay_min_ship_price,
  max(coalesce(a.competitor_shipping_price,0))                                  AS ebay_max_ship_price,
  min(CASE WHEN (a.competitor_price+coalesce(a.competitor_shipping_price,0)) > greatest((c.po_cost + c.shipping_cost + c.variable_cost) / .95,
                                              (c.po_cost + c.shipping_cost) / .76, 3) + 0.1
    THEN (a.competitor_price+coalesce(a.competitor_shipping_price,0)) END)                           AS ebay_min_comp_above_margin

FROM (SELECT * FROM usautoparts.ebay_data WHERE feed_date = (SELECT max(feed_date) FROM usautoparts.ebay_data))  AS a
  LEFT JOIN (SELECT
               sku,
               channel_id,
               po_cost,
               shipping_cost,
               variable_cost,
               effective_date
             FROM base.item_cost) AS c
    ON a.usap_productid = c.sku AND a.feed_date = c.effective_date AND c.channel_id = 16
GROUP BY 1, 2, 3
);


DROP TABLE IF EXISTS temp_custom.hct_views_data;
create table temp_custom.hct_views_data AS
  (select sku, channel_id,
     sum(page_views) as agg_page_views,
     sum(unique_visits) as agg_unique_visits,
     sum(gross_revenue) as revenue
   from base.performance_order
   where transaction_date > (select max(transaction_date) - 28 from base.performance_order)
     AND page_views is not null
   group by 1,2);


DROP TABLE if exists temp_custom.hct_flags;
CREATE TABLE temp_custom.hct_flags as
select sku, channel_id, hct as custom_hct
  FROM
    (select *,
                  case when channel_id = 18 and percent_visits < 33 then 'Head'
                  when channel_id = 18 and percent_visits >= 33 and percent_visits < 67 then 'Core'
                  when channel_id != 18 and percent_views < 33 then 'Head'
                  when channel_id != 18 and percent_views >= 33 and percent_views < 67 then 'Core'
                  else 'Tail' end as hct

FROM
(select a.sku, a.channel_id, a.agg_page_views, a.agg_unique_visits, a.revenue,
        a.running_sum_views, b.total_views, a.running_sum_views/b.total_views*100 as percent_views,
        a.running_sum_visits, b.total_visits, a.running_sum_visits/b.total_visits*100 as percent_visits

  FROM
(select *,
      sum(agg_page_views) OVER (PARTITION BY channel_id ORDER BY agg_page_views DESC ROWS UNBOUNDED PRECEDING ) as running_sum_views,
      sum(agg_unique_visits) OVER (PARTITION BY channel_id ORDER BY agg_unique_visits DESC ROWS UNBOUNDED PRECEDING ) as running_sum_visits

  FROM temp_custom.hct_views_data
ORDER BY channel_id, agg_page_views DESC ) a
left join (select channel_id, sum(agg_page_views) as total_views, sum(agg_unique_visits) as total_visits from temp_custom.hct_views_data GROUP BY 1) b
on a.channel_id = b.channel_id
ORDER BY channel_id, percent_views));

drop table if exists temp_custom.margin_rate;
CREATE TABLE temp_custom.margin_rate as
  select a.sku, a.channel_id, a.merch_l2_name,((b.offer_price - c.po_cost)/b.offer_price) as margin_rate
from base.boomerang_catalog a
inner join  base.item_prices b
on a.sku = b.sku and a.channel_id = b.channel_id and b.offer_price >0
and a.feed_date = (select max(feed_date) from base.boomerang_catalog) and b.feed_date = (select max(feed_date) from base.item_prices)
inner join base.item_cost c
on a.sku = c.sku and a.channel_id = c.channel_id
and c.feed_date = (select max(feed_date) from base.item_cost );



drop table if exists temp_custom.conv_rate;
create table temp_custom.conv_rate as
select sku,channel_id, case when sum(gross_orders) is null then 0
                       when (channel_id != 18 and sum(gross_orders) is not null and (sum(page_views) = 0 or sum(page_views) is null)) then 1
                       when (channel_id = 18 and sum(gross_orders) is not null and (sum(unique_visits) = 0 or sum(unique_visits) is null)) then 1
                       when (channel_id != 18 and sum(page_views) > 0) then sum(gross_orders)/sum(page_views)
                       when (channel_id = 18 and sum(unique_visits) > 0) then sum(gross_orders)/sum(unique_visits)
                       end as conv_rate
from base.performance_order
   where transaction_date > (select max(transaction_date) - 14 from base.performance_order)
GROUP BY 1,2;


drop table if exists temp_custom.selfopt_skus;
create table temp_custom.selfopt_skus as
select sku,channel_id,tc from base.experiment_skus where (exp_id = 215 and channel_id = 14) or (exp_id =216 and channel_id = 18) ;


drop table if exists  temp_custom.median_margin_rate;
create table temp_custom.median_margin_rate as
select sku, channel_id, median(margin_rate) over(partition by channel_id, part_name) as median_margin_rate
from
(select c.sku, c.channel_id, c.part_name, b.margin_rate
from temp_custom.custom_catalogs c
left join temp_custom.margin_rate b
on c.sku = b.sku and c.channel_id = b.channel_id );


drop table if exists temp_custom.median_conv_rate;
create table temp_custom.median_conv_rate as
select sku, channel_id, median((case when conv_rate is null then 0 else conv_rate end)) over(partition by channel_id, part_name) as median_conv_rate
from
(select c.sku, c.channel_id, c.part_name, b.conv_rate
from temp_custom.custom_catalogs c
left join temp_custom.conv_rate  b
on c.sku = b.sku and c.channel_id = b.channel_id ) ;


drop table if exists temp_custom.self_learning_suggested_price ;
create table temp_custom.self_learning_suggested_price  as
select sku,channel_id, self_learning_suggested_price
from base.selfopt_history
where last_check_date = (select max(last_check_date) from base.selfopt_history) ;


drop table if exists temp_custom.reversion_lift_sig;
create table temp_custom.reversion_lift_sig as
select sku, channel_id, lift as reversion_lift, significance as reversion_significance
from base.reversion_history
where last_check_date = (select max(last_check_date) from base.reversion_history);

drop table if exists temp_custom.performance_weekly_rollups;
create table temp_custom.performance_weekly_rollups as
  select
  sku,
  channel_id,
  sum(case when transaction_date > (select dateadd(day, -7, max(transaction_date)) from base.performance_order) then gross_revenue end) as revenue_1weeks,
  sum(case when transaction_date > (select dateadd(day, -7, max(transaction_date)) from base.performance_order) then gross_units end) as units_1weeks,
  sum(case when transaction_date > (select dateadd(day, -7, max(transaction_date)) from base.performance_order) then (gross_revenue-gross_cost)  end) as margin_1weeks,
  sum(case when transaction_date > (select dateadd(day, -14, max(transaction_date)) from base.performance_order) then gross_revenue end) as revenue_2weeks,
  sum(case when transaction_date > (select dateadd(day, -14, max(transaction_date)) from base.performance_order) then gross_units end) as units_2weeks,
  sum(case when transaction_date > (select dateadd(day, -14, max(transaction_date)) from base.performance_order) then (gross_revenue-gross_cost)  end) as margin_2weeks,
  sum(case when transaction_date > (select dateadd(day, -30, max(transaction_date)) from base.performance_order) then gross_revenue end) as revenue_4weeks,
  sum(case when transaction_date > (select dateadd(day, -30, max(transaction_date)) from base.performance_order) then gross_units end) as units_4weeks,
  sum(case when transaction_date > (select dateadd(day, -30, max(transaction_date)) from base.performance_order) then (gross_revenue-gross_cost)  end) as margin_4weeks
from pa_metadata.measurement_performance_order_materialized
group by sku, channel_id;

drop table if exists temp.comp1;
create table temp.comp1 as(
    select client_sku as sku, comp_sku, comp_name, comp_name as comp_website, comp_offer_price as sas_offer_price, comp_shipping_price as sas_shipping_charge, comp_url as product_page_url, comp_in_stock::integer as unkn_in_stock, comp_scrape_date as scrape_date from cm.pricing_adjusted_cm where comp_name = (select value from base.dd_parameters where element = 'comp1' and created = (select max(created) from base.dd_parameters where element = 'comp1') and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm cpac)
));

drop table if exists temp.comp2;
create table temp.comp2 as(
    select client_sku as sku, comp_sku, comp_name, comp_name as comp_website, comp_offer_price as sas_offer_price, comp_shipping_price as sas_shipping_charge, comp_url as product_page_url, comp_in_stock::integer as unkn_in_stock, comp_scrape_date as scrape_date from cm.pricing_adjusted_cm where comp_name = (select value from base.dd_parameters where element = 'comp2' and created = (select max(created) from base.dd_parameters where element = 'comp2') and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm cpac)
));

drop table if exists temp.comp3;
create table temp.comp3 as(
    select client_sku as sku, comp_sku, comp_name, comp_name as comp_website, comp_offer_price as sas_offer_price, comp_shipping_price as sas_shipping_charge, comp_url as product_page_url, comp_in_stock::integer as unkn_in_stock, comp_scrape_date as scrape_date from cm.pricing_adjusted_cm where comp_name = (select value from base.dd_parameters where element = 'comp3' and created = (select max(created) from base.dd_parameters where element = 'comp3') and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm cpac)
));

drop table if exists temp.comp4;
create table temp.comp4 as(
    select client_sku as sku, comp_sku, comp_name, comp_name as comp_website, comp_offer_price as sas_offer_price, comp_shipping_price as sas_shipping_charge, comp_url as product_page_url, comp_in_stock::integer as unkn_in_stock, comp_scrape_date as scrape_date from cm.pricing_adjusted_cm where comp_name = (select value from base.dd_parameters where element = 'comp4' and created = (select max(created) from base.dd_parameters where element = 'comp4') and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm cpac)
));

drop table if exists temp.comp5;
create table temp.comp5 as(
    select client_sku as sku, comp_sku, comp_name, comp_name as comp_website, comp_offer_price as sas_offer_price, comp_shipping_price as sas_shipping_charge, comp_url as product_page_url, comp_in_stock::integer as unkn_in_stock, comp_scrape_date as scrape_date from cm.pricing_adjusted_cm where comp_name = (select value from base.dd_parameters where element = 'comp5' and created = (select max(created) from base.dd_parameters where element = 'comp5') and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm cpac)
));

drop table if exists temp.comp6;
create table temp.comp6 as(
    select client_sku as sku, comp_sku, comp_name, comp_name as comp_website, comp_offer_price as sas_offer_price, comp_shipping_price as sas_shipping_charge, comp_url as product_page_url, comp_in_stock::integer as unkn_in_stock, comp_scrape_date as scrape_date from cm.pricing_adjusted_cm where comp_name = 'realtruck.com' and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm cpac)
);

drop table if exists temp.comp7;
create table temp.comp7 as(
    select client_sku as sku, comp_sku, comp_name, comp_name as comp_website, comp_offer_price as sas_offer_price, comp_shipping_price as sas_shipping_charge, comp_url as product_page_url, comp_in_stock::integer as unkn_in_stock, comp_scrape_date as scrape_date from cm.pricing_adjusted_cm where comp_name = 'rockauto.com' and feed_date = (select max(feed_date) from cm.pricing_adjusted_cm cpac)
);

drop table if exists temp_custom.competition_all;
create table temp_custom.competition_all as(
select distinct b.sku,
    tc1.comp_sku as comp1_sku, tc1.comp_name as comp1_name, tc1.comp_website as comp1_website, tc1.sas_offer_price as comp1_price, tc1.sas_shipping_charge as comp1_shipping_charge, tc1.product_page_url as comp1_product_page_url, tc1.unkn_in_stock as comp1_in_stock,
    tc2.comp_sku as comp2_sku, tc2.comp_name as comp2_name, tc2.comp_website as comp2_website, tc2.sas_offer_price as comp2_price, tc2.sas_shipping_charge as comp2_shipping_charge, tc2.product_page_url as comp2_product_page_url, tc2.unkn_in_stock as comp2_in_stock,
    tc3.comp_sku as comp3_sku, tc3.comp_name as comp3_name, tc3.comp_website as comp3_website, tc3.sas_offer_price as comp3_price, tc3.sas_shipping_charge as comp3_shipping_charge, tc3.product_page_url as comp3_product_page_url, tc3.unkn_in_stock as comp3_in_stock,
    tc4.comp_sku as comp4_sku, tc4.comp_name as comp4_name, tc4.comp_website as comp4_website, tc4.sas_offer_price as comp4_price, tc4.sas_shipping_charge as comp4_shipping_charge, tc4.product_page_url as comp4_product_page_url, tc4.unkn_in_stock as comp4_in_stock,
    tc5.comp_sku as comp5_sku, tc5.comp_name as comp5_name, tc5.comp_website as comp5_website, tc5.sas_offer_price as comp5_price, tc5.sas_shipping_charge as comp5_shipping_charge, tc5.product_page_url as comp5_product_page_url, tc5.unkn_in_stock as comp5_in_stock,
    tc6.comp_sku as comp6_sku, tc6.comp_name as comp6_name, tc6.comp_website as comp6_website, tc6.sas_offer_price as comp6_price, tc6.sas_shipping_charge as comp6_shipping_charge, tc6.product_page_url as comp6_product_page_url, tc6.unkn_in_stock as comp6_in_stock,
    tc7.comp_sku as comp7_sku, tc7.comp_name as comp7_name, tc7.comp_website as comp7_website, tc7.sas_offer_price as comp7_price, tc7.sas_shipping_charge as comp7_shipping_charge, tc7.product_page_url as comp7_product_page_url, tc7.unkn_in_stock as comp7_in_stock

    from (select (max(bc.creation_date)) max_date, bc.sku from base.boomerang_catalog bc group by bc.sku) b
    left join temp.comp1 tc1 on b.sku=tc1.sku and tc1.scrape_date >= (CURRENT_DATE - (select value from base.dd_parameters where element = 'comp_validity' and created = (select max(created) from base.dd_parameters where element = 'comp_validity'))::INTERVAL)
    left join temp.comp2 tc2 on b.sku=tc2.sku and tc2.scrape_date >= (CURRENT_DATE - (select value from base.dd_parameters where element = 'comp_validity' and created = (select max(created) from base.dd_parameters where element = 'comp_validity'))::INTERVAL)
    left join temp.comp3 tc3 on b.sku=tc3.sku and tc3.scrape_date >= (CURRENT_DATE - (select value from base.dd_parameters where element = 'comp_validity' and created = (select max(created) from base.dd_parameters where element = 'comp_validity'))::INTERVAL)
    left join temp.comp4 tc4 on b.sku=tc4.sku and tc4.scrape_date >= (CURRENT_DATE - (select value from base.dd_parameters where element = 'comp_validity' and created = (select max(created) from base.dd_parameters where element = 'comp_validity'))::INTERVAL)
    left join temp.comp5 tc5 on b.sku=tc5.sku and tc5.scrape_date >= (CURRENT_DATE - (select value from base.dd_parameters where element = 'comp_validity' and created = (select max(created) from base.dd_parameters where element = 'comp_validity'))::INTERVAL)
    left join temp.comp6 tc6 on b.sku=tc6.sku and tc6.scrape_date >= (CURRENT_DATE - (select value from base.dd_parameters where element = 'comp_validity' and created = (select max(created) from base.dd_parameters where element = 'comp_validity'))::INTERVAL)
    left join temp.comp7 tc7 on b.sku=tc7.sku and tc7.scrape_date >= (CURRENT_DATE - (select value from base.dd_parameters where element = 'comp_validity' and created = (select max(created) from base.dd_parameters where element = 'comp_validity'))::INTERVAL)
);

drop table if exists temp_custom.agg_comp;
create table temp_custom.agg_comp as (
    select sku,
    case
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp1_price,999999) then comp1_price
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp2_price,999999) then comp2_price
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp3_price,999999) then comp3_price
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp4_price,999999) then comp4_price
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp5_price,999999) then comp5_price
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp6_price,999999) then comp6_price
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp7_price,999999) then comp7_price
    end as min_comp_price,
    case
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp1_price,999999) then comp1_name
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp2_price,999999) then comp2_name
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp3_price,999999) then comp3_name
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp4_price,999999) then comp4_name
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp5_price,999999) then comp5_name
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp6_price,999999) then comp6_name
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp7_price,999999) then comp6_name
    end as min_comp_name,
    case
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp1_price,999999) then comp1_product_page_url
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp2_price,999999) then comp2_product_page_url
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp3_price,999999) then comp3_product_page_url
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp4_price,999999) then comp4_product_page_url
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp5_price,999999) then comp5_product_page_url
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp6_price,999999) then comp6_product_page_url
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp7_price,999999) then comp7_product_page_url
    end as min_comp_url,
    case
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp1_price,999999) then comp1_shipping_charge
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp2_price,999999) then comp2_shipping_charge
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp3_price,999999) then comp3_shipping_charge
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp4_price,999999) then comp4_shipping_charge
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp5_price,999999) then comp5_shipping_charge
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp6_price,999999) then comp6_shipping_charge
        when least(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp7_price,999999) then comp7_shipping_charge
    end as min_comp_shipping,
    case
        when greatest(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp1_price,999999) then comp1_price
        when greatest(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp2_price,999999) then comp2_price
        when greatest(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp3_price,999999) then comp3_price
        when greatest(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp4_price,999999) then comp4_price
        when greatest(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp5_price,999999) then comp5_price
        when greatest(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp6_price,999999) then comp6_price
        when greatest(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) = coalesce(comp7_price,999999) then comp7_price
    end as max_comp_price
from temp_custom.competition_all
where coalesce(comp1_price,comp2_price,comp3_price,comp4_price,comp5_price,comp6_price,comp7_price) is not null
);


drop table if exists temp_custom.brand_part_markup_details ;
create table temp_custom.brand_part_markup_details as
 select
 ic.sku,
 ic.channel_id,
 ic.po_cost,
 brand_name,
 cc.part_name,
 case when ic.channel_id = 11 then bmp.apw
 when ic.channel_id = 12 then bmp.ps
 when ic.channel_id = 13 then bmp.dss
 when ic.channel_id = 14 then bmp.cp
 when ic.channel_id = 15 then 0
 when ic.channel_id = 16 then 0
 when ic.channel_id = 17 then bmp.sty
 when ic.channel_id = 18 then bmp.jcw
 end as new_markup
from base.item_cost ic, usautoparts.markup_bucket_part_brand bmp, usautoparts.custom_catalogs cc, base.boomerang_catalog bc
where ic.po_cost between bmp.bucket_from and bmp.bucket_to
and cc.sku = ic.sku and cc.channel_id = ic.channel_id
and bc.sku = ic.sku and bc.channel_id = ic.channel_id
  and cc.part_name = bmp.part_name
  and bc.brand = bmp.brand_name
and cc.feed_date = (select max(feed_date) from base.boomerang_catalog)
and bc.feed_date = (select max(feed_date) from base.boomerang_catalog)
and ic.feed_date = (select max(feed_date) from base.boomerang_catalog)
and bmp.feed_date = (select max(feed_date) from usautoparts.markup_bucket_part_brand)
;


drop table if exists usautoparts.custom_data_dictionary;
create table usautoparts.custom_data_dictionary as
  select
      tc_cc.sku,
      tc_cc.channel_id,
      tc_cc.feed_date,
      tc_cc.sku_status,
      tc_cc.part_name,
      tc_cc.model,
      tc_cc.sku_mover_type,
      tc_cc.clearance_type,
      tc_cc.worldpac_flag,
      tc_cc.truck_freight_flag,
      tc_cc.map_type,
      tc_cc.stockship,
      tc_cc.delivery_channel,
      tc_cc.sku_number,
      tc_cc.ford_patent,
      tc_cc.usap_sku,
      tc_cc.core_cost,
      tc_cc.core_price,
      tc_cc.po_cost_price,
      tc_cc.fifo_cost,
      tc_cc.ds_cost,
      tc_cc.ss_cost,
      tc_cc.shipping_revenue,
      tc_cc.handling_revenue,
      tc_cc.extra_shipping,
	  tc_cc.pricing_status,
	  tc_cc.team_name,
      tc_r6.revenue_6_months,
      tc_hc.gross_handling_charges,
      tc_tmt.target_margin_today,
      tc_tmy.target_margin_yesterday,
      tc_psmc.parent_sku_min_cost,
      tc_psmp.parent_sku_min_price,
      tc_dfcc.days_for_current_cost,
      tc_cpd.days_for_current_price,
      tc_cpd.norm_sales_current_price,
      tc_cpd.norm_units_current_price,
      tc_cpd.norm_vcm_current_price,
      tc_ppd.days_for_previous_price,
      tc_ppd.previous_price,
      tc_ppd.norm_sales_previous_price,
      tc_ppd.norm_units_previous_price,
      tc_ppd.norm_vcm_previous_price,
      tc_icd.channel_name,
      tc_srdt.shipping_revenue1_today,
      tc_srdt.handling_revenue1_today,
      tc_srdt.shipping_revenue3_today,
      tc_srdt.handling_revenue3_today,
      tc_srdy.shipping_revenue1_yesterday,
      tc_srdy.handling_revenue1_yesterday,
      tc_srdy.shipping_revenue3_yesterday,
      tc_srdy.handling_revenue3_yesterday,
      case tc_icd.channel_name when 'jcw' then 1.03 else 1.00 end as channel_premium,
      tc_ipcmd.mark_up,
      case when tc_ccf.cost_changed_flag = 1 then 0 when (tc_rfp.current_price = tc_rfp.second_last_price and tc_rfp.cost_on_second_last_price_date <> current_cost) then 1 else 0 end as reversion_flag,
      tc_cc.kit,
      tc_cc.iswarranty,
      tc_cc.vendor_name,
      tc_cop.offer_price_price1,
      tc_cop.offer_price_price2,
      tc_cop.offer_price_price3,
      tc_cop.offer_price_price4,
      tc_cop.offer_price_price6,
      tc_cop.offer_price_price8,
      tc_cop.offer_price_stylin,
      tc_cop.offer_price_jcw,
      tc_gmd.global_markup,
      tc_r6ac.revenue_6_months_all_channels,
	  case when tc_ss.reversion_flag is not null then tc_ss.reversion_flag else 0 end as reversion_flag_test,
	  tc_ecd.ebay_data_usap_price,
      tc_ecd.ebay_min_comp_price,
      tc_ecd.ebay_max_comp_price,
      tc_ecd.ebay_min_comp_above_margin,
	  tc_ecd.ebay_usap_sales_last7days,
	  tc_ecd.ebay_usap_sales_last30days,
	  tc_ecd.ebay_min_comp_price_above_usap,
	  case when tc_hct.custom_hct is not null then custom_hct else 'Zero' end as custom_hct,
	  tc_mr.margin_rate,
	  tc_cr.conv_rate,
	  tc_mmr.median_margin_rate,
	  tc_mcr.median_conv_rate,
	  tc_stc.tc as test_control,
	  tc_slsp.self_learning_suggested_price,
	  tc_rls.reversion_lift,
	  tc_rls.reversion_significance,
	  tc_pwr.revenue_1weeks,
	  tc_pwr.units_1weeks,
	  tc_pwr.margin_1weeks,
	  tc_pwr.revenue_2weeks,
	  tc_pwr.units_2weeks,
	  tc_pwr.margin_2weeks,
	  tc_pwr.revenue_4weeks,
	  tc_pwr.units_4weeks,
	  tc_pwr.margin_4weeks,
	  tc_c.comp6_sku, tc_c.comp6_name, tc_c.comp6_website, tc_c.comp6_price, tc_c.comp6_shipping_charge, tc_c.comp6_product_page_url, tc_c.comp6_in_stock,
	  tc_c.comp7_sku, tc_c.comp7_name, tc_c.comp7_website, tc_c.comp7_price, tc_c.comp7_shipping_charge, tc_c.comp7_product_page_url, tc_c.comp7_in_stock,
	  tc_ac.min_comp_price, tc_ac.min_comp_name, tc_ac.min_comp_url, tc_ac.min_comp_shipping, tc_ac.max_comp_price,
    tc_bpmd.new_markup as part_brand_channel_markup

   from temp_custom.custom_catalogs as tc_cc
   left join temp_custom.revenue_6_months as tc_r6 on tc_cc.sku = tc_r6.sku and tc_cc.channel_id = tc_r6.channel_id
   left join temp_custom.revenue_6_months_all_channels as tc_r6ac on tc_cc.sku = tc_r6ac.sku
   left join temp_custom.handling_charges as tc_hc on tc_cc.sku = tc_hc.sku and tc_cc.channel_id = tc_hc.channel_id
   left join temp_custom.target_margin_today as tc_tmt on tc_cc.sku = tc_tmt.sku and tc_cc.channel_id = tc_tmt.channel_id
   left join temp_custom.target_margin_yesterday as tc_tmy on tc_cc.sku = tc_tmy.sku and tc_cc.channel_id = tc_tmy.channel_id
   left join temp_custom.parent_sku_min_cost as tc_psmc on tc_cc.sku = tc_psmc.child_sku and tc_cc.channel_id = tc_psmc.channel_id
   left join temp_custom.parent_sku_min_price as tc_psmp on tc_cc.sku = tc_psmp.child_sku and tc_cc.channel_id = tc_psmp.channel_id
   left join temp_custom.days_for_current_cost as tc_dfcc on tc_cc.sku = tc_dfcc.sku and tc_cc.channel_id = tc_dfcc.channel_id
   left join temp_custom.current_price_details as tc_cpd on tc_cc.sku = tc_cpd.sku and tc_cc.channel_id = tc_cpd.channel_id
   left join temp_custom.previous_price_details as tc_ppd on tc_cc.sku = tc_ppd.sku and tc_cc.channel_id = tc_ppd.channel_id
   left join temp_custom.item_channel_details as tc_icd on tc_cc.sku = tc_icd.sku and tc_cc.channel_id = tc_icd.channel_id
   left join temp_custom.shipping_revenue_details_today as tc_srdt on tc_cc.sku = tc_srdt.sku and tc_cc.channel_id = tc_srdt.channel_id
   left join temp_custom.shipping_revenue_details_yesterday as tc_srdy on tc_cc.sku = tc_srdy.sku and tc_cc.channel_id = tc_srdy.channel_id
   left join temp_custom.item_po_cost_markup_details as tc_ipcmd on tc_cc.sku = tc_ipcmd.sku and tc_cc.channel_id = tc_ipcmd.channel_id
   left join temp_custom.cost_changed_flag as tc_ccf on tc_cc.sku = tc_ccf.sku and tc_cc.channel_id = tc_ccf.channel_id
   left join temp_custom.reversion_flag_parameters as tc_rfp on tc_cc.sku = tc_rfp.sku and tc_cc.channel_id = tc_rfp.channel_id
   left join temp_custom.global_markup_details as tc_gmd on tc_cc.sku = tc_gmd.sku and tc_cc.channel_id = tc_gmd.channel_id
   left join temp_custom.channel_offer_prices as tc_cop on tc_cop.sku = tc_cc.sku
   left join temp_custom.reversion_statistical_significance as tc_ss on tc_ss.sku = tc_cc.sku and tc_ss.channel_id = tc_cc.channel_id
   left join temp_custom.ebay_comp_data as tc_ecd on tc_ecd.usap_productid = tc_cc.sku
   left join temp_custom.hct_flags as tc_hct on tc_hct.sku = tc_cc.sku and tc_hct.channel_id = tc_cc.channel_id
   left join temp_custom.margin_rate as tc_mr on tc_cc.sku = tc_mr.sku and tc_cc.channel_id = tc_mr.channel_id
   left join temp_custom.conv_rate as tc_cr on tc_cc.sku = tc_cr.sku and tc_cc.channel_id = tc_cr.channel_id
   left join temp_custom.median_margin_rate as tc_mmr on tc_cc.sku = tc_mmr.sku and tc_cc.channel_id = tc_mmr.channel_id
   left join temp_custom.median_conv_rate as tc_mcr on tc_cc.sku = tc_mcr.sku and tc_cc.channel_id = tc_mcr.channel_id
   left join temp_custom.selfopt_skus as tc_stc on tc_cc.sku = tc_stc.sku and tc_cc.channel_id = tc_stc.channel_id
   left join temp_custom.self_learning_suggested_price as tc_slsp on tc_cc.sku = tc_slsp.sku and tc_cc.channel_id = tc_slsp.channel_id
   left join temp_custom.reversion_lift_sig as tc_rls on tc_cc.sku = tc_rls.sku and tc_cc.channel_id = tc_rls.channel_id
   left join temp_custom.performance_weekly_rollups as tc_pwr on tc_cc.sku = tc_pwr.sku and tc_cc.channel_id = tc_pwr.channel_id
   left join temp_custom.competition_all as tc_c on tc_cc.sku = tc_c.sku
   left join temp_custom.agg_comp tc_ac on tc_cc.sku = tc_ac.sku
   left join temp_custom.brand_part_markup_details tc_bpmd on tc_cc.sku = tc_bpmd.sku and tc_cc.channel_id = tc_bpmd.channel_id
;