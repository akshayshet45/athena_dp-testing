\set ON_ERROR_STOP 1
\timing on

drop table if exists temp.sku_brand_catalog_temp;
create table temp.sku_brand_catalog_temp as
select sku, brand, sku_status, po_cost from base.data_dictionary
where feed_date = (select max(feed_date) from base.data_dictionary)
GROUP BY 1,2,3,4;

drop table if exists temp.sister_skus_brandmatched;
create table temp.sister_skus_brandmatched as
(select parent_sku, child_sku, b.brand, b.sku_status as parent_status, c.sku_status as child_status, b.po_cost as parent_cost, c.po_cost as child_cost,
case when b.po_cost = 0 and c.po_cost = 0 then 0
when b.po_cost = 0 and c.po_cost > 0 then 1
else abs((c.po_cost/b.po_cost)-1) end as cost_diff, a.feed_date
from base.sku_relationships a
inner join temp.sku_brand_catalog_temp b
on a.parent_sku = b.sku
inner join temp.sku_brand_catalog_temp c
on a.child_sku = c.sku
where
feed_date = (select max(feed_date) from base.sku_relationships) and
relationship_id = 3 and
b.brand = c.brand
);

insert into base.sister_sku_relationships(sku,relationship_id,set_identifier,feed_date,creation_date) select parent_sku, 3, child_sku,feed_date, GETDATE() from temp.sister_skus_brandmatched
where parent_status not in ('7. Obsolete/Retired', '6. Clearance')
and child_status not in ('7. Obsolete/Retired', '6. Clearance')
and cost_diff <= 0.3;