create table if not exists client.req_load_item_price (  channel_id      integer  ,                    
 sku             character varying(500)       ,
 prc_type_cd     character varying(500)       ,
 offer_price     numeric(10,2)                ,
 effective_date  timestamp without time zone  ,
 reg_price       numeric(10,2)                ,
 feed_date       timestamp without time zone  ,
 creation_date   timestamp without time zone );
 
 