create table if not exists client.bac_req_load_item_cost ( sku                        character varying(65535),     
 channel_id                 integer                      ,
 effective_date             timestamp without time zone,  
 po_cost                    numeric(10,2)                ,
 shipping_cost              numeric(10,2)              ,  
 variable_cost              numeric(10,2)                ,
 vendor_subsidized_po_cost  numeric(10,2)      ,          
 feed_date                  timestamp without time zone,  
 creation_date              timestamp without time zone);
 
 
 create table if not exists client.bac_req_store_inventory ( channel_id integer not null,
 sku                character varying(500)       not null,
 inventory_type_cd  character varying(500)       ,
 qty                numeric(10,2)                ,
 inventory_date     timestamp without time zone  ,
 feed_date          timestamp without time zone  not null,
 creation_date      timestamp without time zone   ,
PRIMARY KEY (sku,channel_id,feed_date))
distkey(feed_date)
sortkey(feed_date, sku, channel_id);


create table if not exists client.blocked_item ( sku             character varying(500),       
 division        character varying(500)       ,
 blocked_reason  character varying(500)       ,
 feed_date       timestamp without time zone  not null,
 creation_date   timestamp without time zone );
 
 
 
 create table if not exists client.custom_performance (  sku                  character varying(500),       
 channel_id           integer                      ,
 transaction_date     timestamp without time zone  ,
 fiscal_wk_nbr        integer                      ,
 cust_interaction_id  character varying(500)       ,
 str_format           character varying(500)       ,
 item_id              character varying(500)       ,
 type_cd              character varying(500)       ,
 cost_amt             numeric(10,2)                ,
 sell_type_cd         character varying(500)       ,
 zip_cd               character varying(500)       ,
 reg_price            numeric(10,2)                ,
 price                numeric(10,2)                ,
 md_amt               numeric(10,2)                ,
 sum_sywr             numeric(10,2)                ,
 feed_date            timestamp without time zone  ,
 creation_date        timestamp without time zone  );
 
 
 create table if not exists client.full_load_boomerang_catalog (   sku                character varying(65535),     
 title              character varying(65535)     ,
 shc_item_id        character varying(65535)     ,
 shc_item_desc      character varying(65535)     ,
 prc_prc_link_id    character varying(65535)     ,
 prc_prc_link_desc  character varying(65535)     ,
 merch_l5_id        character varying(65535)     ,
 merch_l5_name      character varying(65535)     ,
 merch_l4_id        character varying(65535)     ,
 merch_l4_name      character varying(65535)     ,
 merch_l3_id        character varying(65535)     ,
 merch_l3_name      character varying(65535)     ,
 merch_l2_id        character varying(65535)     ,
 merch_l2_name      character varying(65535)     ,
 merch_l1_id        character varying(65535)     ,
 merch_l1_name      character varying(65535)     ,
 corp_reporting_id  character varying(65535)     ,
 feed_date          timestamp without time zone  ,
 creation_date      timestamp without time zone   );
 
 
 
 create table if not exists client.req_load_offer_flexibility (  is_offshore            character varying(500) ,      
 shc_channel            character varying(500)       ,
 format_id              character varying(500)       ,
 offer_id               character varying(500)       ,
 offer_name             character varying(500)       ,
 flexible_flag          character varying(500)       ,
 floor_min_discount     numeric(10,2)                ,
 ceiling_max_discount   numeric(10,2)                ,
 members_only           character varying(500)       ,
 offer_start_date_time  timestamp without time zone  ,
 offer_end_date_time    timestamp without time zone  ,
 channel_id             integer                      ,
 sku                    character varying(500)       not null,
 hier_level1            character varying(500)       ,
 hier_level2            character varying(500)       ,
 hier_level3            character varying(500)       ,
 hier_level4            character varying(500)       ,
 hier_level5            character varying(500)       ,
 dl_typ_cd              character varying(500)       ,
 dl_val                 numeric(10,2)                ,
 dl_qty                 numeric(10,2)                ,
 is_reg_fl              character varying(2)         ,
 is_promo_fl            character varying(2)         ,
 is_clear_fl            character varying(2)         ,
 is_pictured            character varying(2)         ,
 deal_msg               character varying(600)       ,
 feed_date              timestamp without time zone  ,
 creation_date          timestamp without time zone );

 

 
 
 
 