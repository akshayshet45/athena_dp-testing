\set ON_ERROR_STOP 1

-- Update sim_run_output
--delete from kmart.sim_run_output where created = (select max(creation_date) from base.boomerang_suggested_price_change_history);

--insert into kmart.sim_run_output
--  select sku, channel_id, suggested_price, creation_date from base.boomerang_suggested_price_change_history
--where creation_date = (select max(creation_date) from base.boomerang_suggested_price_change_history);

-- Explode prices
drop table if exists kmart.latest_po_output;
create table kmart.latest_po_output as
	select h.sku as ksn, lpc.store_num as channel_id, suggestedPrice
	from
	(select * from kmart.sim_run_output where
	created = (select max(created) from kmart.sim_run_output)) h
	inner join
	kmart.latest_pricing_channels lpc
	on lpc.sku = h.sku
	and h.channel_id = lpc.pricing_channel_id;

-- Get clearance items
drop table if exists kmart.latest_clearance;
create table kmart.latest_clearance as
select distinct sku, channel_id from kmart.temp_v_latest_prices where prc_type_cd = 'X';

-- Get price link
drop table if exists kmart.latest_pl_output;
create table kmart.latest_pl_output as
	select ksn, prc_prc_link_id as price_link, channel_id, suggestedPrice
	from
	kmart.latest_po_output o
	inner join
	kmart.latest_catalog_v2 k
	on o.ksn = k.sku;

-- Remove invalid prices and get PL price -- Need to add offer manager to this
drop table if exists kmart.latest_pl_output_postrules;
create table kmart.latest_pl_output_postrules as
	select price_link, o.channel_id, min(suggestedPrice) as suggestedPrice
	from
	kmart.latest_pl_output o
	inner join
	kmart.latest_cost_price_v2 c
	on o.ksn = c.sku
	and o.channel_id = c.channel_id
	where suggestedPrice < reg_price
group by 1,2;

-- Explode out PL IDs
drop table if exists kmart.latest_ksn_output_postrules;
create table kmart.latest_ksn_output_postrules as
select k.sku as ksn, o.channel_id, 
	case when suggestedPrice < 100 then round(suggestedPrice,1)-0.01
    	when suggestedPrice < 1000 then round(suggestedPrice/10,1)*10-0.01
    	else round(suggestedPrice/100,1)*100-0.01 end as suggestedPrice
	from
	kmart.latest_pl_output_postrules o
	inner join
	kmart.latest_catalog_v2 k
	on o.price_link = k.prc_prc_link_id;

--- PL IDs explode out
drop table if exists kmart.latest_sku_output;
create table kmart.latest_sku_output as
	select ksn, op.channel_id, reg_price,
	case when suggestedPrice > reg_price then reg_price else suggestedPrice end as suggestedPrice
	from
	kmart.latest_ksn_output_postrules op
	inner join
	kmart.temp_v_latest_cost c
	on op.ksn = c.sku
	and op.channel_id = c.channel_id
	left join
	kmart.latest_blocks b
	on op.ksn = b.sku
    left join kmart.latest_clearance cl
    on op.ksn = cl.sku
    and op.channel_id = cl.channel_id
    left join kmart.map_data m
    on op.ksn = m.sku
    left join (select distinct sku from kmart.latest_wos) w
    on op.ksn = w.sku
	where (b.sku is null and cl.sku is null and m.sku is null) or w.sku is not null;

--- Remove items price linked to blocks
delete from kmart.latest_sku_output where ksn in
(select distinct sku from kmart.latest_catalog_v2
where prc_prc_link_id in
      (select prc_prc_link_id from kmart.latest_catalog_v2
where sku in (select distinct sku from kmart.latest_blocks)));

-- Output query
drop table if exists kmart.price_output_file;
create table kmart.price_output_file as
select current_date as dt, ksn, shc_item_id, merch_l1_id, channel_id as store_num, suggestedPrice, 
current_date - extract(weekday from current_date) + 7 as from_date, 
current_date - extract(weekday from current_date) + 13 as to_date
from kmart.latest_sku_output p
	inner join kmart.latest_catalog_v2 c
	on p.ksn = c.sku;