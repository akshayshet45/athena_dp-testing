\set ON_ERROR_STOP 1

create temp table stage (like kmart.bac_req_load_item_cost); 
insert into stage
select * from kmart.bac_req_load_item_cost where feed_date= (Select max(feed_date) from kmart.bac_req_load_item_cost);

begin transaction;

delete from kmart.temp_v_latest_cost
using stage 
where kmart.temp_v_latest_cost.sku = stage.sku and kmart.temp_v_latest_cost.channel_id = stage.channel_id; 

insert into kmart.temp_v_latest_cost
select sku,channel_id,po_cost,shipping_cost from stage;

end transaction;
drop table stage;



create temp table stage_price (like kmart.req_load_item_price); 
insert into stage_price 
select * from kmart.req_load_item_price where feed_date= (Select max(feed_date) from kmart.req_load_item_price);

begin transaction;

delete from kmart.temp_v_latest_prices 
using stage_price
where kmart.temp_v_latest_prices.sku = stage_price.sku and kmart.temp_v_latest_prices.channel_id = stage_price.channel_id; 

insert into kmart.temp_v_latest_prices 
select * from stage_price;

end transaction;

drop table stage_price;




create temp table stage_inventory (like kmart.bac_req_store_inventory); 
insert into stage_inventory 
select * from kmart.bac_req_store_inventory where feed_date= (Select max(feed_date) from kmart.bac_req_store_inventory);

begin transaction;

delete from kmart.temp_v_latest_inventory
using stage_inventory
where kmart.temp_v_latest_inventory.sku = stage_inventory.sku and kmart.temp_v_latest_inventory.channel_id = stage_inventory.channel_id; 

insert into kmart.temp_v_latest_inventory 
select * from stage_inventory;

end transaction;

drop table stage_inventory;
