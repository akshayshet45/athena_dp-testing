truncate table kmart.latest_catalog_v2 ;
insert into kmart.latest_catalog_v2
select k.*, case when m.merch_l1_id is not null then 'Major' else 'Minor' end as category_desc
from
kmart.full_load_boomerang_catalog k
left join
kmart.major_categories m
on k.merch_l1_id = m.merch_l1_id
and k.merch_l2_id = m.merch_l2_id
and k.merch_l4_id = m.merch_l4_id
where k.feed_date = (select max(feed_date) 
from kmart.full_load_boomerang_catalog);

-- Update sister SKUs
insert into base.sister_sku_relationships(sku,relationship_id,set_identifier,feed_date,creation_date)
select distinct sku, 1, prc_prc_link_id, feed_date, current_date
from
  kmart.latest_catalog_v2;

-- Get latest blocked item data
drop table if exists kmart.latest_blocks;
create table kmart.latest_blocks as
select distinct sku from kmart.blocked_item
where feed_date = (select max(feed_date) 
from kmart.blocked_item) and format_id = 8
union
select distinct sku from kmart.manual_blocks
where from_date <= current_date - extract(weekday from current_date) + 7
and to_date >= current_date - extract(weekday from current_date) + 13;

-- Get latest TC data
drop table if exists kmart.latest_tc;
create table kmart.latest_tc as
select * from kmart.test_control where feed_date = (select max(feed_date) from kmart.test_control);

-- Get MAP items
drop table if exists kmart.latest_map;
create table kmart.latest_map as
select distinct sku from kmart.map_data 
where current_date between from_date and to_date;

  -- Create data dictionary

drop table if exists kmart.dd_temp1;
create table kmart.dd_temp1 as
    select ch.*, merch_l1_id
       from kmart.latest_pricing_channels ch
       inner join kmart.latest_catalog_v2 c
       on c.sku = ch.sku;


drop table if exists kmart.dd_t1;
create table kmart.dd_t1 as
    select distinct sku, pricing_channel_id, tc, po_cost, offer_price,
        floor_min_discount, ceiling_max_discount, price_code, reg_price
        from
        kmart.dd_temp1 cc
       inner join kmart.latest_tc tc
       on cc.store_num = tc.channel_id
       and cc.merch_l1_id = tc.merch_l1_id;


delete from base.data_dictionary where dd_date = current_date;
insert into base.data_dictionary
  select
    bc.sku,
    bc.title,
    pricing_channel_id as channel_id,
    null as product_url,
    null as image_url,
    null as description,
    null as brand,
    null as upc,
    null as ean,
    null as isbn,
    null as manufacturer,
    null as mpn,
    null as line,
    null as is_marketplace,
    null as is_privatelabel,
    bc.merch_l1_id,
    bc.merch_l1_name,
    bc.merch_l2_id,
    bc.merch_l2_name,
    bc.merch_l3_id,
    bc.merch_l3_name,
    bc.merch_l4_id,
    bc.merch_l4_name,
    bc.merch_l5_id,
    bc.merch_l5_name,
    bc.shc_item_id,
    bc.prc_prc_link_id as price_link,
    pricing_channel_id,
    coalesce(w.price_code,ch.price_code) as pricec_code,
    price1 as price_lowest,
    coalesce(price2,price1) as price_middle,
    coalesce(price3,price2,price1) as  price_highest,
    least((case when (1 - (1 - po_cost/offer_price)*abs(coalesce(ce.elasticity, sle.elasticity, de.elasticity)))/(2*abs(coalesce(ce.elasticity, sle.elasticity, de.elasticity))) < 0
        then (least(0.05, abs((1 - (1 - po_cost/offer_price)*abs(coalesce(ce.elasticity, sle.elasticity, de.elasticity)))/(2*abs(coalesce(ce.elasticity, sle.elasticity, de.elasticity)))), 20/offer_price)*-1)
        else (least(0.05, abs((1 - (1 - po_cost/offer_price)*abs(coalesce(ce.elasticity, sle.elasticity, de.elasticity)))/(2*abs(coalesce(ce.elasticity, sle.elasticity, de.elasticity)))), 20/offer_price)*1)
    end + 1) * offer_price, 0.95*reg_price) as  price_recommended,
    floor_min_discount as min_discount, ceiling_max_discount as max_discount,
    current_date as dd_date,
    tc as test_control,
    case when (bl.sku is not null or m.sku is not null) and w.sku is null then
    TRUE else FALSE end as blocked_flag,
    case when w.sku is not null then TRUE else FALSE end as is_wos, 
    case when floor(pricing_channel_id/1000) = 30009 then 3
    when floor(pricing_channel_id/1000) = 30008 then 2
    when floor(pricing_channel_id/1000) = 30007 then 1
    else 0 end as st_level,
    coalesce(ce.elasticity, de.elasticity) as elasticity,
    ch.po_cost as cost, ch.offer_price
    from
    kmart.latest_catalog_v2 bc
    inner join
      kmart.dd_t1 ch
      on bc.sku = ch.sku
    left join
    kmart.latest_blocks bl
    on bc.sku = bl.sku
    left join kmart.div_elasticity de
    on bc.merch_l1_id = de.div
    left join kmart.class_elasticity ce
    on bc.merch_l1_id = ce.div
            and bc.merch_l4_id = ce.category
            and bc.merch_l5_id = ce.subcategory
        left join kmart.subline_elasticity sle
    on bc.merch_l1_id = sle.div
            and bc.merch_l4_id = sle.category
    left join
    kmart.latest_wos w
    on bc.sku = w.sku
    left join kmart.latest_map m
    on bc.sku = m.sku
    where offer_price > 0;


-- Create cost data subset
drop table if exists kmart.latest_cost_subset;
create table kmart.latest_cost_subset as
    select c.* from 
    kmart.temp_v_latest_cost c
    inner join
    kmart.latest_catalog_v2 c2
    on c.sku = c2.sku
    where c2.merch_l1_id in
    (select distinct merch_l1_id
        from base.po_data_dictionary);
