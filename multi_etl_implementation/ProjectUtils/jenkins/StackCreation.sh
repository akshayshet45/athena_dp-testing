#!/bin/bash
#@author ravi@boomerangcommerce.com
# Asg creation for IS-Projects and auto-deploy of ETLServices

set -e

function error_exit()
{
	if [[ $1 = 0 ]]; then
		echo "Successful"
		exit 0
	else 
		echo "Something went wrong!!"
		exit 1
	fi
}

function create_asg()
{
	client=$1
	echo "ClinetName is : " $client
	CWD=`pwd`
	echo "Current working directory is : " $CWD
	echo "Creating is-"$1"-asg ..."
	aws autoscaling create-auto-scaling-group --auto-scaling-group-name "is-$client-asg" --launch-configuration-name "is-prod-asg-dmoff" --min-size 1 --max-size 1 --desired-capacity 1 --vpc-zone-identifier "subnet-f08ea995" --tags Key=Name,Value=is-$client-asg Key=system,Value=is Key=own,Value=is-team Key=env,Value=prod Key=client,Value=$client && sleep 60
	echo "Done!! is-$client-asg has been created!!"
}

function validate_asg_creation()
{
	asgName=$1
	echo "Verifying the completion of ASG "$asgName
	instanceID=$(aws autoscaling describe-auto-scaling-groups --auto-scaling-group-name $asgName --no-paginate |/usr/bin/jq .AutoScalingGroups[0].Instances[0].InstanceId)
	echo "Instance ID : "$instanceID
	instanceLC=$(aws autoscaling describe-auto-scaling-groups --auto-scaling-group-name $asgName --no-paginate |/usr/bin/jq .AutoScalingGroups[0].Instances[0].LifecycleState)
	echo "Instance LifecycleState: "$instanceLC
	
	echo $instanceID 
	eval "$2=$instanceID"
	echo "Done!! ASG is up and running"
}

function checkSSHConnection()
{
	echo "Verifying ssh connectivity to host "$1
	sleep 30;
	ssh -o StrictHostKeyChecking=no ubuntu@$1 "ls ~;exit" || checkSSHConnection $1
	echo "Done!! ssh connection Successful"
}

function getIPaddress()
{
	echo "Getting ip address of "$1
	instanceID=$1
	publicIp=$(aws ec2 describe-instances --instance-ids $1 |/usr/bin/jq .Reservations[0].Instances[0].PublicIpAddress)
	eval "$2=$publicIp"
	echo $publicIp
}

function deploy_etlservices()
{
	getIPaddress $1 ip
    
	echo "Sleeping for 120 sec for ansible run completion"
	sleep 120;
    
	echo "Getting latest QA signoff version for ETLServices.."
	version=$(aws dynamodb query --table-name app_versions --key-conditions '{"application": { "AttributeValueList": [{"S": "IS-ETLServices"}], "ComparisonOperator": "EQ" }}' --query-filter '{"qasignoff":{ "AttributeValueList": [{"S": "true"}], "ComparisonOperator": "EQ" }}' --region us-west-2 --no-scan-index-forward --max-items 1  |/usr/bin/jq .Items[0].versions.S)
	version=`echo "$version" | sed -e 's/^"//'  -e 's/"$//'`
	echo "version to be deployed is "$version
	
    echo "Verifying deployment group '$2_prod' existence"
    val=0
    aws deploy get-deployment-group --application-name IS-ETLServices --deployment-group-name $2_prod || val=1
    if [[ $val != 0 ]]; then
      echo "Deployment group $2_prod does not exists..Creating deployment group"
      aws deploy create-deployment-group --application-name IS-ETLServices --deployment-group-name $2_prod --auto-scaling-groups is-$2-asg  --deployment-config-name CodeDeployDefault.OneAtATime --region us-west-2 --service-role-arn arn:aws:iam::208876916689:role/codedeployservicerole
    else
    	echo "Deployment group $2_prod exists, continuing deployment..."
    fi
    
	echo "Starting deployment of IS-ETLServices on is-$2-asg..."
    sleep 30
	deploymentID=$(aws deploy create-deployment --application-name IS-ETLServices --deployment-group-name $2_prod --s3-location bucket=boomerangwars,bundleType=zip,key=IS-ETLServices/$version.zip --region us-west-2 --deployment-config-name CodeDeployDefault.AllAtOnce |/usr/bin/jq .deploymentId)
	deploymentID=`echo "$deploymentID" | sed -e 's/^"//'  -e 's/"$//'`
	echo "Deployment is in progress.." $deploymentID

	result=$(aws deploy get-deployment --deployment-id $deploymentID --region us-west-2 |/usr/bin/jq .deploymentInfo.status)
	result=`echo "$result" | sed -e 's/^"//'  -e 's/"$//'`
	echo $result
    
	if [[ "$result" = "Failed" ]]; then
	 	echo "Deployment failed.. retry will happen after 2 mins";
	 	sleep 120;
	 	deploymentID=$(aws deploy create-deployment --application-name IS-ETLServices --deployment-group-name $2_prod --s3-location bucket=boomerangwars,bundleType=zip,key=IS-ETLServices/$version.zip --region us-west-2 --deployment-config-name CodeDeployDefault.AllAtOnce |/usr/bin/jq .deploymentId)
	 	deploymentID=`echo "$deploymentID" | sed -e 's/^"//'  -e 's/"$//'`
	 	echo "Deployment is in progress.." $deploymentID
	 	result=$(aws deploy get-deployment --deployment-id $deploymentID --region us-west-2 |/usr/bin/jq .deploymentInfo.status)
	elif [[ "$result" = "Succeeded" ]]; then
	 	echo "Deployment is Successful"
    else
	    echo "Deployment is in progress"
	 	aws deploy get-deployment --deployment-id $deploymentID --region us-west-2
	 	sleep 120;
    fi
    #For printing deployment status, getting deployment details again. (in success case)
	aws deploy get-deployment --deployment-id $deploymentID --region us-west-2
	echo "Done ETLServices have been deployed!!"
}


clientName=$1
isdeploy=$2

#echo "Skipping creation temporarily"
echo "Creating asg......"
create_asg $clientName
sleep 60
validate_asg_creation is-$clientName-asg iid 

if [[ $? = 0 ]]; then
    if [[ "$isdeploy" = "Yes" ]]; then
		deploy_etlservices $iid $clientName
		error_exit $?
    else
    	echo "No deployment of etl-services happend!! "
    fi
else
	error_exit 1 
fi

echo "Done!!"
