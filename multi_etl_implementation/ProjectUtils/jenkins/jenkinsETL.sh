#!/bin/bash
set -e

CWD=$1
echo "CWD : " $CWD

#project_name=root-$2
project_name=$2
echo "project_name is : " $project_name
flow_name=$3
echo "flow_name is : " $flow_name
etl_date=$4
echo "etl_date is : " $etl_date
email_id=$5
echo "success email_id : " $email_id
success_emails=${email_id}
email_id=$6
echo "failure email_id : " $email_id
failure_emails=${email_id}
flow_parameters=$7

default_azkaban_host_url="ec2-54-90-127-6.compute-1.amazonaws.com"
default_azkaban_username="azkaban"
default_azkaban_passwd="azkaban"

azkaban_host_url="$default_azkaban_host_url"
azkaban_username="$default_azkaban_username"
azkaban_passwd="$default_azkaban_passwd"
azkaban_host=http://$azkaban_host_url

#azkaban_username=azkaban
#azkaban_passwd=azkaban

#etl_run_date=`/bin/date --date="$etl_date -0 day" +%Y%m%d`
etl_run_date=$etl_date
echo "ETL running for feed_date: " $etl_date

if [ "$project_name" == "updateClientList" ];then
   echo "You selected: " $project_name
   echo "Client list got updated."
   exit 1
elif [ "$flow_name" == "updateFlowList" ]
then
   echo "You selected: " $flow_name
   echo "Flow list got updated."
   exit 1
elif [ "$project_name" == "" ]
then
    echo "======> ERROR : client_name  parameter is empty. Please provide a client_name."
    exit 1
elif [ "$flow_name" == "" ]
then
    echo "======> ERROR : flow_name  parameter is empty. Please provide a flow_name."
    exit 1
fi

cd $CWD
echo "CWD was : " $CWD
cd $2
echo "Project directory is : " $project_name

echo "Present working directory is ::: " `pwd`

bash ../IS-Platform/ETLUtilsProjects/installETLUtilsProjects.sh

if [ "$flow_name" == "run-pre-etl" ]
then
   flow_name="pre-etl-end"
   sudo npm install
   grunt deployETL --feedDate=$etl_date --pullattmp="true" --target=prod
elif [ "$flow_name" == "run-etls" ]
then
   flow_name="run-etl-flows"
   sudo npm install
   npm install
   grunt deployETL --feedDate=$etl_date --pullattmp="true" --target=prod
elif [ "$flow_name" == "run-po" ]
then
   flow_name="po-end"
elif [ "$flow_name" == "run-dd" ]
then
   flow_name="dd-end"
elif [ "$flow_name" == "run-dd-po" ]
then
   flow_name="run_dd_po_flows"
elif [ "$flow_name" == "run-e2e" ]
then
   flow_name="end-to-end-flows_$project_name"
   echo "present directory :::::: " `pwd`
   #npm install grunt-cli -g
   sudo npm install grunt-cli -g
   #npm install grunt -g
   sudo npm install grunt -g
   bash ../IS-Platform/ETLUtilsProjects/installETLUtilsProjects.sh
   sudo npm install
   #npm install
   grunt deployETL --feedDate=$etl_date --pullattmp="true" --target=prod
elif [ "$flow_name" == "run-output-processing" ]
then
   flow_name="output-processing-end"
elif [ "$flow_name" == "run-output-check" ]
then
   flow_name="output-check-flows_$project_name"
elif [ "$flow_name" == "run-pre-etl-warning" ]
then
   flow_name="pre-etl-warning-flows_$project_name"
else
   echo "present directory :::::: " `pwd`
   #npm install grunt-cli -g
   sudo npm install grunt-cli -g
   #npm install grunt -g
   sudo npm install grunt -g
   bash ../IS-Platform/ETLUtilsProjects/installETLUtilsProjects.sh
   sudo npm install
   #npm install
   grunt deployETL --feedDate=$etl_date --pullattmp="true" --target=prod
fi
#sudo apt-get --force-yes install node
#sudo apt-get --force-yes install nodejs-legacy
#sudo npm install
#sudo npm install -g grunt-cli
#sudo npm install --save-dev load-grunt-tasks
#grunt package --feedDate=$etl_date
#grunt deploy --feedDate=$etl_date --target=prod
#grunt build --feedDate=$etl_date --target=prod
#grunt deployETL --feedDate=$etl_date --target=prod

azkaban_name=$(cat client-spec.json | jq '.azkaban_name'|sed 's/\"//g')

        count=$(cat client-spec.json | jq ".endpoints | length")
        i=0

        while [ $i -lt $count ]
        do
                name=$(cat client-spec.json | jq ".endpoints[$i] | .name" |sed 's/\"//g')
                type=$(cat client-spec.json | jq ".endpoints[$i] | .type" |sed 's/\"//g')


                if [ "$name" == "$azkaban_name" -a "$type" == "azkaban" ]; then
                        azkaban_username=$(cat client-spec.json | jq ".endpoints[$i] | .properties.username" |sed 's/\"//g')
                        azkaban_passwd=$(cat client-spec.json | jq ".endpoints[$i] | .properties.password" |sed 's/\"//g')
                        azkaban_host_url=$(cat client-spec.json | jq ".endpoints[$i] | .properties.host" |sed 's/\"//g')
                        azkaban_host=http://$azkaban_host_url
                fi

                i=`expr $i + 1`
        done

login_response=$(curl -k -X POST --data "action=login&username=$azkaban_username&password=$azkaban_passwd" $azkaban_host:8081)
echo "Login response:: "$login_response

#if [[ "$project_name" == "kmart" || "$project_name" == "OfficeDepot" ]]
#then
#	login_response=$(curl -k -X POST --data "action=login&username=$azkaban_username&password=$azkaban_passwd" http://ec2-52-11-232-61.us-west-2.compute.amazonaws.com:8081)
#	echo "Login response:: "$login_response
#else
#	login_response=$(curl -k -X POST --data "action=login&username=$azkaban_username&password=$azkaban_passwd" http://ec2-54-90-127-6.compute-1.amazonaws.com:8081)
#	echo "Login response:: "$login_response
#fi

status=$(echo $login_response | jq '."status"')
status=$(echo ${status:1:$(echo ${#status}) - 2})
echo $status

if [ "$status" == "success" ]
then
	echo "Azkaban login successfull."
else
	echo "======> ERROR in Azkaban login. Check Azkaban user-name/password or azkaban_host."
	exit 1
fi

session=$(echo $login_response | jq '."session.id"')
session=$(echo ${session:1:$(echo ${#session}) - 2})
echo "session : $session"

flow=$flow_name

flow_params_curl_command=""
#create curl param command from flow params

flow_params=$(echo $flow_parameters | tr "," "\n")
for flow_param in $flow_params
do
    original_IFS="$IFS" 
    IFS='=' 
	key_value=($flow_param)
	IFS="$original_IFS"
	
    if [ ${#key_value[@]} -ne 2 ]
	then
		echo "ERROR : wrong format of flow_paramters passed. [$key_value]. It should be a key value pair separated with '='."
		exit 1
	fi
	flow_params_curl_command=$flow_params_curl_command" --data flowOverride[${key_value[0]}]=${key_value[1]}"
done


#echo "flow parameters:  $flow_params_curl_command"
echo "Project flow = $flow"
echo "Executing project : $project_name"
execid=$(curl -k --get --data "session.id=$session" --data 'ajax=executeFlow' --data "project=$project_name" --data "flow=$flow" --data "successEmails=$success_emails" --data "failureEmails=$failure_emails" --data "notifyFailureFirst=true" --data "failureAction=finishPossible" --data "concurrentOption=ignore" --data "successEmailsOverride=true" --data "failureEmailsOverride=true" $azkaban_host:8081/executor $flow_params_curl_command | jq ".execid")

#if [[ "$project_name" == "kmart" || "$project_name" == "OfficeDepot" ]]
#then
#echo "in VPC execution"
#execid=$(curl -k --get --data "session.id=$session" --data 'ajax=executeFlow' --data "project=$project_name" --data "flow=$flow" --data "successEmails=$success_emails" --data "failureEmails=$failure_emails" --data "notifyFailureFirst=true" --data "failureAction=finishPossible" --data "concurrentOption=ignore" --data "successEmailsOverride=true" --data "failureEmailsOverride=true" http://ec2-52-11-232-61.us-west-2.compute.amazonaws.com:8081/executor | jq ".execid")
#else
#echo "in without VPC execution"
#execid=$(curl -k --get --data "session.id=$session" --data 'ajax=executeFlow' --data "project=$project_name" --data "flow=$flow" --data "successEmails=$success_emails" --data "failureEmails=$failure_emails" --data "notifyFailureFirst=true" --data "failureAction=finishPossible" --data "concurrentOption=ignore" --data "successEmailsOverride=true" --data "failureEmailsOverride=true" http://ec2-54-90-127-6.compute-1.amazonaws.com:8081/executor | jq ".execid")
#fi

if [ "$execid" == "" ]
then
    echo "======> ERROR while starting project [$project_name] on azkaban."
    exit 1
else
	echo "\nWorkflow [$flow] of project [$project_name] started successfully."
	echo "azkaban execution id : $execid"
fi

ret=$(echo $?)
if [ $ret -ne 0 ]
then
	echo "======> ERROR : ETL failed"
	exit 1
else
	echo "ETL started successfull"
fi
