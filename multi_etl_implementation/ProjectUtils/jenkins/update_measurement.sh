#!/bin/bash
set -e

echo "------------------------- started Performance Measurement copy ------------------------"

client="$1"
pmRepoName="athena_dataprocessing"
isRepoName="athena_is-implementation"
slash="/"
home_dir="$HOME"
tmp_dir="$home_dir/tmp"
echo "tmp_dir $tmp_dir"
current_dir=$tmp_dir$slash$isRepoName$slash$client
custompmname="$2"
pm="/output-processing/"

echo "start "

if [ ! -d $tmp_dir ]; then
mkdir -p $tmp_dir
fi

cd $tmp_dir
if [ ! -d "$pmRepoName" ]; then
git clone https://jenkins:jenkins@athena.unfuddle.com/git/$pmRepoName/
echo "repo cloned -  $pmRepoName"
else
cd $pmRepoName
#git reset --hard HEAD
git clean -f
git remote set-url origin https://jenkins:jenkins@athena.unfuddle.com/git/$pmRepoName/
git clean -f
git checkout master;
git pull origin master
fi

cd $tmp_dir
if [ ! -d "$isRepoName" ]; then
git clone https://jenkins:jenkins@athena.unfuddle.com/git/$isRepoName/
echo "repo cloned -  $isRepoName"
else
cd $isRepoName
#git reset --hard HEAD
git clean -f
git remote set-url origin https://jenkins:jenkins@athena.unfuddle.com/git/$isRepoName/
git clean -f
git checkout master;
git pull


echo "repo checked out -  $isRepoName"
fi

echo "current_dir:   $current_dir"
if [ -f "$current_dir/output-processing/$custompmname" ];then
rm -rf "$current_dir/output-processing/$custompmname"
echo "current_dir:   $current_dir     inside"
fi

echo ""
if [ ! -f "$tmp_dir/$pmRepoName/measurement/$custompmname" ]; then
echo "file not found, filename: $tmp_dir/$pmRepoName/measurement/$custompmname"
fi

if [ -f "$tmp_dir/$pmRepoName/measurement/$custompmname" ]; then
echo "copying file from $tmp_dir/$pmRepoName/measurement/$custompmname to $current_dir/output-processing"
cp "$tmp_dir/$pmRepoName/measurement/$custompmname" "$current_dir/output-processing"
fi

home="/home/ubuntu/is-platform/"
if [ -f "$current_dir/output-processing/$custompmname" ]; then
echo "------------------------- Performance Measurement copy completed --------------------"
current_pwd=`pwd`
echo "current dir : $current_pwd"
cd $current_dir/output-processing/
#sudo git diff --cached --exit-code
sudo git add $custompmname
commit_flag=0
for i in $(sudo git diff --cached --name-only)
    do
    if [ $i = "$client/output-processing/$custompmname" ]; then
        commit_flag=1
    fi
    done
echo "commit_flag $commit_flag"

if [ $commit_flag -eq 1 ]; then
sudo git commit -m "updated by running measurement data dictionary script, commited by jenkins job for client: $client"
sudo git push https://jenkins:jenkins@athena.unfuddle.com/git/athena_is-implementation --all
cd $current_pwd
else
echo "Nothing to commit... All things upto date on your repository"
fi
fi

#Moving to EC2 Machine
remoteEC2=""


if [ -f $current_dir/client-spec.json ]; then
remoteEC2=$(cat test-env-client-spec.json | jq '.remoteMachine'|sed 's/\"//g')
remoteMachineEndpoint=$(cat test-env-client-spec.json | jq '.remoteMachineEndpoint'|sed 's/\"//g')
remoteIP=$(java -cp ./../ProjectUtils/scripts/GetIPAddress-0.0.1-SNAPSHOT.jar GetIPAddress.GetIPAddress.GetRemoteIPAddress $remoteEC2 $remoteMachineEndpoint)
echo "remoteEC2  : $remoteEC2"
echo "remoteMachineEndpoint : $remoteMachineEndpoint"
echo "remoteIP   : $remoteIP"
else
    echo "$current_dir/client-spec.json is not exist"
fi

if [ -n $remoteIP ]; then
echo "copying from $current_dir$pm$custompmname to $remoteIP:$home$client$pm"
scp -i ~/.ssh/id_rsa $current_dir$pm$custompmname ubuntu@$remoteIP:$home$client$pm
fi


