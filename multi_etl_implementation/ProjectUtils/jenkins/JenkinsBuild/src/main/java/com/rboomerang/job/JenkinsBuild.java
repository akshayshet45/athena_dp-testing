package com.rboomerang.job;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.model.Build;
import com.offbytwo.jenkins.model.BuildWithDetails;
import com.offbytwo.jenkins.model.JobWithDetails;
import com.offbytwo.jenkins.model.QueueItem;
import com.offbytwo.jenkins.model.QueueReference;

public class JenkinsBuild {
	private static final String JenkinsUrl = "http://bad.rboomerang.com/";

	public static void main(String[] args) throws Exception {
		String JobName = args[0];
		String UserName = args[1];
		String JenkinsAPIToken = args[2];
		
		JenkinsServer jenkins;
		try {
			jenkins = new JenkinsServer(new URI(JenkinsUrl), UserName, JenkinsAPIToken);
		} catch (Exception e) {
			throw new RuntimeException(String.format(
					"Please check %s, if jenkins is authenticated, please pass %s and %s in flow parameters",
					JenkinsUrl, UserName, JenkinsAPIToken));
		}

		JobWithDetails job = jenkins.getJob(JobName);

		QueueReference queueReference = null;
		Map<String, String> jobParams = new HashMap<String, String>();
		jobParams.put("Applications", args[3]);
		jobParams.put("Type", args[4]);
		jobParams.put("CustomisedName", args[5]);
		jobParams.put("description", args[6]);
		jobParams.put("branch_name", args[7]);
		jobParams.put("DeployToAllClients", args[8]);
		jobParams.put("clients", args[9]);

		System.out.println(String.format("Queuing build for job: %s", job.getName()));
		System.out.println(String.format("with parameters: %s", jobParams));
		queueReference = job.build(jobParams, false);
		QueueItem queueItem = jenkins.getQueueItem(queueReference);

		while (queueItem.getExecutable() == null) {
			System.out.println("Waiting for build to finish");
			Thread.sleep(3000);
			queueItem = jenkins.getQueueItem(queueReference);
		}

		Build build = jenkins.getBuild(queueItem);

		System.out.println(String.format("Build No. in jenkins : %s", build.getNumber()));

		job = jenkins.getJob(JobName);
		BuildWithDetails buildDetails = job.getBuildByNumber(build.getNumber()).details();

		while (buildDetails.isBuilding()) {
			System.out.println(String.format("Waiting for build %s to finish", build.getNumber()));
			Thread.sleep(3000);
			job = jenkins.getJob(JobName);
			buildDetails = job.getBuildByNumber(build.getNumber()).details();
		}

		System.out.println(buildDetails.getConsoleOutputText());

		switch (buildDetails.getResult()) {
		case ABORTED:
		case FAILURE:
		case NOT_BUILT:
		case UNKNOWN:
		case UNSTABLE:
			throw new RuntimeException(
					String.format("Result for build: %s is %s", build.getNumber(), buildDetails.getResult()));
		default:
			System.out
					.println(String.format("Result for build: %s is %s", build.getNumber(), buildDetails.getResult()));
		}

	}
}
