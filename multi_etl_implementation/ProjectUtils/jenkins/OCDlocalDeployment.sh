#!/bin/bash
set -e

#beta deployment code
APPLICATION_NAME=""
JOB_NAME=beta
CWD=`pwd`

echo "deploying ${PWD##*/} project on $JOB_NAME job , link : http://bad.rboomerang.com/job/$JOB_NAME/"

file="create_application.json"
if [ -f "$file" ]
then
	APPLICATION_NAME=$(cat create_application.json | jq '.Name'|sed 's/\"//g')
else
	echo "$file file missing."
	exit 1
fi

if [ $APPLICATION_NAME == "" ] || [ $APPLICATION_NAME == "null" ] 
then
	echo "Name field not proper in create_application.json"
	exit 1
else
	echo "Application Name to be deployed $APPLICATION_NAME"
fi

TYPE="minor"
if [[ -z "$1" ]];then
   TYPE="minor"
else
	TYPE=$1
fi


if [[ -z "$2" ]];then
   CustomisedName=$APPLICATION_NAME
else
	CustomisedName=$2
fi


if [ "$3" != "" ];then
   description=$3
else
	echo "--description argument missing."
	exit 1
fi

branch_name="master"
if [[ -z "$4" ]];then
   branch_name="master"
else
	branch_name=$4
fi

DeployToAllClients="No"
clients="common,internal"

echo "------------------------------------------------------"
echo "options : APPLICATION_NAME : $APPLICATION_NAME"
echo "options : Type : $TYPE"
echo "options : CustomisedName : $CustomisedName"
echo "options : description : $description"
echo "options : branch_name : $branch_name"
echo "options : DeployToAllClients : $DeployToAllClients"
echo "options : clients : $clients"
echo "------------------------------------------------------"

#PARAMETERS="{\"parameter\": [{\"name\": \"Applications\",\"value\": "$APPLICATION_NAME"}, { \"name\": \"Type\",   \"value\": "$TYPE" }, {   \"name\": \"CustomisedName\",   \"value\": "$CustomisedName" }, {   \"name\": \"description\",   \"value\": "$description" }, {   \"name\": \"branch_name\",   \"value\": "$branch_name" }, {   \"name\": \"DeployToAllClients\",   \"value\": "$DeployToAllClients" }, {   \"name\": \"clients\",   \"value\": "$clients" } ] }"

#curl -X POST http://bad.rboomerang.com/job/$JOB_NAME/build  --user shivam@boomerangcommerce.com:984e57fcdddb8509765228890ffee464 --form json=$PARAMETERS
JSON='{"parameter":[{"name":"Applications","value":'\""$APPLICATION_NAME"\"'},{"name":"Type","value":'\""$TYPE"\"'},{"name":"CustomisedName","value":'\""$CustomisedName"\"'},{"name":"description","value":'\""$description"\"'},{"name":"branch_name","value":'\""$branch_name"\"'},{"name":"DeployToAllClients","value":'\""$DeployToAllClients"\"'},{"name":"clients","value":'\""$clients"\"'}]}'
#echo $JSON

#getting user
string="$(cat ~/.ssh/id_rsa.pub)"
IFS=' ' read -r -a array <<< "$string"
USERNAME=${array[2]}
echo "User : $USERNAME"
API_Key=$(aws dynamodb get-item --table-name 'IS-Deployment-Access' --key '{"Email":{"S":"'$USERNAME'"}}' --region us-west-2 | jq  '.Item.APIToken.S'|sed 's/\"//g')
echo  "Api key for jenkins : $API_Key"
if [[ -z "$API_Key" ]];then
   	echo "please get yourself registered for deployment, Contact IS-Team!!"
	exit 1
fi

java -cp ../ProjectUtils/jenkins/JenkinsBuild-0.0.1-jar-with-dependencies.jar  com.rboomerang.job.JenkinsBuild "beta" "$USERNAME" "$API_Key" "$APPLICATION_NAME" "minor" "$CustomisedName" "$description" "$branch_name" "$DeployToAllClients" "$clients"


#RESPONSE=$(curl -i -X  POST http://bad.rboomerang.com/job/$JOB_NAME/build?delay=0sec  --user $USERNAME:$API_Key --form json="$JSON")
#echo "Jenkins Response : $RESPONSE"
#RESPONSE=$(curl -X POST http://bad.rboomerang.com/job/$JOB_NAME/lastBuild/consoleText  --user $USERNAME:$API_Key)
#echo "Jenkins Response : $RESPONSE"
#status=$(echo $login_response | jq '."status"')
#echo "status : $status"
