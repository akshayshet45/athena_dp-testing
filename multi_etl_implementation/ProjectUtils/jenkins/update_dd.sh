#!/bin/bash
set -e

echo "------------------------- started Update Data Dictionary ------------------------"

client="$1"
#client="groupon-is"
echo $client
ddName="$2"
#ddName="groupon_dd.sql"
echo $ddName
current_dir=`pwd`
echo $current_dir
ddRepoName="athena_dataprocessing"
git checkout master
cd ../../../..
cd tmp

if [ ! -d "$ddRepoName" ]; then
        sudo git clone https://jenkins:jenkins@athena.unfuddle.com/git/$ddRepoName
        echo "repo cloned -  $ddRepoName"
else
        cd $ddRepoName
        #git reset --hard HEAD
        sudo git clean -f
#        sudo git remote set-url origin https://jenkins:jenkins@athena.unfuddle.com/git/$ddRepoName/
 #       sudo git clean -f
        sudo git checkout master;
        sudo git pull

        echo "repo checked out -  $ddRepoName"
fi
cd $ddRepoName

path="/var/lib/jenkins/workspace/"
workspace="update_dd_on_new_IS_Project_structure/"
dd="/dd/"
echo $path$workspace$client$dd
#rm -rf $path$workspace$client$dd
cp /var/lib/jenkins/workspace/tmp/$ddRepoName/dd/$ddName $path$workspace$client$dd

#sudo rm -rf /var/lib/jenkins/workspace/tmp/$ddRepoName
cd /var/lib/jenkins/workspace/tmp/
echo "removed repo from /var/lib/jenkins/workspace/tmp/"
#sudo rm -rf $ddRepoName
cd $path$workspace$client$dd

sudo git add $ddName
sudo git commit -m "updated data dictionary script, by running update_dd...  jenkins job"
#sudo git push origin master
sudo git push https://jenkins:jenkins@athena.uefuddle.com/git/athena_is-platform --all
echo "------------------------- Data Dictionary update completed --------------------"
