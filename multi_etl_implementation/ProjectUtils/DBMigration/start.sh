#!/bin/bash
# @author : Ravi
set -e
########################################################
echo "Current Working directory is "`pwd`
env=$3
db_type=$2
schema_dir="schema/"
echo $schema_dir
if [[ $db_type = "redshift" ]]; then
	echo "DB type is "$db_type
	db_file_path=$schema_dir$db_type/db.properties
	mig_dir=$schema_dir$db_type
	echo "Migration script directory is "$mig_dir
	echo "DB properties file location is "$schema_dir/db.properties
elif [[ $db_type = "mysql" ]]; then
	echo "DB type is "$db_type
	db_file_path=$schema_dir$db_type/db.properties
	mig_dir=$schema_dir$db_type
	echo "Migration script directory is "$mig_dir
	echo "DB properties file location is "$mig_dir/db.properties
else
	echo "Invalid DB type. Valid list= {redshift,mysql}"
	exit 1;
fi
############################################################
echo "Identified DB details are\n\n"
echo 
echo "***********************Applying Migration******************************"
echo "Given environment is "$env


java -jar ../ProjectUtils/DBMigration/DBMigration-0.0.1-SNAPSHOT-jar-with-dependencies.jar $mig_dir $schema_dir/db.properties $env $db_type

if [[ $? = "0" ]]; then
	echo "SUCCESS"
	exit 0
else
	echo "FAILURE, refer the log"
	exit 1
fi
#############################################################