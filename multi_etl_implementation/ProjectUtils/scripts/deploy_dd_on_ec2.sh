#!/bin/bash
set -e
client=$1
#client="groupon-is"
dd_name=$2
root_dir=`pwd`
echo $root_dir

cd $client

home="/home/ubuntu/is-platform/"
dd="/dd/"
remoteEC2InstanceID=$(cat client-spec.json | jq '.remoteMachine'|sed 's/\"//g')
echo $remoteEC2InstanceID
remoteEC2InstanceEndpoint=$(cat client-spec.json | jq '.remoteMachineEndpoint'|sed 's/\"//g')
echo $remoteEC2InstanceEndpoint
remoteEC2="$(java -cp ./../ProjectUtils/scripts/GetIPAddress-0.0.1-SNAPSHOT.jar GetIPAddress.GetIPAddress.GetRemoteIPAddress $remoteEC2InstanceID $remoteEC2InstanceEndpoint)"
echo $remoteEC2
slash="/"
scp -r $root_dir$slash$client$dd$dd_name ubuntu@$remoteEC2:$home$client$dd

#ssh ubuntu@$remoteEC2 <<- EOD
#cd $home$client$dd
#pwd
#exit
#EOD
