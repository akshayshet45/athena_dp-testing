#!/bin/bash

set -e

client=$1

rootdir=`pwd`

echo "-----------------Starting HeaderFilesCreation --------------------------"
tablePath='etl/etl-files/tables'
schema_Path='schema'
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar main.java.com.rboomerang.SqlScriptsValidator $rootdir $tablePath $client $schema_Path
echo "-----------------Ended HeaderFilesCreation -----------------------------"