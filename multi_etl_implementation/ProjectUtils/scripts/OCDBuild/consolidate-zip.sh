#!/bin/bash
set -e
home_remote_machine="/home/ubuntu"
project_dir=`pwd`
rootdir=$1
IS_zip_name=$2
unzipedfoldername1=''
unzipedfoldername2=''
consolidated_zip_name=$3
repo=$4
dev_environment=$5

#rootdir=`pwd`
#wexecutordir="$rootdir/athena_data-pipeline/validators/workflow-executor"
#IS_Project_name='IS-Project'
#IS_zip_name='IS-Project-0.0.1-SNAPSHOT-zip.zip'
#unzipedfoldername1=''
#unzipedfoldername2=''
#consolidated_zip_name='IS-Project-zip'
#repo='athena_is-team-code'
etl_spec_file="client-spec.json"

if [ "$dev_environment" == "test" ]; then
if [ ! -f "$project_dir/test-env-client-spec.json" ]; then
echo "File not found, Please configure $project_dir/test-env-client-spec.json"
exit 1
fi
etl_spec_file="test-env-client-spec.json"
echo "Used ETL Specification File : $etl_spec_file"
fi


if [ ! -f "target/$IS_zip_name" ]; then
echo "build might failed as $IS_zip_name is not generated on location: /target/$IS_zip_name"
exit 1
fi

if [ -d "target1/$consolidated_zip_name" ]; then
rm -rf target1/$consolidated_zip_name
fi

mkdir -p target1/$consolidated_zip_name


count=$(cat $project_dir/$etl_spec_file  | jq ".etl_list | length")
client_spec_exist=$count
i=0

if [ $count -eq 0 ]; then
etl_path="etl/etl-files"
etl_name="$(cat $etl_path/etl-spec.json | jq ".client"|sed 's/\"//g')"
job_prefix=""
count=1
fi

while [ $i -lt $count ]
do
if [ $client_spec_exist -ne 0 ]; then
etl_path=$(cat $project_dir/$etl_spec_file  | jq ".etl_list[$i].etl_path"|sed 's/\"//g')
#echo $etl_path
etl_name=$(cat $etl_path/etl-spec.json | jq ".client"|sed 's/\"//g')
#echo $etl_name
job_prefix=$(cat $etl_path/etl-spec.json | jq ".jobPrefix"|sed 's/\"//g')
fi

if [ ! -f "$project_dir/temp-zipfiles/$etl_path/dataplatform-0.0.1-zip.zip" ]; then
echo "$project_dir/temp-zipfiles/$etl_path/dataplatform-0.0.1-zip.zip not found!. Build may have failed for ETL NAME : $etl_name"
        exit 1
fi

if [ -f "target/$IS_zip_name" -a -f "$project_dir/temp-zipfiles/$etl_path/dataplatform-0.0.1-zip.zip" ]; then
job_prefix_etl_name=""
if [ -n $job_prefix ] && [ "$job_prefix" != "null" ]; then
job_prefix_etl_name=$job_prefix$etl_name
else
job_prefix_etl_name="$etl_name"
fi
echo "starting consolidation process for ETL NAME : $job_prefix_etl_name"
unzipedfoldername1=$( unzip -qql $project_dir/temp-zipfiles/$etl_path/dataplatform-0.0.1-zip.zip | head -n1 | tr -s ' ' | cut -d' ' -f5-| sed 's/\///g')
unzip -q -o $project_dir/temp-zipfiles/$etl_path/dataplatform-0.0.1-zip.zip "$unzipedfoldername1/*" -d target1/$consolidated_zip_name

mkdir -p target1/$consolidated_zip_name/$job_prefix_etl_name
mv target1/$consolidated_zip_name/$unzipedfoldername1/* target1/$consolidated_zip_name/$job_prefix_etl_name
fi
i=`expr $i + 1`
done

echo "starting consolidation ZIP for IS-Project"
unzipedfoldername2=$( unzip -qql target/$IS_zip_name | head -n1 | tr -s ' ' | cut -d' ' -f5-| sed 's/\///g')
unzip -q -o target/$IS_zip_name "$unzipedfoldername2/*" -d target1/$consolidated_zip_name
mv -f target1/$consolidated_zip_name/$unzipedfoldername2/* target1/$consolidated_zip_name

if [ "$dev_environment" == "test" ]; then
if [ -f "target1/$consolidated_zip_name/client-spec.json" ]; then
rm -f "target1/$consolidated_zip_name/client-spec.json"
fi

if [ -f "test-env-client-spec.json" ]; then
cp -f test-env-client-spec.json target1/$consolidated_zip_name/client-spec.json

remoteEC2=$(cat test-env-client-spec.json | jq '.remoteMachine'|sed 's/\"//g')
remoteMachineEndpoint=$(cat test-env-client-spec.json | jq '.remoteMachineEndpoint'|sed 's/\"//g')
remoteIP=$(java -cp ./../ProjectUtils/scripts/GetIPAddress-0.0.1-SNAPSHOT.jar GetIPAddress.GetIPAddress.GetRemoteIPAddress $remoteEC2 $remoteMachineEndpoint)
echo "remoteEC2  : $remoteEC2"
echo "remoteMachineEndpoint : $remoteMachineEndpoint"
echo "remoteIP   : $remoteIP"
if [ -n $remoteIP ]; then
client=$(cat $project_dir/client-spec.json  | jq ".client"|sed 's/\"//g')
file="client-spec.json"
ssh ubuntu@$remoteIP mkdir -p $home_remote_machine/is-platform/$client
ssh ubuntu@$remoteIP mkdir -p $home_remote_machine/is-platform/$client/$etl_path
scp -i ~/.ssh/id_rsa test-env-client-spec.json ubuntu@$remoteIP:$home_remote_machine/is-platform/$client/$file
file="etl-spec.json"
scp -i ~/.ssh/id_rsa $etl_path/test-env-etl-spec.json ubuntu@$remoteIP:$home_remote_machine/is-platform/$client/$etl_path/$file

#echo "msg:$msg"
else
echo "remoteURL is null"
fi
echo "--------> Enviroment = $dev_environment"
else 
echo "test-client-etl-spec.json is not exist, Please provide test-client-etl-spec.json"
exit 1
fi
fi

if [ -d "zip-target" ]; then
rm -rf zip-target
fi

mkdir -p zip-target
echo "Creating consolidated Zip... "

echo "Zip Name:" $consolidated_zip_name'.zip'
zip -r -q "zip-target/$consolidated_zip_name.zip" target1/$consolidated_zip_name

if [ -d "temp-zipfiles" ]; then
rm -rf temp-zipfiles
fi
if [ -d "target1" ]; then
rm -rf target1
fi
if [ -d "target" ]; then
rm -rf target
fi
if [ -d tmp-app ]; then
rm -rf tmp-app
fi
