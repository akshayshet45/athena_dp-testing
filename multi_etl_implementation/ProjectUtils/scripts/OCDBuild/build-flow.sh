#!/bin/bash
set -e

IS_zip_name='IS-Project-0.0.1-SNAPSHOT-zip.zip'
consolidated_zip_name='IS-Project-zip'
repo='athena_is-implementation'
client=$1
skip_validation_queries="true"
semantic_validation_flag="true"


rootdir=`pwd`
rootdir=$rootdir


environment=$(cat client-spec.json  | jq ".environment"|sed 's/\"//g')
if [ "$environment"=="test" -a "$environment" != "" -a "$environment" != "null" ]; then
dev_environment="test"
fi

echo "etl_run_date :  $etl_run_date"
chmod 777 ./../ProjectUtils/scripts/Move-Files.sh
chmod 777 ./../ProjectUtils/scripts/ETL-build-zip.sh
chmod 777 ./../ProjectUtils/scripts/build-azkaban-flow.sh
chmod 777 ./../ProjectUtils/scripts/consolidate-zip.sh

echo "-----------------Starting Preparing Project--------------------------"
bash ./../ProjectUtils/scripts/OCDBuild/Move-Files.sh $client $dev_environment
echo "------------------Project is Ready-----------------------------------"

count=$(cat client-spec.json  | jq ".etl_list | length")
client_spec_exist=$count
i=0

if [ $count -eq 0 ]; then
etl_name="$client"
etl_path="etl/etl-files"
job_prefix_etl_name="$etl_name"
echo "ETL PATH: $etl_path"
echo "ETL NAME: $etl_name"
count=1
fi

while [ $i -lt $count ]
do
skip_validation_queries="true"
semantic_validation_flag="true"
if [ $client_spec_exist -ne 0 ]; then
etl_path=$(cat client-spec.json  | jq ".etl_list[$i].etl_path"|sed 's/\"//g')
echo "ETL PATH: $etl_path"
skip_validation_queries_check=$(cat client-spec.json  | jq ".etl_list[$i].skip_validation"|sed 's/\"//g')
if [ -n  $skip_validation_queries_check -a "$skip_validation_queries_check" != "null" ];then
skip_validation_queries=$skip_validation_queries_check
fi
echo " Skip Validation := $skip_validation_queries"
sementic_validation_check=$(cat client-spec.json  | jq ".etl_list[$i].semantic_validation"|sed 's/\"//g')
if [ -n $sementic_validation_check -a "$sementic_validation_check" != "null" ];then
semantic_validation_flag=$sementic_validation_check
fi

echo " Semantic Validation := $semantic_validation_flag"
etl_name=$(cat $etl_path/etl-spec.json | jq ".client"|sed 's/\"//g')
echo "ETL NAME: $etl_name"
job_prefix=$(cat $etl_path/etl-spec.json | jq ".jobPrefix"|sed 's/\"//g')
job_prefix_etl_name=""
if [ -n $job_prefix ] && [ "$job_prefix" != "null" ]; then
job_prefix_etl_name=$job_prefix$etl_name
else
job_prefix_etl_name=$etl_name
fi
fi
echo "-----------------Starting build ETL process for $job_prefix_etl_name-----------------------------------------"
echo "------> job_prefix_etl_name:  $job_prefix_etl_name"
bash ./../ProjectUtils/scripts/OCDBuild/ETL-build-zip.sh $job_prefix_etl_name $skip_validation_queries $etl_path $semantic_validation_flag $rootdir
echo "-----------------Ended build ETL process for $job_prefix_etl_name --------------------------------------------"
i=`expr $i + 1`
done

echo "-----------------Starting build IS-project process-----------------------------------------"
bash ./../ProjectUtils/scripts/OCDBuild/build-azkaban-flow.sh $rootdir $IS_zip_name $repo $client
echo "-----------------Ended build IS-project process--------------------------------------------"


echo "-----------------Starting consolidation process--------------------------------------------"
bash ./../ProjectUtils/scripts/OCDBuild/consolidate-zip.sh $rootdir $IS_zip_name $consolidated_zip_name $repo $environment
echo "-----------------Ended consolidation process-----------------------------------------------"
