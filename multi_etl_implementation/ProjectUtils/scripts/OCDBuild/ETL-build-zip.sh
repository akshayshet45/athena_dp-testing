#!/bin/bash
set -e
client=$1
skip_validation_queries=$2
semantic_validation_flag=$4
etl_path=$3
etl_app_path="tmp-app/$client/etl-app-template"
root_dir=$5


cd $etl_app_path
mvn clean package -Dboomerang.skipvalidation=$skip_validation_queries -Dboomerang.semanticvalidation=$semantic_validation_flag -U -q
cd $root_dir
if [ ! -f $etl_app_path/target/dataplatform-0.0.1-zip.zip ]; then
    echo "$etl_app_path/target/dataplatform-0.0.1-zip.zip not found!. Build may have failed"
    exit 1
else
if [ -d "$root_dir/temp-zipfiles/$etl_path" ]; then
rm -rf $root_dir/temp-zipfiles/$etl_path
fi
mkdir -p $root_dir/temp-zipfiles/$etl_path
cp -rf $etl_app_path/target/dataplatform-0.0.1-zip.zip $root_dir/temp-zipfiles/$etl_path
fi

