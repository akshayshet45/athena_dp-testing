#!/bin/bash
set -e
client=$1

#client='Gilt'
rootdir=`pwd`


echo "-----------------Starting ETL Spec validation  --------------------------"
tablePath='etl/etl-files/etl-spec.json'
java -cp ../ProjectUtils/target/ProjectUtils-0.0.4-SNAPSHOT-jar-with-dependencies.jar com.rboomerang.validator.impl.EtlSpecValidator $rootdir $tablePath $client
echo "-----------------Ended  ETL Spec  validation -----------------------------"

