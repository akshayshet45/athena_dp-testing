#!/bin/bash
set -e
client=$1

#client='Gilt'
rootdir=`pwd`


echo "-----------------Starting sql scripts validation--------------------------"
tablePath='etl/etl-files/tables'
schema_Path='schema'
java -cp ../ProjectUtils/target/ProjectUtils-0.0.4-SNAPSHOT-jar-with-dependencies.jar com.rboomerang.validator.impl.SqlScriptsValidator $rootdir $tablePath $client $schema_Path
echo "-----------------Ended sql scripts validation -----------------------------"

