#!/bin/bash
set -e
client=$1

#client='Gilt'
rootdir=`pwd`



#jar cfm ../ProjectUtils/target/ProjectUtilss-0.0.1-SNAPSHOT-jar-with-dependencies.jar myManifest -C ../ProjectUtils/target/classes
#jar cvf  ../ProjectUtils/target/ProjectUtilss-0.0.1-SNAPSHOT-jar-with-dependencies.jar myManifest -C ../ProjectUtils/target/classes/com/rboomerang/jobValidator/jobValidator.class
echo "-----------------Starting .job files validation--------------------------"
echo "-------------------------- dd/flows/ validation -------------------------"
filePath='dd/flows'
java -cp ../ProjectUtils/target/ProjectUtils-0.0.4-SNAPSHOT-jar-with-dependencies.jar com.rboomerang.validator.impl.JobValidator $rootdir $filePath $client
echo "-------------------------- e2e/flows/ validation -------------------------"
filePath='e2e/flows'
java -cp ../ProjectUtils/target/ProjectUtils-0.0.4-SNAPSHOT-jar-with-dependencies.jar com.rboomerang.validator.impl.JobValidator  $rootdir $filePath $client
echo "-------------------------- etl/flows/ validation -------------------------"
filePath='etl/flows'
java -cp ../ProjectUtils/target/ProjectUtils-0.0.4-SNAPSHOT-jar-with-dependencies.jar com.rboomerang.validator.impl.JobValidator  $rootdir $filePath $client
echo "-------------------------- load-etl/flows/ validation -------------------------"
filePath='load-etl/flows'
java -cp ../ProjectUtils/target/ProjectUtils-0.0.4-SNAPSHOT-jar-with-dependencies.jar  com.rboomerang.validator.impl.JobValidator $rootdir $filePath $client
echo "-------------------------- output-processing/flows/ validation -------------------------"
filePath='output-processing/flows'
java -cp ../ProjectUtils/target/ProjectUtils-0.0.4-SNAPSHOT-jar-with-dependencies.jar  com.rboomerang.validator.impl.JobValidator $rootdir $filePath $client
echo "-------------------------- po/flows/ validation -------------------------"
filePath='po/flows'
java -cp ../ProjectUtils/target/ProjectUtils-0.0.4-SNAPSHOT-jar-with-dependencies.jar  com.rboomerang.validator.impl.JobValidator $rootdir $filePath $client
echo "-------------------------- post-etl/flows/ validation -------------------------"
filePath='post-etl/flows'
java -cp ../ProjectUtils/target/ProjectUtils-0.0.4-SNAPSHOT-jar-with-dependencies.jar  com.rboomerang.validator.impl.JobValidator $rootdir $filePath $client
echo "-------------------------- pre-etl/flows/ validation -------------------------"
filePath='pre-etl/flows'
java -cp ../ProjectUtils/target/ProjectUtils-0.0.4-SNAPSHOT-jar-with-dependencies.jar  com.rboomerang.validator.impl.JobValidator $rootdir $filePath $client
echo "----------------- .job files validation complete -----------------------------"

