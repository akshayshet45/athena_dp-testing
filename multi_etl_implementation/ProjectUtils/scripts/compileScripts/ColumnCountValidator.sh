#!/bin/bash
set -e
client=$1

#client='Gilt'
rootdir=`pwd`


echo "-----------------Starting Feed File validation for column Count --------------------------"
feedPath='etl/etl-files/feeds'
headerPath='feeds-metadata/header-files'
java -cp ../ProjectUtils/target/ProjectUtils-0.0.4-SNAPSHOT-jar-with-dependencies.jar com.rboomerang.validator.impl.ColumnCountValidator $rootdir $feedPath $client $headerPath
echo "-----------------Ended Feed File validation for column count -----------------------------"

