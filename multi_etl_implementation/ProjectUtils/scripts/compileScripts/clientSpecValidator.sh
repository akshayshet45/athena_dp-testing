#!/bin/bash
set -e

#client='Gilt'
rootdir=`pwd`


echo "-----------------Starting ETL Spec validation  --------------------------"
java -cp ../ProjectUtils/target/ProjectUtils-0.0.4-SNAPSHOT-jar-with-dependencies.jar com.rboomerang.validator.impl.ClientSpecValidator $rootdir
echo "-----------------Ended  ETL Spec  validation -----------------------------"

