import sys
import os.path
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import Encoders

# Import the email modules we'll need
from email.mime.text import MIMEText

msg = MIMEMultipart()
msg['Subject'] = sys.argv[1]
msg['From'] = sys.argv[3]
msg['To'] = sys.argv[4]

part = MIMEBase('application', "octet-stream")
part.set_payload(open(sys.argv[2], "rb").read())
Encoders.encode_base64(part)

part.add_header('Content-Disposition', 'attachment; filename="BuildLog.txt"')

msg.attach(part)

# Send the email via gmail SMTP server.
session = smtplib.SMTP("smtp.gmail.com",587)
#session = smtplib.SMTP("email-smtp.us-east-1.amazonaws.com",587)
session.ehlo()
session.starttls()
session.ehlo
session.login('ops@boomerangcommerce.com', 'Naganat123')
session.sendmail(sys.argv[3], sys.argv[4], msg.as_string())
session.quit()
