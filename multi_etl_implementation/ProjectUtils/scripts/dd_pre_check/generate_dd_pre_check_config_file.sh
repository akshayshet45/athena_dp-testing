#!/bin/bash
set -e

if [ -f client-spec.json ]; then
client=$(cat client-spec.json | jq '.client'|sed 's/\"//g')
fi

sourcejsonpath="../IS-Platform/ProjectUtils/scripts/dd_pre_check/kishan.json"
sourcejobpath="../IS-Platform/ProjectUtils/scripts/dd_pre_check/kishan.job"
destjsonpath="dd/flows/properties/${client}_dd_pre_check.json"
destjobpath="dd/flows/${client}_dd_pre_check.job"

cp -f $sourcejobpath $destjobpath
cp -f $sourcejsonpath $destjsonpath

sed -i.bak "s/<client>/$client/g" $destjobpath

if [ -f ${destjobpath}.bak ]; then
rm ${destjobpath}.bak
fi

sed -i.bak "s/<client>/$client/g" $destjobpath

if [ -f ${destjobpath}.bak ]; then
rm ${destjobpath}.bak
fi

sed -i.bak "s/<client>/$client/g" $destjsonpath


if [ -f ${destjsonpath}.bak ]; then
rm ${destjsonpath}.bak
fi

