#!/bin/bash
set -e
  
project_dir=`pwd`
client="$(cat client-spec.json | jq ".client"|sed 's/\"//g')"
  
echo "Project directory : $project_dir"
echo "Client Name :  $client"
  
remoteMachineInstanceID="$(cat client-spec.json | jq ".remoteMachine"|sed 's/\"//g')"
remoteMachineInstanceEndpoint="$(cat client-spec.json | jq ".remoteMachineEndpoint"|sed 's/\"//g')"
  
echo "instanceID : $remoteMachineInstanceID"
echo "instanceEndpoint : $remoteMachineInstanceEndpoint"
  
remoteMachineIP="$(java -cp ./../ProjectUtils/scripts/GetIPAddress-0.0.1-SNAPSHOT.jar GetIPAddress.GetIPAddress.GetRemoteIPAddress $remoteMachineInstanceID $remoteMachineInstanceEndpoint)"
echo "IP Address : $remoteMachineIP"
  
chmod 600 ../ProjectUtils/jenkins_private.pem
  
echo "Removing $client project from remote"
ssh  -i ../ProjectUtils/jenkins_private.pem ubuntu@"$remoteMachineIP" 'sudo rm -rf /home/ubuntu/is-platform/'"$client"
echo "creating $client project on remote machine"
ssh  -i ../ProjectUtils/jenkins_private.pem ubuntu@"$remoteMachineIP" 'mkdir /home/ubuntu/is-platform/'"$client"
echo "Syncing new $client project to remote"
rsync -e 'ssh -v -i ../ProjectUtils/jenkins_private.pem' . ubuntu@"$remoteMachineIP":/home/ubuntu/is-platform/"$client"/ --rsh ssh --recursive --exclude=tmp --exclude=zip-target --exclude=libs --exclude=node_modules --exclude=tmp-app --verbose
