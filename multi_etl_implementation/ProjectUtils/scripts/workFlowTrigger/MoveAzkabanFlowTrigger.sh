#!/bin/bash
set -e

if [ ! -f ../IS-Platform/ProjectUtils/scripts/workFlowTrigger/AzkabanFlowTrigger.sh ] ; then
echo "Please pull latest code from IS-Platform repo. ../IS-Platform/ProjectUtils/scripts/workFlowTrigger/AzkabanFlowTrigger.sh file is not present"
exit 1
fi


if [ ! -d scripts ]; then
mkdir -p scripts
fi
cp -f ../IS-Platform/ProjectUtils/scripts/workFlowTrigger/AzkabanFlowTrigger.sh scripts

if [ -f client-spec.json ]; then
client=$(cat client-spec.json | jq '.client'|sed 's/\"//g')
remoteEC2=$(cat client-spec.json | jq '.remoteMachine'|sed 's/\"//g')
remoteMachineEndpoint=$(cat client-spec.json | jq '.remoteMachineEndpoint'|sed 's/\"//g')
remoteIP=$(java -cp ./../IS-Platform/ProjectUtils/scripts/GetIPAddress-0.0.1-SNAPSHOT.jar GetIPAddress.GetIPAddress.GetRemoteIPAddress $remoteEC2 $remoteMachineEndpoint)
echo "remoteEC2  : $remoteEC2"
echo "remoteMachineEndpoint : $remoteMachineEndpoint"
echo "remoteIP   : $remoteIP"
else
    echo "$current_dir/client-spec.json is not exist"
fi
script="/scripts"
home="/home/ubuntu/is-platform/"
echo "home : $home"
if [ -n $remoteIP ]; then
echo "copying to $remoteIP:$home$client$script"
scp -i ~/.ssh/id_rsa scripts/AzkabanFlowTrigger.sh  ubuntu@$remoteIP:$home$client$script
fi


