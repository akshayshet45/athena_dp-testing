#!/bin/bash
set -e
isCommit=$1
if [ "$isCommit" != "true" ]; then
isCommit=false
echo "generating files to review..."
fi
rootdir=`pwd`

echo "-----------------Starting Canvas Config Automation  --------------------------"
prodSpecConfigPath=$rootdir'/configs/product-spec.json'
echo "-----$prodSpecConfigPath---------"
java -cp ../ProjectUtils/target/ProjectUtils-0.0.4-SNAPSHOT-jar-with-dependencies.jar com.rboomerang.automation.ansibleConfig.CanvasConfiguration "$prodSpecConfigPath" "$isCommit"

echo "-----------------Ended Canvas Config Automation -----------------------------"

