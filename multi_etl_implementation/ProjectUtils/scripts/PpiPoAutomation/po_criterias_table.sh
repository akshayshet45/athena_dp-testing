product_spec_path="configs"
db_host=$(cat $product_spec_path/product-spec.json | jq '.po.rds.host'|sed 's/\"//g')
db_username=$(cat $product_spec_path/product-spec.json | jq '.po.rds.username'|sed 's/\"//g')
db_pwd=$(cat $product_spec_path/product-spec.json | jq '.po.rds.password'|sed 's/\"//g')
export mysql_connection="mysql -h$db_host -u$db_username -p$db_pwd"
path="../IS-Platform/ProjectUtils/scripts/PpiPoAutomation"

#if [ -f $path/child_dd_elements_to_criterias.sql ]; then
$mysql_connection < $path/child_dd_elements_to_criterias.sql
#else
#echo "$path/child_dd_elements_to_criterias.sql not found"
#fi

#if [ -f $path/parent_dd_elements_to_criterias.sql ]; then
$mysql_connection < $path/parent_dd_elements_to_criterias.sql
$mysql_connection -e "USE COSMOS; update CRITERIAS set NAME=REPLACE( NAME,'_', ' ');"
#else
#echo "$path/parent_dd_elements_to_criterias.sql not found"
#fi
