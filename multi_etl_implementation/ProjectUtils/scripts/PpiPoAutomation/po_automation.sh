#!/bin/bash
set -e

product_spec_path="configs"
repo="athena_poconfig"
env="prod"
client_name="$1"
client_name_from_client_spec="$5"
USER_TAG=`id -un`
azkaban_project_name="$USER_TAG-$5"
if [ "$2" == "prod" ]; then
        azkaban_project_name="$5"
fi
transport_host=$(cat $product_spec_path/product-spec.json | jq '.po.transport.host'|sed 's/\"//g')
transposrt_username=$(cat $product_spec_path/product-spec.json | jq '.po.transport.username'|sed 's/\"//g')
transport_password=$(cat $product_spec_path/product-spec.json | jq '.po.transport.password'|sed 's/\"//g')
doPostProcessing_flag=$(cat $product_spec_path/product-spec.json | jq '.outputProcessing.doPostProcessing_flag'|sed 's/\"//g')
flow_name=$(cat $product_spec_path/product-spec.json | jq '.outputProcessing.flow_name'|sed 's/\"//g')
redshift_db_url="$2"
redshift_db_username="$3"
redshift_db_password="$4"
commit_flag="$6"


echo "--------------------------------- Syncing with repo------------------------------"
bash ../ProjectUtils/scripts/PpiPoAutomation/sync_repo.sh $repo
echo "--------------------------------- Synced with repo-------------------------------"


echo "-------------------------------- creating po-client-env configuration----------------------"
bash ../ProjectUtils/scripts/PpiPoAutomation/po-client-env.sh $client_name $env "$transport_host" "$transposrt_username" "$transport_password" "$redshift_db_url" "$redshift_db_username" "$redshift_db_password" "$doPostProcessing_flag" "$azkaban_project_name" "$flow_name"
echo "--------------------------------- Ended creating po-client-env configuration----------------"


#commit changes to repo, only if the commit flag is set to true
if [ "$commit_flag" == "true" ];then
	echo "--------------------------------- start commiting changes to repo--------------------------"
	bash ../ProjectUtils/scripts/PpiPoAutomation/push_changes_to_repo.sh $repo $client_name 
	echo "--------------------------------- Ended with commiting changes to repo---------------------"
fi
