
current_dir=`pwd`
reponame=$1
home_dir=$HOME
client=$2


cd $home_dir
if [ ! -d "$reponame" ]; then
echo "please clone repo properly: $reponame"
else
cd $reponame
git add .
git status
git commit -m"Automated commiting of client: $client"
git push origin master
cd $current_dir
fi
