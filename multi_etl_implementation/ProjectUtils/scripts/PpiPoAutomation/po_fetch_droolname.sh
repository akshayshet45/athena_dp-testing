#!/bin/bash
set -e

default_report_drools_string="sku,channelId,price,suggestedPriceStr,strategyName,stopProcessing,priceChanged,relationshipName"
default_outputfile_drools_string="sku,price,suggestedPriceStr,channelId"
default_reports_header="SKU,Channel Id,Current Price,SuggestedPrice,Strategy Name,Stop Processing,Price Changed,Relationship Name"
default_outputfile_header="SKU,Price,Suggested Price,Channel Id"
product_spec_path="configs"
db_host=$(cat $product_spec_path/product-spec.json | jq '.po.rds.host'|sed 's/\"//g')
db_username=$(cat $product_spec_path/product-spec.json | jq '.po.rds.username'|sed 's/\"//g')
db_pwd=$(cat $product_spec_path/product-spec.json | jq '.po.rds.password'|sed 's/\"//g')
path="configs"
export mysql_connection="mysql -sN -h$db_host -u$db_username -p$db_pwd"


if [ ! -f $path/product-spec.json ]; then
echo "$path/product-spec.json" is not present
exit 1
fi

# referenceFile
dictionary_col_name=$(cat $path/product-spec.json | jq '.referenceFile.data_dictionary'|sed 's/\"//g')
#echo "dictionary_col_name" $dictionary_col_name
IFS=',' read -ra dictionary_col_array <<< "$dictionary_col_name"
arraylenth=${#dictionary_col_array[@]}
drools_string="$default_report_drools_string"
echo "Generating Droolsname for reports:"

i=0
while [ "$i" -lt "$arraylenth" ]
do
drools_name=$( $mysql_connection -e "USE COSMOS; select distinct DROOLS_VARIABLE_NAME from CRITERIAS where MAP_COLUMN='${dictionary_col_array[$i]}'")
echo $drools_name
if [ ! -n "$drools_name" ]; then
echo "ERROR : ${dictionary_col_array[$i]} is not present in Data Dictionary. not able to find Drools name for ${dictionary_col_array[$i]}"
exit 1
fi

drools_string=$drools_string","$drools_name
i=`expr $i + 1`
done
#drools_string=$(echo "$drools_string" | cut -c2-)
header_name=$(echo "$default_reports_header,$dictionary_col_name"|sed 's/_/ /g')
header_name=$( echo $header_name | awk '{for(j=1;j<=NF;j++){ $j=toupper(substr($j,1,1)) substr($j,2) }}1' | awk -F, '{for(j=1;j<=NF;j++){ $j=toupper(substr($j,1,1)) substr($j,2) "," }}1' | sed 's/,$//')
echo $header_name

# outputfile
outputfile_dictionary_col_name="sku,title,gl_l1_name"
outputfile_dictionary_col_name=$(cat $path/product-spec.json | jq '.outputfile.data_dictionary'|sed 's/\"//g')
IFS=',' read -ra outputfile_dictionary_col_array <<< "$outputfile_dictionary_col_name"
outputfile_arraylenth=${#outputfile_dictionary_col_array[@]}
outputfile_drools_string="$default_outputfile_drools_string"

echo "Generating Droolsname for outputfile:"
j=0
while [ "$j" -lt "$outputfile_arraylenth" ]
do
outputfile_drools_name=$( $mysql_connection -e "USE COSMOS; select distinct DROOLS_VARIABLE_NAME from CRITERIAS where MAP_COLUMN='${outputfile_dictionary_col_array[$j]}'")
echo $outputfile_drools_name
if [ ! -n "$outputfile_drools_name" ]; then
echo "ERROR : ${outputfile_dictionary_col_array[$j]} is not present in Data Dictionary. not able to find Drools name for outputfile_dictionary_col_array[$j]"
exit 1
fi
outputfile_drools_string=$outputfile_drools_string","$outputfile_drools_name
j=`expr $j + 1`
done
#outputfile_drools_string=$(echo "$outputfile_drools_string" | cut -c2-)
outputfile_header_name=$(echo "$default_outputfile_header,$outputfile_dictionary_col_name"|sed 's/_/ /g')
#echo $outputfile_drools_string
#echo $outputfile_header_name

outputfile_header_name=$( echo $outputfile_header_name | awk '{for(j=1;j<=NF;j++){ $j=toupper(substr($j,1,1)) substr($j,2) }}1' | awk -F, '{for(j=1;j<=NF;j++){ $j=toupper(substr($j,1,1)) substr($j,2) "," }}1' | sed 's/,$//')
echo $outputfile_header_name

cp $path/product-spec.json $path/product-spec.json.bak

jq ".referenceFile.mapping=\"${drools_string}\"" $path/product-spec.json | jq ".referenceFile.headers=\"${header_name}\"" | jq ".outputfile.mapping=\"${outputfile_drools_string}\"" | jq ".outputfile.headers=\"${outputfile_header_name}\"" > $path/temp.json

if [ -s $path/temp.json ]; then
cp $path/temp.json $path/product-spec.json
echo "Edited mapping and header values in configs : $path/product-spec.json"
else
echo "Error.. please provide valid dictionary_col_name"
cp -f $path/product-spec.json.bak $path/product-spec.json
rm -f $path/temp.json
rm $path/product-spec.json.bak
exit 1 
fi

rm $path/product-spec.json.bak
rm -f $path/temp.json

