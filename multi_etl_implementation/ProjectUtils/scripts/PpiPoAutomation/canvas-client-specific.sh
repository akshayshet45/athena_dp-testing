#!/bin/bash
set -e

client_name=$1
template_value=( "<client>" "<template_vendor_name>" "<template_vendor_website>" "<template_email_domain>")
template_replacement=( "$client_name" "$2" "$3" "$4")
#template_replacement=( "$client_name-tiny.png" "staples,Staples,staples.com" "staples.com" "kuliza.com,setuserv.com,staples.com,rboomerang.com,boomerangcommerce.com" )
filepath="temp/${client_name}_config.yml"
template_location="../ProjectUtils/scripts/PpiPoAutomation/ConfigTemplates/template_canvas_client-specific.yml"
home_dir=$HOME

# Check if the config file, is already in git, if yes then exit to avoid rewrite of other configured
# values
if [ -e "$home_dir/athena_canvasconfig/canvas/vars/configs/client_specific/${client_name}_config.yml" ]
then
    echo "Error (Canvas client specific ) - ${client_name}_config.yml already exists in Repo, Cannot Override"
    exit 1
fi

#copy file from template to temp location
if [ -f $template_location ]; then
mkdir -p temp
cp -f $template_location $filepath
else
echo "template file not found : $template_location"
exit 1
fi

#replace value part in copy of template file.
if [ -f $filepath ]; then
i=0
arraylenth=${#template_value[@]}

while [ $i -lt $arraylenth ]
do
#echo $i
echo "${template_replacement[$i]} is replacement of ${template_value[$i]}"
sed -i.bak "s/${template_value[$i]}/${template_replacement[$i]}/g" $filepath
i=`expr $i + 1`
done

else 
echo "file is not present : $filepath"
exit 1 
fi


#Copy config file into repo
if [ -f $filepath -a -d $home_dir/athena_canvasconfig/canvas/vars/configs/client_specific ]; then
cp -f $filepath $home_dir/athena_canvasconfig/canvas/vars/configs/client_specific/${client_name}_config.yml  
rm -rf temp
else 
echo "$home_dir/athena_canvasconfig/canvas/vars/configs/client_specific is not exist, please clone/pull repo"
exit 1
fi

