#!/bin/bash
set -e

product_spec_path="configs"
repo="athena_ssconfig"
env="prod"

redshift_host=$(cat $product_spec_path/product-spec.json | jq '.common.redshift.host'|sed 's/\"//g')
redshift_url=$(cat $product_spec_path/product-spec.json | jq '.common.redshift.url'|sed 's/\"//g')
redshift_username=$(cat $product_spec_path/product-spec.json | jq '.common.redshift.username'|sed 's/\"//g')
redshift_password=$(cat $product_spec_path/product-spec.json | jq '.common.redshift.password'|sed 's/\"//g')
transport_host=$(cat $product_spec_path/product-spec.json | jq '.po.transport.host'|sed 's/\"//g')
transport_username=$(cat $product_spec_path/product-spec.json | jq '.po.transport.username'|sed 's/\"//g')
transport_password=$(cat $product_spec_path/product-spec.json | jq '.po.transport.password'|sed 's/\"//g')

client_name=$1
commit_flag="$2"



echo "--------------------------------- syncing with repo------------------------------"
bash ../ProjectUtils/scripts/PpiPoAutomation/sync_repo.sh $repo
echo "--------------------------------- synced with repo-------------------------------"

echo "--------------------------------- creating ss-client-env configuration---------------------------"
bash ../ProjectUtils/scripts/PpiPoAutomation/ss-client-env.sh $client_name $env $redshift_host $redshift_username $redshift_password $redshift_url $transport_host $transport_username $transport_password
echo "--------------------------------- Ended creating ss-client-env configuration---------------------"

#commit changes to repo, only if the commit flag is set to true
if [ "$commit_flag" == "true" ];then
	echo "--------------------------------- start commiting changes to repo--------------------------"
	# bash ../ProjectUtils/scripts/PpiPoAutomation/push_changes_to_repo.sh $repo $client_name 
	echo "--------------------------------- Ended with commiting changes to repo---------------------"
fi
