
repo="$2"
client_name="$1"
commit_flag="$3"
#commit changes to repo, only if the commit flag is set to true
if [ "$commit_flag" == "true" ];then
echo "Repo Name: $repo"

        echo "--------------------------------- start commiting changes to repo--------------------------"
        bash ../ProjectUtils/scripts/PpiPoAutomation/push_changes_to_repo.sh $repo $client_name
        echo "--------------------------------- Ended with commiting changes to repo---------------------"
fi
