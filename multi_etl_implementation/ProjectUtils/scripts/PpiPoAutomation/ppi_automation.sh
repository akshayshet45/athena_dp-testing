#!/bin/bash
set -e

product_spec_path="configs"
repo="athena_ppiconfig"
env="prod"
client_name="$1"
database_url="$2"
database_username="$3"
database_password="$4"
commit_flag="$5"
scraper_site=$(cat $product_spec_path/product-spec.json | jq '.ppi.scraperSiteName'|sed 's/\"//g')
client_url_pattern=$(cat $product_spec_path/product-spec.json | jq '.ppi.clientUrlPattern'|sed 's/\"//g')
echo "--------------------------------- syncing with repo------------------------------"
bash ../ProjectUtils/scripts/PpiPoAutomation/sync_repo.sh $repo
echo "--------------------------------- synced with repo-------------------------------"

echo "-------------------------------- creating ppi-client-specific configuration----------------------"
bash ../ProjectUtils/scripts/PpiPoAutomation/ppi-client-specific.sh $client_name "$scraper_site" "$client_url_pattern"
echo "--------------------------------- Ended creating ppi-client-specific configuration----------------"

echo "--------------------------------- creating ppi-client-env configuration---------------------------"
bash ../ProjectUtils/scripts/PpiPoAutomation/ppi-client-env.sh $client_name $env $database_url $database_username $database_password
echo "--------------------------------- Ended creating ppi-client-env configuration---------------------"

#commit changes to repo, only if the commit flag is set to true
if [ "$commit_flag" == "true" ];then
	echo "--------------------------------- start commiting changes to repo--------------------------"
	bash ../ProjectUtils/scripts/PpiPoAutomation/push_changes_to_repo.sh $repo $client_name 
	echo "--------------------------------- Ended with commiting changes to repo---------------------"
fi
