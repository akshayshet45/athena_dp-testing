#!/bin/bash
set -e

repo="athena_canvasconfig"
env="prod"

client_name="$1"
vendor_name="$2"
vendor_website="$3"
email_domain="$4"
commit_flag="$5"

echo "--------------------------------- syncing with repo------------------------------"
bash ../ProjectUtils/scripts/PpiPoAutomation/sync_repo.sh $repo
echo "--------------------------------- synced with repo-------------------------------"

echo "-------------------------------- creating canvas-client-specific configuration----------------------"
bash ../ProjectUtils/scripts/PpiPoAutomation/canvas-client-specific.sh $client_name $vendor_name $vendor_website $email_domain 
echo "--------------------------------- Ended creating canvas-client-specific configuration----------------"

echo "--------------------------------- creating canvas-client-env configuration---------------------------"
bash ../ProjectUtils/scripts/PpiPoAutomation/canvas-client-env.sh $client_name $env 
echo "--------------------------------- Ended creating canvas-client-env configuration---------------------"

#commit changes to repo, only if the commit flag is set to true
if [ "$commit_flag" == "true" ];then
	echo "--------------------------------- start commiting changes to repo--------------------------"
	bash ../ProjectUtils/scripts/PpiPoAutomation/push_changes_to_repo.sh $repo $client_name 
	echo "--------------------------------- Ended with commiting changes to repo---------------------"
fi
