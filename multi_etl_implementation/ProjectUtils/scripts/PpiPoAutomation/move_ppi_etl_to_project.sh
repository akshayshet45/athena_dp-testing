
current_dir=`pwd`
Data_Pipeline_location=$HOME
data_platform_branch="master"
overwritePPIETL=""
overwritePPIETL="$1"

#cloning/checkout the data platform master branch
cd $Data_Pipeline_location
if [ ! -d "athena_data-pipeline" ]; then
git clone https://jenkins:jenkins@athena.unfuddle.com/git/athena_data-pipeline/
git checkout $data_platform_branch
git pull origin $data_platform_branch
echo "repo cloned -  athena_data-pipeline"
else
cd athena_data-pipeline
#git reset --hard HEAD
git clean -f
git remote set-url origin https://jenkins:jenkins@athena.unfuddle.com/git/athena_data-pipeline/
git clean -fd
git reset HEAD
git stash
git checkout $data_platform_branch;
git pull origin $data_platform_branch
echo "repo checked out -  athena_data-pipeline"
fi

if [ -d $Data_Pipeline_location/athena_data-pipeline/validators/ETL_CONFIGS/ppi_etl ]; then
if [ ! -d $current_dir/etl/ppi-etl ]; then
mkdir -p $current_dir/etl/ppi-etl
else

if [ "$overwritePPIETL" != "true" ]; then
echo "$current_dir/etl/ppi-etl already exist. Provide --overwritePPIETL=true to override PPI ETL with grunt command"
exit 1;
else
echo "overwritePPIETL :  $overwritePPIETL"
rm -rf $current_dir/etl/ppi-etl
mkdir -p $current_dir/etl/ppi-etl
fi

fi
cp -rf $Data_Pipeline_location/athena_data-pipeline/validators/ETL_CONFIGS/ppi_etl/* $current_dir/etl/ppi-etl 
else
echo "ppi_etl is not available on $Data_Pipeline_location/athena_data-pipeline/validators/ETL_CONFIGS/ppi_etl"
exit 1;
fi

if [ -f $current_dir/etl/ppi-etl/etl-spec.json ]; then 
echo "PPI ETL copied successfully. Find PPI ETL on $current_dir/etl/ppi-etl/ location"
fi

