#!/bin/bash
set -e


client_name=$1
env=$2

template_value=( "<client>" )
template_replacement=($client_name )
#template_replacement=( "jdbc:mysql://qstae-uat.czx5r0zoummp.us-east-1.rds.amazonaws.com:3306/CMN" "cm_app_user" "C0mp3ng1n!" $client_name )

filepath="temp/${client_name}_${env}_config.yml"
template_location="../ProjectUtils/scripts/PpiPoAutomation/ConfigTemplates/template_canvas_env.yml"
home_dir=$HOME

# Check if the config file, is already in git, if yes then exit to avoid rewrite of other configured
# values
if [ -e "$home_dir/athena_canvasconfig/canvas/vars/configs/client_env/$client_name/${client_name}_${env}_config.yml" ]
then
    echo "Error (Canvas client env) - ${client_name}_${env}_config.yml already exists in Repo, Cannot Override"
    exit 1
fi

#copy file from template to temp location
if [ -f $template_location ]; then
mkdir -p temp
cp -f $template_location $filepath
else
echo "template file not found : $template_location"
exit 1;
fi

#replace value part in copy of template file.
if [ -f $filepath ]; then
i=0
arraylenth=${#template_value[@]}

while [ $i -lt $arraylenth ]
do
#echo $i
echo "${template_replacement[$i]} is replacement of ${template_value[$i]}"
value=$(echo ${template_replacement[$i]} | sed 's/\//\\\//g')
sed -i.bak "s/${template_value[$i]}/$value/g" $filepath
i=`expr $i + 1`
done

else 
echo "file is not present : $filepath"
exit 1 
fi



#Copy config file into repo
if [ -f $filepath -a -d $home_dir/athena_canvasconfig/canvas/vars/configs/client_env ]; then
mkdir -p $home_dir/athena_canvasconfig/canvas/vars/configs/client_env/$client_name
cp -f $filepath $home_dir/athena_canvasconfig/canvas/vars/configs/client_env/$client_name/${client_name}_${env}_config.yml  
rm -rf temp
else 
echo "$home_dir/athena_canvasconfig/canvas/vars/configs/client_env/$client_name, please clone/pull repo"
exit 1
fi

