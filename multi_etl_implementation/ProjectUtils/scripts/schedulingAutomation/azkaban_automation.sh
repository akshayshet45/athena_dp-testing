#!/bin/bash
set -e

default_azkaban_host_url="ec2-54-90-127-6.compute-1.amazonaws.com"
default_azkaban_username="azkaban"
default_azkaban_passwd="azkaban"

azkaban_host_url="$default_azkaban_host_url"
azkaban_username="$default_azkaban_username"
azkaban_passwd="$default_azkaban_passwd"
azkaban_host=http://$azkaban_host_url
product_spec_path="configs"

if [ -f "$product_spec_path/schedule-spec.json" ]; then

	azkaban_name=$(cat $product_spec_path/schedule-spec.json | jq '.azkaban.azkaban_name'|sed 's/\"//g')

	count=$(cat client-spec.json | jq ".endpoints | length")
	i=0

	while [ $i -lt $count ]
	do
		name=$(cat client-spec.json | jq ".endpoints[$i] | .name" |sed 's/\"//g')
		type=$(cat client-spec.json | jq ".endpoints[$i] | .type" |sed 's/\"//g')
		
		 
		if [ "$name" == "$azkaban_name" -a "$type" == "azkaban" ]; then
			azkaban_username=$(cat client-spec.json | jq ".endpoints[$i] | .properties.username" |sed 's/\"//g')
			azkaban_passwd=$(cat client-spec.json | jq ".endpoints[$i] | .properties.password" |sed 's/\"//g')
			azkaban_host_url=$(cat client-spec.json | jq ".endpoints[$i] | .properties.host" |sed 's/\"//g')
			azkaban_host=http://$azkaban_host_url
		fi

		i=`expr $i + 1`
	done

	echo "azkaban_host: " $azkaban_host
	count=$(cat $product_spec_path/schedule-spec.json | jq ".azkaban.schedule_list | length")
	echo "Total Number of schedule Task: $count"

	i=0
	while [ $i -lt $count ]
	do
		echo " "
		echo "---------scheduleing azkaban job: $i ---------"

		project_name=$(cat $product_spec_path/schedule-spec.json | jq ".azkaban.schedule_list[$i].project_name" |sed 's/\"//g')
		if [ "$project_name" ==  "null" ]; then
			project_name=$(cat client-spec.json | jq ".client" |sed 's/\"//g')
		fi

		flow_name=$(cat $product_spec_path/schedule-spec.json | jq ".azkaban.schedule_list[$i].flow_name" |sed 's/\"//g')
		schedule_date=$(cat $product_spec_path/schedule-spec.json | jq ".azkaban.schedule_list[$i].start_date" |sed 's/\"//g')
		schedule_time_zone="PST"
		schedule_am_or_pm=$(cat $product_spec_path/schedule-spec.json | jq ".azkaban.schedule_list[$i].time.am_or_pm" |sed 's/\"//g')
		schedule_minute=$(cat $product_spec_path/schedule-spec.json | jq ".azkaban.schedule_list[$i].time.minute" |sed 's/\"//g')
		schedule_hour=$(cat $product_spec_path/schedule-spec.json | jq ".azkaban.schedule_list[$i].time.hour" |sed 's/\"//g')
        schedule_time_zone=$(cat $product_spec_path/schedule-spec.json | jq ".azkaban.schedule_list[$i].time.time_zone" |sed 's/\"//g')
		repeat_every_freq=$(cat $product_spec_path/schedule-spec.json | jq ".azkaban.schedule_list[$i].repeat_every" |sed 's/\"//g')

		echo "Azkanban Project Name  $project_name"
		echo "Azkaban Flow Name $flow_name"

		bash ../ProjectUtils/scripts/schedulingAutomation/azkaban_single_job_scheduler.sh "$azkaban_username" "$azkaban_passwd" "$azkaban_host" "$project_name" "$flow_name" "$schedule_date" "$schedule_time_zone" "$schedule_am_or_pm" "$schedule_minute" "$schedule_hour" "$repeat_every_freq"

		i=`expr $i + 1`
	done

else
	echo "Error: Schedule specification file not exist on path : $product_spec_path/schedule-spec.json"
	exit 1
fi




