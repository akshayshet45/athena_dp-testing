#!/bin/bash
set -e
client=$1

#client='Gilt'
rootdir=`pwd`

echo "-----------------Starting Pushmon Automation  --------------------------"
pushmonConfigPath='pre-etl/pushmon.json'
preEtlPath="pre-etl/flows/properties/pushmon_pre_etl.json"
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.rboomerang.automation.pushmon.PushmonHandler $rootdir $pushmonConfigPath $client $preEtlPath
pushmonConfigPath="output-processing/pushmon.json"
outputProcessingPath="output-processing/flows/properties/pushmon_output_processing.json"
java -cp ../IS-Platform/ETLUtilsProjects/target/ETLUtilsProject-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.rboomerang.automation.pushmon.PushmonHandler $rootdir $pushmonConfigPath $client $outputProcessingPath
echo "-----------------Ended  Pushmon Automation -----------------------------"

