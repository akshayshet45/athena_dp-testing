azkaban_username=$1
azkaban_passwd=$2
project_name=$4
azkaban_host=$3
flow_name=$5
schedule_date=$6
schedule_time_zone=$7
schedule_am_or_pm=$8
schedule_minute=$9
schedule_hour=${10}
repeat_every_freq=${11}

#session generation through login.
login_response=$(curl -k -X POST --data "action=login&username=$azkaban_username&password=$azkaban_passwd" $azkaban_host:8081)

echo $login_response

status=$(echo $login_response | jq '."status"')
status=$(echo ${status:1:$(echo ${#status}) - 2})

echo $status

if [ "$status" == "success" ]
then
        echo "Azkaban login successfull."
else
        echo "ERROR in azkaban login. Check user/passwd or azkaban_host."
        exit 1
fi

session=$(echo $login_response | jq '."session.id"')
session=$(echo ${session:1:$(echo ${#session}) - 2})
echo "session : $session"

#list of flows
flow_response=$(curl -k --get --data "session.id=$session&ajax=fetchprojectflows&project=$project_name" $azkaban_host:8081/manager)

count=$(echo $flow_response | jq ".flows | length")
projectId=$(echo $flow_response | jq ".projectId")
echo " "
echo "Project ID : $projectId"
i=0
flow_flag="false"
echo "Available Flows :"
while [ $i -lt $count ]
do
        flow=$(echo $flow_response | jq ".flows[$i] | .flowId")
        flow=$(echo ${flow:1:$(echo ${#flow}) - 2})
        echo "project flow   = $flow"
        if [ "$flow" == "$flow_name" ]
        then
                flow_flag="found"
        fi
                i=`expr $i + 1`
done

if [ "$flow_flag" == "found" ]
then
    echo "Selected project flow for sceduling = $flow_name"
else
    echo "======> ERROR in fetching flow [$flow_name] of project [$project_name] from azkaban. Please check Azkaban Project Name and Flow Name."
    exit 1
fi

#start scheduling 
schedule_response=$(curl -k $azkaban_host:8081/schedule -d "ajax=scheduleFlow&is_recurring=on&period=$repeat_every_freq&projectName=$project_name&flow=$flow_name&projectId=$projectId&scheduleTime=$schedule_hour,$schedule_minute,$schedule_am_or_pm,$schedule_time_zone&scheduleDate=$schedule_date" -b azkaban.browser.session.id=$session)

echo $schedule_response
schedul_status=$(echo $login_response | jq '."status"'|sed 's/\"//g')

if [ "$schedul_status" == "success" ]
then
	echo "======> Flow_Name: $flow_name of Project_Name: $project_name is successfully scheduled on $schedule_hour:$schedule_minute $schedule_am_or_pm $schedule_time_zone"
else
	echo "======> scheduling of Flow_Name: $flow_name of Project_Name: $project_name is failed : $schedule_response"
	exit 1
fi
