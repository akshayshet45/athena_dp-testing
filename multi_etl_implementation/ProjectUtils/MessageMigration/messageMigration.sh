#!/bin/bash
set -e

CWD=`pwd`

if [ ! -f ../ProjectUtils/MessageMigration/MessageMigration-0.0.1-SNAPSHOT-jar-with-dependencies.jar ]; then
    echo "Message Migration project not found."
    exit 1
fi    
#mvn clean package install -DskipTests -f ../ProjectUtils/message/messageMigration/pom.xml

CLASSNAME="com.rboomerang.messageMigration.MessageMigration"

echo "Current working directory is : " $CWD
java -cp ../ProjectUtils/MessageMigration/MessageMigration-0.0.1-SNAPSHOT-jar-with-dependencies.jar $CLASSNAME $CWD


