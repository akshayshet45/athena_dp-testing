#!/bin/bash

set -e
set -x 
project_name="etl_custom_vj_snapshot"
version="0.0.1"
project_flow_to_be_schedule="internal_test_automation"

# call Start application of azkaban machine
bash /home/ubuntu/azk_deployment_scripts/start_application.sh $project_name $version $project_flow_to_be_schedule
